#include "ScaPhysicsManager.h"
#include "RogueCore.h"
#include "ScaGUISceneGame.h"
#include "ScaSceneGame.h"

using namespace Rogue;

#pragma region GET AND SET

void ScaPhysicsManager::resetNumUpdates() {
	_numUpdates = 0;
}

int ScaPhysicsManager::getNumUpdates() {
	return _numUpdates;
}

#pragma endregion

#pragma region UPDATE

void ScaPhysicsManager::updatePhysics() {
	PhysicsManager::updatePhysics();
	_numUpdates++;
}

void ScaPhysicsManager::onCollisionPairStay(CollisionPair& pair) {
	//Account for special conditions
	short group0 = pair.first->getBroadphaseHandle()->m_collisionFilterGroup;
	short group1 = pair.second->getBroadphaseHandle()->m_collisionFilterGroup;
	CollisionType collisionType = CT_NULL;
	if ((group0 == COL_CHAR) && (group1 == COL_CHAR)) collisionType = CT_DYNAMIC;
	else if (
				((group0 == COL_CHAR) && (group1 == COL_SCENE_BOUND)) ||
				((group0 == COL_SCENE_BOUND) && (group1 == COL_CHAR))
			)
	{
		collisionType = CT_STATIC;
	}
	else if (
				((group0 == COL_CHAR) && (group1 == COL_NAVMESH_PORTAL)) ||
				((group0 == COL_NAVMESH_PORTAL) && (group1 == COL_CHAR))
			)
	{
		collisionType = CT_PORTAL;
	}
	
	//Save the results
	if (collisionType == CT_STATIC) {
		static_cast<ScaGUISceneGame*>(Core::getManager<InterfaceGUIManager>()->getGUI("GuiSceneGame"))->updateDynamicCollisions();
	}
	else if (collisionType == CT_DYNAMIC) {
		static_cast<ScaGUISceneGame*>(Core::getManager<InterfaceGUIManager>()->getGUI("GuiSceneGame"))->updateStaticCollisions();
	}
	else if (collisionType == CT_PORTAL) {
		GhostObject* portal = (group0 == COL_NAVMESH_PORTAL)? static_cast<GhostObject*>(pair.first) : static_cast<GhostObject*>(pair.second);
		ScaSceneGame* currentScene = static_cast<ScaSceneGame*>(Scene::getCurrent());
		currentScene->portalSensorOverlapping(portal);
	}

	if (collisionType != CT_PORTAL) PhysicsManager::onCollisionPairStay(pair);
}

void ScaPhysicsManager::onCollisionPairAdded(CollisionPair& pair) {
	short group0 = pair.first->getBroadphaseHandle()->m_collisionFilterGroup;
	short group1 = pair.second->getBroadphaseHandle()->m_collisionFilterGroup;
	CollisionType collisionType = CT_NULL;
	if (
			((group0 == COL_CHAR) && (group1 == COL_NAVMESH_PORTAL)) ||
			((group0 == COL_NAVMESH_PORTAL) && (group1 == COL_CHAR))
		)
	{
		collisionType = CT_PORTAL;
	}
	
	//Save the results
	if (collisionType == CT_PORTAL) {
		GhostObject* portal = (group0 == COL_NAVMESH_PORTAL)? static_cast<GhostObject*>(pair.first) : static_cast<GhostObject*>(pair.second);
		ScaSceneGame* currentScene = static_cast<ScaSceneGame*>(Scene::getCurrent());
		currentScene->portalSensorTouched(portal);
	}

	if (collisionType != CT_PORTAL) PhysicsManager::onCollisionPairAdded(pair);
}

#pragma endregion