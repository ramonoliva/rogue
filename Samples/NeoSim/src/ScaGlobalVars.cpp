#include "ScaGlobalVars.h"

bool G_SHOW_CHARACTER = true;
bool G_SHOW_WAY_POINT = true;
bool G_SHOW_TRAJECTORY = true;

bool G_SHOW_CHARACTERS_SMALL = true;
bool G_SHOW_CHARACTERS_MEDIUM = true;
bool G_SHOW_CHARACTERS_BIG = true;

std::string G_RESULTS_FILE_NAME = "results.txt";
