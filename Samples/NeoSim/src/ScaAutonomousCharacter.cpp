#include "ScaAutonomousCharacter.h"
#include "ScaSceneGame.h"

void ScaAutonomousCharacter::setCurrentCellID(int cellID) {
	int prevCellID = currentCellID;
	Rogue::AutonomousCharacter::setCurrentCellID(cellID);
	if ((prevCellID != currentCellID) && (prevCellID >= 0) && (currentCellID >= 0)) {
		//A change of cell has been produced. Register it
		((ScaSceneGame*)Rogue::Scene::getCurrent())->portalCrossed(prevCellID, currentCellID);
	}
}