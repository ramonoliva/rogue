#include "ScaGameApp.h"

using namespace Rogue;

/***********************************************************************************/
/* CREATION AND DESTRUCTION */
/***********************************************************************************/
ScaGameApp::ScaGameApp() {

}

ScaGameApp::~ScaGameApp() {

}

void ScaGameApp::registerPhysicsManager() {
	//PhysicsManager
	_physicsManager = new ScaPhysicsManager();
	Core::registerManager<InterfacePhysicsManager>(_physicsManager);
}
			 
void ScaGameApp::registerScenes() {
	_sceneList.push_back(new ScaSceneGame());
}