#include "RogueGameApp.h"
#include "ScaGameApp.h"

#include "RogueGlobalVars.h"

using namespace Rogue;

/****************************************************************************************/
/* GLOBAL VARS INITIALIZATION */
/****************************************************************************************/
int Rogue::G_SCENE_STATE = 0;
String Rogue::G_APP_NAME = "NeoSim";
bool Rogue::G_MOUSE_EXCLUSIVE = true;

/****************************************************************************************/
/* MAIN	*/
/****************************************************************************************/
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT )
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
		Rogue::GameApp* app = new ScaGameApp();
		srand((unsigned int)time(NULL));

        try {
            app->go();
        } catch( Ogre::Exception& e ) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox( NULL, e.getFullDescription().c_str(), "An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occured: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }
		delete app;
        return 0;
    }

#ifdef __cplusplus
}
#endif