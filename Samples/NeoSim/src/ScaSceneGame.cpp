#include "ScaSceneGame.h"
#include "ScaGlobalVars.h"

#include "RogueDebuggerCharacter.h"

#include "RogueCore.h"
#include "GameMapManager.h"
#include "ScaAutonomousCharacter.h"

#include <exClearanceSolver.h>

using namespace Rogue;
using namespace GeometrySolver;
using namespace NEOGEN;
using namespace Exact;

static const float RADIUS_SMALL_CHAR = 0.5f; //0.5f;
static const float RADIUS_MEDIUM_CHAR = 1.0f; //1.5f;
static const float RADIUS_BIG_CHAR = 1.5f; //3.0f;

static const std::string DEBUGGER_PHYSICS_WORLD_NAME = "DebuggerPhysicsWorld";

bool sortIntFromHighToLow(int i,int j) { 
	return (i > j); 
}

/*******************************************************************************************************************/
/* CREATION AND DESTRUCTION */
/*******************************************************************************************************************/
ScaSceneGame::ScaSceneGame() {
	_mapIndex = 0;
	_isScriptedPath = false;
	_scriptedPath[0] = 4;
	_scriptedPath[4] = 3;
	_scriptedPath[3] = 5;
	_scriptedPath[5] = 2;
	_scriptedPath[2] = 6;
	_scriptedPath[6] = 1;
	_scriptedPath[1] = 7;
	_scriptedPath[7] = 0;
	_guiSceneGame = Core::getManager<InterfaceGUIManager>()->createGUI<ScaGUISceneGame>("GuiSceneGame");
	
	_selectedChar = NULL;

	//Clear the previous results file
	std::ofstream file;
	file.open(G_RESULTS_FILE_NAME.c_str(), std::ofstream::trunc);

	/*InterfaceGUIManager* guiManager = Core::getManager<InterfaceGUIManager>();
	int fileSubMenuID = guiManager->addMenuItem("&File", true);
	guiManager->addMenuItem("&New NavMesh", false, fileSubMenuID);
	guiManager->addMenuItem("&Load NavMesh", false, fileSubMenuID);
	guiManager->addMenuItem("&Save NavMesh", false, fileSubMenuID);
	guiManager->addMenuItem("&Close", false, fileSubMenuID);*/
}

ScaSceneGame::~ScaSceneGame() {
	Core::getManager<InterfaceGUIManager>()->destroyGUI("GuiSceneGame");
}

void ScaSceneGame::createScene() {
	SceneGame::createScene();
	const Ogre::StringVector& gameMapList = GameMapManager::getSingletonPtr()->getGameMapList();
	loadMap(gameMapList[_mapIndex]);
	ClearanceSolver& clearanceSolver = ClearanceSolver::getSingleton();
	_useTables = false;
	_useCriticalRadius = false;
	clearanceSolver.setAcceleration(_useTables, _useCriticalRadius);
	clearanceSolver.setNavMesh(_navMesh);
	clearanceSolver.computeClearance();
	_selectedChar = NULL;
	_isScriptedPath = false;
	_waypointMode = BaseCharacter::PORTAL_SEEK_PROJECTION;
	_shrinkPortalMode = ClearanceSolver::SP_EXACT;
	_guiSceneGame->reset();
	_debugCleared = false;
	
	resetResultsData();
}

void ScaSceneGame::destroyScene() {
	SceneGame::destroyScene();
	_debuggerCharacters.clear();
}

void ScaSceneGame::resetSimulation() {
	destroyCharacters();
	_debuggerCharacters.clear();
	createProximityDataBase();	//The proximity DataBase must be recreated
	_guiSceneGame->resetSimulation();
	resetResultsData();
}

void ScaSceneGame::resetResultsData() {
	saveCollisionHeader();
	((ScaPhysicsManager*)_physicsManager)->resetNumUpdates();
	_guiSceneGame->resetCollisions();
	_lastNumStaticCollisions = 0;
	_lastNumDynamicCollisions = 0;
	_timeSaveCollisions = 0;
	_elapsedSeconds = 0;
	_timeSimulationStart = _timer.getMilliseconds();
	_timeUpdateCrosses = _timer.getMilliseconds();
	_crossStep = 0;

	for (PortalSensor::iterator it = _portalSensorTraversals.begin(); it != _portalSensorTraversals.end(); it++) {
		it->second = 0;
	}

	for (PortalSensor::iterator it = _portalSensorOverlapping.begin(); it != _portalSensorOverlapping.end(); it++) {
		it->second = 0;
	}

	for (PortalCrossCounter::iterator it = _portalCrossCounter.begin(); it != _portalCrossCounter.end(); it++) {
		it->second.clear();
		it->second.push_back(0);
	}
}

void ScaSceneGame::loadNextMap() {
	size_t numMaps = GameMapManager::getSingletonPtr()->getGameMapList().size();
	_mapIndex = (_mapIndex + 1) % numMaps;
	destroyScene();
	createScene();
}

void ScaSceneGame::loadNavMesh(const String& fileName) {
	SceneGame::loadNavMesh(fileName);
	
	//Create a GhostObject for each portal that detects the characters that are in contact with the portal. 
	_portalSensorTraversals.clear();
	_portalSensorOverlapping.clear();
	_portalCrossCounter.clear();
	EdgeTable& eTable = _navMesh->getEdgeTable();
	std::set<int> portalsAdded;
	for (EdgeTable::iterator it = eTable.begin(); it != eTable.end(); it++) {
		if (it->second.isPortal()) {
			Vec3D v1 = _navMesh->getVertex(it->second.v1).getPosition();
			Vec3D v2 = _navMesh->getVertex(it->second.v2).getPosition();
			int portalID = std::min(it->second.id, it->second.opposite);
			if (portalsAdded.find(portalID) == portalsAdded.end()) {
				addPortalSensor(portalID);
				portalsAdded.insert(portalID);
			}
		}
	}
}

void ScaSceneGame::addPortalSensor(int portalID) {
	Edge& portal = _navMesh->getEdge(portalID);
	Vec3D& v1 = _navMesh->getVertex(portal.v1).getPosition();
	Vec3D& v2 = _navMesh->getVertex(portal.v2).getPosition();

	btConvexHullShape* shape = new btConvexHullShape();
	float extrusionDist = 0.5f;
	shape->addPoint(btVector3(v1.getX(), v1.getY(), v1.getZ()));					//v1
	shape->addPoint(btVector3(v2.getX(), v2.getY(), v2.getZ()));					//v2
	shape->addPoint(btVector3(v2.getX(), v2.getY() + extrusionDist, v2.getZ()));	//v2 extruded
	shape->addPoint(btVector3(v1.getX(), v1.getY() + extrusionDist, v1.getZ()));	//v1 extruded
	GhostObject* ghost = _physicsManager->createGhostObject(shape, COL_NAVMESH_PORTAL, navMeshPortalCollidesWith);
	_portalSensorTraversals[ghost] = 0;
	_portalSensorOverlapping[ghost] = 0;
	_portalCrossCounter[portalID].push_back(0);
}

void ScaSceneGame::portalCrossed(int oldCellID, int newCellID) {
	int portalID = _navMesh->getPortalTo(oldCellID, newCellID);
	Edge& portal = _navMesh->getEdge(portalID);
	int key = std::min(portal.id, portal.opposite);
	_portalCrossCounter[key][_crossStep]++;
}

void ScaSceneGame::portalSensorTouched(GhostObject* sensor) {
	if (_portalSensorTraversals.find(sensor) == _portalSensorTraversals.end()) return;
	_portalSensorTraversals[sensor]++;
}

void ScaSceneGame::portalSensorOverlapping(GhostObject* sensor) {
	if (_portalSensorOverlapping.find(sensor) == _portalSensorOverlapping.end()) return;
	_portalSensorOverlapping[sensor]++;
}

BaseCharacter* ScaSceneGame::loadCharacter(const Ogre::String& meshFile, float radius, float height, float mass, const Ogre::Vector3& pos, const Ogre::Vector3& scale) {
	//Configure Ogre Character Attributes
	BaseCharConfigurator baseCFG;
	baseCFG.isInstanced = false;
	baseCFG.ent = _sceneMgr->createEntity("charCylinder.mesh");
	baseCFG.node = _sceneMgr->getRootSceneNode()->createChildSceneNode();
	baseCFG.node->setPosition(pos + Ogre::Vector3(0.0f, 0.1f, 0.0f));
	baseCFG.node->setScale(scale);
	baseCFG.pos = pos;
	baseCFG.navMesh = _navMesh;
	baseCFG.height = height;
	baseCFG.radius = radius;

	//Configure Bullet Character Attributes
	CollisionShapeCapsule* shape = new CollisionShapeCapsule(radius, std::max(0.0f, height - 2.0f * radius));
	RigidBody* rigidBody = _physicsManager->createRigidBody(shape, mass, Vector3(pos.x, pos.y, pos.z), COL_CHAR, charCollidesWith);

	//GhostObject* ghost = _physicsManager->createGhostObject(shape, COL_CHAR, charCollidesWith);
	//rigidBody->setGhostObject(ghost);

	//Configure Open Steer Attributes
	OpenSteerCharConfigurator openSteerCFG;
	openSteerCFG.pdb = _proximityDB;

	BaseCharacter* character = new ScaAutonomousCharacter(baseCFG, rigidBody, openSteerCFG);
	_charList.push_back(character);

	//Create a debugger for this character
	DebuggerCharacter* debugger = new DebuggerCharacter(character);

	if (RealMath::equal(character->radius(), RADIUS_SMALL_CHAR)) {
		debugger->setMaterialTrajectory("Green");
	}
	else if (RealMath::equal(character->radius(), RADIUS_MEDIUM_CHAR)) {
		debugger->setMaterialTrajectory("Yellow");
	}
	else if (RealMath::equal(character->radius(), RADIUS_BIG_CHAR)) {
		debugger->setMaterialTrajectory("Red");
	}
	
	_debuggerCharacters.push_back(debugger);

	return character;
}

/*******************************************************************************************************************/
/* UPDATE */
/*******************************************************************************************************************/
void ScaSceneGame::update() {
	_cameraManager->update();
	_guiManager->update();
	updateInput();
	if (!_pause) {
		_physicsManager->update();	
		updateDebugger();
	}
	updateLogic();
	Core::getManager<InterfaceRenderManager>()->update();

	updatePortalCrosses();

	//save the collision results each 5 seconds
	unsigned long now = _timer.getMilliseconds();
	unsigned long elapsedTime = now - _timeSaveCollisions;
	if (elapsedTime >= 5000) {
		_elapsedSeconds += 5;
		saveCollisionData();
		_timeSaveCollisions = now;
	}
}

void ScaSceneGame::updatePortalCrosses() {
	unsigned long now = _timer.getMilliseconds();
	unsigned long elapsedTime = now - _timeUpdateCrosses;
	if (elapsedTime >= 500) {
		//A cross counter step has been finisihed, so add a new counter for the new step
		for (PortalCrossCounter::iterator it = _portalCrossCounter.begin(); it != _portalCrossCounter.end(); it++) {
			it->second.push_back(0);
		}
		_timeUpdateCrosses = now;
		_crossStep++;
	}
}

void ScaSceneGame::updateDebugger() {
	if (_guiSceneGame->isShowingDebug()) {
		_debugCleared = false;
	}
	else {
		if (!_debugCleared) {
			_debugCleared = true;
		}
	}

	for (CharList::iterator it = _charList.begin(); it != _charList.end(); it++) {
		DebuggerCharacter* debugger = dynamic_cast<DebuggerCharacter*>((*it)->getDebugger());
		debugger->setVisibleShrinkedPortal(*it == _selectedChar);
	}
}

void ScaSceneGame::updateInput() {
	Core::getManager<InterfaceInputManager>()->update();
	processMouseEvents();
	processKeyboardEvents();
	processGUIEvents();	
}

void ScaSceneGame::processMouseEvents() {
	if (_inputManager->isTriggeredMouseButton(OIS::MB_Left)) {
		RayCastResult result;
		_physicsManager->rayCastCursor(result, _renderManager->getMainCamera());
		_selectedChar = result.character;
	}
}

void ScaSceneGame::processGUIEvents() {
	G_SHOW_CHARACTER = _guiSceneGame->isShowingCharacter();
	G_SHOW_WAY_POINT = _guiSceneGame->isShowingAttractorPoint();
	G_SHOW_TRAJECTORY = _guiSceneGame->isShowingTrajectory();

	if (_inputManager->isTriggeredKey(OIS::KC_RIGHT)) {
		_debuggerNavMesh->setEdgeSize(_debuggerNavMesh->getEdgeSize() + 0.05f);
		_debuggerNavMesh->update();
	}

	if (_inputManager->isTriggeredKey(OIS::KC_LEFT)) {
		_debuggerNavMesh->setEdgeSize(_debuggerNavMesh->getEdgeSize() - 0.05f);
		_debuggerNavMesh->update();
	}

	if (_inputManager->isTriggeredKey(OIS::KC_UP)) {
		float x, y, z;
		_debuggerNavMesh->getEdgeOffset(x, y, z);
		_debuggerNavMesh->setEdgeOffset(x, y + 0.05, z);
		_debuggerNavMesh->update();
	}

	if (_inputManager->isTriggeredKey(OIS::KC_DOWN)) {
		float x, y, z;
		_debuggerNavMesh->getEdgeOffset(x, y, z);
		_debuggerNavMesh->setEdgeOffset(x, y - 0.05, z);
		_debuggerNavMesh->update();
	}

	//<-- PORTAL SHRINK MODE
	if (_inputManager->isTriggeredKey(OIS::KC_1)) {
		_guiSceneGame->setShrinkPortalMode(ClearanceSolver::SP_EXACT);
	}
	if (_inputManager->isTriggeredKey(OIS::KC_2)) {
		_guiSceneGame->setShrinkPortalMode(ClearanceSolver::SP_SIMPLE);
	}
	if (_guiSceneGame->getShrinkPortalMode() != _shrinkPortalMode) {
		_shrinkPortalMode = _guiSceneGame->getShrinkPortalMode();
		setShrinkPortalMode(_shrinkPortalMode);
	}
	//-->
	
	//<-- WAYPOINT MODE
	if (_inputManager->isTriggeredKey(OIS::KC_3)) {
		_guiSceneGame->setWaypointMode(BaseCharacter::PORTAL_SEEK_PROJECTION);
	}
	if (_inputManager->isTriggeredKey(OIS::KC_4)) {
		_guiSceneGame->setWaypointMode(BaseCharacter::PORTAL_SEEK_MID_POINT);
	}
	if (_guiSceneGame->getWaypointMode() != _waypointMode) {
		_waypointMode = _guiSceneGame->getWaypointMode();
		setWaypointMode(_waypointMode);
	}
	//-->

	//<-- RECURSIVITY
	if (_inputManager->isTriggeredKey(OIS::KC_5)) {
		_guiSceneGame->useRecursivity(!_guiSceneGame->isUsingRecursivity());
	}
	ClearanceSolver& cSolver = ClearanceSolver::getSingleton();
	if (cSolver.isUsingRecursivity() != _guiSceneGame->isUsingRecursivity()) {
		cSolver.useRecursivity(_guiSceneGame->isUsingRecursivity());
		resetResultsData();
	}
	//-->

	if (_inputManager->isTriggeredKey(OIS::KC_6)) {
		_guiSceneGame->showDebug(!_guiSceneGame->isShowingDebug());
	}

	if (_inputManager->isTriggeredKey(OIS::KC_7)) {
		_guiSceneGame->showCharacter(!_guiSceneGame->isShowingCharacter());
	}

	if (_inputManager->isTriggeredKey(OIS::KC_8)) {
		_guiSceneGame->showAttractorPoint(!_guiSceneGame->isShowingAttractorPoint());
	}

	if (_inputManager->isTriggeredKey(OIS::KC_9)) {
		_guiSceneGame->showTrajectory(!_guiSceneGame->isShowingTrajectory());
	}

	if (_inputManager->isTriggeredKey(OIS::KC_RETURN) || _guiSceneGame->isTriggeredButtonAddCharacters()) {
		savePortalFlow();
		_guiSceneGame->addCharacters();
		initCharacters(_guiSceneGame->getNumCharacters());
		resetResultsData();
	}
	if (_inputManager->isTriggeredKey(OIS::KC_C) || _guiSceneGame->isTriggeredButtonResetCollisions()) {
		savePortalFlow();
		resetResultsData();
	}
	if (_inputManager->isTriggeredKey(OIS::KC_R) || _guiSceneGame->isTriggeredButtonResetSimulation()) {
		savePortalFlow();
		resetSimulation();
	}
	if (_inputManager->isTriggeredKey(OIS::KC_N) || _guiSceneGame->isTriggeredButtonNextScene()) {
		savePortalFlow();
		loadNextMap();
	}
}

void ScaSceneGame::processKeyboardEvents() {
	InterfaceInputManager* inputManager = Core::getManager<InterfaceInputManager>();
	if (inputManager->isTriggeredKey(OIS::KC_F1)) G_SHOW_CHARACTERS_SMALL = !G_SHOW_CHARACTERS_SMALL;
	if (inputManager->isTriggeredKey(OIS::KC_F2)) G_SHOW_CHARACTERS_MEDIUM = !G_SHOW_CHARACTERS_MEDIUM;
	if (inputManager->isTriggeredKey(OIS::KC_F3)) G_SHOW_CHARACTERS_BIG = !G_SHOW_CHARACTERS_BIG;
	if (inputManager->isTriggeredKey(OIS::KC_F4)) setScriptedPath();
	if (inputManager->isTriggeredKey(OIS::KC_F5)) scaTest1();
	if (inputManager->isTriggeredKey(OIS::KC_F6)) {
		_useTables = !_useTables;
		ClearanceSolver::getSingleton().setAcceleration(_useTables, _useCriticalRadius);
	}
	if (inputManager->isTriggeredKey(OIS::KC_F7)) {
		_useCriticalRadius = !_useCriticalRadius;
		ClearanceSolver::getSingleton().setAcceleration(_useTables, _useCriticalRadius);
	}
	if (inputManager->isTriggeredKey(OIS::KC_F11)) {
		/*DebuggerBase* debugger = Core::getManager<InterfaceDebuggerManager>()->getDebugger(DEBUGGER_PHYSICS_WORLD_NAME);
		debugger->setVisible(!debugger->isVisible());*/
	}
	if (inputManager->isTriggeredKey(OIS::KC_ESCAPE)) {
		savePortalFlow();
		G_SCENE_STATE = SCENE_NULL;	
	}
}

/*******************************************************************************************************************/
/* CLASS FUNCTIONS */
/*******************************************************************************************************************/
void ScaSceneGame::scaTest1() {
	Ogre::LogManager::getSingletonPtr()->logMessage("***************************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("TEST SHRINK PERFORMANCE");
	Ogre::LogManager::getSingletonPtr()->logMessage("***************************************************");

	//Open the cfg file
	std::ifstream iFile;
	std::string filename = "scaTest1.cfg";
    iFile.open(filename.c_str(), std::ios::in);
	if (!iFile.is_open()) {
		Ogre::LogManager::getSingletonPtr()->logMessage("ERROR OPENING CFG FILE!!!");
	}
	int minNumIterations, maxNumIterations, step, numHits;
	iFile >> minNumIterations;
	iFile >> maxNumIterations;
	iFile >> step;
	iFile.close();

	Ogre::Timer timer;
	timer.reset();
	CellTable& cTable = _navMesh->getCellTable();
	Ogre::LogManager::getSingletonPtr()->logMessage("numCells = " + std::to_string(cTable.size()));
	size_t numPortals = 0;
	EdgeTable& eTable = _navMesh->getEdgeTable();
	for (EdgeTable::iterator it = eTable.begin(); it != eTable.end(); it++) {
		if (it->second.isPortal()) numPortals++;
	}
	numPortals /= 2;
	Ogre::LogManager::getSingletonPtr()->logMessage("numPortals = " + std::to_string(numPortals));
	Cell* c;
	int r;
	CellTable::iterator itCell;
	std::set<int>::iterator itPortal;
	int entryPortalID, exitPortalID;
	Vec3D v1EntryPortal, v2EntryPortal;
	std::vector<float> clearanceValues;
	clearanceValues.push_back(RADIUS_SMALL_CHAR);
	clearanceValues.push_back(RADIUS_MEDIUM_CHAR);
	clearanceValues.push_back(RADIUS_BIG_CHAR);
	float clearance;
	unsigned long timeBefore, timeAfter;
	std::vector<unsigned long> timeShrink;
	std::vector<float> timeMean;
	for (size_t i = 0; i < 4; i++) {
		timeShrink.push_back(0);
		timeMean.push_back(0);
	}

	//Open the output file where we store the simplified results, ready to paste to an excel file and create the table
	std::ofstream oFile;
	if (_useCriticalRadius) filename = "scaTest1Results_CriticalRadiusON.txt";
	else filename = "scaTest1Results_CriticalRadiusOFF.txt";
	oFile.open(filename.c_str(), std::ios::out);
	if (!oFile.is_open()) {
		Ogre::LogManager::getSingletonPtr()->logMessage("ERROR OPENING OUTPUT FILE!!!");
	}
	//Output the header of the result table
	oFile << "useCriticalRadius\t" << _useCriticalRadius << std::endl;
	oFile << "\t" << "ShrinkSimple(-)\t" << "ShrinkSimple(+)\t" << "ShrinkExact(-)\t" << "ShrinkExact(+)";
	std::string sValue;	//Auxiliar string used to temporaly store the string representation of a float value

	Path path;
	ClearanceSolver& clearanceSolver = ClearanceSolver::getSingleton();
	for (int numIterations = minNumIterations; numIterations <= maxNumIterations; numIterations += step) {
		clearanceSolver.clearShrinkedTables();
		for (size_t i = 0; i < timeShrink.size(); i++) {
			timeShrink[i] = 0;
		}
		numHits = 0;
		for (int i = 0; i < numIterations; i++) {
			//Get a random cell
			c = &_navMesh->getCell(_navMesh->getRandomCell());
		
			//Determine a random cross
			if ((c->portals.size() == 1)) {
				//Cell has a single portal
				entryPortalID = *(c->portals.begin());
				exitPortalID = -1;
			}
			else {
				//Cell has multiple portals
				//1) Choose a random entry portal
				r = rand() % c->portals.size();
				itPortal = c->portals.begin();
				std::advance(itPortal, r);
				entryPortalID = *itPortal;
			
				//2) Choose a random exit portal that must be different from the entryPortal
				exitPortalID = -1;
				for (itPortal = c->portals.begin(); itPortal != c->portals.end(); itPortal++) {
					if ((*itPortal) == entryPortalID) continue;
					if ((exitPortalID == -1) || ((rand() % 2) == 0)) {
						exitPortalID = *itPortal;
					}
				}
			}
			path.clear();
			Edge& entryPortal = _navMesh->getEdge(entryPortalID);
			path.addNode(entryPortal.cellID);

			if (exitPortalID != -1) {
				Edge& exitPortal = _navMesh->getEdge(exitPortalID);
				path.addNode(_navMesh->getEdge(exitPortal.opposite).cellID);
			}

			//Choose a random clearance value
			clearance = clearanceValues[rand() % clearanceValues.size()];

			//FAST SHRINKED TIME
			//clearanceSolver.setAcceleration(false, true);
			clearanceSolver.setAcceleration(false, _useCriticalRadius);
			timeBefore = timer.getMicroseconds();
			clearanceSolver.setShrinkPortalMode(ClearanceSolver::SP_SIMPLE);
			clearanceSolver.shrinkPortal(entryPortalID, path, clearance, v1EntryPortal, v2EntryPortal);
			timeAfter = timer.getMicroseconds();
			timeShrink[0] += (timeAfter - timeBefore);

			//FAST SHRINKED TIME
			clearanceSolver.setAcceleration(true, _useCriticalRadius);
			timeBefore = timer.getMicroseconds();
			clearanceSolver.setShrinkPortalMode(ClearanceSolver::SP_SIMPLE);
			if (clearanceSolver.shrinkPortal(entryPortalID, path, clearance, v1EntryPortal, v2EntryPortal)) numHits++;
			timeAfter = timer.getMicroseconds();
			timeShrink[1] += (timeAfter - timeBefore);

			//EXACT SHRINKED SIMPLE TIME
			clearanceSolver.setAcceleration(false, _useCriticalRadius);
			timeBefore = timer.getMicroseconds();
			clearanceSolver.setShrinkPortalMode(ClearanceSolver::SP_EXACT);
			clearanceSolver.shrinkPortal(entryPortalID, path, clearance, v1EntryPortal, v2EntryPortal);
			timeAfter = timer.getMicroseconds();
			timeShrink[2] += (timeAfter - timeBefore);
		
			//EXACT SHRINKED ACCELERATED TIME
			clearanceSolver.setAcceleration(true, _useCriticalRadius);
			timeBefore = timer.getMicroseconds();
			clearanceSolver.setShrinkPortalMode(ClearanceSolver::SP_EXACT);
			clearanceSolver.shrinkPortal(entryPortalID, path, clearance, v1EntryPortal, v2EntryPortal);
			timeAfter = timer.getMicroseconds();
			timeShrink[3] += (timeAfter - timeBefore);
		}
		//Get the mean time per query
		for (size_t i = 0; i < timeMean.size(); i++) timeMean[i] = float(timeShrink[i]) / float(numIterations);
		Ogre::LogManager::getSingletonPtr()->logMessage("***************************************************");
		Ogre::LogManager::getSingletonPtr()->logMessage("RESULTS FOR NUM ITERATIONS = " + std::to_string(numIterations));
		Ogre::LogManager::getSingletonPtr()->logMessage("ShrinkSimple(-) Mean Time (microseconds) = " + std::to_string(timeMean[0]));
		Ogre::LogManager::getSingletonPtr()->logMessage("ShrinkSimple(+) Mean Time (microseconds) = " + std::to_string(timeMean[1]));
		Ogre::LogManager::getSingletonPtr()->logMessage("ShrinkExact(-) Mean Time (microseconds) = " + std::to_string(timeMean[2]));
		Ogre::LogManager::getSingletonPtr()->logMessage("ShrinkExact(+) Mean Time (microseconds) = " + std::to_string(timeMean[3]));
		Ogre::LogManager::getSingletonPtr()->logMessage("Using Critical Radius = " + std::to_string(_useCriticalRadius));
		Ogre::LogManager::getSingletonPtr()->logMessage("+ VERSION");
		Ogre::LogManager::getSingletonPtr()->logMessage("Max Num of Table Entries = " + std::to_string(numPortals * clearanceValues.size()));
		Ogre::LogManager::getSingletonPtr()->logMessage("Number of hits = " + std::to_string(numHits));
		Ogre::LogManager::getSingletonPtr()->logMessage("Probability of a hit at the end = " + std::to_string(float(numHits) / float(numIterations)));
		Ogre::LogManager::getSingletonPtr()->logMessage("EFFICIENCY OF SHRINKEXACT(+)");
		Ogre::LogManager::getSingletonPtr()->logMessage("ShrinkExact(+) is " + std::to_string(timeMean[3] / timeMean[0]) + " times the cost of ShrinkSimple(-)");
		Ogre::LogManager::getSingletonPtr()->logMessage("ShrinkExact(+) is " + std::to_string(timeMean[3] / timeMean[1]) + " times the cost of ShrinkSimple(+)");
		Ogre::LogManager::getSingletonPtr()->logMessage("ShrinkExact(+) is " + std::to_string(timeMean[3] / timeMean[2]) + " times the cost of ShrinkExact(-)");

		//Simplified results, ready for excel
		oFile << std::endl << numIterations; //The number of iterations
		for (size_t i = 0; i < timeMean.size(); i++) {
			sValue = std::to_string(timeMean[i]);
			//Replace the . of the float by a , as it is the representation used by excel
			for (size_t j = 0; j < sValue.size(); j++) {
				if (sValue[j] == '.') {
					sValue[j] = ',';
					break;
				}
			}
			oFile << "\t" << sValue; //Time spent by the different versions of the algorithm
		}
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("***************************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("CONGRATULATIONS, TEST COMPLETED!!!");
	Ogre::LogManager::getSingletonPtr()->logMessage("***************************************************");
	oFile.close();
}

void ScaSceneGame::scaTest2(std::vector<int>& portals) {
	//PROJECTION TEST
	Ogre::LogManager::getSingletonPtr()->logMessage("PERFORMANCE TEST 2");
	Ogre::Timer timer;
	timer.reset();
	IntersectionPoint3D projection;
	Vec3D v, v1Portal, v2Portal, steeringPoint;
	Edge* portal;
	float d1, d2;
	unsigned long timeProjectionBEFORE, timeProjectionAFTER, timeProjection;
	unsigned long timeMidBEFORE, timeMidAFTER, timeMid;
	timeProjection = 0;
	timeMid = 0;
	for (size_t i = 0; i < 1000; i ++) {
		//Agafo un portal a l'atzar
		portal = &(_navMesh->getEdge(portals[rand() % portals.size()]));
		v1Portal = _navMesh->getVertex(portal->v1).getPosition();
		v2Portal = _navMesh->getVertex(portal->v2).getPosition();
	
		//PROJECTION CASE
		timeProjectionBEFORE = timer.getMicroseconds();
		GeometrySolver::projectionPointLine3D(v, v1Portal, v2Portal, projection); //projecci� del centre del ninja sobre el portal 
		if (GeometrySolver::isBetween(projection.location, v1Portal, v2Portal)) {
			//La projecci� cau entre els extrems del portal transformats. 
			//Per tant, agafo aquest punt com a punt de pas
			steeringPoint = projection.location;
		}
		else {
			//La projecci� CAU FORA dels extrems del portal transformats. 
			//Aagafo el punt que queda m�s allunyat respecte el personatge
			d1 = GeometrySolver::to2D(v).dist2(GeometrySolver::to2D(v1Portal));
			d2 = GeometrySolver::to2D(v).dist2(GeometrySolver::to2D(v2Portal));
			if (d1 > d2) steeringPoint = v1Portal;
			else steeringPoint = v2Portal;
		}
		timeProjectionAFTER = timer.getMicroseconds();
		timeProjection += (timeProjectionAFTER - timeProjectionBEFORE);

		//MIDPOINT CASE
		timeMidBEFORE = timer.getMicroseconds();
		steeringPoint = (v1Portal + v2Portal) / 2.0f;
		timeMidAFTER = timer.getMicroseconds();
		timeMid += (timeMidAFTER - timeMidBEFORE);
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("TEST COMPLETED!");
	Ogre::LogManager::getSingletonPtr()->logMessage("timeMid = " + std::to_string(timeMid));
	Ogre::LogManager::getSingletonPtr()->logMessage("timeProjection = " + std::to_string(timeProjection));
}

void ScaSceneGame::scaTest3() {
	Ogre::LogManager::getSingletonPtr()->logMessage("PERFORMANCE TEST 3");
	Ogre::Timer timer;
	timer.reset();
	size_t numIt = 1000;
	PathFinder& pFinder = PathFinder::getSingleton();
	CellTable& cTable = _navMesh->getCellTable();
	Ogre::LogManager::getSingletonPtr()->logMessage("numCells = " + std::to_string(cTable.size()));
	Vec3D goalPosition;
	Path path;
	CellTable::iterator itCell1, itCell2;
	
	std::vector<float> clearanceValues;
	clearanceValues.push_back(0.0f);
	clearanceValues.push_back(RADIUS_SMALL_CHAR);
	clearanceValues.push_back(RADIUS_MEDIUM_CHAR);
	clearanceValues.push_back(RADIUS_BIG_CHAR);
	
	std::vector<int> hits;
	std::vector<unsigned long> timeAstar;
	for (size_t i = 0; i < clearanceValues.size(); i++) {
		hits.push_back(0);
		timeAstar.push_back(0);
	}
		
	unsigned long timeAstarBEFORE, timeAstarAFTER;
	
	int k = 0;
	for (size_t i = 0; i < numIt; i++) {
		itCell1 = cTable.begin();
		std::advance(itCell1, rand() % cTable.size());
		do {
			itCell2 = cTable.begin();
			std::advance(itCell2, rand() % cTable.size());
		} while (itCell2->first == itCell1->first);
		itCell2->second.p->getCenter(goalPosition);
		for (size_t j = 0; j < clearanceValues.size(); j++) {
			timeAstarBEFORE = timer.getMilliseconds();
			pFinder.solve(itCell1->first, itCell2->first, goalPosition, clearanceValues[j], path);
			timeAstarAFTER = timer.getMilliseconds();
			timeAstar[j] += (timeAstarAFTER - timeAstarBEFORE);
			if (!path.empty()) hits[j]++;
			k++;
		}
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("k = " + std::to_string(k));
	Ogre::LogManager::getSingletonPtr()->logMessage("TEST COMPLETED");
	Ogre::LogManager::getSingletonPtr()->logMessage("timeAstar[0] = " + std::to_string(timeAstar[0]));
	Ogre::LogManager::getSingletonPtr()->logMessage("timeAstar[1] = " + std::to_string(timeAstar[1]));
	Ogre::LogManager::getSingletonPtr()->logMessage("timeAstar[2] = " + std::to_string(timeAstar[2]));
	Ogre::LogManager::getSingletonPtr()->logMessage("timeAstar[3] = " + std::to_string(timeAstar[3]));
	Ogre::LogManager::getSingletonPtr()->logMessage("hits[0] = " + std::to_string(hits[0]));
	Ogre::LogManager::getSingletonPtr()->logMessage("hits[1] = " + std::to_string(hits[1]));
	Ogre::LogManager::getSingletonPtr()->logMessage("hits[2] = " + std::to_string(hits[2]));
	Ogre::LogManager::getSingletonPtr()->logMessage("hits[3] = " + std::to_string(hits[3]));
}

void ScaSceneGame::setShrinkPortalMode(int mode) {
	ClearanceSolver& cSolver = ClearanceSolver::getSingleton();
	cSolver.setShrinkPortalMode(ClearanceSolver::ShrinkPortalMode(mode));
	resetResultsData();
}

void ScaSceneGame::setWaypointMode(int mode) {
	BaseCharacter* character;
	for (CharList::iterator it = _charList.begin(); it != _charList.end(); it++) {
		character = *it;
		if (character->getWaypointMode() != BaseCharacter::PORTAL_SEEK_NULL) {
			character->setWaypointMode(mode);
		}
	}

	//Clear the debuggers
	for (DebuggerCharacterVector::iterator it = _debuggerCharacters.begin(); it != _debuggerCharacters.end(); it++) {
		(*it)->clearSamplePoints();
	}

	savePortalFlow();
	resetResultsData();
}

void ScaSceneGame::badShrinkedPortalDetection() {
	/*float r = 1.5f; //;1.0f; //0.5f;
	EdgeTable& eTable = _navMesh->getEdgeTable();
	Vec3D originalV1, originalV2;	//V1 and V2 BEFORE the shrinkment process
	Vec3D shrinkedV1, shrinkedV2;
	Edge* edge;
	Cell* c;
	for (EdgeTable::iterator it = eTable.begin(); it != eTable.end(); it++) {
		edge = &(it->second);
		originalV1 = *(_navMesh->getVertex(edge->v1).v);
		originalV2 = *(_navMesh->getVertex(edge->v2).v);
		if ((edge->isPortal) && (originalV1.dist2(originalV2) > (r*r))) {
			//The edge is a portal that the character can pass through.
			c = &(_navMesh->getCell(edge->cellID));
			for (std::set<int>::iterator itPortal = c->portals.begin(); itPortal != c->portals.end(); itPortal++) {
				if (*itPortal == edge->id) continue;
				ClearanceSolver::getSingleton().shrinkPortal(edge->id, *itPortal, r, shrinkedV1, shrinkedV2, true);
				if (!GeometrySolver::isBetween(shrinkedV1, originalV1, originalV2)) {
					Ogre::LogManager::getSingletonPtr()->logMessage("ENTRY PORTAL = " + std::to_string(edge->id));
					Ogre::LogManager::getSingletonPtr()->logMessage("EXIT PORTAL = " + std::to_string(*itPortal));
					Ogre::LogManager::getSingletonPtr()->logMessage("BAD SHRINKED V1!!!");
				}
				if (!GeometrySolver::isBetween(shrinkedV2, originalV1, originalV2)) {
					Ogre::LogManager::getSingletonPtr()->logMessage("ENTRY PORTAL = " + std::to_string(edge->id));
					Ogre::LogManager::getSingletonPtr()->logMessage("EXIT PORTAL = " + std::to_string(*itPortal));
					Ogre::LogManager::getSingletonPtr()->logMessage("BAD SHRINKED V2!!!");
				}
			}
		}
	}*/
}

void ScaSceneGame::initCharacters(int numChars) {
	int charType;
	bool enoughClearance;
	Vec3D position;
	float radius;
	Cell* c;
	for (int i = 0; i < numChars; i++) {
		charType = rand() % 3;	//Choose a random character type
		charType = 0;
		if (charType == 0) radius = RADIUS_SMALL_CHAR;
		else if (charType == 1) radius = RADIUS_MEDIUM_CHAR;
		else radius = RADIUS_BIG_CHAR;
		//Find a cell with enough clearance
		enoughClearance = false;
		while (!enoughClearance) {
			//Choose a randomCell
			c = &(_navMesh->getCell(_navMesh->getRandomCell()));
			c->p->getCenter(position);
			enoughClearance = _navMesh->checkCellClearance(c->id, position, radius);
		}
		//When we arrive here is because c has enough clearance
		float radius;
		float height = std::max(4.0f, RADIUS_BIG_CHAR * 2.0f);
		Ogre::String matName;
		if (charType == 0) {
			radius = RADIUS_SMALL_CHAR;
			matName = "scaSmallChar";
		}
		else if (charType == 1) {
			radius = RADIUS_MEDIUM_CHAR;
			matName = "scaMediumChar";
		}
		else if (charType == 2) {
			radius = RADIUS_BIG_CHAR;
			matName = "scaBigChar";
		}
		float mass = 60.0f;
		const Ogre::Vector3 scale(radius * 2.0f, height / 2.0f, radius * 2.0f);
		const Ogre::Vector3 pos(position.getX(), position.getY() + 2.0f, position.getZ());

		//Load the character and configure the rest of the stuff
		BaseCharacter* character = loadCharacter("charCylinder.mesh", radius, height, mass, pos, scale);
		character->setMaterial(matName);
		character->setWaypointMode(_waypointMode);
		if (_isScriptedPath) character->_scriptedPath = _scriptedPath;
	}
}

void ScaSceneGame::setScriptedPath() {
	if (_mapIndex != 1) return;
	
	_isScriptedPath = !_isScriptedPath;
	
	for (CharList::iterator it = _charList.begin(); it != _charList.end(); it++) {
		if (_isScriptedPath) (*it)->_scriptedPath = _scriptedPath;
		else (*it)->_scriptedPath.clear();
		(*it)->targetCellID = -1;
	}
}

#pragma region FILE MANAGEMENT

void ScaSceneGame::saveCollisionHeader() {
	if (_charList.size() == 0) return;

	std::ofstream file;
	file.open(G_RESULTS_FILE_NAME.c_str(), std::ofstream::out | std::ofstream::app);
	
	file << std::endl;
	file << ";==============================================================================================" << std::endl;
	file << "; SIMULATION RESULTS" << std::endl;
	file << ";==============================================================================================" << std::endl;
	file << "Shrink Portal Mode" << "\t" << shrinkPortalModeToString() << std::endl;
	file << "Waypoint Mode" << "\t" << waypointModeToString() << std::endl;
	file << "Using Recursivity" << "\t" << std::to_string(_guiSceneGame->isUsingRecursivity() == true) << std::endl;
	file << "Num. Characters" << "\t" << _charList.size() << std::endl;
	file << "Map ID" << "\t" << _mapIndex << std::endl;
	file << std::endl << std::endl;
	file << "Number of Collisions each 5 seconds" << std::endl;
	file << "Time" << "\t" << "Static Collisions" << "\t" << "Dynamic Collisions" << "\t" << "Total Collisions" << std::endl;

	file.close();
}

void ScaSceneGame::savePortalFlow() {

	if (_charList.size() == 0) return;

	std::ofstream file;
	file.open(G_RESULTS_FILE_NAME.c_str(), std::ofstream::out | std::ofstream::app);
	
	//Compute the total number of portal crosses
	int pCrosses = 0;
	for (PortalSensor::iterator it = _portalSensorTraversals.begin(); it != _portalSensorTraversals.end(); it++) {
		pCrosses += it->second;
	}

	file << std::endl;
	file << "Total num. of Portal Crosses" << "\t" << pCrosses << std::endl;

	//Compute the total simulation time
	float simulationTime = (_timer.getMilliseconds() - _timeSimulationStart) / 1000.0f;	//Simulation time in seconds
	file << "Simulation Time" << "\t" << simulationTime << std::endl;
	
	//Compute the portal flow
	float pFlow = float(pCrosses) / simulationTime;
	file << "Portal Flow (PortalCrosses / SimulationTime)" << "\t" << pFlow << std::endl;

	int pOverlapping = 0;
	for (PortalSensor::iterator it = _portalSensorOverlapping.begin(); it != _portalSensorOverlapping.end(); it++) {
		pOverlapping += it->second;
	}

	int numPhysicsUpdates = ((ScaPhysicsManager*)_physicsManager)->getNumUpdates();
	file << "Total num. character vs portal collisions" << "\t" << pOverlapping << std::endl;
	file << "Num. of physics updates" << "\t" << numPhysicsUpdates << std::endl;
	file << "Portal Overlapping" << "\t" << float(pOverlapping) / float(numPhysicsUpdates) << std::endl;

	file.close();

	savePortalCrosses();
}

void ScaSceneGame::savePortalCrosses() {
	if ((_charList.size() == 0) || (_portalCrossCounter.size() == 0)) return;

	std::ofstream file;
	file.open(G_RESULTS_FILE_NAME.c_str(), std::ofstream::out | std::ofstream::app);

	file << std::endl;
	file << "*****************************" << std::endl;
	file << "PORTAL CROSS TABLE" << std::endl;
	file << "*****************************" << std::endl;
	
	//Write the header of the table
	file << "Portal \\ Step" << "\t";
	for (int i = 0; i <= _crossStep; i++) {
		file << "s" << i << "\t";
	}
	file << "Mean\t" << "Mean (0's discarded)\t" << "Max Characters Crossing\t" << "Num Max Occurrences\t" << std::endl;

	//Save the table values
	for (PortalCrossCounter::iterator it = _portalCrossCounter.begin(); it != _portalCrossCounter.end(); it++) {
		file << "p" << it->first << "\t";
		int sumTotal = 0;
		int sumNonZero = 0;
		int samplesNonZero = 0;
		int samplesTotal = 0;
		std::vector<int> values;
		for (std::vector<int>::iterator itValue = it->second.begin(); itValue != it->second.end(); itValue++) {
			file << *itValue << "\t";
			values.push_back(*itValue);
			sumTotal += *itValue;
			samplesTotal++;
			if (*itValue > 0) {
				sumNonZero += *itValue;
				samplesNonZero++;
			}
		}
		float mean = (samplesTotal)? float(sumTotal) / float(samplesTotal) : 0.0f;
		float meanNonZero = (samplesNonZero)? float(sumNonZero) / float(samplesNonZero) : 0.0f;
		file << mean << "\t" << meanNonZero << "\t";

		std::sort(values.begin(), values.end(), sortIntFromHighToLow);
		size_t i = 0;
		while ((i < values.size()) && (values[i] == values[0])) i++;
		file << values[0] << "\t" << i << "\t" << std::endl;
	}

	file.close();
}

std::string ScaSceneGame::shrinkPortalModeToString() {
	if (_shrinkPortalMode == ClearanceSolver::SP_EXACT) return "ExACT";
	else if (_shrinkPortalMode == ClearanceSolver::SP_SIMPLE) return "Simple";
	return "UNDEFINED";
}

std::string ScaSceneGame::waypointModeToString() {
	if (_waypointMode == BaseCharacter::PORTAL_SEEK_PROJECTION) return "Dynamic Assignation";
	else if (_waypointMode == BaseCharacter::PORTAL_SEEK_MID_POINT) return "Midpoint";
	return "UNDEFINED";
}

void ScaSceneGame::saveCollisionData() {
	if (_charList.size() == 0) return;

	std::ofstream file;
	file.open(G_RESULTS_FILE_NAME.c_str(), std::ofstream::out | std::ofstream::app);

	int diffStaticCollisions = _guiSceneGame->getNumStaticCollisions() - _lastNumStaticCollisions;
	int diffDynamicCollisions = _guiSceneGame->getNumDynamicCollisions() - _lastNumDynamicCollisions;
	int diffTotalCollisions = diffStaticCollisions + diffDynamicCollisions;

	_lastNumStaticCollisions = _guiSceneGame->getNumStaticCollisions();
	_lastNumDynamicCollisions = _guiSceneGame->getNumDynamicCollisions();

	file << std::to_string(_elapsedSeconds) << "\t";
	file << std::to_string(diffStaticCollisions) << "\t";
	file << std::to_string(diffDynamicCollisions) << "\t";
	file << std::to_string(diffTotalCollisions) << std::endl;

	file.close();
}

#pragma endregion