#include <iostream>
#include <iomanip>  
#include <fstream>

#include "ScaGUISceneGame.h"
#include "ScaGlobalVars.h"

const static int MENU_WIDTH = 292;
const static int SLIDER_BOX_WIDTH = 64;
const static OgreBites::TrayLocation MENU_POSITION = OgreBites::TL_TOPLEFT;
const static float MIN_SAMPLE_FREQUENCY = 0.1f;
const static float MAX_SAMPLE_FREQUENCY = 1.0f;
const static int NUM_CHARS_ADD = 25;	//Number of characters added to the simulation per button click

using namespace Rogue;

/*********************************************************************************************/
/* CREATION AND DESTRUCTION */
/*********************************************************************************************/
ScaGUISceneGame::ScaGUISceneGame(OgreBites::SdkTrayManager* tMgr) 
	: GUIBase(tMgr)
{
	_labelSceneConfiguration = _trayMgr->createLabel(MENU_POSITION, "labelSceneConfiguration", "Scene Configuration", MENU_WIDTH);
	
	Ogre::StringVector params;
	params.push_back("Characters");
	params.push_back("Static Col.");
	params.push_back("Dynamic Col.");
	params.push_back("Total Col.");
	_panelNumCharacters = _trayMgr->createParamsPanel(MENU_POSITION, "panelNumCharacters", MENU_WIDTH, params);
	
	/*_buttonAddCharacters = _trayMgr->createButton(MENU_POSITION, "buttonAddCharacters", "Add Characters", MENU_WIDTH);

	_buttonResetCollisions = _trayMgr->createButton(MENU_POSITION, "buttonResetCollisions", "Reset Collisions", MENU_WIDTH);

	_buttonResetSimulation = _trayMgr->createButton(MENU_POSITION, "buttonResetSimulation", "Reset Simulation", MENU_WIDTH);

	_buttonNextScene = _trayMgr->createButton(MENU_POSITION, "buttonNextScene", "Next Scene", MENU_WIDTH);*/

	
	//----------------------------------------------------------------------------

	_labelSceneDebug = _trayMgr->createLabel(MENU_POSITION, "labelSceneDebug", "Scene Debug", MENU_WIDTH);

	_menuShrinkPortalMode = _trayMgr->createLongSelectMenu(MENU_POSITION, "menuShrinkPortalMode", "Shrink Portal Mode:", 136, 10);
	_menuShrinkPortalMode->addItem("SIMPLE");
	_menuShrinkPortalMode->addItem("ExACT");
	_menuShrinkPortalMode->selectItem(1, false);

	_menuWaypointMode = _trayMgr->createLongSelectMenu(MENU_POSITION, "menuWaypointkMode", "Waypoint Mode:", 136, 10);
	_menuWaypointMode->addItem("PROJECTION");
	_menuWaypointMode->addItem("MIDPOINT");
	_menuShrinkPortalMode->selectItem(0, false);

	_checkUseRecursivity = _trayMgr->createCheckBox(MENU_POSITION, "checkRecursivity", "Use Recursivity:", MENU_WIDTH);
	
	_checkShowDebug = _trayMgr->createCheckBox(MENU_POSITION, "checkDebug", "Show Debug Information:", MENU_WIDTH);
	
	_checkShowCharacter = _trayMgr->createCheckBox(MENU_POSITION, "checkShowCharacter", "Show Character:", MENU_WIDTH);
	_widgetsDebug.push_back(_checkShowCharacter);
		
	_checkShowAttractorPoint = _trayMgr->createCheckBox(MENU_POSITION, "checkShowAttractorPoint", "Show Attractor:", MENU_WIDTH);
	_widgetsDebug.push_back(_checkShowAttractorPoint);
			
	_checkShowTrajectory = _trayMgr->createCheckBox(MENU_POSITION, "checkShowTrajectory", "Show Trajectory:", MENU_WIDTH);
	_widgetsDebug.push_back(_checkShowTrajectory);

	_sliderSampleFrequency = _trayMgr->createThickSlider(MENU_POSITION, "sliderSampleFrequency", "Sample Frequency (s):", MENU_WIDTH, SLIDER_BOX_WIDTH, MIN_SAMPLE_FREQUENCY, MAX_SAMPLE_FREQUENCY, 10);
	_widgetsDebug.push_back(_sliderSampleFrequency);

	reset();
}

ScaGUISceneGame::~ScaGUISceneGame() {
	
}

void ScaGUISceneGame::reset() {
	_currentNumCharacters = 0;
	_panelNumCharacters->setParamValue(PARAM_NUM_CHARS, std::to_string(_currentNumCharacters));
	_triggeredButtonAddCharacters = false;
	_triggeredButtonResetCollisions = false;
	_triggeredButtonResetSimulation = false;
	_triggeredButtonNextScene = false;
	_menuShrinkPortalMode->selectItem(1, false);
	_menuWaypointMode->selectItem(0, false);
	_checkUseRecursivity->setChecked(true, false);
	_checkShowDebug->setChecked(true, false);
	_checkShowCharacter->setChecked(true, false);
	_checkShowAttractorPoint->setChecked(true, false);
	_checkShowTrajectory->setChecked(true, false);
	_sliderSampleFrequency->setValue(MIN_SAMPLE_FREQUENCY, false);
	resetCollisions();
}

void ScaGUISceneGame::resetCollisions() {
	_timerCollisions.reset();
	_timeLastUpdate = 0;
	_numStaticCollisions = 0;
	_numDynamicCollisions = 0;
	_staticCPS = 0.0f;
	_dynamicCPS = 0.0f;
	_totalCPS = 0.0f;
}

void ScaGUISceneGame::setWidgetsVisible(WidgetList& widgets, bool visible) {
	for (WidgetList::iterator it = widgets.begin(); it != widgets.end(); it++) {
		if (visible) {
			_trayMgr->moveWidgetToTray(*it, MENU_POSITION);
			(*it)->show();
		}
		else {
			_trayMgr->removeWidgetFromTray(*it);
			(*it)->hide();
		}
	}
}

void ScaGUISceneGame::displayNumCollisions() {
	_panelNumCharacters->setParamValue(PARAM_STATIC_COLLISIONS, collisionsToString(_numStaticCollisions, _staticCPS));
	_panelNumCharacters->setParamValue(PARAM_DYNAMIC_COLLISIONS, collisionsToString(_numDynamicCollisions, _dynamicCPS));
	_panelNumCharacters->setParamValue(PARAM_TOTAL_COLLISIONS, collisionsToString(_numStaticCollisions + _numDynamicCollisions, _totalCPS));
}

String ScaGUISceneGame::collisionsToString(int collisions, float cps) {
	std::stringstream ss;
	String result = std::to_string(collisions);
	ss << std::fixed << std::setprecision(3) << cps;
	result += " (" + ss.str() + " cps)";
	return result;
}

void ScaGUISceneGame::updateStaticCollisions() {
	_numStaticCollisions++;
}

void ScaGUISceneGame::updateDynamicCollisions() {
	_numDynamicCollisions++;
}

/*********************************************************************************************/
/* GUI STATE */
/*********************************************************************************************/
int ScaGUISceneGame::getShrinkPortalMode() {
	return _menuShrinkPortalMode->getSelectionIndex();
}

void ScaGUISceneGame::setShrinkPortalMode(int mode) {
	_menuShrinkPortalMode->selectItem(mode);
}

int ScaGUISceneGame::getWaypointMode() {
	return _menuWaypointMode->getSelectionIndex();
}

void ScaGUISceneGame::setWaypointMode(int mode) {
	_menuWaypointMode->selectItem(mode);
}

bool ScaGUISceneGame::isUsingRecursivity() {
	return _checkUseRecursivity->isChecked();
}

void ScaGUISceneGame::useRecursivity(bool r) {
	_checkUseRecursivity->setChecked(r);
}

bool ScaGUISceneGame::isShowingDebug() {
	return _checkShowDebug->isChecked();
}

void ScaGUISceneGame::showDebug(bool s) {
	_checkShowDebug->setChecked(s);
}

bool ScaGUISceneGame::isShowingCharacter() {
	return _checkShowCharacter->isChecked();
}

void ScaGUISceneGame::showCharacter(bool s) {
	_checkShowCharacter->setChecked(s);
}

bool ScaGUISceneGame::isShowingAttractorPoint() {
	return _checkShowAttractorPoint->isChecked();
}

void ScaGUISceneGame::showAttractorPoint(bool s) {
	_checkShowAttractorPoint->setChecked(s);
}

bool ScaGUISceneGame::isShowingTrajectory() {
	return _checkShowTrajectory->isChecked();
}

void ScaGUISceneGame::showTrajectory(bool s) {
	_checkShowTrajectory->setChecked(s);
}

float ScaGUISceneGame::getSampleFrequency() {
	return _sliderSampleFrequency->getValue();
}

int ScaGUISceneGame::getNumStaticCollisions() {
	return _numStaticCollisions;
}

int ScaGUISceneGame::getNumDynamicCollisions() {
	return _numDynamicCollisions;
}

bool ScaGUISceneGame::isTriggeredButtonAddCharacters() {
	if (_triggeredButtonAddCharacters) {
		//Avoid having multiple checks of the button state
		_triggeredButtonAddCharacters = false;
		return true;
	}
	return false;
}

bool ScaGUISceneGame::isTriggeredButtonResetCollisions() {
	if (_triggeredButtonResetCollisions) {
		_triggeredButtonResetCollisions = false;
		return true;
	}
	return false;
}

bool ScaGUISceneGame::isTriggeredButtonResetSimulation() {
	if (_triggeredButtonResetSimulation) {
		//Avoid having multiple checks of the button state
		_triggeredButtonResetSimulation = false;
		return true;
	}
	return false;
}

bool ScaGUISceneGame::isTriggeredButtonNextScene() {
	if (_triggeredButtonNextScene) {
		//Avoid having multiple checks of the button state
		_triggeredButtonNextScene = false;
		return true;
	}
	return false;
}

int ScaGUISceneGame::getNumCharacters() {
	return NUM_CHARS_ADD;
}

/*********************************************************************************************/
/* SDKTrayListener */
/*********************************************************************************************/
void ScaGUISceneGame::update() {
	unsigned long now = _timerCollisions.getMilliseconds();
	unsigned long elapsedTime = now - _timeLastUpdate;
	if (elapsedTime >= 1000) {
		float seconds = float(now) / 1000.0f;
		_staticCPS = float(_numStaticCollisions) / seconds;
		_dynamicCPS = float(_numDynamicCollisions) / seconds;
		_totalCPS = float(_numStaticCollisions + _numDynamicCollisions) / seconds;
		_timeLastUpdate = now;
	}
	displayNumCollisions();
}

void ScaGUISceneGame::buttonHit(OgreBites::Button* button) {
	_triggeredButtonAddCharacters =		(button == _buttonAddCharacters);
	_triggeredButtonResetCollisions =	(button == _buttonResetCollisions);
	_triggeredButtonResetSimulation =	(button == _buttonResetSimulation);
	_triggeredButtonNextScene =			(button == _buttonNextScene);
}

void ScaGUISceneGame::checkBoxToggled(OgreBites::CheckBox* box) {
	if (box == _checkShowDebug) {
		setWidgetsVisible(_widgetsDebug, box->isChecked());
	}
}

void ScaGUISceneGame::addCharacters() {
	_currentNumCharacters += NUM_CHARS_ADD;
	_panelNumCharacters->setParamValue(0, std::to_string(_currentNumCharacters));
	resetCollisions();
}

void ScaGUISceneGame::resetSimulation() {
	_currentNumCharacters = 0;
	_panelNumCharacters->setParamValue(0, std::to_string(_currentNumCharacters));
	resetCollisions();
}