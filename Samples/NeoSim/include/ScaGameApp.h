#ifndef __RogueScaGameApp_h_
#define __RogueScaGameApp_h_ 

#include "RogueGameApp.h"
#include "ScaSceneGame.h"
#include "ScaPhysicsManager.h"

class ScaGameApp : public Rogue::GameApp {

	public:
		ScaGameApp();
		~ScaGameApp();

	private:

		void registerPhysicsManager();
		void registerScenes();
};

#endif

