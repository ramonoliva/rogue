#ifndef __RogueScaSceneGame_h_
#define __RogueScaSceneGame_h_ 

#include "RogueSceneGame.h"
#include "RogueDebuggers.h"

#include "ScaPhysicsManager.h"
#include "ScaGUISceneGame.h"

#include <unordered_set>

class ScaSceneGame : public Rogue::SceneGame {

	public:

		ScaSceneGame();
		~ScaSceneGame();

		virtual void createScene();
		virtual void destroyScene();

		virtual void portalCrossed(int oldCell, int newCell);
		virtual void portalSensorTouched(Rogue::GhostObject* sensor);
		virtual void portalSensorOverlapping(Rogue::GhostObject* sensor);

	protected:

		typedef std::map<Rogue::GhostObject*, int> PortalSensor;
		typedef std::vector<Rogue::DebuggerCharacter*> DebuggerCharacterVector;
		typedef std::map<int, std::vector<int>> PortalCrossCounter;
		
		Rogue::BaseCharacter* _selectedChar;
		ScaGUISceneGame* _guiSceneGame;
		int _shrinkPortalMode;
		int _waypointMode;
		bool _debugCleared;
		size_t _mapIndex;							//Indicates the map to load. 
		bool _isScriptedPath;
		std::map<int, int> _scriptedPath;
		DebuggerCharacterVector _debuggerCharacters;
		PortalSensor _portalSensorTraversals;
		PortalSensor _portalSensorOverlapping;
		PortalCrossCounter _portalCrossCounter;
		
		int _lastNumStaticCollisions;
		int _lastNumDynamicCollisions;
		unsigned long _timeSaveCollisions;
		unsigned long _timeUpdateCrosses;
		unsigned long _timeSimulationStart;
		int _elapsedSeconds;
		int _crossStep;

		bool _useTables;
		bool _useCriticalRadius;

		virtual void loadNavMesh(const Rogue::String& fileName);
		virtual void addPortalSensor(int portalID);
		virtual Rogue::BaseCharacter* loadCharacter(const Ogre::String& meshFile, float radius, float height, float mass, const Ogre::Vector3& pos = Ogre::Vector3::ZERO, const Ogre::Vector3& scale = Ogre::Vector3(1.0f));

		virtual void resetSimulation();
		virtual void resetResultsData();

		virtual void loadNextMap();

		void update();
		void updateInput();
		void updateDebugger();
		void updatePortalCrosses();
		virtual void processMouseEvents();
		void processKeyboardEvents();
		void processGUIEvents();

		void scaTest1();
		void scaTest2(std::vector<int>& portals);
		void scaTest3();
		
		void setShrinkPortalMode(int mode);
		void setWaypointMode(int mode);
		void badShrinkedPortalDetection();

		void initCharacters(int numChars);
		void setScriptedPath();

		void showCharactersInfo(bool showCharacter, bool showAttractor, bool showTrajectory);

		//FILE MANAGEMENT
		void saveCollisionHeader();
		void saveCollisionData();
		void savePortalFlow();
		void savePortalCrosses();
		virtual std::string shrinkPortalModeToString();
		virtual std::string waypointModeToString();
};

#endif