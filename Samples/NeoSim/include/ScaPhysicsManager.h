#ifndef __RogueScaPhysicsManager_h_
#define __RogueScaPhysicsManager_h_

#include "RoguePhysicsManager.h"

class ScaPhysicsManager : public Rogue::PhysicsManager {

	public:

		ScaPhysicsManager() : PhysicsManager() {
			_numUpdates = 0;
		}

		virtual void resetNumUpdates();
		virtual int getNumUpdates();

	protected:

		enum CollisionType {
			CT_NULL,	//Undefined collision
			CT_DYNAMIC,	//CHAR VS CHAR
			CT_STATIC,	//CHAR VS WALL
			CT_PORTAL,	//CHAR VS PORTAL
		};

		int _numUpdates;

		virtual void updatePhysics();
		virtual void onCollisionPairStay(Rogue::CollisionPair& pair);
		virtual void onCollisionPairAdded(Rogue::CollisionPair& pair);		//Actions to do when the collision pair is created for the first time

};

#endif