#ifndef __RogueScaGlobalVars_h_
#define __RogueScaGlobalVars_h_

#include <string>

extern bool G_SHOW_CHARACTER;
extern bool G_SHOW_WAY_POINT;
extern bool G_SHOW_TRAJECTORY;

extern bool G_SHOW_CHARACTERS_SMALL;
extern bool G_SHOW_CHARACTERS_MEDIUM;
extern bool G_SHOW_CHARACTERS_BIG;

extern std::string G_RESULTS_FILE_NAME;

#endif