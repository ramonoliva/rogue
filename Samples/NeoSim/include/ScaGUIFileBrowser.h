#ifndef __ScaGUIFileBrowser_h_
#define __ScaGUIFileBrowser_h_

#include <windows.h>
#include <stdlib.h>     /* wcstombs, wchar_t(C) */
#include <shobjidl.h> 

namespace Rogue {

	HRESULT BasicFileOpen() {
		HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | 
        COINIT_DISABLE_OLE1DDE);
		if (SUCCEEDED(hr)) {
			IFileOpenDialog *pFileOpen;
			// Create the FileOpenDialog object.
			hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL, IID_IFileOpenDialog, reinterpret_cast<void**>(&pFileOpen));
			if (SUCCEEDED(hr)) {
				// Show the Open dialog box.
				hr = pFileOpen->Show(NULL);
				// Get the file name from the dialog box.
				if (SUCCEEDED(hr)) {
					IShellItem *pItem;
					hr = pFileOpen->GetResult(&pItem);
					if (SUCCEEDED(hr)) {
						wchar_t* pszFilePath;
						hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);
						char buffer[32];
						int ret;
						ret = wcstombs ( buffer, pszFilePath, sizeof(buffer));
						if (ret==32) buffer[31]='\0';
  
						// Display the file name to the user.
						if (SUCCEEDED(hr)) {
							MessageBox(NULL, buffer, "File Path", MB_OK);
							CoTaskMemFree(pszFilePath);
						}
						pItem->Release();
					}
				}
				pFileOpen->Release();
			}
			CoUninitialize();
		}
		return hr;
	}

};

#endif