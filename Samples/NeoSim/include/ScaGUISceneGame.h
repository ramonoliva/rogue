#ifndef __RogueScaGUISceneGame_h_
#define __RogueScaGUISceneGame_h_

#include "RogueGUIBase.h"

class ScaGUISceneGame : public Rogue::GUIBase {

	public:

		ScaGUISceneGame(OgreBites::SdkTrayManager* tMgr);
		~ScaGUISceneGame();
	
		void reset();
		void resetCollisions();

		int getShrinkPortalMode();
		void setShrinkPortalMode(int mode);

		int getWaypointMode();
		void setWaypointMode(int mode);

		bool isUsingRecursivity();
		void useRecursivity(bool r);

		bool isShowingCharacter();
		void showCharacter(bool s);

		bool isShowingAttractorPoint();
		void showAttractorPoint(bool s);

		bool isShowingTrajectory();
		void showTrajectory(bool s);
		
		bool isShowingDebug();
		void showDebug(bool s);
		
		int getNumStaticCollisions();
		int getNumDynamicCollisions();

		float getSampleFrequency();
		bool isTriggeredButtonAddCharacters();
		bool isTriggeredButtonResetCollisions();
		bool isTriggeredButtonResetSimulation();
		bool isTriggeredButtonNextScene();
		int getNumCharacters();

		void addCharacters();
		void resetSimulation();

		void updateStaticCollisions();
		void updateDynamicCollisions();

		void update();
		
	private:

		enum ParamsEnum {
			PARAM_NUM_CHARS, 
			PARAM_STATIC_COLLISIONS,
			PARAM_DYNAMIC_COLLISIONS, 
			PARAM_TOTAL_COLLISIONS
		};

		OgreBites::Label* _labelSceneConfiguration;
		OgreBites::ParamsPanel* _panelNumCharacters;
		OgreBites::Button* _buttonAddCharacters;
		OgreBites::Button* _buttonResetCollisions;
		OgreBites::Button* _buttonResetSimulation;
		OgreBites::Button* _buttonNextScene;
		
		OgreBites::Label* _labelSceneDebug;
		OgreBites::CheckBox* _checkUseRecursivity;
		OgreBites::CheckBox* _checkShowDebug;
		OgreBites::SelectMenu* _menuShrinkPortalMode;
		OgreBites::SelectMenu* _menuWaypointMode;
		OgreBites::CheckBox* _checkShowCharacter;
		OgreBites::CheckBox* _checkShowAttractorPoint;
		OgreBites::CheckBox* _checkShowTrajectory;
		OgreBites::Slider* _sliderSampleFrequency;

		Rogue::Timer _timerCollisions;	//Timer used to obtain the collisions per second
		unsigned long _timeLastUpdate;
		float _staticCPS;
		float _dynamicCPS;
		float _totalCPS;

		WidgetList _widgetsDebug;

		bool _triggeredButtonAddCharacters;
		bool _triggeredButtonResetCollisions;
		bool _triggeredButtonResetSimulation;
		bool _triggeredButtonNextScene;

		int _currentNumCharacters;
		int _numStaticCollisions;
		int _numDynamicCollisions;
				
		void setWidgetsVisible(WidgetList& widgets, bool visible);
		void displayNumCollisions();
		Rogue::String collisionsToString(int collisions, float cps);
		
		//SDKTrayListener
		void buttonHit(OgreBites::Button* button);
		void checkBoxToggled(OgreBites::CheckBox* box);
};

#endif