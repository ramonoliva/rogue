#pragma once

#include "RogueAutonomousCharacter.h"

class ScaAutonomousCharacter : public Rogue::AutonomousCharacter {

	public:

		ScaAutonomousCharacter(Rogue::BaseCharConfigurator& baseCFG, Rogue::RigidBody* rigidBody, Rogue::OpenSteerCharConfigurator& openSteerCFG)
		: Rogue::AutonomousCharacter(baseCFG, rigidBody, openSteerCFG) {

		}

		virtual void setCurrentCellID(int cellID);

};