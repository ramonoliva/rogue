///////////////////////////////////////////////////////////////////////////////////////////////////
// MATERIAL DEFINITION
///////////////////////////////////////////////////////////////////////////////////////////////////
material matClearGBuffer
{
	technique
	{
		pass
		{
			depth_write off
						
			vertex_program_ref StdQuad_vp
			{
			}
			
			fragment_program_ref ClearGBuffer_fp
			{
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// MATERIAL DEFINITION
///////////////////////////////////////////////////////////////////////////////////////////////////
material matGenericShaded
{
	technique
	{
		pass
		{
			vertex_program_ref GBuffer_vp
			{
			}
			
			fragment_program_ref GBuffer_fp
			{
				param_named colorTex int 0
			}
			
			texture_unit
			{
				texture blank.jpg
				tex_address_mode clamp
				filtering none
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MATERIAL DEFINITION
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
material matComposeLighting
{
	technique
	{
		pass
		{
			scene_blend add
						
			vertex_program_ref StdQuad_vp
			{
			}			
			
			fragment_program_ref lighting_fp
			{
				param_named colorTex	int 0
				param_named normalTex	int 1
				param_named vSpaceTex	int 2
				param_named shadowMask	int 3
			}

			texture_unit
			{
				//Textura que cont� el color
				texture sceneColorTex
				tex_address_mode clamp
				filtering none
			}
			
			texture_unit
			{
				//Textura que cont� les normals i la profunditat
				texture sceneNormalDepthTex
				tex_address_mode clamp
				filtering none
			}
			
			texture_unit
			{
				//Textura que cont� la posici� en ViewSpace de cada fragment
				texture sceneViewSpacePosTex
				tex_address_mode clamp
				filtering none
			}
			
			texture_unit
			{
				//Textura que indica quins fragments estan a l'ombra (0) i quins il�luminats (1)
				texture sceneShadowMask
				tex_address_mode clamp
				filtering none
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MATERIAL DEFINITION
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
material matComposeOutlines
{
	technique
	{
		pass
		{
			vertex_program_ref StdQuad_vp
			{
			}	
			
			fragment_program_ref Outlines_fp
			{
			}
			
			texture_unit
			{
				//Textura que cont� el mapa de profunditat de l'escena
				texture sceneNormalDepthTex
				tex_address_mode border
				tex_border_colour 0.0 0.0 0.0 0.0
				filtering none
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MATERIAL DEFINITION
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
material matComposeSSAO
{
	technique
	{
		pass
		{
			vertex_program_ref StdQuad_vp
			{
			}
			
			fragment_program_ref HemisphereSSAO_fp
			{
				param_named normalDepthMap 	int 0
				param_named viewSpacePosMap int 1
				param_named rotationKernel	int 2
			}
			
			texture_unit
			{
				//Texture that contanis the normal (rgb) and normalized depth (a) per pixel
				texture sceneNormalDepthTex
				tex_address_mode clamp
				filtering none
			}
			
			texture_unit
			{
				//Texture that contains the ViewSpace position per pixel
				texture sceneViewSpacePosTex
				tex_address_mode clamp
				filtering none
			}
			
			texture_unit
			{
				//Rotation kernel texture
				texture SSAORotationKernel.png
				tex_address_mode wrap
				filtering none
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MATERIAL COMPOSELIGHTSCATTERING
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
material matComposeLightScattering
{
	technique
	{
		pass
		{
			vertex_program_ref StdQuad_vp
			{
			}
			
			fragment_program_ref LightScattering_FP
			{
				param_named shadowMap		int 0
				param_named viewSpacePosMap	int 1
            }
			
			texture_unit
			{
				//texture shadowMap
				texture shadowMap
				tex_address_mode clamp
				filtering none
			}
			
			texture_unit
			{
				//Textura que cont� la posici� en ViewSpace de cada fragment
				texture sceneViewSpacePosTex
				tex_address_mode clamp
				filtering none
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MATERIAL COMPOSELIGHTSCATTERING
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//material matComposeLightScattering
//{
//	technique
//	{
//		pass
//		{
//			vertex_program_ref StdQuad_vp
//			{
//			}
			
//			fragment_program_ref LightScattering_FP
//			{
//				param_named diffuseMap	int 0
//				param_named normalMap 	int 1
//			}
			
//			texture_unit
//			{
//				texture sceneColorTex
//				tex_address_mode clamp
//				filtering none
//			}
			
//			texture_unit
//			{
//				texture sceneNormalDepthTex
//				tex_address_mode clamp
//				filtering none
//			}
//		}
//	}
//}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MATERIAL DEFINITION
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
material matComposeFinalScene
{
	technique
	{
		pass
		{
			vertex_program_ref StdQuad_vp
			{
			}
			
			fragment_program_ref FinalComposition_fp
			{
				param_named sceneLitTex 		int 0
				param_named sceneOutlinesTex	int 1
				param_named sceneSSAO			int 2
			}
			
			texture_unit
			{
				//Textura que cont� la il�luminaci� de l'escena
				texture sceneLitTex
				tex_address_mode clamp
				filtering none
			}
			
			texture_unit
			{
				//Textura que cont� les outlines de l'escena
				texture sceneOutlinesTex
				tex_address_mode clamp
				filtering none
			}
			
			texture_unit
			{
				//Textura que cont� el SSAO
				texture sceneSSAO
				tex_address_mode clamp
				filtering none
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// MATERIAL DEFINITION: GAUSSIAN FILTER
///////////////////////////////////////////////////////////////////////////////////////////////////
material matGaussianFilter
{
    technique
    {
        pass 
        {
			vertex_program_ref StdQuad_vp
			{
			}
			
            fragment_program_ref gaussianFilter_fp
            {
				param_named theTex int 0
            }
			
			texture_unit
			{
				texture dummy
				tex_address_mode clamp
				filtering none
			}
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// MATERIAL DEFINITION: BOX FILTER
///////////////////////////////////////////////////////////////////////////////////////////////////
material matBoxFilter
{
    technique
    {
        pass 
        {
			vertex_program_ref StdQuad_vp
			{
			}
			
            fragment_program_ref boxFilter_fp
            {
				//param_named theTex int 0
            }
			
			texture_unit
			{
				texture dummy
				tex_address_mode clamp
				filtering none
			}
        }
    }
}