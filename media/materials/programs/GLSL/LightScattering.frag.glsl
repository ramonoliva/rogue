#version 120

/////////////////////////////////////////////////////
// IMPORTED AUXILIARY FUNCTIONS: utils.frag.glsl
///////////////////////////////////////////////////// 
bool realEqual(float a, float b, float epsilon);

/////////////////////////////////////////////////////
// AUXILIARY FUNCTIONS
/////////////////////////////////////////////////////
bool isObstacle(vec2 texCoords, sampler2D normalMap) {
	//Determines if the pixel texCoords corresponds to an obstacle fragment, i.e., its normal is not 0
	vec3 normal = texture2D(normalMap, texCoords).xyz;
	float l2 = dot(normal, normal);	//Squared normal length
	return (!realEqual(l2, 0.0, 0.001));
}

/////////////////////////////////////////////////////
// FRAGMENT SHADER ENTRY POINT
/////////////////////////////////////////////////////

//uniform variables
uniform vec4 lightPosSS;		//Light Position in Screen Space scaled and biased in the range [-1, 1]
uniform	int numSamples;			//The number of sample positions taken along the ray. 
uniform float exposure;			//Controls the overall intensity of the post-process. 
uniform float weight;			//Controls the intensity of each sample.
uniform float decay;			//Dissipates each sample's contribution as the ray progresses away from the light source. 
uniform float density;			//Controls the separation between samples. 
uniform sampler2D diffuseMap;	//The diffuseMap of the Scene
uniform sampler2D normalMap;	//The normalMap of the Scene

//varying variables
varying vec2 uv;

//EQUATION
//exposure * SUM[i = 0..n](decay^i * weight * sample(i))

void main() {
	vec2 deltaUV = (uv - lightPosSS.xy);		//Ray from the light to the fragment, in Screen Space
	deltaUV /= (float(numSamples * density));  	//Divide by number of samples and scale by density control factor.  
	float illuminationDecay = 1.0f;  			//Initialize illumination decay factor.
		
	//Evaluate summation from Equation 3 numSamples iterations.  
	vec2 sampleUV;
	vec3 sampleColor;
	vec3 color = vec3(0.0);
	for (int i = 0; i < numSamples; i++) {  
		sampleUV = uv - (i * deltaUV);			//Step sample location along ray.  
		if (!isObstacle(sampleUV, normalMap)) {
			//If the sample is thin air, retrieve its color and 
			//add to to the current color
			sampleColor = texture2D(diffuseMap, sampleUV).xyz;
			sampleColor *= illuminationDecay * weight;  	//Apply sample attenuation scale/decay factors.
			color += sampleColor;
		}
		illuminationDecay *= decay;//Update exponential decay factor.  
		if (realEqual(decay, 0.0, 0.0001)) i = numSamples;	//Stop if decay factor is too close to 0
	}
		
	//Output final color with a further scale control factor.  
	gl_FragColor = vec4(color * exposure, 1.0);
}
