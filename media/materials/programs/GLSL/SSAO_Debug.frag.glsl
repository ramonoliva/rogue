#version 120

//uniform attributes
//GBuffer Data
uniform sampler2D normalDepthMap;	//Normal (rgb) and normalized Depth (a) per pixel
uniform sampler2D viewSpacePosMap;	//View Space Position per pixel
//SSAO Configuration
uniform int resultType;
uniform sampler2D rotationKernel;
uniform vec3 sampleKernel[32]; 
uniform int sampleKernelSize;
uniform float rotationKernelScaleX;	//rotationKernelScaleX = (viewport.w / rotationKernel.w); 
uniform float rotationKernelScaleY;	//rotationKernelScaleY = (viewport.h / rotationKernel.h);
uniform float radius; 				//Radius of influence of the AO
uniform mat4 projectionMatrix;

//varying parameters
varying vec2 uv;

//Normal-Oriented Hemisphere SSAO
void main() {
	vec2 rotationKernelScale = vec2(rotationKernelScaleX, rotationKernelScaleY);
	vec3 originVS = texture2D(viewSpacePosMap, uv).xyz;	//Fragment's position in View Space
	vec3 normal = texture2D(normalDepthMap, uv).xyz;	//Fragment's normal (xyz) and normalized depth (w)
	
	//Construct a new orthonormal basis matrix to reorient the sample kernel along the fragments's normal. 
	vec3 rVec = texture2D(rotationKernel, uv * rotationKernelScale).xyz * 2.0 - 1.0;	//Recover the rotation vector from the texture + scale bias
		
	vec3 proj = normal * dot(rVec, normal);				//The projection of rVec over the normal. Notice that this vector is parallel to normal
	vec3 tangent = normalize(rVec - proj);				//Obtain a perpendicular vector to normal using rVec by substracting its projection (the parallel component of rVec to normal)
	vec3 bitangent = normalize(cross(normal, tangent));	//As normal and tangent are perpendicular, the result of their cross product is perpendicular to both of them
	
	mat3 rotationMatrix = mat3(tangent, bitangent, normal);
		
	//Compute the occlusion factor
	int numOccluders = 0;	//Number of samples occluding the current fragment
	vec3 sampleDirVS;		//Sample direction in View Space
	vec3 samplePosVS;		//Sample position in View Space
	vec4 samplePosSS = vec4(0.0, 0.0, 0.0, 0.0);		//Sample position in Screen Space
	vec3 texPosVS;
	for (int i = 0; i < sampleKernelSize; i++) {
		//Get the sample position
		sampleDirVS = rotationMatrix * sampleKernel[i];
		samplePosVS = originVS + (sampleDirVS * radius);	//The position of the sample in View Space
		
		//Project sample position to Screen Space
		samplePosSS = projectionMatrix * vec4(samplePosVS, 1.0);
		samplePosSS.xy /= samplePosSS.w;				//Perspective Division
		samplePosSS.xy = (samplePosSS.xy * 0.5) + 0.5;	//Bias from the range [-1, 1] to [0, 1]
				
		//Get the depth value in the sample position and compare
		texPosVS = texture2D(viewSpacePosMap, samplePosSS.xy).xyz;
		
		//float rangeCheck= abs(origin.z - sampleDepth) < uRadius ? 1.0 : 0.0;
		//occlusion += (sampleDepth <= sample.z ? 1.0 : 0.0) * rangeCheck;
				
		if (abs(originVS.z - texPosVS.z) < radius) {
			//if (texPosVS.z <= samplePosVS.z) {
			if (texPosVS.z >= samplePosVS.z) {	//Notice that we are looking towards the -Z
				//The ray from originVS to samplePosVS intersects with the geometry of the scene. 
				//Therefore, we have found an occluder of the current fragment. 
				numOccluders ++;
			}
		}
	}
	
	if (resultType == 0) {
		//Compute the occlusion factor
		float oFactor = 1.0 - (float(numOccluders) / float(sampleKernelSize));
		gl_FragColor = vec4(oFactor, oFactor, oFactor, 1.0);
	}
	else if (resultType == 1) {
		//Show the rotation vector choosen
		gl_FragColor = vec4(texture2D(rotationKernel, uv * rotationKernelScale).xyz, 1.0);
	}
	else if (resultType == 2) {
		//Show the tangent vector
		gl_FragColor = vec4((tangent + 1.0) / 2.0, 1.0);
	}
	else if (resultType == 3) {
		//Show the bitangent vector
		gl_FragColor = vec4((bitangent + 1.0) / 2.0, 1.0);
	}
	else if (resultType == 4) {
		//Show the origin pos
		gl_FragColor = vec4((normalize(originVS) + 1.0) / 2.0, 1.0);
	}
	else if (resultType == 5) {
		//Show compute tex coords
		gl_FragColor = vec4(samplePosSS.xy, 0.0, 1.0);
	}
	else if (resultType == 6) {
		//Show sample kernel
		gl_FragColor = vec4((sampleKernel[0] + 1.0) / 2.0, 1.0);
	}
	else if (resultType == 7) {
		//Show uv coords
		gl_FragColor = vec4(uv, 0.0, 1.0);
	}
}