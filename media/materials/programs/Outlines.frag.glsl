#version 120

/////////////////////////////////////////////////////////////////////////////
// FRAGMENT SHADER: DEFINITION
/////////////////////////////////////////////////////////////////////////////
const mat3 weight = mat3
(
	1.0, 2.0, 1.0,
	2.0, 0.0, 2.0,
	1.0, 2.0, 1.0
);

//uniform variables
uniform sampler2D depthMapTex;		
//uniform sampler2D viewSpacePosTex;
uniform float pixelSizeX;
uniform float pixelSizeY;
uniform float zNear;
uniform float zFar;

//varying variables
varying vec2 uv;

void main() {
    //Check the depth gradient
	float meanDepth = 0.0;
	float nDepth;	//Profunditat del ve�
	float w;
	float wSum = 0.0;
	float clipDistance = zFar - zNear;
	//Calculo la mitjana de la depth dels ve�ns del fragment actual, ponderada amb un cert pes
	//depenent de la posici� del ve�. 
	for (int x = -1; x <= 1; x++) {
		for (int y = -1; y <= 1; y++) {
			nCoords = uv + float2(x * pixelSizeX, y * pixelSizeY);
			nDepth = tex2D(depthMapTex, nCoords).w;
			w = weight[x + 1][y + 1];
			wSum += w;
			meanDepth += nDepth * w;						
		}
	}
	meanDepth /= wSum;
	float gradient = min(1.0, clipDistance * abs(meanDepth - tex2D(depthMapTex, uv).w));
	//gl_FragColor = vec4(gradient, gradient, gradient, 1.0);
	if (gradient > 0.3) gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
	else gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
}



