#version 120

/////////////////////////////////////////////////////
// LIGHTING FUNCTIONS
/////////////////////////////////////////////////////
//Computes the Diffuse and Specular contribution of the light. This computation is 
//the same in all light types. 
void computeLighting(
					//Vectors
					const vec3 L,
					const vec3 P,
                    const vec3 N,
					
					//Material Properties
					const vec3 kd,
					const vec3 ks,
                    const float  shininess,
					
					//Output
					out vec3 dComponent,
					out vec3 sComponent
					)
{
	//DIFFUSE contribution 
	float dContribution = max(dot(N, L), 0.0);
	
	//SPECULAR contribution
	float sContribution;
	if (dContribution > 0.0) {
		vec3 V = normalize(-P);		//View Vector (eyePos - P). However, we are in View Space, so eyePos = (0,0,0)
		vec3 H = normalize(L + V);
		sContribution = pow(max(dot(N, H), 0.0), shininess);
	}
	else sContribution = 0.0;
	
	dComponent = kd * dContribution;
	sComponent = ks * sContribution;
}

//Computes the attenuation of the light. This computation is only valid for POINT and SPOT lights, 
//as DIRECTIONAL lights are not attenuated (they are located at the infinity). 
float computeAttenuation(vec3 lightPosition, vec4 lightAttenuation, vec3 P) {
	float attFactor;
	float d = distance(lightPosition, P);
	float range = lightAttenuation.x;		//Maximum distance where the light has influence. 
	if (d > range) attFactor = 0.0;			//P is outside the light's range. 
	else {
		float constantAtt = lightAttenuation.y;	//CONSTANT factor attenuation
		float linearAtt = lightAttenuation.z;	//LINEAR factor attenuation
		float quadricAtt = lightAttenuation.w;	//QUADRIC factor attenuation
		attFactor = clamp(1.0 / (constantAtt + (linearAtt * d) + (quadricAtt * d * d)), 0.0, 1.0);
	}
	return attFactor;
}

//Computes the spot intensity. This computation is only valid for SPOT lights. 
float spotIntensity(vec3 lightPosition, vec3 lightDirection, float lightCosAngleInner, float lightCosAngleOuter, vec3 P) {
	vec3 V = normalize(P - lightPosition);
	float cosDirPoint = dot(V, lightDirection);			
	if (lightCosAngleInner < cosDirPoint) return 1.0;	//P is inside the interior area of the SPOT, so it recives the full intensity. 
	if (lightCosAngleOuter < cosDirPoint) {
		//P is inside the exterior area of the SPOT, so it receives an interpolated intensity. 
		return smoothstep(lightCosAngleOuter, lightCosAngleInner, cosDirPoint);		
	}
	return 0.0;	//P is OUTSIDE the spot. El punt est� FORA de la zona d'influ�ncia del SPOT. Per tant, no rep cap tipus d'energia provinent de la llum
}

/////////////////////////////////////////////////////
// FRAGMENT SHADER ENTRY POINT
/////////////////////////////////////////////////////

//uniform variables
//Ambient light parameters
uniform vec3 globalAmbient;			//WARNING!!! This should be taken into account only once (not once per light). Maybe in the GBuffer construction. 
//Light parameters
uniform int lightType; 				//0 => POINT; 1 => DIRECTIONAL; 2 => SPOT;
uniform vec3 lightPosition; 						
uniform vec3 lightDirection; 						
uniform vec3 lightColor; 							
uniform float  lightCosAngleInner; 	//Cosinus of the interior angle of the SPOT (irrelevant if it is not a SPOT)
uniform float  lightCosAngleOuter;	//Cosinus of the exterior angle of the SPOT (irrelevant if it is not a SPOT)
uniform vec4 lightAttenuation;				
//G-Buffer information
uniform sampler2D colorTex;  		//Texture that contains the diffuse color per pixel
uniform sampler2D normalTex;  		//Texture that contains the normal (view space) per pixel
uniform sampler2D vSpaceTex;		//Texture that contains the position (view space) per pixel
//ShadowMap information
uniform sampler2D shadowMask;

//varying variables
varying vec2 uv;

void main() {
	//To compute the lighting of the fragment, all vectors need to be on the same space. 
	//We are using View Space to do the lighting computations, so the camera is at the origin.  
	vec3 kd = texture2D(colorTex, uv).xyz;	//DIFFUSE fragment color
	vec3 N = texture2D(normalTex, uv).xyz;	//fragment Surface NORMAL
	vec3 dComponent;						//DIFFUSE component
	vec3 sComponent;						//SPECULAR component
	float att;								//light ATTENUATION factor
	float shadowCoeff;						//SHADOW COEFFICIENT
	
	if (length(N) == 0.0) {
		//Ignore the lighting computation. 
		gl_FragColor = vec4(kd, 1.0);
	}
	else {
		
		vec3 P = texture2D(vSpaceTex, uv).xyz;	//Fragment Position in VIEW SPACE
		float spotInt;
		if (lightType == 2) spotInt = spotIntensity(lightPosition, lightDirection, lightCosAngleInner, lightCosAngleOuter, P);
		else spotInt = 1.0;	//If it is a NON SPOT, the fragment receives the full intensity
		
		if (spotInt > 0.0) {
			//Fragment is inside the SPOT
			vec3 L;
			if (lightType == 1) L = normalize(-lightDirection);	//L for DIRECTIONAL light (position at infinity)
			else L = normalize(lightPosition - P);				//L for POINT or SPOT light
			
			vec3 ks = vec3(1.0, 1.0, 1.0);						//material specular color. WARNING!! Should be codified on the G-Buffer
			float shininess = 200;								//material shininess. WARNING!! Should be codified on the G-Buffer
				
			//Lighting computation
			computeLighting(L, P, N, kd, ks, shininess, dComponent, sComponent);
				
			//Shadow Coefficient
			shadowCoeff = texture2D(shadowMask, uv).x;
							
			//Attenuation Factor
			if (lightType == 1) att = 1.0;	
			else att = computeAttenuation(lightPosition, lightAttenuation, P);
		}
				
		// Calculo el color final
		vec3 aComponent = globalAmbient * kd;
		vec3 finalColor = clamp(aComponent + spotInt * att * shadowCoeff * lightColor * (dComponent + sComponent), 0.0, 1.0);
		gl_FragColor = vec4(finalColor, 1.0);
	}
}