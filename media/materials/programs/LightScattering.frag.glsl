#version 120

//uniform variables
uniform float uViewportW;					//Viewport Width in Pixels
uniform float uViewportH;					//Viewport Height in Pixels 
uniform float uFovY;						//Camera's FovY in Radians
uniform mat4 uViewSpaceToShadowMapMatrix;	//Matrix that given a position in View Space, transforms it into Shadow Map Space.
uniform vec3 uLightPosVS;					//Light Position in View Space
uniform int uNumSamples;					//Number of samples taken along the ray
uniform float uExposure;
uniform float uAttenuation;
uniform sampler2D shadowMap;
uniform sampler2D viewSpacePosMap;

//varying variables
varying vec2 uv;

/////////////////////////////////////////////////////
// IMPORTED AUXILIARY FUNCTIONS: utils.frag.glsl
///////////////////////////////////////////////////// 
bool realLEqual(float a, float b, float epsilon);

/////////////////////////////////////////////////////
// AUXILIAR FUNCTIONS
///////////////////////////////////////////////////// 
float visibility(vec3 posVS) {
	vec4 shadowUV = uViewSpaceToShadowMapMatrix * vec4(posVS, 1.0);	//Position in ShadowMap Space
	shadowUV.xy /= shadowUV.w;											//Perspective division
	vec3 lightDir = uLightPosVS - posVS;
	shadowUV.z = length(lightDir);							
	lightDir /= shadowUV.z;
	if (abs(shadowUV.x) > 1.0 || abs(shadowUV.y) > 1.0 || (shadowUV.w < 0.0)) return 0.0; 	//texCoords out of range
	float shadowMapValue = texture2D(shadowMap, shadowUV.xy).x;						//The depth value stored in the ShadowMap
	if (shadowMapValue == 0.0) return 0.0;											//The depth value stored in the ShadowMap corresponds to a background fragment. 
	
	//If we arrive here is because shadowMapValue and shadowUV are valid. 
	//Determine if the fragment is in shadow or not, by applying a certain bias
	if (realLEqual(shadowUV.z, shadowMapValue, 1.0)) return 1.0;	//The fragment is lit
	return 0.0;
}

vec3 getPixelPosVS() {
	//Given the uv coordinates of the pixel, returns the 3D position of that pixel in View Space
	float d = (float(uViewportH) / 2.0) / tan(uFovY / 2.0);	//Distance from the camera to the projection surface, given the camera's fovY
	
	//Initialize pixelPosVS by Displacing d units in the direction of the forward vector of the camera. 
	//Remember that in View Space, the camera is at the origin, looking at -Z (so the forward direction is -Z)
	vec3 pixelPosVS = vec3(0.0, 0.0, -1.0) * d;	//The VS position at the center of the screen
	//Now add the horizontal and vertical contribution. Notice that as the SS coordinates are in the range 0..1, 
	//and hence we have to correct the bias to get the appropiate coordinates 
	pixelPosVS += vec3(1.0, 0.0, 0.0) * (uViewportW * (uv.x - 0.5));	//Horizontal contribution
	pixelPosVS += vec3(0.0, -1.0, 0.0) * (uViewportH * (uv.y - 0.5));	//Vertical contribution. Notice that the vector -Y is used, as the origin in SS is the TOP LEFT corner
	return pixelPosVS;
}

//EQUATION
//uExposure * SUM[i = 0..n](decay^i * weight * sample(i))

void main() {
	vec3 posVS = texture2D(viewSpacePosMap, uv).xyz;	//Fragment's View Space Position
	vec3 pixelPosVS = getPixelPosVS();	//The pixel position in View Space
	vec3 dirVS = normalize(pixelPosVS);	//Direction vector starting from the camera and going through the pixelPosVS. Remember that we are in View Space, so the camera is at the origin. 
	
	//Compute the step size
	float l;
	if (dot(posVS, posVS) == 0.0) l = 10.0;	//The fragment corresponds to the background
	else l = length(posVS);
	float dl = l / float(uNumSamples);
	
	//Initialization and iteration through the ray
	vec3 samplePosVS;
	float sampleVisibility;
	float d;
	float L = 0.0;
	float sampleScattering;
	for (int i = 0; i < uNumSamples; i++) {
		samplePosVS = (dirVS * i * dl);
		sampleVisibility = visibility(samplePosVS);
		d = length(uLightPosVS - samplePosVS);		//Distance from the Sample Point to the Light Source
		//sampleScattering = sampleVisibility * (1.0 / (uAttenuation * d));
		//L += sampleScattering;
		L += sampleVisibility;
	}
	L = uExposure * (L / float(uNumSamples));	//Scale for the number of samples and control the overall intensity of the effect
	gl_FragColor = vec4(L, L, L, 1.0);	
}

//void main() {
//	vec3 originVS = texture2D(viewSpacePosMap, uv).xyz;	//Fragment's View Space Position
//	vec3 pixelPosVS = getPixelPosVS();	//The pixel position in View Space
//	vec3 dirVS = -originVS;	//Direction Vector from the fragment's position to the camera. As we are working in View Space, camera's position is 0. 
//	float l = length(dirVS);
//	float dl = l / float(uNumSamples);
//	dirVS = normalize(-pixelPosVS);
//	vec3 samplePosVS;
//	float sampleVisibility;
//	float d;
//	float L = 0.0;
//	float sampleScattering;
//	for (int i = 0; i < uNumSamples; i++) {
//		samplePosVS = originVS + (dirVS * i * dl);
//		sampleVisibility = visibility(samplePosVS);
//		d = length(uLightPosVS - samplePosVS);		//Distance from the Sample Point to the Light Source
//		//sampleScattering = sampleVisibility * (1.0 / (uAttenuation * d));
//		//L += sampleScattering;
//		L += sampleVisibility;
//	}
//	L = uExposure * (L / float(uNumSamples));	//Scale for the number of samples and control the overall intensity of the effect
//	gl_FragColor = vec4(L, L, L, 1.0);	
//	if (dot(originVS, originVS) == 0.0) gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
//}
