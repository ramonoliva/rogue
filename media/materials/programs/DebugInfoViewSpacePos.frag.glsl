#version 120

//uniform parameters
uniform sampler2D viewSpacePosMap;

//varying parameters
varying vec2 uv;

void main(){
	vec3 posVS = texture2D(viewSpacePosMap, uv).xyz;
	gl_FragColor = vec4(posVS, 1.0);
}