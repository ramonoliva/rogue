//Shaders per construir el GBuffer de l'escena
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// VERTEX SHADER: DEFINITION
/////////////////////////////////////////////////////////////////////////////
//Estructura que defineix el input del VS
struct VSInput {
	float4 position	:	POSITION;	//Posici� del v�rtex en coordenades de m�n
	float3 normal	:	NORMAL;		//Normal del v�rtex en coordenades de m�n
	float2 uv		:	TEXCOORD0;	//Coordenades de textura
};

//Estructura que defineix el output del VS
struct VSOutput {
	float4 position 	: 	POSITION;	//Coordenades de clipping del v�rtex
	float2 uv			:	TEXCOORD0;	//Coordenades de textura
	float3 normal		:	TEXCOORD1;	//Normal i profunditat del v�rtex en View Space
	float3 viewSpacePos	:	TEXCOORD2;	//Coordenades en View Space del v�rtex
};

//Funci� d'entrada del shader (equivalent al main de GLSL)
VSOutput GBuffer_vp(
        VSInput IN, 

        uniform float4x4 uWorldViewMatrix,					//Matriu per transformar POSICIONS a View Space
		uniform float4x4 uInverseTransposeWorldViewMatrix,	//Matriu per transformar VECTORS a ViewSpace
		uniform float4x4 uProjectionMatrix					//Matriu per transformar POSICIONS a Clipping Space
        )
{
		VSOutput OUT;
        OUT.viewSpacePos = mul(uWorldViewMatrix, IN.position).xyz;						//Posici� en View Space
		OUT.normal = mul(uInverseTransposeWorldViewMatrix, float4(IN.normal, 0.0)).xyz;	//Normal en View Space
		OUT.uv = IN.uv;																	//Les coordenades de textura no varien
		
		OUT.position = mul(uProjectionMatrix, float4(OUT.viewSpacePos, 1.0));			//Posici� en Clipping Space
		
		return OUT;
}

/////////////////////////////////////////////////////////////////////////////
// FRAGMENT SHADER: DEFINITION
/////////////////////////////////////////////////////////////////////////////
//Utilitzem el tipus TEXCOORD per passar par�metres del VS al FS

//Estructura que defineix el input del FS. Els valors han estat interpolats pel rasteritzador
struct FSInput {
	float2 uv			:	TEXCOORD0;	//Coordenades de textura del fragment
	float3 normal		:	TEXCOORD1;	//normal en View Space del fragment
	float3 viewSpacePos	:	TEXCOORD2;	//posici� en View Space del fragment
};

//Estructura que defineix el output del FS
struct FSOutput {
	float4 color			:	COLOR0;	//Color 							(Capa0 del GBuffer)
	float4 normalDepth		:	COLOR1;	//Normal + Depth 					(Capa1 del GBuffer)
	float4 viewSpacePos		:	COLOR2;	//Posici� en View Space 			(Capa2 del GBuffer)
};

FSOutput GBuffer_fp
(
    FSInput IN, 

	uniform sampler colorTex 	: register(s0)  	// Textura que cont� els colors per p�xel
)
{
		FSOutput OUT;
		
		//Guardo el color del fragment
		OUT.color = tex2D(colorTex, IN.uv);
		
		//Guardo la normal del fragment (capa1.rgb del GBuffer) 
		//1) Normalitzo la normal. Aquest pas �s necessari perqu� la interpol�laci� que es produeix
		//durant la rasteritzaci�, pot denormalitzar la normal. 
		OUT.normalDepth.rgb = normalize(IN.normal);
        OUT.normalDepth.a = length(IN.viewSpacePos);
		
		//Guardo les coordenades en View Space
		OUT.viewSpacePos = float4(IN.viewSpacePos, 1.0);

		return OUT;
}

/////////////////////////////////////////////////////////////////////////////
// FRAGMENT SHADER: DEFINITION
/////////////////////////////////////////////////////////////////////////////
FSOutput OnlyColorGBuffer_fp
(
    uniform float3 color
)
{
	FSOutput OUT;
		
	//Guardo el color del fragment
	OUT.color = float4(color, 1.0);
	OUT.normalDepth = float4(0.0);
	OUT.viewSpacePos = float4(0.0);

	return OUT;
}

/////////////////////////////////////////////////////////////////////////////
// FRAGMENT SHADER: DEFINITION
/////////////////////////////////////////////////////////////////////////////
FSOutput ClearGBuffer_fp()
{
	//Neteja la informaci� del GBuffer
	FSOutput OUT;
		
	OUT.color = float4(0.0);
	OUT.normalDepth = float4(0.0);
	OUT.viewSpacePos = float4(0.0);
	return OUT;
}