static const float4 proj1 = float4(1.8106601, 0.0, 0.0, 0.0);
static const float4 proj2 = float4(0.0, -2.4142134, 0.0, 0.0);
static const float4 proj3 = float4(0.0, 0.0, -1.0202020, -2.0202019);
static const float4 proj4 = float4(0.0, 0.0, -1.0, 0.0);


static const float3 sampleKernel[4] = 
{
	float3(-0.03106, -0.0933341, 0.0180004),
	float3(0.0334573, 0.0880263, 0.124684),
	float3(0.0134317, -0.133791, 0.295879),
	float3(0.199716, 0.401678, 0.407808)
};

//Normal-Oriented Hemisphere SSAO
void hemisphereSSAO_fp
(
	in float2 uv						:	TEXCOORD0, 		//Fragment's tex coords
	
	//GBuffer Data
	uniform sampler2D normalDepthMap	:	register(s0),	//Normal (rgb) and normalized Depth (a) per pixel
	uniform sampler2D viewSpacePosMap	:	register(s1),	//View Space Position per pixel
	
	//SSAO Configuration
	uniform int resultType,
	//uniform float3[32] sampleKernel, 
	uniform int sampleKernelSize,
	uniform sampler2D rotationKernel	:	register(s2),	
	uniform float rotationKernelScaleX,						//rotationKernelScale.x = (viewport.w / rotationKernel.w); rotationKernelScale.y = (viewport.h / rotationKernel.h);
	uniform float rotationKernelScaleY,
	uniform float radius, 									//Radius of influence of the AO
	//uniform float4x4 projectionMatrix,
	
	out float4 result				:	COLOR0
)
{
	float2 rotationKernelScale = float2(rotationKernelScaleX, rotationKernelScaleY);
	float3 originVS = tex2D(viewSpacePosMap, uv).xyz;	//Fragment's position in View Space
	float4 normalDepth = tex2D(normalDepthMap, uv);		//Fragment's normal (xyz) and normalized depth (w)
	float3 normal = normalDepth.xyz;
	
	//Construct a new orthonormal basis matrix to reorient the sample kernel along the fragments's normal. 
	float3 rVec = tex2D(rotationKernel, uv * rotationKernelScale).xyz * 2.0 - 1.0;	//Recover the rotation vector from the texture + scale bias
		
	float3 proj = normal * dot(rVec, normal);				//The projection of rVec over the normal. Notice that this vector is parallel to normal
	float3 tangent = normalize(rVec - proj);				//Obtain a perpendicular vector to normal using rVec by substracting its projection (the parallel component of rVec to normal)
	float3 bitangent = normalize(cross(normal, tangent));	//As normal and tangent are perpendicular, the result of their cross product is perpendicular to both of them
	
	float3x3 rotationMatrix = float3x3(tangent, bitangent, normal);
		
	//Compute the occlusion factor
	int numOccluders = 0;	//Number of samples occluding the current fragment
	float3 sampleDirVS;		//Sample direction in View Space
	float3 samplePosVS;		//Sample position in View Space
	float4 samplePosSS = float4(0.0, 0.0, 0.0, 0.0);		//Sample position in Screen Space
	float3 texPosVS;
	for (int i = 0; i < sampleKernelSize; i++) {
		//Get the sample position
		sampleDirVS = mul(rotationMatrix, sampleKernel[i]);
		samplePosVS = originVS + (sampleDirVS * radius);	//The position of the sample in View Space
		
		//Project sample position to Screen Space
		//samplePosSS = mul(projectionMatrix, float4(samplePosVS, 1.0));
		samplePosSS.x = dot(proj1, float4(samplePosVS, 1.0));
		samplePosSS.y = dot(proj2, float4(samplePosVS, 1.0));
		samplePosSS.w = dot(proj4, float4(samplePosVS, 1.0));
		
		samplePosSS.xy /= samplePosSS.w;				//Perspective Division
		samplePosSS.xy = (samplePosSS.xy * 0.5) + 0.5;	//Bias from the range [-1, 1] to [0, 1]
		saturate(samplePosSS.xy);
		
		//Get the depth value in the sample position and compare
		texPosVS = tex2D(viewSpacePosMap, samplePosSS.xy).xyz;
		
		//float rangeCheck= abs(origin.z - sampleDepth) < uRadius ? 1.0 : 0.0;
		//occlusion += (sampleDepth <= sample.z ? 1.0 : 0.0) * rangeCheck;
				
		if (abs(originVS.z - texPosVS.z) < radius) {
			//if (texPosVS.z <= samplePosVS.z) {
			if (texPosVS.z >= samplePosVS.z) {	//Notice that we are looking towards the -Z
				//The ray from originVS to samplePosVS intersects with the geometry of the scene. 
				//Therefore, we have found an occluder of the current fragment. 
				numOccluders ++;
			}
		}
	}
	
	if (resultType == 0) {
		//Compute the occlusion factor
		float oFactor = saturate(1.0 - (float(numOccluders) / 1.0));
		result = float4(oFactor, oFactor, oFactor, 1.0);
	}
	else if (resultType == 1) {
		//Show the rotation vector choosen
		result = float4(tex2D(rotationKernel, uv * rotationKernelScale).xyz, 1.0);
	}
	else if (resultType == 2) {
		//Show the tangent vector
		result = float4((tangent + 1.0) / 2.0, 1.0);
	}
	else if (resultType == 3) {
		//Show the bitangent vector
		result = float4((bitangent + 1.0) / 2.0, 1.0);
	}
	else if (resultType == 4) {
		//Show the origin pos
		result = float4((normalize(originVS) + 1.0) / 2.0, 1.0);
	}
	else if (resultType == 5) {
		//Show compute tex coords
		result = float4(samplePosSS.xy, 0.0, 1.0);
	}
	else if (resultType == 6) {
		//Show sample kernel
		result = float4((sampleKernel[0] + 1.0) / 2.0, 1.0);
	}
	else if (resultType == 7) {
		//Show uv coords
		result = float4(uv, 0.0, 1.0);
	}
}