/////////////////////////////////////////////////////
// AUX FUNCTIONS
/////////////////////////////////////////////////////
int realEqual(float a, float b, float epsilon) {
	float diff = abs(a - b);
	if (diff < epsilon) return 1;
	return 0;
}

/////////////////////////////////////////////////////
// LIGHTING FUNCTIONS
/////////////////////////////////////////////////////
//Computes the Diffuse and Specular contribution of the light. This computation is 
//the same in all light types. 
void computeLighting(
					//Vectors
					const float3 L,
					const float3 P,
                    const float3 N,
					
					//Material Properties
					const float3 kd,
					const float3 ks,
                    const float  shininess,
					
					//Output
					out float3 dComponent,
					out float3 sComponent
					)
{
	//DIFFUSE contribution 
	float dContribution = max(dot(N, L), 0.0);
	
	//SPECULAR contribution
	float sContribution;
	if (dContribution > 0.0) {
		float3 V = normalize(-P);		//View Vector (eyePos - P). However, we are in View Space, so eyePos = (0,0,0)
		float3 H = normalize(L + V);
		sContribution = pow(max(dot(N, H), 0.0), shininess);
	}
	else sContribution = 0.0;
	
	dComponent = kd * dContribution;
	sComponent = ks * sContribution;
}

//Computes the attenuation of the light. This computation is only valid for POINT and SPOT lights, 
//as DIRECTIONAL lights are not attenuated (they are located at the infinity). 
float computeAttenuation(float3 lightPosition, float4 lightAttenuation, float3 P) {
	float attFactor;
	float d = distance(lightPosition, P);
	float range = lightAttenuation.x;		//Maximum distance where the light has influence. 
	if (d > range) attFactor = 0.0;			//P is outside the light's range. 
	else {
		float constantAtt = lightAttenuation.y;	//CONSTANT factor attenuation
		float linearAtt = lightAttenuation.z;	//LINEAR factor attenuation
		float quadricAtt = lightAttenuation.w;	//QUADRIC factor attenuation
		attFactor = saturate(1.0 / (constantAtt + (linearAtt * d) + (quadricAtt * d * d)));
	}
	return attFactor;
}

//Computes the spot intensity. This computation is only valid for SPOT lights. 
float spotIntensity(float3 lightPosition, float3 lightDirection, float lightCosAngleInner, float lightCosAngleOuter, float3 P) {
	float3 V = normalize(P - lightPosition);
	float cosDirPoint = dot(V, lightDirection);			
	if (lightCosAngleInner < cosDirPoint) return 1.0;	//P is inside the interior area of the SPOT, so it recives the full intensity. 
	if (lightCosAngleOuter < cosDirPoint) {
		//P is inside the exterior area of the SPOT, so it receives an interpolated intensity. 
		return smoothstep(lightCosAngleOuter, lightCosAngleInner, cosDirPoint);		
	}
	return 0.0;	//P is OUTSIDE the spot. El punt est� FORA de la zona d'influ�ncia del SPOT. Per tant, no rep cap tipus d'energia provinent de la llum
}

/////////////////////////////////////////////////////
// FRAGMENT SHADER ENTRY POINTS
/////////////////////////////////////////////////////
void lightingSpot_fp(
				in float2 uv				:	TEXCOORD0,
				
				//Ambient light parameters
				uniform float3 globalAmbient, 						//WARNING!!! This should be taken into account only once (not once per light). Maybe in the GBuffer construction. 
				
				//Light parameters
				uniform float lightType, 							//0 => DIRECTIONAL; 1 => POINT; 2 => SPOT;
				uniform float3 lightPosition, 						
				uniform float3 lightDirection, 						
				uniform float3 lightColor, 							
				uniform float  lightCosAngleInner, 					//Cosinus of the interior angle of the SPOT (irrelevant if it is not a SPOT)
				uniform float  lightCosAngleOuter,					//Cosinus of the exterior angle of the SPOT (irrelevant if it is not a SPOT)
				uniform float4 lightAttenuation,				
				
				//G-Buffer information
				uniform sampler2D colorTex 	: 	register(s0),  		//Texture that contains the diffuse color per pixel
				uniform sampler2D normalTex : 	register(s1),  		//Texture that contains the normal (view space) per pixel
				uniform sampler2D vSpaceTex	: 	register(s2),		//Texture that contains the position (view space) per pixel
				
				//ShadowMap information
				uniform sampler2D shadowMask	:	register(s3),
				
				out float4 oColor			: 	COLOR0
				)
{
	//To compute the lighting of the fragment, all vectors need to be on the same space. 
	//We are using View Space to do the lighting computations, so the camera is at the origin.  
	float3 kd = tex2D(colorTex, uv).xyz;	//DIFFUSE fragment color
	float3 N = tex2D(normalTex, uv).xyz;	//fragment Surface NORMAL
	float3 dComponent;						//DIFFUSE component
	float3 sComponent;						//SPECULAR component
	float att;								//light ATTENUATION factor
	float shadowCoeff;						//SHADOW COEFFICIENT
	
	if (length(N) == 0.0) {
		//Ignore the lighting computation. 
		oColor = float4(kd, 1.0);
	}
	else {
		
		float3 P = tex2D(vSpaceTex, uv).xyz;	//Fragment Position in VIEW SPACE
		float spotInt;
		if (lightType == 2) spotInt = spotIntensity(lightPosition, lightDirection, lightCosAngleInner, lightCosAngleOuter, P);
		else spotInt = 1.0;	//If it is a NON SPOT, the fragment receives the full intensity
		
		if (spotInt > 0.0) {
			//Fragment is inside the SPOT
			float3 L = normalize(lightPosition - P);	//Llum PUNTUAL 
			float3 ks = float3(1.0, 1.0, 1.0);			//material specular color. WARNING!! Should be codified on the G-Buffer
			float shininess = 200;						//material shininess. WARNING!! Should be codified on the G-Buffer
				
			//Calculo la il�luminaci�
			computeLighting(L, P, N, kd, ks, shininess, dComponent, sComponent);
				
			//Obtinc el coeficient de sombra
			shadowCoeff = tex2D(shadowMask, uv).x;		//Valor d'ombra del fragment
							
			//Calculo el factor d'attenuaci�
			att = computeAttenuation(lightPosition, lightAttenuation, P);
		}
				
		// Calculo el color final
		float3 aComponent = globalAmbient * kd;
		float3 finalColor = aComponent + spotInt * att * shadowCoeff * lightColor * (dComponent + sComponent);
		oColor = float4(finalColor, 1.0);
	}
}

void lightingPoint_fp(
				in float2 uv					:	TEXCOORD0,
				
				//Ambient light parameters
				uniform float3 globalAmbient, 						//Color de la llum ambient
				
				//Light parameters
				uniform float3 lightPosition, 						//Posici� de la llum
				uniform float3 lightColor, 							//Color de la llum
				uniform float4 lightAttenuation,					//Par�metres d'atenuaci� de la llum
				
				//G-Buffer information
				uniform sampler colorTex 		: 	register(s0),  		//Textura que cont� els colors per p�xel
				uniform sampler normalTex 		: 	register(s1),  		//Textura que cont� les normals per p�xel
				uniform sampler vSpaceTex		: 	register(s2),		//Textura que cont� la posici� en coordenades d'objecte
				
				//ShadowMap information
				uniform sampler2D shadowMask	:	register(s3), 		
				
				out float4 oColor				: 	COLOR0
				)
{
	//Realitza el c�lcul de la il�luminaici� tenint en compte una llum PUNTUAL
	//Per fer el c�lcul de la il�luminaci�, necessitem que tots els vectors estiguin en el mateix
	//sistema de coordenades. En aquest cas, treballem en View Space i per tant, la c�mera est� a l'origen. 
	float3 dColor = tex2D(colorTex, uv).xyz;	//Color DIFUS del fragment
	float3 N = tex2D(normalTex, uv).xyz;		//Normal de la superf�cie en el fragment
	float3 dComponent;
	float3 sComponent;
	float attFactor;
	float luminosity;
	if (length(N) == 0.0) {
		//�s un p�xel en el que hem d'ignorar el c�lcul de la il�luminaci�
		oColor = float4(dColor, 1.0);
	}
	else {
		float shadowCoeff = tex2D(shadowMask, uv).x;	//Valor d'ombra del fragment
		if (shadowCoeff > 0) {
			float3 sColor = float3(1.0, 1.0, 1.0);		//Color ESPECULAR del fragment
			float shininess = 200;						//Intensitat ESPECULAR del fragment
			float3 P = tex2D(vSpaceTex, uv).xyz;		//Posici� en View Space del fragment
			float3 L = normalize(lightPosition - P);	
					
			//Calculo la il�luminaci�
			computeLighting(L, P, N, dColor, sColor, shininess, dComponent, sComponent);
			
			//Calculo el factor d'attenuaci�
			attFactor = computeAttenuation(lightPosition, lightAttenuation, P);	
			if (attFactor > 0.0) luminosity = 1.0 / attFactor;
			else luminosity = 0.0;
		}
		else {
			dComponent = float3(0.0);
			sComponent = float3(0.0);
			luminosity = 0.0;
		}
				
		// Calculo el color final
		float3 aComponent = globalAmbient * dColor;
		float3 finalColor = aComponent + luminosity * lightColor * (dComponent + sComponent) * shadowCoeff;
		oColor = float4(finalColor, 1.0);
	}
}

void lightingDirectional_fp(
				in float2 uv					:	TEXCOORD0,
				
				//Ambient light parameters
				uniform float3 globalAmbient, 						//Color de la llum ambient
				
				//Light parameters
				uniform float3 lightDirection, 						//Posici� de la llum
				uniform float3 lightColor, 							//Color de la llum
								
				//G-Buffer information
				uniform sampler2D colorTex 		: 	register(s0),  		//Textura que cont� els colors per p�xel
				uniform sampler2D normalTex 	: 	register(s1),  		//Textura que cont� les normals NORMALITZADES per p�xel
				uniform sampler2D vSpaceTex		: 	register(s2),		//Textura que cont� la posici� en View Space
				
				//ShadowMap information
				uniform sampler2D shadowMask	:	register(s3), 		
				
				out float4 oColor				: 	COLOR0
				)
{
	//Realitza el c�lcul de la il�luminaici� tenint en compte una llum DIRECCIONAL
	//Per fer el c�lcul de la il�luminaci�, necessitem que tots els vectors estiguin en el mateix
	//sistema de coordenades. En aquest cas, treballem en View Space i per tant, la c�mera est� a l'origen. 
	float3 N = tex2D(normalTex, uv).xyz;		//Normal de la superf�cie en el fragment
	float3 dColor = tex2D(colorTex, uv).xyz;	//Color DIFUS del fragment
	float3 dComponent;
	float3 sComponent;
	float shininess = 200;						//Intensitat ESPECULAR del fragment
	if (length(N) == 0.0) {
		//�s un p�xel en el que hem d'ignorar el c�lcul de la il�luminaci�
		oColor = float4(dColor, 1.0);
	}
	else {
		float shadowCoeff = tex2D(shadowMask, uv).x;	//Valor d'ombra del fragment
		if (shadowCoeff > 0) {
			//Calculem la il�luminaci� del p�xel utilitzant el model de PHONG
			float3 sColor = float3(1.0, 1.0, 1.0);		//Color ESPECULAR del fragment
			float3 P = tex2D(vSpaceTex, uv).xyz;		//Posici� en View Space del fragment
			float3 L = normalize(lightDirection);	
					
			//Calculo la il�luminaci�
			computeLighting(L, P, N, dColor, sColor, shininess, dComponent, sComponent);
		}
		else {
			//Es un punt a l'ombra de la llum. No cal calcular la il�luminaci�
			dComponent = float3(0.0);
			sComponent = float3(0.0);
		}
		// Calculo el color final
		float3 aComponent = globalAmbient * dColor;
		if (shininess > 0) sComponent = 0.0;
		float3 finalColor = aComponent + lightColor * (dComponent + sComponent) * shadowCoeff;
		oColor = float4(finalColor, 1.0);
	}
}
			 
			 
