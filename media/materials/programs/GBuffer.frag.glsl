#version 120
#extension GL_ARB_draw_buffers : enable

//varying parameters
varying	vec3 oViewPos;
varying	vec3 oNormal;
varying vec2 oUV;

//uniform parameters
uniform sampler2D colorTex;

void main() 
{
	//Fragment's diffuse colour
	gl_FragData[0] = texture2D(colorTex, oUV);
	
	//Fragment's normal and depth
	gl_FragData[1] = vec4(normalize(oNormal).xyz, length(oViewPos));
	
	//Fragment's view space position
	gl_FragData[2] = vec4(oViewPos, 1.0);
}