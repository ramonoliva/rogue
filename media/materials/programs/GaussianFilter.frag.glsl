#version 120

const float PI = 3.14159265;

float gaussianValue(float x, float sigma) {
	//return the value of the gaussian at point x with standard deviation sigma
	float term1 = 1.0 / (sigma * sqrt(2.0 * PI));
	float term2 = exp(-((x * x) / (2.0 * sigma * sigma)));
	return term1 * term2;
}

//uniform parameters
uniform int orientation;	//0 => Filter_X; 1 => Filter_Y;
uniform sampler2D theTex;	//The texture to filter	
uniform float texelSizeX;							
uniform float texelSizeY;
uniform int kernelSize;
uniform float sigma; 		//standard deviation of the gaussian

//varying parameters
varying vec2 uv;

//A 2 pass gaussian filter (requires an ODD kernelSize to work properly)
void main() {
	float weight = gaussianValue(0, sigma);
	vec4 sum = weight * texture2D(theTex, uv);
	float sumWeight = weight;
	
	int halfSize = int(floor(float(kernelSize) * 0.5));
	vec2 offset;
			
	if (orientation == 0) {
		//Filter horizontally
		offset.y = 0.0;
		for (int i = 1; i <= halfSize; i++) {
			//In the gaussian distribution, g(i) == g(-i). I exploit this fact here. 
			weight = gaussianValue(float(i), sigma);
			offset.x = texelSizeX * float(i);
			sum += weight * texture2D(theTex, uv + offset);	//+i neighbor
			sum += weight * texture2D(theTex, uv + -offset);	//-i neighbor
			sumWeight += 2.0 * weight;
		}
	}
	else if (orientation == 1) {
		//Filter vertically
		offset.x = 0.0;
		for (int i = 1; i <= halfSize; i++) {
			//In the gaussian distribution, g(i) == g(-i). I exploit this fact here. 
			weight = gaussianValue(float(i), sigma);
			offset.y = texelSizeY * float(i);
			sum += weight * texture2D(theTex, uv + offset);		//+i neighbor
			sum += weight * texture2D(theTex, uv + -offset);	//-i neighbor
			sumWeight += 2.0 * weight;
		}
	}
	float mean = sum / sumWeight;
	gl_FragColor = vec4(mean);
}