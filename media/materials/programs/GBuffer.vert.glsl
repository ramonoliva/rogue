#version 120

//Vertex attributes
attribute vec4 vertex;
attribute vec3 normal;

//uniform parameters
uniform mat4 uWorldViewMatrix;					//Matrix to transform POSITIONS to View Space
uniform mat4 uInverseTransposeWorldViewMatrix;	//Matrix to transform VECTORS to View Space
uniform mat4 uProjectionMatrix;					//Matrix to transform to Clip Space

//Varying parameters
varying	vec3 oViewPos;
varying	vec3 oNormal;
varying vec2 oUV;

void main()
{
	oViewPos = (uWorldViewMatrix * vertex).xyz; 							//Vertex Position in View Space
	oNormal = (uInverseTransposeWorldViewMatrix * vec4(normal, 0)).xyz; 	//Vertex Normal in View Space
	gl_Position = uProjectionMatrix * vec4(oViewPos, 1.0);
	oUV = gl_MultiTexCoord0.xy;
}