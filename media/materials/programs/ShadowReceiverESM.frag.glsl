#version 120

//uniform variables
uniform mat4 uViewSpaceToShadowMapMatrix;	//Matrix to transform positions given in View Space to Shadow Map Space (so, it is the concatenation of the inverse of the view matrix and the shadow map matrix)
uniform vec3 uLightPosVS;					//Light Position in View Space	
uniform float darkeningFactor;
uniform float uShadowDepthBias;
uniform float uShadowDepthBiasClamp;
uniform sampler2D shadowMap;				//Texture that contains the depth render from the point of view of the light
uniform sampler2D viewSpacePosMap;			//Texture that contains the View Space Position for each fragment
uniform sampler2D normalMap;

//varying variables
varying vec2 uv;

void main() {
	//Get the non-homobeneous texture coordinates (divide by w, equivalent to the perspective division in positions)
	vec3 posVS = texture2D(viewSpacePosMap, uv).xyz;				//Fragment's View Space Position
	vec4 shadowUV = uViewSpaceToShadowMapMatrix * vec4(posVS, 1.0);	//Fragment's Shadow Map Coords
    shadowUV.xy = shadowUV.xy / shadowUV.w;							//Texture Coordinates for the ShadowMap (perspective division)
	vec3 lightDir = uLightPosVS - posVS;
	shadowUV.z = length(lightDir);
	lightDir /= shadowUV.z;
	
	float shadowCoeff;
	if ((abs(shadowUV.x) > 1.0) || (abs(shadowUV.y) > 1.0) || (shadowUV.w < 0.0)) {
		//Invalid texture coordiantes. Ignore the computation of shadowCoeff
		shadowCoeff = 1.0;	
	}
	else {
		//Correct the depth with a certain bias
		vec3 normal = texture2D(normalMap, uv).xyz;					//Fragment's normal
		float cosTheta = clamp(dot(normal, lightDir), 0.0, 1.0);	//cosTheta is dot( n,l ), clamped between 0 and 1
		float bias = uShadowDepthBias * tan(acos(cosTheta)); 	
		bias = clamp(bias, 0, uShadowDepthBiasClamp);
		shadowUV.z -= bias;
		
		//Obtain the shadowMapValue and compute the shadowCoeff
		float shadowMapValue = texture2D(shadowMap, shadowUV.xy).x;
		if (shadowMapValue == 0.0) shadowCoeff = 1.0;	//The value stored on the depthMap corresponds to a background fragment. 
		else shadowCoeff = clamp(exp(-darkeningFactor * (shadowUV.z - shadowMapValue)), 0.0, 1.0);
	}
	gl_FragColor = vec4(shadowCoeff);
}