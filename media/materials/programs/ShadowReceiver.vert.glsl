#version 120

//Vertex attributes
attribute vec4 vertex;

//uniform variables
uniform mat4 uWorldMatrix;			//Transform from Object Space to World Space 
uniform mat4 uWorldViewProjMatrix;	//Transform to Clip Space
uniform mat4 uLightViewMatrix;		//Transform to Light Space 
uniform mat4 uShadowMapMatrix;		//Texture Matrix Projection

//varying variables
varying vec4 vShadowUV;
varying vec3 vLightSpacePos;

//This render is done from the point of view of the camera. Notice that this step cannot be 
//merged with the GBuffer construction, as it depends of the light casting the shadows. 
void main() {
    vec4 worldPos = uWorldMatrix * vertex;				//Vertex Position in World Space
	vLightSpacePos = (uLightViewMatrix * worldPos).xyz;	//Vertex Position in Light Space
    vShadowUV = uShadowMapMatrix * worldPos;			//Vertex Texture Coordinates of the Shadow Map
	gl_Position = uWorldViewProjMatrix * vertex;		//Vertex Position in Clip Space
}