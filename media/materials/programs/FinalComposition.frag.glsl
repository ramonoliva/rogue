#version 120

//uniform parameters
uniform sampler2D sceneLitTex; 		//Map that contains the lighting per pixel of the scene
uniform sampler2D sceneOutlinesTex;	//Map that contains the outlines (for toon shading)
uniform sampler2D sceneSSAO;		//Map with the SSAO contribution

//varying parameters
varying vec2 uv;

//Composes the final scene
void main() {
	vec3 litColor = texture2D(sceneLitTex, uv).xyz;
	vec3 outlineColor = texture2D(sceneOutlinesTex, uv).xyz;
	vec3 ssaoColor = texture2D(sceneSSAO, uv).xxx;
	gl_FragColor = vec4(ssaoColor * litColor * outlineColor, 1.0);
}



