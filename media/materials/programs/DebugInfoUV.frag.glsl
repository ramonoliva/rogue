#version 120

//varying parameters
varying vec2 uv;

//Shows the View Space Normals of the scene
void main() {
	gl_FragColor = vec4(uv, 0.0, 1.0);
}