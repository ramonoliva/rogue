#version 120
#extension GL_ARB_draw_buffers : enable

//uniform attributes
uniform vec4 bgColor;

//Clears the contents of the GBuffer
void main() {
	gl_FragData[0] = bgColor;
	gl_FragData[1] = vec4(0.0);
	gl_FragData[2] = vec4(0.0);
}