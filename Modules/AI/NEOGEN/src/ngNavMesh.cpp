#include "ngNavMesh.h"
#include "ngGlobalVars.h"

using namespace NEOGEN;
using namespace GeometrySolver;

#pragma region CREATION AND DESTRUCTION

NavMesh::NavMesh() {
	createGraphics();
}

NavMesh::NavMesh(VertTable& vTable, EdgeTable& eTable, CellTable& cTable) {
	createGraphics();
	vertTable = vTable;
	edgeTable = eTable;
	cellTable = cTable;
	initPortalTable();
}

NavMesh::~NavMesh() {
	OgreUtils::destroySceneNode(_nodeGraph);
	OgreUtils::destroySceneNode(_nodeEdges);

	//DELETE OTHER DATA
	vertTable.clear();
	edgeTable.clear();
	cellTable.clear();
	_portalTable.clear();
	vertsToEdgeTable.clear();
}

void NavMesh::initPortalTable() {
	for (EdgeTable::iterator it = edgeTable.begin(); it != edgeTable.end(); it++) {
		if (it->second.isPortal()) _portalTable.insert(it->first);
	}
}

void NavMesh::createGraphics() {
	mSceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
			
	_nodeEdges = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	_nodeGraph = mSceneMgr->getRootSceneNode()->createChildSceneNode();

	//Creo el manualObject que dibuixar� el CPG
	manualGraph = mSceneMgr->createManualObject();
	_nodeGraph->attachObject(manualGraph);

	//Creo el manualObject que dibuixar� totes les arestes de la geometria
	manualGeometryEdges = mSceneMgr->createManualObject();
	_nodeEdges->attachObject(manualGeometryEdges);

	//Creo el manualObject que dibuixar� totes les arestes de la geometria
	manualSharedEdges = mSceneMgr->createManualObject();
	_nodeEdges->attachObject(manualSharedEdges);

	//Creo el manualObject que dibuixar� totels portals
	manualPortals = mSceneMgr->createManualObject();
	_nodeEdges->attachObject(manualPortals);

	rGreen = 0;	rBlue = 0;
	gRed = 0;	gBlue = 0;
	bRed = 0;	bGreen = 0;
	mGreen = 0;
	yBlue = 0;
	cRed = 0;
	wRed = 0;	wGreen = 0;	wBlue = 0;
	currentColourType = EDGE_COLOUR_RED;
	Ogre::RGBA rgbaBG = Ogre::ColourValue::White.getAsRGBA();
	colourTable[rgbaBG] = -1;
}

void NavMesh::clearCellsToRepair() {
	cellsToRepair.clear();
}

#pragma endregion

#pragma region GET AND SET

int NavMesh::addVertex(Vec3D& v, bool isShared, int id) {
	//Find a valid ID for the vertex
	int idVert;
	if (id >= 0) idVert = id;
	else {
		while (vertTable.find(newVertID) != vertTable.end()) {
			newVertID++;
		}
		idVert = newVertID++;
	}
	Vertex nmV(v);
	nmV.setID(idVert);
	vertTable[idVert] = nmV;
	if (isShared) sharedVertices.insert(idVert);
	return idVert;
}

void NavMesh::eraseSharedVertex(int idVert) {
	sharedVertices.erase(idVert);
}

int NavMesh::addSubEdge(int idP, int idV1, int idV2, Edge::Type eType) {
	//Creo la nova aresta
	int idEdge = newEdgeID;
	Edge nmE;
	nmE.id = idEdge;
	nmE.v1 = idV1;
	nmE.v2 = idV2;
	nmE.cellID = idP;
	nmE._type = eType;
	nmE.clearance2 = 0.0f;
	nmE.isShared = false;
	nmE.opposite = -1;
	nmE.next = -1;
	nmE.manual = mSceneMgr->createManualObject("manualEdge" + std::to_string(idEdge));
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(nmE.manual);
	nmE.material = Ogre::MaterialManager::getSingletonPtr()->getByName("matEdgeGPU");

	//Add the new edge to the corresponding tables
	edgeTable[idEdge] = nmE;
	if (eType == Edge::PORTAL) _portalTable.insert(idEdge);
	vertsToEdgeTable[EdgeKey(idV1, idV2)] = idEdge;
	
	//Indico que els extrems de l'aresta tenen a aquesta aresta com a aresta incident
	addEdgePointer(idV1, idEdge);
	addEdgePointer(idV2, idEdge);
	addSharedEdge(idEdge);
	createEdgeColour(idEdge);
	newEdgeID++;
	return idEdge;
}

void NavMesh::addSharedEdge(int idEdge) {
	//Afegeix l'aresta a la taula de possibles arestes compartides si algun dels seus extrems 
	//est� marcat com a possible v�rtex compartit
	Edge& e = getEdge(idEdge);
	if (e.isPortal()) return;	//Un portal no pot ser una aresta compartida entre dues layers
	Vertex& v1 = getVertex(e.v1);
	Vertex& v2 = getVertex(e.v2);
	if (v1.isShared() || v2.isShared()) {
		e.isShared = true;
		sharedEdges.insert(idEdge);
	}
}

void NavMesh::addEdgePointer(int idVert, int idEdge) {
	Vertex& nmV = getVertex(idVert);
	size_t numIncidentEdges = nmV.getNumIncidentEdges();
	if (numIncidentEdges == 0) {
		//Encara no hem afegit cap aresta a la llista d'arestes
		nmV.addIncidentEdgeBack(idEdge);
	}
	else if (numIncidentEdges == 1) {
		//Hem afegit com a m�nim una aresta. Vull que la primera sigui l'aresta IN
		//i la segona l'aresta OUT
		Edge& currentEdge = edgeTable[nmV.getIncidentEdge(0)];
		if (currentEdge.v1 == nmV.getID()) {
			//L'aresta que ja estava afegida �s la OUT. La intercanvio
			nmV.addIncidentEdgeFront(idEdge);
		}
		else {
			//L'aresta que ja estava afegida �s la IN
			nmV.addIncidentEdgeBack(idEdge);
		}
	}
	else {
		//Miro en quina posici� del vector s'ha de col�locar la nova aresta (que �s un portal)
		//Ogre::LogManager::getSingletonPtr()->logMessage("MORE THAN 2 EDGES!");
		if (referenceEdge(idVert, idEdge)) {
			//idEdge �s la nova aresta de refer�ncia
			nmV.addIncidentEdgeFront(idEdge);
		}
		else {
			float angleE1E2, angleE1ENew;
			Edge e1;
			Edge e2;
			IncidentEdgesList::iterator it = nmV.getIncidentEdges().begin();
			bool found = false;
			while (!found && (it < (nmV.getIncidentEdges().end() - 1))) {
				//Comprovo entre quin parell d'arestes cau la nova aresta
				e1 = edgeTable[*it];
				e2 = edgeTable[*(it + 1)];
				angleE1E2 = angleBetweenEdges(nmV.getID(), e1.id, e2.id);	//Angle entre les arestes e1 i e2
				angleE1ENew = angleBetweenEdges(nmV.getID(), e1.id, idEdge);	//Angle entre les arestes e1 i l'aresta nova
				//Ogre::LogManager::getSingletonPtr()->logMessage("angleE1E2 = " + std::to_string(angleE1E2));
				//Ogre::LogManager::getSingletonPtr()->logMessage("angleE1ENew = " + std::to_string(angleE1ENew));
				found = ((angleE1ENew <= angleE1E2) || Ogre::Math::RealEqual(angleE1ENew, angleE1E2));	//L'aresta nova est� entre e1 i e2?
				if (!found) it++;
			}
			if (found) {
				//He trobat la parella d'arestes entre la qual es troba la nova aresta
				//L'hem de posar entre la posici� (i, i+1)
				//Ogre::LogManager::getSingletonPtr()->logMessage("NEW EDGE BETWEEN 2 EDGES");
				//Ogre::LogManager::getSingletonPtr()->logMessage("e1 = " + std::to_string(e1.id));
				//Ogre::LogManager::getSingletonPtr()->logMessage("e2 = " + std::to_string(e2.id));
				nmV.addIncidentEdgeAt(idEdge, it + 1);
			}
			else {
				nmV.addIncidentEdgeBack(idEdge);
			}
		}
	}
	computeConcEdges(idVert);
}

bool NavMesh::referenceEdge(int idVert, int idEdge) {
	//Indica si idEdge �s l'aresta de refer�ncia per ordenar totes les arestes incidents a idVert
	Edge& newEdge = getEdge(idEdge);
	if (newEdge.v1 == idVert) return false;	//No pot ser aresta de refer�ncia perqu� NO t� com a punt dest� el v�rtex idVert
	
	//Si arribo aqu� �s perqu� �s una aresta IN respecte el vector idVert. 
	//Comprobo si �s l'aresta de refer�ncia. 
	IncidentEdgesList& edges = getVertex(idVert).getIncidentEdges();
	int currentRefEdge = edges.front();	//L'aresta de refer�ncia actual
	int lastEdge = edges.back();		//L'aresta m�s llunyana respecte l'aresta de refer�ncia actual
	float angleCurrentLast = angleBetweenEdges(idVert, currentRefEdge, lastEdge);
	float angleNewLast = angleBetweenEdges(idVert, idEdge, lastEdge);

	return (angleNewLast > angleCurrentLast);
}

bool NavMesh::computeConcEdges(int idVert) {
	Vertex& nmV = getVertex(idVert);
	if (nmV.getNumIncidentEdges() <= 1) return false;

	//Si arribo aqu� �s perqu� com a m�nim, tinc dues arestes incidents al notch. 
	//Comprobo si hi ha una parella d'arestes consecutives que formi un angle c�ncau. 
	float angleE1E2;
	Edge e1;
	Edge e2;
	IncidentEdgesList& incidentEdges = nmV.getIncidentEdges();
	IncidentEdgesList::iterator it = incidentEdges.begin();
	bool found = false;
	float concAngle = 185.0f;
	while (!found && (it < (incidentEdges.end() - 1))) {
		//Comprovo entre quin parell d'arestes cau la nova aresta
		e1 = edgeTable[*it];
		e2 = edgeTable[*(it + 1)];
		angleE1E2 = angleBetweenEdges(nmV.getID(), e1.id, e2.id);	//Angle entre les arestes e1 i e2
		found = ((angleE1E2 >= concAngle) || Ogre::Math::RealEqual(angleE1E2, concAngle));	//La parella (e1, e2) forma un angle c�ncau?
		if (!found) it++;
	}
	if (found) {
		//La parella (e1, e2) formen un angle c�ncau
		//Vull que concEdge1 sigui l'aresta m�s llunyana (en gir) respecte l'aresta de refer�ncia. 
		//Aix� far� que al calcular les arestes de l'�rea d'inter�s, sigui la m�s propera a l'aresta de refer�ncia. 
		nmV.setConcEdge1(*(it + 1));
		nmV.setConcEdge2(*it);
	}
	return found;
}

std::pair<int, int> NavMesh::subdivideEdge(int oldEdge, int idNewVert) {
	//Subdivideix una aresta en dos subarestes pel punt idNewVert
	//Ogre::LogManager::getSingletonPtr()->logMessage("SUBDIVIDING EDGE");
	//Ogre::LogManager::getSingletonPtr()->logMessage("oldEdge = " + std::to_string(oldEdge));
	//Ogre::LogManager::getSingletonPtr()->logMessage("idNewVert = " + std::to_string(idNewVert));
	//L'aresta a subdividir (ceBegin, ceEnd)
	int ceBegin = getEdge(oldEdge).v1;	//El punt inicial de l'aresta antiga
	int ceEnd = getEdge(oldEdge).v2;	//El punt final de l'aresta antiga
	int ceCellID = getEdge(oldEdge).cellID;

	//Creo una aresta nova (ceBegin, idNewVert)
	int idE1 = addSubEdge(ceCellID, ceBegin, idNewVert);
	//Ogre::LogManager::getSingletonPtr()->logMessage("CREATING NEW EDGE 1 = " + std::to_string(idE1));
	redoConcEdge(ceBegin, oldEdge, idE1);	//Pot ser que l'aresta que hagi subdividit sigui la concedge del v�rtex. Haig de canviar el punter

	//Creo una aresta nova (idNewVert, ceEnd)
	int idE2 = addSubEdge(ceCellID, idNewVert, ceEnd);
	//Ogre::LogManager::getSingletonPtr()->logMessage("CREATING NEW EDGE 2 = " + std::to_string(idE2));
	redoConcEdge(ceEnd, oldEdge, idE2);		//Pot ser que l'aresta que hagi subdividit sigui la concedge del v�rtex. Haig de canviar el punter

	//Elimino l'aresta antiga
	eraseEdge(oldEdge);
	return (std::pair<int, int>(idE1, idE2));
}

void NavMesh::redoConcEdge(int idVert, int oldEdge, int newEdge) {
	if (getVertex(idVert).getConcEdge1() == oldEdge) {
		getVertex(idVert).setConcEdge1(newEdge);
	}
	else if (getVertex(idVert).getConcEdge2() == oldEdge) {
		getVertex(idVert).setConcEdge2(newEdge);
	}
}

bool NavMesh::isNonEssentialPortal(int idPortal) {
	Edge& portal = getEdge(idPortal);
	bool nonEssentialV1 = isNonEssentialPortal(idPortal, portal.v1);
	bool nonEssentialV2 = isNonEssentialPortal(idPortal, portal.v2);
	return (nonEssentialV1 && nonEssentialV2);
}

bool NavMesh::isNonEssentialPortal(int idPortal, int idVert) {
	//Ogre::LogManager::getSingletonPtr()->logMessage("CHECKING NON ESSENTIAL PORTAL = " + std::to_string(idPortal));
	//Ogre::LogManager::getSingletonPtr()->logMessage("VERT = " + std::to_string(idVert));
	//Indica si el portal idPortal �s prescindible respecte el v�rtex idVert
	Vertex& v = getVertex(idVert);
	Edge& portal = getEdge(idPortal);
	if (!portal.isPortal()) return false;
	//Si arribo aqu� �s perqu� l'aresta indicada realment �s un portal
	if (!(v.isNotch())) {
		//Si �s un v�rtex convex, evidentment �s prescindible
		return true;
	}
	else {
		//Ogre::LogManager::getSingletonPtr()->logMessage("CONCAVE VERTEX");
		//�s un v�rtex c�ncau...
		//busco quina posici� ocupa el portal idPortal en el vector d'arestes de idVert
		bool found = false;
		size_t i = 1;
		//Ogre::LogManager::getSingletonPtr()->logMessage("NUM INCIDENT EDGES = " + std::to_string(v.edges.size()));
		IncidentEdgesList& edges = v.getIncidentEdges();
		while (!found && (i < (edges.size() - 1))) {
			found = (edges.at(i) == idPortal);
			//Ogre::LogManager::getSingletonPtr()->logMessage("incidentEdge = " + std::to_string(v.edges.at(i)));
			//Ogre::LogManager::getSingletonPtr()->logMessage("idPortal = " + std::to_string(idPortal));
			//Ogre::LogManager::getSingletonPtr()->logMessage("found = " + std::to_string(found));
			if (!found) i++;
		}
		//Al sortir del bucle, la i indica la posici� del portal en el vector d'arestes
		int prevEdgeID, nextEdgeID;
		//Calculo quina �s l'aresta posterior i anterior al portal. 
		//Calculo l'aresta ANTERIOR
		if (portal.opposite == edges.at(i - 1)) {
			//L'aresta immediatament anterior �s la subaresta en sentit contrari d'aquest portal. Per tant, 
			//haig d'agafar l'anterior de l'anterior. 
			prevEdgeID = edges.at(i - 2);
		}
		else {
			prevEdgeID = edges.at(i - 1);
		}

		//Calculo l'aresta POSTERIOR
		if (portal.opposite == edges.at(i + 1)) {
			//L'aresta immediatament posterior �s la subaresta en sentit contrari d'aquest portal. Per tant, 
			//haig d'agafar la posterior de la posterior. 
			nextEdgeID = edges.at(i + 2);
		}
		else {
			nextEdgeID = edges.at(i + 1);
		}

		float angle = angleBetweenEdges(idVert, prevEdgeID, nextEdgeID);
		return (angle <= (180.0f + alphaThreshold));
	}
	//Ogre::LogManager::getSingletonPtr()->logMessage("NON ESSENTIAL CHECKED");
}

void NavMesh::eraseEdge(int idEdge) {
	//Elimina l'aresta idEdge
	//Ogre::LogManager::getSingletonPtr()->logMessage("ERASING EDGE = " + std::to_string(idEdge));
	//Els v�rtexs inicial i final ja no tenen aquesta aresta com a incident
	eraseEdgePointer(idEdge);

	//Ogre::LogManager::getSingletonPtr()->logMessage("ERASING MANUAL");
	//Elimino el seu objecte gr�fic
	if (mSceneMgr->hasManualObject("manualEdge" + std::to_string(idEdge))) {
		edgeTable[idEdge].manual->clear();
		mSceneMgr->destroyManualObject(edgeTable[idEdge].manual);
	}
	//Ogre::LogManager::getSingletonPtr()->logMessage("MANUAL ERASED");

	//La trec de la taula d'arestes
	Edge& e = getEdge(idEdge);
	if (e.isShared) sharedEdges.erase(idEdge);
	vertsToEdgeTable.erase(EdgeKey(e.v1, e.v2));
	_portalTable.erase(idEdge);
	edgeTable.erase(idEdge);
	//Ogre::LogManager::getSingletonPtr()->logMessage("EDGE ERASED");
}

void NavMesh::eraseVertex(int idVert) {
	//Elimina el v�rtex idVert
	if (getVertex(idVert).isShared()) {
		sharedVertices.erase(idVert);
	}
	vertTable.erase(idVert);
}

void NavMesh::eraseEdgePointer(int idEdge) {
	Edge& edge = getEdge(idEdge);
	Vertex& v1 = getVertex(edge.v1);
	Vertex& v2 = getVertex(edge.v2);
	v1.removeIncidentEdge(idEdge);
	v2.removeIncidentEdge(idEdge);
	eraseEdgePointerCell(edge.cellID, idEdge);
}

void NavMesh::eraseEdgePointerCell(int idCell, int idEdge) {
	//elimina l'aresta idEdge de la taula de punters de la cel�la idCell
	//Comprobo si la cel�la existeix
	CellTable::iterator it = cellTable.find(idCell);
	if (it == cellTable.end()) return;	//La cel�la no existeix
	
	//Si arribo aqu� �s perqu� la cel�la existeix
	Cell& c = getCell(idCell);
	c.edges.erase(idEdge);
	c.portals.erase(idEdge);
}

int NavMesh::addCell(std::vector<Vertex>& lVert, std::vector<int>& edges) {

	int idCell = newCellID;
	Cell c;
	c.id = idCell;
	c.p = new Poly(lVert, true);

	//guardo els identificadors dels portals
	std::vector<int>::iterator it = edges.begin();
	std::vector<int>::iterator itNext = it + 1;
	Edge* e;
	size_t i = 0;
	int currentEdgeID, nextEdgeID, prevEdgeID;
	size_t numEdges = edges.size();
	while (i < numEdges) {
		//Identificador de l'aresta anterior a l'actual
		if (i == 0) prevEdgeID = edges[numEdges - 1];
		else prevEdgeID = edges[i - 1];

		//Identificador de l'aresta actual
		currentEdgeID = edges[i];

		//Identificador de l'aresta posterior a l'actual
		if (i == (numEdges - 1)) nextEdgeID = edges[0];
		else nextEdgeID = edges[i + 1];

		//Construeixo els punters de l'aresta actual
		e = &getEdge(currentEdgeID);
		e->next = nextEdgeID;
		e->prev = prevEdgeID;

		//Afegeixo l'aresta actual a la llista d'arestes de la cel�la
		c.edges.insert(currentEdgeID);
		if (e->isPortal()) c.portals.insert(currentEdgeID);
		setEdgeCellPointer(currentEdgeID, idCell);

		i++;
	}
	/*for (std::vector<int>::iterator it = edges.begin(); it != edges.end(); it++, itNext++) {
		c.edges.insert(*it);			//Apilo l'aresta a la llista d'arestes que conformen la cel�la
		e = &(getEdge(*it));
		if (itNext != edges.end()) e->next = *itNext;
		else e->next = *(edges.begin());
		if (e->isPortal) {
			c.portals.insert(*it);	//Si �s portal, apilo l'aresta a la llista d'arestes portals de la cel�la
		}
		setEdgeCellPointer(*it, idCell);
	}*/

	cellTable[idCell] = c;
	newCellID++;
	return idCell;
}

void NavMesh::eraseCell(int idCell) {
	//Elimina la cel�la idCell de la taula de cel�les
	if (cellTable.find(idCell) == cellTable.end()) return;
	//Recorro totes les cel�les adjacents i borro el punter cap a idCell
	Cell* c = &getCell(idCell);
	Cell* cNeighbor;
	for (std::set<int>::iterator it = c->adjacentTo.begin(); it != c->adjacentTo.end(); it++) {
		cNeighbor = &getCell(*it);
		cNeighbor->adjacentTo.erase(idCell);
	}
	cellTable.erase(idCell);
}

void NavMesh::addAdjacency(int c1, int c2) {
	//Relaciona les cel�les c1 i c2 
	cellTable.at(c1).adjacentTo.insert(c2);
	cellTable.at(c2).adjacentTo.insert(c1);
}

bool NavMesh::checkCellClearance(int cellID, Vec3D& position, float clearance) {
	std::set<int>& edges = getCell(cellID).edges;
	Edge* e;
	Vec2D v1Edge;
	Vec2D v2Edge;
	Vec2D goalPosition2D = GeometrySolver::to2D(position);
	IntersectionPoint2D projection;
	std::set<int>::iterator itEdge = edges.begin();
	bool intersection = false;
	while (!intersection && (itEdge != edges.end())) {
		e = &(getEdge(*itEdge));
		if (!e->isPortal()) {
			v1Edge = getVertex(e->v1).getPosition2D();
			v2Edge = getVertex(e->v2).getPosition2D();
			GeometrySolver::projectionPointSegment2D(goalPosition2D, v1Edge, v2Edge, projection);
			intersection = (goalPosition2D.dist2(projection.location) < (clearance * clearance));
		}
		itEdge++;
	}
	return !intersection;
}

std::set<int>& NavMesh::getSharedVertices() {
	return sharedVertices;
}

void NavMesh::setSharedVertices(std::set<int>& shV) {
	sharedVertices = shV;
}

std::set<int>& NavMesh::getSharedEdges() {
	return sharedEdges;
}

void NavMesh::setSharedEdges(std::set<int>& shE) {
	sharedEdges = shE;
}

VertTable& NavMesh::getVertTable() {
	return vertTable;
}

size_t NavMesh::getNumVertices() {
	return vertTable.size();
}

size_t NavMesh::getNumEdges() {
	return edgeTable.size();
}

size_t NavMesh::getNumCells() {
	return cellTable.size();
}

Vertex& NavMesh::getVertex(int idVertex) {
	return vertTable.at(idVertex);
}

Edge& NavMesh::getEdge(int idEdge) {
	return edgeTable.at(idEdge);
}

Edge& NavMesh::getEdge(int idV1, int idV2) {
	int idEdge = vertsToEdgeTable[EdgeKey(idV1, idV2)];
	return getEdge(idEdge);
}

int NavMesh::getEdgeID(Ogre::ColourValue& color) {
	return colourTable[color.getAsRGBA()];
}

EdgeTable& NavMesh::getEdgeTable() {
	return edgeTable;
}

Cell& NavMesh::getCell(int idCell) {
	return cellTable.at(idCell);
}

Cell& NavMesh::getCellByPortal(int idPortal) {
	//Obt� la cel�la a la que s'arriba pel portal idPortal
	Edge& portal = getEdge(idPortal);
	Edge& opposite = getEdge(portal.opposite);
	return getCell(opposite.cellID);
}

CellTable& NavMesh::getCellTable() {
	return cellTable;
}

std::set<int>& NavMesh::getCellsToRepair() {
	return cellsToRepair;
}

void NavMesh::setEdgeCellPointer(int edgeID, int cellID) {
	edgeTable[edgeID].cellID = cellID;
}

int NavMesh::getPortalTo(int cA, int cB) {
	//Indica quin portal de cA haig d'agafar per anar a la cel�la cB
	std::set<int>& portals = getCell(cA).portals;
	std::set<int>::iterator it;
	Edge* port; 
	Edge* opposite;
	for (it = portals.begin(); it != portals.end(); it++) {
		port = &getEdge(*it); //El portal
		opposite = &getEdge(port->opposite); //El portal en sentit oposat
		if (opposite->cellID == cB) {
			//Si el portal oposat pertany a la cel�la cB, �s que
			//per anar de cA a cB haig de passar pel portal port
			break;
		}
	}
	if (it == portals.end()) {
		//ERROR!!! No hi ha portal que connecti cA i cB
		return -1;
	}
	return *it;
}

int NavMesh::getRandomCell() {
	//Retorna una cel�la de la NavMesh a l'atzar
	int r = rand() % cellTable.size();
	CellTable::iterator itCell = cellTable.begin();
	std::advance(itCell, r);
	return itCell->first;
}

#pragma endregion

/****************************************************************/
/* CONSULTORES */
/****************************************************************/
int NavMesh::existEdge(int idV1, int idV2) {
	//Indica si existeix l'aresta amb punt inicial idV1 i punt final idV2
	
	//retorna l'identificador de l'aresta; retorna -1 altrament
	IncidentEdgesList& edgesV1 = getVertex(idV1).getIncidentEdges();	//Les arestes incidents a v1
	IncidentEdgesList::iterator it = edgesV1.begin();
	bool exist = false;
	int idEdge = -1;
	while (!exist && (it != edgesV1.end())) {
		exist = (edgeTable[*it].v2 == idV2);
		if (exist) idEdge = *it;	//L'aresta existeix. Guardo el seu identificador
		it++;
	}
	if (idEdge != -1) return idEdge;

	//Si arribo aqu� �s perqu� l'aresta (idV1, idV2) no existeix. Comprobo
	//si existeix l'aresta (idV2, idV1);
	IncidentEdgesList& edgesV2 = getVertex(idV2).getIncidentEdges();	//Les arestes incidents a v2
	it = edgesV2.begin();
	exist = false;
	idEdge = -1;
	while (!exist && (it != edgesV2.end())) {
		exist = (edgeTable[*it].v2 == idV1);
		if (exist) idEdge = *it;	//L'aresta existeix. Guardo el seu identificador
		it++;
	}

	return idEdge;
}

/****************************************************************/
/* AUXILIARS */
/****************************************************************/
float NavMesh::angleBetweenEdges(int origin, int idE1, int idE2) {
	//Calcula l'angle que hi ha entre 2 arestes orientades. S'ha d'entendre l'angle
	//com al GIR CAP A L'ESQUERRA de l'aresta e1 a l'aresta e2

	//Origin �s el punt d'origen en el qual posem les arestes per calcular l'angle
	Edge& e1 = getEdge(idE1);
	Edge& e2 = getEdge(idE2);
	
	Vec2D e1Begin = getVertex(e1.v1).getPosition2D(NEOGEN_PROJECTION_PLANE);
	Vec2D e1End = getVertex(e1.v2).getPosition2D(NEOGEN_PROJECTION_PLANE);
	Vec2D e1Dir;
	//Vector de l'aresta e1
	if (e1.v1 == origin) e1Dir = e1End - e1Begin;
	else e1Dir = e1Begin - e1End; 

	Vec2D e2Begin = getVertex(e2.v1).getPosition2D(NEOGEN_PROJECTION_PLANE);
	Vec2D e2End = getVertex(e2.v2).getPosition2D(NEOGEN_PROJECTION_PLANE);
	Vec2D e2Dir;
	//Vector de l'aresta e2
	if (e2.v1 == origin) e2Dir = e2End - e2Begin; 
	else e2Dir = e2Begin - e2End;
	
	return GeometrySolver::angleBetweenVectors(e1Dir, e2Dir);
}

bool NavMesh::isSharedVertex(int idVertex) {
	//Indica si el v�rtex amb idVertex es troba a la llista de sharedVertices
	std::set<int>::iterator it = sharedVertices.find(idVertex);
	return (it != sharedVertices.end());
}

void NavMesh::fusionSharedVertices(std::set<int>& sharedVerticesDest, std::set<int>& sharedEdgesDest, std::set<int>& sharedVerticesSource, std::set<int>& sharedEdgesSource, float maxD) {
	//Fusiona els v�rtexs que estan compartits amb la navMesh nm
	Ogre::LogManager::getSingletonPtr()->logMessage("===================================================================");
	Ogre::LogManager::getSingletonPtr()->logMessage("FUSIONING SHARED VERTICES");
	Ogre::LogManager::getSingletonPtr()->logMessage("===================================================================");
	sewMesh(sharedVerticesDest, sharedEdgesDest, sharedVerticesSource, sharedEdgesSource, maxD);
	Ogre::LogManager::getSingletonPtr()->logMessage("SHARED VERTICES FUSIONED");
}

void NavMesh::sewMesh(std::set<int>& sharedVerticesDest, std::set<int>& sharedEdgesDest, std::set<int>& sharedVerticesSource, std::set<int>& sharedEdgesSource, float maxD) {
	Edge* edge;
	float distProj;
	Vec3D proj;
	int closestEdgeID;
	int sharedVertexID; //El id del v�rtex de nm que en coordenades de p�xel, �s accessibles des del v�rtex actual
	std::pair<int, int> subEdges;
	std::set<std::pair<int, int>> pairsToFusion;	//Guarda les parelles de v�rtexs que s'han de fusionar
	//Recorro tots els sharedVErticesDest i obtinc la sharedEdgeSource m�s propera
	IntersectionPoint3D result;
	for (std::set<int>::iterator itDest = sharedVerticesDest.begin(); itDest != sharedVerticesDest.end(); itDest++) {
		Ogre::LogManager::getSingletonPtr()->logMessage("================================================");
		Ogre::LogManager::getSingletonPtr()->logMessage("vertID = " + std::to_string(*itDest));
		closestEdgeID = findClosestSharedEdge(*itDest, maxD, sharedEdgesSource);
		Vec3D v = getVertex(*itDest).getPosition();
		Ogre::LogManager::getSingletonPtr()->logMessage("closestEdgeID = " + std::to_string(closestEdgeID));
		if (closestEdgeID != -1) {
			//Comprovo si algun dels extrems de l'aresta est� prou proper al v�rtex
			sharedVertexID = -1;
			edge = &getEdge(closestEdgeID);
			Vec3D eV1 = getVertex(edge->v1).getPosition();
			Vec3D eV2 = getVertex(edge->v2).getPosition();
			if (v.dist(eV1) <= maxD) {
				sharedVertexID = edge->v1;
			}
			else if (v.dist(eV2) <= maxD) {
				sharedVertexID = edge->v2;
			}
			else {
				//Calculo la projecci� del punt sobre la recta definida per l'aresta closestEdgeID. 
				//IMPORTANT: La projecci� la calculo en el pl� XY
				GeometrySolver::projectionPointSegment3D(v, eV1, eV2, result);
				distProj = v.dist(proj);
				if ((distProj <= maxD) && result.isValid) {
					//Ogre::LogManager::getSingletonPtr()->logMessage("NEW VERTEX REQUIRED!");
					sharedVertexID = addVertex(proj);
					subEdges = subdivideEdge(closestEdgeID, sharedVertexID);
					sharedEdgesSource.erase(closestEdgeID);
					sharedEdgesSource.insert(subEdges.first);
					sharedEdgesSource.insert(subEdges.second);
				}
			}
			Ogre::LogManager::getSingletonPtr()->logMessage("sharedVertexID = " + std::to_string(sharedVertexID));
			if (sharedVertexID != -1) {
				//�s un sharedVertex. Cal comprobar si �s un v�rtex accessible des del 
				//v�rtex v
				//Moc el v�rtex sharedVertexID a la posici� on est� el v�rtex de dest�
				Vertex vShared = getVertex(sharedVertexID);
				vShared.setPosition(v);
				pairsToFusion.insert(std::pair<int, int>(*itDest, sharedVertexID));
			}
		}
	}
	//Recorro totes les parelles de v�rtexs que s'han de fusionar i les fusiono
	VertEquivalence vEquivalence;
	VertEquivalence::iterator itFound;
	int idVertDest, idVertSource, oldVertDest;
	for (std::set<std::pair<int, int>>::iterator it = pairsToFusion.begin(); it != pairsToFusion.end(); it++) {
		idVertDest = it->first;
		idVertSource = it->second;
		itFound = vEquivalence.find(idVertSource);
		if (itFound != vEquivalence.end()) {
			//El v�rtex idVertSource ja s'ha fusionat pr�viament amb un altre v�rtex. 
			Ogre::LogManager::getSingletonPtr()->logMessage("SOURCE VERTEX ALREADY FUSIONED! = " + std::to_string(idVertSource));
			oldVertDest = idVertDest;
			idVertDest = vEquivalence[idVertSource];
			idVertSource = oldVertDest;
			sharedVerticesDest.erase(oldVertDest);
		}
		fusionVertices(idVertDest, sharedEdgesDest, idVertSource, sharedEdgesSource);
		vEquivalence[idVertSource] = idVertDest;	//Indico que idVertSource s'ha fusionat a idVertDest
		eraseVertex(idVertSource);
		sharedVerticesDest.erase(idVertDest);
		sharedVerticesSource.erase(idVertSource);
	}
}

int NavMesh::findClosestSharedEdge(int idVert, float maxD, std::set<int>& sharedEdgesSource) {
	//Busca l'aresta potencialment compartida de nm m�s propera al v�rtex idVert, sense superar la dist�ncia maxD
	Ogre::LogManager::getSingletonPtr()->logMessage("FINDING CLOSEST SHARED EDGE");
	float bestD = maxD * 2.0f;
	float currentD;
	int closestEdge = -1;
	Edge* e;
	for (std::set<int>::iterator it = sharedEdgesSource.begin(); it != sharedEdgesSource.end(); it++) {
		//Ogre::LogManager::getSingletonPtr()->logMessage("edgeID = " + std::to_string(*it));
		e = &getEdge(*it);
		//Ogre::LogManager::getSingletonPtr()->logMessage("eOK");
		Vec3D v = getVertex(idVert).getPosition();
		//Ogre::LogManager::getSingletonPtr()->logMessage("vOK");
		Vec3D eBegin = getVertex(e->v1).getPosition();
		Vec3D eEnd = getVertex(e->v2).getPosition();
		currentD = GeometrySolver::pointSegmentDistance(&v, &eBegin, &eEnd);
		if ((currentD < bestD) && (currentD < maxD)) {
			//He trobat una aresta m�s propera
			closestEdge = *it;
			bestD = currentD;
		}
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("CLOSEST SHARED EDGE FOUND");
	return closestEdge;
}

void NavMesh::fusionVertices(int idVertDest, std::set<int>& sharedEdgesDest, int idVertSource, std::set<int>& sharedEdgesSource) {
	//idVertDest i idVertSource s�n dos v�rtexs equivalents. Eliminarem idVertSource
	//i mantindrem idVertDest
	Ogre::LogManager::getSingletonPtr()->logMessage("FUSIONING VERTICES");
	Ogre::LogManager::getSingletonPtr()->logMessage("idVertDest = " + std::to_string(idVertDest));
	Ogre::LogManager::getSingletonPtr()->logMessage("idVertSource = " + std::to_string(idVertSource));
	int joiningEdge = existEdge(idVertDest, idVertSource);
	if (joiningEdge != -1) {
		Ogre::LogManager::getSingletonPtr()->logMessage("EDGE JOINING DEST AND SOURCE EXIST!");
		eraseEdge(joiningEdge);
		sharedEdgesDest.erase(joiningEdge);
		sharedEdgesSource.erase(joiningEdge);
	}
	Vertex& vertDest = getVertex(idVertDest);
	Vertex& vertSource = getVertex(idVertSource);
	IncidentEdgesList oldDestEdges = vertDest.getIncidentEdges();		//Les arestes incidents al vertDest ABANS de fer la fusi�
	IncidentEdgesList& sourceEdges = vertSource.getIncidentEdges();	//Les arestes incidents al vertSource
	int oppositeID;
	Edge* currentEdge;
	Edge* oppositeEdge;
	//Totes les arestes incidents a vertSource passen a ser arestes incidents al vertDest
	for (std::deque<int>::iterator it = sourceEdges.begin(); it != sourceEdges.end(); it++) {
		replaceVertPointer(*it, idVertSource, idVertDest);	//L'extrem de l'aresta que apuntava a idVertSource, passa a apuntar a idVertDest
		addEdgePointer(idVertDest, *it);	//Afegeixo l'aresta a la llista d'arestes incidents a idVertDest
		//Comprobo si l'aresta que he afegit t� una aresta oposada en la llista d'arestes incidents a idVertDest
		oppositeID = findOppositeEdge(*it, oldDestEdges);	
		if (oppositeID != -1) {
			//Existeix una aresta oposada a l'aresta actual. Per tant, les dues arestes
			//en realitat s�n dues sub-arestes d'un mateix portal. 
			/*Ogre::LogManager::getSingletonPtr()->logMessage("OPPOSITE EDGE FOUND");
			Ogre::LogManager::getSingletonPtr()->logMessage("currentEdge = " + std::to_string(*it));
			Ogre::LogManager::getSingletonPtr()->logMessage("oppositeEdge = " + std::to_string(oppositeID));*/
			currentEdge = &(getEdge(*it));
			oppositeEdge = &(getEdge(oppositeID));
			
			//Aquestes arestes passen a ser portals
			currentEdge->_type = Edge::PORTAL;	
			oppositeEdge->_type = Edge::PORTAL;

			//l'una �s l'oposada de l'altre
			currentEdge->opposite = oppositeID;
			oppositeEdge->opposite = *it;

			//Afegeixo la relaci� d'adjac�ncia entre les dues cel�les
			addAdjacency(currentEdge->cellID, oppositeEdge->cellID);
		}
	}
	tJoints.insert(idVertDest);
}

int NavMesh::findOppositeEdge(int idEdge, std::deque<int>& edgeList) {
	//Indica si l'aresta idEdge t� una aresta oposada (mateixos extrems per� en sentit oposat)
	//en la llista edgeList. 
	//Si existeix aresta oposada, retorna el seu ID;
	//altrament retorna -1;
	int eBegin = getEdge(idEdge).v1;
	int eEnd = getEdge(idEdge).v2;
	int oppositeID = -1;
	std::deque<int>::iterator it = edgeList.begin();
	while ((oppositeID == -1) && (it != edgeList.end())) {
		if ((getEdge(*it).v1 == eEnd) && (getEdge(*it).v2 == eBegin)) {
			//He trobat una aresta amb els mateixos extrems per� sentit oposat
			oppositeID = *it;
		}
		it++;
	}
	return oppositeID;
}

void NavMesh::fusionEdges(int idEdgeDestination, int idEdgeSource) {
	//idEdgeDestination i idEdgeSource representen dues sub-arestes orientades d'una mateixa
	//aresta. 
	//* idEdgeDestination �s el ID de l'aresta resultat de la fusi�. 
	//* idEdgeSource �s el ID de l'aresta que es perdr� durant el proc�s de fusi�
	Edge& edgeDestination = getEdge(idEdgeDestination);
	Edge& edgeSource = getEdge(idEdgeSource);

	//Aquestes arestes passen a ser portals
	edgeDestination._type = Edge::PORTAL;
	edgeSource._type = Edge::PORTAL;

	//Aquestes arestes en realitat, s�n dues sub-arestes orientades que conformen
	//una aresta. Per tant, l'una �s l'oposada de l'altre
	edgeDestination.opposite = idEdgeSource;
	edgeSource.opposite = idEdgeDestination;

	//Afegeixo la relaci� d'adjac�ncia entre les dues cel�les
	addAdjacency(edgeDestination.cellID, edgeSource.cellID);

	//Els extrems de l'aresta idEdgeSource es substitueixen pels extrems de l'aresta idEdgeDestination
	int eSourceV1 = edgeSource.v1;
	int eSourceV2 = edgeSource.v2;
	
	//Les arestes incidents al v�rtex INICIAL de l'aresta idEdgeSource, passen a ser arestes
	//incidents al v�rtex FINAL de l'aresta idEdgeDestination
	IncidentEdgesList& edgesV1 = getVertex(edgeSource.v1).getIncidentEdges();	//Les arestes incidents al v�rtex INICIAL de edgeSource
	for (std::deque<int>::iterator it = edgesV1.begin(); it != edgesV1.end(); it++) {
		replaceVertPointer(*it, edgeSource.v1, edgeDestination.v2);
		addEdgePointer(edgeDestination.v2, *it);
	}

	//Les arestes incidents al v�rtex FINAL de l'aresta idEdgeSource, passen a ser arestes
	//incidents al v�rtex FINAL de l'aresta idEdgeDestination
	IncidentEdgesList& edgesV2 = getVertex(edgeSource.v2).getIncidentEdges();	//Les arestes incidents al v�rtex FINAL de edgeSource
	for (std::deque<int>::iterator it = edgesV2.begin(); it != edgesV2.end(); it++) {
		replaceVertPointer(*it, edgeSource.v2, edgeDestination.v1);
		addEdgePointer(edgeDestination.v1, *it);
	}

	//Elimino els v�rtexs que sobren de l'aresta edgeSource
	//vertTable.erase(eSourceV1);
	//vertTable.erase(eSourceV2);

}

void NavMesh::replaceVertPointer(int idEdge, int idOldVert, int idNewVert) {
	//Reempla�a el punter idOldVert per idNEwVert de l'aresta idEdge
	Edge& edge = getEdge(idEdge);
	if (edge.v1 == idOldVert) edge.v1 = idNewVert;
	else if (edge.v2 == idOldVert) edge.v2 = idNewVert;
}

void NavMesh::merge(NavMesh* nmSource) {
	//Fusiona la informaci� de nmSource amb la informaci� de la navMesh actual
	Ogre::LogManager::getSingletonPtr()->logMessage("FUSIONING VERT TABLE");
	mergeVertTable(nmSource->getVertTable());
	Ogre::LogManager::getSingletonPtr()->logMessage("FUSIONING EDGE TABLE");
	mergeEdgeTable(nmSource->getEdgeTable());
	Ogre::LogManager::getSingletonPtr()->logMessage("FUSIONING CELL TABLE");
	mergeCellTable(nmSource->getCellTable());
}

void NavMesh::mergeVertTable(VertTable& vTableSource) {
	for (VertTable::iterator it = vTableSource.begin(); it != vTableSource.end(); it++) {
		vertTable.insert(*it);
	}
}

void NavMesh::mergeEdgeTable(EdgeTable& eTableSource) {
	for (EdgeTable::iterator it = eTableSource.begin(); it != eTableSource.end(); it++) {
		edgeTable.insert(*it);
	}
}

void NavMesh::mergeCellTable(CellTable& cTableSource) {
	for (CellTable::iterator it = cTableSource.begin(); it != cTableSource.end(); it++) {
		cellTable.insert(*it);
	}
}

void NavMesh::deleteTJoints(std::set<int>& sharedEdgesDest, std::set<int>& sharedEdgesSource) {
	//Elimina les T-Joints resultants d'unir dues NavMeshes
	Ogre::LogManager::getSingletonPtr()->logMessage("TESTING DELETE TJOINTS");
	Vertex* vert;
	IncidentEdgesList initialEdges;	//Com que eliminar� les arestes incidents al v�rtex, faig una c�pia del vector original
	std::set<int> equivalentCells;	//Al eliminar t-joints, cel�les que estaven separades entren en contacte. Aquestes cel�les es poden unir en una de sola. 
	std::set<int> initialTJoints = tJoints;	//Com que eliminar� algunes TJoints, em guardo una c�pia
	//Elimino les TJoints reals
	for (std::set<int>::iterator itVert = initialTJoints.begin(); itVert != initialTJoints.end(); itVert++) {
		if (isWeakVertex(*itVert)) {
			//Elimino totes les arestes incidents al v�rtex	
			Ogre::LogManager::getSingletonPtr()->logMessage("WEAK VERTEX! = " + std::to_string(*itVert));
			vert = &getVertex(*itVert);
			initialEdges = vert->getIncidentEdges();
			for (std::deque<int>::iterator itEdge = initialEdges.begin(); itEdge != initialEdges.end(); itEdge++) {
				equivalentCells.insert(getEdge(*itEdge).cellID);
				eraseEdge(*itEdge);
				sharedEdgesDest.erase(*itEdge);
				sharedEdgesSource.erase(*itEdge);
			}
			eraseVertex(*itVert);
			tJoints.erase(*itVert);
		}
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("WEAK VERTICES DELETED!");

	//En aquest punt, les TJoints ja s'han eliminat. Els v�rtexs que queden en la llista tJoints, s�n 
	//v�rtexs forts, per� pot ser que tinguin algun protal prescindible. Els elimino. 
	for (std::set<int>::iterator itVert = tJoints.begin(); itVert != tJoints.end(); itVert++) {
		vert = &getVertex(*itVert);
		Ogre::LogManager::getSingletonPtr()->logMessage("strongVertID = " + std::to_string(vert->getID()));
		initialEdges = vert->getIncidentEdges();
		for (std::deque<int>::iterator itEdge = initialEdges.begin(); itEdge != initialEdges.end(); itEdge++) {
			Ogre::LogManager::getSingletonPtr()->logMessage("edge = " + std::to_string(*itEdge));
			if (isNonEssentialPortal(*itEdge)) {
				Ogre::LogManager::getSingletonPtr()->logMessage("NON ESSENTIAL PORTAL");
				Ogre::LogManager::getSingletonPtr()->logMessage(std::to_string(getEdge(*itEdge).cellID));
				equivalentCells.insert(getEdge(*itEdge).cellID);
				eraseEdge(*itEdge);
				sharedEdgesDest.erase(*itEdge);
				sharedEdgesSource.erase(*itEdge);
			}
		}
	}
	tJoints.clear();	//Ja he eliminat totes les TJoints
	Ogre::LogManager::getSingletonPtr()->logMessage("NON ESSENTIAL PORTAL CLEARED");

	int cellID = mergeEquivalentCells(equivalentCells);	//Merging de totes les cel�les equivalents en una de sola. 
	if (cellID != -1) cellsToRepair.insert(cellID);	//La cel�la resultant pot contenir concavitats
	Ogre::LogManager::getSingletonPtr()->logMessage("TESTING DELETE TJOINTS OK");
}

bool NavMesh::isWeakVertex(int idVertex) {
	//Indica si el v�rtex idVertex �s d�bil, es a dir, si totes les arestes
	//incidents al v�rtex s�n portals
	Vertex& vertex = getVertex(idVertex);
	Edge* edge;
	IncidentEdgesList::iterator it = vertex.getIncidentEdges().begin();
	bool geometryEdgeFound = false;
	while (!geometryEdgeFound && (it != vertex.getIncidentEdges().end())) {
		edge = &getEdge(*it);
		geometryEdgeFound = !(edge->isPortal());
		it++;
	}
	return !geometryEdgeFound;
}

int NavMesh::mergeEquivalentCells(std::set<int>& equivalentCells) {
	//Uneixo totes les cel�les equivalents en una de sola. 
	//Agafo com a base, la primera cel�la de la llista. Aquesta cel�la ser� la que sobrevisqui; les dem�s s'eliminaran. 
	if (equivalentCells.size() == 0) return -1;							//Cap cel�la en la llista d'equival�ncies
	if (equivalentCells.size() == 1) return *equivalentCells.begin();	//Una �nica cel�la. Retorno el seu identificador
	//Si arribo aqu� �s perqu�, com a m�nim, tinc dues cel�les en la llista equivalentCells
	std::set<int>::iterator cellIt = equivalentCells.begin();
	int destCellID = *cellIt;
	cellIt++;
	for (; cellIt != equivalentCells.end(); cellIt ++) {
		mergeCells(destCellID, *cellIt);
	}
	computeAdjacency(destCellID);	//Recalculo l'adjac�ncia de la cel�la resultant
	return destCellID;
}

void NavMesh::mergeCells(int destCellID, int sourceCellID) {
	//Uneix la cel�la destCellID i sourceCellID en una de sola. 
	//El resultat es guarda a destCellID
	Ogre::LogManager::getSingletonPtr()->logMessage("MERGING CELLS");
	Ogre::LogManager::getSingletonPtr()->logMessage("destCellID = " + std::to_string(destCellID));
	Ogre::LogManager::getSingletonPtr()->logMessage("sourceCellID = " + std::to_string(sourceCellID));
		
	//1) Merging d'informaci� (de cellSource a cellDest)
	Cell& destCell = getCell(destCellID);			//La cel�la de dest�
	Cell& sourceCell = getCell(sourceCellID);		//La cel�la font
	destCell.edges.insert(sourceCell.edges.begin(), sourceCell.edges.end());
	destCell.portals.insert(sourceCell.portals.begin(), sourceCell.portals.end());
	destCell.voxels.insert(sourceCell.voxels.begin(), sourceCell.voxels.end());
	
	//2) Elimino la cel�la sourceCellID
	eraseCell(sourceCellID);
}

void NavMesh::computeAdjacency(int idCell) {
	//Recorro totes les arestes de la cel�la que s�n portals
	Cell& c = getCell(idCell);
	c.adjacentTo.clear();
	Edge* e;
	for (std::set<int>::iterator itEdge = c.portals.begin(); itEdge != c.portals.end(); itEdge++) {
		e = &(getEdge(*itEdge));
		addAdjacency(c.id, getEdge(e->opposite).cellID);
	}
}

/****************************************************************/
/* DIBUIXAT */
/****************************************************************/
void NavMesh::showNavMesh(bool v) {
	manualGeometryEdges->setVisible(v);
	manualSharedEdges->setVisible(v);
	manualPortals->setVisible(v);
	manualGraph->setVisible(v);
}

void NavMesh::clearManualEdges() {
	for (EdgeTable::iterator it = edgeTable.begin(); it != edgeTable.end(); it++) {
		it->second.manual->clear();
	}
}

void NavMesh::drawEdges() {
	float x, y, z;
	manualGeometryEdges->clear();
	manualSharedEdges->clear();
	manualPortals->clear();
	manualGeometryEdges->begin("Red", Ogre::RenderOperation::OT_LINE_LIST);
	manualSharedEdges->begin("pureBlue", Ogre::RenderOperation::OT_LINE_LIST);
	manualPortals->begin("pureGreen", Ogre::RenderOperation::OT_LINE_LIST);
	//manualPortals->setLinePattern(3, 0, 0xFFFF);
	//manualGeometryEdges->setLinePattern(3, 0, 0xFFFF);
	std::set<int> drawnPortals;
	Ogre::ManualObject* m;
	for (EdgeTable::iterator it = edgeTable.begin(); it != edgeTable.end(); it++) {
		if (drawnPortals.find(it->second.id) != drawnPortals.end()) continue;
		if (it->second.isPortal()) {
			m = manualPortals;
			drawnPortals.insert(it->second.id);
			drawnPortals.insert(it->second.opposite);
		}
		else {
			if (it->second.isShared) {
				//m = manualSharedEdges;
				m = manualGeometryEdges;
			}
			else {
				m = manualGeometryEdges;
			}
		}

		Vec3D v1 = vertTable.at(it->second.v1).getPosition();
		Vec3D v2 = vertTable.at(it->second.v2).getPosition();

		x = v1.getX();
		y = v1.getY();
		z = v1.getZ();
		if (applicationMode == APPLICATION_MODE_2D) {
			if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XY) z = 0.0f;
			else if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XZ) y = 0.0f;
			else x = 0.0f;
		}
		m->position(x, y, z);
	
		x = v2.getX();
		y = v2.getY();
		z = v2.getZ();
		if (applicationMode == APPLICATION_MODE_2D) {
			if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XY) z = 0.0f;
			else if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XZ) z = 0.0f;
			else x = 0.0f;
		}
		m->position(x, y, z);
	}
	manualGeometryEdges->end();
	manualSharedEdges->end();
	manualPortals->end();
}

void NavMesh::drawEdge(int idEdge, Ogre::ColourValue& color) {
	//Dibuixa una aresta
	Edge& edge = getEdge(idEdge);
	Vec3D v1 = vertTable.at(edge.v1).getPosition();
	Vec3D v2 = vertTable.at(edge.v2).getPosition();

	if (edge.manual == NULL) {
		edge.manual = mSceneMgr->createManualObject();
		mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(edge.manual);
	}
	Ogre::ManualObject* m = edge.manual;

	float x, y, z;

	m->clear();
	m->begin("matEdgeGPU", Ogre::RenderOperation::OT_LINE_LIST);

	m->colour(color);

	x = v1.getX();
	y = v1.getY();
	z = v1.getZ();
	if (applicationMode == APPLICATION_MODE_2D) {
		if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XY) z = 0.0f;
		else if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XZ) y = 0.0f;
		else x = 0.0f;
	}
	m->position(x, y, z);
	
	x = v2.getX();
	y = v2.getY();
	z = v2.getZ();
	if (applicationMode == APPLICATION_MODE_2D) {
		if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XY) z = 0.0f;
		else if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XZ) y = 0.0f;
		else x = 0.0f;
	}
	m->position(x, y, z);
	
	m->end();
}

void NavMesh::drawEdge(int idEdge, const Ogre::String& matName) {
	//Dibuixa una aresta
	Edge& edge = getEdge(idEdge);
	Vec3D v1 = vertTable.at(edge.v1).getPosition();
	Vec3D v2 = vertTable.at(edge.v2).getPosition();

	if (edge.manual == NULL) {
		edge.manual = mSceneMgr->createManualObject();
		mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(edge.manual);
		float x, y, z;

		edge.manual->begin(matName, Ogre::RenderOperation::OT_LINE_LIST);

		x = v1.getX();
		y = v1.getY();
		z = v1.getZ();
		if (applicationMode == APPLICATION_MODE_2D) {
			if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XY) z = 0.0f;
			else if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XZ) y = 0.0f;
			else x = 0.0f;
		}
		edge.manual->position(x, y, z);
	
		x = v2.getX();
		y = v2.getY();
		z = v2.getZ();
		if (applicationMode == APPLICATION_MODE_2D) {
			if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XY) z = 0.0f;
			else if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XZ) y = 0.0f;
			else x = 0.0f;
		}
		edge.manual->position(x, y, z);
	
		edge.manual->end();
	}
	else {
		edge.manual->setMaterialName(0, matName);
	}
}

void NavMesh::drawCell(int idCell) {
	Cell& c = getCell(idCell);
	Edge* edge;
	for (std::set<int>::iterator it = c.edges.begin(); it != c.edges.end(); it++) {
		edge = &getEdge(*it);
		if (edge->isPortal()) drawEdge(*it, Ogre::ColourValue(0.0f, 1.0f, 0.0f));
		else drawEdge(*it, Ogre::ColourValue(1.0f, 0.0f, 0.0f));
	}
}

void NavMesh::drawCell(int idCell, const Ogre::String& matName) {
	Cell& c = getCell(idCell);
	for (std::set<int>::iterator it = c.edges.begin(); it != c.edges.end(); it++) {
		drawEdge(*it, matName);
	}
}

void NavMesh::hideCell(int idCell) {
	Cell& c = getCell(idCell);
	Edge* edge;
	for (std::set<int>::iterator it = c.edges.begin(); it != c.edges.end(); it++) {
		edge = &getEdge(*it);
		edge->manual->clear();
	}
}

void NavMesh::drawGraph() {

	manualGraph->clear();
	manualGraph->begin("navMeshAdjacency", Ogre::RenderOperation::OT_LINE_LIST);

	//For each cell, draw a line joining its center with the midpoint of its portals
	for (CellTable::iterator itCell = cellTable.begin(); itCell != cellTable.end(); itCell++) {
		Cell* c = &(itCell->second);
		//c->p->getCenterCoords(x, y, z); 
		Vec3D center = computeCenterCell(itCell->first);
				
		for (std::set<int>::iterator itPortal = c->portals.begin(); itPortal != c->portals.end(); itPortal++) {
			//Compute the midpoint of the portal
			Edge& portal = getEdge(*itPortal);
			Vec3D& v1 = getVertex(portal.v1).getPosition();
			Vec3D& v2 = getVertex(portal.v2).getPosition();
			Vec3D midPoint = (v1 + v2) / 2.0f;

			//Draw the line joining the center of the cell with the midpoint of the portal
			manualGraph->position(center.getX(), center.getY(), center.getZ());
			manualGraph->position(midPoint.getX(), midPoint.getY(), midPoint.getZ());
		}
	}
	manualGraph->end();
}

Vec3D NavMesh::computeCenterCell(int cellID) {
	Cell& c = getCell(cellID);
	Vec3D center(0,0,0);
	for (std::set<int>::iterator itEdge = c.edges.begin(); itEdge != c.edges.end(); itEdge++) {
		Edge& e = getEdge(*itEdge);
		center += getVertex(e.v1).getPosition();
	}
	center /= float(c.edges.size());
	return center;
}

void NavMesh::showGraph(bool v) {
	manualGraph->setVisible(v);
}

/****************************************************************/
/* INFORMACIO */
/****************************************************************/
void NavMesh::createEdgeColour(int idEdge) {
	//Crea un color per l'aresta idEdge. Aquest color s'utilitza per indetnificar les arestes
	//en mode GPU
	Ogre::ColourValue& edgeColour = getEdge(idEdge).colour;
	switch (currentColourType) {
		case EDGE_COLOUR_RED:
			createEdgeColourRed(edgeColour);
			break;
		
		case EDGE_COLOUR_GREEN:
			createEdgeColourGreen(edgeColour);
			break;

		case EDGE_COLOUR_BLUE:
			createEdgeColourBlue(edgeColour);
			break;

		case EDGE_COLOUR_MAGENTA:
			createEdgeColourMagenta(edgeColour);
			break;

		case EDGE_COLOUR_YELLOW:
			createEdgeColourYellow(edgeColour);
			break;

		case EDGE_COLOUR_CYAN:
			createEdgeColourCyan(edgeColour);
			break;

		case EDGE_COLOUR_GREY:
			createEdgeColourGrey(edgeColour);
			break;
	}
	Ogre::RGBA rgba = edgeColour.getAsRGBA();
	/*ColourTable::iterator it = colourTable.find(rgba);
	if (it != colourTable.end()) {
		Ogre::LogManager::getSingletonPtr()->logMessage("COLOUR EDGE ALREADY EXISTS!");
		Ogre::LogManager::getSingletonPtr()->logMessage("edgeID = " + std::to_string(colourTable[rgba]));
	}*/
	colourTable[rgba] = idEdge;	//Creo l'entrada a la taula que relaciona el color amb una aresta
	currentColourType = (currentColourType + 1) % NUM_EDGE_COLOUR_TYPES;
}

void NavMesh::createEdgeColourRed(Ogre::ColourValue& edgeColour) {
	//Crea un color per l'aresta idEdge. Aquest color s'utilitza per indetnificar les arestes
	//en mode GPU
	edgeColour.r = 1.0f;
	edgeColour.g = (float)rGreen / 255.0f;
	edgeColour.b = (float)rBlue / 255.0f;

	rGreen ++;
	if (rGreen > 254) {
		rGreen = 0;
		rBlue ++;
		if (rBlue < 254) rBlue ++;
	}
}

void NavMesh::createEdgeColourGreen(Ogre::ColourValue& edgeColour) {
	//Crea un color per l'aresta idEdge. Aquest color s'utilitza per indetnificar les arestes
	//en mode GPU
	edgeColour.r = (float)gRed / 255.0f;
	edgeColour.g = 1.0f;
	edgeColour.b = (float)gBlue / 255.0f;

	gRed ++;
	if (gRed > 254) {
		gRed = 0;
		gBlue ++;
		if (gBlue < 254) gBlue ++;
	}
}

void NavMesh::createEdgeColourBlue(Ogre::ColourValue& edgeColour) {
	//Crea un color per l'aresta idEdge. Aquest color s'utilitza per indetnificar les arestes
	//en mode GPU
	edgeColour.r = (float)bRed / 255.0f;
	edgeColour.g = (float)bGreen / 255.0f;
	edgeColour.b = 1.0f;

	bRed ++;
	if (bRed > 254) {
		bRed = 0;
		bGreen ++;
		if (bGreen < 254) bGreen ++;
	}
}

void NavMesh::createEdgeColourMagenta(Ogre::ColourValue& edgeColour) {
	//Crea un color per l'aresta idEdge. Aquest color s'utilitza per indetnificar les arestes
	//en mode GPU
	edgeColour.r = 1.0f;
	edgeColour.g = (float)mGreen / 255.0f;
	edgeColour.b = 1.0f;

	if (mGreen < 254) mGreen ++;
}

void NavMesh::createEdgeColourYellow(Ogre::ColourValue& edgeColour) {
	//Crea un color per l'aresta idEdge. Aquest color s'utilitza per indetnificar les arestes
	//en mode GPU
	edgeColour.r = 1.0f;
	edgeColour.g = 1.0f;
	edgeColour.b = (float)yBlue / 255.0f;

	if (yBlue < 254) yBlue ++;
}

void NavMesh::createEdgeColourCyan(Ogre::ColourValue& edgeColour) {
	//Crea un color per l'aresta idEdge. Aquest color s'utilitza per indetnificar les arestes
	//en mode GPU
	edgeColour.r = (float)cRed / 255.0f;
	edgeColour.g = 1.0f;
	edgeColour.b = 1.0f;

	if (cRed < 254) cRed ++;
}

void NavMesh::createEdgeColourGrey(Ogre::ColourValue& edgeColour) {
	//Crea un color per l'aresta idEdge. Aquest color s'utilitza per indetnificar les arestes
	//en mode GPU
	edgeColour.r = (float)wRed / 255.0f;
	edgeColour.g = (float)wGreen / 255.0f;
	edgeColour.b = (float)wBlue / 255.0f;

	wRed ++;
	if (wRed > 254) {
		wRed = 0;

		wGreen ++;
		if (wGreen > 254) {
			wGreen = 0;
			wBlue ++;
		}
	}
}

/****************************************************************/
/* INFORMACIO */
/****************************************************************/
void NavMesh::printData() {
	printVertTable();
	printEdgeTable();
	printCellTable();
}

void NavMesh::printVertTable() {
	Ogre::LogManager::getSingletonPtr()->logMessage("*** VERTEX TABLE INFO ***");
	Ogre::LogManager::getSingletonPtr()->logMessage("=========================");

	for (VertTable::iterator it = vertTable.begin(); it != vertTable.end(); it++) {
		printVertex(it->first);
	}
}

void NavMesh::printVertex(int idVert) {
	Vertex& nmV = getVertex(idVert);
	Ogre::LogManager::getSingletonPtr()->logMessage("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-");
	Ogre::LogManager::getSingletonPtr()->logMessage("Vertex id = " + std::to_string(nmV.getID()));
	Ogre::LogManager::getSingletonPtr()->logMessage("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-");
	Vec3D v = nmV.getPosition();
	Ogre::LogManager::getSingletonPtr()->logMessage("x = " + std::to_string(v.getX()));
	Ogre::LogManager::getSingletonPtr()->logMessage("y = " + std::to_string(v.getY()));
	Ogre::LogManager::getSingletonPtr()->logMessage("z = " + std::to_string(v.getZ()));
	Ogre::LogManager::getSingletonPtr()->logMessage("isConcave = " + std::to_string(nmV.isNotch()));
	Ogre::LogManager::getSingletonPtr()->logMessage("concEdge1 = " + std::to_string(nmV.getConcEdge1()));
	Ogre::LogManager::getSingletonPtr()->logMessage("concEdge2 = " + std::to_string(nmV.getConcEdge2()));
	
	Ogre::LogManager::getSingletonPtr()->logMessage("Edges incidents to vert:");
	Edge* edge;
	Ogre::String s;
	for (IncidentEdgesList::iterator it = nmV.getIncidentEdges().begin(); it != nmV.getIncidentEdges().end(); it++) {
		edge = &(getEdge(*it));
		s = "id = " + std::to_string(*it) + ", type = " + std::to_string(edge->_type);
		Ogre::LogManager::getSingletonPtr()->logMessage(s);
	}	
}

void NavMesh::printSharedVertices() {
	Ogre::LogManager::getSingletonPtr()->logMessage("*** SHARED VERTICES ***");
	Ogre::LogManager::getSingletonPtr()->logMessage("=======================");
	for (std::set<int>::iterator it = sharedVertices.begin(); it != sharedVertices.end(); it++) {
		Ogre::LogManager::getSingletonPtr()->logMessage(std::to_string(*it));
	}
}

void NavMesh::printSharedEdges() {
	Ogre::LogManager::getSingletonPtr()->logMessage("*** SHARED EDGES ***");
	Ogre::LogManager::getSingletonPtr()->logMessage("=======================");
	for (std::set<int>::iterator it = sharedEdges.begin(); it != sharedEdges.end(); it++) {
		Ogre::LogManager::getSingletonPtr()->logMessage(std::to_string(*it));
	}
}

void NavMesh::printEdgeTable() {
	Ogre::LogManager::getSingletonPtr()->logMessage("*** EDGE TABLE INFO ***");
	Ogre::LogManager::getSingletonPtr()->logMessage("=======================");
	
	for (EdgeTable::iterator it = edgeTable.begin(); it != edgeTable.end(); it++) {
		printEdge(it->first);
	}
}

void NavMesh::printEdge(int idEdge) {
	Edge& nmE = getEdge(idEdge);
	Ogre::LogManager::getSingletonPtr()->logMessage("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-");
	if (!nmE.isPortal()) {
		Ogre::LogManager::getSingletonPtr()->logMessage("Edge id = " + std::to_string(nmE.id));
	}
	else {
		Ogre::LogManager::getSingletonPtr()->logMessage("Edge id = " + std::to_string(nmE.id) + " (PORTAL) ");
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-");
	Ogre::LogManager::getSingletonPtr()->logMessage("v1 = " + std::to_string(nmE.v1));
	Ogre::LogManager::getSingletonPtr()->logMessage("v2 = " + std::to_string(nmE.v2));
	Ogre::LogManager::getSingletonPtr()->logMessage("isShared = " + std::to_string(nmE.isShared));
	if (nmE.isPortal()) {
		Ogre::LogManager::getSingletonPtr()->logMessage("opposite = " + std::to_string(nmE.opposite));
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("Cell using edge:");
	Ogre::LogManager::getSingletonPtr()->logMessage(std::to_string(nmE.cellID));
}

void NavMesh::printCellTable() {
	size_t numCells = cellTable.size();
	Ogre::LogManager::getSingletonPtr()->logMessage("==================================================================");
	Ogre::LogManager::getSingletonPtr()->logMessage("CELL TABLE INFORMATION");
	Ogre::LogManager::getSingletonPtr()->logMessage("==================================================================");
	Ogre::LogManager::getSingletonPtr()->logMessage("NUM CELLS = " + std::to_string((int)(cellTable.size())));
	for (CellTable::iterator it = cellTable.begin(); it != cellTable.end(); it++) {
		printCell(it->first);
	}
}

void NavMesh::printCell(int idCell) {
	Cell& c = getCell(idCell);
	Ogre::LogManager::getSingletonPtr()->logMessage("==================================================================");
	Ogre::LogManager::getSingletonPtr()->logMessage("Cell ID = " + std::to_string(c.id));
	Ogre::LogManager::getSingletonPtr()->logMessage("==================================================================");
	
	Ogre::LogManager::getSingletonPtr()->logMessage("Edges conforming cell:");
	for (std::set<int>::iterator itEdges = c.edges.begin(); itEdges != c.edges.end(); itEdges++) {
		Ogre::LogManager::getSingletonPtr()->logMessage(std::to_string(*itEdges));
	}

	Ogre::LogManager::getSingletonPtr()->logMessage("Edges that are portal:");
	for (std::set<int>::iterator itPortals = c.portals.begin(); itPortals != c.portals.end(); itPortals++) {
		Ogre::LogManager::getSingletonPtr()->logMessage(std::to_string(*itPortals));
	}

	Ogre::LogManager::getSingletonPtr()->logMessage("Adjacent to:");
	for (std::set<int>::iterator itAdjacent = c.adjacentTo.begin(); itAdjacent != c.adjacentTo.end(); itAdjacent++) {
		Ogre::LogManager::getSingletonPtr()->logMessage(std::to_string(*itAdjacent));
	}

	Ogre::LogManager::getSingletonPtr()->logMessage("Voxel Description:");
	Ogre::LogManager::getSingletonPtr()->logMessage("numVoxels = " + std::to_string(c.voxels.size()));
	Ogre::String sX, sY, sZ;
	for (VoxelPointerTable::iterator it = c.voxels.begin(); it != c.voxels.end(); it++) {
		sX = std::to_string(it->x);
		sY = std::to_string(it->y);
		sZ = std::to_string(it->z);
		Ogre::LogManager::getSingletonPtr()->logMessage("(" + sX + ", " + sY + ", " + sZ + ")");
	}
}