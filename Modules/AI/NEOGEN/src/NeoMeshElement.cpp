#include "NeoMeshElement.h"

using namespace NEOGEN;

#pragma region CREATION AND DESTRUCTION

NeoMeshElement::NeoMeshElement(ElementID id) {
	_id = id;
}

#pragma endregion

#pragma region GET AND SET

ElementID NeoMeshElement::getID() const {
	return _id;
}

#pragma endregion