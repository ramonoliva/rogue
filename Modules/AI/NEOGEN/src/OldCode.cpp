void ObstacleSolver::generateObstaclePartition(size_t refX, size_t refY) {
	normalDepthMap.save("00normalDepthMap.png");
	
	timeBefore = timer->getMilliseconds();
	initPixelDepth();
	detectObstacles(refX, refY);
	savePartition("01BinaryPartitionRough.png");

	//deleteSpines();
	//savePartition("02BinaryPartitionNoSpines.png");

	//deleteIslands();
	//savePartition("02BinaryPartitionRoughNoIslands.png");
	
	//contourExpansion8();
	//savePartition("03BinaryPartitionExpansion.png");
	
	//contourRefinement();
	//savePartition("04BinaryPartitionRefinement.png");

	//createContourVerts();
	
	polygonReconstruction();
	savePartition("05FinalPartition.png");

	timeAfter = timer->getMilliseconds();
	elapsedTime = timeAfter - timeBefore;
}

void ObstacleSolver::createContourVerts(void) {
	std::pair<size_t, size_t> contourPixel;
	for (ContourMap::iterator it = contourMap.begin(); it != contourMap.end(); it++) {
		contourPixel = it->first;
		createContourVerts(contourPixel);
	}
}

void ObstacleSolver::createContourVerts(std::pair<size_t, size_t>& contourPixel) {
	int NType = getPixelType(obstaclePartition, (int)contourPixel.first, (int)contourPixel.second - 1);	//Tipus del ve� NORD
	int SType = getPixelType(obstaclePartition, (int)contourPixel.first, (int)contourPixel.second + 1);	//Tipus del ve� SUD
	int WType = getPixelType(obstaclePartition, (int)contourPixel.first - 1, (int)contourPixel.second);	//Tipus del ve� OEST
	int EType = getPixelType(obstaclePartition, (int)contourPixel.first + 1, (int)contourPixel.second);	//Tipus del ve� EST
	float vX, vY;
	//Comprobo si puc crear el v�rtex NW. 
	//Es pot crear si els ve�ns N o W del p�xel s�n accessibles i NO s�n contorn. 
	if ((NType == PIXEL_TYPE_WALKABLE) || (WType == PIXEL_TYPE_WALKABLE)) {
		vX = (float)contourPixel.first - 0.5f;
		vY = (float)contourPixel.second - 0.5f;
		addVertContour(vX, vY);
	}
	//Comprobo si puc crear el v�rtex NE. 
	//Es pot crear si els ve�ns N o E del p�xel s�n accessibles i NO s�n contorn. 
	if ((NType == PIXEL_TYPE_WALKABLE) || (EType == PIXEL_TYPE_WALKABLE)) {
		vX = (float)contourPixel.first + 0.5f;
		vY = (float)contourPixel.second - 0.5f;
		addVertContour(vX, vY);
	}
	//Comprobo si puc crear el v�rtex SW. 
	//Es pot crear si els ve�ns S o W del p�xel s�n accessibles i NO s�n contorn. 
	if ((SType == PIXEL_TYPE_WALKABLE) || (WType == PIXEL_TYPE_WALKABLE)) {
		vX = (float)contourPixel.first - 0.5f;
		vY = (float)contourPixel.second + 0.5f;
		addVertContour(vX, vY);
	}
	//Comprobo si puc crear el v�rtex SE. 
	//Es pot crear si els ve�ns N o W del p�xel s�n accessibles i NO s�n contorn. 
	if ((SType == PIXEL_TYPE_WALKABLE) || (EType == PIXEL_TYPE_WALKABLE)) {
		vX = (float)contourPixel.first + 0.5f;
		vY = (float)contourPixel.second + 0.5f;
		addVertContour(vX, vY);
	}
}

/*****************************************************************/
/* DELETE ISLANDS */
/*****************************************************************/

//Proc�s provisional que m'elimina p�xels de contorn a�llats (rodejats de blanc)

void ObstacleSolver::deleteIslands(void) {
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("DELETING ISLANDS");
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");
	ContourMapIterator it;
	std::pair<size_t, size_t> pixelID;
	bool colourNorth, colourSouth, colourWest, colourEast; 
	PixelVector pixelsToDelete;
	for (it = contourMap.begin(); it != contourMap.end(); it++) {
		pixelID = it->first;
		
		PixelColor pColorNorth = getColourAt(obstaclePartition, (int)pixelID.first, (int)pixelID.second - 1);
		PixelColor pColorSouth = getColourAt(obstaclePartition, (int)pixelID.first, (int)pixelID.second + 1);
		PixelColor pColorWest = getColourAt(obstaclePartition, (int)pixelID.first - 1, (int)pixelID.second);
		PixelColor pColorEast = getColourAt(obstaclePartition, (int)pixelID.first + 1, (int)pixelID.second);

		colourNorth = pColorNorth.first && (Ogre::Math::RealEqual(pColorNorth.second.r, 1.0f));
		colourSouth = pColorSouth.first && (Ogre::Math::RealEqual(pColorSouth.second.r, 1.0f));
		colourWest = pColorWest.first && (Ogre::Math::RealEqual(pColorWest.second.r, 1.0f));
		colourEast = pColorEast.first && (Ogre::Math::RealEqual(pColorEast.second.r, 1.0f));

		if (
			(colourNorth) && 
			(colourSouth) && 
			(colourWest) && 
			(colourEast)
			)
		{
			pixelsToDelete.push_back(pixelID);
			obstaclePartition.setColourAt(Ogre::ColourValue::White, pixelID.first, pixelID.second, 0);
		}
	}

	for (size_t i = 0; i < pixelsToDelete.size(); i++) {
		pixelID = pixelsToDelete.at(i);
		deletePixelContour(pixelID.first, pixelID.second);
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("ISLANDS DELETED");
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");
}

void ObstacleSolver::deleteSpines(void) {
	//Elimina les "espines", que s�n p�xels contorn que faran que el proc�s
	//de reconstrucci� de pol�gons no sigui correcte. 
	//NOTA: LA IMPLEMENTACI� ACTUAL �S TOTALMENT PROVISIONAL, CAL FER-HO BE!!!

	//deletePixelContour(203, 233);
	//obstaclePartition.setColourAt(Ogre::ColourValue::White, 203, 233, 0);
	//deletePixelContour(270, 233);
	//obstaclePartition.setColourAt(Ogre::ColourValue::White, 270, 233, 0);

	ContourMapIterator it = contourMap.begin();
	ContourMapIterator itAux;
	int numContour;
	std::pair<size_t, size_t> pixelID;
	while (it != contourMap.end()) {
		pixelID = it->first;
		it++;
		numContour = 0;
		//Calculo quants dels 4 ve�ns s�n contorn
		if (pixelContourExist(pixelID.first, pixelID.second - 1)) numContour++;	//El ve� NORD �s contorn
		if (pixelContourExist(pixelID.first, pixelID.second + 1)) numContour++;	//El ve� SUD �s contorn
		if (pixelContourExist(pixelID.first - 1, pixelID.second)) numContour++;	//El ve� OEST �s contorn
		if (pixelContourExist(pixelID.first + 1, pixelID.second)) numContour++;	//El ve� EST �s contorn
		
		if (numContour <= 1) {
			deletePixelContour(pixelID.first, pixelID.second);
			obstaclePartition.setColourAt(Ogre::ColourValue::White, pixelID.first, pixelID.second, 0);
		}
	}
}

/*****************************************************************/
/* EXPANSI� DELS CONTORNS DELS OBSTACLES */
/*****************************************************************/
void ObstacleSolver::contourExpansion8(void) {
	//Contour expansion �s equivalent al erode de matlab. Aquest
	//proc�s el faig per evitar tenir obstacles que nom�s ocupen un p�xel
	//o s�n "massa prims". Ambd�s tipus d'obstacles donen problemes 
	//al fer la reconstrucci� en pol�gons. 
	
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("EXPANDING CONTOUR");
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");
	ContourMap initMap(contourMap);
	contourMap.clear();
		
	std::pair<size_t, size_t> pixelID;
	int x, y;
	ContourMapIterator it;
	for (it = initMap.begin(); it != initMap.end(); it++) {
		pixelID = it->first;
		x = (int)pixelID.first;
		y = (int)pixelID.second;
		obstaclePartition.setColourAt(Ogre::ColourValue::Black, x, y, 0);	//Per seguretat, pinto de negre el contorn

		for (int offsetX = -1; offsetX <= 1; offsetX++) {
			for (int offsetY = -1; offsetY <= 1; offsetY++) {
				if (getPixelType(obstaclePartition, x + offsetX, y + offsetY) == PIXEL_TYPE_WALKABLE) {
					addPixelContour((size_t)(x + offsetX), (size_t)(y + offsetY));
					obstaclePartition.setColourAt(Ogre::ColourValue::Black, (size_t)(x + offsetX), (size_t)(y + offsetY), 0);	//Per seguretat, pinto de negre el contorn
				}
			}
		}
	}
	initMap.clear();
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("CONTOUR EXPANDED");
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");
}

/*****************************************************************/
/* REFINAMENT DE CONTORNS */
/*****************************************************************/

//	1 => blanc (p�xel del terra)
//	0 => negre (interior taca)
//	c => negre (contorn)
/*
1	c		=>		1	c
c	c				c	0
*/

void ObstacleSolver::contourRefinement(void) {
	//Refina el contorn per eliminar p�xels innecessaris

	ContourMapIterator it;

	std::pair<size_t, size_t> pixelID;
	PixelVector pixelsToDelete;

	for (it = contourMap.begin(); it != contourMap.end(); it ++) {
		pixelID = it->first;
		
		//Condici� d'eliminaci�: p�xel de contorn rodejat de negres, no �s contorn
		float colourN	= obstaclePartition.getColourAt(pixelID.first, pixelID.second - 1, 0).r;
		float colourNW	= obstaclePartition.getColourAt(pixelID.first - 1, pixelID.second - 1, 0).r;
		float colourNE	= obstaclePartition.getColourAt(pixelID.first + 1, pixelID.second - 1, 0).r;

		float colourS	= obstaclePartition.getColourAt(pixelID.first, pixelID.second + 1, 0).r;
		float colourSW	= obstaclePartition.getColourAt(pixelID.first - 1, pixelID.second + 1, 0).r;
		float colourSE	= obstaclePartition.getColourAt(pixelID.first + 1, pixelID.second + 1, 0).r;
		
		float colourW	= obstaclePartition.getColourAt(pixelID.first - 1, pixelID.second, 0).r;
		float colourE	= obstaclePartition.getColourAt(pixelID.first + 1, pixelID.second, 0).r;

		if (
			(Ogre::Math::RealEqual(colourN, 0.0f))	&& 
			(Ogre::Math::RealEqual(colourNW, 0.0f)) && 
			(Ogre::Math::RealEqual(colourNE, 0.0f)) && 
			(Ogre::Math::RealEqual(colourS, 0.0f))	&& 
			(Ogre::Math::RealEqual(colourSW, 0.0f)) && 
			(Ogre::Math::RealEqual(colourSE, 0.0f)) && 
			(Ogre::Math::RealEqual(colourW, 0.0f))	&& 
			(Ogre::Math::RealEqual(colourE, 0.0f))
			)
		{
			//Ogre::LogManager::getSingletonPtr()->logMessage("CONTOUR PIXEL DELETED!");
			pixelsToDelete.push_back(pixelID);
		}

	}

	for (size_t i = 0; i < pixelsToDelete.size(); i++) {
		pixelID = pixelsToDelete.at(i);
		deletePixelContour(pixelID.first, pixelID.second);
	}
}

/*void ObstacleSolver::createPolygon(std::pair<size_t, size_t>& initialPixelID, int label) {
	//Crea un pol�gon a partir del p�xel inicial initialPixelID
	
	Ogre::LogManager::getSingletonPtr()->logMessage("*****************************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("CREATING POLYGON");

	//1) Recorrer els p�xels adjacents fins que tornem a trobar el p�xel amb el que hem comen�at
	std::pair<int, int> currentPixelID = initialPixelID;	//El p�xel actual
	std::pair<int, int> nextPixelID;						//El pr�xim p�xel que visitarem
	Ogre::LogManager::getSingletonPtr()->logMessage("initialPixelID");
	Ogre::LogManager::getSingletonPtr()->logMessage("x = " + Ogre::StringConverter::toString(initialPixelID.first));
	Ogre::LogManager::getSingletonPtr()->logMessage("y = " + Ogre::StringConverter::toString(initialPixelID.second));
	
	//std::vector<Vert*> lVert;
	PixelVector lPoint;

	bool nextFound, isContour;

	PixelVector candidates;
	PixelVector::iterator it;

	if (label != 0) Ogre::LogManager::getSingletonPtr()->logMessage("CREATING OBSTACLE");
	else Ogre::LogManager::getSingletonPtr()->logMessage("CREATING FLOOR");

	if ((initialPixelID.first == 86) && (initialPixelID.second == 251)) {
		Ogre::LogManager::getSingletonPtr()->logMessage("TESTING ANGLE BETWEEN VECTORS");
		Vert prev(142.0f, 276.0f);
		//Vert next((float)initialPixelID.first, (float)initialPixelID.second + 1.0f);
		Vert origin(143.0f, 277.0f);
		Vert refEdge = prev - origin;
		Vert vNeighbor;
		float neighborX, neighborY, angle;
	
		Ogre::LogManager::getSingletonPtr()->logMessage("COMPUTING ANGLE NORTH" );
		Ogre::LogManager::getSingletonPtr()->logMessage("================================" );
		neighborX = origin.getX();
		neighborY = origin.getY() - 1.0f;
		vNeighbor = Vert(neighborX, neighborY);
		angle = angleBetweenVectors(refEdge, vNeighbor - origin);
		//angle = angleBetweenVectors(vNeighbor - origin, refEdge);
		Ogre::LogManager::getSingletonPtr()->logMessage("neighbor.x = " + Ogre::StringConverter::toString(neighborX));
		Ogre::LogManager::getSingletonPtr()->logMessage("neighbor.y = " + Ogre::StringConverter::toString(neighborY));
		Ogre::LogManager::getSingletonPtr()->logMessage("angle = " + Ogre::StringConverter::toString(angle));

		Ogre::LogManager::getSingletonPtr()->logMessage("COMPUTING ANGLE NORTHWEST" );
		Ogre::LogManager::getSingletonPtr()->logMessage("================================" );
		neighborX = origin.getX() - 1.0f;
		neighborY = origin.getY() - 1.0f;
		vNeighbor = Vert(neighborX, neighborY);
		angle = angleBetweenVectors(refEdge, vNeighbor - origin);
		//angle = angleBetweenVectors(vNeighbor - origin, refEdge);
		Ogre::LogManager::getSingletonPtr()->logMessage("neighbor.x = " + Ogre::StringConverter::toString(neighborX));
		Ogre::LogManager::getSingletonPtr()->logMessage("neighbor.y = " + Ogre::StringConverter::toString(neighborY));
		Ogre::LogManager::getSingletonPtr()->logMessage("angle = " + Ogre::StringConverter::toString(angle));

		Ogre::LogManager::getSingletonPtr()->logMessage("COMPUTING ANGLE WEST" );
		Ogre::LogManager::getSingletonPtr()->logMessage("================================" );
		neighborX = origin.getX() - 1.0f;
		neighborY = origin.getY();
		vNeighbor = Vert(neighborX, neighborY);
		angle = angleBetweenVectors(refEdge, vNeighbor - origin);
		//angle = angleBetweenVectors(vNeighbor - origin, refEdge);
		Ogre::LogManager::getSingletonPtr()->logMessage("neighbor.x = " + Ogre::StringConverter::toString(neighborX));
		Ogre::LogManager::getSingletonPtr()->logMessage("neighbor.y = " + Ogre::StringConverter::toString(neighborY));
		Ogre::LogManager::getSingletonPtr()->logMessage("angle = " + Ogre::StringConverter::toString(angle));

		Ogre::LogManager::getSingletonPtr()->logMessage("COMPUTING ANGLE SOUTHWEST" );
		Ogre::LogManager::getSingletonPtr()->logMessage("================================" );
		neighborX = origin.getX() - 1.0f;
		neighborY = origin.getY() + 1.0f;
		vNeighbor = Vert(neighborX, neighborY);
		angle = angleBetweenVectors(refEdge, vNeighbor - origin);
		//angle = angleBetweenVectors(vNeighbor - origin, refEdge);
		Ogre::LogManager::getSingletonPtr()->logMessage("neighbor.x = " + Ogre::StringConverter::toString(neighborX));
		Ogre::LogManager::getSingletonPtr()->logMessage("neighbor.y = " + Ogre::StringConverter::toString(neighborY));
		Ogre::LogManager::getSingletonPtr()->logMessage("angle = " + Ogre::StringConverter::toString(angle));

		Ogre::LogManager::getSingletonPtr()->logMessage("COMPUTING ANGLE SOUTH" );
		Ogre::LogManager::getSingletonPtr()->logMessage("================================" );
		neighborX = origin.getX();
		neighborY = origin.getY() + 1.0f;
		vNeighbor = Vert(neighborX, neighborY);
		angle = angleBetweenVectors(refEdge, vNeighbor - origin);
		//angle = angleBetweenVectors(vNeighbor - origin, refEdge);
		Ogre::LogManager::getSingletonPtr()->logMessage("neighbor.x = " + Ogre::StringConverter::toString(neighborX));
		Ogre::LogManager::getSingletonPtr()->logMessage("neighbor.y = " + Ogre::StringConverter::toString(neighborY));
		Ogre::LogManager::getSingletonPtr()->logMessage("angle = " + Ogre::StringConverter::toString(angle));

		Ogre::LogManager::getSingletonPtr()->logMessage("COMPUTING ANGLE SOUTHEAST" );
		Ogre::LogManager::getSingletonPtr()->logMessage("================================" );
		neighborX = origin.getX() + 1.0f;
		neighborY = origin.getY() + 1.0f;
		vNeighbor = Vert(neighborX, neighborY);
		angle = angleBetweenVectors(refEdge, vNeighbor - origin);
		//angle = angleBetweenVectors(vNeighbor - origin, refEdge);
		Ogre::LogManager::getSingletonPtr()->logMessage("neighbor.x = " + Ogre::StringConverter::toString(neighborX));
		Ogre::LogManager::getSingletonPtr()->logMessage("neighbor.y = " + Ogre::StringConverter::toString(neighborY));
		Ogre::LogManager::getSingletonPtr()->logMessage("angle = " + Ogre::StringConverter::toString(angle));

		Ogre::LogManager::getSingletonPtr()->logMessage("COMPUTING ANGLE EAST" );
		Ogre::LogManager::getSingletonPtr()->logMessage("================================" );
		neighborX = origin.getX() + 1.0f;
		neighborY = origin.getY();
		vNeighbor = Vert(neighborX, neighborY);
		angle = angleBetweenVectors(refEdge, vNeighbor - origin);
		//angle = angleBetweenVectors(vNeighbor - origin, refEdge);
		Ogre::LogManager::getSingletonPtr()->logMessage("neighbor.x = " + Ogre::StringConverter::toString(neighborX));
		Ogre::LogManager::getSingletonPtr()->logMessage("neighbor.y = " + Ogre::StringConverter::toString(neighborY));
		Ogre::LogManager::getSingletonPtr()->logMessage("angle = " + Ogre::StringConverter::toString(angle));

		Ogre::LogManager::getSingletonPtr()->logMessage("COMPUTING ANGLE NORTHEAST" );
		Ogre::LogManager::getSingletonPtr()->logMessage("================================" );
		neighborX = origin.getX() + 1.0f;
		neighborY = origin.getY() - 1.0f;
		vNeighbor = Vert(neighborX, neighborY);
		angle = angleBetweenVectors(refEdge, vNeighbor - origin);
		//angle = angleBetweenVectors(vNeighbor - origin, refEdge);
		Ogre::LogManager::getSingletonPtr()->logMessage("neighbor.x = " + Ogre::StringConverter::toString(neighborX));
		Ogre::LogManager::getSingletonPtr()->logMessage("neighbor.y = " + Ogre::StringConverter::toString(neighborY));
		Ogre::LogManager::getSingletonPtr()->logMessage("angle = " + Ogre::StringConverter::toString(angle));
	}
	
	do {
		setPixelContourLabel(currentPixelID.first, currentPixelID.second, label);
		candidates.clear();
		if (label != 0) {
			//�s un obstacle
			candidates.push_back(std::pair<int, int>(currentPixelID.first, currentPixelID.second + 1));		//pixelS
			candidates.push_back(std::pair<int, int>(currentPixelID.first + 1, currentPixelID.second));		//pixelE
			candidates.push_back(std::pair<int, int>(currentPixelID.first + 1, currentPixelID.second + 1));	//pixelSE

			candidates.push_back(std::pair<int, int>(currentPixelID.first, currentPixelID.second - 1));		//pixelN
			candidates.push_back(std::pair<int, int>(currentPixelID.first - 1, currentPixelID.second));		//pixelW
			candidates.push_back(std::pair<int, int>(currentPixelID.first + 1, currentPixelID.second - 1));	//pixelNE

			candidates.push_back(std::pair<int, int>(currentPixelID.first - 1, currentPixelID.second - 1));	//pixelNW
			candidates.push_back(std::pair<int, int>(currentPixelID.first - 1, currentPixelID.second + 1));	//pixelSW
		}
		else {
			//�s el terra
			candidates.push_back(std::pair<int, int>(currentPixelID.first, currentPixelID.second - 1));		//pixelN
			candidates.push_back(std::pair<int, int>(currentPixelID.first + 1, currentPixelID.second));		//pixelE
			candidates.push_back(std::pair<int, int>(currentPixelID.first + 1, currentPixelID.second - 1));	//pixelNE

			candidates.push_back(std::pair<int, int>(currentPixelID.first, currentPixelID.second + 1));		//pixelS
			candidates.push_back(std::pair<int, int>(currentPixelID.first - 1, currentPixelID.second));		//pixelW
			candidates.push_back(std::pair<int, int>(currentPixelID.first + 1, currentPixelID.second + 1));	//pixelSE

			candidates.push_back(std::pair<int, int>(currentPixelID.first - 1, currentPixelID.second - 1));	//pixelNW
			candidates.push_back(std::pair<int, int>(currentPixelID.first - 1, currentPixelID.second + 1));	//pixelSW
		}
		
		nextFound = false;
		it = candidates.begin(); 

		while (!nextFound && (it != candidates.end())) {
			isContour = pixelContourExist(it->first, it->second);
			if (isContour && (!pixelVisited[it->first][it->second])) {
				nextPixelID = *it;
				nextFound = true;
			}
			it++;
		}
		
		//Comprovo si realment cal crear el v�rtex
		if (!isPixelContourAlligned(currentPixelID.first, currentPixelID.second)) {
			//Si no �s un p�xel aliniat, no el puc eliminar
			lPoint.push_back(std::pair<int, int> (currentPixelID.first, currentPixelID.second));
		}

		pixelVisited[currentPixelID.first][currentPixelID.second] = true;
		currentPixelID = nextPixelID;
		
	} while (nextFound);
	simplifyPolygon(lPoint);
}*/

