#include "ngNavMeshGeneratorBase.h"

using namespace NEOGEN;

#pragma region CREATION AND DESTRUCTION

NavMeshGeneratorBase::NavMeshGeneratorBase(Ogre::SceneManager* scMan) {
	mSceneMgr = scMan;
	_initialized = false;
	navMesh = NULL;
}

NavMeshGeneratorBase::~NavMeshGeneratorBase() {
	mSceneMgr = NULL;
	navMesh = NULL;
}

void NavMeshGeneratorBase::initializeData() {
	//Initialize the predefined portals
	for (EdgeSetID::iterator it = _predefinedPortals.begin(); it != _predefinedPortals.end(); it++) {
		addSubEdge(-1, it->first, it->second, Edge::PORTAL);
	}
}

#pragma endregion

#pragma region GET AND SET

NavMesh* NavMeshGeneratorBase::getNavMesh() {
	return navMesh;
}

void NavMeshGeneratorBase::setPredefinedPortals(EdgeSetID& p) {
	_predefinedPortals = p;
}

void NavMeshGeneratorBase::setData(Poly* f, PolygonList& h) {

	//Inicialitza les dades necess�ries per la construcci� de la navmesh. 
	//Aix� �s el pol�gon que representa el terra i el conjunt de forats

	//f �s un pol�gon simple, sense forats i sense autointerseccions, que 
	//representa el terra del mapa. Els v�rtexs de f es donen en sentit
	//ANTIHORARI. 

	//h �s un conjunt de forats. Tamb� s�n pol�gons simples i els v�rtexs
	//es donen en sentit HORARI. 
	navMesh = new NavMesh();
	setFloor(f);

	holes.clear();
	for (PolygonList::iterator it = h.begin(); it != h.end(); it++) {
		addHole(*it);
	}
}

void NavMeshGeneratorBase::setFloor(Poly *f) {
	if (f == NULL) return;
	if (GeometrySolver::polygonOrientation(*f, NEOGEN_PROJECTION_PLANE) == GeometrySolver::TURN_CWISE) {
		f->reverse();
	}
	floor = f;
	floor->setPolyName("floor");
	addPoly(floor);
	Ogre::LogManager::getSingletonPtr()->logMessage("FLOOR ADDED");

	//floor->draw("navMeshFloor", false);
	//floor->setVisible(true);
}

void NavMeshGeneratorBase::addHole(Poly* h) {
	if (GeometrySolver::polygonOrientation(*h, NEOGEN_PROJECTION_PLANE) == GeometrySolver::TURN_COUNTER_CWISE) {
		h->reverse();
	}
	size_t idHole = holes.size();
	h->setPolyName("h" + std::to_string(idHole));
	holes.push_back(h);
	addPoly(h);

	//h->draw("navMeshHole", false);
	//h->setVisible(true);
}

#pragma endregion

void NavMeshGeneratorBase::addPoly(Poly* p) {
	//Afegeix un pol�gon a la taula de pol�gons
	int idPoly = (int)(polyTable.size());
	int numVert = p->getNumVert();
	
	struct nmPoly nmP;
	nmP.id = idPoly;

	int idEdge;
	int v1ID, v2ID;
		
	//Manually add the first vertex 
	Vertex& firstVertex = p->getVertexAt(0);
	int firstVertexID = navMesh->addVertex(firstVertex.getPosition(), false, firstVertex.getID());
	v1ID = firstVertexID;

	//Construct the edges
	for (int i = 1; i <= numVert; i++) {
		if (i < numVert) {
			//We are creating an intermediate edge
			Vertex& v = p->getVertexAt(i);
			v2ID = navMesh->addVertex(v.getPosition(), false, v.getID());
		}
		else {
			//We are creating the last edge, so both vertices has been already created
			v2ID = firstVertexID;
		}
		idEdge = addSubEdge(idPoly, v1ID, v2ID); 
		nmP.edges.push_back(idEdge); 

		v1ID = v2ID;
	}
	polyTable.push_back(nmP);
}

/*******************************************************************************/
/* CONSTRUCCI� TAULA ARESTES */
/*******************************************************************************/
int NavMeshGeneratorBase::addSubEdge(int idP, int v1, int v2, Edge::Type eType) {
	//Afegeix una aresta a la taula d'arestes. Els v�rtexs inicials i finals ja han estat creats
	int idEdge = navMesh->addSubEdge(idP, v1, v2, eType);
	candidateEdges.insert(idEdge);
	return idEdge;
}

/**************************************************************************************/
/* PORTAL SPLITTING */
/**************************************************************************************/
void NavMeshGeneratorBase::portalSplit() {
	Ogre::LogManager::getSingletonPtr()->logMessage("numPortalsBeforeSplit = " + std::to_string(portals.size()));
	for (PortalSet::iterator it = portals.begin(); it != portals.end(); it++) {
		//Ogre::LogManager::getSingletonPtr()->logMessage("SPLITTING PORTAL = " + std::to_string(*it));
		Edge* p = &(navMesh->getEdge(*it));
		if (p->opposite == -1) {
			int v1 = navMesh->getEdge(*it).v1;
			int v2 = navMesh->getEdge(*it).v2;
			
			int idSub1 = addSubEdge(-1, v2, v1, Edge::PORTAL);

			navMesh->getEdge(idSub1).opposite = *it;
			navMesh->getEdge(*it).opposite = idSub1;
		}
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("numPortalsAfterSplit = " + std::to_string(portals.size()));
}

#pragma region CPG GENERATION

void NavMeshGeneratorBase::createCPG() {
	//1) split each portal edge into two oriented edges sharing the same endpoints but in 
	//opposite directions
	portalSplit();

	//2) Reconstruct the cells that forms the CPG
	reconstructCells();
}

void NavMeshGeneratorBase::reconstructCells() {
	//Reconstreix les cel�les convexes que formen el graf. 

	//Estrat�gia: Comen�o per una aresta qualsevol i vaig
	//recorrent les arestes ve�nes, agafant a cada iteraci�
	//la que forma un angle menor amb la actual, fins que
	//torno a trobar l'aresta amb la que he comen�at
		
	std::vector<Vertex> listVert;	//Llista de v�rtexs que forma la cel�la
	std::vector<int> listEdges;	//Llista d'arestes que forma la cel�la
	std::vector<int> newCells;	//Guarda els identificadors de les noves cel�les que s'han creat
	std::set<int> visitedEdges;	//Indica quines arestes han estat ja visitades
	
	for (std::set<int>::iterator it = candidateEdges.begin(); it != candidateEdges.end(); it++) {	//recorro totes les arestes
		Edge* eCurrent = &(navMesh->getEdge(*it));	//The edge currently visited
		if (visitedEdges.find(eCurrent->id) == visitedEdges.end()) {
			//He trobat una aresta que encara no ha estat visitada
			int startId = eCurrent->id;	//Identifies the starting edge
			Ogre::LogManager::getSingletonPtr()->logMessage("RECONSTRUCTING CELL");
			Ogre::LogManager::getSingletonPtr()->logMessage("startID = " + std::to_string(startId));
			listVert.clear();
			listEdges.clear();
			do {
				int next = chooseNextEdge(eCurrent);
				listVert.push_back(navMesh->getVertex(eCurrent->v1));
				listEdges.push_back(eCurrent->id);
				eCurrent = &(navMesh->getEdge(next));
				visitedEdges.insert(eCurrent->id);
			} while (eCurrent->id != startId);
			int cellID = navMesh->addCell(listVert, listEdges);
			newCells.push_back(cellID);
			numCurrentCells ++;
		}
	}
	//WARNING!!!
	//Aquest codi nom�s �s per comprobar que tots els v�xels accessibles estiguin almenys en alguna cel�la
	/*Voxel* v;
	for (std::set<Voxel*>::iterator it = voxelDescriptor.begin(); it != voxelDescriptor.end(); it++) {
		v = *it;
		if (v->numCells() == 0) {
			Ogre::LogManager::getSingletonPtr()->logMessage("EMPTY ACCESSIBLE VOXEL!");
			v->draw("matVoxelNegative");
		}
	}*/
	/*Voxel* v = VoxelGrid::getSingleton().getVoxelFromPos(Ogre::Vector3(15.006210f, -6.2303853f, 11.389488f));
	v->draw("matVoxelNegative");
	while (v->getType() != Voxel::TYPE_OUT) {
		Ogre::LogManager::getSingletonPtr()->logMessage("===============================");
		Ogre::LogManager::getSingletonPtr()->logMessage("x = " + std::to_string(v->keyX));
		Ogre::LogManager::getSingletonPtr()->logMessage("y = " + std::to_string(v->keyY));
		Ogre::LogManager::getSingletonPtr()->logMessage("z = " + std::to_string(v->keyZ));
		Ogre::LogManager::getSingletonPtr()->logMessage("type = " + std::to_string(v->getType()));
		v = VoxelGrid::getSingleton().getVoxel(v->keyX, v->keyY - 1, v->keyZ);
	}*/

	//Calculo l'adjac�ncia de les noves cel�les
	Ogre::LogManager::getSingletonPtr()->logMessage("COMPUTING ADJACENCY");
	for (std::vector<int>::iterator it = newCells.begin(); it != newCells.end(); it++) {
		navMesh->computeAdjacency(*it);
	}
}

int NavMeshGeneratorBase::chooseNextEdge(Edge *eCurrent) {

	float angle = 400.0f;	//angle de l'aresta que escollim
	float angleAux;
	int next = -1;		//identificador de la seg�ent aresta que escollim

	//Les arestes que m'interessen s�n aquelles que tenen com a PUNT INICIAL el 
	//PUNT FINAL de l'aresta actual

	IncidentEdgesList v2Edges = navMesh->getVertex(eCurrent->v2).getIncidentEdges();	//arestes incidents a v2
	Edge* e;

	for (size_t i = 0; i < v2Edges.size(); i++) {
		e = &(navMesh->getEdge(v2Edges.at(i)));
		
		if ((e->v1 == eCurrent->v2) && (e->id != eCurrent->opposite)) { 
			//L'aresta comen�a amb el v�rtex final de l'aresta actual. 
			//Cal comprobar el seu angle. 
			angleAux = navMesh->angleBetweenEdges(eCurrent->v2, eCurrent->id, e->id);
			if (angleAux < angle) {
				angle = angleAux;
				next = e->id;
			}
		}
	}
	return next;
}

#pragma endregion

void NavMeshGeneratorBase::test() {
	Ogre::LogManager::getSingletonPtr()->logMessage("TEST OK!!!");
}