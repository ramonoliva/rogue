#include "NeoClipper.h"

using namespace NEOGEN;
using namespace ClipperLib;
using namespace std;
using namespace NeoGeometrySolver;

#pragma region GET AND SET

void NeoClipper::addPolygonSubject(const Polygon3& polygon) {
	//Add the subject polygon to the clipper
	Path subj;
	toClipper(polygon, subj);
	
	_clipper.AddPath(subj, PolyType::ptSubject, true);
}

void NeoClipper::addPolygonsClip(const Polygons3& clippers) {
	for (Polygons3::const_iterator it = clippers.begin(); it != clippers.end(); it++) {
		addPolygonClip(*it);
	}
}

void NeoClipper::addPolygonClip(const Polygon3& polygon) {
	//Add the clipping polygon to the clipper
	Path path;
	toClipper(polygon, path);
	
	//Offset a little bit the clipping polygons, as clipper requires closed clip polygons. 
	//This way, we are robust against line obstacles formed by perpendicular faces. 
	Paths offsetSolution;
	ClipperOffset cOffset;
	cOffset.AddPath(path, JoinType::jtRound, EndType::etClosedPolygon);
	cOffset.Execute(offsetSolution, 1.0);

	_clipper.AddPaths(offsetSolution, PolyType::ptClip, true);
}

#pragma endregion

#pragma region UPDATE

void NeoClipper::applyCeilConstraints(const Polygon3& subject, const Polygons3& ceilConstraints, PolyTree& solution) {
	_clipper.Clear();
	addPolygonSubject(subject);
	addPolygonsClip(ceilConstraints);

	_clipper.Execute(ClipType::ctDifference, solution, PolyFillType::pftNonZero, PolyFillType::pftNonZero);
}

void NeoClipper::execute(const Polygon3& polygonA, const Polygon3& polygonB, NeoClipper::Operation op, Polygon3& floor, Polygons3& holes) {
	_clipper.Clear();

	//Add the subject polygon
	Path pathA;
	toClipper(polygonA, pathA);
	_clipper.AddPath(pathA, PolyType::ptSubject, true);

	//Add the clip polygon
	Path pathB;
	toClipper(polygonB, pathB);
	_clipper.AddPath(pathB, PolyType::ptClip, true);

	//Execute the desired operation
	PolyTree solution;
	_clipper.Execute(op, solution, PolyFillType::pftNonZero, PolyFillType::pftNonZero);
}

#pragma endregion