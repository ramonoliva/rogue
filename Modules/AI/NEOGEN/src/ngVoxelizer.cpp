#include "ngVoxelizer.h"
#include "ngTriBoxOverlap.h"
#include "ngGlobalVars.h"

using namespace NEOGEN;

/********************************************************************************/
/* CREACIO I DESTRUCCIO*/
/********************************************************************************/
Voxelizer::Voxelizer(Ogre::SceneManager* mgr) {
	mSceneMgr = mgr;
	sceneNavMesh = new NavMesh();
	voxelWalkable = NULL;
	voxelizationMode = VOXELIZATION_MODE_GPU_APPROXIMATE;
}

void Voxelizer::createGrid(Ogre::Entity* ent, float charRadius, float maxStepDepth) {
	//Creo el manual object que representar� el model de voxels
	VoxelGrid::getSingleton().createGrid(ent, charRadius, maxStepDepth);
	charHeight = 2.0f;
	numVoxelsCharDepth = (size_t)(Ogre::Math::Ceil(charHeight / getVoxelDim().y));
}

int Voxelizer::getVoxelizationMode() {
	return voxelizationMode;
}

float Voxelizer::getCharHeight() {
	return charHeight;
}

void Voxelizer::initGridData(SliceMap* sMapPositive, SliceMap* sMapNegative) {
	Ogre::LogManager::getSingletonPtr()->logMessage("INITIALIZING GRID DATA");
	/*Voxel* v;
	for (size_t x = 0; x < sliceMapWidth; x++) {
		for (size_t y = 0; y < sliceMapDepth; y++) {
			for (size_t z = 0; z < sliceMapHeight; z++) {
				v = VoxelGrid::getSingleton().getVoxel(x, y, z);
				if (sMapPositive->isSolidVoxel(x, y, z)) VoxelGrid::getSingleton().setVoxelType(x, y, z, Voxel::TYPE_POSITIVE);
				else if (sMapNegative->isSolidVoxel(x, y, z)) VoxelGrid::getSingleton().setVoxelType(x, y, z, Voxel::TYPE_NEGATIVE);
			}
		}
	}*/
}

Voxelizer::~Voxelizer() {
	entModel = NULL;
	mSceneMgr = NULL;
	voxelWalkable = NULL;
}

void Voxelizer::printVoxelPointerTable(VoxelPointerTable& vTable, const Ogre::String& tableName) {
	Ogre::LogManager::getSingletonPtr()->logMessage("==========================================");
	Ogre::LogManager::getSingletonPtr()->logMessage("PRINTING VOXEL TABLE " + tableName);
	Ogre::LogManager::getSingletonPtr()->logMessage("==========================================");
	Ogre::LogManager::getSingletonPtr()->logMessage("numVoxels = " + std::to_string(vTable.size()));
	for (VoxelPointerTable::iterator it = vTable.begin(); it != vTable.end(); it++) {
		Ogre::LogManager::getSingletonPtr()->logMessage("SOLID VOXEL");
		Ogre::LogManager::getSingletonPtr()->logMessage("x = " + std::to_string(it->x));
		Ogre::LogManager::getSingletonPtr()->logMessage("y = " + std::to_string(it->y));
		Ogre::LogManager::getSingletonPtr()->logMessage("z = " + std::to_string(it->z));
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("==========================================");
}

void Voxelizer::printSceneLayers() {
	Ogre::LogManager::getSingletonPtr()->logMessage("==========================================");
	Ogre::LogManager::getSingletonPtr()->logMessage("PRINTING SCENE LAYERS");
	Ogre::LogManager::getSingletonPtr()->logMessage("==========================================");
	Ogre::LogManager::getSingletonPtr()->logMessage("numLayers = " + std::to_string(sceneLayers.size()));
	for (LayerTable::iterator it = sceneLayers.begin(); it != sceneLayers.end(); it++) {
		printLayer(it->first);
	}
}

void Voxelizer::printLayer(int layerID) {
	Layer* layer = sceneLayers[layerID];
	Ogre::LogManager::getSingletonPtr()->logMessage("==========================================");
	Ogre::LogManager::getSingletonPtr()->logMessage("PRINTING LAYER = " + std::to_string(layerID));
	Ogre::LogManager::getSingletonPtr()->logMessage("==========================================");
	Ogre::LogManager::getSingletonPtr()->logMessage("LAYER VOXELS = " + std::to_string(layer->voxels.size()));
	Ogre::String sX, sY, sZ;
	for (std::set<Voxel*>::iterator it = layer->voxels.begin(); it != layer->voxels.end(); it++) {
		sX = std::to_string((*it)->keyX);
		sY = std::to_string((*it)->keyY);
		sZ = std::to_string((*it)->keyZ);
		Ogre::LogManager::getSingletonPtr()->logMessage("(" + sX + ", " + sY + ", " + sZ + ")");
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("PORTAL VOXELS = " + std::to_string(layer->portalVoxels.size()));
	Ogre::LogManager::getSingletonPtr()->logMessage("NEIGHBOR LAYERS");
	for (std::set<int>::iterator it = layer->neighbors.begin(); it != layer->neighbors.end(); it++) {
		Ogre::LogManager::getSingletonPtr()->logMessage(std::to_string(*it));
	}
}

/********************************************************************************/
/* GET I SET*/
/********************************************************************************/
Ogre::ManualObject* Voxelizer::getManualPerpendicularFacesIntersection() {
	return manualPerpendicularFacesIntersection;
}

NavMesh* Voxelizer::getSceneNavMesh() {
	return sceneNavMesh;
}

size_t Voxelizer::getVoxelGridSizeX() {
	return VoxelGrid::getSingleton().getSizeX();
}

size_t Voxelizer::getVoxelGridSizeY() {
	return VoxelGrid::getSingleton().getSizeY();
}

size_t Voxelizer::getVoxelGridSizeZ() {
	return VoxelGrid::getSingleton().getSizeZ();
}

Layer* Voxelizer::getActiveLayer() {
	//Retorna la layer actualment activa
	return (activeLayerIt->second);
}

Ogre::Vector3& Voxelizer::getVoxelDim() {
	return VoxelGrid::getSingleton().getVoxelDim();
}

float Voxelizer::getVoxelDimX() {
	return VoxelGrid::getSingleton().getVoxelDim().x;
}

float Voxelizer::getVoxelDimY() {
	return VoxelGrid::getSingleton().getVoxelDim().y;
}

float Voxelizer::getVoxelDimZ() {
	return VoxelGrid::getSingleton().getVoxelDim().z;
}

size_t Voxelizer::getNumVoxelsCharDepth() {
	return numVoxelsCharDepth;
}

/********************************************************************************/
/* OPERACIONS DE CLASSE */
/********************************************************************************/
bool Voxelizer::selectWalkableArea(const Ogre::Ray& ray) {
	Ogre::LogManager::getSingletonPtr()->logMessage("SELECTING WALKABLE AREA");
	std::pair<bool, Ogre::Real> intersection = Ogre::Math::intersects(ray, VoxelGrid::getSingleton().getBoundingBox());
	bool walkableVoxelHit = false;
	if (intersection.first) {
		//El raig ha intersecat amb la BB del model
		//Calculo el voxel inicial
		Ogre::LogManager::getSingletonPtr()->logMessage("MODEL HIT!");
		Ogre::Vector3 realPos = ray.getPoint(intersection.second);	//Coordenades reals punt d'intersecci�
		//Transformo les coordenades al sistema de coordenades del slicemap
		Voxel* initialVoxel = VoxelGrid::getSingleton().getVoxelFromPos(realPos);
		walkableVoxelHit = setVoxelSeed(ray, initialVoxel);
	}
	return walkableVoxelHit;
}

bool Voxelizer::setVoxelSeed(const Ogre::Ray& ray, Voxel* initialVoxel) {
	//Comprovo si algun ve� del v�xel actual intersecciona amb el raig
	//El v�xel actual �s el v�xel central
	Ogre::LogManager::getSingletonPtr()->logMessage("SETTING VOXEL SEED");
	std::vector<Voxel*> voxelList;
	voxelList.push_back(initialVoxel);
	bool solidVoxelFound = false;
	bool rayIntersectsVoxel;
	bool walkableVoxelHit = false;
	std::set<Voxel*> traversedVoxels;
	std::set<Voxel*>::iterator itFound;
	Voxel* v;
	Voxel* vNeighbor;
	bool solidVoxelAccessible, solidVoxelNegative;
	//Segueixo fins que hagi comprobat tots els v�xels que interseccionen amb el raig, o be fins que he trobat un v�xel s�lid que intersecciona amb el raig
	while (!voxelList.empty() && !solidVoxelFound) {	
		v = voxelList.back();
		voxelList.pop_back();
		traversedVoxels.insert(v);
		solidVoxelAccessible = (v->getType() == Voxel::TYPE_ACCESSIBLE);
		solidVoxelNegative = (v->getType() == Voxel::TYPE_NEGATIVE);
		solidVoxelFound = (solidVoxelAccessible || solidVoxelNegative);
		//drawVoxelEnt(Voxel::TYPE_NEGATIVE, currentVoxelPos.x, currentVoxelPos.y, currentVoxelPos.z);
		if (solidVoxelFound) {
			if (solidVoxelAccessible) voxelWalkable = v;
		}
		else {	
			//El v�xel actual no �s s�lid. Comprovo els ve�ns
			//Ogre::LogManager::getSingletonPtr()->logMessage("CURRENT VOXEL IS NOT SOLID!");
			//Ogre::LogManager::getSingletonPtr()->logMessage("currentVoxel = " + std::to_string(currentVoxelPos));
			for (int offsetX = -1; offsetX <= 1; offsetX++) {
				for (int offsetY = -1; offsetY <= 1; offsetY++) {
					for (int offsetZ = -1; offsetZ <= 1; offsetZ++) {
						if ((offsetX == 0) && (offsetY == 0) && (offsetZ == 0)) continue;
						vNeighbor = VoxelGrid::getSingleton().getVoxel(v->keyX + offsetX, v->keyY + offsetY, v->keyZ + offsetZ);
						if (vNeighbor->getType() == Voxel::TYPE_OUT) continue;
						itFound = traversedVoxels.find(vNeighbor);
						if (itFound == traversedVoxels.end()) {
							//El v�xel no s'havia travessat encara
							//Ogre::LogManager::getSingletonPtr()->logMessage("VOXEL NOT TRAVERSED!");
							//Ogre::LogManager::getSingletonPtr()->logMessage("voxelPos = " + std::to_string(voxelPos));
							traversedVoxels.insert(vNeighbor);
							rayIntersectsVoxel = ray.intersects(*vNeighbor).first;
							if (rayIntersectsVoxel) {
								//Ogre::LogManager::getSingletonPtr()->logMessage("RAY VOXEL INTERSECTION!");
								voxelList.push_back(vNeighbor);
							}
						}
					}
				}
			}
		}
	}
	if (solidVoxelAccessible) {
		Ogre::LogManager::getSingletonPtr()->logMessage("ACCESSIBLE VOXEL HIT!");
		//El raig intersecta amb un v�xel s�lid i positiu, indicat per l'usuari, que sap segur
		//que pertany a l'�rea caminable. 
		appTimeBefore = globalTimer->getMilliseconds();
		
		layerSubdivision();
		
		appTimeAfter = globalTimer->getMilliseconds();
		appTimeDiff = appTimeAfter - appTimeBefore;
		appTime += appTimeDiff;
		Ogre::LogManager::getSingletonPtr()->logMessage("TIME STAGE layerSubdivision = " + std::to_string((float)appTimeDiff / 1000.0f));

		walkableVoxelHit = true;
	}
	return walkableVoxelHit;
}

/********************************************************************************************************/
/* LAYER SUBDIVISION */
/********************************************************************************************************/
void Voxelizer::setVoxelLayer(Voxel* v, int layerID) {
	if (isTopVoxelAccessible(v->keyX, v->keyY, v->keyZ)) {
		//�s un v�xel positiu i accessible
		sceneLayers.at(layerID)->voxels.insert(v);
		std::pair<int, int> column(v->keyX, v->keyZ);
		addColumnLayer(column, layerID); 
		VoxelGrid::getSingleton().setVoxelType(v->keyX, v->keyY, v->keyZ, Voxel::TYPE_ACCESSIBLE);
	}
	if (v->isPortal()) sceneLayers[layerID]->portalVoxels.push_back(v);
	v->setLayer(layerID);
}

void Voxelizer::addColumnLayer(std::pair<int, int>& column, int layerID) {
	//Afegeix la layerID a la llista de layers incidents de la columna column
	std::set<int>& layersInColumn = columnLayers[column];	//El conjunt actual de layers incidents a la columna
	//Totes aquestes layers entren en conflicte amb la nova layer i la layerID, t� conflicte amb
	//totes aquestes layers. 
	for (std::set<int>::iterator it = layersInColumn.begin(); it != layersInColumn.end(); it++) {
		sceneLayers[*it]->conflictLayers.insert(layerID);
		sceneLayers[layerID]->conflictLayers.insert(*it);
	}
	columnLayers[column].insert(layerID);
}

void Voxelizer::eraseColumnLayer(std::pair<int, int>& column, int layerID) {
	//Elimina la layerID de la llista de layers incidents de la columna column
	std::set<int>& incidentLayers = columnLayers[column];	//El conjunt actual de layers incidents a la columna
	incidentLayers.erase(layerID);
	//Totes aquestes layers tenien conflicte amb la layerID i la layerID, tenia conflicte amb
	//totes aquestes layers. 
	for (std::set<int>::iterator it = incidentLayers.begin(); it != incidentLayers.end(); it++) {
		sceneLayers[*it]->conflictLayers.erase(layerID);
		sceneLayers[layerID]->conflictLayers.erase(*it);
	}
}

bool Voxelizer::layerInColumn(std::pair<int, int>& column, int layerID) {
	//Indica si la layerID es troba en la columna column
	if ((column.first < 0) || (column.first >= (int)VoxelGrid::getSingleton().getSizeX())) return false;
	if ((column.second < 0) || (column.second >= (int)VoxelGrid::getSingleton().getSizeZ())) return false;

	//Si arribo aqu� �s perqu� l'identificador de la columna est� dins dels l�mits de la voxelgrid
	std::set<int>& incidentLayers = columnLayers[column];		//El conjunt actual de layers incidents a la columna
	return (incidentLayers.find(layerID) != incidentLayers.end());
}

void Voxelizer::createLayer(int layerID) {
	//Crea una nova layer
	//sceneLayers[layerID];
	sceneLayers.insert(std::pair<int, Layer*>(layerID, new Layer(layerID)));
}

void Voxelizer::propagateLayerID(Voxel* currentVoxel, std::vector<Voxel*>& accessibleNeighbors, std::vector<Voxel*>& obstacleNeighbors) {
	//Intento propagar el identificador de capa del v�xel actual als ve�ns. 
	//Tots els v�xels que cont� la taula neighbors s�n POSITIUS o ACCESSIBLES
	Voxel* neighborVoxel;
	for (std::vector<Voxel*>::iterator it = accessibleNeighbors.begin(); it != accessibleNeighbors.end(); it ++) {
		neighborVoxel = *it;
		propagateLayerIDAccessible(currentVoxel, neighborVoxel);
	}
	for (std::vector<Voxel*>::iterator it = obstacleNeighbors.begin(); it != obstacleNeighbors.end(); it++) {
		neighborVoxel = *it;
		propagateLayerIDObstacle(currentVoxel, neighborVoxel);
	}
}

void Voxelizer::propagateLayerIDAccessible(Voxel* currentVoxel, Voxel* neighborVoxel) {
	int layerCurrentVoxel = currentVoxel->getLayer();	//Nota: El valor de la layer del v�xel actual pot haver canviat durant el proc�s de merging
	int layerNeighborVoxel = neighborVoxel->getLayer();
	if (layerNeighborVoxel == LAYER_INVALID) {
		//El ve� pertany a la LAYER_INVALID, �s a dir, cap v�xel li ha transm�s l'etiqueta encara. 
		//Per tant, el v�xel actual li pot transmetre la seva etiqueta si en la columna on es 
		//troba el v�xel ve�, cap altra v�xel t� ja aquesta etiqueta que vull transmetre. 
		std::pair<int, int> column(neighborVoxel->keyX, neighborVoxel->keyZ);
		if (!layerInColumn(column, layerCurrentVoxel)) {
			setVoxelLayer(neighborVoxel, layerCurrentVoxel);
		}
		else {
			//Ja hi havia un v�xel en la mateixa columna que el ve� amb l'etiqueta que estem
			//intentant transmetre. Per tant, el v�xel actual �s un v�xel portal. 
			addVoxelPortal(currentVoxel);
		}
	}
	else if (layerNeighborVoxel != layerCurrentVoxel) {
		//El ve� ja pertany a una layer, per tant, cal resoldre el conflicte, �s a dir, veure si 
		//les dues layers es poden unir. 
		bool layersMerged = mergeLayers(layerCurrentVoxel, layerNeighborVoxel);
		if (!layersMerged) {
			//No he pogut unir les layers. Per tant, aquest v�xel forma part del portal
			//entre les dues layers
			addVoxelPortal(currentVoxel);
		}
	}
	//else, les dues etiquetes s�n iguals, per tant no cal fer res
}

void Voxelizer::propagateLayerIDObstacle(Voxel* currentVoxel, Voxel* neighborVoxel) {
	//Comprobo si neighborVoxel �s un topVoxel, �s a dir, si com a m�nim hi ha un v�xel no positiu ni accessible a sobre
	int layerCurrentVoxel = currentVoxel->getLayer();
	neighborVoxel->setLayer(layerCurrentVoxel);
	sceneLayers[layerCurrentVoxel]->voxels.insert(neighborVoxel);
	neighborVoxel->setType(Voxel::TYPE_CONTOUR);
	neighborVoxel->setContour(true);
}

void Voxelizer::addVoxelPortal(Voxel* v) {
	v->setPortal(true);
	Layer* layer = sceneLayers[v->getLayer()];
	layer->portalVoxels.push_back(v);
}

bool Voxelizer::isTopVoxelAccessible(int x, int y, int z) {
	//Comprova si un voxel POSITIU �s accessible, �s a dir, si el v�xel
	//t� prous v�xels buits damunt seu per que hi pugui cabre un personatge. 
	Voxel* v = VoxelGrid::getSingleton().getVoxel(x, y, z);
	int voxelType = v->getType();
	if ((voxelType == Voxel::TYPE_NEGATIVE) || (voxelType == Voxel::TYPE_OUT) || (voxelType == Voxel::TYPE_EMPTY)) return false;	//Si el v�xel est� fora dels l�mits o �s buit, no �s accessible
	if ((voxelType == Voxel::TYPE_ACCESSIBLE)) return true;	//El v�xel ja ha estat marcat com a accessible
	//Comprobo si t� prous v�xels buits per damunt seu	
	bool emptyVoxel = true;
	int type;
	size_t i = 1;
	while (emptyVoxel && (i <= numVoxelsCharDepth)) {
		type = VoxelGrid::getSingleton().getVoxel(x, y + i, z)->getType();
		emptyVoxel = ((type == Voxel::TYPE_EMPTY) || (type == Voxel::TYPE_OUT));
		i++;
	}
	return emptyVoxel;
}

bool Voxelizer::mergeLayers(int layerID1, int layerID2) {
	//Uneix les layers ID1 i ID2 si s�n compatibles. 
	//Ogre::LogManager::getSingletonPtr()->logMessage("MERGING LAYERS");
	bool layersMerged = false;
	//FASE0: Comprobo quina �s la layer amb menys v�xels, per accelerar el proc�s de merging
	int layerDestinationID, layerSourceID;
	if ((sceneLayers[layerID1]->voxels.size()) < (sceneLayers[layerID2]->voxels.size())) { 
		//La layer1 t� menys v�xels que la layer2
		layerDestinationID = layerID1;
		layerSourceID = layerID2;
	}
	else {
		layerDestinationID = layerID2;
		layerSourceID = layerID1;
	}
	Layer* layerDestination = sceneLayers[layerDestinationID];	//Layer que ser� el resultat del merge
	Layer* layerSource = sceneLayers[layerSourceID];			//Layer que desapareixer�
	std::pair<int, int> column;
	if (!(layerDestination->inConflict(layerSourceID))) {
		//NO hi ha conflicte. Per tant, les dues layers es poden unir. 
		//FASE1: VOXEL MERGING. Tots els v�xels de layerSource passen a ser v�xels de layerDestination
		std::set<Voxel*>& vTableSource = layerSource->voxels;	//Els v�xels accessibles de la layerSource
		Voxel* vSource;
		for (std::set<Voxel*>::iterator it = vTableSource.begin(); it != vTableSource.end(); it++) {
			vSource = *it;
			column.first = vSource->keyX; 
			column.second = vSource->keyZ; 
			eraseColumnLayer(column, layerSourceID);
			//Ogre::LogManager::getSingletonPtr()->logMessage("problematicVoxelLayerBefore = " + std::to_string(VoxelGrid::getSingleton().getVoxel(20, 12, 15)->getLayer()));
			setVoxelLayer(vSource, layerDestinationID);
			//Ogre::LogManager::getSingletonPtr()->logMessage("problematicVoxelLayerBefore = " + std::to_string(VoxelGrid::getSingleton().getVoxel(20, 12, 15)->getLayer()));
		}

		//FASE1: CONTOUR MERGING. Tots els v�xels contorn de layerSource passen a ser v�xels contorn de layerDestination
		std::set<Voxel*>& vContourSource = layerSource->voxels;	//Els v�xels accessibles de la layerSource
		for (std::set<Voxel*>::iterator it = vContourSource.begin(); it != vContourSource.end(); it++) {
			vSource = *it;
			vSource->setLayer(layerDestinationID);
			sceneLayers[layerDestinationID]->voxels.insert(vSource);
		}
		
		//FASE2: NEIGHBOR MERGING. Totes les layers ve�nes de layerSource, passen a ser layers ve�nes de layerDestination
		std::set<int>& neighborsDestination = layerDestination->neighbors;
		std::set<int>& neighborsSource = layerSource->neighbors;
		for (std::set<int>::iterator it = neighborsSource.begin(); it != neighborsSource.end(); it++) {
			neighborsDestination.insert(*it);						//El ve� de la layerSource passa a ser ve� de la layerDestination
			sceneLayers[*it]->neighbors.erase(layerSourceID);		//El ve� de la layerSource deixa de tenir com a ve� la layerSource
			sceneLayers[*it]->neighbors.insert(layerDestinationID);	//El ve� de la layerSource passa a tenir com a ve� la layerDestination
		}

		//FASE3: DELETE LAYER. Elimino la layerSource de la taula de layers. 
		delete sceneLayers[layerSourceID];
		sceneLayers.erase(layerSourceID);	//Quan he acabat, destrueixo la layer source
		//invalidLayers.push_back(layerSourceID);
		layersMerged = true;
	}
	else {
		//Hi ha conflicte, per� les dues layers estan en contacte. Per tant, s�n ve�nes
		layerDestination->neighbors.insert(layerSourceID);
		layerSource->neighbors.insert(layerDestinationID);
	}
	//Ogre::LogManager::getSingletonPtr()->logMessage("LAYERS MERGED");
	return layersMerged;
}

void Voxelizer::layerSubdivision() {
	//Fa un proc�s de flooding ordenat, comen�ant pel v�xel positiu amb una y m�s baixa i va pujant. 
	//D'aquesta forma, es creen les layers i es detecta l'�rea caminable al mateix temps
	Ogre::LogManager::getSingletonPtr()->logMessage("SUBDIVIDING ACCESSIBLE SPACE INTO LAYERS");
	//printVoxelPointerTable(positiveVoxels, "POSITIVE VOXELS");
	int neighborX, neighborY, neighborZ;
	int currentLayerID = 0;	//La layer que estem transmetent actualment
	Voxel* currentVoxel;
	Voxel* neighborVoxel;
	std::vector<Voxel*> accessibleNeighbors;
	std::vector<Voxel*> obstacleNeighbors;
	bool currentVoxelAccessible;
	VoxelPointerTable& accessibleVoxels = VoxelGrid::getSingleton().getAccessibleVoxels();
	for (VoxelPointerTable::iterator it = accessibleVoxels.begin(); it != accessibleVoxels.end(); it++) {
		currentVoxel = VoxelGrid::getSingleton().getVoxel(it->x, it->y, it->z);
		currentVoxelAccessible = isTopVoxelAccessible(it->x, it->y, it->z);
		if (currentVoxelAccessible) {
			if (currentVoxel->getLayer() == LAYER_INVALID) {
				//He trobat un nou punt de flooding, un v�xel accessible que encara no estava etiquetat. Creo una nova layer
				/*Ogre::LogManager::getSingletonPtr()->logMessage("NEW SEED FOUND");
				Ogre::LogManager::getSingletonPtr()->logMessage("x = " + std::to_string(vPointer.x));
				Ogre::LogManager::getSingletonPtr()->logMessage("y = " + std::to_string(vPointer.y));
				Ogre::LogManager::getSingletonPtr()->logMessage("z = " + std::to_string(vPointer.z));*/
				createLayer(currentLayerID);
				setVoxelLayer(currentVoxel, currentLayerID);
				currentLayerID++;
			}
			accessibleNeighbors.clear();
			obstacleNeighbors.clear();
			//Apilo els ve�ns del v�xel (26-connectivity) que s�n POSITIUS o ja han estat marcats com a ACCESSIBLES
			//Ve�ns FRONT I BACK:
			for (int offsetX = -1; offsetX <= 1; offsetX ++) {
				for (int offsetY = 0; offsetY <= 1; offsetY++) {
					for (int offsetZ = -1; offsetZ <= 1; offsetZ++) {
						if ((offsetX == 0) && (offsetZ == 0)) continue;
						neighborX = it->x + offsetX;
						neighborY = it->y + offsetY;
						neighborZ = it->z + offsetZ;
						neighborVoxel = VoxelGrid::getSingleton().getVoxel(neighborX, neighborY, neighborZ);
						if (currentVoxel->getLayer() != neighborVoxel->getLayer()) {
							if (isTopVoxelAccessible(neighborX, neighborY, neighborZ)) {
								//El ve� �s accessible
								accessibleNeighbors.push_back(neighborVoxel);
							}
							//else if ((offsetY == 0) && (neighborVoxel->getType() == Voxel::TYPE_NEGATIVE)) {
							else if (neighborVoxel->getType() == Voxel::TYPE_NEGATIVE) {
								obstacleNeighbors.push_back(neighborVoxel);
							}
						}
					}
				}
			}
			/*//Apilo els ve�ns del v�xel (14-connectivity) que s�n POSITIUS o ja han estat marcats com a ACCESSIBLES
			//Ve�ns FRONT I BACK:
			for (int offsetZ = -1; offsetZ <= 1; offsetZ += 2) {
				for (int offsetY = -1; offsetY <= 1; offsetY++) {
					neighborX = vPointer.x;
					neighborY = vPointer.y + offsetY;
					neighborZ = vPointer.z + offsetZ;
					neighborVoxel = VoxelGrid::getSingleton().getVoxel(neighborX, neighborY, neighborZ);
					if (isTopVoxelAccessible(neighborX, neighborY, neighborZ)) {
						neighbors.insert(VoxelPointer(neighborX, neighborY, neighborZ));
					}
				}
			}
			//Ve�ns LEFT I RIGHT:
			for (int offsetX = -1; offsetX <= 1; offsetX += 2) {
				for (int offsetY = -1; offsetY <= 1; offsetY++) {
					neighborX = vPointer.x + offsetX;
					neighborY = vPointer.y + offsetY;
					neighborZ = vPointer.z;
					neighborVoxel = VoxelGrid::getSingleton().getVoxel(neighborX, neighborY, neighborZ);
					if (isTopVoxelAccessible(neighborX, neighborY, neighborZ)) {
						neighbors.insert(VoxelPointer(neighborX, neighborY, neighborZ));
					}
				}
			}
			//Ve�ns UP I DOWN:
			for (int offsetY = -1; offsetY <= 1; offsetY+=2) {
				neighborX = vPointer.x;
				neighborY = vPointer.y + offsetY;
				neighborZ = vPointer.z;
				neighborVoxel = VoxelGrid::getSingleton().getVoxel(neighborX, neighborY, neighborZ);
				if (isTopVoxelAccessible(neighborX, neighborY, neighborZ)) {
					neighbors.insert(VoxelPointer(neighborX, neighborY, neighborZ));
				}
			}*/
			propagateLayerID(currentVoxel, accessibleNeighbors, obstacleNeighbors); //Propago l'identificador del layer a tots els ve�ns
		}
		else {
			//VoxelGrid::getSingleton().setVoxelType(vPointer.x, vPointer.y, vPointer.z, Voxel::TYPE_EMPTY);
		}
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("LAYERS CREATED");
	drawSceneLayers();
	//drawVoxelEnt(Voxel::TYPE_ACCESSIBLE, 47, 4, 70);
	//drawVoxelizedModel();
	Ogre::LogManager::getSingletonPtr()->logMessage("LAYERS CREATED");
	Ogre::LogManager::getSingletonPtr()->logMessage("TOTAL LAYERS = " + std::to_string(sceneLayers.size()));
	Ogre::LogManager::getSingletonPtr()->logMessage("currentLayerID = " + std::to_string(currentLayerID));
}

void Voxelizer::detectAccessibleVoxels() {
	VoxelPointerTable initialPositiveVoxels = VoxelGrid::getSingleton().getPositiveVoxels();
	Voxel* vUP;
	Voxel* v;
	bool obstacleFound;
	int offsetY;
	for (VoxelPointerTable::iterator it = initialPositiveVoxels.begin(); it != initialPositiveVoxels.end(); it++) {
		vUP = VoxelGrid::getSingleton().getVoxel(it->x, it->y + 1, it->z);
		if ((vUP->getType() == Voxel::TYPE_OUT) || (vUP->getType() == Voxel::TYPE_EMPTY)) {
			//El v�xel que est� just per damunt del v�xel actual �s buit. Per tant, �s un 
			//TOP_VOXEL. Cal comprobar si �s accessible, �s a dir, si t� prou espai buit damunt seu
			if (isTopVoxelAccessible(it->x, it->y, it->z)) {
				//Hi ha prou espai buit per damunt del v�xel. Per tant, �s accessible
				VoxelGrid::getSingleton().setVoxelType(it->x, it->y, it->z, Voxel::TYPE_ACCESSIBLE);
			}
			else {
				//NO hi ha prou espai buit damunt seu. Per tant, haig de marcar tot el SLAB com 
				//a obstacle
				obstacleFound = true;
				offsetY = 0;
				while (obstacleFound) {
					v = VoxelGrid::getSingleton().getVoxel(it->x, it->y - offsetY, it->z);
					obstacleFound = ((v->getType() == Voxel::TYPE_POSITIVE) || (v->getType() == Voxel::TYPE_NEGATIVE));
					if (obstacleFound) {
						VoxelGrid::getSingleton().setVoxelType(v->keyX, v->keyY, v->keyZ, Voxel::TYPE_NEGATIVE);
					}
					offsetY ++;
				}
			}
		}
	}
}

/********************************************************************************************************/
/* END LAYER SUBDIVISION */
/********************************************************************************************************/
/********************************************************************************************************/
/* WALKABLE AREA DETECTION */
/********************************************************************************************************/
void Voxelizer::detectConnectedWalkableLayers() {
	//walkableVoxel �s un v�xel que l'usuari ha indicat que sap segur que est� dins la zona
	//caminable. Aleshores, la zona caminable est� formada per totes aquelles layers que 
	//estan connectades amb la layer que cont� aquest v�xel. 
	
	Ogre::LogManager::getSingletonPtr()->logMessage("DETECTING CONNECTED WALKABLE AREA");
	//printSceneLayers();
	//Recorro totes les layers i em guardo el seu id
	int initialLayer = voxelWalkable->getLayer();	//La layer que utilitzem com a punt de partida
	std::set<int> walkableLayers; 
	detectRealWalkableLayers(initialLayer, walkableLayers);	//Recorro recursivament totes les layers connectades a la inicial
	//Elimino totes les layers que NO estan en la llista walkableLayers
	std::set<int>::iterator itFound;
	LayerTable::iterator itBefore;
	LayerTable::iterator it = sceneLayers.begin();
	while (it != sceneLayers.end()) {
		itBefore = it++;								//Guardo el iterador abans d'incrementar
		itFound = walkableLayers.find(itBefore->first);	//Si la layer �s caminable, ha d'estar a la llista
		if (itFound == walkableLayers.end()) {
			//La layer no est� a la llista walkableLayers, per tant, no �s caminable i s'ha d'eliminar. 
			delete sceneLayers[itBefore->first];
			sceneLayers.erase(itBefore->first);
		}
		else {
			findSeedVoxelLayer(itBefore->first);
		}
	}
	activeLayerIt = sceneLayers.begin();	//Inicialitzo el iterador activeLayerIt a la primera layer de la llista
	drawSceneLayers();
	//printSceneLayers();
	Ogre::LogManager::getSingletonPtr()->logMessage("CONNECTED WALKABLE AREA DETECTED");
}

void Voxelizer::detectRealWalkableLayers(int layerID, std::set<int>& walkableLayers) {
	walkableLayers.insert(layerID);								//La layerID �s una layer realment caminable
	std::set<int>& neighbors = sceneLayers[layerID]->neighbors;	//Els ve�ns de la layer caminable
	std::set<int>::iterator itFound;
	for (std::set<int>::iterator it = neighbors.begin(); it != neighbors.end(); it++) {
		//Comprobo si la layer ve�na ja ha estat visitada
		itFound = walkableLayers.find(*it);
		if (itFound == walkableLayers.end()) {
			//La layer ve�na encara no ha sigut visitada. Crida recursiva
			detectRealWalkableLayers(*it, walkableLayers);
		}
	}
}
/********************************************************************************************************/
/* END WALKABLE AREA DETECTION */
/********************************************************************************************************/
/********************************************************************************************************/
/* CONTOUR EXPANSION */
/********************************************************************************************************/
void Voxelizer::findSeedVoxelLayer(int layerID) {
	//Determina quin �s el v�xel llavor de la layerID. 
	//Recorro tots els v�xels accessibles de la layerID i em quedo amb aquell 
	//que est� m�s rodejat per v�xels accessibles
	Ogre::LogManager::getSingletonPtr()->logMessage("FINDING SEED VOXEL LAYER = " + std::to_string(layerID));
	std::set<Voxel*>& layerAccessibleVoxels = sceneLayers[layerID]->voxels;
	int bestNumAccessible = -1;
	int currentNumAccessible;
	Voxel* bestSeed;
	Voxel* v;
	int neighborX, neighborZ;
	int maxPossibleAccessible = 9;	//Nombre m�xim de ve�ns accessibles (contant el propi v�xel) que pot tenir un v�xel
	std::set<Voxel*>::iterator it = layerAccessibleVoxels.begin();
	while ((it != layerAccessibleVoxels.end()) && (bestNumAccessible < maxPossibleAccessible)) {
		currentNumAccessible = 0;
		v = *it;
		//Comprobo quants ve�ns s�n accessibles des del ve� actual
		for (int offsetX = -1; offsetX <= 1; offsetX++) {
			for (int offsetZ = -1; offsetZ <= 1; offsetZ++) {
				neighborX = v->keyX + offsetX;
				neighborZ = v->keyZ + offsetZ;
				if (
					(VoxelGrid::getSingleton().getVoxel(neighborX, v->keyY, neighborZ)->getType() != Voxel::TYPE_OUT) &&
					(VoxelGrid::getSingleton().getVoxel(neighborX, v->keyY, neighborZ)->getLayer() == layerID) &&
					(isNeighborAccessible(neighborX, v->keyY, neighborZ))
					) 
				{
					currentNumAccessible++;
				}
			}
		}
		//Si el v�xel actual t� m�s ve�ns accessibles que el millor que havia trobat fins ara, 
		//el actual passa a ser el millor
		if (currentNumAccessible > bestNumAccessible) {
			bestNumAccessible = currentNumAccessible;
			bestSeed = v;
		}
		it++;
	}
	sceneLayers[layerID]->voxelSeed = bestSeed;
	Ogre::LogManager::getSingletonPtr()->logMessage("SEED VOXEL LAYER FOUND");
	Ogre::LogManager::getSingletonPtr()->logMessage("x = " + std::to_string(bestSeed->keyX));
	Ogre::LogManager::getSingletonPtr()->logMessage("y = " + std::to_string(bestSeed->keyY));
	Ogre::LogManager::getSingletonPtr()->logMessage("z = " + std::to_string(bestSeed->keyZ));
	Ogre::LogManager::getSingletonPtr()->logMessage("bestNumAccessible = " + std::to_string(bestNumAccessible));
}

/********************************************************************************************************/
/* END CONTOUR EXPANSION */
/********************************************************************************************************/
/********************************************************************************************************/
/* DRAW ACTIVE LAYER */
/********************************************************************************************************/
void Voxelizer::drawActiveLayer(ngTriangleList& perpendicularFaces) {
	//Dibuixa la layer actual amb la seva cuttingmesh corresponent
	Ogre::LogManager::getSingletonPtr()->logMessage("DRAWING ACTIVE LAYER");
	clearVoxelizedModel();
	Layer* layer = activeLayerIt->second;
	std::set<Voxel*>& layerAccessibleVoxels = layer->voxels;
	Ogre::LogManager::getSingletonPtr()->logMessage("NUM ACCESSIBLE VOXELS = " + std::to_string(layerAccessibleVoxels.size()));
	//Dibuixo cada v�xel de la layer i el seu cuttingPlane corresponent
	Voxel* v;
	manualCuttingPlaneAccessible->begin("matCuttingPlaneAccessible", Ogre::RenderOperation::OT_TRIANGLE_LIST);
	manualCuttingPlanePortal->begin("matCuttingPlanePortal", Ogre::RenderOperation::OT_TRIANGLE_LIST);
	manualPerpendicularFacesIntersection->begin("matLevelRefinementPerpendicular", Ogre::RenderOperation::OT_TRIANGLE_LIST);
	//Calculo el cuttingMesh dels v�xels de la layer que s�n accessibles
	for (std::set<Voxel*>::iterator it = layerAccessibleVoxels.begin(); it != layerAccessibleVoxels.end(); it++) {
		v = *it;			//El v�xel de la layer
		if (v->getType() == Voxel::TYPE_ACCESSIBLE) v->draw("matVoxelContour", sgVoxelModelAccessible); //El dibuixo
		else v->draw("matVoxelNegative", sgVoxelModelNegative); 
		computeCuttingPlane(v, perpendicularFaces);
	}
	manualCuttingPlaneAccessible->end();
	manualCuttingPlanePortal->end();
	manualPerpendicularFacesIntersection->end();
	buildVoxelizedModel();
	Ogre::LogManager::getSingletonPtr()->logMessage("ACTIVE LAYER DRAWN");
}

void Voxelizer::computeCuttingPlane(Voxel* v, ngTriangleList& perpendicularFaces) {
	//FASE 1: Dibuixar el cutting plane definit pel v�xel que est� a dist�ncia numVoxelsCharDepth en l'eix y. 
	//Busco el v�xel que est� a dist�ncia numVoxelsCharDepth. 
	//Aquest v�xel representa on hem de posar el cutting plane per la columna (x, z)
	int offsetY = numVoxelsCharDepth;
	if (v->isContour()) {
		//Es un v�xel de contorn. Cal comprobar quan espai buit hi ha realment per sobre del personatge
		int i = 1;
		bool positiveFound = false;
		Voxel* vAux;
		while (!positiveFound && (i <= (int)numVoxelsCharDepth)) {
			vAux = VoxelGrid::getSingleton().getVoxel(v->keyX, v->keyY + i, v->keyZ);
			positiveFound = ((vAux->getType() == Voxel::TYPE_ACCESSIBLE) || (vAux->getType() == Voxel::TYPE_POSITIVE) || (vAux->getType() == Voxel::TYPE_CONTOUR));
			if (positiveFound) {
				offsetY = i - 1;
			}
			i++;
		}
	}
	//Ogre::LogManager::getSingletonPtr()->logMessage("offsetY = " + std::to_string(offsetY));
	//Ogre::LogManager::getSingletonPtr()->logMessage("numVoxelsCharDepth = " + std::to_string(numVoxelsCharDepth));
	if (v->isPortal()) {
		//�s un v�xel accessible de tipus portal
		drawCuttingPlane(v->keyX, (int)std::min((float)(v->keyY + offsetY), (float)(VoxelGrid::getSingleton().getSizeY() - 1)), v->keyZ, manualCuttingPlanePortal);
		//drawCuttingPlane(it->x, (int)std::min((float)(it->y), (float)(sliceMapDepth - 1)), it->z, manualCuttingPlanePortal);
	}
	else {
		//�s un v�xel accessible no portal
		drawCuttingPlane(v->keyX, (int)std::min((float)(v->keyY + offsetY), (float)(VoxelGrid::getSingleton().getSizeY() - 1)), v->keyZ, manualCuttingPlaneAccessible);
		//drawCuttingPlane(it->x, (int)std::min((float)(it->y), (float)(sliceMapDepth - 1)), it->z, manualCuttingPlaneAccessible);
	}
	//FASE 2: Comprovo si aquest v�xel est� en contacte amb una cara problem�tica
	voxelPerpendicularFacesIntersection(v->keyX, v->keyY + numVoxelsCharDepth, v->keyZ, perpendicularFaces);
}

void Voxelizer::voxelPerpendicularFacesIntersection(int x, int y, int z, ngTriangleList& perpendicularFaces) {
	Voxel* vCPlane = VoxelGrid::getSingleton().getVoxel(x, y, z);
	int offsetY = 0;
	bool emptyFound = false;
	for (ngTriangleList::iterator itTriangle = perpendicularFaces.begin(); itTriangle != perpendicularFaces.end(); itTriangle++) {
		if (checkTriangleBoxIntersection(itTriangle->at(0), itTriangle->at(1), itTriangle->at(2), vCPlane)) {
			//El v�xel vCPlane intersecciona amb aquesta cara problem�tica. 
			//Cal dibuixar-la en mode wireFrame
			drawPerpendicularFace(*itTriangle, manualPerpendicularFacesIntersection);
			//Ogre::LogManager::getSingletonPtr()->logMessage("PERPENDICULAR FACE INTERSECTION!");
		}
	}
}

void Voxelizer::drawPerpendicularFace(ngTriangle& t, Ogre::ManualObject* manual) {
	//Dibuixa una cara perpendicular en mode wire-frame
	manual->position(t[0]);  
	manual->position(t[1]);  
	manual->position(t[2]);  
}

bool Voxelizer::checkTriangleBoxIntersection(Ogre::Vector3& v1, Ogre::Vector3& v2, Ogre::Vector3& v3, Voxel* v) {
	float boxCenter[3] = {v->getCenter().x, v->getCenter().y, v->getCenter().z};
	float boxHalfSize[3] = {v->getHalfSize().x, v->getHalfSize().y, v->getHalfSize().z};
	float triverts[3][3];
	triverts[0][0] = v1.x; triverts[0][1] = v1.y; triverts[0][2] = v1.z; 
	triverts[1][0] = v2.x; triverts[1][1] = v2.y; triverts[1][2] = v2.z; 
	triverts[2][0] = v3.x; triverts[2][1] = v3.y; triverts[2][2] = v3.z; 
	return (Akenine::triBoxOverlap(boxCenter, boxHalfSize, triverts) == 1);
}

void Voxelizer::drawCuttingPlane(int x, int y, int z, Ogre::ManualObject* manual) {
	Ogre::Vector3& voxelDim = VoxelGrid::getSingleton().getVoxelDim();
	Ogre::Vector3& bbHalfSize = VoxelGrid::getSingleton().getHalfSize();
	float posX = (float)x * voxelDim.x - bbHalfSize.x;
	float posY = (float)y * voxelDim.y - bbHalfSize.y + voxelDim.y;
	float posZ = (float)z * voxelDim.z - bbHalfSize.z;
	manual->position(posX, posY, posZ);
	manual->textureCoord(0, 1);
	manual->position(posX + voxelDim.x, posY, posZ);
	manual->textureCoord(1, 1);
	manual->position(posX + voxelDim.x, posY, posZ + voxelDim.z);
	manual->textureCoord(1, 0);

	manual->position(posX + voxelDim.x, posY, posZ + voxelDim.z);
	manual->textureCoord(1, 0);
	manual->position(posX, posY, posZ + voxelDim.z);
	manual->textureCoord(0, 0);
	manual->position(posX, posY, posZ);
	manual->textureCoord(0, 1);
}

bool Voxelizer::existActiveLayer() {
	return (activeLayerIt != sceneLayers.end());
}

void Voxelizer::nextLayer() {
	activeLayerIt ++;
}


/*************************************************************************/
/* SETUP CUTTING MESH */
/*************************************************************************/
void Voxelizer::setupCuttingMesh() {
	Ogre::LogManager::getSingletonPtr()->logMessage("SETUP CUTTING MESH");
	//Prepara l'escena per renderitzar la cutting mesh
	bbManual->setVisible(false);
	sgVoxelModelAccessible->setVisible(false);
	if (manualCuttingPlaneAccessible->getNumSections() > 0) {
		manualCuttingPlaneAccessible->setMaterialName(0, "matLevelSelectorAccessible");
	}
	if (manualCuttingPlanePortal->getNumSections() > 0) {
		manualCuttingPlanePortal->setMaterialName(0, "matLevelSelectorPortal");
	}
	manualPerpendicularFacesIntersection->setVisible(false);
	Ogre::LogManager::getSingletonPtr()->logMessage("END SETUP CUTTING MESH");
}
/*************************************************************************/
/* END SETUP CUTTING MESH */
/*************************************************************************/
/*************************************************************************/
/* ACTIVE LAYER CELLS CLASSIFICATION */
/*************************************************************************/
void Voxelizer::navMeshVoxelDescription() {
/*	Layer* l = getActiveLayer();
	NavMesh* nm = l->navMesh;
	CellTable& cTable = nm->getCellTable();
	for (CellTable::iterator it = cTable.begin(); it != cTable.end(); it++) {
		cellVoxelDescription(nm, l->voxels, it->second);
	}
	cellDescriptorIt = nm->getCellTable().begin();
	oldCellDescriptorIt = nm->getCellTable().end();*/
}

/*************************************************************************/
/* END ACTIVE LAYER CELLS CLASSIFICATION */
/*************************************************************************/
/*************************************************************************/
/* NAV MESH SCENE GENERATION */
/*************************************************************************/
void Voxelizer::generateNavMeshScene() {
	//Inicialitzo la navMesh final de l'escena amb la mateixa informaci� que la primera layer. 
	//La idea �s que despr�s anir� unint les ve�nes, i despr�s les ve�nes de les ve�nes etc
	Ogre::LogManager::getSingletonPtr()->logMessage("GENERATING NAVMESH SCENE");
	//for (LayerTable::iterator itLayer = sceneLayers.begin(); itLayer != sceneLayers.end(); itLayer++) {
		//sceneNavMesh->merge(itLayer->second->navMesh);
	//}
	if (sceneLayers.size() == 1) {
		Ogre::LogManager::getSingletonPtr()->logMessage("SINGLE LAYERED ENVIRONMENT");
		sceneNavMesh->merge(sceneLayers.begin()->second->navMesh);
		nmSource = NULL;
		nmDest = NULL;
		Ogre::LogManager::getSingletonPtr()->logMessage("MERGE OK");
	}
	showNavMeshLayers(true);
	//Recorro totes les layers i em guardo el seu id, per saber que ja l'hem fusionat
	int currentLayerID, neighborLayerID;
	std::pair<int, int> layerPair;
	if (nextPairToFusion(layerPair)) {
		currentLayerID = layerPair.first;
		neighborLayerID = layerPair.second;
		fusionNavMeshes(currentLayerID, neighborLayerID); 
		//Indico que aquesta la parella de navMeshes (currentLayerID, neighborLayerID) ja han estat fusionades
		//L'identificador de la parella el formo fent que el ID menor sigui el primer, i el ID major
		//sigui el segon element de la parella. 
		if (currentLayerID < neighborLayerID) {
			fusionedPairs.insert(std::pair<int, int>(currentLayerID, neighborLayerID));
		}
		else {
			fusionedPairs.insert(std::pair<int, int>(neighborLayerID, currentLayerID));
		}
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("NAVMESH SCENE GENERATED");
	showNavMeshLayers(false);
	sceneNavMesh->drawEdges();
	//sceneNavMesh->drawGraph(); //Aqu� el graf es dibuixa be
	//sceneNavMesh->printData();
}

bool Voxelizer::allLayersFusioned() {
	std::pair<int, int> layerPair;
	bool pairFound = nextPairToFusion(layerPair);
	return !pairFound;
}

bool Voxelizer::nextPairToFusion(std::pair<int, int>& layerPair) {
	//Indica quina �s la pr�xima parella de layers a fusionar
	layerPair.first = -1;
	layerPair.second = -1;
	bool pairFound = false;
	std::set<int>::iterator itNeighbor;
	LayerTable::iterator layerIt = sceneLayers.begin();
	Layer* currentLayer;
	int currentLayerID, neighborLayerID;
	while (!pairFound && (layerIt != sceneLayers.end())) {
		currentLayer = layerIt->second;	//La layer actual
		currentLayerID = layerIt->first;
		//Comprobo si hi ha algun ve� que encara no s'ha fusionat amb la currentLayer
		itNeighbor = currentLayer->neighbors.begin();
		while ((itNeighbor != currentLayer->neighbors.end()) && isFusionedPair(currentLayerID, *itNeighbor)) {
			//Busco el primer ve� que encara no hagi estat fusionat amb la currentLayerID
			itNeighbor++;
		}
		if (itNeighbor != currentLayer->neighbors.end()) {
			//Si arribo aqu� �s perqu� la parella currentLayerID, *itNeighbor no s'havia fusionat encara
			neighborLayerID = *itNeighbor;
			layerPair.first = currentLayerID;
			layerPair.second = neighborLayerID;
			pairFound = true;
		}
		else {
			//La layer currentLayerID ja s'ha fusionat amb totes les layers ve�nes
			layerIt++;
		}
	}
	return pairFound;
}

bool Voxelizer::isFusionedPair(int layerID1, int layerID2) {
	//Indica si les navMeshes de la parella (layerID1, layerID2) ja han estat fusionades
	int minLayerID, maxLayerID;
	if (layerID1 < layerID2) {
		minLayerID = layerID1;
		maxLayerID = layerID2;
	}
	else {
		minLayerID = layerID2;
		maxLayerID = layerID1;
	}
	std::set<std::pair<int, int>>::iterator itFound = fusionedPairs.find(std::pair<int, int>(minLayerID, maxLayerID));
	return (itFound != fusionedPairs.end());
}

void Voxelizer::fusionNavMeshes(int layerID1, int layerID2) {
	//Fusiona les dues navMeshes. El resultat s'uneix amb el que ja cont� sceneNavMesh. 
	Ogre::LogManager::getSingletonPtr()->logMessage("FUSIONING NAV MESHES");
	Ogre::LogManager::getSingletonPtr()->logMessage("NavMesh1 = " + std::to_string(layerID1));
	Ogre::LogManager::getSingletonPtr()->logMessage("NavMesh2 = " + std::to_string(layerID2));
	nmDest = sceneLayers[layerID1]->navMesh;
	nmSource = sceneLayers[layerID2]->navMesh;
	if ((nmDest == NULL) || (nmSource == NULL)) return;
	//Comprobo si layerID1 ja ha estat fusionada amb sceneNavMesh
	std::set<int>::iterator itFound = fusionedScene.find(layerID1);
	if (itFound == fusionedScene.end()) {
		//La layerID1 encara no s'havia fusionat amb la sceneNavMesh
		sceneNavMesh->merge(nmDest);
		fusionedScene.insert(layerID1);
	}
	//Comprobo si layerID2 ja ha estat fusionada amb sceneNavMesh
	itFound = fusionedScene.find(layerID2);
	if (itFound == fusionedScene.end()) {
		//La layerID2 encara no s'havia fusionat amb la sceneNavMesh
		sceneNavMesh->merge(nmSource);
		fusionedScene.insert(layerID2);
	}
	sharedVerticesDest = nmDest->getSharedVertices();
	sharedEdgesDest = nmDest->getSharedEdges();
	sharedVerticesSource = nmSource->getSharedVertices();
	sharedEdgesSource = nmSource->getSharedEdges();
	float maxD = 0.3f;
	sceneNavMesh->fusionSharedVertices(sharedVerticesDest, sharedEdgesDest, sharedVerticesSource, sharedEdgesSource, maxD);
	sceneNavMesh->fusionSharedVertices(sharedVerticesSource, sharedEdgesSource, sharedVerticesDest, sharedEdgesDest, maxD);
}

/*************************************************************************/
/* END NAV MESH SCENE GENERATION */
/*************************************************************************/
/*************************************************************************/
/* TJOINTS DELETION */
/*************************************************************************/
void Voxelizer::deleteTJoints() {
	if ((nmDest == NULL) || (nmSource == NULL)) return;
	Ogre::LogManager::getSingletonPtr()->logMessage("DELETING T-JOINTS");
	sceneNavMesh->deleteTJoints(sharedEdgesDest, sharedEdgesSource);
	nmDest->setSharedVertices(sharedVerticesDest);
	nmDest->setSharedEdges(sharedEdgesDest);
	nmSource->setSharedVertices(sharedVerticesSource);
	nmSource->setSharedEdges(sharedEdgesSource);
	sceneNavMesh->drawEdges();
	Ogre::LogManager::getSingletonPtr()->logMessage("T-JOINTS DELETED");
}

/*************************************************************************/
/* END TJOINTS DELETION */
/*************************************************************************/
bool Voxelizer::isNeighborAccessible(int neighborX, int currentY, int neighborZ) {
	if (VoxelGrid::getSingleton().getVoxel(neighborX, currentY, neighborZ)->getType() == Voxel::TYPE_OUT) return true;	//V�xel fora de l�mits

	//Condici� 1: Cap dels ve�ns ha de ser obstacle
	if (isNeighborObstacle(neighborX, currentY, neighborZ)) return false;
		
	//Si arribo aqu� �s perqu� cap ve� �s obstacle. 

	//Condici� 2: Almenys un dels ve�ns ha de ser accessible
	return (!isNeighborEmpty(neighborX, currentY, neighborZ));
}

/*bool Voxelizer::isNeighborObstacle(int neighborX, int currentY, int neighborZ) {
	//Indica si alg�n v�xel ve� accessible des del v�xel actual �s obstacle
	int numVoxelNegative = 0;
	int neighborY;
	int neighborType, neighborLayer;
	Voxel* vNeighbor;
	for (int offsetY = -1; offsetY <= 1; offsetY++) {
		neighborY = std::max(0, std::min(currentY + offsetY, (int)sliceMapDepth - 1));
		vNeighbor = VoxelGrid::getSingleton().getVoxel(neighborX, neighborY, neighborZ);
		neighborType = vNeighbor->getType();
		neighborLayer = vNeighbor->getLayer();
		if (
			((neighborType == Voxel::TYPE_NEGATIVE) || (neighborType == Voxel::TYPE_POSITIVE)) &&
			(neighborLayer == -1)
			) 
		{
			//Hem trobat un v�xel negatiu, per tant, el ve� no �s accessible
			numVoxelNegative ++;
			break;
		}
	}
	return (numVoxelNegative != 0);
}*/

bool Voxelizer::isNeighborObstacle(int neighborX, int currentY, int neighborZ) {
	//Indica si alg�n v�xel ve� accessible des del v�xel actual �s obstacle
	int neighborType, neighborLayer;
	Voxel* vNeighbor;
	vNeighbor = VoxelGrid::getSingleton().getVoxel(neighborX, currentY, neighborZ);
	neighborType = vNeighbor->getType();
	neighborLayer = vNeighbor->getLayer();
	return (
			((neighborType == Voxel::TYPE_NEGATIVE) || (neighborType == Voxel::TYPE_POSITIVE)) &&
			(neighborLayer == -1));
}

bool Voxelizer::isNeighborEmpty(int neighborX, int currentY, int neighborZ) {
	int numVoxelPositive = 0;
	int neighborY;
	int neighborType;
	for (int offsetY = -1; offsetY <= 1; offsetY++) {
		neighborY = std::max(0, std::min(currentY + offsetY, (int)VoxelGrid::getSingleton().getSizeY() - 1));
		neighborType = VoxelGrid::getSingleton().getVoxel(neighborX, neighborY, neighborZ)->getType();
		if (neighborType == Voxel::TYPE_ACCESSIBLE) {
			//Hem trobat un v�xel accessible, per tant, el ve� �s accessible
			numVoxelPositive ++;
			break;
		}
	}
	return (numVoxelPositive == 0);
}

void Voxelizer::setSliceMapPositive(std::vector<Ogre::TexturePtr>& topTexList, std::vector<Ogre::TexturePtr>& frontTexList, std::vector<Ogre::TexturePtr>& sideTexList) {
	//sliceMapTopPositive = new SliceMap(topTexList, sliceMapWidth, sliceMapDepth, sliceMapHeight, SliceMap::ORIENTATION_TOP);
	//sliceMapFrontPositive = new SliceMap(frontTexList, sliceMapWidth, sliceMapHeight, sliceMapDepth, SliceMap::ORIENTATION_FRONT);
	//sliceMapSidePositive = new SliceMap(sideTexList, sliceMapHeight, sliceMapWidth, sliceMapDepth, SliceMap::ORIENTATION_SIDE);
}

void Voxelizer::setSliceMapNegative(std::vector<Ogre::TexturePtr>& topTexList, std::vector<Ogre::TexturePtr>& frontTexList, std::vector<Ogre::TexturePtr>& sideTexList) {
	//sliceMapTopNegative = new SliceMap(topTexList, sliceMapWidth, sliceMapDepth, sliceMapHeight, SliceMap::ORIENTATION_TOP);
	//sliceMapFrontNegative = new SliceMap(frontTexList, sliceMapWidth, sliceMapHeight, sliceMapDepth, SliceMap::ORIENTATION_FRONT);
	//sliceMapSideNegative = new SliceMap(sideTexList, sliceMapHeight, sliceMapWidth, sliceMapDepth, SliceMap::ORIENTATION_SIDE);
}

void Voxelizer::fusion() {
	fusion(sliceMapTopPositive, sliceMapFrontPositive, sliceMapSidePositive);
	//En principi, puc descartar el slicemap negatiu
	//fusion(sliceMapTopNegative, sliceMapFrontNegative, sliceMapSideNegative);	
	initGridData(sliceMapTopPositive, sliceMapTopNegative);
	drawVoxelizedModel();
}

void Voxelizer::expandPositiveVoxel(int x, int y, int z) {
	//Expandeix un v�xel positiu en el pl� XZ
	for (int offsetX = -1; offsetX <= 1; offsetX++) {
		for (int offsetZ = -1; offsetZ <= 1; offsetZ++) {
			VoxelGrid::getSingleton().setVoxelType(x + offsetX, y, z + offsetZ, Voxel::TYPE_POSITIVE);
		}
	}
}

void Voxelizer::fusion(SliceMap* sMapTop, SliceMap* sMapFront, SliceMap* sMapSide) {
	//Fusiona les 3 textures del slice map (TOP, FRONT, SIDE) en una de sola (TOP)
	Ogre::LogManager::getSingletonPtr()->logMessage("FUSIONING IMAGES");
	/*bool solidVoxelTop, solidVoxelFront, solidVoxelSide;
	for (size_t x = 0; x < sliceMapWidth; x++) {
		for (size_t y = 0; y < sliceMapDepth; y++) {
			for (size_t z = 0; z < sliceMapHeight; z++) {
				solidVoxelTop	= sMapTop->isSolidVoxel(x, y, z);
				if (!solidVoxelTop) {
					solidVoxelFront = sMapFront->isSolidVoxel(x, y, z);
					solidVoxelSide	= sMapSide->isSolidVoxel(x, y, z);
					if (solidVoxelFront || solidVoxelSide) {
						sMapTop->setSolidVoxel(x, y, z);
					}
				}
			}
		}
	}*/
	Ogre::LogManager::getSingletonPtr()->logMessage("FUSION END");
}

/********************************************************************************/
/* FUNCIONS DE VISUALITZACI� DELS MODEL VOXELITZAT*/
/********************************************************************************/
void Voxelizer::buildVoxelizedModel() {
	sgVoxelModelPositive->build();
	sgVoxelModelPositive->setVisible(true);
	
	sgVoxelModelNegative->build();
	sgVoxelModelNegative->setVisible(true);
	
	sgVoxelModelContour->build();
	sgVoxelModelContour->setVisible(true);
	
	sgVoxelModelAccessible->build();
	sgVoxelModelAccessible->setVisible(true);
}

void Voxelizer::clearVoxelizedModel() {
	if (!mSceneMgr->hasStaticGeometry("sgVoxelModelPositive")) {
		sgVoxelModelPositive = mSceneMgr->createStaticGeometry("sgVoxelModelPositive");
	}
	if (!mSceneMgr->hasStaticGeometry("sgVoxelModelNegative")) {
		sgVoxelModelNegative = mSceneMgr->createStaticGeometry("sgVoxelModelNegative");
	}
	if (!mSceneMgr->hasStaticGeometry("sgVoxelModelContour")) {
		sgVoxelModelContour = mSceneMgr->createStaticGeometry("sgVoxelModelContour");
	}
	if (!mSceneMgr->hasStaticGeometry("sgVoxelModelAccessible")) {
		sgVoxelModelAccessible = mSceneMgr->createStaticGeometry("sgVoxelModelAccessible");
	}
	if (!mSceneMgr->hasManualObject("bbManual")) {
		bbManual = mSceneMgr->createManualObject("bbManual");
		mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(bbManual);
	}
	if (!mSceneMgr->hasManualObject("manualCuttingPlaneAccessible")) {
		manualCuttingPlaneAccessible = mSceneMgr->createManualObject("manualCuttingPlaneAccessible");
		mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(manualCuttingPlaneAccessible);
	}
	if (!mSceneMgr->hasManualObject("manualCuttingPlanePortal")) {
		manualCuttingPlanePortal = mSceneMgr->createManualObject("manualCuttingPlanePortal");
		mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(manualCuttingPlanePortal);
	}
	if (!mSceneMgr->hasManualObject("manualPerpendicularFacesIntersection")) {
		manualPerpendicularFacesIntersection = mSceneMgr->createManualObject("manualPerpendicularFacesIntersection");
		mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(manualPerpendicularFacesIntersection);
	}
	sgVoxelModelPositive->reset();
	sgVoxelModelNegative->reset();
	sgVoxelModelContour->reset();
	sgVoxelModelAccessible->reset();
	bbManual->clear();
	manualCuttingPlaneAccessible->clear();
	manualCuttingPlanePortal->clear();
	manualPerpendicularFacesIntersection->clear();
	showVoxelizedModel(true);
	showCuttingShape(true);
}

void Voxelizer::drawSceneLayers() {
	Ogre::LogManager::getSingletonPtr()->logMessage("DRAWING SCENE LAYERS");
	clearVoxelizedModel();
	Layer* layer;
	int layerID = 0;
	for (LayerTable::iterator itLayer = sceneLayers.begin(); itLayer != sceneLayers.end(); itLayer++) {
		layer = itLayer->second;
		drawLayer(layer, layerID);
		layerID ++;
	}
	buildVoxelizedModel();
	Ogre::LogManager::getSingletonPtr()->logMessage("SCENE LAYERS DRAWN");
}

void Voxelizer::drawLayer(Layer* layer, int layerID) {
	Voxel* v;
	int numColors = 4;
	std::set<Voxel*>& aVoxels = layer->voxels;
	Ogre::Vector3& voxelDim = VoxelGrid::getSingleton().getVoxelDim();
	for (std::set<Voxel*>::iterator itVoxel = aVoxels.begin(); itVoxel != aVoxels.end(); itVoxel++) {
		v = *itVoxel;
		if (v->getType() == Voxel::TYPE_CONTOUR) v->draw("matVoxelNegative", sgVoxelModelNegative);
		else {
			if ((layerID % numColors) == 0) v->draw("matVoxelPositive", sgVoxelModelPositive);
			else if ((layerID % numColors) == 1) v->draw("matVoxelNegative", sgVoxelModelContour);
			else if ((layerID % numColors) == 2) v->draw("matVoxelAccessible", sgVoxelModelAccessible);
			else if ((layerID % numColors) == 3) v->draw("matVoxelLevel0", sgVoxelModelAccessible);
		}
	}
}

void Voxelizer::showVoxelizedModel(bool b) {
	sgVoxelModelPositive->setVisible(b);
	sgVoxelModelNegative->setVisible(b);
	sgVoxelModelContour->setVisible(b);
	sgVoxelModelAccessible->setVisible(b);
	bbManual->setVisible(b);
}

void Voxelizer::showCuttingShape(bool b) {
	manualCuttingPlaneAccessible->setVisible(b);
	manualCuttingPlanePortal->setVisible(b);
}

void Voxelizer::showNavMeshLayers(bool b) {
	for (LayerTable::iterator it = sceneLayers.begin(); it != sceneLayers.end(); it++) {
		showNavMesh(it->first, b);
	}
}

void Voxelizer::showNavMesh(int idLayer, bool b) {
	sceneLayers[idLayer]->showNavMesh(b);
}

void Voxelizer::drawVoxelizedModel(int voxelType) {
	Ogre::LogManager::getSingletonPtr()->logMessage("DRAWING VOXELIZED MODEL");
	clearVoxelizedModel();
	bbManual->setVisible(false);
	Voxel* v;
	VoxelPointerTable& positiveVoxels = VoxelGrid::getSingleton().getPositiveVoxels();
	VoxelPointerTable& negativeVoxels = VoxelGrid::getSingleton().getNegativeVoxels();
	VoxelPointerTable& accessibleVoxels = VoxelGrid::getSingleton().getAccessibleVoxels();
	Ogre::Vector3& voxelDim = VoxelGrid::getSingleton().getVoxelDim();
	//Dibuixo els v�xels POSITIUS
	for (VoxelPointerTable::iterator it = positiveVoxels.begin(); it != positiveVoxels.end(); it++) {
		v = VoxelGrid::getSingleton().getVoxel(it->x, it->y, it->z);
		v->draw("matVoxelPositive", sgVoxelModelPositive);
	}
	//Dibuixo els v�xels NEGATIUS
	for (VoxelPointerTable::iterator it = negativeVoxels.begin(); it != negativeVoxels.end(); it++) {
		v = VoxelGrid::getSingleton().getVoxel(it->x, it->y, it->z);
		v->draw("matVoxelNegative", sgVoxelModelNegative);
	}
	//Dibuixo els v�xels ACCESSIBLES
	for (VoxelPointerTable::iterator it = accessibleVoxels.begin(); it != accessibleVoxels.end(); it++) {
		v = VoxelGrid::getSingleton().getVoxel(it->x, it->y, it->z);
		v->draw("matVoxelAccessible", sgVoxelModelAccessible);
	}
	buildVoxelizedModel();
	Ogre::LogManager::getSingletonPtr()->logMessage("VOXELIZED MODEL DRAWN");
}

void Voxelizer::drawVoxelizedModelTop() {
	Ogre::LogManager::getSingletonPtr()->logMessage("DRAWING VOXELIZED MODEL TOP");
	clearVoxelizedModel();
	drawSliceMap(Voxel::TYPE_POSITIVE, sliceMapTopPositive);
	drawSliceMap(Voxel::TYPE_NEGATIVE, sliceMapTopNegative);
	sgVoxelModelPositive->build();
	sgVoxelModelNegative->build();
	Ogre::LogManager::getSingletonPtr()->logMessage("VOXELIZED MODEL DRAWN TOP");
}

void Voxelizer::drawVoxelizedModelFront() {
	Ogre::LogManager::getSingletonPtr()->logMessage("DRAWING VOXELIZED MODEL FRONT");
	clearVoxelizedModel();
	drawSliceMap(Voxel::TYPE_POSITIVE, sliceMapFrontPositive);
	drawSliceMap(Voxel::TYPE_NEGATIVE, sliceMapFrontNegative);
	sgVoxelModelPositive->build();
	sgVoxelModelNegative->build();
	Ogre::LogManager::getSingletonPtr()->logMessage("VOXELIZED MODEL DRAWN FRONT");
}

void Voxelizer::drawVoxelizedModelSide() {
	Ogre::LogManager::getSingletonPtr()->logMessage("DRAWING VOXELIZED MODEL SIDE");
	clearVoxelizedModel();
	drawSliceMap(Voxel::TYPE_POSITIVE, sliceMapSidePositive);
	drawSliceMap(Voxel::TYPE_NEGATIVE, sliceMapSideNegative);
	sgVoxelModelPositive->build();
	sgVoxelModelNegative->build();
	Ogre::LogManager::getSingletonPtr()->logMessage("VOXELIZED MODEL DRAWN SIDE");
}

void Voxelizer::drawSliceMap(int voxelType, SliceMap* sMap) {
	/*for (size_t x = 0; x < sliceMapWidth; x++) {
		for (size_t y = 0; y < sliceMapDepth; y++) {
			for (size_t z = 0; z < sliceMapHeight; z++) {
				if (sMap->isSolidVoxel(x, y, z)) {
					//Ogre::LogManager::getSingletonPtr()->logMessage("SOLID VOXEL FOUND!");
					//Ogre::LogManager::getSingletonPtr()->logMessage("COLUMN");
					//Ogre::LogManager::getSingletonPtr()->logMessage("x = " + std::to_string(x));
					//Ogre::LogManager::getSingletonPtr()->logMessage("z = " + std::to_string(z));
					//Ogre::LogManager::getSingletonPtr()->logMessage("value = " + std::to_string(sliceMap.getColourAt(x, z, 0).getAsRGBA()));
					drawVoxel(voxelType, x, y, z);	//Creo el v�xel a la posici� corresponent
				}
			}
		}
	}*/
}

void Voxelizer::initShowCellDescription() {
	if (applicationState == STATE_FINAL_NAVMESH_GENERATED) {
		cellDescriptorIt = sceneNavMesh->getCellTable().begin();
		oldCellDescriptorIt = sceneNavMesh->getCellTable().end();
	}
	else {
		cellDescriptorIt = getActiveLayer()->navMesh->getCellTable().begin();
		oldCellDescriptorIt = getActiveLayer()->navMesh->getCellTable().end();
	}
}

void Voxelizer::showCellDescription() {
	Ogre::LogManager::getSingletonPtr()->logMessage("SHOWING CELL DESCRIPTION");
	NavMesh* nm;
	if (applicationState == STATE_FINAL_NAVMESH_GENERATED) nm = sceneNavMesh;
	else nm = getActiveLayer()->navMesh;
	if (cellDescriptorIt == nm->getCellTable().end()) return;
	if (!mSceneMgr->hasStaticGeometry("sgCellDescription")) {
		sgCellDescription = mSceneMgr->createStaticGeometry("sgCellDescription");
	}
	sgCellDescription->reset();
	nm->showNavMesh(false);
	if (oldCellDescriptorIt != nm->getCellTable().end()) {
		Cell& oldC = oldCellDescriptorIt->second;
		nm->hideCell(oldC.id);
	}
	Cell& c = cellDescriptorIt->second;
	nm->printCell(c.id);
	nm->drawCell(c.id);
	for (VoxelPointerTable::iterator it = c.voxels.begin(); it != c.voxels.end(); it++) {
		Ogre::LogManager::getSingletonPtr()->logMessage("Drawing Voxel!");
		VoxelGrid::getSingleton().getVoxel(it->x, it->y, it->z)->draw("matVoxelAccessible", sgCellDescription);
	}
	sgCellDescription->build();
	sgCellDescription->setVisible(true);
	oldCellDescriptorIt = cellDescriptorIt;
	cellDescriptorIt++;
	if (cellDescriptorIt == nm->getCellTable().end()) cellDescriptorIt = nm->getCellTable().begin();
}

