#include "ObjMeshManager.h"

/***************************************************************************************/
/* CREACIO I DESTRUCCIO */
/***************************************************************************************/

template<> ObjMeshManager *Ogre::Singleton<ObjMeshManager>::ms_Singleton = 0;

ObjMeshManager *ObjMeshManager::getSingletonPtr(void) {
     return ms_Singleton;
}
 
ObjMeshManager &ObjMeshManager::getSingleton(void) {  
	assert(ms_Singleton);  
    return(*ms_Singleton);
}
 
ObjMeshManager::ObjMeshManager(void) {
    mResourceType = "ObjMesh";
 
    // low, because it will likely reference other resources
    mLoadOrder = 30.0f;
 
	// this is how we register the ResourceManager with OGRE
    Ogre::ResourceGroupManager::getSingleton()._registerResourceManager(mResourceType, this);
}
 
ObjMeshManager::~ObjMeshManager() {
    // and this is how we unregister it
	ObjMeshList.clear();
    Ogre::ResourceGroupManager::getSingleton()._unregisterResourceManager(mResourceType);
}
 
ObjMeshPtr ObjMeshManager::load(const Ogre::String &name, const Ogre::String &group) {
	ObjMeshPtr textf = getByName(name);
 
	if (textf.isNull()) {
		textf = create(name, group);
	}
 
    textf->load();
    return textf;
}

ObjMeshPtr ObjMeshManager::load(size_t pos) {
	Ogre::LogManager::getSingletonPtr()->logMessage("LOADING BY POS = " + Ogre::StringConverter::toString(pos));
	Ogre::String name = ObjMeshList.at(pos);
	return load(name);
}
 
Ogre::Resource *ObjMeshManager::createImpl(const Ogre::String &name, Ogre::ResourceHandle handle, 
                                           const Ogre::String &group, bool isManual, Ogre::ManualResourceLoader *loader, 
                                           const Ogre::NameValuePairList *createParams) {
    return new ObjMesh(this, name, handle, group, isManual, loader);
}

void ObjMeshManager::initObjMeshList(void) {
	Ogre::LogManager::getSingletonPtr()->logMessage("INITIALIZING GAME MAP LIST");
	ResourceMapIterator it = getResourceIterator();
	ObjMeshPtr gMap;
	while (it.hasMoreElements()) {
		gMap = it.getNext();
		ObjMeshList.push_back(gMap->getName());
		Ogre::LogManager::getSingletonPtr()->logMessage("mapName = " + gMap->getName());
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("ObjMeshList INITIALIZED!");
}

/***************************************************************************************/
/* GET I SET */
/***************************************************************************************/

size_t ObjMeshManager::getNumResources(void) {
	return mResources.size();
}