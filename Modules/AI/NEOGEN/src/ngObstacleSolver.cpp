#include "ngObstacleSolver.h"
#include "ngGlobalVars.h"
#include "ngDataStructures.h"

using namespace NEOGEN;
using namespace GeometrySolver;

/*****************************************************************/
/* CREACIO I DESTRUCCIO */
/*****************************************************************/
ObstacleSolver::ObstacleSolver() {
	elapsedTime = 0;
	timer = new Ogre::Timer();
	timer->reset();
}

void ObstacleSolver::initializeData(Ogre::TexturePtr normalDepthTex, float mSlope, float mHeight, float zN, float zF, float cOffset, float tRes) {
	Ogre::LogManager::getSingletonPtr()->logMessage("INITIALIZING DATA OBSTACLESOLVER");
	obstacles.clear();
	contourMap.clear();
	createNormalDepthMap(normalDepthTex);
	maxSlope = mSlope;
	cosMaxSlope = Ogre::Math::Cos(Ogre::Degree(maxSlope));
	maxStepHeight = mHeight;
	zNear = zN;
	zFar = zF;
	Ogre::LogManager::getSingletonPtr()->logMessage("zNear = " + std::to_string(zNear));
	Ogre::LogManager::getSingletonPtr()->logMessage("zFar = " + std::to_string(zFar));
	cameraOffset = cOffset;
	pixelsPerUnit = tRes;
}

void ObstacleSolver::createNormalDepthMap(Ogre::TexturePtr normalDepthTex) {
	//Per poder treballar de forma m�s c�mode amb les textures, les converteixo a imatges i, posteriorment, 
	//les torno a convertir a textura
	Ogre::LogManager::getSingletonPtr()->logMessage("***********************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("CREATING NORMAL DEPTH MAP");
	normalDepthTex->convertToImage(normalDepthMap);
	
	normalDepthMapWidth = normalDepthMap.getWidth();
	normalDepthMapHeight = normalDepthMap.getHeight();

	//Inicialitzo la imatge obstaclePartition. �s on guardar� el resultat de la partici�. Inicialment, 
	//�s una imatge amb les mateixes dimensions que la imatge normalDepthMap i inicialitzada a color negre. 
	obstaclePartition.load("blackBase.png", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	obstaclePartition.resize(normalDepthMapWidth, normalDepthMapHeight, Ogre::Image::FILTER_NEAREST);
}

ObstacleSolver::~ObstacleSolver() {
	normalDepthMap.freeMemory();
}

void ObstacleSolver::initPixelVisited(size_t w, size_t h) {
	//Inicialitza el vector de p�xels visitats
	pixelVisited.clear();
	std::vector<bool> vCol;
	for (size_t i = 0; i < w; i++) {
		vCol.clear();
		for (size_t j = 0; j < h; j++) {
			vCol.push_back(false);
		}
		pixelVisited.push_back(vCol);
	}
}

void ObstacleSolver::initPixelTable() {
	//Inicialitza el vector de p�xels visitats
	pixelTable.clear();
	std::vector<Pixel> vCol;
	Ogre::ColourValue mapValue;
	float depth;
	int type;
	bool isPortal;
	bool isContour;
	for (size_t i = 0; i < normalDepthMapWidth; i++) {
		vCol.clear();
		for (size_t j = 0; j < normalDepthMapHeight; j++) {
			mapValue = normalDepthMap.getColourAt(i, j, 0);
			isPortal = false;
			isContour = false;
			if (Ogre::Math::RealEqual(mapValue.r, 0.0f)) {
				//�s un p�xel obstacle
				depth = -1.0f;
				type = PIXEL_TYPE_NEGATIVE;
			}
			else {
				//�s un p�xel positiu
				type = PIXEL_TYPE_POSITIVE;
				isPortal = !Ogre::Math::RealEqual(mapValue.r, 1.0f);
				isContour = !Ogre::Math::RealEqual(mapValue.g, 1.0f);
				depth = mapValue.a * (zFar + zNear) - zNear;	//Profunditat respecte la c�mera quan es va fer el render des de dalt
				depth = cameraOffset - depth;					//Profunditat "real" del fragment en coordenades de m�n
				depth += CELL_OFFSET;							//El despla�o una mica cap amunt per poder veure millor la cel�la resultant
			}
			vCol.push_back(Pixel(-1, type, isContour, isPortal, depth));
		}
		pixelTable.push_back(vCol);
	}
}

void ObstacleSolver::addPixelContour(size_t x, size_t y, int label) {
	//Afegeix un nou p�xel al mapa de contorns. 
	std::pair<size_t, size_t> pixelID(x, y);
	contourMap.push_back(pixelID);
	pixelTable[x][y].label = label;
}

void ObstacleSolver::addVertContour(float x, float y) {
	std::pair<float, float> vertID(x, y);
	contourVerts.insert(vertID);
}

void ObstacleSolver::setPixelContourLabel(size_t x, size_t y, int label) {
	if (pixelContourExist((int)x, (int)y)) {
		//El pixelID introdu�t correspon al pixelID d'un contorn
		pixelTable[x][y].label = label;
	}
}

bool ObstacleSolver::pixelContourExist(int x, int y) {
	//Indica si el p�xel x, y �s un p�xel de tipus contorn
	if ((x < 0) || (y < 0) || (x >= (int)normalDepthMapWidth) || (y >= (int)normalDepthMapHeight)) return false;
	return ((pixelTable[x][y].type == PIXEL_TYPE_ACCESSIBLE) && (pixelTable[x][y].isContour));
}

std::vector<Poly*>& ObstacleSolver::getObstacles() {
	return obstacles;
}

float ObstacleSolver::getElapsedTime() {
	return ((float)elapsedTime / 1000.0f);
}

size_t ObstacleSolver::getMapWidth() {
	return normalDepthMapWidth;
}

size_t ObstacleSolver::getMapHeight() {
	return normalDepthMapHeight;
}

/*****************************************************************/
/* GENERACI� DEL PROC�S DE PARTICI� D'OBSTACLES */
/*****************************************************************/

void ObstacleSolver::generateObstaclePartition(size_t refX, size_t refY) {
	if (DEBUG_MODE) normalDepthMap.save("00normalDepthMap.png");
	
	timeBefore = timer->getMilliseconds();
	initPixelTable();
	detectObstacles(refX, refY);
	savePartition("01BinaryPartitionRough.png");

	polygonReconstruction();
	savePartition("05FinalPartition.png");

	timeAfter = timer->getMilliseconds();
	elapsedTime = timeAfter - timeBefore;
}

/*****************************************************************/
/* DETECCI� D'OBSTACLES */
/*****************************************************************/
void ObstacleSolver::detectObstacles(int refX, int refY) {
	//Comprobo si el node actual �s semblant al node de refer�ncia
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("DETECTING OBSTACLES");
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");

	std::vector<std::pair<int, int>> q;
	q.push_back(std::pair<int, int>(refX, refY));
	pixelTable[refX][refY].type = PIXEL_TYPE_ACCESSIBLE;
	std::pair<int, int> currentPixel;
	
	int currentX, currentY;
	int neighborX, neighborY;
	
	while (!q.empty()) {
		currentPixel = q.back();
		q.pop_back();
		currentX = currentPixel.first; 
		currentY = currentPixel.second;

		//Si arribo aqu� �s perqu� els nodes s�n similars. 
		if (pixelTable[currentX][currentY].isContour) {
			//He trobat un p�xel que forma part del contorn d'un obstacle
			addPixelContour(currentX, currentY);
		}
		else {
			obstaclePartition.setColourAt(Ogre::ColourValue::White, currentX, currentY, 0);
		
			//4-Connectivity
			for (int offsetX = -1; offsetX <= 1; offsetX += 2) {
				neighborX = (int)currentX + offsetX;
				neighborY = (int)currentY;
				if (
					(neighborX >= 0) && (neighborX <= (int)normalDepthMapWidth - 1) &&
					(neighborY >= 0) && (neighborY <= (int)normalDepthMapHeight - 1) &&
					(pixelTable[neighborX][neighborY].type == PIXEL_TYPE_POSITIVE)
					) 
				{
					q.push_back(std::pair<int, int>(neighborX, neighborY));
					pixelTable[neighborX][neighborY].type = PIXEL_TYPE_ACCESSIBLE;
				}
			}
			for (int offsetY = -1; offsetY <= 1; offsetY += 2) {
				neighborX = (int)currentX;
				neighborY = (int)currentY + offsetY;
				if (
					(neighborX >= 0) && (neighborX <= (int)normalDepthMapWidth - 1) &&
					(neighborY >= 0) && (neighborY <= (int)normalDepthMapHeight - 1) &&
					(pixelTable[neighborX][neighborY].type == PIXEL_TYPE_POSITIVE)
					) 
				{
					q.push_back(std::pair<int, int>(neighborX, neighborY));
					pixelTable[neighborX][neighborY].type = PIXEL_TYPE_ACCESSIBLE;
				}
			}
		}
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("OBSTACLES DETECTED");
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");
}

/*void ObstacleSolver::detectObstacles(int refX, int refY) {
	//Faig el flooding mitjan�ant scanlines horitzontals
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("DETECTING OBSTACLES");
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");

	std::deque<std::pair<int, int>> seeds;
	seeds.push_back(std::pair<int, int>(refX, refY));
	std::pair<int, int> s;
	Pixel* currentPixel;
			
	bool seedUP, seedDOWN;
	while (!seeds.empty()) {
		//Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");
		s = seeds.back();
		seeds.pop_back();
		
		seedUP = false;
		seedDOWN = false;
				
		//M'expandeixo cap a la DRETA tant com puc
		scanLine(seeds, s, 1, seedUP, seedDOWN);
		scanLine(seeds, s, -1, seedUP, seedDOWN);
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("OBSTACLES DETECTED");
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");
}

void ObstacleSolver::scanLine(std::deque<std::pair<int, int>>& seeds, std::pair<int, int>& s, int incr, bool& seedUP, bool& seedDOWN) {
	int offsetX = 0;
	Pixel* currentPixel;
	Pixel* neighborPixelUP;
	Pixel* neighborPixelDOWN;
	do {
		//Marco el p�xel actual com a accessible
		currentPixel = &(pixelTable[s.first + offsetX][s.second]);
		currentPixel->type = PIXEL_TYPE_ACCESSIBLE;
		obstaclePartition.setColourAt(Ogre::ColourValue::White, s.first + offsetX, s.second, 0);
		
		//Comprovo si el ve� superior �s contorn
		neighborPixelUP = &(pixelTable[s.first + offsetX][s.second + 1]);
		if (neighborPixelUP->isContour) {
			neighborPixelUP->type = PIXEL_TYPE_ACCESSIBLE;
			addPixelContour(s.first + offsetX, s.second + 1);
			seedUP = false;	//Com que he trobat un contorn, necessitar� plantar una nova llavor a la fila superior
		}
		else {
			//El ve� superior NO �s contorn. Comprovo si �s una nova llavor
			if (!seedUP && (neighborPixelUP->type == PIXEL_TYPE_POSITIVE)) {
				//Es requereix una llavor a la fila superior
				seeds.push_back(std::pair<int, int>(s.first + offsetX, s.second + 1));
				seedUP = true;
			}
		}

		//Comprovo si el ve� inferior �s contorn
		neighborPixelDOWN = &(pixelTable[s.first + offsetX][s.second - 1]);
		if (neighborPixelDOWN->isContour) {
			neighborPixelDOWN->type = PIXEL_TYPE_ACCESSIBLE;
			addPixelContour(s.first + offsetX, s.second - 1);
			seedDOWN = true;	//Com que he trobat un contorn, necessitar� plantar una nova llavor a la fila superior
		}
		else {
			//El ve� inferior NO �s contorn. Comprovo si �s una nova llavor
			if (!seedDOWN && (neighborPixelDOWN->type == PIXEL_TYPE_POSITIVE)) {
				//Es requereix una llavor a la fila inferior
				seeds.push_back(std::pair<int, int>(s.first + offsetX, s.second - 1));
				seedDOWN = true;
			}
		}
		offsetX += incr;
	} while (!currentPixel->isContour);
}*/

bool ObstacleSolver::isLimitPixel(size_t pX, size_t pY) {
	//Indica si �s un p�xel del l�mit de la imatge. Per defecte, aquests p�xels 
	//es cataloguen com a contorn. 
	if ((pX == 0) || (pY == 0)) return true;
	if ((pX == (normalDepthMapWidth - 1)) || (pY == (normalDepthMapHeight - 1))) return true;
	return false;
}

bool ObstacleSolver::isOutOfBounds(int pX, int pY) {
	if ((pX < 0) || (pY < 0)) return true;
	if ((pX >= ((int)normalDepthMapWidth - 1)) || (pY >= ((int)normalDepthMapHeight - 1))) return true;
	return false;
}

/*****************************************************************/
/* RECONSTRUCCI� DE POL�GONS */
/*****************************************************************/
void ObstacleSolver::polygonReconstruction() {
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("RECONSTRUCTING POLYGONS");
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");
	//Ordeno la llista que cont� els vectors de contorn
	std::sort(contourMap.begin(), contourMap.end());

	//Inicialitzo la taula que cont� els labels per p�xel
	initPixelVisited(obstaclePartition.getWidth(), obstaclePartition.getHeight());
	ContourMap::iterator it;
	int contourLabel = 0;
	for (it = contourMap.begin(); it != contourMap.end(); it++) {
		//Ogre::LogManager::getSingletonPtr()->logMessage("CONTOUR LABEL = " + std::to_string(contourLabel));
		if (!pixelVisited[it->first][it->second]) {
			createPolygon(*it, contourLabel);
			contourLabel++;
		}
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("POLYGONS RECONSTRUCTED");
}

void ObstacleSolver::createPolygon(std::pair<size_t, size_t>& initialPixelID, int label) {
	//Crea un pol�gon a partir del p�xel inicial initialPixelID
	
	Ogre::LogManager::getSingletonPtr()->logMessage("*****************************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("CREATING POLYGON");

	//1) Recorrer els p�xels adjacents fins que tornem a trobar el p�xel amb el que hem comen�at
	std::pair<int, int> currentPixelID = initialPixelID;	//El p�xel actual
	std::pair<int, int> nextPixelID;						//El pr�xim p�xel que visitarem
	std::pair<int, int> prevPixelID;						//L'�ltim p�xel que hem visitat
	std::pair<int, int> neighborPixelID;
	Ogre::LogManager::getSingletonPtr()->logMessage("initialPixelID");
	Ogre::LogManager::getSingletonPtr()->logMessage("x = " + std::to_string(initialPixelID.first));
	Ogre::LogManager::getSingletonPtr()->logMessage("y = " + std::to_string(initialPixelID.second));
	Ogre::LogManager::getSingletonPtr()->logMessage("label = " + std::to_string(label));
	
	//std::vector<Vec3D*> lVert;
	PixelVector lPoint;

	bool nextFound;

	Vec3D vPrev;
	Vec3D vOrigin((float)initialPixelID.first, (float)initialPixelID.second);
	Vec3D vNeighbor;
	Vec3D refEdge;
	float lesserAngle;
	float neighborAngle;
	if (label != 0) {
		Ogre::LogManager::getSingletonPtr()->logMessage("CREATING OBSTACLE");
		prevPixelID.first = initialPixelID.first;
		prevPixelID.second = initialPixelID.second - 1;
	}
	else {
		Ogre::LogManager::getSingletonPtr()->logMessage("CREATING FLOOR");
		prevPixelID.first = initialPixelID.first;
		prevPixelID.second = initialPixelID.second + 1;
	}
	if (pixelContourExist(prevPixelID.first, prevPixelID.second)) {
		lPoint.push_back(prevPixelID);
		setPixelContourLabel(prevPixelID.first, prevPixelID.second, label);
		pixelVisited[prevPixelID.first][prevPixelID.second] = true;
		/*Ogre::LogManager::getSingletonPtr()->logMessage("ADDED VERTEX");
		Ogre::LogManager::getSingletonPtr()->logMessage("x = " + std::to_string(prevPixelID.first));
		Ogre::LogManager::getSingletonPtr()->logMessage("y = " + std::to_string(prevPixelID.second));
		Ogre::LogManager::getSingletonPtr()->logMessage("==============================");*/
	}

	do {
		lesserAngle = 400.0f;
		nextFound = false;
		vOrigin.setCoords((float)currentPixelID.first, (float)currentPixelID.second, 0.0f);
		vPrev.setCoords((float)prevPixelID.first, (float)prevPixelID.second, 0.0f);
		refEdge = vPrev - vOrigin;
		for (int offsetX = -1; offsetX <= 1; offsetX++) {
			for (int offsetY = -1; offsetY <= 1; offsetY++) {
				if ((offsetX == 0) && (offsetY == 0)) continue;
				neighborPixelID.first = currentPixelID.first + offsetX;
				neighborPixelID.second = currentPixelID.second + offsetY;
				if (
					(neighborPixelID != prevPixelID) &&
					(!isOutOfBounds(neighborPixelID.first, neighborPixelID.second)) &&
					(!pixelVisited[neighborPixelID.first][neighborPixelID.second]) &&
					(pixelContourExist(neighborPixelID.first, neighborPixelID.second))
				)
				{
					//Si arribo aqu� �s perqu� el ve� �s un p�xel de contorn que encara no ha estat visitat.
					//Miro si �s el ve� amb menor gir
					vNeighbor.setCoords((float)neighborPixelID.first, (float)neighborPixelID.second, 0.0f);
					neighborAngle = angleBetweenVectors(refEdge, vNeighbor - vOrigin);
					if (neighborAngle < lesserAngle) {
						//He trobat un ve� contorn amb menor angle que el ve� contorn actual
						lesserAngle = neighborAngle;
						nextPixelID = neighborPixelID;
						nextFound = true;
					}
				}
			}
		}
		
		//Comprovo si realment cal crear el v�rtex
		//if (!isPixelContourAlligned(currentPixelID.first, currentPixelID.second)) {
			//Si no �s un p�xel aliniat, no el puc eliminar
			lPoint.push_back(currentPixelID);
			setPixelContourLabel(currentPixelID.first, currentPixelID.second, label);
			//Ogre::LogManager::getSingletonPtr()->logMessage("ADDED VERTEX");
			//Ogre::LogManager::getSingletonPtr()->logMessage("x = " + std::to_string(currentPixelID.first));
			//Ogre::LogManager::getSingletonPtr()->logMessage("y = " + std::to_string(currentPixelID.second));
			//Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
		//}

		pixelVisited[currentPixelID.first][currentPixelID.second] = true;
		prevPixelID = currentPixelID;
		currentPixelID = nextPixelID;

		/*//L'ordenaci� per angle que faig servir, fa que em pugui saltar v�rtexs que estan en una cantonada. 
		//Aix� d�na un ajustament pobre en obstacles amb angles rectes. Comprovo si m'he saltat una cantonada
		std::pair<int, int> corner1;
		std::pair<int, int> corner2;
		bool condition1 = ((currentPixelID.first == 622) && (currentPixelID.second == 2));
		if ((currentPixelID.first != prevPixelID.first) && (currentPixelID.second != prevPixelID.second)) {
			//El nou p�xel actual i el p�xel anterior formen una diagonal. Calculo les cantonades
			corner1.first = currentPixelID.first;
			corner1.second = prevPixelID.second;

			corner2.first = prevPixelID.first;
			corner2.second = currentPixelID.second;

			if ((!pixelVisited[corner1.first][corner1.second]) && (pixelContourExist(corner1.first, corner1.second))) {
				lPoint.push_back(corner1);
				setPixelContourLabel(corner1.first, corner1.second, label);
				pixelVisited[corner1.first][corner1.second] = true;
				Ogre::LogManager::getSingletonPtr()->logMessage("ADDED VERTEX");
				Ogre::LogManager::getSingletonPtr()->logMessage("x = " + std::to_string(corner1.first));
				Ogre::LogManager::getSingletonPtr()->logMessage("y = " + std::to_string(corner1.second));
				Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
			}
			else if ((!pixelVisited[corner2.first][corner2.second]) && (pixelContourExist(corner2.first, corner2.second))) {
				lPoint.push_back(corner2);
				setPixelContourLabel(corner2.first, corner2.second, label);
				pixelVisited[corner2.first][corner2.second] = true;
				Ogre::LogManager::getSingletonPtr()->logMessage("ADDED VERTEX");
				Ogre::LogManager::getSingletonPtr()->logMessage("x = " + std::to_string(corner2.first));
				Ogre::LogManager::getSingletonPtr()->logMessage("y = " + std::to_string(corner2.second));
				Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
			}
		}*/
	} while (nextFound);
	simplifyPolygon(lPoint, label);
}

float ObstacleSolver::angleBetweenVectors(Vec3D& v1, Vec3D& v2) {
	//Calcula l'angle que hi ha entre 2 vectors. S'ha d'entendre l'angle
	//com al GIR CAP A LA DRETA del vector v1 cap al vector v2

	//NOTA: v1 i v2 han de tenir el mateix origen
	std::vector<float> u;
	u.push_back(v1.getX());
	u.push_back(v1.getY());

	std::vector<float> v;
	v.push_back(v2.getX());
	v.push_back(v2.getY());

	float angle = (atan2(v.at(1), v.at(0)) - atan2(u.at(1), u.at(0))) * 180.0f / Ogre::Math::PI;
	//Ogre::LogManager::getSingletonPtr()->logMessage("ANGLE BEFORE CORRECTION = " + std::to_string(angle));
	//l'angle calculat s'ha d'entendre com el gir m�s curt per anar de v1 a v2. Per tant, si �s negatiu haig de corregir l'angle	
	if (angle < 0.0f) angle += 360.0f;

	//angle actualment cont� el GIR CAP A L'ESQUERRA. Calculo el GIR CAP A LA DRETA
	angle = 360.0f - angle;
	if (Ogre::Math::RealEqual(angle, 0.0f) || Ogre::Math::RealEqual(angle, 360.0f, 0.01f)) angle = 0.0f;
	//Ogre::LogManager::getSingletonPtr()->logMessage("ANGLE AFTER CORRECTION = " + std::to_string(angle));

	return angle;
}

void ObstacleSolver::simplifyPolygon(PixelVector& lPoint, int label) {
	//Simplifica la llista lPoint
	Ogre::LogManager::getSingletonPtr()->logMessage("SIMPLIFYING POLYGON");
	
	boost::geometry::model::linestring<BoostPoint> original;
		
	PixelVector::iterator itPoint;
	float x, y, z;
	for (itPoint = lPoint.begin(); itPoint != lPoint.end(); itPoint++) {
		x = float(itPoint->first);
		y = float(itPoint->second);
		z = pixelTable[itPoint->first][itPoint->second].depth;
		original.push_back(BoostPoint(x, y, z));	
	}
	
	float minDistance = 2 * std::sqrtf(2); //0.25f * pixelsPerUnit;
	Ogre::LogManager::getSingletonPtr()->logMessage("pixelsPerUnit = " + std::to_string(pixelsPerUnit));
	Ogre::LogManager::getSingletonPtr()->logMessage("minDistance = " + std::to_string(minDistance));
	//Simplifico el pol�gon
	boost::geometry::model::linestring<BoostPoint> simplified;
	//boost::geometry::simplify(original, simplified, 0.5);
	boost::geometry::simplify(original, simplified, minDistance);
	
	Ogre::LogManager::getSingletonPtr()->logMessage("ORIGINAL");
	Ogre::LogManager::getSingletonPtr()->logMessage("size = " + std::to_string(original.size()));
		
	//Creo el pol�gon a partir de la llista simplificada
	Ogre::LogManager::getSingletonPtr()->logMessage("SIMPLIFIED");
	Ogre::LogManager::getSingletonPtr()->logMessage("size = " + std::to_string(simplified.size()));
	if (simplified.size() <= 2) {
		Ogre::LogManager::getSingletonPtr()->logMessage("WARNING! BAD POLYGON FOUND");
		return;
	}
	
	Ogre::Vector2 halfSizeMap((float)normalDepthMap.getWidth() / 2.0f, (float)normalDepthMap.getHeight() / 2.0f);
	halfSizeMap /= pixelsPerUnit;
	std::vector<Vertex> lVert;
	BoostPoint* bp;
	bool isShared;
	Pixel* p;
	for (int i = 0; i < int(simplified.size()); i++) {
		bp = &(simplified.at(i));
		p = &pixelTable[(int)bp->get<0>()][(int)bp->get<1>()];
		p->strong = true;	//Es un p�xel que ha sobreviscut a la simplificaci�
		if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XY) {
			x = ((float)bp->get<0>() / pixelsPerUnit) - halfSizeMap.x;
			y = ((float)bp->get<1>() / pixelsPerUnit) - halfSizeMap.y;
			z = p->depth;
		}
		else if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XZ) {
			x = ((float)bp->get<0>() / pixelsPerUnit) - halfSizeMap.x;
			y = p->depth;
			z = ((float)bp->get<1>() / pixelsPerUnit) - halfSizeMap.y;
		}
		else {
			x = p->depth;
			y = ((float)bp->get<0>() / pixelsPerUnit) - halfSizeMap.x;
			z = ((float)bp->get<1>() / pixelsPerUnit) - halfSizeMap.y;
		}
		Vertex v(Vec3D(x, y, z));
		isShared = p->portal;
		v.setShared(isShared);
		lVert.push_back(v);
	}

	//Comprobo si el punt final del pol�gon �s necessari. Aquesta comprobaci� cal fer-la degut a 
	//qu� l'algorisme de DP simplifica corbes. Per tant, agafa el punt inicial i el final del pol�gon
	//com a punts fixes, per� pot ser que el punt inicial i final estiguin molt propers. 
	Vec3D first = lVert.at(0).getPosition();						//El primer element
	Vec3D last = lVert.at(lVert.size() - 1).getPosition();			//L'�ltim element (el que vull comprobar si �s prescindible)
	Vec3D penultimate = lVert.at(lVert.size() - 2).getPosition();	//El pen�ltim element

	IntersectionPoint3D result;
	GeometrySolver::projectionPointLine3D(last, first, penultimate, result);
	float dist2 = last.dist2(result.location);
	if (dist2 <= (0.5f * 0.5f)) {
		//Ogre::LogManager::getSingletonPtr()->logMessage("LAST VERTEX WAS PRESCINDIBLE!");
		lVert.pop_back();
	}
	if (lVert.size() >= 3) {
		Poly* poly = new Poly(lVert);
		if (
			((label == 0) && (GeometrySolver::polygonOrientation(*poly, NEOGEN_PROJECTION_PLANE) == GeometrySolver::TURN_CWISE)) ||		//El terra s'ha donat en sentit horari
			((label != 0) && (GeometrySolver::polygonOrientation(*poly, NEOGEN_PROJECTION_PLANE) == GeometrySolver::TURN_COUNTER_CWISE))	//L'obstacle s'ha donat en sentit anti horari
			)
		{
			Ogre::LogManager::getSingletonPtr()->logMessage("POLY REVERSED!");
			poly->reverse();
		}
		else Ogre::LogManager::getSingletonPtr()->logMessage("POLY ORIENTATION OK!");
		obstacles.push_back(poly);	
	}

	Ogre::LogManager::getSingletonPtr()->logMessage("POLYGON SIMPLIFIED");
}

bool ObstacleSolver::isPixelContourAlligned(size_t x, size_t y) {
	//Indica si el p�xel (x, y) est� completament aliniat amb altres p�xels de contorn
	
	int numContour = 0;
	std::vector<bool> neighborContour;
	bool isContour;
	for (int offsetX = -1; offsetX <= 1; offsetX++) {
		for (int offsetY = -1; offsetY <= 1; offsetY++) {
			isContour = pixelContourExist((int)x + offsetX, (int)y + offsetY);
			neighborContour.push_back(isContour);
			if (isContour) numContour++;
		}
	}
	bool allignedHorz = (neighborContour[1] && neighborContour[7]);		//W i E s�n tamb� contorns
	bool allignedVert = (neighborContour[3] && neighborContour[5]);		//N i S s�n tamb� contorns
	bool allignedCross1 = (neighborContour[0] && neighborContour[8]);	//NW i SE s�n tamb� contorns
	bool allignedCross2 = (neighborContour[6] && neighborContour[2]);	//NE i SW s�n tamb� contorns

	return ((numContour == 2) && (allignedHorz || allignedVert || allignedCross1 || allignedCross2));
}

/*****************************************************************/
/* INFORMACI� */
/*****************************************************************/
void ObstacleSolver::savePartition(const Ogre::String& imgName) {
	if (!DEBUG_MODE) return;
	Ogre::Image img(obstaclePartition);
	contourPaint(img);
	img.save(imgName);
}

void ObstacleSolver::contourPaint(Ogre::Image& img) {
	//Funci� de test, per visualitzar els spots creats
	ContourMap::iterator it;
	size_t x, y;
	int label;
	//int colourID;
	for (it = contourMap.begin(); it != contourMap.end(); it++) {
		x = it->first;
		y = it->second;
		label = pixelTable[x][y].label;
		if (label == -1) {
			img.setColourAt(Ogre::ColourValue(1.0f, 0.5f, 0.5f), x, y, 0);	//Rose => Undefined label
		}
		else {
			if (pixelTable[x][y].portal && pixelTable[x][y].strong) img.setColourAt(Ogre::ColourValue(0.0f, 0.5f, 1.0f), x, y, 0);
			else {
				img.setColourAt(Ogre::ColourValue::Red, x, y, 0);
				/*colourID = (label % 9);
				if (colourID == 0) {
					if  img.setColourAt(Ogre::ColourValue(1.0f, 0.0f, 0.0f), x, y, 0);	//Red
					else img.setColourAt(Ogre::ColourValue(0.5f, 0.0f, 0.0f), x, y, 0);
				}
				if (colourID == 1) img.setColourAt(Ogre::ColourValue(0.0f, 1.0f, 0.0f), x, y, 0);	//Green
				if (colourID == 2) img.setColourAt(Ogre::ColourValue(0.0f, 0.0f, 1.0f), x, y, 0);	//Blue
				if (colourID == 3) {
					if (pixelTable[x][y].portal) img.setColourAt(Ogre::ColourValue(1.0f, 1.0f, 0.0f), x, y, 0);	//Yellow
					else img.setColourAt(Ogre::ColourValue(0.5f, 0.5f, 0.0f), x, y, 0);	//Yellow
				}
				if (colourID == 4) img.setColourAt(Ogre::ColourValue(1.0f, 0.0f, 1.0f), x, y, 0);	//Magenta
				if (colourID == 5) img.setColourAt(Ogre::ColourValue(0.0f, 1.0f, 1.0f), x, y, 0);	//Cyan
				if (colourID == 6) img.setColourAt(Ogre::ColourValue(1.0f, 0.5f, 0.0f), x, y, 0);	//Orange
				if (colourID == 7) img.setColourAt(Ogre::ColourValue(0.5f, 1.0f, 0.0f), x, y, 0);	//Light Green
				if (colourID == 8) img.setColourAt(Ogre::ColourValue(0.0f, 0.5f, 1.0f), x, y, 0);	//Light Blue*/
			}
		}
	}
}