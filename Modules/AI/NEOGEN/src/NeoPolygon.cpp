#include "NeoPolygon.h"

using namespace NEOGEN;

#pragma region CREATION AND DESTRUCTION

Polygon::Polygon() {
	_isClosed = false;
}

#pragma endregion

#pragma region GET AND SET

void Polygon::addPoint(const Vector3& p) {
	_points.push_back(p);
}

void Polygon::close() {
	_isClosed = true;
}

const Points& Polygon::getPoints() {
	return _points;
}

PolygonOrientation Polygon::getOrientation(const Vector3& up) {
	
}