#include "ngVoxelizerCPU.h"

using namespace NEOGEN;

/******************************************************/
/* CREACIO I DESTRUCCIO */
/******************************************************/
VoxelizerCPU::VoxelizerCPU(Ogre::SceneManager* mgr) : Voxelizer(mgr) {
	voxelizationMode = VOXELIZATION_MODE_CPU_EXACT;
}

VoxelizerCPU::~VoxelizerCPU() {
	
}

/******************************************************/
/* INICIALITZACIO */
/******************************************************/

void VoxelizerCPU::voxelization(ngTriangleList& tList, int voxelType) {
	//Obtinc la informaci� del model que representa l'escena
	//Ogre::LogManager::getSingletonPtr()->logMessage("numTris = " + std::to_string(tList.size()));
	//Recorro tots els triangles de la geometria i miro amb quins v�xels intersecta
	Ogre::Vector3 v1, v2, v3, initialVertex;
	Voxel* v;
	Voxel* vNeighbor;
	bool intersection;
	std::vector<Voxel*> voxelList;
	std::set<Voxel*> traversedVoxels;
	std::vector<Ogre::Vector3> neighbors;
	neighbors.push_back(Ogre::Vector3(-1.0f, 0.0f, 0.0f));
	neighbors.push_back(Ogre::Vector3(1.0f, 0.0f, 0.0f));
	neighbors.push_back(Ogre::Vector3(0.0f, 0.0f, -1.0f));
	neighbors.push_back(Ogre::Vector3(0.0f, 0.0f, 1.0f));
	neighbors.push_back(Ogre::Vector3(0.0f, 1.0f, 0.0f));
	std::vector<Ogre::Vector3>::iterator itNeighbor;
	VoxelGrid& vGrid = VoxelGrid::getSingleton();
	for (ngTriangleList::iterator it = tList.begin(); it != tList.end(); it++) {
		//Obtinc els 3 v�rtexs del triangle
		v1 = it->at(0);
		v2 = it->at(1);
		v3 = it->at(2);
		initialVertex = v1;
		if (v2.y < initialVertex.y) initialVertex = v2;
		if (v3.y < initialVertex.y) initialVertex = v3;
		voxelList.clear();
		traversedVoxels.clear();
		v = vGrid.getVoxelFromPos(initialVertex); //El v�xel inicial
		voxelList.push_back(v);	
		traversedVoxels.insert(v);
		while (!voxelList.empty()) {
			v = voxelList.back();
			voxelList.pop_back();
			intersection = checkTriangleBoxIntersection(it->at(0), it->at(1), it->at(2), v);
			if (intersection) {
				vGrid.setVoxelType(v->keyX, v->keyY, v->keyZ, voxelType);
				//Apilo els ve�ns
				for (itNeighbor = neighbors.begin(); itNeighbor != neighbors.end(); itNeighbor++) {
					vNeighbor = vGrid.getVoxel(v->keyX + (int)(itNeighbor->x), v->keyY + (int)(itNeighbor->y), v->keyZ + (int)(itNeighbor->z));
					if (vNeighbor->getType() == Voxel::TYPE_OUT) continue;	//Voxel out of bounds
					if (traversedVoxels.find(vNeighbor) == traversedVoxels.end()) {
						//El v�xel vNeighbor no ha estat visitat encara. Per tant, l'apilo
						voxelList.push_back(vNeighbor);
						traversedVoxels.insert(vNeighbor);
					}
				}
			}
		}
	}
	//Ogre::LogManager::getSingletonPtr()->logMessage("NUM POSITIVE = " + std::to_string(positiveVoxels.size()));
	drawVoxelizedModel();
}