#include "ngPathFinder.h"
#include "exClearanceSolver.h"

using namespace NEOGEN;
using namespace Exact;

/**************************************************************************************/
/* CREATION, INITIALIZATION AND DESTRUCTION */
/**************************************************************************************/
PathFinder::PathFinder() {
	startID = -1;
	goalID = -1;
	navMesh = NULL;
}

void PathFinder::initialize(NavMesh* nm) {
	//Clear the current stateList
	for (StateKeyTable::iterator it = _stateList.begin(); it != _stateList.end(); it++) delete it->second;
	_stateList.clear();
	_visitedStates.clear();

	//Initialize the state list for the new NavMesh
	navMesh = nm;
	CellTable& cTable = navMesh->getCellTable();
	int cellID;
	for (CellTable::iterator it = cTable.begin(); it != cTable.end(); it++) {
		cellID = it->first;
		_stateList[cellID] = new State(cellID);
	}
}

PathFinder::~PathFinder() {
	for (StateKeyTable::iterator it = _stateList.begin(); it != _stateList.end(); it++) delete it->second;
	_stateList.clear();
}

/**************************************************************************************/
/* STATE TREATMENT */
/**************************************************************************************/
State* PathFinder::openState(int stateID, int parentID) {
	State* s = _stateList[stateID];
	s->parentID = parentID;
	s->g = computeG(stateID, parentID);
	s->h = computeH(stateID);
	s->f = s->g + s->h;
	s->type = State::TYPE_OPEN;
	openStates.insert(s);
	_visitedStates.push_back(stateID);
	return s;
}

State* PathFinder::reopenState(int stateID, int parentID) {
	State* s = _stateList[stateID];
	openStates.erase(s);
	s->reset();
	return openState(stateID, parentID);
}

void PathFinder::closeState(State* s) {
	s->type = State::TYPE_CLOSED;
	openStates.erase(s);
}

/**************************************************************************************/
/* CLASS FUNCTIONS */
/**************************************************************************************/
void PathFinder::solve(int begin, int end, Vec3D& goalPosition, float charRadius, Path& path) {
	path.clear();
	startID = begin;
	goalID = end;
	//Primer de tot cal comprovar si el punt de dest� t� prou clearance. 
	bool goalClearance = checkGoalClearance(goalPosition, charRadius);
	if (!goalClearance) return;
	
	minClearance2 = (charRadius * 2.0f) * (charRadius * 2.0f);	//Clearance m�nim necessari perqu� el personatge pugui arribar al dest�
	//Creo el primer node
	State* cStart = openState(startID, -1);
	
	//Estructura on guardo el cam�
	std::map<int, int> cameFrom;	//cameFrom[i] = x => el node predecessor de i �s x
	cameFrom[startID] = -1;
	
	State* current = cStart;
	//Ogre::LogManager::getSingletonPtr()->logMessage("STARTING SOLVING");
	while ((current->cellID != goalID) && (!openStates.empty())) {
		//Indico que l'estat actual ja ha estat visitat (el tanco)
		//Ogre::LogManager::getSingletonPtr()->logMessage("currentCellID = " + std::to_string(current->cellID));
		closeState(current);
		
		//Genero els successors del node actual i faig el tractament de repetits
		generateSuccessors(current);

		if (!openStates.empty()) {
			//Agafo el seg�ent node amb menor cost
			current = *openStates.begin();
			cameFrom[current->cellID] = current->parentID;
		}
	}
	//Check if we have finished because we have arrived to the goal node
	if (current->cellID == goalID) reconstructPath(cameFrom, path);

	//Restore the PathFinder state
	//for (StateKeyTable::iterator it = _stateList.begin(); it != _stateList.end(); it++) it->second->reset();
	for (std::vector<int>::iterator it = _visitedStates.begin(); it != _visitedStates.end(); it++) _stateList[*it]->reset();
	_visitedStates.clear();
	openStates.clear();
}

bool PathFinder::checkGoalClearance(Vec3D& goalPosition, float charRadius) {
	//Indica si la posici� de dest� del personatge t� prou clearance, �s a dir, si totes les arestes obstacle 
	//de la cel�la final estan a una dist�ncia >= al radi del personatge. 
	return navMesh->checkCellClearance(goalID, goalPosition, charRadius);
}

void PathFinder::generateSuccessors(State* current) {
	//Genera els successors de l'estat current
	//Ogre::LogManager::getSingletonPtr()->logMessage("GENERATING SUCCESSORS");
	std::set<int>& adjacentTo = navMesh->getCell(current->cellID).adjacentTo; //Nodes adjacents a la cel�la actual
		
	State* successor;
	float newG, oldG;
	//Ogre::LogManager::getSingletonPtr()->logMessage("numAdjacents = " + std::to_string(adjacentTo.size()));
	std::pair<int, int> portalPair;
	float clearance2;
	for (std::set<int>::iterator it = adjacentTo.begin(); it != adjacentTo.end(); it++) {
		if (*it == current->parentID) continue;	//Evitem tornar a obrir el node pare
		//Comprobo si tinc prou clearance per arribar al successor
		if (current->cellID == startID) {
			//�s el node inicial. En aquest cas, comprobo si el personatge passa pel portal que uneix el node actual amb 
			//el successor. 
			portalPair.first = navMesh->getPortalTo(current->cellID, *it);
			clearance2 = navMesh->getEdge(portalPair.first).clearance2;
		}
		else {
			//�s un node intermig que est� entre l'inicial i el final. 
			portalPair.first = navMesh->getPortalTo(current->cellID, current->parentID);
			portalPair.second = navMesh->getPortalTo(current->cellID, *it);
			clearance2 = ClearanceSolver::getSingleton().getCross(portalPair);
		}
		if (RealMath::greaterOrEqual(clearance2, minClearance2)) {
			//El cam� fins al successor t� prou clearance. Comprobo si cal obrir el successor. 
			successor = _stateList[*it];
			if (successor->type == State::TYPE_NULL) {
				//�s un estat que mai s'ha obert. El genero
				openState(*it, current->cellID);
			}
			else {
				//L'estat ja ha estat obert en algun moment. 
				//Cal comprobar si el cost nou per arribar a aquest estat �s inferior o no al cost que t� actualment
				//El cost d'un node �s f = g + h, on g �s la part coneguda i h �s la part estimada per arribar al node
				//final. Com que el node �s el mateix (nom�s canvia d'on venim) el valor h �s el mateix en els dos casos. 
				//Per tant, n'hi ha prou amb comprobar el cost g. 
				oldG = computeG(successor->cellID, successor->parentID);
				newG = computeG(successor->cellID, current->cellID);
				if (newG < oldG) {
					//El nou cost �s inferior al cost antic. Reobro/actualitzo el node
					reopenState(*it, current->cellID);
				}
			}
		}
	}
	//Ogre::LogManager::getSingletonPtr()->logMessage("SUCCESSORS GENERATED");
}

float PathFinder::computeG(int stateID, int parentID) {
	//Calcula el cost g del node . El calculo com el cost g del pare + 
	//la dist�ncia^2 entre el centre del pare i el centre del node
	if ((stateID == startID) || (parentID == -1)) return 0.0f;
	else {
		Cell c1 = navMesh->getCell(stateID);
		Cell c2 = navMesh->getCell(parentID);
		float gParent = _stateList[parentID]->g;
		return (gParent + computeDistance(&c1, &c2));
	}
}

float PathFinder::computeH(int stateID) {
	//Calcula el cost h del node-> Com a heur�stic faig servir la dist�ncia^2 entre el centre
	//del node i el centre del node final
	if (stateID == goalID) return 0.0f;
	else {
		Cell c1 = navMesh->getCell(stateID);
		Cell c2 = navMesh->getCell(goalID);
		return (computeDistance(&c1, &c2));
	}
}

float PathFinder::computeDistance(Cell* c1, Cell* c2) {
	//Calcula la dist�ncia^2 entre els centres de dues cel�les

	//Centre de c1
	Vec3D centerC1;
	c1->p->getCenter(centerC1);

	//Centre de c2
	Vec3D centerC2;	
	c2->p->getCenter(centerC2);
	
	return (centerC1.dist2(centerC2));	//dist�ncia^2 entre centres
}

void PathFinder::reconstructPath(std::map<int, int>& cameFrom, Path& path) {
	//Reconstrueix el cam� trobat per arribar del node inicial al node final

	//El cam� el reconstrueixo comen�ant pel node final i fins el node inicial
	if (startID == goalID) path.addNode(goalID);
	else {
		std::deque<int> nodes;
		int nodeID = goalID;
		do {
			nodes.push_front(nodeID);
			nodeID = cameFrom.at(nodeID);
		} while (cameFrom.at(nodeID) != -1);
		nodes.push_front(startID);

		while (!nodes.empty()) {
			nodeID = nodes.front();
			nodes.pop_front();
			path.addNode(nodeID);
		}
	}
}