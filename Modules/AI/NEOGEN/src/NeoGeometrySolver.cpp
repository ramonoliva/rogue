#include <boost/geometry.hpp>
#include <boost/geometry/geometries/geometries.hpp>
#include <boost/geometry/geometries/point.hpp>

#include "NeoGeometrySolver.h"
#include "NeoMath.h"
#include "NeoFace.h"
#include "NeoClipper.h"
#include "NeoTriBoxOverlap.h"
#include "NeoTriTriOverlap.h"
#include "NeoAABB.h"

using namespace NEOGEN;
using namespace NeoGeometrySolver;

#pragma region POLYGON OPERATIONS

Vector3 NeoGeometrySolver::getPolygonNormal(const Polygon3& polygon) {
	Vector3 normal = Vector3(0, 0, 0);
	if (polygon.size() > 2) {

		//Find a vertex of the Convex Hull of the face. For example, the left most vertex. 
		size_t leftMostVertexID = 0;
		for (size_t i = 1; i < polygon.size(); i++) {
			if (polygon[i].getX() < polygon[leftMostVertexID].getX()) {
				leftMostVertexID = i;
			}
		}

		//Compute the normal, taking as reference the begin vertex of the left edge
		const Vector3& p = polygon[leftMostVertexID];
		const Vector3& pPrev = polygon[(leftMostVertexID > 0) ? leftMostVertexID - 1 : polygon.size() - 1];

		//Find pNext. This cannot be collinear to (p, pPrev)
		Vector3 v = (pPrev - p).normalized();
		Vector3 pNext;
		size_t markID = (leftMostVertexID + 1) % polygon.size();
		size_t nextVertexID = markID;
		do {
			pNext = polygon[nextVertexID];
			Vector3 u = (pNext - p).normalized();
			normal = u.cross(v).normalized();
			nextVertexID = (nextVertexID + 1) % polygon.size();
		} while (normal.equal(Vector3(0, 0, 0)) && (nextVertexID != markID));
	}

	return normal;
}

bool NeoGeometrySolver::getPolygonOrientation(const Polygon3& polygon, const Vector3& up) {
	//1) Look for a vertex in the convex hull. For example, the most left vertex
	size_t leftPointID = 0;
	for (size_t i = 1; i < polygon.size(); i++) {
		if (polygon[i].getX() < polygon[leftPointID].getX()) leftPointID = i;
	}
	size_t prevPointID = (leftPointID > 0)? leftPointID - 1 : polygon.size() - 1;
	size_t nextPointID = (leftPointID + 1) % polygon.size();

	//2) By definition, a vertex of the convex hull cannot pertain to a concavity. So the 
	//normal at this point will determine the orientation of the whole polygon. 
	Vector3 leftPoint = polygon[leftPointID];
	Vector3 nextPoint = polygon[nextPointID];
	Vector3 prevPoint = polygon[prevPointID];

	Vector3 e1Dir = nextPoint - leftPoint;
	Vector3 e2Dir = prevPoint - leftPoint;
	Vector3 n = e1Dir.cross(e2Dir);
	NeoReal d = n.dot(up);

	return (!Math::equal(d, 0) && (d > 0));
}

bool NeoGeometrySolver::getPolygonOrientation(const Triangle& triangle, const Vector3& up) {
	//Transform the triangle into a polygon
	Polygon3 polygon;
	for (Triangle::const_iterator it = triangle.begin(); it != triangle.end(); it++) {
		polygon.push_back((*it)->getPosition());
	}
	return getPolygonOrientation(polygon, up);
}

void NeoGeometrySolver::getSubPolygon(const Polygon3& contourOriginal, size_t startVertex, size_t endVertex, Polygon3& result) {
	size_t i = startVertex;
	size_t numVertices = contourOriginal.size();
	do {
		result.push_back(contourOriginal[i]);
		i = (i + 1) % numVertices;
	} while (i != ((endVertex + 1) % numVertices));

}

bool NeoGeometrySolver::pointInPolygon(const Vector3& point, const Polygon3& polygon) {
	ClipperLib::Path path;
	NeoClipper::toClipper(polygon, path);
	ClipperLib::IntPoint cPoint = NeoClipper::toClipper(point);

	return (ClipperLib::PointInPolygon(cPoint, path) != 0);
}

NeoReal NeoGeometrySolver::polygonArea(const Polygon3& polygon) {
	ClipperLib::Path path;
	NeoClipper::toClipper(polygon, path);
	NeoReal sf = NeoClipper::getClipperScaleFactor() * NeoClipper::getClipperScaleFactor();
	return std::abs(NeoReal(ClipperLib::Area(path)) / sf);
}

void NeoGeometrySolver::polygonReverse(Polygon3& polygon) {
	Polygon3 tmp = polygon;
	polygon.clear();
	for (Polygon3::reverse_iterator it = tmp.rbegin(); it != tmp.rend(); it++) {
		polygon.push_back(*it);
	}
}

bool NeoGeometrySolver::isPolygonMonotone(const Polygon3& polygon) {
	int localMins = 0;
	for (size_t i = 0; i < polygon.size(); i++) {
		const Vector3& prevPos = (i == 0) ? polygon[polygon.size() - 1] : polygon[i - 1];
		const Vector3& currentPos = polygon[i];
		const Vector3& nextPos = polygon[(i + 1) % polygon.size()];
		if (
			(Math::lessOrEqual(currentPos.getX(), nextPos.getX())) &&
			(Math::less(currentPos.getX(), prevPos.getX()))
			)
		{
			localMins++;
		}
	}

	return localMins == 1;
}

bool NeoGeometrySolver::isPolygonIntersection(const Polygon3& p1, const Polygon3& p2) {
	bool intersection = false;
	Polygon3::const_iterator it = p2.begin(); 
	while (!intersection && (it != p2.end())) {
		intersection = pointInPolygon(*it, p1);
		it++;
	}

	return intersection;
}

bool NeoGeometrySolver::isTriangleBoxIntersection(const Polygon3& tri, const NeoAABB& aabb) {
	const Vector3 c = aabb.getCenter();
	const Vector3 h = aabb.getSizeHalf();
	
	NeoReal boxCenter[3] = {c.getX(), c.getY(), c.getZ()};
	NeoReal boxHalfSize[3] = {h.getX(), h.getY(), h.getZ()};
	NeoReal triVerts[3][3] = 
							{
								{tri[0].getX(), tri[0].getY(), tri[0].getZ()},
								{tri[1].getX(), tri[1].getY(), tri[1].getZ()},
								{tri[2].getX(), tri[2].getY(), tri[2].getZ()}
							};
	return (Akenine::triBoxOverlap(boxCenter, boxHalfSize, triVerts) == 1);
}

bool NeoGeometrySolver::isTriangleTriangleIntersection(const Polygon3& t1, const Polygon3& t2) {
	float t1_0[] = { float(t1[0].getX()), float(t1[0].getY()), float(t1[0].getZ()) };
	float t1_1[] = { float(t1[1].getX()), float(t1[1].getY()), float(t1[1].getZ()) };
	float t1_2[] = { float(t1[2].getX()), float(t1[2].getY()), float(t1[2].getZ()) };

	float t2_0[] = { float(t2[0].getX()), float(t2[0].getY()), float(t2[0].getZ()) };
	float t2_1[] = { float(t2[1].getX()), float(t2[1].getY()), float(t2[1].getZ()) };
	float t2_2[] = { float(t2[2].getX()), float(t2[2].getY()), float(t2[2].getZ()) };

	return (Akenine::tri_tri_intersect(t1_0, t1_1, t1_2, t2_0, t2_1, t2_2) == 1);
}

#pragma endregion

#pragma region LINE PLANE INTERSECTION

bool NeoGeometrySolver::linePlaneIntersection(const Line3& line, const Plane& plane, IntersectionResult3& result) {
	return linePlaneIntersection(line._v0, line._v1, plane, result);
}

bool NeoGeometrySolver::linePlaneIntersection(const Vector3& p0, const Vector3& p1, const Plane& plane, IntersectionResult3& result) {
	const Vector3& v0 = plane._v0;
	const Vector3& n = plane._normal;
	Vector3 u = p1 - p0;
	Vector3 w = p0 - v0;
	NeoReal d = n.dot(u);
	if (Math::equal(d, 0)) {
		//The line and the plane are parallel. Check if the line is contained in the plane or not. 
		//We check if one of the endpoints of the line is also a point of the plane, so:
		NeoReal dist = pointLineDistance(v0, p0, p1);
		if (Math::equal(dist, 0)) {
		/*if (Math::equal(n.dot(w), 0)) {*/
			result._intersectionType = IntersectionType::COINCIDENT;
			result._intersectionPoint = pointPlaneProjection(p0, plane); //p0;
			result._intersectionPoint2 = pointPlaneProjection(p1, plane); //p1;
		}
		else result._intersectionType = IntersectionType::PARALLEL;
	}
	else {
		//The line and the plane are not parallel, so they intersect in a single point
		NeoReal si = n.dot(-w) / d;
		if (Math::equal(si, 0)) result._intersectionPoint = result._intersectionPoint2 = p0;
		else if (Math::equal(si, 1)) result._intersectionPoint = result._intersectionPoint2 = p1;
		else result._intersectionPoint = result._intersectionPoint2 = p0 + u * si;

		//Check if the intersection point is in the direction pointed by the 
		//hintDir of the plane. 
		Vector3 t = result._intersectionPoint - v0;
		result._intersectionType = (Math::greaterOrEqual(plane._hintDir.dot(t), 0))? IntersectionType::SINGLE_POINT : IntersectionType::UNDEFINED;
	}

	return result.isIntersection();
}

bool NeoGeometrySolver::rayPlaneIntersection(const Ray3& ray, const Plane& plane, IntersectionResult3& result) {
	linePlaneIntersection(ray._v0, ray._v0 + ray._direction, plane, result);
	if ((result._intersectionType == IntersectionType::SINGLE_POINT) && Math::less(ray._direction.dot(result._intersectionPoint - ray._v0), NeoReal(0.0))) {
		result._intersectionType = IntersectionType::UNDEFINED;
	}

	return result.isIntersection();
}

bool NeoGeometrySolver::segmentPlaneIntersection(const Vector3& p0, const Vector3& p1, const Plane& plane, IntersectionResult3& result) {
	linePlaneIntersection(p0, p1, plane, result);
	if ((result._intersectionType == IntersectionType::SINGLE_POINT) && !isPointInSegment(result._intersectionPoint, p0, p1)) {
		result._intersectionType = IntersectionType::UNDEFINED;
	}

	return result.isIntersection();
}

#pragma endregion

#pragma region LINE TRIANGLE INTERSECTION

bool NeoGeometrySolver::computeBarycentricCoordinates(const Vector3& a, const Vector3& b, const Vector3& c, const Vector3& p, Vector3& result) {
	Vector3 ab = b - a;
	Vector3 ac = c - a;
	NeoReal areaABC = ab.cross(ac).norm() * NeoReal(0.5);

	Vector3 ca = a - c;
	Vector3 cp = p - c;
	NeoReal areaCAP = ca.cross(cp).norm() * NeoReal(0.5);

	Vector3 ap = p - a;
	NeoReal areaABP = ab.cross(ap).norm() * NeoReal(0.5);

	NeoReal u = areaCAP / areaABC;
	NeoReal v = areaABP / areaABC;

	result = (Vector3(u, v, NeoReal(1.0) - u - v));

	return
		(
		(Math::greaterOrEqual(result.getX(), NeoReal(0.0)) && Math::lessOrEqual(result.getX(), NeoReal(1.0))) &&
		(Math::greaterOrEqual(result.getY(), NeoReal(0.0)) && Math::lessOrEqual(result.getY(), NeoReal(1.0))) &&
		(Math::greaterOrEqual(result.getZ(), NeoReal(0.0)) && Math::lessOrEqual(result.getZ(), NeoReal(1.0)))
		);

}

bool NeoGeometrySolver::lineTriangleIntersection(const Line3& line, const Vector3& a, const Vector3& b, const Vector3& c, IntersectionResult3& result) {
	return lineTriangleIntersection(line._v0, line._v1, a, b, c, result);
}

bool NeoGeometrySolver::lineTriangleIntersection(const Vector3& p0, const Vector3& p1, const Vector3& a, const Vector3& b, const Vector3& c, IntersectionResult3& result) {
	Vector3 ab = b - a;
	Vector3 ac = c - a;
	linePlaneIntersection(p0, p1, Plane(a, ab.cross(ac)), result);

	if (result._intersectionType == IntersectionType::SINGLE_POINT) {
		//Check the barycentric coordinates of the single intersection point. 
		Vector3 bCoordinates;
		if (!computeBarycentricCoordinates(a, b, c, result._intersectionPoint, bCoordinates)) {
			result._intersectionType = IntersectionType::UNDEFINED;
		}
	}
	else if (result._intersectionType == IntersectionType::COINCIDENT) {
		//Check if the line intersects any of the edges of the triangle
		IntersectionResult3 tmpAB, tmpBC, tmpCA;
		lineSegmentIntersection(p0, p1, a, b, tmpAB);
		lineSegmentIntersection(p0, p1, b, c, tmpBC);
		lineSegmentIntersection(p0, p1, c, a, tmpCA);

		//Case 1: The line is coincident with one of the edges of the triangle
		if (tmpAB._intersectionType == IntersectionType::COINCIDENT) result = tmpAB;
		else if (tmpBC._intersectionType == IntersectionType::COINCIDENT) result = tmpBC;
		else if (tmpCA._intersectionType == IntersectionType::COINCIDENT) result = tmpCA;

		//Case 2: The line intersects 2 edges of the triangle in a single point each one. 
		else if ((tmpAB._intersectionType == IntersectionType::SINGLE_POINT) && (tmpBC._intersectionType == IntersectionType::SINGLE_POINT)) {
			result._intersectionPoint = tmpAB._intersectionPoint;
			result._intersectionPoint2 = tmpBC._intersectionPoint;
		}
		else if ((tmpAB._intersectionType == IntersectionType::SINGLE_POINT) && (tmpCA._intersectionType == IntersectionType::SINGLE_POINT)) {
			result._intersectionPoint = tmpAB._intersectionPoint;
			result._intersectionPoint2 = tmpCA._intersectionPoint;
		}
		else if ((tmpBC._intersectionType == IntersectionType::SINGLE_POINT) && (tmpCA._intersectionType == IntersectionType::SINGLE_POINT)) {
			result._intersectionPoint = tmpBC._intersectionPoint;
			result._intersectionPoint2 = tmpCA._intersectionPoint;
		}

		//Case 3: No intersection at all. 
		else {
			result._intersectionType = IntersectionType::UNDEFINED;
		}
	}

	return result.isIntersection();
}

bool NeoGeometrySolver::rayTriangleIntersection(const Ray3& ray, const Vector3& a, const Vector3& b, const Vector3& c, IntersectionResult3& result) {
	IntersectionResult3 tmp;
	lineTriangleIntersection(ray._v0, ray._v0 + ray._direction, a, b, c, tmp);
	result = tmp;

	int uSign = Math::sign(ray._direction.dot(result._intersectionPoint - ray._v0));	//1 => intersectionPoint in the direction of the ray; -1 otherwise
	int vSign = Math::sign(ray._direction.dot(result._intersectionPoint2 - ray._v0));	//1 => intersectionPoint2 in the direction of the ray; -1 otherwise

	if ((result._intersectionType == IntersectionType::SINGLE_POINT) && (uSign == -1)) {
		result._intersectionType = IntersectionType::UNDEFINED;
	}
	else if (result._intersectionType == IntersectionType::COINCIDENT) {
		if ((uSign == -1) && (vSign == -1)) {
			//Both intersection points are behind the ray. So there is no intersection at all. 
			result._intersectionType = IntersectionType::UNDEFINED;
		}
		else if ((uSign == 1) && (vSign == -1)) {
			//The first intersection point is in the direction of the ray, but the second intersection point is behind. 
			result._intersectionPoint = ray._v0;
			result._intersectionPoint2 = tmp._intersectionPoint;
		}
		else if ((uSign == -1) && (vSign == 1)) {
			//The first intersection point is behind the ray, but the second intersection point is in the direction of the ray. 
			result._intersectionPoint = ray._v0;
			result._intersectionPoint2 = tmp._intersectionPoint2;
		}
	}

	return result.isIntersection();
}

#pragma endregion

#pragma region LINE LINE INTERSECTION

bool NeoGeometrySolver::lineLineIntersection(const Vector3& p0, const Vector3& p1, const Vector3& q0, const Vector3& q1, IntersectionResult3& result) {
	//The line intersection is computed in 2D
	Vector3 s = p1 - p0;
	Vector3 t = q1 - q0;

	Math::ProjectionPlane pPlane;
	if (!s.isParallelTo(Vector3(0, 1, 0)) && !t.isParallelTo(Vector3(0, 1, 0))) pPlane = Math::ProjectionPlane::XZ;
	else if (!s.isParallelTo(Vector3(1, 0, 0)) && !t.isParallelTo(Vector3(1, 0, 0))) pPlane = Math::ProjectionPlane::YZ;
	else pPlane = Math::ProjectionPlane::XY;

	IntersectionResult2 tmpResult;
	lineLineIntersection(p0.to2D(pPlane), p1.to2D(pPlane), q0.to2D(pPlane), q1.to2D(pPlane), tmpResult);

	//Transform back the result to a 3D point
	result._intersectionType = tmpResult._intersectionType;
	if (result._intersectionType == IntersectionType::SINGLE_POINT) {
		Vector2 u = p1.to2D(pPlane) - p0.to2D(pPlane);
		Vector2 v = q1.to2D(pPlane) - q0.to2D(pPlane);
		Vector2 w = p0.to2D(pPlane) - q0.to2D(pPlane);
		
		NeoReal si = -v.perp().dot(w) / v.perp().dot(u);
		if (Math::equal(si, 0)) result._intersectionPoint = result._intersectionPoint2 = p0;
		else if (Math::equal(si, 1)) result._intersectionPoint = result._intersectionPoint2 = p1;
		else result._intersectionPoint = result._intersectionPoint2 = p0 + (p1 - p0) * si;
		
		//Check if the intersection point is a point of both lines. 
		if (
			!Math::equal(pointLineDistance2(result._intersectionPoint, p0, p1), 0) ||
			!Math::equal(pointLineDistance2(result._intersectionPoint, q0, q1), 0)
			)
		{
			result._intersectionType = IntersectionType::UNDEFINED;
		}
	}

	return result.isIntersection();
}

bool NeoGeometrySolver::lineLineIntersection(const Vector2& p0, const Vector2& p1, const Vector2& q0, const Vector2& q1, IntersectionResult2& result) {
	Vector2 u = p1 - p0;
	Vector2 v = q1 - q0;
	Vector2 w = p0 - q0;
	
	if (Math::equal(u.perp().dot(v), NeoReal(0))) {
		//Parallel lines. Check if they are coincident or not. 
		result._intersectionType = Math::equal(w.perp().dot(v), NeoReal(0))? IntersectionType::COINCIDENT : IntersectionType::PARALLEL;
	}
	else {
		NeoReal si =  -v.perp().dot(w) / v.perp().dot(u);
		if (Math::equal(si, 0)) result._intersectionPoint = result._intersectionPoint2 = p0;
		else if (Math::equal(si, 1)) result._intersectionPoint = result._intersectionPoint2 = p1;
		else result._intersectionPoint = result._intersectionPoint2 = p0 + u * si;

		result._intersectionType = IntersectionType::SINGLE_POINT;
	}

	return result.isIntersection();
}

#pragma endregion

#pragma region RAY BOX INTERSECTION

bool NeoGeometrySolver::rayBoxIntersection(const Ray3& ray, const NeoAABB& box) {
	IntersectionResult3 dummyResult;
	return rayBoxIntersection(ray, box, dummyResult);
}

bool NeoGeometrySolver::rayBoxIntersection(const Ray3& ray, const NeoAABB& box, IntersectionResult3& result) {
	NeoReal lowt = NeoReal(0);
	NeoReal t;
	bool hit = false;
	Vector3 hitpoint;
	const Vector3& rayorig = ray._v0;
	const Vector3& raydir = ray._direction;
	const Vector3& min = box.getMin();
	const Vector3& max = box.getMax();

	// Check origin inside first
	if (box.contains(rayorig)) return true;

	// Check each face in turn, only check closest 3
	// Min x
	if (Math::lessOrEqual(rayorig.getX(), min.getX()) && Math::greater(raydir.getX(), 0)) {
		t = (min.getX() - rayorig.getX()) / raydir.getX();
		if (Math::greaterOrEqual(t, 0)) {
			// Substitute t back into ray and check bounds and dist
			hitpoint = rayorig + raydir * t;
			if (Math::greaterOrEqual(hitpoint.getY(), min.getY()) && Math::lessOrEqual(hitpoint.getY(), max.getY()) &&
				Math::greaterOrEqual(hitpoint.getZ(), min.getZ()) && Math::lessOrEqual(hitpoint.getZ(), max.getZ()) &&
				(!hit || t < lowt))
			{
				hit = true;
				lowt = t;
			}
		}
	}

	// Max x
	if (Math::greaterOrEqual(rayorig.getX(), max.getX()) && Math::less(raydir.getX(), 0)) {
		t = (max.getX() - rayorig.getX()) / raydir.getX();
		if (Math::greaterOrEqual(t, 0)) {
			// Substitute t back into ray and check bounds and dist
			hitpoint = rayorig + raydir * t;
			if (Math::greaterOrEqual(hitpoint.getY(), min.getY()) && Math::lessOrEqual(hitpoint.getY(), max.getY()) &&
				Math::greaterOrEqual(hitpoint.getZ(), min.getZ()) && Math::lessOrEqual(hitpoint.getZ(), max.getZ()) &&
				(!hit || t < lowt))
			{
				hit = true;
				lowt = t;
			}
		}
	}

	// Min y
	if (Math::lessOrEqual(rayorig.getY(), min.getY()) && Math::greater(raydir.getY(), 0)) {
		t = (min.getY() - rayorig.getY()) / raydir.getY();
		if (Math::greaterOrEqual(t, 0)) {
			// Substitute t back into ray and check bounds and dist
			hitpoint = rayorig + raydir * t;
			if (Math::greaterOrEqual(hitpoint.getX(), min.getX()) && Math::lessOrEqual(hitpoint.getX(), max.getX()) &&
				Math::greaterOrEqual(hitpoint.getZ(), min.getZ()) && Math::lessOrEqual(hitpoint.getZ(), max.getZ()) &&
				(!hit || t < lowt))
			{
				hit = true;
				lowt = t;
			}
		}
	}

	// Max y
	if (Math::greaterOrEqual(rayorig.getY(), max.getY()) && Math::less(raydir.getY(), 0)) {
		t = (max.getY() - rayorig.getY()) / raydir.getY();
		if (Math::greaterOrEqual(t, 0)) {
			// Substitute t back into ray and check bounds and dist
			hitpoint = rayorig + raydir * t;
			if (Math::greaterOrEqual(hitpoint.getX(), min.getX()) && Math::lessOrEqual(hitpoint.getX(), max.getX()) &&
				Math::greaterOrEqual(hitpoint.getZ(), min.getZ()) && Math::lessOrEqual(hitpoint.getZ(), max.getZ()) &&
				(!hit || t < lowt))
			{
				hit = true;
				lowt = t;
			}
		}
	}

	// Min z
	if (Math::lessOrEqual(rayorig.getZ(), min.getZ()) && Math::greater(raydir.getZ(), 0)) {
		t = (min.getZ() - rayorig.getZ()) / raydir.getZ();
		if (Math::greaterOrEqual(t, 0)) {
			// Substitute t back into ray and check bounds and dist
			hitpoint = rayorig + raydir * t;
			if (Math::greaterOrEqual(hitpoint.getX(), min.getX()) && Math::lessOrEqual(hitpoint.getX(), max.getX()) &&
				Math::greaterOrEqual(hitpoint.getY(), min.getY()) && Math::lessOrEqual(hitpoint.getY(), max.getY()) &&
				(!hit || t < lowt))
			{
				hit = true;
				lowt = t;
			}
		}
	}

	// Max z
	if (Math::greaterOrEqual(rayorig.getZ(), max.getZ()) && Math::less(raydir.getZ(), 0)) {
		t = (max.getZ() - rayorig.getZ()) / raydir.getZ();
		if (Math::greaterOrEqual(t, 0)) {
			// Substitute t back into ray and check bounds and dist
			hitpoint = rayorig + raydir * t;
			if (Math::greaterOrEqual(hitpoint.getX(), min.getX()) && Math::lessOrEqual(hitpoint.getX(), max.getX()) &&
				Math::greaterOrEqual(hitpoint.getY(), min.getY()) && Math::lessOrEqual(hitpoint.getY(), max.getY()) &&
				(!hit || t < lowt))
			{
				hit = true;
				lowt = t;
			}
		}
	}

	if (hit && Math::lessOrEqual(lowt, ray._length)) {
		result._intersectionType = IntersectionType::SINGLE_POINT;
		result._intersectionPoint = result._intersectionPoint2 = rayorig + raydir * lowt;
		return true;
	}

	return false;
}

#pragma endregion

NeoReal NeoGeometrySolver::pointLineDistance2(const Vector3& p, const Vector3& l0, const Vector3& l1) {
	Vector3 u = (l1 - l0).normalized();
	Vector3 w = p - l0;
	return u.cross(w).norm2();
}

NeoReal NeoGeometrySolver::pointLineDistance2(const Vector2& p, const Vector2& l0, const Vector2& l1) {
	return pointLineDistance2(Vector3(p.getX(), p.getY(), 0), Vector3(l0.getX(), l0.getY(), 0), Vector3(l1.getX(), l1.getY(), 0));
}

Vector3 NeoGeometrySolver::pointPlaneProjection(const Vector3& p, const Plane& plane) {
	//1) Compute the projection of p on the line supported by the plane's normal
	Vector3 nProj = pointLineProjection(p, plane._v0, plane._v0 + plane._normal);

	//2) Move p towards the plane in the direction of the normal. 
	return p + (plane._v0 - nProj);
}

