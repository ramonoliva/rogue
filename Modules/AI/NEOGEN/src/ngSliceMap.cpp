#include "ngSliceMap.h"

using namespace NEOGEN;

/****************************************************************/
/* CREACIO I DESTRUCCIO */
/****************************************************************/

SliceMap::SliceMap(std::vector<Ogre::TexturePtr>& texList, size_t w, size_t d, size_t h, int orient) {
	width = w;
	depth = d;
	height = h;
	orientation = orient;
	createColourTable(texList);
}

void SliceMap::createColourTable(std::vector<Ogre::TexturePtr>& texList) {
	//Crea la colourTable a partir d'una llista de textures que representa el sliceMap
	Ogre::LogManager::getSingletonPtr()->logMessage("CREATING COLOUR TABLE!!!");
	Ogre::Image img;
	Ogre::uint32 colorValue;
	ColourTexture cTexture;
	std::vector<Ogre::uint32> vCol;
	float requiredNumTargets = Ogre::Math::Ceil((float)depth / (float)COLOUR_TEXTURE_DEPTH);
	Ogre::LogManager::getSingletonPtr()->logMessage("requiredNumTargets = " + std::to_string(requiredNumTargets));
	Ogre::LogManager::getSingletonPtr()->logMessage("width = " + std::to_string(width));
	Ogre::LogManager::getSingletonPtr()->logMessage("depth = " + std::to_string(depth));
	Ogre::LogManager::getSingletonPtr()->logMessage("height = " + std::to_string(height));
	Ogre::LogManager::getSingletonPtr()->logMessage("orientation = " + std::to_string(orientation));
	for (size_t target = 0; target < (size_t)requiredNumTargets; target++) {
	//for (std::vector<Ogre::TexturePtr>::iterator it = texList.begin(); it != texList.end(); it++) {
		//Construeixo la cTexture a partir de la imatge
		texList[target]->convertToImage(img);
		cTexture.clear();
		for (size_t x = 0; x < width; x++) {
			vCol.clear();
			for (size_t z = 0; z < height; z++) {
				colorValue = img.getColourAt(x, z, 0).getAsRGBA();
				vCol.push_back(colorValue);
			}
			cTexture.push_back(vCol);
		}
		cTable.push_back(cTexture);
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("NUM COLOURTEXTURES = " + std::to_string(cTable.size()));
}

SliceMap::SliceMap(size_t w, size_t d, size_t h) {
	width = w;
	depth = d;
	height = h;
	orientation = ORIENTATION_TOP;
	createEmptyColourTable();
}

void SliceMap::createEmptyColourTable() {
	//Crea una colourTable buida TENINT EN COMPTE ORIENTACI� TOP
	std::vector<Ogre::uint32> vCol;
	ColourTexture cTexture;
	float requiredNumTargets = Ogre::Math::Ceil((float)depth / (float)COLOUR_TEXTURE_DEPTH);
	for (size_t target = 0; target < (size_t)requiredNumTargets; target++) {
		//Construeixo la cTexture
		cTexture.clear();
		for (size_t x = 0; x < width; x++) {
			vCol.clear();
			for (size_t z = 0; z < height; z++) {
				vCol.push_back(0);
			}
			cTexture.push_back(vCol);
		}
		//Afegeixo la cTexture
		cTable.push_back(cTexture);
	}
}

SliceMap::~SliceMap() {}

void SliceMap::clear() {
	//Neteja totes les textures que formen el sliceMap
	for (ColourTextureList::iterator it = cTable.begin(); it != cTable.end(); it++) {
		clearColourTexture(*it);
	}
}

void SliceMap::clearColourTexture(ColourTexture& cTexture) {
	//Neteja una textura del sliceMap
	for (size_t x = 0; x < width; x++) {
		for (size_t z = 0; z < height; z++) {
			cTexture[x][z] = 0;
		}
	}
}

/****************************************************************/
/* GET I SET */
/****************************************************************/

Ogre::uint32 SliceMap::getColourAt(size_t target, size_t x, size_t z) {
	ColourTexture& cTexture = cTable[target];	//La colourTexture que cont� el color que m'interessa
	return (cTexture[x][z]);
}

void SliceMap::setColourAt(size_t target, Ogre::uint32 color, size_t x, size_t z) {
	ColourTexture& cTexture = cTable[target];	//La colourTexture que cont� el color que vull canviar
	cTexture[x][z] = color;
}

void SliceMap::setSolidVoxel(size_t x, size_t y, size_t z) {
	//Ogre::LogManager::getSingletonPtr()->logMessage("FUSION");
	//Ogre::LogManager::getSingletonPtr()->logMessage("x = " + std::to_string(x));
	//Ogre::LogManager::getSingletonPtr()->logMessage("y = " + std::to_string(y));
	//Ogre::LogManager::getSingletonPtr()->logMessage("z = " + std::to_string(z));
	size_t target = y / COLOUR_TEXTURE_DEPTH;
	target = (cTable.size() - target - 1);
	size_t coordY = y % COLOUR_TEXTURE_DEPTH;
	Ogre::uint32 cellMask = 1 << coordY;
	Ogre::uint32 columnValue = getColourAt(target, x, z);
	setColourAt(target, cellMask | columnValue, x, z);
}

void SliceMap::setEmptyVoxel(size_t x, size_t y, size_t z) {
	//Ogre::LogManager::getSingletonPtr()->logMessage("FUSION");
	//Ogre::LogManager::getSingletonPtr()->logMessage("x = " + std::to_string(x));
	//Ogre::LogManager::getSingletonPtr()->logMessage("y = " + std::to_string(y));
	//Ogre::LogManager::getSingletonPtr()->logMessage("z = " + std::to_string(z));
	size_t target = y / COLOUR_TEXTURE_DEPTH;
	target = (cTable.size() - target - 1);
	size_t coordY = y % COLOUR_TEXTURE_DEPTH;
	Ogre::uint32 cellMask = ~(1 << coordY);
	Ogre::uint32 columnValue = getColourAt(target, x, z);
	setColourAt(target, cellMask & columnValue, x, z);
}

/****************************************************************/
/* CONSULTORES */
/****************************************************************/
bool SliceMap::isOutOfBounds(int x, int y, int z) {
	int realX, realY, realZ;
	if (orientation == ORIENTATION_TOP) {
		realX = x; realY = y; realZ = z;
	}
	else if (orientation == ORIENTATION_FRONT) {
		realX = x; realY = z; realZ = y;
	}
	else {
		realX = y; realY = x; realZ = z;
	}
	return (
		(x < 0) || (x >= (int)width) ||
		(y < 0) || (y >= (int)depth) ||
		(z < 0) || (z >= (int)height)
	);
}

bool SliceMap::isSolidVoxel(int x, int y, int z) {
	//if (isOutOfBounds(x, y, z)) return false;	//Coords out of bounds

	bool solidVoxel = false;
	switch (orientation) {
		case SliceMap::ORIENTATION_TOP:
			solidVoxel = isSolidVoxelTop(x, y, z);
			break;

		case SliceMap::ORIENTATION_FRONT:
			solidVoxel = isSolidVoxelFront(x, y, z);
			break;

		case SliceMap::ORIENTATION_SIDE:
			solidVoxel = isSolidVoxelSide(x, y, z);
			break;
	}
	return solidVoxel;
}

bool SliceMap::isSolidVoxelTop(size_t x, size_t y, size_t z) {
	//Indica si el v�xel (x, y, z) �s s�lid en el cas d'un SliceMap amb orientaci� TOP. 
	//En aquest cas, la profunditat la indica la coordenada y del v�xel
	size_t target = y / COLOUR_TEXTURE_DEPTH;	//La target on cau el v�xel
	target = (cTable.size() - target - 1);
	size_t coordY = y % COLOUR_TEXTURE_DEPTH;	//La depth del v�xel dins el target
	Ogre::uint32 cellMask = 1 << coordY;
	Ogre::uint32 column = getColourAt(target, x, z);	//Cada p�xel de la imatge representa una columna del slicemap
	bool solidVoxel = ((cellMask & column) > 0);
	return solidVoxel;
}
	
bool SliceMap::isSolidVoxelFront(size_t x, size_t y, size_t z) {
	//Indica si el v�xel (x, y, z) �s s�lid en el cas d'un SliceMap amb orientaci� FRONT. 
	//En aquest cas, la profunditat la indica la coordenada z del v�xel
	size_t target = z / COLOUR_TEXTURE_DEPTH;	//La target on cau el v�xel
	target = (cTable.size() - target - 1);
	size_t coordZ = z % COLOUR_TEXTURE_DEPTH;	//La depth del v�xel dins el target 
	Ogre::uint32 cellMask = 1 << coordZ;
	Ogre::uint32 column = getColourAt(target, x, height - 1 - y);	//Cada p�xel de la imatge representa una fila del slicemap
	bool solidVoxel = ((cellMask & column) > 0);
	return solidVoxel;
}

bool SliceMap::isSolidVoxelSide(size_t x, size_t y, size_t z) {
	//Indica si el v�xel (x, y, z) �s s�lid en el cas d'un SliceMap amb orientaci� SIDE.
	//EN aquest cas, la profunditat la indica la coordenada x del v�xel
	size_t target = x / COLOUR_TEXTURE_DEPTH;	//La target on cau el v�xel
	target = (cTable.size() - target - 1);
	size_t coordX = x % COLOUR_TEXTURE_DEPTH;	//La depth del v�xel dins el target
	Ogre::uint32 cellMask = 1 << coordX;
	Ogre::uint32 column = getColourAt(target, width - 1 - z, height - 1 - y);	//Cada p�xel de la imatge representa una columna del slicemap
	bool solidVoxel = ((cellMask & column) > 0);
	return solidVoxel;
}