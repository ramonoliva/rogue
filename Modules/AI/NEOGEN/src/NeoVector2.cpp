#include "NeoVector2.h"
#include "NeoVector3.h"

using namespace NEOGEN;

Vector2 NEOGEN::operator*(NeoReal d, const Vector2& v) {
	return Vector2(v._x * d, v._y * d);
}

Vector2 NEOGEN::operator*(const Vector2& v, NeoReal d) {
	return d * v;
}

Vector2 NEOGEN::operator-(const Vector2& v) {
	return -1.0 * v;
}

Vector3 Vector2::to3D(Math::ProjectionPlane projectionPlane) const {
	if (projectionPlane == Math::ProjectionPlane::XZ) return Vector3(_x, 0, _y);
	if (projectionPlane == Math::ProjectionPlane::XY) return Vector3(_x, _y, 0);
	return Vector3(0, _x, _y);
};