#include "NavMeshGenerator.h"

/**********************************************/
		/* GPU MODE */
		/**********************************************/
		Ogre::RenderTexture* notchRender;
		Ogre::TexturePtr notchTexture;	//Textura que guarda el render des del punt de vista del notch
		Ogre::Image notchView;			//Imatge que guarda el render des del punt de vista del notch
		bool resetCamera;
		float zFarScene;				//zFar mitj� de l'escena
		float zFarCurrent;				//el zFar actual que utilitzem
		int zFarSamples;				//nombre de mostres de zFar que hem pres
		int zFarMiss;					//Indica el nombre de vegades que ha fallat el zFar adaptatiu
		int portalCheck;				//Indica quantes vegades hem hagut de comprobar si es pot tra�ar un portal amb un altre portal


void NavMeshGenerator::generateStepByStepGPU() {
	timeBefore = timer->getMilliseconds();
	
	showNotchEdges(false);								//Oculto les arestes que s�n incidents al notch
	cameraToNotch();									//Coloco la c�mera en el punt de vista del notch

	ClosestElement ce = computeClosestElementGPU();		//Busco l'element m�s proper des del punt de vista del notch

	nmVert& notch = navMesh->getVertex(*notchIterator);	//El notch que hem de dividir
	createPortal(&notch, &ce);							//Creo un portal entre el notch i l'element m�s proper trobat
	showNotchEdges(true);								//Torno a mostrar les arestes incidents al v�rtex

	//Actualitzo el temps transcorregut
	timeAfter = timer->getMilliseconds();
	unsigned long diffTime = timeAfter - timeBefore;
	elapsedTimePortalCreation += diffTime;

	notchIterator++;
	splittedVertexs ++;
}

void NavMeshGenerator::cameraToNotch() {
	//Posa la c�mera en el punt de vista actual
	
	float zNear = 0.1f;
	//1) Orientar la c�mera en X i Z
	Ogre::Quaternion q1(SQRT_HALF, SQRT_HALF, 0.0f, 0.0f);	//Roto 90� sobre l'eix x
	Ogre::Quaternion q2(SQRT_HALF, 0.0f, 0.0f, -SQRT_HALF);	//Roto -90� sobre l'eix z
	Ogre::Quaternion orientation = q1 * q2;
	mCamera->setOrientation(orientation);

	//2) Calcular el nou forward de la c�mera i rotar en Y
	Vec2D vRight = vAOIRight;
	Vec2D vLeft = vAOILeft;
	//vLeft *= -1;
	Vec2D initCamDir = vRight + vLeft;
	Ogre::Vector3 initCamForward(initCamDir.getX(), initCamDir.getY(), 0.0f);
	Ogre::Vector3 forward = Ogre::Vector3::UNIT_Y;	//Direcci� on est� mirant la c�mera actualment
	//Quaternion que representa la rotaci� a aplicar perqu� la c�mera miri cap a camForward
	Ogre::Quaternion q3 = forward.getRotationTo(initCamForward);
	mCamera->rotate(q3);
	
	//3) Posiciono la c�mera
	Vert* notch = navMesh->getVertex(*notchIterator).v;
	//La tiro una mica enrere per evitar l'efecte del zNear
	Ogre::Vector3 camBackward = -1 * (mCamera->getDirection());	
	Ogre::Vector3 camPos(notch->getX(), notch->getY(), 0.0f);
	camPos = camPos + camBackward * zNear;
	mCamera->setPosition(camPos);

	//4) Calculo el fov de la c�mera tenint en compte l'angle de l'�rea d'inter�s
	Ogre::Vector3 vE1(vRight.getX(), vRight.getY(), 0.0f);
	Ogre::Vector3 vE2(vLeft.getX(), vLeft.getY(), 0.0f);
	Ogre::Degree aBetween = vE1.angleBetween(vE2);
	mCamera->setFOVy(aBetween);

	//5) Assigno el zNear, el zFar i la relaci� d'aspecte
	mCamera->setNearClipDistance(zNear);
	mCamera->setFarClipDistance(Ogre::Math::Ceil(zFarCurrent));
	mCamera->setAspectRatio((float)(notchRender->getWidth()) / (float)(notchRender->getHeight()));
}

void NavMeshGenerator::portalVertPortalEndPointGPU(nmVert* vi, int portalID, int endPointID, std::set<int>& visibleEdges) {
	//Crea un portal entre el v�rtex vi i un punt que �s un extrem d'un portal

	//Comprovo si �s segur crear un portal entre el notch i l'extrem final de l'aresta
	Vec2D portalEnd = GeometrySolver::to2D(*(navMesh->getVertex(endPointID).v), NEOGEN_PROJECTION_PLANE);
	Vec2D v2D = GeometrySolver::to2D(*(vi->v), NEOGEN_PROJECTION_PLANE);
	float d2portalEnd = portalEnd.dist2(v2D);	//dist�ncia^2 entre el notch i l'extrem del portal

	ClosestElement cIntersection = computeClosestIntersectionGPU(v2D, portalEnd, visibleEdges);
	bool isSafe =	(
					(cIntersection.id == -1) || 
					(cIntersection.dist2 > d2portalEnd) ||
					((cIntersection.elementType == ELEMENT_TYPE_VERTEX) && (cIntersection.id == endPointID))
					);
	if (isSafe) {
		//Ogre::LogManager::getSingletonPtr()->logMessage("END POINT IS SAFE (GPU)");
		cIntersection.elementType = ELEMENT_TYPE_VERTEX;
		cIntersection.id = endPointID;
	}
	else {
		//Ogre::LogManager::getSingletonPtr()->logMessage("END POINT IS NOT SAFE (GPU)");
	}
	createPortal(vi, &cIntersection);
}

NavMeshGenerator::ClosestElement NavMeshGenerator::computeClosestIntersectionGPU(Vec2D& initPoint, Vec2D& endPoint, std::set<int>& visibleEdges) {
	//Calcula quina �s la intersecci� m�s propera entre la recta definida pel vector
	//vDir i un punt de la recta p, i la resta d'arestes

	nmEdge edge;
	Vec2D beginEdge2D;	//punt inicial de l'aresta
	Vec2D endEdge2D;	//punt final de l'aresta

	ClosestElement closestIntersection;
	closestIntersection.id = -1;
	closestIntersection.elementType = ELEMENT_TYPE_EDGE;
	closestIntersection.dist2 = -1.0f;
	SegmentIntersection2D intersection;	//Punt d'intersecci� entre l'aresta i i la recta
	float d2aux;
	
	//Recorro totes les arestes visibles des del notch
	for (std::set<int>::iterator it = visibleEdges.begin(); it != visibleEdges.end(); it++) {
		edge = navMesh->getEdge(*it);						//L'aresta
		beginEdge2D = GeometrySolver::to2D(*(navMesh->getVertex(edge.v1).v), NEOGEN_PROJECTION_PLANE);	//L'extrem inicial de l'aresta
		endEdge2D = GeometrySolver::to2D(*(navMesh->getVertex(edge.v2).v), NEOGEN_PROJECTION_PLANE);		//L'extrem final de l'aresta

		GeometrySolver::segmentSegmentIntersection2D(initPoint, endPoint, beginEdge2D, endEdge2D, intersection);
		if (intersection.p1Valid) {
			//Hi ha intersecci� entre la recta (vDir, p) i l'aresta
			//Cal comprobar si �s una intersecci� m�s propera
			d2aux = intersection.p1.dist2(initPoint);
			if ((closestIntersection.dist2 < 0.0f) || (closestIntersection.dist2 > d2aux)) {
				//La intersecci� calculada �s m�s propera
				closestIntersection.id = edge.id;
				closestIntersection.candidate = intersection.p1;
				closestIntersection.dist2 = d2aux;
			}
		}
	}
	//Si la intersecci� est� molt a prop d'un dels dos extrems de l'aresta, 
	//agafo aquest extrem en comptes del punt
	if ((closestIntersection.id != -1) && (closestIntersection.elementType == ELEMENT_TYPE_EDGE)) {
		int v1ID = navMesh->getEdge(closestIntersection.id).v1;	//Extrem inicial de l'aresta m�s propera
		int v2ID = navMesh->getEdge(closestIntersection.id).v2;	//Extrem final de l'aresta m�s propera
		Vec2D closestEdgeV1 = GeometrySolver::to2D(*(navMesh->getVertex(v1ID).v), NEOGEN_PROJECTION_PLANE);					//V�rtex inicial de l'aresta m�s propera
		Vec2D closestEdgeV2 = GeometrySolver::to2D(*(navMesh->getVertex(v2ID).v), NEOGEN_PROJECTION_PLANE);					//V�rtex final de l'aresta m�s propera
		float d2V1 = closestIntersection.candidate.dist2(closestEdgeV1);	//d^2 entre v1 i el candidat
		float d2V2 = closestIntersection.candidate.dist2(closestEdgeV2);	//d^2 entre v2 i el candidat

		if (RealMath::equal(d2V1, 0.0f, 0.01f)) {
			//El candidat est� molt a prop de V1
			closestIntersection.elementType = ELEMENT_TYPE_VERTEX;
			closestIntersection.id = v1ID;
		}

		if (RealMath::equal(d2V2, 0.0f, 0.01f)) {
			//El candidat est� molt a prop de V2
			closestIntersection.elementType = ELEMENT_TYPE_VERTEX;
			closestIntersection.id = v2ID;
		}
	}
	return closestIntersection;
}

void NavMeshGenerator::portalVertPortalGPU(nmVert* vi, nmEdge* e) {
	//Crea un portal entre un v�rtex i un portal

	/*Ogre::LogManager::getSingletonPtr()->logMessage("**************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("CREATING PORTAL VERT-PORTAL (GPU)");
	Ogre::LogManager::getSingletonPtr()->logMessage("**************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("vi->id = " + Ogre::StringConverter::toString(vi->id));
	Ogre::LogManager::getSingletonPtr()->logMessage("e->id = " + Ogre::StringConverter::toString(e->id));*/
	portalCheck++;
	
	//L'aresta m�s propera �s un portal. Cal "partir-lo". Comprobo 
	//quants portals haig de generar
	nmEdge& e1 = navMesh->getEdge(vi->concEdge1);	
	nmEdge& e2 = navMesh->getEdge(vi->concEdge2);	
	Vec2D notch = GeometrySolver::to2D(*(vi->v), NEOGEN_PROJECTION_PLANE);
	Vec2D portalBegin = GeometrySolver::to2D(*(navMesh->getVertex(e->v1).v), NEOGEN_PROJECTION_PLANE);
	Vec2D portalEnd = GeometrySolver::to2D(*(navMesh->getVertex(e->v2).v), NEOGEN_PROJECTION_PLANE);

	bool testBegin = inZone(portalBegin, vi->id, vAOIRight, vAOILeft);	//Punt inicial del portal en la zona d'inter�s del notch?
	bool testEnd = inZone(portalEnd, vi->id, vAOIRight, vAOILeft);		//Punt final del portal en la zona d'inter�s del notch?

	//1) Calculo el nou zFar: Utilitzo com a zFar la dist�ncia m�xima entre el
	//notch i els extrems del portal
	float dist2Begin = notch.dist2(portalBegin);	//Dist�ncia entre el notch i l'extrem inicial del portal
	float dist2End = notch.dist2(portalEnd);		//Dist�ncia entre el notch i l'extrem final del portal
	float oldZFar = zFarCurrent;
	zFarCurrent = std::sqrtf(std::max(dist2Begin, dist2End));

	//2) Calculo els nous vectors que delimiten la AOI
	//Busco quin �s el nou vector right. Ser� aquell que deixi a l'ESQUERRA l'altre extrem del portal. 
	//Per eliminaci�, el nou vector left ser� el format pel notch i l'altre extrem del portal. 
	int orientationBegin = GeometrySolver::orientationTest(notch, portalBegin, portalEnd);
	if (orientationBegin >= 0) {
		//L'extrem begin �s el que forma el nou vector RIGHT
		vAOIRight = portalBegin - notch;
		vAOILeft = notch - portalEnd;
	}
	else {
		//L'extrem end �s el que forma el nou vector RIGHT
		vAOIRight = portalEnd - notch;
		vAOILeft = notch - portalBegin;
	}
	vAOIRight.normalize();
	vAOILeft.normalize();
	
	//3) reposiciono la c�mera i faig un render extra
	cameraToNotch();
	//mRoot->renderOneFrame();
	notchRender->update();
	zFarCurrent = oldZFar;
	notchTexture->convertToImage(notchView);
	//notchView.save("notchView" + Ogre::StringConverter::toString(*notchIterator) + "Portal" + Ogre::StringConverter::toString(e->id) + ".png");

	//4) Busco quines s�n les arestes visibles des del notch
	std::set<int> visibleEdges;
	Ogre::ColourValue pixelColour;
	size_t h = notchView.getHeight();
	int edgeID = -1;
	for (size_t y = 0; y < h; y++) {
		pixelColour = notchView.getColourAt(0, y, 0);
		edgeID = navMesh->getEdgeID(Ogre::ColourValue(pixelColour.r, pixelColour.g, pixelColour.b));
		if ((edgeID == -1) || (edgeID == e->id)) continue;	//He detectat un p�xel del fons o un p�xel del propi portal
		visibleEdges.insert(edgeID);
	}
	
	if (!testBegin && !testEnd) {
		//Els dos extrems del portal cauen FORA de la zona d'inter�s
		//del v�rtex que volem dividir. Per tant, ELS DOS portals 
		//s�n imprescindibles
		
		//Ogre::LogManager::getSingletonPtr()->logMessage("INITIAL AND FINAL POINTS ARE OUTSIDE THE AREA OF INTEREST");
		//Comprovo si �s segur crear un portal entre el notch i l'extrem INICIAL del portal
		portalVertPortalEndPointGPU(vi, e->id, e->v1, visibleEdges);

		//Comprovo si �s segur crear un portal entre el notch i l'extrem FINAL del portal
		portalVertPortalEndPointGPU(vi, e->id, e->v2, visibleEdges);
	}
	else if (testBegin) {
		//L'extrem inicial del portal cau DINS de la zona d'inter�s
		//del v�rtex que volem dividir. Per tant, nom�s �s imprescindible
		//el portal format entre el v�rtex que volem dividir i l'extrem
		//inicial del portal amb el que hem interseccionat. 

		//Ogre::LogManager::getSingletonPtr()->logMessage("INITIAL POINT IS INSIDE THE AREA OF INTEREST");
		//Comprovo si �s segur crear un portal entre el notch i l'extrem INICIAL del portal
		portalVertPortalEndPointGPU(vi, e->id, e->v1, visibleEdges);
	}
	else if (testEnd) {
		//L'extrem final del portal cau DINS de la zona d'inter�s
		//del v�rtex que volem dividir. Per tant, nom�s �s imprescindible
		//el portal format entre el v�rtex que volem dividir i l'extrem
		//final del portal amb el que hem interseccionat. 
		
		//Ogre::LogManager::getSingletonPtr()->logMessage("FINAL POINT IS INSIDE THE AREA OF INTEREST");
		//Comprovo si �s segur crear un portal entre el notch i l'extrem FINAL del portal
		portalVertPortalEndPointGPU(vi, e->id, e->v2, visibleEdges);
	}
	if (navMesh->isNonEssentialPortal(e->id)) {
		erasePortalPointer(e->id);
	}
	//Ogre::LogManager::getSingletonPtr()->logMessage("PORTAL VERT-PORTAL CREATED!");
}

NavMeshGenerator::ClosestElement NavMeshGenerator::computeClosestElementGPU() {
	ClosestElement ce;
	ce.id = -1;
	while (ce.id == -1) {							//Segueixo mentre no trobi cap element (degut al zFar utilitzat)
		//mRoot->renderOneFrame();					//Renderitzo l'escena des del punt de vista del notch
		notchRender->update();
		ce = computeClosestElementNotch();			//Busco l'element m�s proper des del punt de vista del notch
	}
	return ce;
}

NavMeshGenerator::ClosestElement NavMeshGenerator::computeClosestElementNotch() {
	//Calcula quin �s l'element m�s proper utilitzant el render des del punt de vista del notch
	//Ogre::LogManager::getSingletonPtr()->logMessage("COMPUTING CLOSEST ELEMENT TO NOTCH!");
	notchTexture->convertToImage(notchView);	//Transformo la textura a imatge, per poder-la tractar
	//notchView.save("notchView" + Ogre::StringConverter::toString(*notchIterator) + ".png");
	//Ogre::Image testImage = notchView;
	//Ogre::ColourValue testColour;
	//for (size_t y = 0; y < testImage.getHeight(); y++) {
	//	testColour = testImage.getColourAt(0, y, 0);
	//	testImage.setColourAt(testColour, 0, y, 0);
	//}
	//testImage.save("notchView" + Ogre::StringConverter::toString(*notchIterator) + ".png");
	//La imatge resultant t� dimensions 1 x h. 
	//Recorro la imatge i em quedo amb el fragment (p�xel) m�s proper. 
	//Aquest fragment ha de pert�nyer a l'element m�s proper al v�rtex
	
	std::set<int> visibleEdges;

	Ogre::ColourValue pixelColour;
	size_t h = notchView.getHeight();
	int edgeID = -1;

	for (size_t y = 0; y < h; y++) {
		pixelColour = notchView.getColourAt(0, y, 0);
		edgeID = navMesh->getEdgeID(Ogre::ColourValue(pixelColour.r, pixelColour.g, pixelColour.b));
		if (edgeID == -1) continue;	//He detectat un p�xel del fons
		visibleEdges.insert(edgeID);
	}

	ClosestElement ceResult;		//Aresta actualment m�s propera a v
	ceResult.id = -1;
	//Busco l'aresta m�s propera a v dins la zona d'inter�s
	ceResult.dist2 = -1.0f;			//Dist�ncia actual de eClosest a v
	ClosestElement bestCandidate;	

	nmVert& notch = navMesh->getVertex(*notchIterator);
	//Recorro les arestes visibles des del notch i em quedo amb la que t� el millor candidat
	for (std::set<int>::iterator it = visibleEdges.begin(); it != visibleEdges.end(); it++) {
		//Ogre::LogManager::getSingletonPtr()->logMessage("visible edge = " + Ogre::StringConverter::toString(*it));
		bestCandidate = computeBestCandidateEdge(notch, navMesh->getEdge(*it));	//Calculo el millor candidat de l'aresta actual
		//Si el millor candidat est� m�s a prop que el candidat actual, substitueixo
		if ((ceResult.dist2 < 0.0f) || (ceResult.dist2 > bestCandidate.dist2)) {
			if (bestCandidate.id != -1) ceResult = bestCandidate;
		}
	}
	if (ceResult.id != -1) {
		//Actualitzo el zFar amb la dist�ncia al millor candidat
		float zFarVert = GeometrySolver::to2D(*(notch.v), NEOGEN_PROJECTION_PLANE).dist(ceResult.candidate);
		updateZFarScene(zFarVert);
		//if (ceResult.id == -1) Ogre::LogManager::getSingletonPtr()->logMessage("INVALID CANDIDATE!");
	}
	else {
		//No he trobat cap element amb el zFar actual
		//Cap element era visible des del notch amb l'actual zFar. 
		//Cal incrementar el zFar i fer un nou render
		//Ogre::LogManager::getSingletonPtr()->logMessage("NO ELEMENT FOUND WITH CURRENT ZFAR!");
		zFarMiss ++;
		mCamera->setNearClipDistance(mCamera->getFarClipDistance());
		zFarCurrent *= 2.0f;
		mCamera->setFarClipDistance(Ogre::Math::Ceil(zFarCurrent));	//Modifico el zFar de la c�mera
	}
	return ceResult;
}

void NavMeshGenerator::updateZFarScene(float zFarVert) {
	//Actualitza el valor del zFar de l'escena
	//zFarVert = Ogre::Math::Ceil(zFarVert);
	//zFarVert += 0.5f;
	
	zFarScene = zFarScene * zFarSamples;
	zFarSamples ++;
	zFarScene += zFarVert;
	zFarScene /= zFarSamples;

	//Actualitzo el zFar actual
	zFarCurrent = zFarScene;
}