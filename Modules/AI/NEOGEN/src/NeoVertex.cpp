#include "NeoVertex.h"
#include "NeoFace.h"

using namespace NEOGEN;

#define MIN_ANGLE_NOTCH NeoReal(185.0)

#pragma region CREATION AND DESTRUCTION

NeoVertex::NeoVertex(ElementID id, const Vector3& position) : NeoMeshElement(id) {
	setPosition(position);
}

NeoVertex::~NeoVertex() {
		
}

#pragma endregion

#pragma region GET AND SET

const Vector3& NeoVertex::getPosition() const {
	return _position;
}

void NeoVertex::setPosition(const Vector3& position) {
	_position = position;
}

Vector3 NeoVertex::getNormal() {
	Vector3 normal = Vector3(0,0,0);
	NeoFacePSet incidentFaces;
	getIncidentFaces(incidentFaces);
	for (NeoFacePSet::iterator it = incidentFaces.begin(); it != incidentFaces.end(); it++) {
		NeoFace* face = *it;
		normal += face->getNormal();
	}
	normal.normalize();
	return normal;
}

void NeoVertex::addIncidentEdge(NeoEdge* edge) {
	if (edge) _incidentEdges.insert(edge);
}

void NeoVertex::removeIncidentEdge(NeoEdge* edge) {
	_incidentEdges.erase(edge);
}

bool NeoVertex::isIncidentEdge(NeoEdge* edge) {
	return (_incidentEdges.find(edge) != _incidentEdges.end());
}

NeoEdgePSet& NeoVertex::getIncidentEdges() {
	return _incidentEdges;
}

void NeoVertex::getIncidentEdgesIn(NeoEdgePSet& result) {
	//Returns the set of edges such that its FINAL endpoint is this vertex
	for (NeoEdgePSet::iterator it = _incidentEdges.begin(); it != _incidentEdges.end(); it++) {
		NeoEdge* edge = *it;
		if (edge->getVertexEnd() == this) result.insert(edge);
	}
}

void NeoVertex::getIncidentEdgesOut(NeoEdgePSet& result) {
	//Returns the set of edges such that its INITIAL endpoint is this vertex
	for (NeoEdgePSet::iterator it = _incidentEdges.begin(); it != _incidentEdges.end(); it++) {
		NeoEdge* edge = *it;
		if (edge->getVertexBegin() == this) result.insert(edge);
	}
}

NeoEdge* NeoVertex::getIncidentEdgeObstacle(const Vector3& edgeDir) {
	for (NeoEdgePSet::iterator it = _incidentEdges.begin(); it != _incidentEdges.end(); it++) {
		NeoEdge* e = *it;
		if (e->isTypeObstacle() && (Math::equal(e->getDirection().angle(edgeDir), NeoReal(0.0)))) return e;
	}
	return NULL;
}

NeoEdge* NeoVertex::getEdgeObstacle(NeoEdgePSet& edges) {
	NeoEdge* result = NULL;
	for (NeoEdgePSet::iterator it = edges.begin(); it != edges.end(); it++) {
		NeoEdge* tmp = *it;
		if (tmp->isTypeObstacle()) {
			if (!result) {
				result = tmp;
			}
		}
	}

	return result;
}

size_t NeoVertex::getNumIncidentEdges() {
	return _incidentEdges.size();
}

bool NeoVertex::isIncidentFace(NeoFace* face) {
	NeoFacePSet incidentFaces;
	getIncidentFaces(incidentFaces);
	return (incidentFaces.find(face) != incidentFaces.end());
}

void NeoVertex::getIncidentFaces(NeoFacePSet& result) {
	for (NeoEdgePSet::iterator it = _incidentEdges.begin(); it != _incidentEdges.end(); it++) {
		NeoEdge* edge = *it;
		if (edge->getFace()) result.insert(edge->getFace());
	}
}

bool NeoVertex::isObstacle() {
	bool isObstacle = false;
	NeoEdgePSet::iterator it = _incidentEdges.begin();
	while (!isObstacle && (it != _incidentEdges.end())) {
		isObstacle = (*it)->isTypeObstacle();
		it++;
	}
	
	return isObstacle;
}

bool NeoVertex::isNotch(NeoEdge* edgeIn) {
	if (!edgeIn) return false;

	NeoEdge* current = edgeIn;
	NeoEdge* next = NULL;
	
	do {
		next = current->getFace()->getNextEdge(current);
		current = next->getOpposite();
	} while (!next->isTypeObstacle() && !next->isTypeSubPortal());

	Vector2 u = edgeIn->getDirectionInverse().to2D().normalized();
	Vector2 v = next->getDirection().to2D().normalized();
	NeoReal d = v.det(u);
	return Math::greaterOrEqual(d, Math::sin(Math::toRadians(5))) || u.equal(v);
}

bool NeoVertex::isNotch(const Vector3& edgeInDir) {
	return isNotch(getIncidentEdgeObstacle(edgeInDir));
}

#pragma endregion