#include "NeoCell.h"
#include "NeoFace.h"
#include "NeoPortal.h"

#include "NeoGeometrySolver.h"

using namespace NEOGEN;

#pragma region CREATION AND DESTRUCTION

NeoCell::NeoCell(ElementID id) : NeoMeshElement(id) {
	
}

#pragma endregion

#pragma region GET AND SET

void NeoCell::begin() {
	_faces.clear();
	_portals.clear();
	_vertices.clear();
}

void NeoCell::addFace(NeoFace* face) {
	if (!hasFace(face)) {
		_faces.insert(face);

		//If the face has a portal, at it to the portals list
		NeoEdgePVector& edges = face->getEdges();
		for (NeoEdgePVector::iterator it = edges.begin(); it != edges.end(); it++) {
			NeoEdge* e = *it;
			if (e->isTypeSubPortal()) _portals.insert((NeoPortal*)(e->getParent()));
		}

		face->setCell(this);
	}
}

void NeoCell::removeFace(NeoFace* face) {
	face->setCell(NULL);
	_faces.erase(face);
}

void NeoCell::end() {
	computeContour();
	triangulate();
}

bool NeoCell::hasFace(NeoFace* face) {
	return (_faces.find(face) != _faces.end());
}

NeoFacePSet& NeoCell::getFaces() {
	return _faces;
}

void NeoCell::computeContour() {
	//1) Find a start edge of the contour, i.e., a portal or an obstacle edge of any of the contained faces in the cell. 
	NeoEdge* start = NULL;
	if (!_portals.empty()) start = *_portals.begin();
	else {
		for (NeoFacePSet::iterator itFace = _faces.begin(); itFace != _faces.end(); itFace++) {
			const NeoEdgePVector& edges = (*itFace)->getEdges();
			for (NeoEdgePVector::const_iterator itEdges = edges.begin(); itEdges != edges.end(); itEdges++) {
				NeoEdge* e = *itEdges;
				if (e->isTypeObstacle()) {
					start = e;
					break;
				}
			}
			if (start) break;
		}
	}
	if (!start) return;
	
	//2) Walk through the contour until we reach the starting portal. 
	NeoEdge* e = start;
	do {
		_vertices.push_back(e->getVertexBegin());
		e = getNextEdge(e);
	} while (e && (e != start));
}

void NeoCell::getContour(Polygon3& result) {
	for (NeoVertexPVector::iterator it = _vertices.begin(); it != _vertices.end(); it++) {
		result.push_back((*it)->getPosition());
	}
}

void NeoCell::getContour2D(Polygon2& result) {
	Polygon3 contour3;
	getContour(contour3);
	
	for (Polygon3::iterator it = contour3.begin(); it != contour3.end(); it++) {
		result.push_back((*it).to2D());
	}
}

void NeoCell::getNeighbors(NeoCellPSet& result) {
	for (NeoPortalPSet::iterator it = _portals.begin(); it != _portals.end(); it++) {
		NeoCell* cell = (*it)->getOpposite()->getCell();
		if (cell) result.insert(cell);
	}
}

NeoEdge* NeoCell::getNextEdge(NeoEdge* edge) {
	NeoEdgePSet edgesOut;
	edge->getVertexEnd()->getIncidentEdgesOut(edgesOut);
	for (NeoEdgePSet::iterator it = edgesOut.begin(); it != edgesOut.end(); it++) {
		NeoEdge* e = *it;
		if (hasFace(e->getFace())) {
			if (e->isTypeObstacle()) return e;
			if (e->isTypeSubPortal()) return e->getParent();
		}
	}

	return NULL;
}

void NeoCell::triangulate() {
	//Create the contour
	Polygon2 contour;
	getContour2D(contour);
		
	//Construct the triangles list
	NeoTriangulator::IndexList indices;
	NeoTriangulator::Process(contour, indices);
	_triangles.clear();
	for (size_t i = 0; i < indices.size(); i += 3) {
		Triangle t;
		t.push_back(_vertices[indices[i]]);
		t.push_back(_vertices[indices[i + 1]]);
		t.push_back(_vertices[indices[i + 2]]);

		if (!NeoGeometrySolver::getPolygonOrientation(t, Vector3(0,1,0))) std::reverse(t.begin(), t.end());

		_triangles.push_back(t);
	}
}

const Triangles& NeoCell::getTriangles() {
	return _triangles;
}

#pragma endregion