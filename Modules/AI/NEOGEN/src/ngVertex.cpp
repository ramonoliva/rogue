#include "ngVertex.h"
#include "ngDataStructures.h"

using namespace NEOGEN;

#pragma region CREATION AND DESTRUCTION

Vertex::Vertex(Vec3D& pos) {
	_id = -1;
	_position = pos;
	_shared = false;
	_concEdge1 = NULL;
	_concEdge2 = NULL;
	_isNotch = false;
}

Vertex::~Vertex() {
	_edges.clear();
}

#pragma endregion

#pragma region GET AND SET

int Vertex::getID() {
	return _id;
}

void Vertex::setID(int id) {
	_id = id;
}

Vec3D Vertex::getPosition() {
	return _position;
}

Vec2D Vertex::getPosition2D(RealMath::ProjectionPlaneEnum projectionPlane) {
	if (projectionPlane == RealMath::PROJECTION_XY) return Vec2D(_position.getX(), _position.getY());
	if (projectionPlane == RealMath::PROJECTION_XZ) return Vec2D(_position.getX(), _position.getZ());
	return Vec2D(_position.getY(), _position.getZ());
}

void Vertex::setPosition(Vec3D& pos) {
	_position = pos;
}

int Vertex::getConcEdge1() {
	return _concEdge1;
}

int Vertex::getConcEdge2() {
	return _concEdge2;
}

void Vertex::setConcEdge1(int edge) {
	_concEdge1 = edge;
}

void Vertex::setConcEdge2(int edge) {
	_concEdge2 = edge;
}

IncidentEdgesList& Vertex::getIncidentEdges() {
	return _edges;
}

int Vertex::getIncidentEdge(size_t pos) {
	if (pos >= getNumIncidentEdges()) return NULL;
	return _edges.at(pos);
}

size_t Vertex::getNumIncidentEdges() {
	return _edges.size();
}

void Vertex::addIncidentEdgeBack(int edge) {
	_edges.push_back(edge);
}

void Vertex::addIncidentEdgeFront(int edge) {
	_edges.push_front(edge);
}

void Vertex::addIncidentEdgeAt(int edge, IncidentEdgesList::iterator& it) {
	_edges.insert(it, edge);
}

void Vertex::removeIncidentEdge(int edge) {
	IncidentEdgesList::iterator it = std::find(_edges.begin(), _edges.end(), edge);
	if (it != _edges.end()) _edges.erase(it);
}

bool Vertex::hasIncidentEdge(int edge) {
	return (std::find(_edges.begin(), _edges.end(), edge) != _edges.end());
}

bool Vertex::isShared() {
	return _shared;
}

bool Vertex::isNotch() {
	return ((_concEdge1 != NULL) || (_concEdge2 != NULL));
}

void Vertex::setShared(bool b) {
	_shared = b;
}

#pragma endregion

#pragma region DEBUG

void Vertex::print() {
	Ogre::LogManager::getSingletonPtr()->logMessage("VERTEX INFORMATION");
	Ogre::LogManager::getSingletonPtr()->logMessage("id = " + std::to_string(_id));
	Ogre::LogManager::getSingletonPtr()->logMessage("x = " + std::to_string(_position.getX()));
	Ogre::LogManager::getSingletonPtr()->logMessage("y = " + std::to_string(_position.getY()));
	Ogre::LogManager::getSingletonPtr()->logMessage("z = " + std::to_string(_position.getZ()));
}

#pragma endregion