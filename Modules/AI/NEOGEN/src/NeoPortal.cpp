#include "NeoPortal.h"
#include "NeoFace.h"
#include "NeoCell.h"

using namespace NEOGEN;

#pragma region CREATION AND DESTRUCTION

NeoPortal::NeoPortal(ElementID id) : NeoEdge(id, NULL, NULL) {
	
}

#pragma endregion

#pragma region GET AND SET

void NeoPortal::addSubPortalBack(NeoEdge* sPortal) {
	if (!sPortal) return;

	if (_subPortals.empty()) _v1 = sPortal->getVertexBegin();
	
	sPortal->setParent(this);
	_subPortals.push_back(sPortal);
	
	_v2 = sPortal->getVertexEnd();
}

void NeoPortal::addSubPortalFront(NeoEdge* sPortal) {
	if (!sPortal) return;

	if (_subPortals.empty()) _v2 = sPortal->getVertexEnd();

	sPortal->setParent(this);
	_subPortals.push_front(sPortal);

	_v1 = sPortal->getVertexBegin();
}

const NeoSubPortals& NeoPortal::getSubPortals() {
	return _subPortals;
}

Vector3 NeoPortal::getDirection() {
	return _subPortals.empty() ? Vector3(0, 0, 0) : _subPortals[0]->getDirection();
}

NeoPortal* NeoPortal::getOpposite() {
	return dynamic_cast<NeoPortal*>(_opposite);
}

void NeoPortal::setOpposite(NeoPortal* opposite) {
	_opposite = opposite;
}

NeoCell* NeoPortal::getCell() {
	if (_subPortals.empty()) return NULL;
	
	return (*_subPortals.begin())->getFace()->getCell();	
}

#pragma endregion