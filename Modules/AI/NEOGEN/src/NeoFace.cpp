#include "NeoFace.h"
#include "NeoCell.h"
#include "NeoGeometrySolver.h"

#include <OgreLogManager.h>
#include <OgreStringConverter.h>

using namespace NEOGEN;

#pragma region CREATION AND DESTRUCTION

NeoFace::NeoFace(ElementID id) : NeoMeshElement(id) {
	_cell = NULL;
	_type = Type::UNDEFINED;
}

NeoFace::~NeoFace() {
	//Break the link between the edges conforming the face and the face itself. 
	for (NeoEdgePVector::iterator it = _edges.begin(); it != _edges.end(); it++) (*it)->removeIncidentFace(this);
}

#pragma endregion

#pragma region GET AND SET

void NeoFace::begin() {
	_edges.clear();
	_vertices.clear();
}

void NeoFace::addEdge(NeoEdge* edge) {
	edge->addIncidentFace(this);	//Make this face as incident to both endpoints of the edge
	_vertices.push_back(edge->getVertexBegin());

	_edges.push_back(edge);
}

void NeoFace::end() {
	refresh();
}

void NeoFace::setType(Type type) {
	_type = type;
}

NeoFace::Type NeoFace::getType() {
	return _type;
}

bool NeoFace::isTypeUndefined() {
	return _type == Type::UNDEFINED;
}

bool NeoFace::isTypeWalkable() {
	return _type == Type::WALKABLE;
}

bool NeoFace::isTypeObstacle() {
	return _type == Type::OBSTACLE;
}

NeoCell* NeoFace::getCell() {
	return _cell;
}

void NeoFace::setCell(NeoCell* cell) {
	if (_cell != cell) {
		_cell = cell;
		if (_cell) _cell->addFace(this);
	}
}

void NeoFace::getContour(Polygon3& result) {
	for (NeoEdgePVector::iterator it = _edges.begin(); it != _edges.end(); it++) {
		result.push_back((*it)->getVertexBegin()->getPosition());
	}
}

void NeoFace::computeCenter() {
	_center = Vector3(0,0,0);
	for (NeoEdgePVector::iterator it = _edges.begin(); it != _edges.end(); it++) {
		NeoEdge* edge = *it;
		_center += edge->getVertexBegin()->getPosition();
	}
	_center /= NeoReal(_edges.size());
}

void NeoFace::computeNormal() {
	_normal = Vector3(0,0,0);
	Polygon3 contour;
	getContour(contour);
	_normal = NeoGeometrySolver::getPolygonNormal(contour);
}

void NeoFace::computeAABB() {
	Polygon3 contour;
	getContour(contour);
	_aabb.addPoints(contour);
}

NeoEdgePVector& NeoFace::getEdges() {
	return _edges;
}

void NeoFace::getEdgesTo(NeoFace* face, NeoEdgePSet& result) {
	for (NeoEdgePVector::iterator it = _edges.begin(); it != _edges.end(); it++) {
		NeoEdge* e = *it;
		if (e->getOpposite() && (e->getOpposite()->getFace() == face)) result.insert(e);
	}
}

NeoEdge* NeoFace::getNextEdge(NeoEdge* edge) {
	NeoEdge* next = NULL;
	for (size_t i = 0; i < _edges.size(); i++) {
		if (_edges[i] == edge) {
			next = _edges[(i + 1) % _edges.size()];
			break;
		}
	}

	return next;
}

NeoEdge* NeoFace::getPrevEdge(NeoEdge* edge) {
	NeoEdge* prev = NULL;
	for (size_t i = 0; i < _edges.size(); i++) {
		if (_edges[i] == edge) {
			prev = (i > 0)? _edges[(i - 1)] : _edges.back();
			break;
		}
	}

	return prev;
}

bool NeoFace::isCoplanarTo(NeoFace* face) {
	return _normal.isParallelTo(face->getNormal());
}

Vector3& NeoFace::getNormal() {
	return _normal;
}

Vector3& NeoFace::getCenter() {
	return _center;
}

const NeoAABB& NeoFace::getAABB() {
	return _aabb;
}

NeoVertexPVector& NeoFace::getVertices() {
	return _vertices;
}

size_t NeoFace::getNumVertices() {
	return _vertices.size();
}

NeoVertex* NeoFace::getVertex(size_t i) {
	if (i >= _vertices.size()) return NULL;
	return _vertices[i];
}

Triangles& NeoFace::getTriangles() {
	return _triangles;
}

size_t NeoFace::getNumTriangles() {
	return _triangles.size();
}

void NeoFace::getAdjacents(NeoFacePSet& result) {
	for (NeoEdgePVector::iterator it = _edges.begin(); it != _edges.end(); it++) {
		NeoEdge* e = *it;
		if (e->isTypeWalkable()) result.insert(e->getOpposite()->getFace());
	}
}

void NeoFace::getAdjacentsCoplanar(NeoFacePSet& result) {
	NeoFacePSet tmp;
	getAdjacents(tmp);
	for (NeoFacePSet::iterator it = tmp.begin(); it != tmp.end(); it++) {
		NeoFace* f = *it;
		if (f->getNormal().equal(_normal)) result.insert(f);
	}
}

void NeoFace::getNeighbors(NeoFacePSet& result) {
	for (NeoEdgePVector::iterator itEdge = _edges.begin(); itEdge != _edges.end(); itEdge++) {
		NeoEdge* edge = *itEdge;
		edge->getVertexBegin()->getIncidentFaces(result);
		edge->getVertexEnd()->getIncidentFaces(result);
	}

	result.erase(this);
}

bool NeoFace::isNeighbor(NeoFace* face) {
	//The face shares at least one vertex with this face
	bool sharedVertex = false;
	for (NeoEdgePVector::iterator itEdge = _edges.begin(); itEdge != _edges.end(); itEdge++) {
		NeoEdge* edge = *itEdge;
		sharedVertex = (edge->getVertexBegin()->isIncidentFace(face) || edge->getVertexEnd()->isIncidentFace(face));
		if (sharedVertex) break;
	}
	
	return sharedVertex;
}

bool NeoFace::isAdjacent(NeoFace* face) {
	//The face shares exactly one edge with this face
	bool sharedEdge = false;
	for (NeoEdgePVector::iterator itEdge = _edges.begin(); itEdge != _edges.end(); itEdge++) {
		NeoEdge* edge = *itEdge;
		NeoEdge* opposite = edge->getOpposite();
		sharedEdge = (opposite && (opposite->getFace() == face));
		if (sharedEdge) break;
	}
	
	return sharedEdge;
}

#pragma endregion

#pragma region UPDATE

void NeoFace::refresh() {
	computeCenter();
	computeNormal();
	computeAABB();
	triangulate();
}

void NeoFace::triangulate() {
	//Look for the best projection plane
	NeoReal dYZ = std::abs(_normal.dot(Vector3(1,0,0)));
	NeoReal dXZ = std::abs(_normal.dot(Vector3(0,1,0)));
	NeoReal dXY = std::abs(_normal.dot(Vector3(0,0,1)));
	
	NeoReal dMax = std::max(dYZ, std::max(dXZ, dXY));
	Math::ProjectionPlane pPlane;
	if (Math::equal(dMax, dYZ)) pPlane = Math::ProjectionPlane::YZ;
	else if (Math::equal(dMax, dXZ)) pPlane = Math::ProjectionPlane::XZ;
	else pPlane = Math::ProjectionPlane::XY;

	//Create the contour
	Polygon2 contour;
	NeoTriangulator::IndexList indices;
	for (NeoVertexPVector::iterator it = _vertices.begin(); it != _vertices.end(); it++) {
		contour.push_back((*it)->getPosition().to2D(pPlane));
	}
	
	//Construct the triangles list
	NeoTriangulator::Process(contour, indices);
	_triangles.clear();
	for (size_t i = 0; i < indices.size(); i+=3) {
		Triangle t;
		t.push_back(_vertices[indices[i]]);
		t.push_back(_vertices[indices[i+1]]);
		t.push_back(_vertices[indices[i+2]]);

		//Check if the orientation of the triangle fits with the 
		if (!NeoGeometrySolver::getPolygonOrientation(t, _normal)) std::reverse(t.begin(), t.end());

		_triangles.push_back(t);
	}
}

#pragma endregion

#pragma region DEBUG

void NeoFace::log() {
	Ogre::LogManager* logManager = Ogre::LogManager::getSingletonPtr();
	logManager->logMessage("================================================");
	logManager->logMessage("FACE " + std::to_string(_id));
	logManager->logMessage("================================================");
	Ogre::LogManager::getSingletonPtr()->logMessage("Normal = " + _normal.toString());
	Polygon3 contour;
	getContour(contour);
	Ogre::LogManager::getSingletonPtr()->logMessage("Area = " + std::to_string(NeoGeometrySolver::polygonArea(contour)));
	Ogre::LogManager::getSingletonPtr()->logMessage("Type = " + std::to_string(_type));
		
	Ogre::LogManager::getSingletonPtr()->logMessage("-> Edges");
	for (NeoEdgePVector::iterator it = _edges.begin(); it != _edges.end(); it++) {
		NeoEdge* edge = *it;
		Vector3 v1Pos = edge->getVertexBegin()->getPosition();
		Vector3 v2Pos = edge->getVertexEnd()->getPosition();

		std::string s = "(";
		s += std::to_string(v1Pos.getX()) + ", "; 
		s += std::to_string(v1Pos.getY()) + ", ";
		s += std::to_string(v1Pos.getZ());
		
		s += "), (";

		s += std::to_string(v2Pos.getX()) + ", "; 
		s += std::to_string(v2Pos.getY()) + ", ";
		s += std::to_string(v2Pos.getZ());

		s += ")";
		
		logManager->logMessage(s);
	}
}

#pragma endregion

