#include "NeoMath.h"

using namespace NEOGEN;

#pragma region MATH CONSTANTS

const NeoReal Math::NEO_REAL_PRECISION = NeoReal(0.0001);
const NeoReal Math::NEO_REAL_INFINITY = std::numeric_limits<NeoReal>::infinity();
const NeoReal Math::NEO_PI = NeoReal(3.14159265358979323846);
const NeoReal Math::NEO_VERTEX_MERGE_DISTANCE = NeoReal(0.001);

#pragma endregion