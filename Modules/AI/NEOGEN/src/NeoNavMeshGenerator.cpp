#include "NeoNavMeshGenerator.h"
#include "NeoTerrain.h"
#include "NeoNavMesh.h"
#include "NeoPSimpl.h"
#include "NeoCell.h"

#include <OgreLogManager.h>
#include <OgreStringConverter.h>

using namespace NEOGEN;
using namespace NeoGeometrySolver;

#pragma region CREATION AND DESTRUCTION

NeoNavMeshGenerator::NeoNavMeshGenerator() {
	_testIndex = 0;
	_navMesh = NULL;
}

NeoNavMeshSPtr NeoNavMeshGenerator::initNavMesh(NeoTerrain* terrain) {
	if (!_navMesh) {
		//Init the faces of the NavMesh using the walkable faces of the original terrain. 
		_navMesh = NeoNavMeshSPtr(new NeoNavMesh(terrain->getNeoAABB()));
		NeoFaceMap& accessibleFaces = terrain->getMeshAccessible()->getFaces();
		for (NeoFaceMap::iterator itFace = accessibleFaces.begin(); itFace != accessibleFaces.end(); itFace++) {
			initNavMeshFace(itFace->second.get());
		}

		createObstacleContours();
		computeAllNotches();
		applyConvexityRelaxation(0);	//Apply the minimal convexity relaxation in order to avoid notches of exactly 180�
	}

	return _navMesh;
}

void NeoNavMeshGenerator::initNavMeshFace(NeoFace* face) {
	Polygon3 faceContour;
	const NeoEdgePVector& originalEdges = face->getEdges();
	for (NeoEdgePVector::const_iterator it = originalEdges.begin(); it != originalEdges.end(); it++) {
		faceContour.push_back((*it)->getVertexBegin()->getPosition());
	}
	NeoFace* f = _navMesh->createFace(faceContour);
	f->setType(NeoFace::Type::WALKABLE);
	
	//Mark the wall edges of the recently created face. 
	NeoEdgePVector& edges = f->getEdges();
	for (size_t i = 0; i < edges.size(); i++) edges[i]->setTypeWall(originalEdges[i]->isTypeWall());
}

void NeoNavMeshGenerator::createObstacleContours() {
	NeoEdgeMap& edgeMap = _navMesh->getEdges();
	NeoEdgePSet visitedEdges;
	for (NeoEdgeMap::iterator itEdge = edgeMap.begin(); itEdge != edgeMap.end(); itEdge++) {
		NeoEdge* edge = itEdge->second.get();
		if (edge->isTypeObstacle() && (visitedEdges.find(edge) == visitedEdges.end())) {
			
			//We have found an edge of a new contour. 
			NeoEdge* current = edge;
			Polygon3 obstacleContour;
			do {
				obstacleContour.push_back(current->getVertexBegin()->getPosition());
				visitedEdges.insert(current);
				
				do {
					current = current->getFace()->getNextEdge(current);
					if (!current->isTypeObstacle()) {
						current = current->getOpposite();
					}
				} while (!current->isTypeObstacle());
				/*current = current->getNextObstacle();*/
			} while (current != edge);
			//} while (visitedEdges.find(current) == visitedEdges.end());
			_obstacleContours.push_back(obstacleContour);
		}
	}
}

void NeoNavMeshGenerator::createCells() {
	NeoFaceMap& faces = _navMesh->getFaces();
	for (NeoFaceMap::iterator itFace = faces.begin(); itFace != faces.end(); itFace++) {
		NeoFace* f = itFace->second.get();
		if (!f->getCell()) {
			//This is the first face of a new cell. 
			NeoCell* cell = _navMesh->createCell();
			cell->begin();
			addFaceToCell(f, cell);
			cell->end();
		}
	}
}

#pragma endregion

#pragma region GET AND SET

void NeoNavMeshGenerator::addFaceToCell(NeoFace* face, NeoCell* cell) {
	if (face->getCell() != cell) {
		cell->addFace(face);

		//Recursive call to the neighbours connected by a non-portal walkable edge. 
		NeoEdgePVector& edges = face->getEdges();
		for (NeoEdgePVector::iterator it = edges.begin(); it != edges.end(); it++) {
			NeoEdge* e = *it;
			if (e->isTypeWalkable() && !e->isTypeSubPortal()) {
				addFaceToCell(e->getOpposite()->getFace(), cell);
			}
		}
	}
}

NeoNotchMap& NeoNavMeshGenerator::getNotchesReal() {
	return _realNotches;
}

NeoNotchMap& NeoNavMeshGenerator::getNotchesStrict() {
	return _allNotches;
}

void NeoNavMeshGenerator::getCandidateEdges(NeoVertex* vertex, NeoEdgePSet& result) {
	NeoFacePSet facesIgnored;
	getCandidateEdges(vertex, facesIgnored, result);
}

void NeoNavMeshGenerator::getCandidateEdges(NeoVertex* vertex, NeoFacePSet& facesIgnored, NeoEdgePSet& result) {
	//Add the edges of all the incident faces to the notch
	NeoFacePSet incidentFaces;
	vertex->getIncidentFaces(incidentFaces);
	for (NeoFacePSet::iterator itFace = incidentFaces.begin(); itFace != incidentFaces.end(); itFace++) {
		if (facesIgnored.find(*itFace) != facesIgnored.end()) continue;

		getCandidateEdges(vertex, *itFace, result);
	}
}

void NeoNavMeshGenerator::getCandidateEdges(NeoVertex* vertex, NeoFace* face, NeoEdgePSet& result) {
	//Add the edges of this face to the result. 
	NeoEdgePVector& faceEdges = face->getEdges();
	for (NeoEdgePVector::iterator itEdge = faceEdges.begin(); itEdge != faceEdges.end(); itEdge++) {
		if (!vertex->isIncidentEdge(*itEdge)) result.insert(*itEdge);
	}
}

void NeoNavMeshGenerator::computeCandidatePoint(NeoNotch& notch, NeoEdge* edge, NeoCandidate& result) {
	const Vector3& nPos = notch._vertex->getPosition();
	const Line3 lineEdge = Line3(edge->getVertexBegin()->getPosition(), edge->getVertexEnd()->getPosition());
	Vector3Set candidatePoints;
	
	//1) Check if the projection of the point over the edge lies inside the Area of Interest. If so, this is the 
	//best candidate point. 
	Vector3 proj = pointSegmentProjection(nPos, lineEdge);
	if (notch.isInsideAreaOfInterest(proj)) {
		result._edge = edge;
		result._point = proj;
		result._dist = nPos.dist(proj);
		return;
	}
	
	//The projection is not a valid candidate because it lies outside the area of interest. 
	//Compute the intersection point defined by the prolongation of the edges of the notch and
	//the edge. 
	//2) Compute the intersection of the prolongation of edgeIn and the edge. 
	IntersectionResult3 resultIn;
	Plane planeIn = Plane(nPos, notch._dirIn.cross(Vector3(0,1,0)), notch._dirIn);
	if (segmentPlaneIntersection(lineEdge._v0, lineEdge._v1, planeIn, resultIn)) {
		candidatePoints.insert(resultIn._intersectionPoint);
		candidatePoints.insert(resultIn._intersectionPoint2);
	}
		
	//3) The intersection of the prolongation of edgeOut and the edge. 
	IntersectionResult3 resultOut;
	Plane planeOut = Plane(nPos, notch._dirOut.cross(Vector3(0,1,0)), notch._dirOut);
	if (segmentPlaneIntersection(lineEdge._v0, lineEdge._v1, planeOut, resultOut)) {
		candidatePoints.insert(resultOut._intersectionPoint);
		candidatePoints.insert(resultOut._intersectionPoint2);
	}

	//Choose the closest candidate point (if any)
	NeoCandidate tmp;
	tmp._edge = edge;
	for (Vector3Set::iterator it = candidatePoints.begin(); it != candidatePoints.end(); it++) {
		if (!notch.isInsideAreaOfInterest(*it)) continue;

		tmp._point = *it;
		tmp._dist = nPos.dist(tmp._point);
		if (tmp < result) result = tmp;
	}
}

#pragma endregion

#pragma region UPDATE

NeoNavMeshSPtr NeoNavMeshGenerator::execute(NeoTerrain* terrain, NeoReal convexityRelaxation) {
	if (!terrain) return NULL;
	
	initNavMesh(terrain);
	applyConvexityRelaxation(convexityRelaxation);
	breakRealNotches();
	createCells();

	return _navMesh;
}

void NeoNavMeshGenerator::applyConvexityRelaxation(NeoReal cr) {
	//1) Simplify the original set of obstacle contours by applying the Douglas-Peucker Algorithm. 
	//Keep a set of all the surviving points. 
	KeyPoints keyPoints;
	cr = std::max(Math::NEO_REAL_PRECISION, cr);
	for (Polygons3::iterator itContour = _obstacleContours.begin(); itContour != _obstacleContours.end(); itContour++) {
		Polygon3 simp;
		NeoPSimpl::simplifyDouglasPeucker(*itContour, cr, simp);
		
		for (Polygon3::iterator itPoint = simp.begin(); itPoint != simp.end(); itPoint++) {
			keyPoints.insert(*itPoint);
		}
	}

	//2) If the notch is a real notch, the vertex will be a key vertex of one of the simplified polygons.
	_realNotches.clear();
	for (NeoNotchMap::iterator it = _allNotches.begin(); it != _allNotches.end(); it++) {
		Vector3 vPos = it->second._vertex->getPosition();
		if (keyPoints.find(vPos) != keyPoints.end()) _realNotches.insert(*it);
	}
}

void NeoNavMeshGenerator::computeAllNotches() {
	for (Polygons3::iterator itObstacle = _obstacleContours.begin(); itObstacle != _obstacleContours.end(); itObstacle++) {
		Polygon3& obstacle = *itObstacle;
		
		if (obstacle.size() < 3) continue;

		for (size_t i = 0; i < obstacle.size(); i++) {
			NeoVertex* v = _navMesh->getVertex(obstacle[i]);
			NeoVertex* prev = (i == 0) ? _navMesh->getVertex(obstacle.back()) : _navMesh->getVertex(obstacle[i - 1]);
			NeoVertex* next = _navMesh->getVertex(obstacle[(i + 1) % obstacle.size()]);
			
			NeoEdge* edgeIn = _navMesh->getEdge(prev, v);
			NeoEdge* edgeOut = _navMesh->getEdge(v, next);

			if (edgeIn && edgeOut && (edgeIn != edgeOut) && v->isNotch(edgeIn)) _allNotches[v->getPosition()] = NeoNotch(v, edgeIn, edgeOut);
		}
	}
	_realNotches = _allNotches;
}

void NeoNavMeshGenerator::breakRealNotches() {
	for (NeoNotchMap::iterator itNotches = _realNotches.begin(); itNotches != _realNotches.end(); itNotches++) {
		//Check if the vertex is still a notch, as it is possible that the notch condition has been broken 
		//due to a case vertex-vertex portal. 
		NeoNotch& notch = itNotches->second;
		if (notch._vertex->isNotch(notch._dirIn)) {
			//_navMesh->exportOBJ("navMesh" + std::to_string(_testIndex++) + ".obj");
			breakRealNotch(notch);
		}
	}
}

void NeoNavMeshGenerator::breakRealNotch(NeoNotch& notch) {
	NeoEdgePSet candidateEdges;
	getCandidateEdges(notch._vertex, candidateEdges);
	NeoCandidate bestCandidate;
	computeBestCandidate(notch, candidateEdges, bestCandidate);

	NeoPortalPair portals = NeoPortalPair(_navMesh->createPortal(), _navMesh->createPortal());
	portals.first->setOpposite(portals.second);
	portals.second->setOpposite(portals.first);

	const Vector3& posNotch = notch._vertex->getPosition();
	if (bestCandidate._edge->isTypeObstacle()) {
		createSubPortals(bestCandidate._point, Ray3(posNotch, bestCandidate._point - posNotch), portals);
	}
	else {
		//The best candidate is a portal. First, check if any of the endpoints lie inside the area of interest
		//of the notch. If so, it is safe to create a portal with that point and this portal will break the 
		//notch, because it is inside the area of interest. 
		NeoPortal* portal = (NeoPortal*)(bestCandidate._edge->getParent());
		const Vector3& posBegin = portal->getVertexBegin()->getPosition();
		const Vector3& posEnd = portal->getVertexEnd()->getPosition();
		bool insideBegin = notch.isInsideAreaOfInterest(posBegin);
		bool insideEnd = notch.isInsideAreaOfInterest(posEnd);

		if (insideBegin && insideEnd) {
			if (Math::less(posNotch.dist(posBegin), posNotch.dist(posEnd))) {
				createSubPortals(posBegin, Ray3(posNotch, posBegin - posNotch), portals);
			}
			else {
				createSubPortals(posEnd, Ray3(posNotch, posEnd - posNotch), portals);
			}
		}
		else if (insideBegin) createSubPortals(posBegin, Ray3(posNotch, posBegin - posNotch), portals);
		else if (insideEnd) createSubPortals(posEnd, Ray3(posNotch, posEnd - posNotch), portals);
		else {
			//None of the endpoints of the portals lies in the area of interest. Split the 
			//notch into two notches and break them until we reach a base case. 
			Vector3 dirBegin = posBegin - posNotch;
			Vector3 dirEnd = posEnd - posNotch;
			breakRealNotch(NeoNotch(notch._vertex, dirBegin, dirBegin));
			breakRealNotch(NeoNotch(notch._vertex, dirEnd, dirEnd));
		}
	}
}

void NeoNavMeshGenerator::computeBestCandidate(NeoNotch& notch, NeoEdgePSet& candidateEdges, NeoCandidate& result) {
	//Among all the candidate edges, find the best candidate. 
	NeoCandidate tmpCandidate;
	for (NeoEdgePSet::iterator itCandidate = candidateEdges.begin(); itCandidate != candidateEdges.end(); itCandidate++) {
		if (result._traversedEdges.find(*itCandidate) != result._traversedEdges.end()) continue;

		computeCandidatePoint(notch, *itCandidate, tmpCandidate);
		if (tmpCandidate < result) {
			result._edge = tmpCandidate._edge;
			result._point = tmpCandidate._point;
			result._dist = tmpCandidate._dist;
		}
	}

	if (!result._edge->isTypeObstacle() && !result._edge->isTypeSubPortal()) {
		//The closest element is a walkable edge. Add the candidate edges of the face this edge connects to. 
		NeoFace* oFace = result._edge->getOpposite()->getFace();
		getCandidateEdges(notch._vertex, oFace, candidateEdges);
		result._traversedEdges.insert(result._edge);
		result._traversedEdges.insert(result._edge->getOpposite());
		result._edge = NULL;
		
		computeBestCandidate(notch, candidateEdges, result);
	}

	//Refine the position of the best candidate, i.e., check if it is close to an endpoint of the candidate edge. 
	NeoReal d2Begin = result._point.dist2(result._edge->getVertexBegin()->getPosition());
	NeoReal d2End = result._point.dist2(result._edge->getVertexEnd()->getPosition());
	NeoReal d2Edge = result._edge->getVertexBegin()->getPosition().dist2(result._edge->getVertexEnd()->getPosition());
	if ((d2Begin > d2Edge) || (d2End > d2Edge)) {
		//The candidate is out of bounds. 
		result._point = (d2Begin < d2End) ? result._edge->getVertexBegin()->getPosition() : result._edge->getVertexEnd()->getPosition();
	}
}

#pragma endregion

#pragma region PORTAL CREATION

void NeoNavMeshGenerator::createSubPortals(const Vector3& bestCandidatePos, const Ray3& ray, NeoPortalPair& portals) {
	//_navMesh->exportOBJ("navMesh" + std::to_string(_testIndex++) + ".obj");

	NeoVertex* v1 = _navMesh->getVertex(ray._v0);
	NeoEdgePair subEdges;
	NeoVertex* v2 = subdivideIntersectedEdge(ray, subEdges);

	bool test = _navMesh->getVertex(ray._v0)->getID() == 13;

	//Check if the subportals already exists, i.e., it is a walkable edge that will
	//be converted to subportal. 
	NeoEdgePair subPortals(_navMesh->getEdge(v1, v2), _navMesh->getEdge(v2, v1));
	if (!subPortals.first) {
		//Destroy the face intersected by the portal. By construction, the portal is always
		//created between a vertex of a walkable face and another vertex of this face. 
		NeoFace* face = subEdges.first->getFace();
		Polygon3 contourOriginal;
		face->getContour(contourOriginal);
		
		const Vector3& posV1 = v1->getPosition();
		const Vector3& posV2 = v2->getPosition();
		size_t startF1, startF2;
		size_t numVertices = contourOriginal.size();
		for (size_t i = 0; i < numVertices; i++) {
			if (contourOriginal[i].equal(posV1)) startF1 = i;
			if (contourOriginal[i].equal(posV2)) startF2 = i;
		}

		if (startF1 != startF2) {
			//Reconstruct the subfaces
			Polygon3 contourF1, contourF2;
			getSubPolygon(contourOriginal, startF1, startF2, contourF1);
			getSubPolygon(contourOriginal, startF2, startF1, contourF2);
			bool isValidF1 = !getPolygonNormal(contourF1).equal(Vector3(0, 0, 0));
			bool isValidF2 = !getPolygonNormal(contourF2).equal(Vector3(0, 0, 0));

			if (!isValidF1 || !isValidF2) {
				Ogre::LogManager::getSingletonPtr()->logMessage("NON VALID FACE!!!");
			}

			_navMesh->destroyFaceOnly(face);
			//if (isValidF1 && isValidF2) {
				//Both faces resulting from the subdivision are valid. 
				_navMesh->createFace(contourF1)->setType(NeoFace::Type::WALKABLE);
				_navMesh->createFace(contourF2)->setType(NeoFace::Type::WALKABLE);
			//}
			//else {
				//At least one face resulting of the subdivision is too narrow or small to be considered
				//a valid face. Therefore, we reconstruct a single face
			//}

			//Create the subportals. 
			subPortals = NeoEdgePair(_navMesh->createEdge(v1, v2), _navMesh->createEdge(v2, v1));
		}
	}

	//Add the corresponding subportal to the corresponding portal in the corresponding order. 
	portals.first->addSubPortalBack(subPortals.first);
	portals.second->addSubPortalFront(subPortals.second);

	if (!v2->isObstacle()) {
	//if (!v2->getPosition().equal(bestCandidatePos)) {
		//If v2 is not an obstacle, it means that is a point over an intermediate walkable edge. 
		//Therefore, we recursively call this function until we reach an obstacle edge. 
		createSubPortals(bestCandidatePos, Ray3(v2->getPosition(), bestCandidatePos - v2->getPosition()), portals);
	}
}

NeoVertex* NeoNavMeshGenerator::subdivideIntersectedEdge(const Ray3& ray, NeoEdgePair& result) {
	//1) Check if any of the incident edges to ray._v0 is parallel to the direction of the ray.
	NeoEdgePSet edgesOut;
	bool test = _navMesh->getVertex(ray._v0)->getID() == 13;
	_navMesh->getVertex(ray._v0)->getIncidentEdgesOut(edgesOut);
	for (NeoEdgePSet::iterator it = edgesOut.begin(); it != edgesOut.end(); it++) {
		NeoEdge* e = *it;
		//if (ray._direction.isParallelTo(e->getDirection()) && Math::greater(ray._direction.dot(e->getDirection()), NeoReal(0.0))) {
		
		if (Math::equal(ray._direction.angle(e->getDirection()), 0.0f)) {
			result.first = result.second = *it;
			return e->getVertexEnd();
		}
	}

	//2) At this point, none of the incident edges to ray._v0 is parallel to the direction of the ray. So the ray intersects
	//an edge by a single point. Find this edge and the intersection point. 
	NeoEdgePSet candidateEdges;
	getCandidateEdges(_navMesh->getVertex(ray._v0), candidateEdges);
	NeoEdge* edge = NULL;
	IntersectionResult3 closestResult, tmpResult;

	for (NeoEdgePSet::iterator it = candidateEdges.begin(); it != candidateEdges.end(); it++) {
		segmentPlaneIntersection((*it)->getVertexBegin()->getPosition(), (*it)->getVertexEnd()->getPosition(), Plane(ray._v0, ray._direction.cross(Vector3(0,1,0)), ray._direction), tmpResult);
		if (tmpResult._intersectionType == IntersectionType::SINGLE_POINT) {
			if (_navMesh->getVertex(tmpResult._intersectionPoint)) {
				closestResult = tmpResult;
				edge = *it;
				break;
			}
			if (
				(closestResult._intersectionType != IntersectionType::SINGLE_POINT) ||
				(Math::greater(closestResult._intersectionPoint.dist(ray._v0), tmpResult._intersectionPoint.dist(ray._v0)))
				)
			{
				closestResult = tmpResult;
				edge = *it;
			}
		}
	}

	if (closestResult._intersectionPoint.equal(ray._v0)) 
	{
		Ogre::LogManager::getSingletonPtr()->logMessage("EQUAL VERTICES!!!");
		Ogre::LogManager::getSingletonPtr()->logMessage("id = " + std::to_string(_navMesh->getVertex(ray._v0)->getID()));
	}

	//Subdivide the edge and its opposite (if any) at this point. 
	NeoVertex* v = _navMesh->createVertex(closestResult._intersectionPoint);
	result = _navMesh->subdivideEdge(edge, v);

	return v;
}

NeoFace* NeoNavMeshGenerator::reconstructFace(const Polygon3& contourOriginal, size_t startVertex, size_t endVertex) {
	size_t i = startVertex;
	size_t numVertices = contourOriginal.size();
	Polygon3 contour;
	do {
		contour.push_back(contourOriginal[i]);
		i = (i + 1) % numVertices;
	} while (i != ((endVertex + 1) % numVertices));
	NeoPSimpl::simplifyRadialDistance(contour);
	
	NeoFace* f = _navMesh->createFace(contour);
	f->setType(NeoFace::Type::WALKABLE);

	return f;
}

#pragma endregion