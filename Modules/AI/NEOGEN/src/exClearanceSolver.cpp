#include "exClearanceSolver.h"

using namespace Exact;
using namespace NEOGEN;
using namespace GeometrySolver;

#pragma region CREATION AND DESTRUCTION

ClearanceSolver::ClearanceSolver() {
	_navMesh = NULL;
	setShrinkPortalMode(SP_EXACT);
	setAcceleration(true, true);
	useRecursivity(true);
	resetStatistics();
	_timer.reset();
}

void ClearanceSolver::resetStatistics() {
	_statistics.reset();
}

#pragma endregion

#pragma region GET AND SET

ClearanceSolver::Statistics& ClearanceSolver::getStatistics() {
	return _statistics;
}

void ClearanceSolver::useRecursivity(bool rec) {
	if (rec != _useRecursivity) {
		_useRecursivity = rec;
		clearShrinkedTables();
	}
}

bool ClearanceSolver::isUsingRecursivity() {
	return _useRecursivity;
}

void ClearanceSolver::setShrinkPortalMode(ShrinkPortalMode mode) {
	_shrinkPortalMode = mode;
}

ClearanceSolver::ShrinkPortalMode ClearanceSolver::getShrinkPortalMode() {
	return _shrinkPortalMode;
}

void ClearanceSolver::setNavMesh(NavMesh* nm) {
	_navMesh = nm;
}

void ClearanceSolver::setAcceleration(bool useTables, bool useCriticalRadius) {
	_useTables = useTables;
	_useCriticalRadius = useCriticalRadius;
}

void ClearanceSolver::addCross(CrossKey& key, float cValue2) {
	//Si el cross definit per la parella no existeix, l'afegeix a la taula. 
	correctCrossKey(key);
	_crossTable[key] = cValue2;
}

float ClearanceSolver::getCross(CrossKey& key) {
	correctCrossKey(key);
	CrossTable::iterator itFound = _crossTable.find(key);
	if (itFound != _crossTable.end()) return itFound->second;	//El cross indicat existeix
	else return 0.0f;											//El cross indicat NO existeix
}

void ClearanceSolver::correctCrossKey(CrossKey& key) {
	//For conveniency, key.first is the lowest portalID, and key.second is the biggest one. 
	int kFirst = key.first;
	int kSecond = key.second;
	key.first = std::min(kFirst, kSecond);
	key.second = std::max(kFirst, kSecond);
}

#pragma endregion

#pragma region CLEARANCE BEGIN

void ClearanceSolver::computeClearance() {
	//Compute the critical radius of each portal endpoint
	computeCriticalRadii();

	//Calcula el valor de clearance de totes les cel�les de la NavMesh
	computeClearanceCells();
}

#pragma endregion

#pragma region CLEARANCE BY CELL

void ClearanceSolver::computeClearanceCells() {
	unsigned long tBefore = _timer.getMilliseconds();
	
	CellTable& cellTable = _navMesh->getCellTable();
	for (CellTable::iterator itCell = cellTable.begin(); itCell != cellTable.end(); itCell++) {
		computeClearanceCell(itCell->first);
	}

	_statistics._timeComputeClearanceCell += (_timer.getMilliseconds() - tBefore);
}

void ClearanceSolver::computeClearanceCell(int idCell) {
	//Calcula el valor de clearance de la cel�la idCell
	//1) El valor de clearance d'una cel�la dep�n de com travessem aquesta cel�la. El primer
	//que cal fer �s determinar de quantes formes podem creuar la cel�la, �s a dir, determinar
	//quantes parelles de portals diferents hi ha. 
	Ogre::LogManager::getSingletonPtr()->logMessage("******************************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("COMPUTING CLEARANCE FOR CELL " + std::to_string(idCell));
	Ogre::LogManager::getSingletonPtr()->logMessage("******************************************************");
	Cell& c = _navMesh->getCell(idCell);
	std::pair<int, int> portalPair;
	size_t numPortals = c.portals.size();
	float cValue2;
	if (numPortals > 1) {
		//�s una cel�la amb un m�nim de dos portals. 

		//Utilitzo un vector de portals en comptes d'un set, perqu� �s m�s f�cil i eficient 
		//determinar les parelles en un vector. 
		std::vector<int> vecPortals;
		for (std::set<int>::iterator itPortal = c.portals.begin(); itPortal != c.portals.end(); itPortal++) vecPortals.push_back(*itPortal);

		//Determino quantes parelles diferents de portals hi ha. 
		size_t i = 0;
		size_t j;
		Ogre::LogManager::getSingletonPtr()->logMessage("numPortals = " + std::to_string(numPortals));
		while (i < (numPortals - 1)) {
			j = i + 1;
			while (j < numPortals) {
				portalPair.first = vecPortals[i];
				portalPair.second = vecPortals[j];
				cValue2 = computeClearanceCrossCell(c, portalPair);
				addCross(portalPair, cValue2);
				Ogre::LogManager::getSingletonPtr()->logMessage("=======================");
				Ogre::LogManager::getSingletonPtr()->logMessage("PORTAL ENTRY = " + std::to_string(portalPair.first));
				Ogre::LogManager::getSingletonPtr()->logMessage("PORTAL EXIT = " + std::to_string(portalPair.second));
				Ogre::LogManager::getSingletonPtr()->logMessage("CLEARANCE VALUE = " + std::to_string(cValue2));
				Ogre::LogManager::getSingletonPtr()->logMessage("=======================");
				j++;
			}
			i++;
		}
	}
	else {
		//�s una cel�la dead-end. �s a dir, nom�s t� un sol portal
		Ogre::LogManager::getSingletonPtr()->logMessage("DEAD-END CELL");
		cValue2 = _navMesh->getEdge(*(c.portals.begin())).clearance2;
		Ogre::LogManager::getSingletonPtr()->logMessage("=======================");
		Ogre::LogManager::getSingletonPtr()->logMessage("CLEARANCE VALUE = " + std::to_string(cValue2));
		Ogre::LogManager::getSingletonPtr()->logMessage("=======================");
	}
}

float ClearanceSolver::computeClearanceCrossCell(Cell& c, CrossKey& portalPair) {
	//Calcula el valor de clearance de la cel�la c donat el cross que ve definit per portalPair

	//1) Haig de subdividir les arestes en 2 grups, un que queda a l'esquerra de la direcci� de pas, 
	//i l'altre que queda a la dreta
	int entryPortalID = portalPair.first;
	int exitPortalID = portalPair.second;
	std::vector<int> leftString;
	std::vector<int> rightString;
	computeVertStrings(entryPortalID, exitPortalID, leftString, rightString);

	//2) Calculo el clearance m�xim que pot tenir la cel�la, que �s la longitud del portal del cross m�s petit. 
	//Perqu� el personatge pugui passar per aquest cross, com a m�nim ha de ser capa� de passar pel portal
	//del cross m�s estret
	float cMax2 = std::min(_navMesh->getEdge(entryPortalID).clearance2, _navMesh->getEdge(exitPortalID).clearance2);
	
	//3) Calculo el valor de clearance del stringLeft respecte el stringRight i 
	//el valor de clearance del stringRight respecte el stringLeft
	//Les dues cadenes tenen com a m�nim una aresta cadascuna. 
	float cLeft2, cRight2;
	if (leftString.size() == 1) {
		//Ogre::LogManager::getSingletonPtr()->logMessage("LEFT VERTEX SHARED!!!");
		//La leftString cont� un �nic v�rtex, que �s el que comparteixen entryPortal i exitPortal per l'esquerra. 
		cLeft2 = computeClearanceVertexString(leftString[0], rightString);
		cRight2 = cMax2;	//La dist�ncia de la cadena d'strtings al v�rtex, nom�s pot ser >= al clearance m�xim
	}
	else if (rightString.size() == 1) {
		//Ogre::LogManager::getSingletonPtr()->logMessage("RIGHT VERTEX SHARED!!!");
		//La rightString cont� un �nic v�rtex, que �s el que comparteixen entryPortal i exitPortal per la dreta. 
		cLeft2 = cMax2;	//La dist�ncia de la cadena d'strtings al v�rtex, nom�s pot ser >= 
		cRight2 = computeClearanceVertexString(rightString[0], leftString);
	}
	else {
		//entryPortal i exitPortal no comparteixen cap dels seus extrems
		cLeft2 = computeClearanceStringString(leftString, rightString);
		cRight2 = computeClearanceStringString(rightString, leftString);
	}
	
	//4) El valor de clearance de la cel�la �s el m�nim valor de clearance calculat
	return std::min(cMax2, std::min(cLeft2, cRight2));
}

void ClearanceSolver::computeVertStrings(int entryPortalID, int exitPortalID, std::vector<int>& leftString, std::vector<int>& rightString) {
	//Donat un portal d'entrada entryPortal i un portal de sortida exitPortal, retorna la cadena de v�rtexs que formen les arestes	
	//que uneixen el portal d'entrada i de sortida per l'esquerra (leftString) i els v�rtexs que formen les arestes
	//que uneixen el portal d'entrada i de sortida per la dreta (rightString). 

	//Per conveni, considero que leftString �s la cadena de v�rtexs entre entryPortal i exitPortal movent-me sempre agafant 
	//el punter next, i rightString cont� la cadena de v�rtexs agafant sempre el punter prev. 
	
	Edge& entryPortal = _navMesh->getEdge(entryPortalID);
	Edge& exitPortal = _navMesh->getEdge(exitPortalID);
	Edge* ePtr;

	//Calculo leftString. Cont� la seq��ncia de v�rtexs en ordre de les arestes que uneixen entryPortal i exitPortal
	//per l'esquerra
	if (entryPortal.next == exitPortalID) {
		//El portal d'entrada i de sortida comparteixen l'extrem per l'esquerra. Per tant, stringLeft cont� un 
		//�nic element, que �s l'identificador d'aquest v�rtex que comparteixen. 
		if (entryPortal.v1 == exitPortal.v2) leftString.push_back(entryPortal.v1);	//L'extrem compartit �s entryPortal.v1
		else leftString.push_back(entryPortal.v2);	//L'extrem compartit �s entryPortal.v2
	}
	else {
		//entryPortal i exitPortal NO comparteixen l'extrem per l'esquerra. 
		ePtr = &entryPortal;
		do {
			ePtr = &(_navMesh->getEdge(ePtr->next));
			leftString.push_back(ePtr->v1);
		} while (ePtr->id != exitPortalID);
	}

	//Calculo rightString. Cont� la seq��ncia de v�rtexs en ordre de les arestes que uneixen entryPortal i exitPortal
	//per la dreta
	if (entryPortal.prev == exitPortalID) {
		//El portal d'entrada i de sortida comparteixen l'extrem per la dreta. Per tant, stringRight cont� un 
		//�nic element, que �s l'identificador d'aquest v�rtex que comparteixen. 
		if (entryPortal.v1 == exitPortal.v2) rightString.push_back(entryPortal.v1);	//L'extrem compartit �s entryPortal.v1
		else rightString.push_back(entryPortal.v2);	//L'extrem compartit �s entryPortal.v2
	}
	else {
		//entryPortal i exitPortal NO comparteixen l'extrem per la dreta. 
		ePtr = &entryPortal;
		do {
			ePtr = &(_navMesh->getEdge(ePtr->prev));
			rightString.push_back(ePtr->v2);
		} while (ePtr->id != exitPortalID);
	}
}

float ClearanceSolver::computeClearanceStringString(std::vector<int>& vertString1, std::vector<int>& vertString2) {
	IntersectionPoint2D projection;
	float cValue2 = FLOAT_INFINITY;
	float cAux2;
	//Recorro tots els v�rtexs de vertString1
	for (std::vector<int>::iterator itVert = vertString1.begin(); itVert != vertString1.end(); itVert++) {
		cAux2 = computeClearanceVertexString(*itVert, vertString2);
		if (cAux2 < cValue2) cValue2 = cAux2;
	}
	return cValue2;
}

float ClearanceSolver::computeClearanceVertexString(int idVertex, std::vector<int>& vertString) {
	//Calcula la dist�ncia^2 m�nima entre el vertex idVertex i la cadena vertString (on cada parella de v�rtexs consecutius
	//defineix una aresta). 
	
	//PRE: La cadena vertString cont� com a m�nim dos elements (�s a dir, definex com a m�nim una aresta). 
	
	Vec2D v1 = _navMesh->getVertex(idVertex).getPosition2D();
	
	//Comprobo quina �s l'aresta m�s propera de vertString
	Vec2D s1, s2;
	IntersectionPoint2D projection;	
	float cValue2 = FLOAT_INFINITY;
	float cAux2;
	size_t numVerts = vertString.size();
	for (size_t i = 0; i < (numVerts - 1); i++) {
		s1 = _navMesh->getVertex(vertString[i]).getPosition2D();
		s2 = _navMesh->getVertex(vertString[i + 1]).getPosition2D();
		GeometrySolver::projectionPointSegment2D(v1, s1, s2, projection);
		cAux2 = v1.dist2(projection.location);
		if (cAux2 < cValue2) cValue2 = cAux2;
	}
	return cValue2;
}

#pragma endregion

#pragma region CRITICAL RADII

float ClearanceSolver::computeClearancePortal(int idPortal) {
	Edge& portal = _navMesh->getEdge(idPortal);
	Vec2D v1 = _navMesh->getVertex(portal.v1).getPosition2D();
	Vec2D v2 = _navMesh->getVertex(portal.v2).getPosition2D();
	float cValue2 = v1.dist2(v2);
	portal.clearance2 = cValue2;
	return cValue2;
}

void ClearanceSolver::computeCriticalRadii() {
	//For each endpoint of a portal, its critical radius is computed, i.e., the
	//maximum radius for which is necessary only the first step of the portal shrinkment
	/*for (PortalTable::iterator it = _portalTable.begin(); it != _portalTable.end(); it++) {
		//We have to take into account the two cells that shares the portal. 
		Edge& portal = _navMesh->getEdge(*it);
		computeCriticalRadius(portal.id);
		computeCriticalRadius(portal.opposite);
	}*/

	unsigned long tBefore = _timer.getMilliseconds();

	EdgeTable& edgeTable = _navMesh->getEdgeTable();
	for (EdgeTable::iterator it = edgeTable.begin(); it != edgeTable.end(); it++) {
		//We have to take into account the two cells that shares the portal. 
		Edge& e = it->second;
		if (e.isPortal()) {
			computeClearancePortal(e.id);
			computeCriticalRadius(e.id);
			computeCriticalRadius(e.opposite);
		}
	}

	_statistics._timeCriticalRadii += (_timer.getMilliseconds() - tBefore);
}

void ClearanceSolver::computeCriticalRadius(int portalID) {
	//Computes the critical radius of the portal portalID. 
	Edge* e = &(_navMesh->getEdge(portalID));		
	Vec2D vDirPortal = e->_vDirNormalized2D;	//Portal's Normalized Direction Vector
	Vec2D vNormalPortal = e->_vNormal2D;		//Portal's Normal Vector
	int v1PortalID = e->v1;
	int v2PortalID = e->v2;
	Vec3D v1 = _navMesh->getVertex(v1PortalID).getPosition();	//Portal's Initial Endpoint
	Vec3D v2 = _navMesh->getVertex(v2PortalID).getPosition();	//Portal's Final Endpoint
		
	float maxR2 = v1.dist2(v2) / 4.0f;		//The maximum Radius^2 that we have to check
	//Note: v1->dist2(*v2) is the total length of the portal, i.e., the maximum DIAMETER^2 that the
	//character can have to go through the portal. As we want the radius, and the distance computed
	//is squared, we need to divide by 4 (2^2) to obtain the RADIUS^2
	Vec2D v1Portal = GeometrySolver::to2D(v1);
	Vec2D v2Portal = GeometrySolver::to2D(v2);

	float d2 = maxR2;
	do {
		e = &(_navMesh->getEdge(e->next));
		if (e->isPortal()) continue;
		//We are only interested in the edges pointing towards the portal, i.e., those edges that has
		//a Direction Vector that forms an angle > 90� with the Portal's Direction Vector 
		//(so its dot product is < 0)
		if (RealMath::greaterOrEqual(vDirPortal.dotProduct(e->_vDirNormalized2D), 0.0f)) continue;	//Edge not pointing to portal
		
		//If we arrive here is because the edge is pointing towards the portal. Compute its distance. 
		Vec2D v1Edge = _navMesh->getVertex(e->v1).getPosition2D();	//Extrem inicial de l'aresta
		Vec2D v2Edge = _navMesh->getVertex(e->v2).getPosition2D();	//Extrem final de l'aresta
		d2 = GeometrySolver::segmentSegmentSquaredDistance2D(v1Portal, v2Portal, v1Edge, v2Edge);
		
		//Check the vertex that we must update
		if (vNormalPortal.dotProduct(e->_vDirNormalized2D) < 0.0f) {
			//v1Edge es troba a l'esquerra del portal, i v2 a la dreta. Per tant, �s una aresta IN respecte el portal.
			setCriticalRadius(v1PortalID, d2);
		}
		else {
			//v1Edge es troba a la dreta del portal, i v2 a l'esquerra. Per tant, �s una aresta OUT respecte el portal. 
			setCriticalRadius(v2PortalID, d2);
		}
	} while (e->id != portalID);
}

void ClearanceSolver::setCriticalRadius(int vertexID, float r) {
	if (
		(_criticalRadiiMap.find(vertexID) == _criticalRadiiMap.end()) ||	//This entry does not exist yet
		(_criticalRadiiMap[vertexID] > r)									//The new radius is more restrective
		) 
	{
		_criticalRadiiMap[vertexID] = r;
	}
}

#pragma endregion

#pragma region PORTAL SHRINKMENT 

bool ClearanceSolver::shrinkPortalSimple(int portalID, float clearance, Vec3D& v1Result, Vec3D& v2Result) {
	//Com a m�nim, haig de moure els extrems del portal clearance unitats cap a l'interior del portal. 
	if (_useTables && getShrinkedPortal(_shrinkedPortalMapSimple, portalID, clearance, v1Result, v2Result)) return true;	//Shrinked portal in table
	Edge& portal = _navMesh->getEdge(portalID);
	Vec3D v1Portal = _navMesh->getVertex(portal.v1).getPosition();
	Vec3D v2Portal = _navMesh->getVertex(portal.v2).getPosition();
	v1Result = v1Portal + portal._vDirNormalized * clearance;
	v2Result = v2Portal - portal._vDirNormalized * clearance;

	bool v1Refined = _useCriticalRadius && ((clearance * clearance) <= _criticalRadiiMap[portal.v1]);
	bool v2Refined = _useCriticalRadius && ((clearance * clearance) <= _criticalRadiiMap[portal.v2]);
	if ((!v1Refined || !v2Refined) && _useTables) addShrinkedPortal(_shrinkedPortalMapSimple, portalID, clearance, v1Result, v2Result);
	return false;	//Shrinked portal not found
}

int ClearanceSolver::shrinkPortalByExitPortalEndpoint(int shrinkPortalID, Vec2D& vToShrink, Vec2D& vFixed, bool inEndpoint, int startingCellID, Path& path, float clearance) {
	//Shrinks the endpoint vToShrink of the portal by determining if the exit portals in the path 
	//influences the portal to shrink. 
	//@shrinkPortalID: The ID of the portal that we are shrinking
	//@vToShrink: The endpoint of the portal that will be shrinked if it is influenced by any of the exit portals
	//@vFixed: The other endpoint of the portal that will remain fixed during this process
	//@inEndpoint: Indicates if vToShrink is the starting endpoint (true) or the final one (false)
	//@startingCellID: The cell of the portal to shrink
	//@@return: The entry portal of the last cell of the path that influences the portal to shrink. 
	//It is used for the 3rd step of the shrinkment process. 
	int exitPortalID = -1;				//The exit portal influencing the portal to shrink
	int cellID = startingCellID;		//The cellID of the portal to shrink
	int nextCellID = -1;				//The ID of the next cell in the path
	bool exitPortalInfluence = true;	//Indicates if the current exit portal influences the portal to shrink
	int nextCellEntryPortalID = shrinkPortalID;
	
	while (exitPortalInfluence && !path.isGoalNode(cellID)) {
		nextCellID = path.getNextNode(cellID);
		exitPortalID = _navMesh->getPortalTo(cellID, nextCellID);	//The portal of cellID used to go from cellID to nextCellID
		
		//Check if the exit portal influences the portal to shrink
		Edge& exitPortal = _navMesh->getEdge(exitPortalID);
		Vec2D v1ExitPortal = _navMesh->getVertex(exitPortal.v1).getPosition2D();
		Vec2D v2ExitPortal = _navMesh->getVertex(exitPortal.v2).getPosition2D();
		if (inEndpoint) {
			//The FINAL endpoint of the exit portal may determine the STARTING endpoint of the portal to shrink
			exitPortalInfluence = shrinkPortalByPortalEndpoint(vToShrink, vFixed, v2ExitPortal, clearance);
		}
		else {
			//The STARTING endpoint of the exit portal may determine the FINAL endpoint of the portal to shrink
			exitPortalInfluence = shrinkPortalByPortalEndpoint(vToShrink, vFixed, v1ExitPortal, clearance);
		}
	
		if (exitPortalInfluence) {
			nextCellEntryPortalID = exitPortal.opposite;
			cellID = nextCellID;
		}
	}
	return nextCellEntryPortalID;
}

/*bool ClearanceSolver::shrinkPortal(int entryPortalID, Path& path, float clearance, Vec3D& v1Result, Vec3D& v2Result) {
	//Comprobo si el portal ja existeix
	if (_useTables && getShrinkedPortal(_shrinkedPortalMap, entryPortalID, clearance, v1Result, v2Result)) return true;	//Shrinked portal in table
	
	//Si arribo aqu� �s perqu� el shrinkedPortal de entryPortalID per el valor de clearance indicat no s'ha calculat mai. 
	
	//1) STEP1: At least, we have to displace both endpoints of the portal a distance clearance towards its center. 
	Edge& entryPortal = _navMesh->getEdge(entryPortalID);
	Vec3D v1EntryPortalOriginal = _navMesh->getVertex(entryPortal.v1).getPosition();	//Extrem inicial 3D del portal d'entrada
	Vec3D v2EntryPortalOriginal = _navMesh->getVertex(entryPortal.v2).getPosition();	//Extrem final 3D del portal d'entrada
	v1Result = v1EntryPortalOriginal + entryPortal._vDirNormalized * clearance;
	v2Result = v2EntryPortalOriginal - entryPortal._vDirNormalized * clearance;

	bool v1Refined = _useCriticalRadius && ((clearance * clearance) <= _criticalRadiiMap[entryPortal.v1]);
	bool v2Refined = _useCriticalRadius && ((clearance * clearance) <= _criticalRadiiMap[entryPortal.v2]);
			
	if (!v1Refined || !v2Refined) {

		//STEP2: Check if the endpoints of the exit portal determines the endpoints of the entry portal
		//Determine if an exitPortal exists
		Vec2D v1EntryPortal2D = GeometrySolver::to2D(v1Result);
		Vec2D v2EntryPortal2D = GeometrySolver::to2D(v2Result);
		int v1NextCellEntryPortalID = shrinkPortalByExitPortalEndpoint(entryPortalID, v1EntryPortal2D, v2EntryPortal2D, true, entryPortal.cellID, path, clearance);
		int v2NextCellEntryPortalID = shrinkPortalByExitPortalEndpoint(entryPortalID, v2EntryPortal2D, v1EntryPortal2D, false, entryPortal.cellID, path, clearance);

		//STEP3: Check if the endpoints are determined by an obstacle edge. 
		//Ogre::LogManager::getSingletonPtr()->logMessage("ShrinkPortalID = " + std::to_string(entryPortal.id));
		//Ogre::LogManager::getSingletonPtr()->logMessage("ExitPortalID = " + std::to_string(exitPortalID));
		//if (!v1Refined) {
			shrinkPortalByCellRec(entryPortal.id, v1EntryPortal2D, v2EntryPortal2D, EM_IN, clearance, v1NextCellEntryPortalID);
			shrinkPortalByCellRec(entryPortal.opposite, v2EntryPortal2D, v1EntryPortal2D, EM_IN, clearance, entryPortal.opposite);
		//}
		//if (!v2Refined) {
			shrinkPortalByCellRec(entryPortal.id, v1EntryPortal2D, v2EntryPortal2D, EM_OUT, clearance, v2NextCellEntryPortalID);
			shrinkPortalByCellRec(entryPortal.opposite, v2EntryPortal2D, v1EntryPortal2D, EM_OUT, clearance, entryPortal.opposite);
		//}
		//Ogre::LogManager::getSingletonPtr()->logMessage("OK!!!");
		//Els c�lculs els he fet en 2D. Cal transformar el resultat a 3D. 
		Vec2D v1EntryPortalOriginal2D = GeometrySolver::to2D(v1EntryPortalOriginal);	//Extrem inicial en 2D del portal d'entrada
		Vec2D v2EntryPortalOriginal2D = GeometrySolver::to2D(v2EntryPortalOriginal);	//Extrem final en 2D del portal d'entrada
		float portalLength2D = v1EntryPortalOriginal2D.dist(v2EntryPortalOriginal2D);	//Longitud del portal en 2D
		float t1 = v1EntryPortalOriginal2D.dist(v1EntryPortal2D) / portalLength2D;
		float t2 = v1EntryPortalOriginal2D.dist(v2EntryPortal2D) / portalLength2D;
		v1Result = v1EntryPortalOriginal + t1 * (v2EntryPortalOriginal - v1EntryPortalOriginal);
		v2Result = v1EntryPortalOriginal + t2 * (v2EntryPortalOriginal - v1EntryPortalOriginal);
		
		if (_useTables) addShrinkedPortal(_shrinkedPortalMap, entryPortalID, clearance, v1Result, v2Result);
	}

	return false;	//Shrinked portal not found
}*/

bool ClearanceSolver::shrinkPortal(int entryPortalID, Path& path, float clearance, NEOGEN::Vec3D& v1Result, NEOGEN::Vec3D& v2Result) {
	if (_shrinkPortalMode == SP_SIMPLE) return shrinkPortalSimple(entryPortalID, clearance, v1Result, v2Result);
	else return shrinkPortalExACT(entryPortalID, path, clearance, v1Result, v2Result);
}

bool ClearanceSolver::shrinkPortalExACT(int entryPortalID, Path& path, float clearance, Vec3D& v1Result, Vec3D& v2Result) {
	//Comprobo si el portal ja existeix
	if (_useTables && getShrinkedPortal(_shrinkedPortalMap, entryPortalID, clearance, v1Result, v2Result)) return true;	//Shrinked portal in table
	
	//Si arribo aqu� �s perqu� el shrinkedPortal de entryPortalID per el valor de clearance indicat no s'ha calculat mai. 

	//1) STEP1: At least, we have to displace both endpoints of the portal a distance clearance towards its center. 

	Edge& entryPortal = _navMesh->getEdge(entryPortalID);
	Vec3D& v1EntryPortal = _navMesh->getVertex(entryPortal.v1).getPosition();	//Extrem inicial 3D del portal d'entrada
	Vec3D& v2EntryPortal = _navMesh->getVertex(entryPortal.v2).getPosition();	//Extrem final 3D del portal d'entrada
	v1Result = v1EntryPortal + entryPortal._vDirNormalized * clearance;
	v2Result = v2EntryPortal - entryPortal._vDirNormalized * clearance;

	bool v1Refined = _useCriticalRadius && ((clearance * clearance) <= _criticalRadiiMap[entryPortal.v1]);
	bool v2Refined = _useCriticalRadius && ((clearance * clearance) <= _criticalRadiiMap[entryPortal.v2]);
	if (!v1Refined || !v2Refined) {

		Vec2D v1EntryPortalOriginal2D = GeometrySolver::to2D(v1EntryPortal);	//Extrem inicial en 2D del portal d'entrada
		Vec2D v2EntryPortalOriginal2D = GeometrySolver::to2D(v2EntryPortal);	//Extrem final en 2D del portal d'entrada
		Vec2D v1EntryPortal2D = GeometrySolver::to2D(v1Result);
		Vec2D v2EntryPortal2D = GeometrySolver::to2D(v2Result);

		//STEP2: Check if the endpoints of the exit portal determines the endpoints of the entry portal
		//Determine if an exitPortal exists
		int exitPortalID;
		int cellID = entryPortal.cellID;
		if (path.isGoalNode(cellID)) exitPortalID = -1;	//The cell of the shrinked portal is the final one in the path, so there is no exit portal
		else exitPortalID = _navMesh->getPortalTo(cellID, path.getNextNode(cellID));	//The portal of cellID used to pass from cellID to the next cell in the path
		
		//Keep the ID of the furthest exit portal that affects the starting endpoint and the final endpoint of the portal that we are shrinking. 
		std::pair<int, int> furthestExitPortal;	
		furthestExitPortal.first = -1;
		furthestExitPortal.second = -1;

		if (exitPortalID != -1) {
			Edge& exitPortal = _navMesh->getEdge(exitPortalID);
			Vec2D v1ExitPortal = GeometrySolver::to2D(_navMesh->getVertex(exitPortal.v1).getPosition());
			Vec2D v2ExitPortal = GeometrySolver::to2D(_navMesh->getVertex(exitPortal.v2).getPosition());
	
			//L'extrem v2 del portal de sortida, pot determinar l'extrem v1 del portal d'entrada
			if (!v1Refined) v1Refined = shrinkPortalByPortalEndpoint(v1EntryPortal2D, v2EntryPortal2D, v2ExitPortal, clearance);

			//L'extrem v1 del portal de sortida, pot determinar l'extrem v2 del portal d'entrada
			if (!v2Refined) v2Refined = shrinkPortalByPortalEndpoint(v2EntryPortal2D, v1EntryPortal2D, v1ExitPortal, clearance);
		}

		//STEP3: Check if the endpoints are determined by an obstacle edge. 
		//Ogre::LogManager::getSingletonPtr()->logMessage("ShrinkPortalID = " + std::to_string(entryPortal.id));
		//Ogre::LogManager::getSingletonPtr()->logMessage("ExitPortalID = " + std::to_string(exitPortalID));
		if (!v1Refined || !v2Refined) {
			shrinkPortalByCellRec(entryPortal.id, v1EntryPortal2D, v2EntryPortal2D, clearance, entryPortal.id);
			shrinkPortalByCellRec(entryPortal.opposite, v2EntryPortal2D, v1EntryPortal2D, clearance, entryPortal.opposite);
		}
		//Ogre::LogManager::getSingletonPtr()->logMessage("OK!!!");
		//Els c�lculs els he fet en 2D. Cal transformar el resultat a 3D. 
		float portalLength2D = v1EntryPortalOriginal2D.dist(v2EntryPortalOriginal2D);	//Longitud del portal en 2D
		float t1 = v1EntryPortalOriginal2D.dist(v1EntryPortal2D) / portalLength2D;
		float t2 = v1EntryPortalOriginal2D.dist(v2EntryPortal2D) / portalLength2D;
		v1Result = v1EntryPortal + t1 * (v2EntryPortal - v1EntryPortal);
		v2Result = v1EntryPortal + t2 * (v2EntryPortal - v1EntryPortal);
		
		if (_useTables) addShrinkedPortal(_shrinkedPortalMap, entryPortalID, clearance, v1Result, v2Result);
	}

	return false;	//Shrinked portal not found
}

void ClearanceSolver::shrinkPortalByCellRec(int shrinkPortalID, NEOGEN::Vec2D& v1Result, NEOGEN::Vec2D& v2Result, float clearance, int entryPortalID) {
	//The idea is to shrink the cell surrounding the portal by displacing its edges a distance of
	//clearance using its normal vector. 
	
	//The edges of the cell are defined in counter-clockwise order (ccw). 
	//So the endpoints fo a portal can be defined as the intersection between 
	//an edge that points to the portal (IN) and the portal, and another edge
	//that points outwards the portal (OUT) and the portal. 
	
	Edge& shrinkPortal = _navMesh->getEdge(shrinkPortalID);
	Edge& entryPortal = _navMesh->getEdge(entryPortalID);

	Vec2D v1Edge, v2Edge;
	SegmentIntersection2D intersection;

	Edge* e = &(_navMesh->getEdge(entryPortal.next));
	while (e->id != entryPortalID) {
		//Nom�s m'interessen les arestes que apuntin cap al portal. �s a dir, les arestes que tenen 
		//un vector director que forma un angle > 90� amb el vector director del portal. 

		//Si el dotProduct dels dos vectors directors �s negatiu, vol dir que els vectors apunten en direccions oposades, 
		//�s a dir, formen un angle major de 90�. Per tant, al despla�ar l'aresta cap a l'interior de la cel�la, pot 
		//ser que produeixi intersecci� amb el portal. 

		v1Edge = _navMesh->getVertex(e->v1).getPosition2D();	//Extrem inicial de l'aresta
		v2Edge = _navMesh->getVertex(e->v2).getPosition2D();	//Extrem final de l'aresta
		if (e->isPortal()) {
			if (_useRecursivity) {
				//Check if portal e determines the endpoints of the portal and do the corresponding recursive call
				if (
					shrinkPortalByPortalEndpoint(v1Result, v2Result, v2Edge, clearance) ||
					shrinkPortalByPortalEndpoint(v2Result, v1Result, v1Edge, clearance)
					)
				{
					//Portal e has modified the endpoint of the portal, and therefore, it might be possible that
					//an edge of the adjacent cell by this portal further refines this endpoint. 
					shrinkPortalByCellRec(shrinkPortalID, v1Result, v2Result, clearance, e->opposite);
				}
			}
		}
		else {
			//e is a solid edge. Displace its endpoints a distance of clearance with the normal vector. 
			//We are only interested in those solid edges that has a normal pointing towards the portal, so
			//in the other case, the displaced edge will not intersect with the portal. 
			if (shrinkPortal._vDirNormalized2D.dotProduct(e->_vDirNormalized2D) < 0.0f) {
				v1Edge = v1Edge + clearance * e->_vNormal2D;
				v2Edge = v2Edge + clearance * e->_vNormal2D;

				//Check if the displaced edge intersects with the portal
				GeometrySolver::segmentSegmentIntersection2D(v1Result, v2Result, v1Edge, v2Edge, intersection);
				if (intersection.p1Valid) {
					//There is an intersection. Update the corresponding endpoint by taking into account
					//if the edge e is an IN edge or an OUT edge with respect to the portal 
					if (shrinkPortal._vNormal2D.dotProduct(e->_vDirNormalized2D) < 0.0f) {
						//e is an IN edge, so v1 must be updated
						v1Result = intersection.p1;
					}
					else {
						//e is an OUT edge, so v2 must be updated
						v2Result = intersection.p1;
					}
				}
			}
		}
		e = &(_navMesh->getEdge(e->next));
	}
}

bool ClearanceSolver::edgeModeMatch(Edge& shrinkPortal, Edge& e, ShrinkEdgeMode shrinkEdgeMode) {
	//Check if we need to take into account this edge, according to its orientation
	if (e.isPortal() || (shrinkEdgeMode == EM_BOTH)) return true;
	
	bool edgeIN = shrinkPortal._vNormal2D.dotProduct(e._vDirNormalized2D) < 0.0f;	//Determines which endpoint should be updated
	return ((edgeIN && (shrinkEdgeMode == EM_IN)) || (!edgeIN && (shrinkEdgeMode == EM_OUT)));
}

bool ClearanceSolver::shrinkPortalByPortalEndpoint(Vec2D& vToShrink, Vec2D& vFixed, Vec2D& vTest, float clearance) {
	SegmentIntersection2D intersection;
	GeometrySolver::segmentCircleIntersection2D(vToShrink, vFixed, vTest, clearance, intersection);
	if (!intersection.p1Valid && !intersection.p2Valid) return false;

	//If we arrive here is because the circunference centered at vTest with radius clearance, intersects
	//with the portal. So vToShrink must be updated. 
	if (intersection.p1Valid && intersection.p2Valid) {
		//La circunfer�ncia intersecciona el portal en 2 punts. Em quedo amb el m�s restrictiu, �s a dir, 
		//amb el que est� m�s lluny de l'extrem original. 
		if (vToShrink.dist2(intersection.p1) > vToShrink.dist2(intersection.p2)) vToShrink = intersection.p1;
		else vToShrink = intersection.p2;
	}
	else if (intersection.p1Valid) {
		//La circunfer�ncia intersecciona el portal en un sol punt v�lid, definit per intersection.p1
		vToShrink = intersection.p1;
	}
	else {
		//La circunfer�ncia intersecciona el portal en un sol punt v�lid, definit per intersection.p2
		vToShrink = intersection.p2;
	}
	return true;
}

void ClearanceSolver::clearShrinkedTables() {
	_shrinkedPortalMap.clear();
	_shrinkedPortalMapSimple.clear();
}

void ClearanceSolver::addShrinkedPortal(ShrinkedPortalMap& sMap, int portalID, float clearance, Vec3D& shrinkedPortalV1, Vec3D& shrinkedPortalV2) {
	//Comprovo si ja existeix l'entrada portalID al mapa. Com que el shrinked portal d'un portal i el seu 
	//oposat �s el mateix, nom�s guardo el valor del shrinkedPortal per un dels dos. Per conveni, la clau �s 
	//l'identificador de portal menor. 
	Edge& portal = _navMesh->getEdge(portalID);
	if (portal.opposite < portalID) portalID = portal.opposite;
	ShrinkedPortalKey key(portalID, clearance);
	sMap[key] = ShrinkedPortal(shrinkedPortalV1, shrinkedPortalV2);
}

bool ClearanceSolver::getShrinkedPortal(ShrinkedPortalMap& sMap, int portalID, float clearance, Vec3D& shrinkedPortalV1, Vec3D& shrinkedPortalV2) {
	//Indica si existeix l'entrada del sMap per al portalID, amb valor de clearance. 
	//En cas d'existir, guarda els extrems del portal a shrinkedPortalV1 i shrinkedPortalV2 respectivament. 
	Edge& portal = _navMesh->getEdge(portalID);
	if (portal.opposite < portalID) portalID = portal.opposite;
	ShrinkedPortalKey key(portalID, clearance);
	//Comprovo si existeix una entra per portalID en el mapa
	ShrinkedPortalMap::iterator itPortalMap = sMap.find(key);
	if (itPortalMap == sMap.end()) return false;	//No existeix cap entrada per portalID

	//Si arribo aqu� �s perqu� existeix una entrada portalID. Comprovo si existeix una entrada pel valor de clearance
	//Com que la clau �s un real, de moment em recorrer� tots els elements del vector un a un.
	shrinkedPortalV1 = itPortalMap->second.first;
	shrinkedPortalV2 = itPortalMap->second.second;
	return true;
}

#pragma endregion
		