#include "NeoEdge.h"
#include "NeoFace.h"
#include "NeoVector3.h"

#include <OgreLogManager.h>
#include <OgreStringConverter.h>

using namespace NEOGEN;

#pragma region CREATION AND DESTRUCTION

NeoEdge::NeoEdge(ElementID id, NeoVertex* v1, NeoVertex* v2) : NeoMeshElement(id) {
	_v1 = v1;
	_v2 = v2;
	_opposite = _parent = NULL;
	_isWall = false;

	_direction = (v1 && v2)? (v2->getPosition() - v1->getPosition()).normalized() : Vector3(0,0,0);
}

NeoEdge::~NeoEdge() {
	if (_opposite) _opposite->setOpposite(NULL);

	_v1->removeIncidentEdge(this);
	_v2->removeIncidentEdge(this);
}

#pragma endregion

#pragma region GET AND SET

NeoVertex* NeoEdge::getVertexBegin() {
	return _v1;
}

NeoVertex* NeoEdge::getVertexEnd() {
	return _v2;
}

NeoEdge* NeoEdge::getOpposite() {
	return _opposite;
}

void NeoEdge::setOpposite(NeoEdge* opposite) {
	_opposite = opposite;
}

NeoEdge* NeoEdge::getParent() {
	return _parent;
}

void NeoEdge::setParent(NeoEdge* parent) {
	_parent = parent;
}

NeoFace* NeoEdge::getFace() {
	return _incidentFaces.empty() ? NULL : *(_incidentFaces.begin());
}

void NeoEdge::addIncidentFace(NeoFace* face) {
	_incidentFaces.insert(face);
}

void NeoEdge::removeIncidentFace(NeoFace* face) {
	_incidentFaces.erase(face);
}

bool NeoEdge::isIncidentFace(NeoFace* face) {
	return _incidentFaces.find(face) != _incidentFaces.end();
}

void NeoEdge::clearIncidentFaces() {
	_incidentFaces.clear();
}

const NeoFacePSet& NeoEdge::getIncidentFaces() {
	return _incidentFaces;
}

Vector3 NeoEdge::getDirection() {
	return _direction;
}

Vector3 NeoEdge::getDirectionInverse() {
	return (-1 * getDirection());
}

void NeoEdge::setTypeWall(bool isWall) {
	_isWall = isWall;
}

bool NeoEdge::isTypeWall() {
	return _isWall;
}

bool NeoEdge::isTypeWalkable() {
	return !isTypeObstacle();
}

bool NeoEdge::isTypeObstacle() {
	if (_isWall || !_opposite) return true;

	//Check if any of the incident faces to this edge is an obstacle face. 
	for (NeoFacePSet::iterator it = _incidentFaces.begin(); it != _incidentFaces.end(); it++) {
		if ((*it)->isTypeObstacle()) return true;
	}

	//Check if any of the incident faces to the opposite of this edge is an obstacle face. 
	const NeoFacePSet& oppositeFaces = _opposite->getIncidentFaces();
	for (NeoFacePSet::const_iterator it = oppositeFaces.begin(); it != oppositeFaces.end(); it++) {
		if ((*it)->isTypeObstacle()) return true;
	}

	return false;
}

bool NeoEdge::isTypeSubPortal() {
	return (isTypeWalkable() && (_parent != NULL));
}

#pragma endregion

#pragma region DEBUG

void NeoEdge::log() {
	Ogre::LogManager* logManager = Ogre::LogManager::getSingletonPtr();
	
	std::string s = "(";
	s += std::to_string(_v1->getPosition().getX()) + ", ";
	s += std::to_string(_v1->getPosition().getY()) + ", ";
	s += std::to_string(_v1->getPosition().getZ());

	s += "), (";

	s += std::to_string(_v2->getPosition().getX()) + ", ";
	s += std::to_string(_v2->getPosition().getY()) + ", ";
	s += std::to_string(_v2->getPosition().getZ());

	s += ")";

	logManager->logMessage("--------------------------------------------------------");
	logManager->logMessage("EDGE " + std::to_string(_id) + " = " + s);
	logManager->logMessage("isWall = " + std::to_string(_isWall));
	logManager->logMessage("opposite = " + (_opposite? std::to_string(_opposite->getID()) : "-1"));
}

#pragma endregion