#include "ngNavMeshGenerator.h"

using namespace NEOGEN;
using namespace GeometrySolver;

/********************************************************************************/
/* CREACIO I DESTRUCCIO*/
/********************************************************************************/
NavMeshGenerator::NavMeshGenerator(Ogre::SceneManager* scMan) : NavMeshGeneratorBase(scMan) {
	floor = NULL;
	navMeshGenerated = false;
	showAdjacencyGraph = true;
	initializeStats();
	timer = new Ogre::Timer();
	timer->reset();
	navMesh = NULL;
	_convexDistance = 0.0f;
}

NavMeshGenerator::~NavMeshGenerator() {
	delete timer;
}

void NavMeshGenerator::createCurrentNotchNode() {
	Ogre::Entity* ent = mSceneMgr->createEntity("Vert.mesh");
	currentNotchNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	currentNotchNode->attachObject(ent);
	currentNotchNode->setScale(vertScale, vertScale, vertScale);
	currentNotchNode->setVisible(false);

	manualInterestArea = createManualObject("manualInterestArea");
	manualInterestArea->setVisible(false);
}

void NavMeshGenerator::createBoard() {
	manualBoard = createManualObject("manualBoard");
	manualBoard->setVisible(false);
	drawBoard();
}

Ogre::ManualObject* NavMeshGenerator::createManualObject() {
	Ogre::ManualObject* manual = mSceneMgr->createManualObject();
	Ogre::SceneNode* node = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	node->attachObject(manual);
	return manual;
}

Ogre::ManualObject* NavMeshGenerator::createManualObject(const Ogre::String& manualName) {
	Ogre::ManualObject* manual;
	if (!mSceneMgr->hasManualObject(manualName)) {
		manual = mSceneMgr->createManualObject(manualName);
		mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(manual);
	}
	else {
		manual = mSceneMgr->getManualObject(manualName);
	}
	return manual;
}

/********************************************************************************/
/* GET I SET */
/********************************************************************************/
void NavMeshGenerator::setConvexDistance(float d) {
	_convexDistance = d;
}

int NavMeshGenerator::getNumPoly() {
	int np = 0;
	if (floor != NULL) np++;
	np += (int)(holes.size());
	return np;
}

bool NavMeshGenerator::getNavMeshGenerated() {
	return navMeshGenerated;
}

int NavMeshGenerator::getInitialVertexs() {
	return initialVertexs;
}

int NavMeshGenerator::getRealNotches() {
	return realNotches;
}

int NavMeshGenerator::getFalseNotches() {
	return falseNotches;
}

int NavMeshGenerator::getSplittedVertexs() {
	return splittedVertexs;
}

int NavMeshGenerator::getNewVertexs() {
	return newVertexs;
}

int NavMeshGenerator::getNumCurrentCells() {
	return numCurrentCells;
}

int NavMeshGenerator::getNumCurrentPortals() {
	return numCurrentPortals;
}

float NavMeshGenerator::getElapsedTimeNotchDetection() {
	return ((float)elapsedTimeNotchDetection / 1000.0f);
}

float NavMeshGenerator::getElapsedTimePortalCreation() {
	return ((float)elapsedTimePortalCreation / 1000.0f);
}

float NavMeshGenerator::getElapsedTimeNonEssentialPortals() {
	return ((float)elapsedTimeNonEssentialPortals / 1000.0f);
}

float NavMeshGenerator::getElapsedTime() {
	return (getElapsedTimeNotchDetection() + getElapsedTimePortalCreation() + getElapsedTimeNonEssentialPortals());
}

void NavMeshGenerator::getStats(int* concV, int* splittedV, int* numCurrentP, int* numCurrentC) {
	*concV = realNotches;
	*splittedV = splittedVertexs;
	*numCurrentP = numCurrentPortals;
	*numCurrentC = numCurrentCells;
}

void NavMeshGenerator::setVertPosition(Ogre::String vertName, Ogre::Vector3& position) {
	bool isFloor;
	int vertID;
	size_t holeID;
	Ogre::String polyID;
	size_t paramBegin = 0;								//Posici� on comen�a el par�metre que estic desencriptant
	size_t dividerPos = vertName.find_first_of('$');	//Posici� del car�cter separador
	vertName.at(dividerPos) = ('@');
	Ogre::String param = vertName.substr(paramBegin, dividerPos - paramBegin);

	//Desencripto a quin pol�gon pertany
	paramBegin = dividerPos + 1;
	dividerPos = vertName.find_first_of('$');
	vertName.at(dividerPos) = ('@');
	param = vertName.substr(paramBegin, dividerPos - paramBegin);
	isFloor = (param == "floor");
	polyID = param;

	//Desencripto l'identificador del v�rtex
	paramBegin = dividerPos + 1;
	dividerPos = vertName.find_first_of('$');
	vertName.at(dividerPos) = ('@');
	param = vertName.substr(paramBegin, dividerPos - paramBegin);
	vertID = (int)(Ogre::StringConverter::parseInt(param));

	if (isFloor) {
		//Es tracta d'un v�rtex d'el terra
		floor->getVertexAt(vertID).getPosition().setCoords(position.x, position.y, position.z);
	}
	else {
		//Es tracta d'un v�rtex d'un forat
		holeID = (size_t)(Ogre::StringConverter::parseInt(polyID.substr(1)));
		Poly* h = holes.at(holeID);
		h->getVertexAt(vertID).getPosition().setCoords(position.x, position.y, position.z);
	}
}

/********************************************************************************/
/* INICIALITZACI� */
/********************************************************************************/
void NavMeshGenerator::initializeStats() {
	generationMode = GENERATION_MODE_STEP_BY_STEP;
	createCurrentNotchNode();
	createBoard();

	initialVertexs = 0;
	realNotches = 0;
	falseNotches = 0;
	splittedVertexs = 0;
	newVertexs = 0;
	numCurrentPortals = 0;
	numNonEssentialPortals = 0;
	numCurrentCells = 0;
	elapsedTimeNotchDetection = 0;
	elapsedTimePortalCreation = 0;
	elapsedTimeNonEssentialPortals = 0;

	redComponent = 0;
	greenComponent = 0;
	blueComponent = 0;
}

void NavMeshGenerator::initializeCandidateEdges(EdgeTable& eTable) {
	candidateEdges.clear();
	for (EdgeTable::iterator it = eTable.begin(); it != eTable.end(); it++) {
		candidateEdges.insert(it->first);
	}
}

void NavMeshGenerator::initializeData() {
	if (_initialized) return;

	//Ogre::LogManager::getSingletonPtr()->logMessage("INITIALIZING DATA");
	//Inicialitza les taules de pol�gons, arestes i v�rtexs amb 
	//la informaci� del terra i dels forats
	//EdgeTable& eTable = navMesh->getEdgeTable();
	showBoard(false);
	initialVertexs = (int)(navMesh->getNumVertices());
	timeBefore = timer->getMilliseconds();
	initializeVertsToSplit();
	timeAfter = timer->getMilliseconds();
	elapsedTimeNotchDetection = timeAfter - timeBefore;
	_initialized = true;
}

void NavMeshGenerator::clearData() {

	_initialized = false;
	candidateEdges.clear();
	polyTable.clear();		//Taula de pol�gons
	portals.clear();

	generationMode = GENERATION_MODE_STEP_BY_STEP;
	createCurrentNotchNode();
	createBoard();

	initialVertexs = 0;
	realNotches = 0;
	falseNotches = 0;
	splittedVertexs = 0;
	newVertexs = 0;
	numCurrentPortals = 0;
	numNonEssentialPortals = 0;
	numCurrentCells = 0;
	elapsedTimeNotchDetection = 0;
	elapsedTimePortalCreation = 0;
	elapsedTimeNonEssentialPortals = 0;
	
	redComponent = 0;
	greenComponent = 0;
	blueComponent = 0;

	navMeshGenerated = false;
}

void NavMeshGenerator::showContour(bool s) {
	if (floor != NULL) floor->setVisible(false);
	std::vector<Poly*>::iterator it;
	for (it = holes.begin(); it != holes.end(); it++) {
		(*it)->setVisible(false);
	}
}

void NavMeshGenerator::initializeVertsToSplit() {
	//Busca v�rtexs concaus en els pol�gons inicialitzats
	vertsToSplit.clear();

	for (size_t i = 0; i < polyTable.size(); i++) { //recorro tots els pol�gons de la geometria
		nmPoly& p = polyTable.at(i);
		std::vector<int> notchString;	//Cont� els identificadors de les ARESTES que formen una cadena de notches
		notchString.clear();
		bool lastWasNotch = false;

		size_t edgeIT = 0;
		size_t numEdges = p.edges.size();

		while (edgeIT < numEdges) {	//recorro totes les arestes de cada pol�gon
			do {
				int idE0 = p.edges.at(edgeIT);
				int idE1 = p.edges.at((edgeIT + 1) % numEdges);
				Edge* e0 = &(navMesh->getEdge(idE0));
				Edge* e1 = &(navMesh->getEdge(idE1));

				if (navMesh->getVertex(e1->v1).isNotch()) {
					//v1 �s un notch. Afegeixo l'aresta e0 a la cadena 
					notchString.push_back(idE0);	
					lastWasNotch = true;					//Indico que el v�rtex actual �s un notch
				}
				else {
					//Si v1 �s un v�rtex convex, comprobo si �s el final d'una cadena de notches. 
					//Si �s aix�, afegeixo l'aresta e0 a la cadena
					if (lastWasNotch) notchString.push_back(idE0);
					lastWasNotch = false;
				}
				edgeIT ++;
			} while (lastWasNotch && (edgeIT < numEdges));
			//tracto la cadena de notches actual
			detectFalseNotches(notchString);
			notchString.clear();
		}
	}
	realNotches = (int)vertsToSplit.size();
	Ogre::LogManager::getSingletonPtr()->logMessage("realNotches = " + std::to_string(vertsToSplit.size()));
	notchIterator = vertsToSplit.begin();
}

void NavMeshGenerator::initializeVertsToSplit(Cell& c) {
	//Busca v�rtexs concaus en els pol�gons inicialitzats
	vertsToSplit.clear();
	std::set<int>& edges = c.edges;	//Les arestes que formen la cel�la
	//Recorro totes les arestes de la cel�la i comprovo si l'extrem inicial �s un notch. 
	int eBeginID;
	for (std::set<int>::iterator it = edges.begin(); it != edges.end(); it++) {
		eBeginID = navMesh->getEdge(*it).v1;
		if (navMesh->computeConcEdges(eBeginID)) {
			vertsToSplit.insert(eBeginID);
		}
	}
	notchIterator = vertsToSplit.begin();
}

void NavMeshGenerator::detectFalseNotches(std::vector<int>& notchString) {
	//Comprobo si el v�rtex inicial de la cadena i el v�rtex final s�n diferents
	
	//El v�rtex inicial de la cadena �s el v�rtex inicial de la primera aresta de la cadena
	if (notchString.size() >= 2) {
		int v1ID = navMesh->getEdge(notchString.at(0)).v1;

		//El v�rtex final de la cadena �s el v�rtex final de l'�ltima aresta de la cadena
		int v2ID = navMesh->getEdge(notchString.at(notchString.size() - 1)).v2;

		if (v1ID == v2ID) {
			//El v�rtex inicial i final de la cadena s�n el mateix. 
			//Per tant, elimino l'�ltima aresta de la llista
			notchString.pop_back();
		}
	}

	if (!notchString.empty()) {
		computeNotchString(notchString);
	}
}

void NavMeshGenerator::computeNotchString(std::vector<int>& notchString) {
	//Determina quins v�rtexs de la cadena notchString s�n realment notches
	//tenint en compte el par�metre _convexDistance
	if (notchString.empty()) return;
	if (notchString.size() == 1) {
		//Cas especial. Nom�s tenim una aresta. 
		//En aquest cas, si els nodes que formen l'aresta estaven marcats com a notches temporals, 
		//els marco com a notches reals. 
		int eBeginID = navMesh->getEdge(notchString.at(0)).v1;		//Identificador del v�rtex inicial de l'aresta
		int eEndID = navMesh->getEdge(notchString.at(0)).v2;		//Identificador del v�rtex final de l'aresta
		if (navMesh->getVertex(eBeginID).isNotch()) addVertToSplit(eBeginID);	//Si el v�rtex �s notch, l'afegeixo a la llista de notches
		if (navMesh->getVertex(eEndID).isNotch()) addVertToSplit(eEndID);		//Si el v�rtex �s notch, l'afegeixo a la llista de notches
	}
	else {
		//Tenim m�s de d'una aresta a la llista
		//El v�rtex inicial de la cadena �s el v�rtex inicial de la primera aresta de la cadena
		int v1ID = navMesh->getEdge(notchString.at(0)).v1;
		Vec2D v1 = navMesh->getVertex(v1ID).getPosition2D(NEOGEN_PROJECTION_PLANE);

		//El v�rtex final de la cadena �s el v�rtex final de l'�ltima aresta de la cadena
		int v2ID = navMesh->getEdge(notchString.at(notchString.size() - 1)).v2;
		Vec2D v2 = navMesh->getVertex(v2ID).getPosition2D(NEOGEN_PROJECTION_PLANE);

		//Comprobo si algun dels extrems �s un notch. 
		//Si �s aix�, l'afegeixo a la llista de v�rtexs a dividir. 
		//addVertToSplit(notchList.at(0));
		//addVertToSplit(notchList.at(notchList.size() - 1));

		//Busco quin �s el notch de la cadena que es troba a m�s dist�ncia del segment(v1, v2)
		Vec2D projection;
		int auxNotchID;
		Vec2D auxNotch;
		float auxDist2;

		int maxNotchID;
		std::vector<int>::iterator maxEdgeIt = notchString.begin();	//Posici� dins la cadena de l'aresta que cont� el notch m�xim
		float maxDist2 = -1.0f;	//dist�ncia^2 a la que est� el maxNotch del segment

		//Recorro totes les arestes de la cadena
		IntersectionPoint2D result;
		for (std::vector<int>::iterator edgeIt = notchString.begin(); edgeIt != (notchString.end() - 1); edgeIt++) {
			auxNotchID = navMesh->getEdge(*edgeIt).v2;
			auxNotch = navMesh->getVertex(auxNotchID).getPosition2D(NEOGEN_PROJECTION_PLANE);

			//Calculo la projecci� del notch sobre el segment (v1, v2)
			GeometrySolver::projectionPointLine2D(auxNotch, v1, v2, result);	
			projection = result.location;

			//Calculo la dist�ncia al segment i actualitzo si cal
			auxDist2 = auxNotch.dist2(result.location);	//dist�ncia^2 entre el punt de projecci� i el notch
			if (auxDist2 > maxDist2) {
				//He trobat un punt m�s lluny� al segment
				maxEdgeIt	= edgeIt;
				maxNotchID	= auxNotchID;
				maxDist2	= auxDist2;
			}
			else break; //Si la cadena comen�a a decr�ixer, ja no tornar� a trobar un altre m�xim
		}

		//Comprobo si el notch m�s lluny� s'ha de mantenir o no. 
		if (maxDist2 > (_convexDistance * _convexDistance)) {
			//El notch m�s lluny� est� a una dist�ncia superior a _convexDistance respecte el segment. 
			//Per tant, �s un notch que no es pot eliminar. El marquem com a notch i subdividim la cadena
			addVertToSplit(maxNotchID);
			
			//Subdivideixo la cadena
			std::vector<int> sub1;	//Subcadena(v1, n)
			std::vector<int> sub2;	//Subcadena(n, v2)
			int notchID;
			bool appendToSub1 = true;
		
			for (std::vector<int>::iterator edgeIt = notchString.begin(); edgeIt != notchString.end(); edgeIt++) {
				if (appendToSub1) sub1.push_back(*edgeIt);
				else sub2.push_back(*edgeIt);
				notchID = navMesh->getEdge(*edgeIt).v2;
				if (notchID == maxNotchID) {
					//Hem trobat el m�xim, per tant, hem completat la primera subcadena. 
					appendToSub1 = false;
				}
			}
			computeNotchString(sub1);	//Crida recursiva amb sub1
			computeNotchString(sub2);	//Crida recursiva amb sub2
		}
		else {
			navMesh->getVertex(maxNotchID).setConcEdge1(-1);
			navMesh->getVertex(maxNotchID).setConcEdge2(-1);
			falseNotches ++;
		}
	}
}

void NavMeshGenerator::addVertToSplit(int idVert) {
	//Afegeix el v�rtex idVert a la llista de v�rtexs a dividir
	if (
		(navMesh->getVertex(idVert).isNotch())
		) {
		//El v�rtex encara no s'ha afegit a la llista de v�rtexs a dividir
		vertsToSplit.insert(idVert);
	}
}

/********************************************************************************/
/* DIBUIXAT */
/********************************************************************************/
void NavMeshGenerator::drawCurrentNotch() {
	if (applicationState != STATE_GENERATE_NAVMESH_LAYER) return;
	Vec3D v = navMesh->getVertex(*notchIterator).getPosition();
	if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XY) currentNotchNode->setPosition(v.getX(), v.getY(), 0.0f);
	else if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XZ) currentNotchNode->setPosition(v.getX(), 0.0f, v.getZ());
	else currentNotchNode->setPosition(0.0f, v.getY(), v.getZ());
	currentNotchNode->setVisible(true);
	float notchScale = vertScale * 1.1f;
	currentNotchNode->setScale(notchScale, notchScale, notchScale);
	drawInterestArea(*notchIterator);
}

void NavMeshGenerator::drawInterestArea(int idVert) {
	//Dibuixa l'�rea d'inter�s d'un determinat notch
		
	Vec2D vRight = vAOIRight;	//Vector E1 incident al notch
	Vec2D vLeft = vAOILeft;		//Vector E2 incident al notch
	
	//Escalo els vectors
	float scale = 50.0f;
	vRight *= scale;
	vLeft *= scale;
	
	//Calculo els punts
	Vertex& v = navMesh->getVertex(idVert);
	Vec2D origin = v.getPosition2D(NEOGEN_PROJECTION_PLANE);
	Vec2D p = origin + vRight;
	Vec2D q = origin + vLeft;

	//Dibuixo el pol�gon
	manualInterestArea->clear();
	manualInterestArea->setVisible(true);
	manualInterestArea->begin("interestArea", Ogre::RenderOperation::OT_LINE_LIST);
	if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XY) {
		manualInterestArea->position(origin.getX(), origin.getY(), 0.0f);
		manualInterestArea->position(p.getX(), p.getY(), 0.0f);
		manualInterestArea->position(origin.getX(), origin.getY(), 0.0f);
		manualInterestArea->position(q.getX(), q.getY(), 0.0f);
	}
	else if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XZ) {
		manualInterestArea->position(origin.getX(), 0.0f, origin.getY());
		manualInterestArea->position(p.getX(), 0.0f, p.getY());
		manualInterestArea->position(origin.getX(), 0.0f, origin.getY());
		manualInterestArea->position(q.getX(), 0.0f, q.getY());
	}
	else {
		manualInterestArea->position(0.0f, origin.getX(), origin.getY());
		manualInterestArea->position(0.0f, p.getX(), p.getY());
		manualInterestArea->position(0.0f, origin.getX(), origin.getY());
		manualInterestArea->position(0.0f, q.getX(), q.getY());
	}
	manualInterestArea->end();
}

void NavMeshGenerator::drawBoard() {
	manualBoard->clear();
	manualBoard->begin("board", Ogre::RenderOperation::OT_LINE_LIST);
	for (int x = -HALF_EDGES_BOARD; x != HALF_EDGES_BOARD; x++) {
		manualBoard->position((float)x, -0.15f, (float)(-HALF_EDGES_BOARD));
		manualBoard->position((float)x, -0.15f, (float)(HALF_EDGES_BOARD));
	}
	for (int z = -HALF_EDGES_BOARD; z != HALF_EDGES_BOARD; z++) {
		manualBoard->position((float)(-HALF_EDGES_BOARD), -0.15f, (float)z);
		manualBoard->position((float)(HALF_EDGES_BOARD), -0.15f, (float)z);
	}
	manualBoard->end();
}

void NavMeshGenerator::showGraph(bool v) {
	//Mostra u oculta el graph d'adjac�ncia
	navMesh->showGraph(v); 
	showAdjacencyGraph = v;
}

void NavMeshGenerator::showBoard(bool s) {
	//Mostra u oculta el board
	manualBoard->setVisible(s);
}

void NavMeshGenerator::drawNavMeshPortals() {
	//Dibuixo els portals
	PortalSet::iterator it;
	for (it = portals.begin(); it != portals.end(); it++) {
		navMesh->drawEdge(*it, Ogre::ColourValue(0.0f, 1.0f, 0.0f));
	}
}

/********************************************************************************/
/* GENERACI� DE LA MALLA */
/********************************************************************************/

void NavMeshGenerator::clearScene() {
	Ogre::LogManager::getSingletonPtr()->logMessage("CLEARING NAVMESH GENERATOR");
	
	//Ogre::LogManager::getSingletonPtr()->logMessage("CLEARING TABLES");
	polyTable.clear();		//Taula de pol�gons
	portals.clear();
	initializeStats();
	navMeshGenerated = false;
}

void NavMeshGenerator::reset() {
	clearScene();	
	setData(floor, holes);
}

/***************************************************************************/
/* GENERACI� DE LA MALLA NAVEGACIONAL */
/***************************************************************************/
void NavMeshGenerator::repairCells(NavMesh* nm) {
	//Particiona la cel�la idCell de la navMesh nm en regions convexes
	Ogre::LogManager::getSingletonPtr()->logMessage("REPARING CELLS!");
	navMesh = nm;
	std::set<int>& cellsToRepair = navMesh->getCellsToRepair();
	Ogre::LogManager::getSingletonPtr()->logMessage("NUM CELLS TO REPAIR = " + std::to_string(cellsToRepair.size()));
	int idCell;
	Cell* c;
	bool cellRepaired = false;
	for (std::set<int>::iterator it = cellsToRepair.begin(); it != cellsToRepair.end(); it++) {
		portals.clear();
		idCell = *it;
		c = &(navMesh->getCell(idCell));
		candidateEdges = c->edges;
		c->voxels.clear();
		Ogre::LogManager::getSingletonPtr()->logMessage("REPARING CELL = " + std::to_string(idCell));
		initializeVertsToSplit(navMesh->getCell(idCell));
		Ogre::LogManager::getSingletonPtr()->logMessage("numVertsToSplit = " + std::to_string(vertsToSplit.size()));
		//generateFull();
		while (!cellRepaired) cellRepaired = generate();
		Ogre::LogManager::getSingletonPtr()->logMessage("REPARED CELL = " + std::to_string(idCell));
		navMesh->eraseCell(idCell);
	}
	navMesh->clearCellsToRepair();
	navMesh = NULL;
	Ogre::LogManager::getSingletonPtr()->logMessage("CELLS REPARED!");
}

bool NavMeshGenerator::generate() {
	//Genera una malla de navegaci� a partir d'un pol�gon que
	//representa el terra i d'una llista de forats que representen
	//els obstacles est�tics que hi ha a l'escenari. 
	if (!_initialized) initializeData();

	std::set<int> test;
	test.insert(1);
	test.insert(2);
	test.insert(3);

	for (std::set<int>::iterator it = test.begin(); it != test.end(); it++) {
		Ogre::LogManager::getSingletonPtr()->logMessage("test = " + std::to_string(*it));
	}

	printVertsToSplit();

	bool finished = (notchIterator == vertsToSplit.end());
	if (!finished) {
		if (generationMode == GENERATION_MODE_STEP_BY_STEP) generateStepByStep();
		else generateFull();
	}
	else {
		//Ja he acabat la generaci� de la malla navegacional
		Ogre::LogManager::getSingletonPtr()->logMessage("PARTITION COMPLETED");
		navMeshGenerated = true;
		navMesh->clearManualEdges();
		manualInterestArea->clear();
		createCPG();
		showContour(false);
		Ogre::LogManager::getSingletonPtr()->logMessage("=================================================");
		Ogre::LogManager::getSingletonPtr()->logMessage("GENERATION RESULTS");
		Ogre::LogManager::getSingletonPtr()->logMessage("=================================================");
		Ogre::LogManager::getSingletonPtr()->logMessage("numNonEssentialPortals = " + std::to_string(numNonEssentialPortals));
		Ogre::LogManager::getSingletonPtr()->logMessage("elapsedTimeNotchDetection = " + std::to_string(getElapsedTimeNotchDetection()));
		Ogre::LogManager::getSingletonPtr()->logMessage("elapsedTimePortalCreation = " + std::to_string(getElapsedTimePortalCreation()));
		Ogre::LogManager::getSingletonPtr()->logMessage("elapsedTimeNonEssentialPortals = " + std::to_string(getElapsedTimeNonEssentialPortals()));
		Ogre::LogManager::getSingletonPtr()->logMessage("TOTAL ELAPSED TIME = " + std::to_string(getElapsedTime()));
	}
	return finished;
}

void NavMeshGenerator::createCPG() {
	deleteNonEssentialPortals();
	NavMeshGeneratorBase::createCPG();
	printData();
}

void NavMeshGenerator::generateFull() {
	//Genera la malla navegacional completament
	while (notchIterator != vertsToSplit.end()) {
		generateStepByStep();
	}
}

void NavMeshGenerator::generateStepByStep() {
	//Genera la malla navegacional pas a pas
	Ogre::LogManager::getSingletonPtr()->logMessage("************************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("CURRENT NOTCH = " + std::to_string(*notchIterator));
	Vec3D v = navMesh->getVertex(*notchIterator).getPosition();
	Ogre::LogManager::getSingletonPtr()->logMessage("x = " + std::to_string(v.getX()));
	Ogre::LogManager::getSingletonPtr()->logMessage("y = " + std::to_string(v.getY()));
	
	//Calculo els vectors directors inicials de l'�rea d'inter�s
	computeAreaOfInterest(*notchIterator, vAOIRight, vAOILeft);
	
	generateStepByStepCPU();
}

void NavMeshGenerator::generateStepByStepCPU() {
	//Calculo l'element m�s proper i creo el tipus de portal adecuat
	if (generationMode == GENERATION_MODE_STEP_BY_STEP) {
		drawCurrentNotch();
	}
	timeBefore = timer->getMilliseconds();
	
	computeClosestElement();	//Calculo quin �s el v�rtex i aresta m�s propers al notch actual
	
	timeAfter = timer->getMilliseconds();
	unsigned long diffTime = timeAfter - timeBefore;
	elapsedTimePortalCreation += diffTime;

	if (generationMode == GENERATION_MODE_STEP_BY_STEP) {
		drawNavMeshPortals();
	}
	
	notchIterator++;
	splittedVertexs ++;
}

void NavMeshGenerator::computeClosestElement() {
	//Calcula el v�rtex i l'aresta m�s propers al v�rtexs v, dins la zona d'inter�s. 

	//Donat un v�rtex v, la zona que ens interessa �s la que queda a l'esquerra
	//de les dues arestes incidents a v. 

	//Ogre::LogManager::getSingletonPtr()->logMessage("COMPUTE CLOSEST ELEMENT");

	Vertex& vi = navMesh->getVertex(*notchIterator); //El notch que hem de dividir

	ClosestElement ce = computeClosestEdge(vi); //calculo l'aresta m�s propera a vi

	createPortal(&vi, &ce);
	//Ogre::LogManager::getSingletonPtr()->logMessage("CLOSEST ELEMENT COMPUTED");
}

void NavMeshGenerator::createPortal(Vertex* vi, ClosestElement* ce) {
	Ogre::LogManager::getSingletonPtr()->logMessage("closestElementType = " + std::to_string(ce->elementType));
	Ogre::LogManager::getSingletonPtr()->logMessage("closestElementID = " + std::to_string(ce->id));
	
	if (ce->id == -1) return; //L'element m�s proper no existeix. 

	if (ce->elementType == ELEMENT_TYPE_VERTEX) {
		//L'element m�s proper �s un v�rtex. 
		portalVertVert(vi, ce->id);
	}
	else {
		//L'element m�s proper �s una aresta (que pot ser tamb� un portal)
		portalVertEdge(vi, ce);
	}
}

NavMeshGenerator::ClosestElement NavMeshGenerator::computeClosestEdge(Vertex& vi) {
	//Calcula quina �s l'aresta m�s propera al notch. 
	//Els punts que ens interessen (candidats) d'una aresta son:

	//1) la projecci� del vertex vi sobre l'aresta
	//2) els extrems de l'aresta
	//3) la intersecci� entre l'aresta i les arestes incidents a vi

	//D'aquests candidats, ens quedarem amb el millor, �s a dir, el que est� m�s
	//a prop del notch i cau dins la seva zona d'inter�s. 

	//Busco l'aresta m�s propera a v dins la zona d'inter�s
	ClosestElement eClosest;		//Aresta actualment m�s propera a v
	eClosest.id = -1;
	eClosest.dist2 = -1.0f;			//Dist�ncia actual de eClosest a v
	ClosestElement bestCandidate;	
	
	for (std::set<int>::iterator it = candidateEdges.begin(); it != candidateEdges.end(); it++) {
		if (!vi.hasIncidentEdge(*it)) { //Evidentment haig de descartar les arestes incidents al v�rtex
			bestCandidate = computeBestCandidateEdge(vi, navMesh->getEdge(*it));	//Calculo el millor candidat de l'aresta actual
			//Si el millor candidat est� m�s a prop que el candidat actual, substitueixo
			if ((eClosest.dist2 < 0.0f) || (eClosest.dist2 > bestCandidate.dist2)) {
				if (bestCandidate.id != -1) eClosest = bestCandidate;
			}
		}
	}
	return eClosest;
}

NavMeshGenerator::ClosestElement NavMeshGenerator::computeBestCandidateEdge(Vertex& vi, Edge& edge) {
	//Calcula el millor candidat de l'aresta edge
	//arestes incidents a vi. Determinen la zona d'inter�s
	Edge& e1 = navMesh->getEdge(vi.getConcEdge1()); 
	Edge& e2 = navMesh->getEdge(vi.getConcEdge2()); 

	Vec3D eiBegin = navMesh->getVertex(edge.v1).getPosition();	//Punt inicial de l'aresta ei	
	Vec3D eiEnd = navMesh->getVertex(edge.v2).getPosition();	//Punt final de l'aresta ei

	Vec2D eiBegin2D = GeometrySolver::to2D(eiBegin, NEOGEN_PROJECTION_PLANE);
	Vec2D eiEnd2D = GeometrySolver::to2D(eiEnd, NEOGEN_PROJECTION_PLANE);
	Vec2D v2D = GeometrySolver::to2D(vi.getPosition(), NEOGEN_PROJECTION_PLANE);
	IntersectionPoint2D iPoint;

	Ray2D ray;
	ray.setOrigin(v2D);

	ClosestElement eTest;
	eTest.elementType = ELEMENT_TYPE_EDGE;
	std::vector<ClosestElement> candidates(3);

	//1� CANDIDAT: Punt m�s proper del v�rtex a l'aresta (projecci� ortogonal o extrems de l'aresta)
	GeometrySolver::projectionPointSegment2D(v2D, eiBegin2D, eiEnd2D, iPoint);
	eTest.id = edge.id;
	eTest.candidate = iPoint.location;
	candidates[0] = eTest;
			
	//2� CANDIDAT: Intersecci� entre l'aresta e1 i l'aresta eAux
	ray.setDirection(vAOIRight);
	GeometrySolver::raySegmentIntersection2D(ray, eiBegin2D, eiEnd2D, iPoint); 
	if (iPoint.isValid) eTest.id = edge.id;
	else eTest.id = -1;
	eTest.candidate = iPoint.location;
	eTest.elementType = ELEMENT_TYPE_EDGE;
	candidates[1] = eTest;
			
	//3� CANDIDAT: Intersecci� entre l'aresta e2 i l'aresta eAux
	ray.setDirection(vAOILeft);
	GeometrySolver::raySegmentIntersection2D(ray, eiBegin2D, eiEnd2D, iPoint); 
	if (iPoint.isValid) eTest.id = edge.id;
	else eTest.id = -1;
	eTest.candidate = iPoint.location;
	eTest.elementType = ELEMENT_TYPE_EDGE;
	candidates[2] = eTest;
			
	//Recorro tots els candidates i em quedo amb el millor
	float daux;
	ClosestElement bestCandidate;		//El millor candidat fins al moment
	bestCandidate.id = -1;
	bestCandidate.dist2 = -1.0f;		//Dist�ncia actual de eClosest a v

	int candidateID = -1;
	for (size_t j = 0; j < candidates.size(); j++) {
		eTest = candidates.at(j);
		if (eTest.id != -1) {															//Descarto els candidats que no cauen entre els extrems de l'aresta
			if (inZone(eTest.candidate,vi.getID(), vAOIRight, vAOILeft)) {					//Descarto els candidats que cauen fora de l'�rea d'inter�s
				daux = v2D.dist2(eTest.candidate);										//d^2 entre el notch i el candidat
				if ((bestCandidate.dist2 < 0.0f) || (bestCandidate.dist2 > daux)) {
					//�s una aresta m�s propera i est� dins la zona d'inter�s
					bestCandidate = eTest;
					bestCandidate.dist2 = daux;
					candidateID = j;
				}
			}
		}
	}
	//Si el candidat est� molt a prop d'un dels dos extrems de l'aresta, 
	//agafo aquest extrem en comptes del punt, per evitar crear nous punts 
	//i tamb� per solucionar errors de precisi� (ex: projecci� coincideix
	//amb un dels extrems, aix� generaria una aresta de tamany 0)
	if ((bestCandidate.id != -1) && (bestCandidate.elementType == ELEMENT_TYPE_EDGE)) {
		int v1ID = navMesh->getEdge(bestCandidate.id).v1;
		int v2ID = navMesh->getEdge(bestCandidate.id).v2;
		Vec2D closestEdgeV1 = navMesh->getVertex(v1ID).getPosition2D(NEOGEN_PROJECTION_PLANE);	//V�rtex inicial de l'aresta m�s propera
		Vec2D closestEdgeV2 = navMesh->getVertex(v2ID).getPosition2D(NEOGEN_PROJECTION_PLANE);	//V�rtex final de l'aresta m�s propera
		
		if (closestEdgeV1.equal(bestCandidate.candidate, 0.01f)) {
			//El candidat est� molt a prop de V1
			bestCandidate.elementType = ELEMENT_TYPE_VERTEX;
			bestCandidate.id = v1ID;	
		}
		else if (closestEdgeV2.equal(bestCandidate.candidate, 0.01f)) {
			//El candidat est� molt a prop de V1
			bestCandidate.elementType = ELEMENT_TYPE_VERTEX;
			bestCandidate.id = v2ID;
		}
	}
	return bestCandidate;
}

void NavMeshGenerator::computeAreaOfInterest(int idVert, Vec2D& vDirRight, Vec2D& vDirLeft) {
	//Calcula els vectors directors que formen l'�rea d'inter�s del v�rtex idVert
	//* idVert => v�rtex del que volem calcular l'�rea d'inter�s
	//* vDirRight => par�metre on guardem el resultat del vector director de l'aresta que delimita l'AOI per la DRETA del v�rtex
	//* vDirLeft => par�metre on guardem el resultat del vector director de l'aresta que delimita l'AOI per la ESQUERRA del v�rtex
	//Ogre::LogManager::getSingletonPtr()->logMessage("COMPUTING AREA OF INTEREST");
	Vertex& v = navMesh->getVertex(idVert);
	//Calculo el vector director de l'aresta e1, tq:
	//* Vec3D FINAL �s idVert
	//* Vec3D INICIAL �s l'altre extrem
	Edge& e1 = navMesh->getEdge(v.getConcEdge1());
	Edge& e2 = navMesh->getEdge(v.getConcEdge2());
	
	Vec2D rightBegin = navMesh->getVertex(e1.v1).getPosition2D(NEOGEN_PROJECTION_PLANE);
	Vec2D rightEnd = navMesh->getVertex(e1.v2).getPosition2D(NEOGEN_PROJECTION_PLANE);
	vDirRight = rightEnd - rightBegin;
	vDirRight.normalize();
	if (e1.v1 == idVert) {
		//L'aresta e1 t� com a extrem INICIAL el v�rtex idVert. 
		vDirRight *= -1;
	}

	Vec2D leftBegin = navMesh->getVertex(e2.v1).getPosition2D(NEOGEN_PROJECTION_PLANE);
	Vec2D leftEnd = navMesh->getVertex(e2.v2).getPosition2D(NEOGEN_PROJECTION_PLANE);
	vDirLeft = leftEnd - leftBegin;
	vDirLeft.normalize();
	if (e2.v1 == idVert) {
		//L'aresta e2 t� com a extrem INICIAL el v�rtex idVert
		vDirLeft *= -1;
	}

	//Roto vRight...
	Ogre::Degree angle(alphaThreshold / 2.0f);
	vDirRight.rotate(angle.valueRadians());

	//Roto vLeft...
	angle = Ogre::Degree(-alphaThreshold / 2.0f);
	vDirLeft.rotate(angle.valueRadians());
	//Ogre::LogManager::getSingletonPtr()->logMessage("AREA OF INTEREST COMPUTED");
}

/*******************************************************************/
/* CREACI� DE PORTALS */
/*******************************************************************/
/***********************/
/* PORTAL VERTEX-ARESTA
/***********************/
void NavMeshGenerator::portalVertEdge(Vertex* vi, ClosestElement* ce) {
	//Crea un portal entre el v�rtex vi i l'aresta ce. 
	//L'aresta pot ser una aresta de la geometria o un portal pr�viament creat. 
	Edge e = navMesh->getEdge(ce->id);
	if (e.isPortal()) {
		portalVertPortal(vi, &e);		//Creo un portal v�rtex-portal. 
	}
	else {
		portalVertGeometryEdge(vi, ce);	//Creo un portal v�rtex-aresta
	}
}

void NavMeshGenerator::portalVertGeometryEdge(Vertex* vi, ClosestElement* ce) {
	//Crea un portal entre un v�rtex i una aresta de la geometria. 
	//Cal dividir l'aresta en 2
	Ogre::LogManager::getSingletonPtr()->logMessage("**************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("CREATING PORTAL VERT-EDGE");
	Ogre::LogManager::getSingletonPtr()->logMessage("**************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("vi->id = " + std::to_string(vi->getID()));
	Ogre::LogManager::getSingletonPtr()->logMessage("ce->id = " + std::to_string(ce->id));
	int idEdge = ce->id;
	Vertex v1 = navMesh->getVertex(navMesh->getEdge(idEdge).v1);
	Vertex v2 = navMesh->getVertex(navMesh->getEdge(idEdge).v2);
	Vec3D eBegin = v1.getPosition();
	Vec3D eEnd = v2.getPosition();
	
	//Els c�lculs s'han fet en 2D. Cal trobar el punt 3D. Dedueixo la coordenada que falta
	Vec2D eBegin2D = GeometrySolver::to2D(eBegin, NEOGEN_PROJECTION_PLANE);
	Vec2D eEnd2D = GeometrySolver::to2D(eEnd, NEOGEN_PROJECTION_PLANE);
	float eDist = eBegin2D.dist(eEnd2D);		//Dist�ncia entre els extrems del segment en 2D
	float pDist = eBegin2D.dist(ce->candidate);	//Dist�ncia entre el punt inicial del segment i el candidat en 2D
	float t = pDist / eDist;					//Relaci� entre les dist�ncies
	Vec3D closestCandidate3D = eBegin + t*(eEnd - eBegin);
	Vec3D v(closestCandidate3D.getX(), closestCandidate3D.getY(), closestCandidate3D.getZ());
	//Comprovo si �s un v�rtex que est� sobre una aresta de portal entre layers
	bool shared = v1.isShared() || v2.isShared();
	Ogre::LogManager::getSingletonPtr()->logMessage("isShared = " + std::to_string(shared));
	int idNewVert = navMesh->addVertex(v, shared);
	newVertexs ++;
	Ogre::LogManager::getSingletonPtr()->logMessage("NEW VERTEX CREATED! = " + std::to_string(idNewVert));

	//subdivideixo l'aresta
	std::pair<int, int> subEdges = navMesh->subdivideEdge(ce->id, idNewVert);
	candidateEdges.erase(ce->id);
	candidateEdges.insert(subEdges.first);
	candidateEdges.insert(subEdges.second);

	//Creo el portal
	int idPortal = addSubEdge(-1, vi->getID(), idNewVert, Edge::PORTAL);
	portals.insert(idPortal);

	numCurrentPortals ++;
	//Ogre::LogManager::getSingletonPtr()->logMessage("PORTAL VERT-EDGE CREATED! " + std::to_string(idPortal));
}

/***********************/
/* PORTAL VERTEX-PORTAL
/***********************/
void NavMeshGenerator::portalVertPortal(Vertex* vi, Edge* e) {
	portalVertPortalCPU(vi, e);
}

void NavMeshGenerator::portalVertPortalCPU(Vertex* vi, Edge* e) {
	//Crea un portal entre un v�rtex i un portal

	/*Ogre::LogManager::getSingletonPtr()->logMessage("**************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("CREATING PORTAL VERT-PORTAL (CPU)");
	Ogre::LogManager::getSingletonPtr()->logMessage("**************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("vi->id = " + std::to_string(vi->id));
	Ogre::LogManager::getSingletonPtr()->logMessage("e->id = " + std::to_string(e->id));*/
	//L'aresta m�s propera �s un portal. Cal "partir-lo". Comprobo 
	//quants portals haig de generar
	Vec2D eBegin2D = navMesh->getVertex(e->v1).getPosition2D(NEOGEN_PROJECTION_PLANE);
	Vec2D eEnd2D = navMesh->getVertex(e->v2).getPosition2D(NEOGEN_PROJECTION_PLANE);

	bool testBegin = inZone(eBegin2D, vi->getID(), vAOIRight, vAOILeft);	//Punt inicial del portal en la zona d'inter�s del notch?
	bool testEnd = inZone(eEnd2D, vi->getID(), vAOIRight, vAOILeft);		//Punt final del portal en la zona d'inter�s del notch?

	if (!testBegin && !testEnd) {
		//Els dos extrems del portal cauen FORA de la zona d'inter�s
		//del v�rtex que volem dividir. Per tant, ELS DOS portals 
		//s�n imprescindibles
		
		//Ogre::LogManager::getSingletonPtr()->logMessage("INITIAL AND FINAL POINTS ARE OUTSIDE THE AREA OF INTEREST");
		//Comprovo si �s segur crear un portal entre el notch i l'extrem INICIAL del portal
		portalVertPortalEndPointCPU(vi, e->id, e->v1);

		//Comprovo si �s segur crear un portal entre el notch i l'extrem FINAL del portal
		portalVertPortalEndPointCPU(vi, e->id, e->v2);
	}
	else if (testBegin) {
		//L'extrem inicial del portal cau DINS de la zona d'inter�s
		//del v�rtex que volem dividir. Per tant, nom�s �s imprescindible
		//el portal format entre el v�rtex que volem dividir i l'extrem
		//inicial del portal amb el que hem interseccionat. 

		//Ogre::LogManager::getSingletonPtr()->logMessage("INITIAL POINT IS INSIDE THE AREA OF INTEREST");
		//Comprovo si �s segur crear un portal entre el notch i l'extrem INICIAL del portal
		portalVertPortalEndPointCPU(vi, e->id, e->v1);
	}
	else if (testEnd) {
		//L'extrem final del portal cau DINS de la zona d'inter�s
		//del v�rtex que volem dividir. Per tant, nom�s �s imprescindible
		//el portal format entre el v�rtex que volem dividir i l'extrem
		//final del portal amb el que hem interseccionat. 
		
		//Ogre::LogManager::getSingletonPtr()->logMessage("FINAL POINT IS INSIDE THE AREA OF INTEREST");
		//Comprovo si �s segur crear un portal entre el notch i l'extrem FINAL del portal
		portalVertPortalEndPointCPU(vi, e->id, e->v2);
	}
	if (navMesh->isNonEssentialPortal(e->id)) {
		erasePortalPointer(e->id);
	}
	//Ogre::LogManager::getSingletonPtr()->logMessage("PORTAL VERT-PORTAL CREATED!");
}

void NavMeshGenerator::portalVertPortalEndPointCPU(Vertex* vi, int portalID, int endPointID) {
	//Crea un portal entre el v�rtex vi i un punt que �s un extrem d'un portal

	//Comprovo si �s segur crear un portal entre el notch i l'extrem final de l'aresta
	Vec2D portalEnd = navMesh->getVertex(endPointID).getPosition2D(NEOGEN_PROJECTION_PLANE);
	Vec2D v2D = vi->getPosition2D(NEOGEN_PROJECTION_PLANE);
	float d2portalEnd = portalEnd.dist2(v2D);	//dist�ncia^2 entre el notch i l'extrem del portal

	ClosestElement cIntersection = computeClosestIntersectionCPU(v2D, portalEnd);
	bool isSafe =	(
					(cIntersection.id == -1) || 
					(cIntersection.dist2 > d2portalEnd) ||
					((cIntersection.elementType == ELEMENT_TYPE_VERTEX) && (cIntersection.id == endPointID))
					);
	if (isSafe) {
		//Ogre::LogManager::getSingletonPtr()->logMessage("END POINT IS SAFE (CPU)");
		cIntersection.elementType = ELEMENT_TYPE_VERTEX;
		cIntersection.id = endPointID;
	}
	else {
		//Ogre::LogManager::getSingletonPtr()->logMessage("END POINT IS NOT SAFE (CPU)");
	}
	createPortal(vi, &cIntersection);
}

NavMeshGenerator::ClosestElement NavMeshGenerator::computeClosestIntersectionCPU(Vec2D& initPoint, Vec2D& endPoint) {
	//Calcula quina �s la intersecci� m�s propera entre la recta definida pel vector
	//vDir i un punt de la recta p, i la resta d'arestes

	Edge* edge;
	Vec2D beginEdge;	//punt inicial de l'aresta
	Vec2D endEdge;	//punt final de l'aresta

	ClosestElement closestIntersection;
	closestIntersection.id = -1;
	closestIntersection.elementType = ELEMENT_TYPE_EDGE;
	closestIntersection.dist2 = -1.0f;
	SegmentIntersection2D intersection;	//Punt d'intersecci� entre l'aresta i i la recta
	float d2aux;
	
	//Recorro totes les arestes
	for (std::set<int>::iterator it = candidateEdges.begin(); it != candidateEdges.end(); it++) {
		edge = &(navMesh->getEdge(*it));				//L'aresta
		beginEdge = navMesh->getVertex(edge->v1).getPosition2D(NEOGEN_PROJECTION_PLANE);	//L'extrem inicial de l'aresta
		endEdge = navMesh->getVertex(edge->v2).getPosition2D(NEOGEN_PROJECTION_PLANE);		//L'extrem final de l'aresta

		if (navMesh->getVertex(*notchIterator).hasIncidentEdge(edge->id)) continue;	//Descarto les arestes incidents al notch
		
		GeometrySolver::segmentSegmentIntersection2D(initPoint, endPoint, beginEdge, endEdge, intersection);
		if (intersection.p1Valid) {
			//Hi ha intersecci� entre la recta (vDir, p) i l'aresta
			//Cal comprobar si �s una intersecci� m�s propera
			Ogre::LogManager::getSingletonPtr()->logMessage("SEGMENT SEGMENT INTERSECTION");
			d2aux = intersection.p1.dist2(initPoint);
			if ((closestIntersection.dist2 < 0.0f) || (closestIntersection.dist2 > d2aux)) {
				//La intersecci� calculada �s m�s propera
				closestIntersection.id = edge->id;
				closestIntersection.candidate = intersection.p1;
				closestIntersection.dist2 = d2aux;
			}
		}
	}
	//Si la intersecci� est� molt a prop d'un dels dos extrems de l'aresta, 
	//agafo aquest extrem en comptes del punt
	if ((closestIntersection.id != -1) && (closestIntersection.elementType == ELEMENT_TYPE_EDGE)) {
		int v1ID = navMesh->getEdge(closestIntersection.id).v1;	//Extrem inicial de l'aresta m�s propera
		int v2ID = navMesh->getEdge(closestIntersection.id).v2;	//Extrem final de l'aresta m�s propera
		Vec2D closestEdgeV1 = navMesh->getVertex(v1ID).getPosition2D(NEOGEN_PROJECTION_PLANE);	//V�rtex inicial de l'aresta m�s propera
		Vec2D closestEdgeV2 = navMesh->getVertex(v2ID).getPosition2D(NEOGEN_PROJECTION_PLANE);	//V�rtex final de l'aresta m�s propera
		
		if (closestEdgeV1.equal(closestIntersection.candidate, 0.01f)) {
			//El candidat est� molt a prop de V1
			closestIntersection.elementType = ELEMENT_TYPE_VERTEX;
			closestIntersection.id = v1ID;
		}
		else if (closestEdgeV2.equal(closestIntersection.candidate, 0.01f)) {
			//El candidat est� molt a prop de V2
			closestIntersection.elementType = ELEMENT_TYPE_VERTEX;
			closestIntersection.id = v2ID;
		}
	}
	return closestIntersection;
}

void NavMeshGenerator::erasePortalPointer(int idEdge) {
	//elimina l'aresta idEdge de la taula de punters del v�rtex idVert
	navMesh->eraseEdge(idEdge);
	candidateEdges.erase(idEdge);
	portals.erase(idEdge);
	numCurrentPortals --;
	numNonEssentialPortals ++;
}

/***********************/
/* PORTAL VERTEX-VERTEX
/***********************/
void NavMeshGenerator::portalVertVert(Vertex* vi, int closestVertID) {
	//crea un portal entre 2 v�rtexs
	/*Ogre::LogManager::getSingletonPtr()->logMessage("**************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("CREATING PORTAL VERT-VERT");
	Ogre::LogManager::getSingletonPtr()->logMessage("**************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("Vi = " + std::to_string(vi->id));
	Ogre::LogManager::getSingletonPtr()->logMessage("closestVert = " + std::to_string(closestVertID));*/
	//Comprobo si el cv �s tamb� un v�rtex a dividir
	NotchSet::iterator it = vertsToSplit.find(closestVertID);
	if (it != vertsToSplit.end()) {
		//El v�rtex m�s proper �s un v�rtex que tamb� s'ha de dividir. Cal 
		//comprobar si creant aquest portal, el v�rtex m�s proper tamb�
		//queda ja dividit. 
		Vec2D v2D, vClosestRight, vClosestLeft;
		computeAreaOfInterest(closestVertID, vClosestRight, vClosestLeft);
		v2D = vi->getPosition2D(NEOGEN_PROJECTION_PLANE);
		if (inZone(v2D, closestVertID, vClosestRight, vClosestLeft)) {
			//El v�rtex vi cau dins la zona d'inter�s del v�rtex cv. Aix� vol
			//dir que al crear el portal (ci,cv), el v�rtex cv tamb� queda
			//dividit. Per tant, puc eliminar cv de la llista de v�rtexs a 
			//dividir. 
			vertsToSplit.erase(it);
		}
	}
	int idPortal = addSubEdge(-1, vi->getID(), closestVertID, Edge::PORTAL);
	//Ogre::LogManager::getSingletonPtr()->logMessage("portalID = " + std::to_string(idPortal));
	portals.insert(idPortal);
	numCurrentPortals ++;
	//Ogre::LogManager::getSingletonPtr()->logMessage("PORTAL VERT-VERT CREATED! " + std::to_string(idPortal));
}

/**************************************************************************************/
/* NONESSENTIAL PORTAL DELETION */
/**************************************************************************************/
void NavMeshGenerator::deleteNonEssentialPortals() {
	//Elimino els portals que s�n prescindibles
	timeBefore = timer->getMilliseconds();
	PortalSet::iterator it = portals.begin();
	PortalSet::iterator prev;
	while (it != portals.end()) {
		prev = it++;
		if (navMesh->isNonEssentialPortal(*prev)) {
			//El portal �s no essencial. Cal treure'l de la llista
			erasePortalPointer(*prev);
		}
	}
	timeAfter = timer->getMilliseconds();
	elapsedTimeNonEssentialPortals = timeAfter - timeBefore;
}

/**************************************************************************************/
/* CELL RECONSTRUCTION */
/**************************************************************************************/


void NavMeshGenerator::cellVoxelDescription(Cell& c, std::set<Voxel*>& voxels) {
	Ogre::LogManager::getSingletonPtr()->logMessage("COMPUTING CELL VOXEL DESCRIPTION");
	Ogre::LogManager::getSingletonPtr()->logMessage("cellID = " + std::to_string(c.id));
	//Determina quins v�xels de la llista aVoxels interseccionen amb la cel�la c. 
	//La idea �s comen�ar per un v�xel que sabem segur que intersecciona amb 
	//la cel�la i continuar amb un proc�s de flooding fins que ja no es 
	//detecten m�s interseccions
	Vec3D initialVert = navMesh->getVertex(navMesh->getEdge(*(c.edges.begin())).v1).getPosition();
	Ogre::LogManager::getSingletonPtr()->logMessage("INITIAL VERT COORDS");
	Ogre::LogManager::getSingletonPtr()->logMessage("x = " + std::to_string(initialVert.getX()));
	Ogre::LogManager::getSingletonPtr()->logMessage("y = " + std::to_string(initialVert.getY()));
	Ogre::LogManager::getSingletonPtr()->logMessage("z = " + std::to_string(initialVert.getZ()));
	Ogre::Vector3 vPos; 
	if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XY) vPos = Ogre::Vector3(initialVert.getX(), initialVert.getZ() - CELL_OFFSET, initialVert.getY());
	else if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XZ) vPos = Ogre::Vector3(initialVert.getX(), initialVert.getY() - CELL_OFFSET, initialVert.getZ());
	else vPos = Ogre::Vector3(initialVert.getY(), initialVert.getZ() - CELL_OFFSET, initialVert.getX());
	VoxelGrid& vGrid = VoxelGrid::getSingleton();
	Voxel* v = vGrid.getVoxelFromPos(vPos); //El v�xel on cau aquest punt. 
	//initialVert.draw();
	//v->draw("matVoxelNegative");
	if (voxels.find(v) != voxels.end()) Ogre::LogManager::getSingletonPtr()->logMessage("SAFE STARTING VOXEL!");
	else {
		Ogre::LogManager::getSingletonPtr()->logMessage("BAD STARTING VOXEL!");
		v = vGrid.getVoxel(v->keyX, 0, v->keyZ);
		while (voxels.find(v) == voxels.end()) v = vGrid.getVoxel(v->keyX, v->keyY + 1, v->keyZ);
	}
	
	//v->draw("matVoxelNegative");
	//Faig un proc�s de flooding a partir del v�xel inicial per detectar tots els v�xels que
	//formen la cel�la
	std::vector<Voxel*> voxelList;
	std::set<Voxel*> visitedVoxels;
	std::set<Voxel*>::iterator itFound;
	voxelList.push_back(v);
	visitedVoxels.insert(v);
	Voxel* vNeighbor;
	
	while (!voxelList.empty()) {
		v = voxelList.back();
		voxelList.pop_back();
		if (projectedIntersection(v, *(c.p))) {
			//El v�xel actual t� una intersecci� amb la cel�la c. Per tant, ho indico i 
			//intento apilar els seus ve�ns
			v->addCell(c.id);
			c.voxels.insert(VoxelPointer(v->keyX, v->keyY, v->keyZ));
			for (int x = -1; x <= 1; x++) {
				for (int y = -1; y <= 1; y++) {
					for (int z = -1; z <= 1; z++) {
						if ((x == 0) && (y == 0) && (z == 0)) continue;
						vNeighbor = vGrid.getVoxel(v->keyX + x, v->keyY + y, v->keyZ + z);
						itFound = visitedVoxels.find(vNeighbor);
						if (
							(visitedVoxels.find(vNeighbor) == visitedVoxels.end()) &&	//vNeighbor no ha estat visitat encara
							(voxels.find(vNeighbor) != voxels.end())					//vNeighbor �s un v�xel que pertany a la llista de candidats
							)
						{
							voxelList.push_back(vNeighbor);
							visitedVoxels.insert(vNeighbor);
						}
					}
				}
			}
		}
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("numVoxelsCell = " + std::to_string(c.voxels.size()));
	Ogre::LogManager::getSingletonPtr()->logMessage("CELL VOXEL DESCRIPTION COMPUTED");
}

bool NavMeshGenerator::projectedIntersection(Voxel* v, Poly& cProjected) {
	//Calcula si el v�xel v i el pol�gon cProjected interseccionen. 
	//cProjected representa la projecci� sobre el pl� XZ d'una cel�la
	//si hi ha intersecci�
	std::vector<Vertex> lVert;
	//1) Construeixo la projecci� del v�xel sobre el pl�
	Ogre::Vector3& vMax = v->getMaximum();
	Ogre::Vector3& vMin = v->getMinimum();
	if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XY) {
		lVert.push_back(Vertex(Vec3D(vMin.x, vMin.z, 0.0f)));
		lVert.push_back(Vertex(Vec3D(vMin.x, vMax.z, 0.0f)));
		lVert.push_back(Vertex(Vec3D(vMax.x, vMax.z, 0.0f)));
		lVert.push_back(Vertex(Vec3D(vMax.x, vMin.z, 0.0f)));
	}
	else if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XZ) {
		lVert.push_back(Vertex(Vec3D(vMin.x, 0.0f, vMin.z)));
		lVert.push_back(Vertex(Vec3D(vMin.x, 0.0f, vMax.z)));
		lVert.push_back(Vertex(Vec3D(vMax.x, 0.0f, vMax.z)));
		lVert.push_back(Vertex(Vec3D(vMax.x, 0.0f, vMin.z)));
	}
	else {
		lVert.push_back(Vertex(Vec3D(0.0f, vMin.x, vMin.z)));
		lVert.push_back(Vertex(Vec3D(0.0f, vMin.x, vMax.z)));
		lVert.push_back(Vertex(Vec3D(0.0f, vMax.x, vMax.z)));
		lVert.push_back(Vertex(Vec3D(0.0f, vMax.x, vMin.z)));
	}
	Poly vProjected(lVert);

	//2) Calcular intersecci�
	return GeometrySolver::polygonIntersection(vProjected, cProjected, NEOGEN_PROJECTION_PLANE);
}

/****************************************************************************/
/* OPERACIONS GEOM�TRIQUES */
/****************************************************************************/
bool NavMeshGenerator::inZone(Vec2D& a, int b, Vec2D& vRight, Vec2D& vLeft) {
	//indica si A est� dins la zona d'inter�s de B
	
	Vec2D vNotch = navMesh->getVertex(b).getPosition2D(NEOGEN_PROJECTION_PLANE);
	Vec2D vDirA = (a - vNotch);	//Vector director amb inici vNotch i final a
	
	//Calculo l'angle de l'AOI
	//L'angle de l'AOI �s el m�nim del gir cap a la dreta de v1 a v2, 
	//o el gir cap a la dreta de v2 cap a v1
	float angle1 = GeometrySolver::angleBetweenVectors(vRight, vLeft);
	float angle2 = GeometrySolver::angleBetweenVectors(vLeft, vRight);
	float angleAOI = std::min(angle1, angle2); 

	//Comprobo si a est� dins la zona d'inter�s de b
	float angleVDirA = GeometrySolver::angleBetweenVectors(vRight, vDirA);
	return ((angleVDirA <= angleAOI) || (RealMath::equal(angleVDirA, angleAOI, 0.01f)));
}

/********************************************************************************/
/* INFORMACI� I DIBUIXAT */
/********************************************************************************/
void NavMeshGenerator::printPolyTable() {
	struct nmPoly nmP;
	Ogre::LogManager::getSingletonPtr()->logMessage("*** POLY TABLE INFO ***");
	Ogre::LogManager::getSingletonPtr()->logMessage("=======================");
	
	for (size_t i = 0; i < polyTable.size(); i++) {
		nmP = polyTable.at(i);
		Ogre::LogManager::getSingletonPtr()->logMessage("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-");
		Ogre::LogManager::getSingletonPtr()->logMessage("Polygon id = " + std::to_string((int)(nmP.id)));
		Ogre::LogManager::getSingletonPtr()->logMessage("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-");

		Ogre::LogManager::getSingletonPtr()->logMessage("Edges conforming polygon");
		for (size_t j = 0; j < nmP.edges.size(); j++) {
			Ogre::LogManager::getSingletonPtr()->logMessage(std::to_string((int)(nmP.edges.at(j))));						
		}
	}
}

void NavMeshGenerator::printVertsToSplit() {
	Ogre::LogManager::getSingletonPtr()->logMessage("*** VERTEX TO SPLIT ***");
	Ogre::LogManager::getSingletonPtr()->logMessage("=======================");

	NotchSet::iterator it = vertsToSplit.begin();
	while (it != vertsToSplit.end()) {
		Vertex& nmV = navMesh->getVertex(*it);

		Ogre::LogManager::getSingletonPtr()->logMessage("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-");
		Ogre::LogManager::getSingletonPtr()->logMessage("Vertex id = " + std::to_string((int)(nmV.getID())));
		Ogre::LogManager::getSingletonPtr()->logMessage("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-");
		Vec3D v = nmV.getPosition();
		Ogre::LogManager::getSingletonPtr()->logMessage("x = " + std::to_string(v.getX()));
		Ogre::LogManager::getSingletonPtr()->logMessage("y = " + std::to_string(v.getY()));

		advance(it, 1);
	}
}

void NavMeshGenerator::printPortals() {
	Edge* nmE;
	Ogre::LogManager::getSingletonPtr()->logMessage("*** PORTALS INFO ***");
	Ogre::LogManager::getSingletonPtr()->logMessage("====================");
	
	PortalSet::iterator it;
	for (it = portals.begin(); it != portals.end(); it++) {
		nmE = &(navMesh->getEdge(*it));
		Ogre::LogManager::getSingletonPtr()->logMessage("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-");
		Ogre::LogManager::getSingletonPtr()->logMessage("Portal id = " + std::to_string(nmE->id));
		Ogre::LogManager::getSingletonPtr()->logMessage("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-");
		Ogre::LogManager::getSingletonPtr()->logMessage("v1 = " + std::to_string(nmE->v1));
		Ogre::LogManager::getSingletonPtr()->logMessage("v2 = " + std::to_string(nmE->v2));

	}
}

void NavMeshGenerator::printData() {
	Ogre::LogManager::getSingletonPtr()->logMessage("*****************************************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("NavMeshGenerator data BEGIN");
	Ogre::LogManager::getSingletonPtr()->logMessage("*****************************************************************");
	navMesh->printVertTable();
	navMesh->printEdgeTable();
	printPolyTable();
	printVertsToSplit();
	printPortals();
	navMesh->printCellTable();
	Ogre::LogManager::getSingletonPtr()->logMessage("*****************************************************************");
	Ogre::LogManager::getSingletonPtr()->logMessage("NavMeshGenerator data END");
	Ogre::LogManager::getSingletonPtr()->logMessage("*****************************************************************");
}

/**********************************************************************************************/
/* GUARDAR I CARREGAR INFORMACI� */
/**********************************************************************************************/
void NavMeshGenerator::saveMapData(std::string fileName) {
	std::vector<Poly*> data;
	data.push_back(floor);
	std::vector<Poly*>::iterator it;
	for (it = holes.begin(); it != holes.end(); it++) {
		data.push_back(*it);
	}
	MapParser::getSingleton().savePolyMapData(&data, fileName);
}

void NavMeshGenerator::loadMapData(std::string fileName) {
	Ogre::LogManager::getSingletonPtr()->logMessage("LOADING MAP");
	navMesh = new NavMesh();
	std::vector<Poly*> data;
	MapParser::getSingleton().loadPolyMapData(&data, fileName);
	if (data.size() > 0) {
		setFloor(data.at(0));
	}
	holes.clear();
	std::vector<Poly*>::iterator it;
	for (it = (data.begin() + 1); it != data.end(); it++) {
		addHole(*it);
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("MAP LOADED!");
}