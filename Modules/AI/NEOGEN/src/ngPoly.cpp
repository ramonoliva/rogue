#include "ngPoly.h"
#include "ngGlobalVars.h"

using namespace NEOGEN;

/*******************************************************************************/
/* CREACIO I DESTRUCCIO */
/*******************************************************************************/

Poly::Poly() {
	polyName = "";
	manualObject = NULL;
	mSceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
	_validCenter = false;
}

Poly::Poly(VertexList& l, bool needsTriangulation) {
	setVerts(l);
	polyName = "";
	if (needsTriangulation) triangulate();
	manualObject = NULL;
	mSceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
	float cX, cY, cZ;
	getCenterCoords(cX, cY, cZ);
	_center.setCoords(cX, cY, cZ);
	_validCenter = true;
}

Poly::~Poly() {
	lVert.clear();
	if (manualObject) {
		mSceneMgr->destroyManualObject(manualObject);
	}
	manualObject = NULL;
	mSceneMgr = NULL;
}

void Poly::clearManualObject() {
	if (manualObject != NULL) manualObject->clear();
}

/*******************************************************************************/
/* GET I SET */
/*******************************************************************************/
VertexList& Poly::getVertexList() {
	return lVert;
}

void Poly::setVisible(bool v) {
	if (manualObject) {
		manualObject->setVisible(v);
	}
}

void Poly::setPolyName(Ogre::String name) {
	polyName = name;
}

Ogre::String Poly::getPolyName() {
	return polyName;
}

void Poly::getCenter(Vec3D& result) {
	if (_validCenter) result.setCoords(_center.getX(), _center.getY(), _center.getZ());
	else {
		float cX, cY, cZ;
		getCenterCoords(cX, cY, cZ);
		result.setCoords(cX, cY, cZ);
	}
}

void Poly::getCenterCoords(float& cX, float& cY, float& cZ) {
	/*Calcula quin �s el centre del pol�gon*/

	unsigned int numVerts = (unsigned int)(lVert.size());
	
	float x = 0.0;
	float y = 0.0;
	float z = 0.0;
	
	for (unsigned int i = 0; i < numVerts; i++) {
		//sumo les coordenades de tots els vectors
		Vec3D v = lVert.at(i).getPosition();
		x += v.getX();
		y += v.getY();
		z += v.getZ();
	}
	//faig el promig
	x /= (float)numVerts;
	y /= (float)numVerts;
	z /= (float)numVerts;

	cX = x; cY = y; cZ = z;
}

unsigned int Poly::getNumVert() {
	//Retorna el nombre de v�rtexs que t� un pol�gon
	unsigned int n = (unsigned int)(lVert.size());
	return n;
}

Vertex& Poly::getVertexAt(size_t idVert) {
	//retorna un vertex concret del pol�gon
	return lVert.at(idVert);
}

void Poly::setVerts(VertexList& l) {
	lVert = l;
}

void Poly::reverse() {
	//Capgira l'ordre dels v�rtexs
	VertexList initialVerts = lVert;
	lVert.clear();
	for (int i = (initialVerts.size() - 1); i >= 0; i--) {
		lVert.push_back(initialVerts[i]);
	}
}

/*******************************************************************************/
/* ALTRES */
/*******************************************************************************/

void Poly::draw(const Ogre::String& materialName, bool solid) {

	/* Dibuixa un pol�gon */
	if (manualObject == NULL) {
		//Encara no s'ha creat el manual object. El creo. 
		manualObject = mSceneMgr->createManualObject();
		mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(manualObject);
	}
	manualObject->clear();
	if (solid) drawSolid(materialName);
	else drawWireFrame(materialName);
}

void Poly::drawSolid(const Ogre::String& materialName) {
	manualObject->begin(materialName, Ogre::RenderOperation::OT_TRIANGLE_LIST);

	int tcount = lTriangle.size()/3;
	Vec3D v1;
	Vec3D v2;
	Vec3D v3;

	for (int i=0; i<tcount; i++)	{
		v1 = lVert.at(lTriangle[i*3+0]).getPosition();
		v2 = lVert.at(lTriangle[i*3+1]).getPosition();
		v3 = lVert.at(lTriangle[i*3+2]).getPosition();
		if (applicationMode == APPLICATION_MODE_2D) {
			if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XY) {v1.setZ(0.0f); v2.setZ(0.0f); v3.setZ(0.0f);}
			else if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XZ) {v1.setY(0.0f); v2.setY(0.0f); v3.setY(0.0f);}
			else {v1.setX(0.0f); v2.setX(0.0f); v3.setX(0.0f);}
		}
		manualObject->position(v1.getX(), v1.getY(), v1.getZ());
		manualObject->position(v2.getX(), v2.getY(), v2.getZ());
		manualObject->position(v3.getX(), v3.getY(), v3.getZ());
	}
	manualObject->end();
}

void Poly::drawWireFrame(const Ogre::String& materialName) {
	Ogre::Vector3 pos;
	manualObject->begin(materialName, Ogre::RenderOperation::OT_LINE_STRIP);
	
	size_t numVerts = lVert.size();
	size_t vertID;
	for (size_t i = 0; i <= lVert.size(); i++) {
		//Recorro tots els v�rtexs del pol�gon
		vertID = (i % numVerts);
		Vec3D v = lVert.at(vertID).getPosition();
		pos = Ogre::Vector3(v.getX(), v.getY(), v.getZ());
		if (applicationMode == APPLICATION_MODE_2D) {
			if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XY) pos.z = 0.0f;
			else if (NEOGEN_PROJECTION_PLANE == RealMath::PROJECTION_XZ) pos.y = 0.0f;
			else pos.x = 0.0f;
		}
		manualObject->position(pos);
	}
	manualObject->end();
}

void Poly::printData() {
	Ogre::LogManager::getSingletonPtr()->logMessage("****** POLYGON DATA ******");
	unsigned int numVerts = getNumVert();
	Ogre::LogManager::getSingletonPtr()->logMessage("numVerts = " + std::to_string((int)numVerts));
	Ogre::LogManager::getSingletonPtr()->logMessage("*** VERTEX DATA ***");
	for (unsigned int i = 0; i < getNumVert(); i++) {
		Ogre::LogManager::getSingletonPtr()->logMessage("vertex " + std::to_string((int)i));
		getVertexAt(i).getPosition().printData();
	}
}

/************************************************************/
/* AUXILIARS */
/************************************************************/

//NOTA: Estaria b� que aquestes funcions anessin a una classe a part, 
//ja que no s�n pr�pies d'un pol�gon, per� de moment ho deixo 
//aqu�. 

void Poly::triangulate() {
	//Triangula el pol�gon per poder-lo visualitzar
	Triangulator::Process(lVert, lTriangle);
}

