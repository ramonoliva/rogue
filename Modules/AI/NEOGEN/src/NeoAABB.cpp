#include "NeoAABB.h"
#include "NeoGeometrySolver.h"

using namespace NEOGEN;
using namespace NeoGeometrySolver;

#pragma region CREATION AND DESTRUCTION

NeoAABB::NeoAABB() {
	clear();
}

NeoAABB::NeoAABB(const Vector3& min, const Vector3& max) {
	_min = min;
	_max = max;
}

#pragma endregion

#pragma region GET AND SET

void NeoAABB::clear() {
	_min = _max = Vector3(0, 0, 0);
	_isEmpty = true;
}

void NeoAABB::addPoint(const Vector3& p) {
	if (_isEmpty) {
		_min = _max = p;	//This is the first point added
		_isEmpty = false;
	}
	else {
		//Update min
		if (p.getX() < _min.getX()) _min.setX(p.getX());
		if (p.getY() < _min.getY()) _min.setY(p.getY());
		if (p.getZ() < _min.getZ()) _min.setZ(p.getZ());

		//Update max
		if (p.getX() > _max.getX()) _max.setX(p.getX());
		if (p.getY() > _max.getY()) _max.setY(p.getY());
		if (p.getZ() > _max.getZ()) _max.setZ(p.getZ());
	}
}

void NeoAABB::addPoints(const Polygon3& polygon) {
	for (Polygon3::const_iterator it = polygon.begin(); it != polygon.end(); it++) {
		addPoint(*it);
	}
}

const Vector3& NeoAABB::getMin() const {
	return _min;
}

const Vector3& NeoAABB::getMax() const {
	return _max;
}

void NeoAABB::setMin(const Vector3& min) {
	_min = min;
}

void NeoAABB::setMax(const Vector3& max) {
	_max = max;
}

Vector3 NeoAABB::getCenter() const {
	return (_min + _max) * NeoReal(0.5);
}

Vector3 NeoAABB::getSize() const {
	Vector3 result;
	result.setX(std::abs(_max.getX() - _min.getX()));
	result.setY(std::abs(_max.getY() - _min.getY()));
	result.setZ(std::abs(_max.getZ() - _min.getZ()));

	return result;
}

Vector3 NeoAABB::getSizeHalf() const {
	return NeoReal(0.5) * getSize();
}

bool NeoAABB::contains(const Vector3& p) const {
	return
		(
		Math::greaterOrEqual(p.getX(), _min.getX()) &&
		Math::greaterOrEqual(p.getY(), _min.getY()) &&
		Math::greaterOrEqual(p.getZ(), _min.getZ()) &&
		Math::lessOrEqual(p.getX(), _max.getX()) &&
		Math::lessOrEqual(p.getY(), _max.getY()) &&
		Math::lessOrEqual(p.getZ(), _max.getZ())
		);
}

bool NeoAABB::contains(const NeoAABB& aabb) const {
	//Determines if this box completely contains the input aabb inside. 
	return
		(
		Math::greaterOrEqual(aabb.getMin().getX(), _min.getX()) &&
		Math::greaterOrEqual(aabb.getMin().getY(), _min.getY()) &&
		Math::greaterOrEqual(aabb.getMin().getZ(), _min.getZ()) &&
		Math::lessOrEqual(aabb.getMax().getX(), _max.getX()) &&
		Math::lessOrEqual(aabb.getMax().getY(), _max.getY()) &&
		Math::lessOrEqual(aabb.getMax().getZ(), _max.getZ())
		);
}

bool NeoAABB::intersects(const NeoAABB& aabb) const {
	//Determines if this box intersects with aabb
	if (Math::less(aabb.getMax().getX(), _min.getX())) return false;	//aabb is at the left of this box
	if (Math::greater(aabb.getMin().getX(), _max.getX())) return false;	//aabb is at the right of this box

	if (Math::less(aabb.getMax().getY(), _min.getY())) return false;	//aabb is below this box
	if (Math::greater(aabb.getMin().getY(), _max.getY())) return false;	//aabb is above this box

	if (Math::less(aabb.getMax().getZ(), _min.getZ())) return false;	//aabb is in front of this box
	if (Math::greater(aabb.getMin().getZ(), _max.getZ())) return false;	//aabb is back of this box

	return true;
}

bool NeoAABB::intersects(const Ray3& ray, IntersectionResult3& result) const {
	return NeoGeometrySolver::rayBoxIntersection(ray, *this, result);
}

#pragma endregion