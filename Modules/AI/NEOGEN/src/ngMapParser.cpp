#include "ngMapParser.h"

using namespace NEOGEN;

/************************************************************************/
/* CREACI� I DESTRUCCI�
/************************************************************************/
MapParser::MapParser() {}

MapParser::~MapParser() {}

/************************************************************************/
/* PARSER FUNCTIONS
/************************************************************************/
bool MapParser::isVertPattern(int pat) {
	return ((pat == PATTERN_VERT0) || (pat == PATTERN_VERT1) || (pat == PATTERN_VERT2) || (pat == PATTERN_VERT3));
}

Poly* MapParser::createPolygon(Ogre::Image* img, std::vector<std::vector<bool>>& visited, int i, int j) {

	//Crea un pol�gon que comen�a a la posici� (i,j)

	std::vector<Vertex> lVert;
	int initI = i;
	int initJ = j;

	int pat;

	do {
		visited[i][j] = true;
		pat = pattern(img, (int)i, (int)j);
		if (isVertPattern(pat)) { //Si detecto un v�rtex, el creo
			lVert.push_back(Vertex(Vec3D((float)(i + 1), (float)(-(j + 1)))));
		}
		//Determino com haig d'avan�ar
		if ((pat == PATTERN_VERT0) || (pat == PATTERN_EDGE0)) {
			j--;
		}
		if ((pat == PATTERN_VERT1) || (pat == PATTERN_EDGE1)) {
			i++;
		}
		if ((pat == PATTERN_VERT2) || (pat == PATTERN_EDGE2)) {
			i--;
		}
		if ((pat == PATTERN_VERT3) || (pat == PATTERN_EDGE3)) {
			j++;
		}
	} while ((i != initI) || (j != initJ));
	//Creo el pol�gon amb la llista de v�rtexs obtinguda
	return (new Poly(lVert));

}

bool MapParser::isWallColor(Ogre::ColourValue *c) {
	return ((c->r == WALL_R) && (c->r == WALL_G) && (c->b == WALL_B));
}

int MapParser::pattern(Ogre::Image *img, int i, int j) {
	//Indica quin �s el patr� que hi ha en la posici� (i,j) de la imatge
	Ogre::ColourValue c0, c1, c2, c3;
	
	c0 = img->getColourAt(i, j, 0);
	c1 = img->getColourAt(i + 1, j, 0);
	c2 = img->getColourAt(i, j + 1, 0);
	c3 = img->getColourAt(i + 1, j + 1, 0);

	//Detecci� de patrons d'arestes

	if (
		!isWallColor(&c0) && 
		isWallColor(&c1) &&
		!isWallColor(&c2) &&
		isWallColor(&c3)
		) return PATTERN_EDGE0;

	if (
		!isWallColor(&c0) && 
		!isWallColor(&c1) &&
		isWallColor(&c2) &&
		isWallColor(&c3)
		) return PATTERN_EDGE1;

	if (
		isWallColor(&c0) && 
		isWallColor(&c1) &&
		!isWallColor(&c2) &&
		!isWallColor(&c3)
		) return PATTERN_EDGE2;

	if (
		isWallColor(&c0) && 
		!isWallColor(&c1) &&
		isWallColor(&c2) &&
		!isWallColor(&c3)
		) return PATTERN_EDGE3;

	//Detecci� de patrons de v�rtexs

	if (
		!isWallColor(&c0) && 
		isWallColor(&c1) //&&
		//isWallColor(&c2) //&&
		//isWallColor(&c3)
		) return PATTERN_VERT0;
	
	if (
		//isWallColor(&c0) && 
		!isWallColor(&c1) &&
		//isWallColor(&c2) &&
		isWallColor(&c3)
		) return PATTERN_VERT1;

	if (
		isWallColor(&c0) && 
		//isWallColor(&c1) &&
		!isWallColor(&c2) //&&
		//isWallColor(&c3)
		) return PATTERN_VERT2;

	if (
		//isWallColor(&c0) && 
		//isWallColor(&c1) &&
		isWallColor(&c2) &&
		!isWallColor(&c3)
		) return PATTERN_VERT3;

	return -1;
}

/******************************************************************************/
/* TRACTAMENT DE FITXERS */
/******************************************************************************/

/**********************************************************/
/* SAVE & LOAD DE POL�GONS */
/**********************************************************/


void MapParser::savePolyMapData(std::vector<Poly*>* data, const std::string& fileName) {
	//Guarda la geometria del mapa en un fitxer (terra i forats)

	//El primer element del vector data �s el pol�gon que representa
	//el terra. La resta d'elements s�n els forats

	size_t numPoly = data->size();
	unsigned int numVerts;
	std::ofstream file;
	file.open(fileName.c_str(), std::ios::trunc);
	Poly* p;
	Vec3D* v;
	
	file << numPoly << "\n";
	for (size_t i = 0; i < numPoly; i ++) {
		p = data->at(i);
		numVerts = p->getNumVert();
		file << numVerts << "\n";

		for (unsigned int j = 0; j <  numVerts; j++) {
			//Escric les coordenades de cada v�rtex del pol�gon al fitxer
			v = &(p->getVertexAt(j).getPosition());
			file << v->getX() << "\n";
			file << v->getY() << "\n";
			file << v->getZ() << "\n";
		}
	}
	file.close();
}

void MapParser::loadPolyMapData(std::vector<Poly*>* data, const std::string& fileName) {
	//Llegeix la geometria del mapa des d'un fitxer (terra i forats)

	//El primer element del vector data �ss el pol�gon que representa
	//el terra. La resta d'elements s�n els forats

	unsigned int numPoly;
	unsigned int numVert;
	std::ifstream file;
	file.open(fileName.c_str());
	std::string line;

	file >> numPoly; //llegeixo el nombre de pol�gons que hi ha al fitxer

	Ogre::LogManager::getSingletonPtr()->logMessage("numPoly = " + std::to_string((int)numPoly));

	std::vector<float> coord;
	float d;
	
	std::vector<Vertex> lVert;
	unsigned int numCoords = 2;
	
	for (unsigned int i = 0; i < numPoly; i ++) {
		file >> numVert; //llegeixo el nombre de v�rtexs del pol�gon
		lVert.clear();
		
		for (unsigned int j = 0; j < numVert; j++) {
			//llegeixo les 3 coordenades del v�rtex
			coord.clear();
			for (unsigned int k = 0; k < numCoords; k++) {
				file >> d;
				coord.push_back(d);
			}
			lVert.push_back(Vertex(Vec3D(coord.at(0), coord.at(1))));
		}
		data->push_back(new Poly(lVert)); //creo el poligon llegit
	}
	file.close();
}

/**********************************************************/
/* SAVE & LOAD DE NAVMESH */
/**********************************************************/
/******************************************************************************/
/* XML FORMAT */
/******************************************************************************/
/************************/
/* SAVE */
/************************/
void MapParser::saveObstacleDescription(ObstacleDescription& obstacles, PortalDescription& portals, const std::string& fileName) {
	pugi::xml_document doc;
	pugi::xml_node versionNode = doc.append_child("Version");
	versionNode.append_attribute("id") = "1.1";
	pugi::xml_node rootNode = doc.append_child("ObstacleDescription");
	
	//Save the obstacles
	for (ObstacleDescription::iterator it = obstacles.begin(); it != obstacles.end(); it++) {
		pugi::xml_node obstacleNode = rootNode.append_child("Obstacle");
		
		pugi::xml_node verticesNode = obstacleNode.append_child("Vertices");
		for (Obstacle::iterator itObs = it->begin(); itObs != it->end(); itObs++) {
			int id = itObs->first;
			Vec3D& pos = itObs->second;
			pugi::xml_node vNode = verticesNode.append_child("Vertex");
			vNode.append_attribute("id") = id;
			
			pugi::xml_node posNode = vNode.append_child("Position");
			posNode.append_attribute("x") = pos.getX();
			posNode.append_attribute("y") = pos.getY();
			posNode.append_attribute("z") = pos.getZ();
		}
	}
	
	//Save the user portals
	pugi::xml_node userPortalsNode = doc.append_child("UserPortals");
	for (PortalDescription::iterator it = portals.begin(); it != portals.end(); it++) {
		pugi::xml_node pNode = userPortalsNode.append_child("Portal");
		pNode.append_attribute("v1") = it->first;
		pNode.append_attribute("v2") = it->second;
	}

	doc.save_file(fileName.c_str());
}

void MapParser::loadObstacleDescription(ObstacleDescription& resultObstacles, PortalDescription& resultPortals, const std::string& fileName) {
	pugi::xml_document doc;
	doc.load_file(fileName.c_str());

	//Load the obstacle's description
	pugi::xml_node rootNode = doc.child("ObstacleDescription");
	pugi::xml_node versionNode = doc.child("Version");
	if (versionNode.empty()) loadObstacleDescription_1_0(rootNode, resultObstacles);
	else {
		std::string version = versionNode.attribute("id").as_string();
		if (version == "1.1") {
			loadObstacleDescription_1_1(rootNode, resultObstacles);
		}
		else Ogre::LogManager::getSingletonPtr()->logMessage("BAD OBSTACLE DESCRIPTION FILE VERSION!!!");
	}

	//Load the user defined portals
	pugi::xml_node userPortalsNode = doc.child("UserPortals");
	if (!userPortalsNode.empty()) loadUserPortals(userPortalsNode, resultPortals);
}

void MapParser::loadObstacleDescription_1_0(pugi::xml_node& rootNode, ObstacleDescription& resultObstacles) {
	Ogre::LogManager::getSingletonPtr()->logMessage("1.0 VERSION!!!");
	Obstacle obstacle;
	int idVertex = 0;
	for (pugi::xml_node o = rootNode.child("Obstacle"); o; o = o.next_sibling("Obstacle")) {
		obstacle.clear();
				
		pugi::xml_node verticesNode = o.child("Vertices");
		for (pugi::xml_node v = verticesNode.child("Position"); v; v = v.next_sibling("Position")) {
			Vec3D position;
			position.setX(v.attribute("x").as_float());
			position.setY(v.attribute("y").as_float());
			position.setZ(v.attribute("z").as_float());

			obstacle.push_back(VertexDescription(idVertex, position));
			idVertex++;
		}
		resultObstacles.push_back(obstacle);
	}
}

void MapParser::loadUserPortals(pugi::xml_node& userPortalsNode, PortalDescription& resultPortals) {
	for (pugi::xml_node pNode = userPortalsNode.child("Portal"); pNode; pNode = pNode.next_sibling("Portal")) {
		int v1 = pNode.attribute("v1").as_int();
		int v2 = pNode.attribute("v2").as_int();
		resultPortals.push_back(PairInt(v1, v2));
	}
}

void MapParser::loadObstacleDescription_1_1(pugi::xml_node& rootNode, ObstacleDescription& resultObstacles) {
	Ogre::LogManager::getSingletonPtr()->logMessage("1.1 VERSION!!!");
	Obstacle obstacle;
	for (pugi::xml_node o = rootNode.child("Obstacle"); o; o = o.next_sibling("Obstacle")) {
		obstacle.clear();
				
		pugi::xml_node verticesNode = o.child("Vertices");
		for (pugi::xml_node v = verticesNode.child("Vertex"); v; v = v.next_sibling("Vertex")) {
			//Recover the vertex id
			int id = v.attribute("id").as_int();
			
			//Recover the vertex position
			Vec3D position;
			pugi::xml_node positionNode = v.child("Position");
			position.setX(positionNode.attribute("x").as_float());
			position.setY(positionNode.attribute("y").as_float());
			position.setZ(positionNode.attribute("z").as_float());

			obstacle.push_back(VertexDescription(id, position));
		}
		resultObstacles.push_back(obstacle);
	}
}

void MapParser::saveNavMesh(NavMesh* navMesh, const std::string& fileName) {
	Ogre::LogManager::getSingletonPtr()->logMessage("SAVING NAV MESH");
	pugi::xml_document doc;
	pugi::xml_node voxelGridNode = doc.append_child("VoxelGrid");
	VoxelGrid& vGrid = VoxelGrid::getSingleton();
	saveVoxelGrid(vGrid, voxelGridNode);
	pugi::xml_node rootNode = doc.append_child("NavMesh");
	pugi::xml_node vertexTableNode = rootNode.append_child("VertexTable");
	saveVertTable(navMesh->getVertTable(), vertexTableNode);
	pugi::xml_node edgeTableNode = rootNode.append_child("EdgeTable");
	saveEdgeTable(navMesh->getEdgeTable(), edgeTableNode);
	pugi::xml_node cellTableNode = rootNode.append_child("CellTable");
	saveCellTable(navMesh->getCellTable(), cellTableNode);
	doc.save_file(fileName.c_str());
	Ogre::LogManager::getSingletonPtr()->logMessage("NAV MESH SAVED");
}

void MapParser::saveVoxelGrid(VoxelGrid& vGrid, pugi::xml_node& voxelGridNode) {
	//Guardo les dimensions de la VoxelGrid
	Ogre::LogManager::getSingletonPtr()->logMessage("SAVING VOXEL GRID");
	pugi::xml_node gridSizeNode = voxelGridNode.append_child("GridSize");
	gridSizeNode.append_attribute("sizeX") = vGrid.getSizeX();
	gridSizeNode.append_attribute("sizeY") = vGrid.getSizeY();
	gridSizeNode.append_attribute("sizeZ") = vGrid.getSizeZ();

	//Guardo les dimensions d'un v�xel
	pugi::xml_node voxelSizeNode = voxelGridNode.append_child("VoxelSize");
	voxelSizeNode.append_attribute("sizeX") = vGrid.getVoxelDim().x;
	voxelSizeNode.append_attribute("sizeY") = vGrid.getVoxelDim().y;
	voxelSizeNode.append_attribute("sizeZ") = vGrid.getVoxelDim().z;

	//Guardo els v�xels accessibles que formen part d'alguna cel�la
	VoxelPointerTable& accessibleVoxels = vGrid.getAccessibleVoxels();
	Voxel* v;
	std::set<int>* vCells;
	pugi::xml_node voxelNode;
	pugi::xml_node cellsNode;
	for (VoxelPointerTable::iterator it = accessibleVoxels.begin(); it != accessibleVoxels.end(); it++) {
		v = vGrid.getVoxel(it->x, it->y, it->z);
		vCells = &(v->getCells());
		if (vCells->size() == 0) continue; //El v�xel no forma part de la descripci� de cap cel�la
		voxelNode = voxelGridNode.append_child("Voxel");
		voxelNode.append_attribute("x") = it->x;
		voxelNode.append_attribute("y") = it->y;
		voxelNode.append_attribute("z") = it->z;
		//cellsNode = voxelNode.append_child("Cells");
		for (std::set<int>::iterator itCell = vCells->begin(); itCell != vCells->end(); itCell++) {
			voxelNode.append_child("Cell").append_attribute("id") = *itCell;
		}
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("VOXEL GRID SAVED");
}

void MapParser::saveVertTable(VertTable& vertTable, pugi::xml_node& vertTableNode) {
	//Guardo la informaci� dels v�rtexs
	Vertex* v;
	pugi::xml_node vertexNode;
	pugi::xml_node auxNode;
	for (VertTable::iterator it = vertTable.begin(); it != vertTable.end(); it++) {
		v = &(it->second);
		vertexNode = vertTableNode.append_child("Vertex");
		vertexNode.append_attribute("id") = v->getID();
		//Coordenades
		Vec3D position = v->getPosition();
		auxNode = vertexNode.append_child("Position");
		auxNode.append_attribute("x") = position.getX();
		auxNode.append_attribute("y") = position.getY();
		auxNode.append_attribute("z") = position.getZ();
		//Arestes incidents a v
		auxNode = vertexNode.append_child("IncidentEdges");
		for (IncidentEdgesList::iterator it = v->getIncidentEdges().begin(); it != v->getIncidentEdges().end(); it++) {
			auxNode.append_child("Edge").append_attribute("id") = *it;
		}
	}
}

void MapParser::saveEdgeTable(EdgeTable& edgeTable, pugi::xml_node& edgeTableNode) {
	EdgeTable::iterator it;
	Edge* e;
	pugi::xml_node edgeNode;
	for (it = edgeTable.begin(); it != edgeTable.end(); it++) {
		e = &(it->second);
		
		//Identificador de l'aresta
		edgeNode = edgeTableNode.append_child("Edge");
		edgeNode.append_attribute("id") = e->id;
		
		//Identificadors dels extrems
		edgeNode.append_attribute("v1") = e->v1;
		edgeNode.append_attribute("v2") = e->v2;
		
		//Es portal?
		edgeNode.append_attribute("type") = int(e->_type);

		//Si �s un portal, afegeixo el valor de clearance i el portal oposat
		if (e->isPortal()) {
			edgeNode.append_attribute("clearance2") = e->clearance2;
			edgeNode.append_attribute("opposite") = e->opposite;
		}

		//Aresta seg�ent
		edgeNode.append_attribute("next") = e->next;

		//Aresta anterior
		edgeNode.append_attribute("prev") = e->prev;
		
		//Cel�la a la que pertany l'aresta
		edgeNode.append_attribute("cellID") = e->cellID;
	}
}
		
void MapParser::saveCellTable(CellTable& cellTable, pugi::xml_node& cellTableNode) {
	Cell* c;
	pugi::xml_node cellNode;
	pugi::xml_node auxNode;
	pugi::xml_node voxelNode;
	pugi::xml_node crossNode;
	for (CellTable::iterator it = cellTable.begin(); it != cellTable.end(); it++) {
		c = &(it->second);
		
		//Identificador de la cel�la
		cellNode = cellTableNode.append_child("Cell");
		cellNode.append_attribute("id") = c->id;

		//Arestes que formen la cel�la
		auxNode = cellNode.append_child("ShapeEdges");
		for (std::set<int>::iterator itEdges = c->edges.begin(); itEdges != c->edges.end(); itEdges++) {
			auxNode.append_child("Edge").append_attribute("id") = *itEdges;
		}

		//Arestes de la cel�la que s�n portals
		auxNode = cellNode.append_child("PortalEdges"); 
		for (std::set<int>::iterator itPortals = c->portals.begin(); itPortals != c->portals.end(); itPortals++) {
			auxNode.append_child("Portal").append_attribute("id") = *itPortals;
		}

		//Adjacent cells
		auxNode = cellNode.append_child("AdjacentCells");
		for (std::set<int>::iterator it = c->adjacentTo.begin(); it != c->adjacentTo.end(); it++) {
			auxNode.append_child("Cell").append_attribute("id") = *it;
		}

		//Voxels que descriuen la cel�la
		auxNode = cellNode.append_child("Voxels");
		for (VoxelPointerTable::iterator it = c->voxels.begin(); it != c->voxels.end(); it++) {
			voxelNode = auxNode.append_child("VoxelID");
			voxelNode.append_attribute("x") = it->x;
			voxelNode.append_attribute("y") = it->y;
			voxelNode.append_attribute("z") = it->z;
		}
	}
}

/************************/
/* LOAD */
/************************/

void MapParser::loadNavMesh(NavMesh* navMesh, const std::string& fileName) {
	Ogre::LogManager::getSingletonPtr()->logMessage("LOADING NAV MESH");
	VoxelGrid& vGrid = VoxelGrid::getSingleton();
	std::string fileLocation = fileName;
	pugi::xml_document doc;
	pugi::xml_parse_result result;
	result = doc.load_file(fileLocation.c_str());
	pugi::xml_node voxelGridNode = doc.child("VoxelGrid");
	loadVoxelGrid(vGrid, voxelGridNode);
	pugi::xml_node navMeshNode = doc.child("NavMesh");
	pugi::xml_node vertexTableNode = navMeshNode.child("VertexTable");
	loadVertTable(navMesh->getVertTable(), vertexTableNode);
	pugi::xml_node edgeTableNode = navMeshNode.child("EdgeTable");
	loadEdgeTable(navMesh->getVertTable(), navMesh->getEdgeTable(), edgeTableNode);
	pugi::xml_node cellTableNode = navMeshNode.child("CellTable");
	loadCellTable(navMesh->getVertTable(), navMesh->getEdgeTable(), navMesh->getCellTable(), cellTableNode);
	Ogre::LogManager::getSingletonPtr()->logMessage("NAV MESH LOADED");
}

void MapParser::loadVoxelGrid(VoxelGrid& vGrid, pugi::xml_node& voxelGridNode) {
	Ogre::LogManager::getSingletonPtr()->logMessage("LOADING VOXEL GRID");
	pugi::xml_node gridSizeNode = voxelGridNode.child("GridSize");
	pugi::xml_node voxelSizeNode = voxelGridNode.child("VoxelSize");
	vGrid.createGrid(
						gridSizeNode.attribute("sizeX").as_uint(), 
						gridSizeNode.attribute("sizeY").as_uint(), 
						gridSizeNode.attribute("sizeZ").as_uint(), 
						voxelSizeNode.attribute("sizeX").as_float(), 
						voxelSizeNode.attribute("sizeY").as_float(), 
						voxelSizeNode.attribute("sizeZ").as_float()
					);
	
	//Recorro tots els v�xels accessibles de la grid
	/*int x, y, z;
	int cellID;
	for (pugi::xml_node voxelNode = voxelGridNode.child("Voxel"); voxelNode; voxelNode = voxelNode.next_sibling("Voxel")) {
		x = voxelNode.attribute("x").as_int();
		y = voxelNode.attribute("y").as_int();
		z = voxelNode.attribute("z").as_int();
		vGrid.setVoxelType(x, y, z, Voxel::TYPE_ACCESSIBLE);
		//Recorro totes les cel�les que interseccionen amb el v�xel
		for (pugi::xml_node cellNode = voxelNode.child("Cell"); cellNode; cellNode = cellNode.next_sibling("Cell")) {
			cellID = cellNode.attribute("id").as_int();
			vGrid.getVoxel(x, y, z)->addCell(cellID);
		}
	}*/
	Ogre::LogManager::getSingletonPtr()->logMessage("VOXEL GRID LOADED");
}

void MapParser::loadVertTable(VertTable& vertTable, pugi::xml_node& vertexTableNode) {

	//El generador de malles treballa en el pl� (X, -Y). El joc en canvi, treballa
	//en el pl� (X, Z). Cal fer la conversi�
	Ogre::LogManager::getSingletonPtr()->logMessage("LOADING VERTEX TABLE");
	//Recorro tots els nodes vertexs
	float x, y, z;
	pugi::xml_node incidentEdgesNode;
	pugi::xml_node edgeIDNode;
	for (pugi::xml_node node = vertexTableNode.child("Vertex"); node; node = node.next_sibling("Vertex")) {
		//Posicio del vertex
		x = node.child("Position").attribute("x").as_float();
		y = node.child("Position").attribute("y").as_float();
		z = node.child("Position").attribute("z").as_float();
		Vertex v(Vec3D(x, y, z));

		//ID del vertex
		v.setID(node.attribute("id").as_int());
		
		//Arestes incidents al vertex
		incidentEdgesNode = node.child("IncidentEdges");
		for (edgeIDNode = incidentEdgesNode.child("Edge"); edgeIDNode; edgeIDNode = edgeIDNode.next_sibling("Edge")) {
			v.addIncidentEdgeBack(edgeIDNode.attribute("id").as_int());
		}
		vertTable[v.getID()] = v;
	}
}

void MapParser::loadEdgeTable(VertTable& vertTable, EdgeTable& edgeTable, pugi::xml_node& edgeTableNode) {

	Ogre::LogManager::getSingletonPtr()->logMessage("LOADING EDGE TABLE");

	//Recorro totes les arestes del document
	Edge e;
	Vec3D v1, v2;

	for (pugi::xml_node node = edgeTableNode.child("Edge"); node; node = node.next_sibling("Edge")) {
		//Identificador de l'aresta
		e.id = node.attribute("id").as_int();

		//Extrems de l'aresta
		e.v1 = node.attribute("v1").as_int();
		e.v2 = node.attribute("v2").as_int();

		//Compute its normalized vDir and its normal that points towards the interior of the cell. 
		v1 = vertTable[e.v1].getPosition();
		v2 = vertTable[e.v2].getPosition();
		e._vDirNormalized = (v2 - v1).normalized();
		e._vDirNormalized2D = GeometrySolver::to2D(e._vDirNormalized).normalized();
		e._vNormal2D = Vec2D(-e._vDirNormalized2D.getY(), e._vDirNormalized2D.getX());

		//Cel�la a la que pertany l'aresta
		e.cellID = node.attribute("cellID").as_int();

		//Es portal?
		pugi::xml_attribute att = node.attribute("opposite");
		if (att.empty()) {
			e._type = Edge::OBSTACLE;
			e.clearance2 = 0.0f;
			e.opposite = -1;
		}
		else {
			e._type = Edge::PORTAL;
			e.clearance2 = node.attribute("clearance2").as_float();
			e.opposite = node.attribute("opposite").as_int();
		}

		//Aresta seg�ent
		e.next = node.attribute("next").as_int();

		//Aresta anterior
		e.prev = node.attribute("prev").as_int();
		
		e.manual = NULL;

		if (e.id != -1) edgeTable[e.id] = e;
	}
}

void MapParser::loadCellTable(VertTable& vertTable, EdgeTable& edgeTable, CellTable& cellTable, pugi::xml_node& cellTableNode) {

	Ogre::LogManager::getSingletonPtr()->logMessage("LOADING CELL TABLE");
	
	//Recorro totes les cel�les
	int edgeID, initialEdgeID;
	Edge* e;
	Cell c;
	std::vector<Vertex> lVert;
	std::pair<int, int> portalPair;
	pugi::xml_node shapeEdgesNode;
	pugi::xml_node portalEdgesNode;
	pugi::xml_node adjacentCellsNode;
	pugi::xml_node voxelsNode;
	pugi::xml_node crossesNode;
	VoxelPointer vPointer;
	for (pugi::xml_node node = cellTableNode.child("Cell"); node; node = node.next_sibling("Cell")) {
		lVert.clear();
		c = Cell();
				
		//Identificador de la cel�la
		c.id = node.attribute("id").as_int();

		//Arestes que formen la cel�la
		shapeEdgesNode = node.child("ShapeEdges");
		for (pugi::xml_node edgeNodeID = shapeEdgesNode.child("Edge"); edgeNodeID; edgeNodeID = edgeNodeID.next_sibling("Edge")) {
			edgeID = edgeNodeID.attribute("id").as_int();
			c.edges.insert(edgeID);
		}

		//Arestes de la cel�la que son portals
		portalEdgesNode = node.child("PortalEdges");
		for (pugi::xml_node edgeNodeID = portalEdgesNode.child("Portal"); edgeNodeID; edgeNodeID = edgeNodeID.next_sibling("Portal")) {
			c.portals.insert(edgeNodeID.attribute("id").as_int());
		}

		//Creo el pol�gon
		initialEdgeID = *(c.edges.begin());
		edgeID = initialEdgeID;
		do {
			e = &(edgeTable[edgeID]);
			lVert.push_back(vertTable[e->v1]);
			edgeID = e->next;
		} while (edgeID != initialEdgeID);
		c.p = new Poly(lVert, true);

		//Cel�les adjacents
		adjacentCellsNode = node.child("AdjacentCells");
		for (pugi::xml_node cellNodeID = adjacentCellsNode.child("Cell"); cellNodeID; cellNodeID = cellNodeID.next_sibling("Cell")) {
			c.adjacentTo.insert(cellNodeID.attribute("id").as_int());
		}

		//V�xels que descriuen la cel�la
		VoxelGrid& vGrid = VoxelGrid::getSingleton();
		voxelsNode = node.child("Voxels");
		for (pugi::xml_node voxelNodeID = voxelsNode.child("VoxelID"); voxelNodeID; voxelNodeID = voxelNodeID.next_sibling("VoxelID")) {
			vPointer.x = voxelNodeID.attribute("x").as_int();
			vPointer.y = voxelNodeID.attribute("y").as_int();
			vPointer.z = voxelNodeID.attribute("z").as_int();
			c.voxels.insert(vPointer);
			//Indico a la v�xel grid que aquest v�xel �s accessible
			vGrid.setVoxelType(vPointer.x, vPointer.y, vPointer.z, Voxel::TYPE_ACCESSIBLE);
			//Indico que el v�xel forma part de la cel�la actual
			vGrid.getVoxel(vPointer.x, vPointer.y, vPointer.z)->addCell(c.id);
		}

		//Creo la cel�la
		cellTable[c.id] = c;
	}
}




