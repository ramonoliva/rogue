#include "NeoTerrain.h"
#include "NeoPSimpl.h"
#include "NeoMath.h"
#include "NeoPoly2Tri.h"

#include <OgreLogManager.h>
#include <OgreStringConverter.h>

using namespace NEOGEN;
using namespace NEOGEN::NeoGeometrySolver;

#pragma region CREATION AND DESTRUCTION

NeoTerrain::NeoTerrain(Vector3List& pointList, ElementIDVector& triangleList) : NeoMesh(pointList, triangleList) {
	_meshAccessible = NeoMeshUPtr(new NeoMesh(_aabb));
	_test = 0;
	/*sew();
	simplify();*/
}

NeoTerrain::~NeoTerrain() {
	
}

#pragma endregion

#pragma region GET AND SET

void NeoTerrain::addFaceWalkable(NeoFace* face) {
	face->setType(NeoFace::Type::WALKABLE);
	_facesWalkable.insert(face);
}

void NeoTerrain::addFaceObstacle(NeoFace* face) {
	face->setType(NeoFace::Type::OBSTACLE);
	_facesObstacle.insert(face);
}

NeoMesh* NeoTerrain::getMeshAccessible() {
	return _meshAccessible.get();
}

NeoFacePSet& NeoTerrain::getFacesWalkable() {
	return _facesWalkable;
}

NeoFacePSet& NeoTerrain::getFacesObstacle() {
	return _facesObstacle;
}

#pragma endregion

#pragma region UPDATE

#pragma region SLOPE CONSTRAINT

void NeoTerrain::applySlopeConstraint(NeoReal maxSlope) {
	NeoReal cosSlope = Math::cos(Math::toRadians(maxSlope));
	NEOGEN::Vector3 up = NEOGEN::Vector3(0, 1, 0);
	for (NeoFaceMap::iterator it = _faceMap.begin(); it != _faceMap.end(); it++) {
		const NEOGEN::Vector3& normal = it->second->getNormal();
		if (up.dot(normal) > cosSlope) addFaceWalkable(it->second.get());
		else addFaceObstacle(it->second.get());
	}

	/*for (NeoFacePSet::iterator it = _facesObstacle.begin(); it != _facesObstacle.end(); it++) {
		NeoFace* f = *it;
		f->log();
	}*/
}

#pragma endregion

#pragma region CEIL CONSTRAINTS

void NeoTerrain::applyCeilConstraints(NeoReal height) {
	//Compute the floor and holes partition for each walkable face
	for (NeoFacePSet::iterator it = _facesWalkable.begin(); it != _facesWalkable.end(); it++) {
		applyCeilConstraints(*it, height);
	}

	//_meshAccessible->simplify();
	//if (!_is2D) sewMeshAccessible();
}

void NeoTerrain::applyCeilConstraints(NeoFace* face, NeoReal height) {
	if (_is2D) {
	//if (true) {
		Polygon3 contour;
		face->getContour(contour);
		_meshAccessible->createFace(contour);
	}
	else {
		//1) Compute the AABB of the face and extrude it height in the Y direction. 
		NeoFacePSet ceilFaces;
		NeoAABB aabb = face->getAABB();
		aabb.setMax(aabb.getMax() + Vector3(0, 1, 0) * height);
		_octree->computeIntersectedObjects(aabb, ceilFaces);

		//2) For each ceil face, compute the exact subpolygon that is a ceil constraint of the input face. 
		Polygons3 ceilConstraints;
		for (NeoFacePSet::iterator itRawCeil = ceilFaces.begin(); itRawCeil != ceilFaces.end(); itRawCeil++) {
			NeoFace* rawCeil = *itRawCeil;
			//if ((rawCeil == face) || (rawCeil->isNeighbor(face) && rawCeil->isTypeWalkable())) continue;
			if ((rawCeil == face) || (rawCeil->isNeighbor(face))) continue;

			Polygon3 polygon;
			refineCeil(height, face, *itRawCeil, polygon);
			if (polygon.size() > 0) ceilConstraints.push_back(polygon);
		}

		if (ceilConstraints.size() == 0) {
			//The face does not have any constraint, so is completelly accessible. 
			Polygon3 polygon;
			face->getContour(polygon);
			NeoFace* f = _meshAccessible->createFace(polygon);
			_accessibleFaceMap[face].push_back(f->getID());
		}
		else {
			//3) Compute the floor and holes partition of this face. Clip the subject poligon with the ceil constraints. 
			ClipperLib::PolyTree partition;
			NeoClipper clipper;
			Polygon3 subj;
			face->getContour(subj);
			clipper.applyCeilConstraints(subj, ceilConstraints, partition);

			//4) Create the accessible mesh
			ClipperLib::PolyNodes& outerNodes = partition.Childs;
			for (ClipperLib::PolyNodes::iterator itNode = outerNodes.begin(); itNode != outerNodes.end(); itNode++) {
				createFaces(*itNode, face, height, ceilFaces);
			}
		}
	}
}

void NeoTerrain::refineCeil(NeoReal height, NeoFace* face, NeoFace* rawCeil, Polygon3& result) {
	Polygon3 contourFace, contourCeil;
	face->getContour(contourFace);
	rawCeil->getContour(contourCeil);
	for (size_t i = 0; i < contourFace.size(); i++) contourFace[i].setY(0);
	for (size_t i = 0; i < contourCeil.size(); i++) contourCeil[i].setY(0);

	if (!isTriangleTriangleIntersection(contourFace, contourCeil)) return;
		
	const Vector3 down = Vector3(0, -1, 0);
	const Plane planeFace(face->getVertex(0)->getPosition(), face->getNormal());

	NeoEdgePVector& edges = rawCeil->getEdges();
	for (NeoEdgePVector::iterator itEdges = edges.begin(); itEdges != edges.end(); itEdges++) {
		NeoEdge* e = *itEdges;
		const Vector3& eBeginPos = e->getVertexBegin()->getPosition();
		const Vector3& eEndPos = e->getVertexEnd()->getPosition();
			
		IntersectionResult3 resultBegin, resultEnd;
		linePlaneIntersection(eBeginPos, eBeginPos + down, planeFace, resultBegin);
		linePlaneIntersection(eEndPos, eEndPos + down, planeFace, resultEnd);
		
		//Compute the ceil subedge
		NeoReal beginDist = Math::sign(down.dot(resultBegin._intersectionPoint - eBeginPos)) * eBeginPos.dist(resultBegin._intersectionPoint);
		NeoReal endDist = Math::sign(down.dot(resultEnd._intersectionPoint - eEndPos)) * eEndPos.dist(resultEnd._intersectionPoint);
		bool isBeginMin = Math::lessOrEqual(beginDist, endDist);
		NeoReal dMin, dMax;
		Vector3 pMin, pMax;

		if (isBeginMin) {
			dMin = beginDist;
			pMin = resultBegin._intersectionPoint;

			dMax = endDist;
			pMax = resultEnd._intersectionPoint;
		}
		else {
			dMin = endDist;
			pMin = resultEnd._intersectionPoint;

			dMax = beginDist;
			pMax = resultBegin._intersectionPoint;
		}
		
		if (Math::lessOrEqual(dMin, height) && Math::greaterOrEqual(dMax, 0)) {
			//This is a ceil edge, as it contains a subsegment that is at a distance
			//less or equal than the constraint height. Compute such subsegment. 
			
			NeoReal d = std::max(dMax - dMin, NeoReal(0.0));
			NeoReal t1 = Math::equal(d, 0)? 0 : -dMin / d;
			NeoReal t2 = Math::equal(d, 0)? 0 : (height - dMin) / d;
			Vector3 subEdgeMin = Vector3::lerp(pMin, pMax, t1);
			Vector3 subEdgeMax = Vector3::lerp(pMin, pMax, t2);
			if (isBeginMin) {
				result.push_back(subEdgeMin);
				result.push_back(subEdgeMax);
			}
			else {
				result.push_back(subEdgeMax);
				result.push_back(subEdgeMin);
			}
		}
	}

	NeoPSimpl::simplifyRadialDistance(result);
	
	if (result.empty()) return;

	//Check if it is a perpendicular face. If this is the case, create a clossed polygon by offseting 
	//the resulting line a little bit. 
	const Vector3& normal = rawCeil->getNormal();
	if (Math::equal(normal.dot(Vector3(0, 1, 0)), NeoReal(0))) {
		NeoReal offset = Math::NEO_REAL_PRECISION * 0.5; //NeoReal(0.001);
		Vector3Cmp cmp;
		Polygon3 tmp = result;
		sort(tmp.begin(), tmp.end(), cmp);

		Vector3 p0 = tmp[0];
		Vector3 p1 = tmp[tmp.size() - 1];
		result.clear();
		result.push_back(p0);
		result.push_back(p1);
		result.push_back(p1 - normal * offset);
		result.push_back(p0 - normal * offset);
	}
}

void NeoTerrain::createFaces(ClipperLib::PolyNode* node, NeoFace* face, NeoReal height, NeoFacePSet& ceilFaces) {
	const Vector3 up = Vector3(0, 1, 0);
	const Plane projectionPlane = Plane(face->getVertex(0)->getPosition(), face->getNormal());
	
	const NeoReal distTolerance = NeoReal(0.001);

	//1) Create the floor. 
	Polygon2 floor;
	for (ClipperLib::Path::const_iterator itPoint = node->Contour.begin(); itPoint != node->Contour.end(); itPoint++) {
		Vector3 v = NeoClipper::toNeogen(*itPoint);
		floor.push_back(v.to2D());
	}

	//Simplify the polygon in order to avoid 0 length edges and other degeneracies. 
	NeoPSimpl::cleanPolygon<Vector2, Vector2Cmp>(floor, distTolerance);
	if (floor.size() < 3) return;
	
	//2) Create the holes
	ClipperLib::PolyNodes& childs = node->Childs;
	Polygons2 holes;
	for (ClipperLib::PolyNodes::iterator it = childs.begin(); it != childs.end(); it++) {
		//Reproject the clipper polygon into the plane defined by the face
		ClipperLib::Path& p = (*it)->Contour;
		Polygon2 h;
		for (ClipperLib::Path::const_iterator itPoint = p.begin(); itPoint != p.end(); itPoint++) {
			Vector3 v = NeoClipper::toNeogen(*itPoint);
			h.push_back(v.to2D());
		}

		//Simplify the polygon in order to avoid 0 length edges and other degeneracies. 
		NeoPSimpl::cleanPolygon<Vector2, Vector2Cmp>(h, distTolerance);
		if (h.size() < 3) continue;

		holes.push_back(h);
	}

	//3) Triangulate the floor and holes partition, so we avoid the need of having holes. 
	Polygons2 result;
	if (holes.empty()) result.push_back(floor);
	else NeoPoly2Tri::triangulate(floor, holes, result);
	
	//4) Create the faces resulting of the triangulation. 
	ElementIDVector faceVectorID;
	for (Polygons2::iterator it = result.begin(); it != result.end(); it++) {
		Polygon3 polygon;
		Polygon2& p = *it;
		for (size_t i = 0; i < p.size(); i++) {
			Vector3 v = Vector3(p[i].getX(), NeoReal(0.0), p[i].getY());
			NeoGeometrySolver::IntersectionResult3 intersection;
			NeoGeometrySolver::linePlaneIntersection(v, v + Vector3(0, 1, 0), projectionPlane, intersection);
			polygon.push_back(intersection._intersectionPoint);
		}

		if (!NeoGeometrySolver::getPolygonOrientation(polygon)) std::reverse(polygon.begin(), polygon.end());
		NeoFace* f = _meshAccessible->createFace(polygon);
		_accessibleFaceMap[face].push_back(f->getID());
		faceVectorID.push_back(f->getID());

		//Determine if there is any wall edge
		NeoEdgePVector& edges = f->getEdges();
		NeoReal h2 = height * height;
		const Vector3 offset = Vector3(0, Math::NEO_REAL_PRECISION * 10, 0);
		for (NeoEdgePVector::iterator itEdge = edges.begin(); itEdge != edges.end(); itEdge++) {
			NeoEdge* e = *itEdge;
			const Vector3& eBeginPos = e->getVertexBegin()->getPosition() + offset;
			const Vector3& eEndPos = e->getVertexEnd()->getPosition() + offset;

			Ray3 ray(Vector3::lerp(eBeginPos, eEndPos, NeoReal(0.5)), Vector3(0, 1, 0), height);
			for (NeoFacePSet::iterator itIntersection = ceilFaces.begin(); itIntersection != ceilFaces.end(); itIntersection++) {
				NeoFace* tmp = *itIntersection;
				if (tmp->isTypeWalkable() || !rayBoxIntersection(ray, tmp->getAABB())) continue;

				Triangles& triangles = tmp->getTriangles();
				for (Triangles::iterator itTriangle = triangles.begin(); itTriangle != triangles.end(); itTriangle++) {
					IntersectionResult3 intersectionResult;
					Triangle& t = *itTriangle;

					if (rayTriangleIntersection(ray, t[0]->getPosition(), t[1]->getPosition(), t[2]->getPosition(), intersectionResult)) {
						if (Math::lessOrEqual(ray._v0.dist2(intersectionResult._intersectionPoint), h2)) {
							e->setTypeWall(true);
							break;
						}
					}
				}

				if (e->isTypeWall()) break;
			}
		}
	}

	//5) Simplify the resulting set of faces. 
	//_meshAccessible->simplify(faceVectorID);
}

void NeoTerrain::sewMeshAccessible() {
	//Sew up the accessible mesh. This is necessary because after applying the ceil constraints step, the 
	//resulting accessible mesh may contain false obstacle edges due to they overlap but they do not
	//share exactly the same endpoints. 
	
	_meshAccessible->exportOBJ("test_BEFORE_SWEEP.obj");

	for (NeoFacePSet::iterator it = _facesWalkable.begin(); it != _facesWalkable.end(); it++) {
		NeoFace* f = *it;
		NeoFacePSet adjacentFaces;
		f->getAdjacents(adjacentFaces);
		
		for (NeoFacePSet::iterator itAdjacent = adjacentFaces.begin(); itAdjacent != adjacentFaces.end(); itAdjacent++) {
			sewAccessibleFaces(_accessibleFaceMap[f], _accessibleFaceMap[*itAdjacent]);
		}
	}

	_meshAccessible->exportOBJ("test_AFTER_SWEEP.obj");
}

void NeoTerrain::sewAccessibleFaces(const ElementIDVector& s1, const ElementIDVector& s2) {
	for (ElementIDVector::const_iterator itS1 = s1.cbegin(); itS1 != s1.cend(); itS1++) {
		for (ElementIDVector::const_iterator itS2 = s2.cbegin(); itS2 != s2.cend(); itS2++) {
			sewAccessibleFaces(_meshAccessible->getFace(*itS1), _meshAccessible->getFace(*itS2));
		}
	}
}

void NeoTerrain::sewAccessibleFaces(NeoFace* f1, NeoFace* f2) {
	NeoEdgePVector& f1Edges = f1->getEdges();
	NeoEdgePVector& f2Edges = f2->getEdges();
	NeoEdge* e1Overlap = NULL;
	NeoEdge* e2Overlap = NULL;
	
	//Check if there is any edge where the two faces overlap. 
	for (NeoEdgePVector::iterator it1 = f1Edges.begin(); (it1 != f1Edges.end()) && (!e1Overlap); it1++) {
		NeoEdge* e1 = *it1;
		const Vector3& e1Begin = e1->getVertexBegin()->getPosition();
		const Vector3& e1End = e1->getVertexEnd()->getPosition();
		if (e1->isTypeWalkable() || e1->isTypeWall()) continue;
		
		for (NeoEdgePVector::iterator it2 = f2Edges.begin(); it2 != f2Edges.end(); it2++) {
			NeoEdge* e2 = *it2;
			const Vector3& e2Begin = e2->getVertexBegin()->getPosition();
			const Vector3& e2End = e2->getVertexEnd()->getPosition();
			if (e2->isTypeWalkable() || e2->isTypeWall() || !segmentSegmentOverlapping(e1Begin, e1End, e2Begin, e2End)) continue;

			e1Overlap = e1;
			e2Overlap = e2;
		}
	}

	if (!e1Overlap) return;

	Ogre::LogManager::getSingletonPtr()->logMessage("===================================");
	Ogre::LogManager::getSingletonPtr()->logMessage("OVERLAP!!!");
	Ogre::LogManager::getSingletonPtr()->logMessage("===================================");
	e1Overlap->log();
	e2Overlap->log();

	const Vector3& e1Begin = e1Overlap->getVertexBegin()->getPosition();
	const Vector3& e1End = e1Overlap->getVertexEnd()->getPosition();
	const Vector3& e2Begin = e2Overlap->getVertexBegin()->getPosition();
	const Vector3& e2End = e2Overlap->getVertexEnd()->getPosition();

	//At this point, f1 and f2 overlaps at e1Overlap and e2Overlap respectively. 
	NeoFace* t1 = reconstructAccessibleFace(f1, e1Begin, e1End, e2Begin, e2End);
	//_meshAccessible->exportOBJ("test_" + std::to_string(_test) + "_f1.obj");
	NeoFace* t2 = reconstructAccessibleFace(f2, e2Begin, e2End, e1Begin, e1End);
	//_meshAccessible->exportOBJ("test_" + std::to_string(_test) + "_f2.obj");
	_test++;
}

NeoFace* NeoTerrain::reconstructAccessibleFace(NeoFace* f, const Vector3& e1Begin, const Vector3& e1End, const Vector3& e2Begin, const Vector3& e2End) {
	NeoEdgePVector& edges = f->getEdges();
	Polygon3 contour;
	for (NeoEdgePVector::iterator it = edges.begin(); it != edges.end(); it++) {
		NeoEdge* e = *it;
		contour.push_back(e->getVertexBegin()->getPosition());
		if (e->getVertexBegin()->getPosition().equal(e1Begin)) {
			bool inSegmentEnd = isPointInSegment(e2End, e1Begin, e1End) && !e2End.equal(e1Begin);
			bool inSegmentBegin = isPointInSegment(e2Begin, e1Begin, e1End) && !e2Begin.equal(e1End);
			if (inSegmentEnd) contour.push_back(e2End);
			if (inSegmentBegin) contour.push_back(e2Begin);
		}
	}
	
	NeoPSimpl::simplifyRadialDistance(contour);
	ElementID faceID = f->getID();
	_meshAccessible->destroyFaceOnly(f);
	_meshAccessible->destroyEdge(_meshAccessible->getEdge(e1Begin, e1End));
	return _meshAccessible->createFace(contour, faceID);
}

#pragma endregion

#pragma endregion


