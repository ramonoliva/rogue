#include "ObjMesh.h"
#include "ObjMeshSerializer.h"
#include <OgreLogManager.h>
 
/*********************************************************************************************/
/* CREACIO I DESTRUCCIO */
/*********************************************************************************************/

ObjMesh::ObjMesh(Ogre::ResourceManager* creator, 
				 const Ogre::String &name, 
                 Ogre::ResourceHandle handle, 
				 const Ogre::String &group, bool isManual, 
                 Ogre::ManualResourceLoader *loader) : Ogre::Resource(creator, name, handle, group, isManual, loader) {
    /* If you were storing a pointer to an object, then you would set that pointer to NULL here. */

    /* For consistency with StringInterface, but we don't add any parameters here
    That's because the Resource implementation of StringInterface is to
    list all the options that need to be set before loading, of which 
    we have none as such. Full details can be set through scripts.
    */ 
    createParamDictionary("ObjMesh");
}
 
ObjMesh::~ObjMesh() {
    unload();
}

/*********************************************************************************************/
/* GET I SET */
/*********************************************************************************************/
 
void ObjMesh::setGeometryMode(int mode) {
	geometryMode = mode;
}

int ObjMesh::getGeometryMode(void) {
	return geometryMode;
}

void ObjMesh::setGeometryFile(const Ogre::String& fName) {
	geometryFile = fName;
}

const Ogre::String& ObjMesh::getGeometryFile(void) const {
	return geometryFile;
}

void ObjMesh::setImgPreview(const Ogre::String& fName) {
	imgPreview = fName;
}

const Ogre::String& ObjMesh::getImgPreview(void) const {
	return imgPreview;
}

void ObjMesh::setNavMeshFile(const Ogre::String& fName) {
	navMeshFile = fName;
}

const Ogre::String& ObjMesh::getNavMeshFile(void) const {
	return navMeshFile;
}

void ObjMesh::setRedFlagPos(float x, float y, float z) {
	flagRedPosX = x;
	flagRedPosY = y;
	flagRedPosZ = z;
}

void ObjMesh::setBlueFlagPos(float x, float y, float z) {
	flagBluePosX = x;
	flagBluePosY = y;
	flagBluePosZ = z;
}

void ObjMesh::getRedFlagPos(float& x, float& y, float& z) {
	x = flagRedPosX;
	y = flagRedPosY;
	z = flagRedPosZ;
}

void ObjMesh::getBlueFlagPos(float& x, float& y, float& z) {
	x = flagBluePosX;
	y = flagBluePosY;
	z = flagBluePosZ;
}

/*********************************************************************************************/
/* OPERACIONS DE CLASSE */
/*********************************************************************************************/
 
// farm out to ObjMeshSerializer
void ObjMesh::loadImpl(void) {
    ObjMeshSerializer serializer;
    Ogre::DataStreamPtr stream = Ogre::ResourceGroupManager::getSingleton().openResource(mName, mGroup, true, this);
    serializer.importObjMesh(stream, this);
}
 
void ObjMesh::unloadImpl(void) {
    /* If you were storing a pointer to an object, then you would check the pointer here,
    and if it is not NULL, you would destruct the object and set its pointer to NULL again.
    */
	geometryFile.clear();
	imgPreview.clear();
	navMeshFile.clear();
}
 
size_t ObjMesh::calculateSize(void) const {
	/*size_t size = 0;
	size += geometryFile.length();
	size += imgPreview.length();
	size += navMeshFile.length();*/
    return 0;
}
