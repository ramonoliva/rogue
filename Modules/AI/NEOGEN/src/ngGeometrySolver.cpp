#include "ngGeometrySolver.h"
#include "ngGlobalVars.h"
#include <algorithm>

using namespace NEOGEN;

/******************************************************************************************/
/* FUNCIONS DE CLASSE */
/******************************************************************************************/
Vec3D GeometrySolver::to3D(Vec2D& v, RealMath::ProjectionPlaneEnum projectionPlane) {
	if (projectionPlane == RealMath::PROJECTION_XY) return Vec3D(v.getX(), v.getY(), 0.0f);
	if (projectionPlane == RealMath::PROJECTION_XZ) return Vec3D(v.getX(), 0.0f, v.getY());
	return Vec3D(0.0f, v.getX(), v.getY());
}

Vec2D GeometrySolver::to2D(Vec3D& v, RealMath::ProjectionPlaneEnum projectionPlane) {
	if (projectionPlane == RealMath::PROJECTION_XY) return Vec2D(v.getX(), v.getY());
	if (projectionPlane == RealMath::PROJECTION_XZ) return Vec2D(v.getX(), v.getZ());
	return Vec2D(v.getY(), v.getZ());
}

void GeometrySolver::to2D(Poly& p, std::vector<Vec2D>& result, RealMath::ProjectionPlaneEnum projectionPlane) {
	//Projecta un pol�gon en un pla de projecci� determinat
	std::vector<Vertex>& lVert = p.getVertexList();
	for (std::vector<Vertex>::iterator it = lVert.begin(); it != lVert.end(); it++) {
		result.push_back(to2D(it->getPosition(), projectionPlane));
	}
}

float GeometrySolver::pointSegmentDistance(Vec3D* p, Vec3D* lBegin, Vec3D* lEnd) {
	//Calcula la dist�ncia entre el punt p i la recta que passa pels punts lBEgin i lEnd
	BoostPoint3D bP(p->getX(), p->getZ(), p->getY());
	BoostPoint3D blBegin(lBegin->getX(), lBegin->getZ(), lBegin->getY());
	BoostPoint3D blEnd(lEnd->getX(), lEnd->getZ(), lEnd->getY());
	BoostSegment s(blBegin, blEnd);
	return (float)boost::geometry::distance(bP, s);
}

void GeometrySolver::projectionPointLine3D(Vec3D& v, Vec3D& s1, Vec3D& s2, IntersectionPoint3D& result) {
	//Calcula la projecci� del v�rtex v sobre la recta definida pel segment (s1, s2)
	Vec3D w = v - s1;		//Vector del punt v al punt del segment s1
	Vec3D vDir = s2 - s1;	//Vector director de la recta definida pels extrems s1 s2
	//Comprobo si v �s un punt que ja est� sobre el segment. En 3D:
	//|AxB| = |A||B|sin(a)
	//Si |AxB| => sin(a) = 0
	Vec3D cross = w.crossProduct(vDir);
	if (RealMath::equal(cross.norm2(), 0.0f)) {
		//v �s un punt que est� sobre la recta definida pel segment (s1, s2). Comprobo si est� entre s1 i s2
		result.location = v;
		result.t = v.dist(s1);	
		//Hem de determinar el signe de t
		if (isBetween(s1, v, s2)) result.t *= -1.0f;	//Cal canviar el signe, ja que v est� en sentit oposat al vector s1->s2
	}
	else {
		//v NO est� sobre la recta. Per tant, calculo la seva projecci�
		float b = w.dotProduct(vDir) / vDir.dotProduct(vDir);	//Indica on es troba la projecci� de v sobre la recta (s1, s2)
		result.location = s1 + b*vDir;
		result.t = b;
	}
	result.isValid = true;	//Sempre podem projectar un punt sobre una recta
}

void GeometrySolver::projectionPointSegment3D(Vec3D& v, Vec3D& s1, Vec3D& s2, IntersectionPoint3D& result) {
	//Calcula la projecci� del v�rtex v sobre el segment (s1, s2)
	projectionPointLine3D(v, s1, s2, result);
	//M'haig d'assegurar que el punt cau entre els extrems del segment
	float b = result.t;
	//result.isValid = (RealMath::greaterOrEqual(b, 0.0f) && RealMath::lessOrEqual(b, 1.0f));
	if (result.t < 0.0f) {
		//El punt de projecci� cau fora de (s1, s2) per la banda de s1. Per tant, el punt de projecci� �s s1
		result.t = 0.0f;
		result.location = s1;
	}
	else if (result.t > 1.0f) {
		//El punt de projecci� cau fora de (s1, s2) per la banda de s2. Per tant, el punt de projecci� �s s2
		result.t = 1.0f;
		result.location = s2;
	}
	result.isValid = true;
}

void GeometrySolver::projectionPointLine2D(Vec2D& v, Vec2D& s1, Vec2D& s2, IntersectionPoint2D& result) {
	//Calcula la projecci� del v�rtex v sobre la recta definida pel segment (s1, s2)
	Vec2D w = v - s1;		//Vector del punt v al punt del segment s1
	Vec2D vDir = s2 - s1;	//Vector director de la recta definida pels extrems s1 s2
	if (RealMath::equal(w.perpProduct(vDir), 0.0f)) {
		//s1 �s colinear a la linia definida pel segment (s1, s2)
		//v �s un punt que est� sobre la recta definida pel segment (s1, s2). Comprobo si est� entre s1 i s2
		result.location = v;
		result.t = v.dist(s1);	
		//Hem de determinar el signe de t
		if (isBetween2D(s1, v, s2)) result.t *= -1.0f;	//Cal canviar el signe, ja que v est� en sentit oposat al vector s1->s2
	}
	else {
		//v NO est� sobre la recta. Per tant, calculo la seva projecci�
		float b = w.dotProduct(vDir) / vDir.dotProduct(vDir);	//Indica on es troba la projecci� de v sobre la recta (s1, s2)
		result.location = s1 + b*vDir;
		result.t = b;
	}
	result.isValid = true;	//Sempre podem projectar un punt sobre una recta
}

void GeometrySolver::projectionPointSegment2D(Vec2D& v, Vec2D& s1, Vec2D& s2, IntersectionPoint2D& result) {
	//Calcula la projecci� del v�rtex v sobre el segment (s1, s2)
	projectionPointLine2D(v, s1, s2, result);
	//M'haig d'assegurar que el punt cau entre els extrems del segment
	float b = result.t;
	//result.isValid = (RealMath::greaterOrEqual(b, 0.0f) && RealMath::lessOrEqual(b, 1.0f));
	if (result.t < 0.0f) {
		//El punt de projecci� cau fora de (s1, s2) per la banda de s1. Per tant, el punt de projecci� �s s1
		result.t = 0.0f;
		result.location = s1;
	}
	else if (result.t > 1.0f) {
		//El punt de projecci� cau fora de (s1, s2) per la banda de s2. Per tant, el punt de projecci� �s s2
		result.t = 1.0f;
		result.location = s2;
	}
	result.isValid = true;
}

void GeometrySolver::segmentSegmentIntersection2D(Vec2D& s1, Vec2D& s2, Vec2D& s3, Vec2D& s4, SegmentIntersection2D& result) {
	
	result.p1Valid = false;
	result.p2Valid = false;

    Vec2D u = s2 - s1;
	Vec2D v = s4 - s3;
	Vec2D w = s1 - s3;
    
	float D = u.perpProduct(v);

	if (!RealMath::equal(D, 0.0f)) {
		//Cas simple: Els segments NO s�n colinears i per tant, interseccionen en un sol punt. Calculem la intersecci�
		
		//Comprobo si el punt d'intersecci� cau entre (s1, s2)
		float sI = v.perpProduct(w) / D;
		if (!RealMath::greaterOrEqual(sI, 0.0f) || !RealMath::lessOrEqual(sI, 1.0f)) return;	//El punt cau fora dels extrems (s1, s2)
		
		//Comprobo si el punt d'intersecci� cau entre (s3, s4)
		float tI = u.perpProduct(w) / D;
		if (!RealMath::greaterOrEqual(tI, 0.0f) || !RealMath::lessOrEqual(tI, 1.0f)) return;	//El punt cau fora dels extrems (s1, s2)

		//Si arribem aqu� �s perqu� el punt d'intersecci� est� entre (s1, s2) i (s3, s4). Per tant, �s v�lid
		result.p1 = s1 + sI * u;
		result.p1Valid = true;
	}
	else {
		//Els dos segments s�n paral�lels
		if (!RealMath::equal(u.perpProduct(w), 0.0f) || !RealMath::equal(v.perpProduct(w), 0.0f)) return;	//NO s�n colinears
		
		//Si arribem aqu� �s perqu� s�n colinears o s�n casos degenerats
        //Comprobem si estem en un cas degenerat
        float du = u.dotProduct(u);
		float dv = v.dotProduct(v);
		if (RealMath::equal(du, 0.0f) && RealMath::equal(dv, 0.0f)) {
			//Els dos segments s�n en realitat, un punt. 
			result.p1Valid = (s1.equal(s3));	//Si s�n el mateix punt, el sistema �s compatible i la intersecci� �s aquest punt
			result.p1 = s1;
        }
        else if (RealMath::equal(du, 0.0f)) {	
			//El segment (s1, s2) en realitat �s un sol punt.
			result.p1Valid = isBetween2D(s1, s3, s4); //Si s1 est� en el segment (s3, s4), el sistema �s compatible i la intersecci� �s s1
			result.p1 = s1;
        }
        else if (RealMath::equal(dv, 0.0f)) {	
			//El segment (s3, s4) en realitat �s un sol punt
            result.p1Valid = isBetween2D(s3, s1, s2);  //Si s3 est� en el segment (s1, s2), el sistema �s compatible i la intersecci� �s s3
			result.p1 = s3;
        }
		else {
			//S�n segments col�linears NO DEGENERATS, que potser tenen overlap o potser no
			Ogre::LogManager::getSingletonPtr()->logMessage("COLLINEAR NON DEGENERATED SEGMENTS");
			bool s1_in_s3_s4 = isBetween2D(s1, s3, s4);	//s1 est� contingut en el segment (s3, s4)
			bool s2_in_s3_s4 = isBetween2D(s2, s3, s4);	//s2 est� contingut en el segment (s3, s4)
			bool s3_in_s1_s2 = isBetween2D(s3, s1, s2);	//s3 est� contingut en el segment (s1, s2)
			bool s4_in_s1_s2 = isBetween2D(s4, s1, s2);	//s4 est� contingut en el segment (s1, s2)
			bool overlap = (
							s1_in_s3_s4 || s2_in_s3_s4 ||	//Part del segment (s1, s2) est� contingut en el segment (s3, s4)
							s3_in_s1_s2 || s4_in_s1_s2);	//Part del segment (s3, s4) est� contingut en el segment (s1, s2)
			result.p1Valid = result.p2Valid = overlap;
			if (overlap) {
				//Hi ha superposici� entre segments
				//1) Comprobo si algun dels segments est� completament contingut en l'altre segment
				if (s3_in_s1_s2 && s4_in_s1_s2) {
					//El segment (s3, s4) est� completament contingut en (s1, s2)
					result.p1 = s3; result.p2 = s4;
				}
				else if (s1_in_s3_s4 && s2_in_s3_s4) {
					//El segment (s1, s2) est� completament contingut en (s3, s4)
					result.p1 = s1; result.p2 = s2;
				}
				//2) Comprobo la resta de possibles casos
				else if (s3_in_s1_s2 && s2_in_s3_s4) {
					//El segment compartit �s el (s3, s2)
					result.p1 = s3; result.p2 = s2;
				}
				else if (s4_in_s1_s2 && s2_in_s3_s4) {
					//El segment compartit �s el (s4, s2)
					result.p1 = s4; result.p2 = s2;
				}
				else if (s3_in_s1_s2 && s1_in_s3_s4) {
					//El segment compartit �s el (s3, s1)
					result.p1 = s3; result.p2 = s1;
				}
				else if ((s4_in_s1_s2 && s1_in_s3_s4)) {
					//El segment compartit �s el (s4, s1)
					result.p1 = s4; result.p2 = s1;
				}
			}
		}
    }
}

float GeometrySolver::segmentSegmentSquaredDistance2D(Vec2D& s1, Vec2D& s2, Vec2D& s3, Vec2D& s4) {
	//Compute the projection of each endpoint against the other segment
	IntersectionPoint2D prS1, prS2, prS3, prS4;
	projectionPointSegment2D(s1, s3, s4, prS1);
	projectionPointSegment2D(s2, s3, s4, prS2);
	projectionPointSegment2D(s3, s1, s2, prS3);
	projectionPointSegment2D(s4, s1, s2, prS4);

	//Compute the distances from each endpoint to its projection in the other segment
	float d1 = s1.dist2(prS1.location);
	float d2 = s2.dist2(prS2.location);
	float d3 = s3.dist2(prS3.location);
	float d4 = s4.dist2(prS4.location);
	
	//The segment-segment distance is the minimum of those distances
	return std::min(d1, std::min(d2, std::min(d3, d4)));
}

void GeometrySolver::lineLineIntersection2D(Vec2D& s1, Vec2D& s2, Vec2D& s3, Vec2D& s4, IntersectionPoint2D& result) {
	
	result.isValid = false;

    Vec2D u = s2 - s1;
	Vec2D v = s4 - s3;
	Vec2D w = s1 - s3;
    
	float D = u.perpProduct(v);

	if (!RealMath::equal(D, 0.0f)) {
		//Cas simple: Els segments NO s�n colinears i per tant, interseccionen en un sol punt. Calculem la intersecci�
		//Dues l�nies no paral�leles sempre interseccionen en un �nic punt. 
		
		float sI = v.perpProduct(w) / D;
		result.t = sI;
		result.location = s1 + sI * u;
		result.isValid = true;
	}
}

void GeometrySolver::raySegmentIntersection2D(Ray2D& ray, Vec2D& s3, Vec2D& s4, IntersectionPoint2D& result) {
	
	result.isValid = false;
	
	Vec2D s1 = ray.getOrigin();
	Vec2D s2 = s1 + ray.getDirection();
    Vec2D u = s2 - s1;
	Vec2D v = s4 - s3;
	Vec2D w = s1 - s3;
    
	float D = u.perpProduct(v);

	if (!RealMath::equal(D, 0.0f)) {
		//Cas simple: Els segments NO s�n colinears i per tant, interseccionen en un sol punt. Calculem la intersecci�
		
		//Comprobo si el punt d'intersecci� cau en el raig
		float sI = v.perpProduct(w) / D;
		if (!RealMath::greaterOrEqual(sI, 0.0f)) return;
		
		//Comprobo si el punt d'intersecci� cau entre (s3, s4)
		float tI = u.perpProduct(w) / D;
		if (!RealMath::greaterOrEqual(tI, 0.0f) || !RealMath::lessOrEqual(tI, 1.0f)) return;	//El punt cau fora dels extrems (s1, s2)

		//Si arribem aqu� �s perqu� el punt d'intersecci� est� entre (s1, s2) i (s3, s4). Per tant, �s v�lid
		result.location = s1 + sI * u;
		result.isValid = true;
		result.t = sI;
	}
}

bool GeometrySolver::existLineCircleIntersection2D(Vec2D& s1, Vec2D& s2, Vec2D& center, float radius) {
	//1) Equacio de la recta
	//======================
	//p = s1 + t*vDir

	//2) Equaci� de la circunfer�ncia
	//===============================
	//(p - o)*(p - o) - r^2 = 0

	//Substituir 1 en 2 and simplify:
	//t^2*(vDir�vDir) + t*2*vDir�(s1 - center) + (s1 - center)�(s1 - center) - r^2 = 0

	Vec2D vDir = s2 - s1;
	Vec2D w = s1 - center;

	//Check if the discriminant is positive, so there is an intersection between the line suporting the segment
	//and the circunference. Notice that even if an intersection with the supporting line exists, this not guarantees
	//that it intersects also with the finite segment. 
	float a = vDir.dotProduct(vDir);
	float b = 2.0f * vDir.dotProduct(w); 
	float c = w.dotProduct(w) - (radius * radius);
	float discriminant = (b*b) - 4.0f*a*c;
	return RealMath::greaterOrEqual(discriminant, 0.0f);
}

void GeometrySolver::segmentCircleIntersection2D(Vec2D& s1, Vec2D& s2, Vec2D& center, float radius, SegmentIntersection2D& result) {
	//1) Equacio de la recta
	//======================
	//p = s1 + t*vDir

	//2) Equaci� de la circunfer�ncia
	//===============================
	//(p - o)*(p - o) - r^2 = 0

	//Substituir 1 en 2 and simplify:
	//t^2*(vDir�vDir) + t*2*vDir�(s1 - center) + (s1 - center)�(s1 - center) - r^2 = 0

	result.p1Valid = false;
	result.p2Valid = false;

	Vec2D vDir = s2 - s1;
	Vec2D w = s1 - center;

	//Check if the discriminant is positive, so there is an intersection between the line suporting the segment
	//and the circunference. Notice that even if an intersection with the supporting line exists, this not guarantees
	//that it intersects also with the finite segment. 
	float a = vDir.dotProduct(vDir);
	float b = 2.0f * vDir.dotProduct(w); 
	float c = w.dotProduct(w) - (radius * radius);
	float discriminant = (b*b) - 4.0f*a*c;
	if (RealMath::greaterOrEqual(discriminant, 0.0f)) {
		//There is intersection. Determine the intersection type and choose the appropiate root
		float root1 = (-b + std::sqrtf(discriminant)) / (2.0f * a);
		float root2 = (-b - std::sqrtf(discriminant)) / (2.0f * a);
		if ((root1 < 0) && (root2 < 0)) return;	//Both roots are negative, so the circunference DOES NOT intersect the segment
		
		//If we arrive here is because at least one root is positive, 
		//i.e., represents a valid intersection between the segment and the circunference
		if (RealMath::greaterOrEqual(root1, 0.0f) && RealMath::greaterOrEqual(root2, 0.0f)) {
			//Case1: Both roots are positive. 
			result.p1Valid = RealMath::lessOrEqual(root1, 1.0f);
			result.p1 = s1 + vDir * root1;

			result.p2Valid = RealMath::lessOrEqual(root2, 1.0f);
			result.p2 = s1 + vDir * root2;
		}
		else {
			//Case2: One of the roots is negative. So we choose the positive one
			float t = std::max(root1, root2);
			result.p1Valid = RealMath::lessOrEqual(t, 1.0f);
			result.p1 = s1 + vDir * t;
			//As one of the roots is negative, using max we ensure that the final result
			//will be the positive root. 
		}
	}
}

bool GeometrySolver::existSegmentCircleIntersection2D(Vec2D& s1, Vec2D& s2, Vec2D& center, float radius) {
	//1) Equacio de la recta
	//======================
	//p = s1 + t*vDir

	//2) Equaci� de la circunfer�ncia
	//===============================
	//(p - o)*(p - o) - r^2 = 0

	//Substituir 1 en 2 and simplify:
	//t^2*(vDir�vDir) + t*2*vDir�(s1 - center) + (s1 - center)�(s1 - center) - r^2 = 0

	Vec2D vDir = s2 - s1;
	Vec2D w = s1 - center;

	//Check if the discriminant is positive, so there is an intersection between the line suporting the segment
	//and the circunference. Notice that even if an intersection with the supporting line exists, this not guarantees
	//that it intersects also with the finite segment. 
	float a = vDir.dotProduct(vDir);
	float b = 2.0f * vDir.dotProduct(w); 
	float c = w.dotProduct(w) - (radius * radius);
	float discriminant = (b*b) - 4.0f*a*c;
	if (discriminant < 0.0f) return false;

	float root1 = (-b + std::sqrtf(discriminant)) / (2.0f * a);
	float root2 = (-b - std::sqrtf(discriminant)) / (2.0f * a);
	return (RealMath::greaterOrEqual(root1, 0.0f) || RealMath::greaterOrEqual(root2, 0.0f));
}

bool GeometrySolver::isBetween(Vec3D& v, Vec3D& a, Vec3D& b) {
	//indica si el v�rtex v est� entre els v�rtexs a i b. Se suposa
	//que (a,b,v) s�n colinears
	float length = a.dist2(b); //Dist�ncia entre els extrems despla�ats de l'aresta
	float d1 = v.dist2(a);		//Dist�ncia entre el v�rtex v i l'extrem A
	float d2 = v.dist2(b);		//Dist�ncia entre el v�rtex v i l'extrem B

	return (RealMath::lessOrEqual(d1, length) && (RealMath::lessOrEqual(d2, length)));
}

bool GeometrySolver::isBetween2D(Vec2D& v, Vec2D& a, Vec2D& b) {
	//indica si el v�rtex v est� entre els v�rtexs a i b. Se suposa
	//que (a,b,v) s�n colinears
	float length = a.dist2(b); //Dist�ncia entre els extrems despla�ats de l'aresta
	float d1 = v.dist2(a);		//Dist�ncia entre el v�rtex v i l'extrem A
	float d2 = v.dist2(b);		//Dist�ncia entre el v�rtex v i l'extrem B

	return (RealMath::lessOrEqual(d1, length) && (RealMath::lessOrEqual(d2, length)));
}

int GeometrySolver::orientationTest(Vec2D& p, Vec2D& q, Vec2D& r, bool normalize) {
	//Calcula l'�rea del paral�lelogram format pels punts p,q,r

	//El producte vectorial AxB ens d�na com a resultat un altre vector C que �s perpendicular als dos primers. 
	//El m�dul d'aquest vector C, correspon a l'�rea del paral�lelogram definit pels vectors A i B. 

	//Si area > 0 => p,q,r en sentit ANTIHORARI
	//Si area < 0 => p,q,r en sentit HORARI
	//Si area = 0 => p,q,r, COLINEARS

	//Aquest test tamb� es pot aplicar per saber si un punt est� a la DRETA o a 
	//L'ESQUERRA en relaci� a una aresta orientada. Si suposem que l'aresta
	//ve donada pels punts (p,q), aleshores:

	//r est� a la DRETA de (p,q) si area < 0 (perqu� es forma un gir HORARI)
	//r est� a l'ESQUERRA de (p,q) si area > 0 (perqu� es forma un gir ANTIHORARI)
	//r est� ALINEAT amb (p,q) si area = 0

	//El m�dul del producte vectorial es pot escriure com:
	// |A x B| = |A||B|sin(a)
	//on a �s l'angle entre els vectors A i B. Per tant, si A i B s�n vectors unitaris, el resultat
	//del c�lcul ser� el sinus de l'angle que formen. 

	//En realitat, el que estem calculant �s el producte vectorial (cross product)
	//entre els vectors PR i PQ. Si aquests vectors estan normalitzats, el resultat del 
	//producte vectorial �s el sinus de l'angle que formen aquests dos vectors. 
	/*Ogre::LogManager::getSingletonPtr()->logMessage("p.x = " + std::to_string(p->getX()));
	Ogre::LogManager::getSingletonPtr()->logMessage("p.y = " + std::to_string(p->getY()));
	Ogre::LogManager::getSingletonPtr()->logMessage("q.x = " + std::to_string(q->getX()));
	Ogre::LogManager::getSingletonPtr()->logMessage("q.y = " + std::to_string(q->getY()));
	Ogre::LogManager::getSingletonPtr()->logMessage("r.x = " + std::to_string(r->getX()));
	Ogre::LogManager::getSingletonPtr()->logMessage("r.y = " + std::to_string(r->getY()));*/

	Vec2D dirPR = r - p;
	Vec2D dirPQ = q - p;

	if (normalize) {
		dirPR.normalize();
		dirPQ.normalize();
	}

	/*Ogre::LogManager::getSingletonPtr()->logMessage("pr.x = " + std::to_string(dirPR.getX()));
	Ogre::LogManager::getSingletonPtr()->logMessage("pr.y = " + std::to_string(dirPR.getY()));
	Ogre::LogManager::getSingletonPtr()->logMessage("pq.x = " + std::to_string(dirPQ.getX()));
	Ogre::LogManager::getSingletonPtr()->logMessage("pq.y = " + std::to_string(dirPQ.getY()));*/

	//Sinus de l'angle format pels vectors PQ i PR
	float orientation = ((dirPQ.getX()) * (dirPR.getY()) - (dirPR.getX()) * (dirPQ.getY()));
	//Ogre::LogManager::getSingletonPtr()->logMessage("orientation = " + std::to_string(orientation));

	//Comprobo si s�n col�linears
	if (RealMath::equal(orientation, 0.0f)) return 0;

	//Comprobo si el gir �s ANTIHORARI (r est� a l'esquerra del vector pq)
	if (orientation > 0.0f) return 1;

	//Si no, �s que el gir �s HORARI (r est� a la dreta del vector pq)
	return -1;
}

float GeometrySolver::angleBetweenVectors(Vec2D& v1, Vec2D& v2, AngleTurnEnum aTurn) {
	//Calcula l'angle que hi ha entre 2 vectors. S'ha d'entendre l'angle
	//com al GIR CAP A LA DRETA del vector v1 cap al vector v2

	//NOTA: v1 i v2 han de tenir el mateix origen
	std::vector<float> u;
	u.push_back(v1.getX());
	u.push_back(v1.getY());

	std::vector<float> v;
	v.push_back(v2.getX());
	v.push_back(v2.getY());

	float angle = (atan2(v.at(1), v.at(0)) - atan2(u.at(1), u.at(0))) * 180.0f / Ogre::Math::PI;
	//Ogre::LogManager::getSingletonPtr()->logMessage("ANGLE BEFORE CORRECTION = " + std::to_string(angle));
	//l'angle calculat s'ha d'entendre com el gir m�s curt per anar de v1 a v2. Per tant, si �s negatiu haig de corregir l'angle	
	if (angle < 0.0f) angle += 360.0f;
	if (aTurn == ANGLE_TURN_RIGHT) {
		//angle actualment cont� el GIR CAP A L'ESQUERRA. Calculo el GIR CAP A LA DRETA
		angle = 360.0f - angle;
	}
	if (RealMath::equal(angle, 0.0f) || RealMath::equal(angle, 360.0f, 0.01f)) angle = 0.0f;
	//Ogre::LogManager::getSingletonPtr()->logMessage("ANGLE AFTER CORRECTION = " + std::to_string(angle));

	return angle;
}

/****************************************************************/
/* POLYGON FUNCTIONS */
/****************************************************************/
int GeometrySolver::polygonOrientation(Poly& p, RealMath::ProjectionPlaneEnum projectionPlane) {
	//Buscar un v�rtex que est� a l'envolvent convexa. 
	//Per exemple, el v�rtex m�s a l'esquerra
	std::vector<Vec2D> pProjected;
	to2D(p, pProjected, projectionPlane);
	size_t vLeftMostID = 0;
	for (size_t i = 1; i < pProjected.size(); i++) {
		if (pProjected[i].getX() < pProjected[vLeftMostID].getX()) {
			vLeftMostID = i;
		}
	}
	//Al finalitzar el proc�s, vLeftMost cont� el v�rtex del 
	//pol�gon que est� m�s a l'esquerra i per tant, forma part de la convex hull. 
	//Comprovo la orientaci�
	Vec2D vLeftMost = pProjected[vLeftMostID];
	
	Vec2D vPrev;
	if (vLeftMostID == 0) vPrev = pProjected[pProjected.size() - 1];
	else vPrev = pProjected[vLeftMostID -1];
	
	Vec2D vNext;
	if (vLeftMostID == (pProjected.size() - 1)) vNext = pProjected[0];
	else vNext = pProjected[vLeftMostID + 1];

	return orientationTest(vPrev, vLeftMost, vNext);
}

bool GeometrySolver::polygonIntersection(Poly& p1, Poly& p2, RealMath::ProjectionPlaneEnum projectionPlane) {
	//Indca si els pol�gons p1 i p2 interseccionen. 
	//Se suposa que p1 i p2 s�n pol�gons simples. 
	//0) Projectem els pol�gons sobre el pl�
	std::vector<Vec2D> pProjected1;
	to2D(p1, pProjected1, projectionPlane);
	std::vector<Vec2D> pProjected2;
	to2D(p2, pProjected2, projectionPlane);
	//Test1: Comprobar si algun dels v�rtexs de self est� dins de p o viceversa. 
	if (polygonInPolygon(pProjected1, pProjected2)) return true;	//Algun dels punts de p1 est� dins del pol�gon p2?
	if (polygonInPolygon(pProjected2, pProjected1)) return true;	//Algun dels punts de p2 est� dins del pol�gon p1?
	
	//Test2: Comprobar intersecci� entre arestes. Aquest test �s necessari ja que
	//encara que el Test1 falli, no vol dir que els dos pol�gons no interseccionin. 
	bool test2 = false;
	size_t i = 0;
	size_t numVerts = pProjected1.size();
	while (!test2 && (i < numVerts)) {
		test2 = edgeIntersection(pProjected1.at(i), pProjected1.at((i + 1) % numVerts), pProjected2);
		if (!test2) i++;
	}

	return test2;
}

bool GeometrySolver::polygonInPolygon(std::vector<Vec2D>& pProjected1, std::vector<Vec2D>& pProjected2) {

	//Indica si el pol�gon self intersecciona amb el pol�gon p. 
	//Es produeix intersecci� quan algun dels punts de self 
	//est� dins del pol�gon p

	std::vector<Vec2D>::iterator it = pProjected1.begin();
	bool inside = false;
	while (!inside && (it != pProjected1.end())) {
		inside = pointInPolygon(*it, pProjected2);
		it++;
	}
	return inside;
}

bool GeometrySolver::pointInPolygon(Vec2D& v, std::vector<Vec2D>& pProjected) {

	//Indica si el Vertex v es troba dins el pol�gon self

	//  The function will return YES if the point x,y is inside the polygon, or
	//  NO if it is not.  If the point is exactly on the edge of the polygon,
	//  then the function may return YES. 
	//
	//  Note that division by zero is avoided because the division is protected
	//  by the "if" clause which surrounds it.

	size_t numVerts = pProjected.size();
	size_t j = numVerts - 1;
	bool oddNodes = false;

	float x = v.getX(); //Coordenada X del v�rtex sobre el que estem fent el test
	float y = v.getY(); //Coordenada Y del v�rtex sobre el que estem fent el test

	Vec2D* v1;
	Vec2D* v2;

	for (size_t i = 0; i < numVerts; i++) {
		v1 = &pProjected.at(i);
		v2 = &pProjected.at(j);
		if (((v1->getY() < y) && (v2->getY() >= y))
			|| ((v2->getY() < y) && (v1->getY() >= y))) { 
				//hi ha intersecci� entre l'aresta (i,j) i el raig horitzontal que surt del punt de test
		  
			if (v1->getX() + (y - v1->getY()) / (v2->getY() - v1->getY()) * (v2->getX() - v1->getX()) <= x) {
				oddNodes = !oddNodes; 
			}
		}
		j = i; 
	}
	return oddNodes; 
}

bool GeometrySolver::pointInPolygon(Vec3D& v, Poly& p, RealMath::ProjectionPlaneEnum projectionPlane) {
	std::vector<Vec2D> pProjected;
	to2D(p, pProjected, projectionPlane);	//Projecci� del pol�gon sobre el pl�
	Vec2D v2D = to2D(v, projectionPlane);	//Projecci� del punt sobre el pl�
	return pointInPolygon(v2D, pProjected);
}

bool GeometrySolver::edgeIntersection(Vec2D& v1, Vec2D& v2, std::vector<Vec2D>& pProjected) {
	//Comproba si alguna de les arestes de self intersecciona amb l'aresta formada 
	//pels v�rtexs v1, v2
	Vec2D *eV1, *eV2;
	size_t i = 0;
	size_t numVerts = pProjected.size();
	SegmentIntersection2D intersection;
	intersection.p1Valid = false;
	while (!intersection.p1Valid && (i < numVerts)) {
		eV1 = &(pProjected.at(i));
		eV2 = &(pProjected.at((i + 1) % numVerts));
		GeometrySolver::segmentSegmentIntersection2D(v1, v2, *eV1, *eV2, intersection);
		if (!intersection.p1Valid) i++;
	}
	return intersection.p1Valid;
}

