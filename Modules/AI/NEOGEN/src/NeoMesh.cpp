#include "NeoMesh.h"
#include "NeoPSimpl.h"
#include "NeoPoly2Tri.h"

#include <OgreLogManager.h>
#include <OgreStringConverter.h>

using namespace NEOGEN;
using namespace NEOGEN::NeoGeometrySolver;

#pragma region CREATION AND DESTRUCTION

NeoMesh::NeoMesh(const NeoAABB& aabb) {
	init();
	
	_aabb = aabb;
	_octree = NeoOctreeFaceUPtr(new NeoOctreeFace(_aabb));
}

NeoMesh::NeoMesh(Vector3List& pointList, ElementIDVector& triangleList) {
	init();

	//Create the vertices of the mesh
	for (Vector3List::iterator it = pointList.begin(); it != pointList.end(); it++) {
		createVertex(*it);
	}

	_octree = NeoOctreeFaceUPtr(new NeoOctreeFace(_aabb));

	//Create the edges and faces
	for (ElementIDVector::iterator it = triangleList.begin(); it != triangleList.end(); it += 3) {
		Polygon3 polygon;
		polygon.push_back(pointList[*it]);
		polygon.push_back(pointList[*(it + 1)]);
		polygon.push_back(pointList[*(it + 2)]);
		
		//NeoPSimpl::cleanPolygon<Vector3, Vector3Cmp>(polygon);
		createFace(polygon);
	}

	//refreshOctree();
}

NeoMesh::~NeoMesh() {
	//Destroy all the elements that conforms the Mesh. 
	_faceMap.clear();
	_edgeMap.clear();
	_vertexMap.clear();

	_edgeMapEndpoints.clear();
	_vertexMapPosition.clear();
}

void NeoMesh::init() {
	_newVertexID = 1;
	_newEdgeID = 1;
	_newFaceID = 1;
	_aabb.clear();
	_octree = NULL;
	_is2D = true;
}

NeoVertex* NeoMesh::createVertex(const Vector3& position) {
	NeoVertex* vertex = getVertex(position);
	if (vertex) return vertex;	//The vertex already exists

	//At this point, we are creating a new vertex
	ElementID vertexID = _newVertexID++;
	_vertexMap[vertexID] = NeoVertexUPtr(new NeoVertex(vertexID, position));
	vertex = _vertexMap[vertexID].get();
	_vertexMapPosition[position] = vertex;

	_aabb.addPoint(position);

	_is2D = _is2D && (position.getY() == NeoReal(0));

	return vertex;
}

NeoEdge* NeoMesh::createEdge(NeoVertex* v1, NeoVertex* v2) {
	NeoEdge* edge = getEdge(v1, v2);
	//if (edge != NULL) Ogre::LogManager::getSingletonPtr()->logMessage("EDGE ALREADY EXISTS!!!");
	if (edge) return edge;	//The edge already exists

	//At this point, we are creating a new edge
	ElementID edgeID = _newEdgeID++;
	_edgeMap[edgeID] = NeoEdgeUPtr(new NeoEdge(edgeID, v1, v2));
	edge = _edgeMap[edgeID].get();
	_edgeMapEndpoints[NeoVertexPair(v1, v2)] = edge;

	//Check if the opposite edge has already been created and define the pointers accordingly
	NeoEdge* edgeOpposite = getEdge(v2, v1);
	if (edgeOpposite) {
		edge->setOpposite(edgeOpposite);
		edgeOpposite->setOpposite(edge);
	}

	v1->addIncidentEdge(edge);
	v2->addIncidentEdge(edge);

	return edge;
}

NeoEdge* NeoMesh::createEdge(ElementID v1ID, ElementID v2ID) {
	return createEdge(getVertex(v1ID), getVertex(v2ID));
}

NeoEdge* NeoMesh::createEdge(const Vector3& pos1, const Vector3& pos2) {
	NeoVertex* v1 = createVertex(pos1);
	NeoVertex* v2 = createVertex(pos2);
	return createEdge(v1, v2);
}

NeoEdgePair NeoMesh::subdivideEdge(NeoEdge* edge, NeoVertex* v, bool subdivideOpposite) {
	if ((v == edge->getVertexBegin()) || (v == edge->getVertexEnd())) return NeoEdgePair(edge, edge);

	NeoVertex* eBegin = edge->getVertexBegin();
	NeoVertex* eEnd = edge->getVertexEnd();
	NeoEdgePair subEdges = NeoEdgePair(createEdge(eBegin, v), createEdge(v, eEnd));
	
	subEdges.first->setTypeWall(edge->isTypeWall());
	subEdges.second->setTypeWall(edge->isTypeWall());

	//Redo the face with the new contour. 
	NeoFace* face = edge->getFace();
	if (face) {
		NeoEdgePVector edges = face->getEdges();

		face->begin();

		for (NeoEdgePVector::iterator it = edges.begin(); it != edges.end(); it++) {
			if (*it == edge) {
				face->addEdge(subEdges.first);
				face->addEdge(subEdges.second);
			}
			else face->addEdge(*it);
		}

		face->end();
	}
	edge->clearIncidentFaces();	//Avoid destroying the face when destroying the edge. 

	if (subdivideOpposite) {
		//Subdivide the opposite of this edge (if any)
		NeoEdge* opposite = edge->getOpposite();
		if (opposite) {
			opposite->setOpposite(NULL);	//Avoid re-subdividing this edge again. 
			subdivideEdge(opposite, v);
		}
	}
	edge->setOpposite(NULL);

	destroyEdge(edge);
	
	return subEdges;
}

NeoEdgePair NeoMesh::subdivideEdge(NeoEdge* edge, const Vector3& point, bool subdivideOpposite) {
	return subdivideEdge(edge, createVertex(point));
}

NeoFace* NeoMesh::createFace(const Polygon3& polygon, ElementID faceID) {
	//We need at least 3 non-collinear vertices to create a face
	Vector3 normal = getPolygonNormal(polygon);
	if (normal.equal(Vector3(0, 0, 0))) return NULL;

	if (faceID == 0) faceID = _newFaceID++;
	_faceMap[faceID] = NeoFaceUPtr(new NeoFace(faceID));
	NeoFace* face = getFace(faceID);

	//Create all the edges conforming the face. 
	face->begin();
	size_t numPoints = polygon.size();
	for (size_t i = 0; i < numPoints; i++) {
		Vector3 pos1 = polygon[i];
		Vector3 pos2 = polygon[(i + 1) % numPoints];
		face->addEdge(createEdge(pos1, pos2));
	}
	face->end();

	_octree->insert(face);

	return face;
}

void NeoMesh::destroyVertex(NeoVertex* vertex) {
	if (!vertex) return;

	//Deletes the vertex. As side effects, it will also delete edges and faces using this vertex. 
	NeoEdgePSet incidentEdges = vertex->getIncidentEdges();
	for (NeoEdgePSet::iterator it = incidentEdges.begin(); it != incidentEdges.end(); it++) {
		destroyEdge(*it);
	}

	_vertexMapPosition.erase(vertex->getPosition());
	_vertexMap.erase(vertex->getID());
}

void NeoMesh::destroyEdge(NeoEdge* edge) {
	if (!edge) return;

	//Deletes the edge. As side effects, it will also delete faces using one or more of the deleted edges. 
	//However, vertices defining the deleted edges are not deleted, unless they remain unlinked (i.e. no more edge use them).
	destroyFace(edge->getFace());

	//This edge is no more an incident edge of its endpoints. 
	NeoVertex* vBegin = edge->getVertexBegin();
	NeoVertex* vEnd = edge->getVertexEnd();
	
	_edgeMapEndpoints.erase(NeoVertexPair(vBegin, vEnd));
	_edgeMap.erase(edge->getID());

	if (vBegin->getNumIncidentEdges() == 0) destroyVertex(vBegin);
	if (vEnd->getNumIncidentEdges() == 0) destroyVertex(vEnd);
}

void NeoMesh::destroyFace(NeoFace* face) {
	if (!face) return;

	//Deletes the face. Vertices and edges defining the deleted face are not deleted, unless they remain unlinked (i.e. no more face use them).
	NeoEdgePVector edges = face->getEdges();

	_octree->remove(face);
	_faceMap.erase(face->getID());

	//Remove the face from the incidentFaces list of all its edges, and remove them if the edge does not have any more adjacent faces. 
	for (NeoEdgePVector::iterator it = edges.begin(); it != edges.end(); it++) {
		NeoEdge* e = *it;
		if (e->getIncidentFaces().empty()) destroyEdge(e);
	}
}

void NeoMesh::destroyFaceOnly(NeoFace* face) {
	if (!face) return;

	_octree->remove(face);
	_faceMap.erase(face->getID());
}

void NeoMesh::destroyFaceAndEdgesOnly(NeoFace* face) {
	if (!face) return;

	NeoEdgePVector edges = face->getEdges();
	destroyFaceOnly(face);
	for (NeoEdgePVector::iterator it = edges.begin(); it != edges.end(); it++) {
		NeoEdge* edge = *it;
		_edgeMapEndpoints.erase(NeoVertexPair(edge->getVertexBegin(), edge->getVertexEnd()));
		_edgeMap.erase(edge->getID());
	}
}

#pragma endregion

#pragma region GET AND SET

NeoVertex* NeoMesh::getVertex(ElementID vertexID) {
	NeoVertexMap::iterator it = _vertexMap.find(vertexID);
	if (it == _vertexMap.end()) return NULL;
	return it->second.get();
}

NeoVertex* NeoMesh::getVertex(const Vector3& position) {
	NeoVertexMapPosition::iterator it = _vertexMapPosition.find(position);
	if (it == _vertexMapPosition.end()) return NULL;
	return it->second;
}

NeoEdge* NeoMesh::getEdge(ElementID edgeID) {
	NeoEdgeMap::iterator it = _edgeMap.find(edgeID);
	if (it == _edgeMap.end()) return NULL;
	return it->second.get();
}

NeoEdge* NeoMesh::getEdge(NeoVertex* v1, NeoVertex* v2) {
	if (!v1 || !v2) return NULL;

	NeoVertexPair key = NeoVertexPair(v1, v2);
	NeoEdgeMapEndpoints::iterator it = _edgeMapEndpoints.find(key);
	if (it == _edgeMapEndpoints.end()) return NULL;
	return it->second;
}

NeoEdge* NeoMesh::getEdge(const Vector3& posBegin, const Vector3& posEnd) {
	return getEdge(getVertex(posBegin), getVertex(posEnd));
}

NeoFace* NeoMesh::getFace(ElementID faceID) {
	NeoFaceMap::iterator it = _faceMap.find(faceID);
	if (it == _faceMap.end()) return NULL;
	return _faceMap[faceID].get();
}

NeoVertexMap& NeoMesh::getVertices() {
	return _vertexMap;
}

NeoEdgeMap& NeoMesh::getEdges() {
	return _edgeMap;
}

NeoFaceMap& NeoMesh::getFaces() {
	return _faceMap;
}

void NeoMesh::getFaceMapKeys(ElementIDSet& result) {
	for (NeoFaceMap::iterator it = _faceMap.begin(); it != _faceMap.end(); it++) {
		result.insert(it->first);
	}
}

size_t NeoMesh::getNumVertices() {
	return _vertexMap.size();
}

size_t NeoMesh::getNumEdges() {
	return _edgeMap.size();
}

size_t NeoMesh::getNumFaces() {
	return _faceMap.size();
}

const NeoAABB& NeoMesh::getNeoAABB() {
	return _aabb;
}

NeoOctreeFace* NeoMesh::getOctree() {
	return _octree.get();
}

#pragma endregion

#pragma region UPDATE

void NeoMesh::removeDuplicatedVertices() {
	ElementIDVector vertexMapKeys;
	//getVertexMapKeys();

}

void NeoMesh::sew() {
	for (NeoVertexMap::iterator it = _vertexMap.begin(); it != _vertexMap.end(); it++) {
		sew(it->second.get());
	}
}

void NeoMesh::sew(NeoVertex* v) {
	//Compute the intersected faces by this vertex
	NeoReal offset = Math::NEO_REAL_PRECISION * 2;
	Vector3 u = Vector3(1, 1, 1).normalized();
	ElementIDSet faces;
	NeoAABB aabb(v->getPosition() - u * offset, v->getPosition() + u * offset);
	_octree->computeIntersectedObjects(aabb, faces);

	//For each face, check the edges and see if this vertex intersects any edge at some intermediate point. 
	for (ElementIDSet::iterator itFace = faces.begin(); itFace != faces.end(); itFace++) {
		NeoFace* f = getFace(*itFace);
		if (!f) continue;

		NeoEdgePVector& edges = f->getEdges();
		NeoEdge* intersectedEdge = NULL;

		for (NeoEdgePVector::iterator itEdge = edges.begin(); (itEdge != edges.end()) && !intersectedEdge; itEdge++) {
			NeoEdge* e = *itEdge;
			if ((e->getVertexBegin() == v) || (e->getVertexEnd() == v)) continue;
			
			if (isPointInSegment(v->getPosition(), e->getVertexBegin()->getPosition(), e->getVertexEnd()->getPosition())) {
				intersectedEdge = e;
			}
		}
		if (!intersectedEdge) continue;	

		//At this point, vertex v intersects intersectedEdge of face f in a point in between its endpoints. Subdivide this 
		//edge and recreate the face. 
		Polygon3 contourOriginal, contourNew;
		f->getContour(contourOriginal);
		const Vector3& eBegin = intersectedEdge->getVertexBegin()->getPosition();
		const Vector3& eEnd = intersectedEdge->getVertexEnd()->getPosition();
		
		for (size_t i = 0; i < contourOriginal.size(); i++) {
			Vector3 vCurrent = contourOriginal[i];
			Vector3 vNext = contourOriginal[(i + 1) % contourOriginal.size()];
			contourNew.push_back(vCurrent);
			if (vCurrent.equal(eBegin) && vNext.equal(eEnd)) contourNew.push_back(v->getPosition());
		}

		destroyFace(f);
		createFace(contourNew);
	}
}

void NeoMesh::simplify() {
	//Simplifies the whole mesh. 
	ElementIDSet faceMapKeys;
	getFaceMapKeys(faceMapKeys);
	simplify(faceMapKeys);
}

void NeoMesh::simplify(const ElementIDSet& faces) {
	//Simplifies a specific set of faces. 
	//for (ElementIDVector::const_iterator it = faceMapKeys.begin(); it != faceMapKeys.end(); it++) {
	//	NeoFace* face = getFace(*it);
	//	if (!face) continue;

	//	NeoFacePSet adjacentFaces;
	//	do {
	//		adjacentFaces.clear();
	//		face->getAdjacentsCoplanar(adjacentFaces);
	//		if (!adjacentFaces.empty()) {
	//			ElementID idPrev = face->getID();
	//			face = mergeFaces(face, *adjacentFaces.begin());
	//			if (idPrev == face->getID()) break;	//The merge process has failed because the result was not monotone. 
	//		}
	//	} while (!adjacentFaces.empty());
	//}

	for (ElementIDSet::const_iterator itFace = faces.begin(); itFace != faces.end(); itFace++) {
		NeoFace* f = getFace(*itFace);
		if (!f) continue;

		//1) f is the first face of a new coplanar patch. Compute this patch. 
		ElementIDSet coplanarPatch;
		getCoplanarPatch(f, faces, coplanarPatch);

		//2) get the rings (contours) contained in the patch. 
		Polygons3 rings;
		getRings(coplanarPatch, rings);

		//3) Destroy all the faces of the patch and create the new ones using the computed rings. 
		Polygons3::iterator itFloor = rings.end(); //The position of the floor in the rings vector. 
		NeoReal floorArea = 0;
		for (Polygons3::iterator it = rings.begin(); it != rings.end(); it++) {
			const Polygon3& p = *it;
			NeoReal a = polygonArea(p);
			if (Math::greater(a, floorArea)) {
				floorArea = a;
				itFloor = it;
			}
		}
		Polygon3 floor = *itFloor;
		rings.erase(itFloor);
		Polygons3 result;
		NeoPoly2Tri::triangulate(floor, rings, result);

		for (ElementIDSet::iterator itPatch = coplanarPatch.begin(); itPatch != coplanarPatch.end(); itPatch++) {
			destroyFace(getFace(*itPatch));
		}

		for (Polygons3::iterator it = result.begin(); it != result.end(); it++) {
			createFace(*it);
		}
	}
}

void NeoMesh::getCoplanarPatch(NeoFace* face, const ElementIDSet& facesCandidate, ElementIDSet& result) {
	if (facesCandidate.find(face->getID()) == facesCandidate.end()) return;	//The face does not belong to the set we want to simplify. 
	if (result.find(face->getID()) != result.end()) return;					//The face already belongs to the patch. 
	
	//NeoFacePSet nonValidFaces;
	//face->getNeighborsNonManifold(nonValidFaces);
	//for (NeoFacePSet::iterator it = nonValidFaces.begin(); it != nonValidFaces.end(); it++) {
	//	if (result.find((*it)->getID()) != result.end()) return; //a nonManifold neighbor is in the patch, so face cannot be on the same patch. 
	//}

	//At this point, the face belongs to the patch. 
	result.insert(face->getID());
	NeoFacePSet adjacentFaces;
	face->getAdjacentsCoplanar(adjacentFaces);
	for (NeoFacePSet::iterator it = adjacentFaces.begin(); it != adjacentFaces.end(); it++) {
		getCoplanarPatch(*it, facesCandidate, result);
	}
}

void NeoMesh::getRings(const ElementIDSet& faces, Polygons3& result) {
	NeoEdgePSet ringEdges;	//Edges already assigned to a ring
	while (true) {
		//1) Find an edge of a ring. 
		NeoEdge* startEdge = NULL;
		NeoFace* fCurrent = NULL;
		for (ElementIDSet::const_iterator itFace = faces.begin(); (itFace != faces.end()) && !startEdge; itFace++) {
			fCurrent = getFace(*itFace);
			NeoEdgePVector& edges = fCurrent->getEdges();

			for (NeoEdgePVector::iterator itEdge = edges.begin(); (itEdge != edges.end()) && !startEdge; itEdge++) {
				NeoEdge* e = *itEdge;
				if (ringEdges.find(e) != ringEdges.end()) continue;	//This edge has already been assigned to a ring. 

				if (!e->getOpposite() || (faces.find(e->getOpposite()->getFace()->getID()) == faces.end())) {
					startEdge = e;
				}
			}
		}
		if (!startEdge) break;	//There are no more contours. 

		//2) At this point, startEdge is the edge of a new contour. 
		NeoEdge* eCurrent = startEdge;
		Polygon3 ring;
		do {
			ring.push_back(eCurrent->getVertexBegin()->getPosition());
			ringEdges.insert(eCurrent);

			//Get the next obstacle edge
			eCurrent = fCurrent->getNextEdge(eCurrent);
			NeoFace* fNext = getRingNextFace(eCurrent, faces);
			while (fNext) {
				fCurrent = fNext;
				eCurrent = fCurrent->getNextEdge(eCurrent->getOpposite());
				fNext = getRingNextFace(eCurrent, faces);
			}

			/*eCurrent = eCurrent->getFace()->getNextEdge(eCurrent);
			while (eCurrent->getOpposite() && (faces.find(eCurrent->getOpposite()->getFace()->getID()) != faces.end())) {
				eCurrent = eCurrent->getOpposite()->getFace()->getNextEdge(eCurrent->getOpposite());
			}*/
		} while (eCurrent != startEdge);
		NeoPSimpl::simplifyRadialDistance(ring);
		NeoPSimpl::simplifyPerpendicularDistance(ring);
		result.push_back(ring);
	}
}

NeoFace* NeoMesh::getRingNextFace(NeoEdge* eCurrent, const ElementIDSet& faces) {
	if (!eCurrent->getOpposite()) return NULL;

	const NeoFacePSet& facesAdjacent = eCurrent->getOpposite()->getIncidentFaces();
	for (NeoFacePSet::const_iterator it = facesAdjacent.begin(); it != facesAdjacent.end(); it++) {
		if (faces.find((*it)->getID()) != faces.end()) return *it;
	}

	return NULL;
}

NeoFace* NeoMesh::mergeFaces(NeoFace* f1, NeoFace* f2) {
	if (!f1->isAdjacent(f2)) return f1;

	/*Ogre::LogManager::getSingletonPtr()->logMessage("======================================");
	Ogre::LogManager::getSingletonPtr()->logMessage("MERGING FACES!!!");
	Ogre::LogManager::getSingletonPtr()->logMessage("======================================");
	f1->log();
	f2->log();*/

	//1) Find an edge that will be for sure in the contour of the merged faces. This is
	//any of the edges that is not shared by both faces. 
	NeoEdgePSet sharedEdges, uniqueEdges;
	f1->getEdgesTo(f2, sharedEdges);
	f2->getEdgesTo(f1, sharedEdges);
	uniqueEdges.insert(f1->getEdges().begin(), f1->getEdges().end());
	uniqueEdges.insert(f2->getEdges().begin(), f2->getEdges().end());
	for (NeoEdgePSet::iterator it = sharedEdges.begin(); it != sharedEdges.end(); it++) uniqueEdges.erase(*it);

	NeoEdge* startEdge = *uniqueEdges.begin();
	
	//2) Reconstruct the contour, taking special care when hitting an edge that is shared by 
	//both faces. 
	NeoEdge* currentEdge = startEdge;
	Polygon3 contour;
	do {
		contour.push_back(currentEdge->getVertexBegin()->getPosition());
		//Get the next edge
		NeoEdgePSet edgesOut;
		currentEdge->getVertexEnd()->getIncidentEdgesOut(edgesOut);
		for (NeoEdgePSet::iterator it = edgesOut.begin(); it != edgesOut.end(); it++) {
			if (uniqueEdges.find(*it) != uniqueEdges.end()) {
				currentEdge = *it;
				break;
			}
		}
	} while (currentEdge != startEdge);

	//Compute the area of f1
	Polygon3 contour1;
	f1->getContour(contour1);
	NeoReal a1 = NeoGeometrySolver::polygonArea(contour1);

	//Compute the area of f2
	Polygon3 contour2;
	f2->getContour(contour2);
	NeoReal a2 = NeoGeometrySolver::polygonArea(contour2);

	//Compute the area of the new contour
	NeoReal aNew = NeoGeometrySolver::polygonArea(contour);

	if (NeoGeometrySolver::isPolygonMonotone(contour) && Math::equal(a1 + a2, aNew)) {
		//3) Destroy both faces, the shared edges between them, and create a new face with the merged contour. 
		//The destruction process is done this way in only to keep the properties of the contour edges, e.g., 
		//isWall. 
		destroyFaceOnly(f1);
		destroyFaceOnly(f2);
		for (NeoEdgePSet::iterator it = sharedEdges.begin(); it != sharedEdges.end(); it++) {
			destroyEdge(*it);
		}

		/*NeoPSimpl::simplifyRadialDistance(contour);
		NeoPSimpl::simplifyPerpendicularDistance(contour);*/
		return createFace(contour);
	}
	
	return f1;	//The faces cannot be merged because the result is not monotone. 
}

void NeoMesh::refreshOctree() {
	_octree = NeoOctreeFaceUPtr(new NeoOctreeFace(_aabb));
	for (NeoFaceMap::iterator it = _faceMap.begin(); it != _faceMap.end(); it++) {
		NeoFace* face = it->second.get();
		_octree->insert(face);
	}
}

#pragma endregion

#pragma region DEBUG

void NeoMesh::log() {
	Ogre::LogManager* logManager = Ogre::LogManager::getSingletonPtr();
	logManager->logMessage("================================================");
	logManager->logMessage("NEO MESH LOG");
	logManager->logMessage("================================================");
	
	logManager->logMessage("numVertices = " + std::to_string(_vertexMap.size()));
	logManager->logMessage("numEdges = " + std::to_string(_edgeMap.size()));
	logManager->logMessage("numFaces = " + std::to_string(_faceMap.size()));

	logManager->logMessage("================================================");
	logManager->logMessage("-> VERTICES LIST");
	logManager->logMessage("================================================");
	for (NeoVertexMap::iterator it = _vertexMap.begin(); it != _vertexMap.end(); it++) {
		logManager->logMessage("id = " + std::to_string(it->first));
		NeoReal x, y, z;
		it->second->getPosition().getCoords(x, y, z);
		const Vector3& pos = it->second->getPosition();
		logManager->logMessage("pos = " + Ogre::StringConverter::toString(Ogre::Vector3(Ogre::Real(x), Ogre::Real(y), Ogre::Real(z))));
		logManager->logMessage("numIncidentEdges = " + Ogre::StringConverter::toString(it->second->getNumIncidentEdges()));
		logManager->logMessage("-----------------------------------------------");
	}
	
	logManager->logMessage("================================================");
	logManager->logMessage("-> EDGES LIST");
	logManager->logMessage("================================================");
	for (NeoEdgeMap::iterator it = _edgeMap.begin(); it != _edgeMap.end(); it++) {
		logManager->logMessage("id = " + std::to_string(it->first));
		NeoReal x, y, z;
		it->second->getVertexBegin()->getPosition().getCoords(x, y, z);
		logManager->logMessage("eBegin = " + Ogre::StringConverter::toString(Ogre::Vector3(Ogre::Real(x), Ogre::Real(y), Ogre::Real(z))));
		it->second->getVertexEnd()->getPosition().getCoords(x, y, z);
		logManager->logMessage("eEnd = " + Ogre::StringConverter::toString(Ogre::Vector3(Ogre::Real(x), Ogre::Real(y), Ogre::Real(z))));
		it->second->getDirection().getCoords(x, y, z);
		logManager->logMessage("direction = " + Ogre::StringConverter::toString(Ogre::Vector3(Ogre::Real(x), Ogre::Real(y), Ogre::Real(z))));
		logManager->logMessage("-----------------------------------------------");
	}

	logManager->logMessage("================================================");
	logManager->logMessage("-> FACES LIST");
	logManager->logMessage("================================================");
	for (NeoFaceMap::iterator it = _faceMap.begin(); it != _faceMap.end(); it++) {
		logManager->logMessage("id = " + std::to_string(it->first));
	}
}

#pragma endregion

#pragma region FILE MANAGEMENT 

void NeoMesh::exportOBJ(const std::string& path) {
	typedef std::map<Vector3, size_t, Vector3Cmp> Point3IndexMap;

	std::ofstream file(path, std::ofstream::out | std::ofstream::trunc);

	Point3IndexMap vMap;
	size_t vertexID = 1;

	//Save the vertices
	for (NeoVertexMap::iterator it = _vertexMap.begin(); it != _vertexMap.end(); it++) {
		const NEOGEN::Vector3& pos = it->second->getPosition();
		vMap[pos] = vertexID++;

		file << "v " << pos.getX() << " " << pos.getY() << " " << pos.getZ() << std::endl;
	}

	//Save the faces
	for (NeoFaceMap::iterator it = _faceMap.begin(); it != _faceMap.end(); it++) {
		Triangles& triangles = it->second->getTriangles();
		for (Triangles::iterator itTriangle = triangles.begin(); itTriangle != triangles.end(); itTriangle++) {
			Triangle& t = *itTriangle;
			file << "f "
				<< vMap[t[0]->getPosition()] << " "
				<< vMap[t[1]->getPosition()] << " "
				<< vMap[t[2]->getPosition()] << std::endl;
		}
	}

	file.close();
}

#pragma endregion