#include "ngVoxelGrid.h"
#include <cmath>

using namespace NEOGEN;

/**********************************************************/
/* CONSTRUCCIO I DESTRUCCIO */
/**********************************************************/
VoxelGrid::VoxelGrid() {
	voxelOut = new Voxel(Ogre::Vector3(0.0f, 0.0f, 0.0f), Ogre::Vector3(1.0f, 1.0f, 1.0f), Voxel::TYPE_OUT);
	mSceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
}

VoxelGrid::~VoxelGrid() {
	delete voxelOut;
	voxelOut = NULL;
	mSceneMgr = NULL;
}

void VoxelGrid::createGrid(size_t sX, size_t sY, size_t sZ, float vDimX, float vDimY, float vDimZ) {
	//Dimensions de la voxel grid
	sizeX = sX;
	sizeY = sY; 
	sizeZ = sZ;

	//Dimensions d'un v�xel
	voxelDim = Ogre::Vector3(vDimX, vDimY, vDimZ);

	//Calculo les dimensions de la bb de la grid
	Ogre::Vector3 bbMax((sizeX * voxelDim.x) / 2.0f, (sizeY * voxelDim.y) / 2.0f, (sizeZ * voxelDim.z) / 2.0f);
	Ogre::Vector3 bbMin = -bbMax;
	bbGrid.setExtents(bbMin, bbMax);
	bbHalfSize = bbGrid.getHalfSize();

	//Creo la grid de voxels
	std::vector<Voxel*> gridColumn;
	std::vector<std::vector<Voxel*>> gridSlice;
	Voxel* v;
	Ogre::Vector3 origin, bbVoxelMin, bbVoxelMax;
	Ogre::Vector3 voxelSize(voxelDim.x, voxelDim.y, voxelDim.z);
	grid.clear();
	for (size_t x = 0; x < sizeX; x++) {
		gridSlice.clear();
		for (size_t y = 0; y < sizeY; y++) {
			gridColumn.resize(0);
			for (size_t z = 0; z < sizeZ; z++) {
				origin.x = ((float)x * voxelDim.x); origin.y = ((float)y * voxelDim.y); origin.z = ((float)z * voxelDim.z);
				bbVoxelMin = origin - bbHalfSize; 
				bbVoxelMax = origin + voxelSize - bbHalfSize;
				v = new Voxel(bbVoxelMin, bbVoxelMax);
				v->keyX = x;
				v->keyY = y;
				v->keyZ = z;
				gridColumn.push_back(v);
			}
			gridSlice.push_back(gridColumn);
		}
		grid.push_back(gridSlice);
	}
	Ogre::LogManager::getSingletonPtr()->logMessage("VoxelGridSizeX = " + std::to_string(sizeX));
	Ogre::LogManager::getSingletonPtr()->logMessage("VoxelGridSizeY = " + std::to_string(sizeY));
	Ogre::LogManager::getSingletonPtr()->logMessage("VoxelGridSizeZ = " + std::to_string(sizeZ));
}

void VoxelGrid::createGrid(Ogre::Entity* ent, float charRadius, float maxStepDepth) {
	//Creo el manual object que representar� el model de voxels
	Ogre::LogManager::getSingletonPtr()->logMessage("INITIALIZING VOXEL PARAMETERS");
	const Ogre::AxisAlignedBox& bbModel = ent->getBoundingBox();
	Ogre::Vector3 bbModelSize = bbModel.getSize();			//Tamany de la BB del model
	
	size_t sX = (size_t)Ogre::Math::Ceil(bbModelSize.x / charRadius);
	size_t sY = (size_t)Ogre::Math::Ceil(bbModelSize.y / maxStepDepth); 
	size_t sZ = (size_t)Ogre::Math::Ceil(bbModelSize.z / charRadius);

	createGrid(sX, sY, sZ, charRadius, maxStepDepth, charRadius);
}

/**********************************************************/
/* GET I SET */
/**********************************************************/
size_t VoxelGrid::getSizeX() {
	return sizeX;
}

size_t VoxelGrid::getSizeY() {
	return sizeY;
}

size_t VoxelGrid::getSizeZ() {
	return sizeZ;
}

Ogre::Vector3& VoxelGrid::getVoxelDim() {
	return voxelDim;
}

Ogre::Vector3& VoxelGrid::getHalfSize() {
	return bbHalfSize;
}

Voxel* VoxelGrid::getVoxel(VoxelPointer& vPointer) {
	return getVoxel(vPointer.x, vPointer.y, vPointer.z);
}

Voxel* VoxelGrid::getVoxel(int x, int y, int z) {
	if (isOutOfBounds(x, y, z)) return voxelOut;
	//Si arribem aqu� �s perqu� el v�xel est� dins els l�mits de la grid
	return (grid[x][y][z]);
}

Voxel* VoxelGrid::getVoxelFromPos(Ogre::Vector3& realPos) {
	//Calculo el voxel inicial
	//Transformo les coordenades al sistema de coordenades del slicemap
	//El sistema de coordenades del model t� el seu origen al centre de masses, que correspon
	//amb el centre de la bounding box. 
	//En canvi, la voxelGrid t� l'origen al punt (0, 0, 0).
	Ogre::Vector3 voxelPos = realPos + bbHalfSize;	//Coordenades respecte el sistema de coordenades del sliceMap
	voxelPos.x = std::max(0.0f, std::min(Ogre::Math::Floor(voxelPos.x * (1.0f / voxelDim.x)), (float)sizeX - 1));
	voxelPos.y = std::max(0.0f, std::min(Ogre::Math::Floor(voxelPos.y * (1.0f / voxelDim.y)), (float)sizeY - 1));
	voxelPos.z = std::max(0.0f, std::min(Ogre::Math::Floor(voxelPos.z * (1.0f / voxelDim.z)), (float)sizeZ - 1));
	//drawVoxelEnt(Voxel::TYPE_NEGATIVE, voxelPos.x, voxelPos.y, voxelPos.z);
	//Ogre::LogManager::getSingletonPtr()->logMessage("realPos = " + std::to_string(realPos));
	//Ogre::LogManager::getSingletonPtr()->logMessage("voxelPos = " + std::to_string(voxelPos));
	return getVoxel((int)voxelPos.x, (int)voxelPos.y, (int)voxelPos.z);
}

void VoxelGrid::setVoxelType(int x, int y, int z, int newType) {
	Voxel* v = getVoxel(x, y, z);
	VoxelPointer vPtr(x, y, z);
	int oldType = v->getType();
	if ((oldType == Voxel::TYPE_OUT) || (oldType == newType)) return;
	
	//Elimino el v�xel de la taula d'acc�s r�pid corresponent
	if (oldType == Voxel::TYPE_POSITIVE) positiveVoxels.erase(vPtr);
	else if (oldType == Voxel::TYPE_NEGATIVE) negativeVoxels.erase(vPtr);
	else if (oldType == Voxel::TYPE_ACCESSIBLE) accessibleVoxels.erase(vPtr);
	
	//Assigno el nou tipus i inserto el punter a la taula d'acc�s r�pid corresponent
	if (newType == Voxel::TYPE_POSITIVE) positiveVoxels.insert(vPtr);
	else if (newType == Voxel::TYPE_NEGATIVE) negativeVoxels.insert(vPtr);
	else if (newType == Voxel::TYPE_ACCESSIBLE) accessibleVoxels.insert(vPtr);
	
	v->setType(newType);
}

Ogre::AxisAlignedBox& VoxelGrid::getBoundingBox() {
	return bbGrid;
}

VoxelPointerTable& VoxelGrid::getPositiveVoxels() {
	return positiveVoxels;
}

VoxelPointerTable& VoxelGrid::getNegativeVoxels() {
	return negativeVoxels;
}

VoxelPointerTable& VoxelGrid::getAccessibleVoxels() {
	return accessibleVoxels;
}

bool VoxelGrid::isOutOfBounds(int x, int y, int z) {
	return (
		(x < 0) || (x >= (int)sizeX) ||
		(y < 0) || (y >= (int)sizeY) ||
		(z < 0) || (z >= (int)sizeZ)
	);
}

/**********************************************************/
/* DIBUIXAT */
/**********************************************************/
void VoxelGrid::drawGrid() {
	Ogre::ManualObject* manual = mSceneMgr->createManualObject();
	drawBoundingBox(manual, bbGrid.getMinimum(), bbGrid.getMaximum());
	Ogre::SceneNode* node = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	node->attachObject(manual);
	
	Voxel* v;
	Ogre::ManualObject* manualGrid = mSceneMgr->createManualObject();
	manualGrid->begin("pureRed", Ogre::RenderOperation::OT_LINE_LIST);
	for (size_t x = 0; x < sizeX; x++) {
		for (size_t y = 0; y < sizeY; y++) {
			for (size_t z = 0; z < sizeZ; z++) {
				v = getVoxel(x, y, z);
				drawGrid(manualGrid, v->getMinimum(), v->getMaximum());
			}
		}
	}
	manualGrid->end();
	node = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	node->attachObject(manualGrid);
}

void VoxelGrid::drawBoundingBox(Ogre::ManualObject* manual, Ogre::Vector3& bbMin, Ogre::Vector3& bbMax) {
	manual->clear();
	manual->begin("pureWhite", Ogre::RenderOperation::OT_LINE_LIST);
 
	// define vertex position
	manual->position(bbMin.x, bbMin.y, bbMin.z);	//v0
	manual->position(bbMax.x, bbMin.y, bbMin.z);	//v1
	manual->position(bbMax.x, bbMax.y, bbMin.z);	//v2
	manual->position(bbMin.x, bbMax.y, bbMin.z);	//v3	

	manual->position(bbMin.x, bbMin.y, bbMax.z);	//v4
	manual->position(bbMax.x, bbMin.y, bbMax.z);	//v5
	manual->position(bbMax.x, bbMax.y, bbMax.z);	//v6
	manual->position(bbMin.x, bbMax.y, bbMax.z);	//v7
 
	// define usage of vertices by refering to the indexes
	manual->index(0); manual->index(1);
	manual->index(1); manual->index(2);
	manual->index(2); manual->index(3);
	manual->index(3); manual->index(0);

	manual->index(4); manual->index(5);
	manual->index(5); manual->index(6);
	manual->index(6); manual->index(7);
	manual->index(7); manual->index(4);

	manual->index(3); manual->index(7);
	manual->index(2); manual->index(6);
	manual->index(0); manual->index(4);
	manual->index(1); manual->index(5);
	 
	manual->end();
}

void VoxelGrid::drawGrid(Ogre::ManualObject* manual, Ogre::Vector3& bbMin, Ogre::Vector3& bbMax) {
	// define usage of vertices by refering to the indexes
	manual->position(bbMin.x, bbMin.y, bbMin.z); manual->position(bbMax.x, bbMin.y, bbMin.z);
	manual->position(bbMax.x, bbMin.y, bbMin.z); manual->position(bbMax.x, bbMax.y, bbMin.z);
	manual->position(bbMax.x, bbMax.y, bbMin.z); manual->position(bbMin.x, bbMax.y, bbMin.z);
	manual->position(bbMin.x, bbMax.y, bbMin.z); manual->position(bbMin.x, bbMin.y, bbMin.z);

	manual->position(bbMin.x, bbMin.y, bbMax.z); manual->position(bbMax.x, bbMin.y, bbMax.z);
	manual->position(bbMax.x, bbMin.y, bbMax.z); manual->position(bbMax.x, bbMax.y, bbMax.z);
	manual->position(bbMax.x, bbMax.y, bbMax.z); manual->position(bbMin.x, bbMax.y, bbMax.z);
	manual->position(bbMin.x, bbMax.y, bbMax.z); manual->position(bbMin.x, bbMin.y, bbMax.z);

	manual->position(bbMin.x, bbMax.y, bbMin.z); manual->position(bbMin.x, bbMax.y, bbMax.z);
	manual->position(bbMax.x, bbMax.y, bbMin.z); manual->position(bbMax.x, bbMax.y, bbMax.z);
	manual->position(bbMin.x, bbMin.y, bbMin.z); manual->position(bbMin.x, bbMin.y, bbMax.z);
	manual->position(bbMax.x, bbMin.y, bbMin.z); manual->position(bbMax.x, bbMin.y, bbMax.z);
}