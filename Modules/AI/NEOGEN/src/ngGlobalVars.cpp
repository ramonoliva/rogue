#include "ngGlobalVars.h"

RealMath::ProjectionPlaneEnum NEOGEN_PROJECTION_PLANE = RealMath::PROJECTION_XZ;

float vertScale = MIN_VERT_SCALE;
float vertScaleInc = 0.005f;
int applicationState = STATE_IDLE;
int applicationMode = APPLICATION_MODE_3D;
int computationMode = COMPUTATION_MODE_CPU;
int generationMode = GENERATION_MODE_STEP_BY_STEP;
float alphaThreshold = 5.0f;

int newVertID = 0;
int newEdgeID = 0;
int newCellID = 0;

Ogre::Timer* globalTimer = new Ogre::Timer();
unsigned long appTime = 0;
unsigned long appTimeBefore = 0;
unsigned long appTimeAfter = 0;
unsigned long appTimeDiff = 0;

bool DEBUG_MODE = true;