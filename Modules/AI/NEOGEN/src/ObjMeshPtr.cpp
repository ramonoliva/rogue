#include "ObjMeshPtr.h"

/*********************************************************************************************/
/* CREACIO I DESTRUCCIO */
/*********************************************************************************************/

ObjMeshPtr::ObjMeshPtr() : Ogre::SharedPtr<ObjMesh>() {}
	
ObjMeshPtr::ObjMeshPtr(ObjMesh *rep) : Ogre::SharedPtr<ObjMesh>(rep) {}

ObjMeshPtr::ObjMeshPtr(const ObjMeshPtr &r) : Ogre::SharedPtr<ObjMesh>(r) {} 

ObjMeshPtr::ObjMeshPtr(const Ogre::ResourcePtr &r) : Ogre::SharedPtr<ObjMesh>() {
	if (r.isNull()) return;
	lockAndCopyMutexPointer(r);
}

void ObjMeshPtr::lockAndCopyMutexPointer(const Ogre::ResourcePtr &r) {
	// lock & copy other mutex pointer
	OGRE_LOCK_MUTEX(*r.OGRE_AUTO_MUTEX_NAME)
	OGRE_COPY_AUTO_SHARED_MUTEX(r.OGRE_AUTO_MUTEX_NAME)
	pRep = static_cast<ObjMesh*>(r.getPointer());
	pUseCount = r.useCountPointer();
	useFreeMethod = r.freeMethod();
	if (pUseCount) {
		++(*pUseCount);
	}
}

/*********************************************************************************************/
/* OPERATORS */
/*********************************************************************************************/

/// Operator used to convert a ResourcePtr to a ObjMeshPtr
ObjMeshPtr& ObjMeshPtr::operator=(const Ogre::ResourcePtr& r) {
	if(pRep == static_cast<ObjMesh*>(r.getPointer())) return *this;
	release();
	if(r.isNull()) return *this; // resource ptr is null, so the call to release above has done all we need to do.
	lockAndCopyMutexPointer(r);
	return *this;
}