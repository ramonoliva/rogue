#include "ngNavMeshGeneratorManual.h"

using namespace NEOGEN;

#pragma region CREATION AND DESTRUCTION

NavMeshGeneratorManual::NavMeshGeneratorManual(Ogre::SceneManager* scMan) : NavMeshGeneratorBase(scMan) {

}

#pragma endregion

void NavMeshGeneratorManual::initializeData() {
	if (_initialized) return;

	for (EdgeSetID::iterator it = _predefinedPortals.begin(); it != _predefinedPortals.end(); it++) {
		int idEdge = addSubEdge(-1, it->first, it->second, NEOGEN::Edge::PORTAL);
		portals.insert(idEdge);
	}
}

bool NavMeshGeneratorManual::generate() {
	if (!_initialized) initializeData();

	createCPG();

	return true;
}