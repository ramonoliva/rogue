#include "ngVoxel.h"
#include "ngGlobalVars.h"
#include <OgreSubEntity.h>

using namespace NEOGEN;

/***************************************************************/
/* CREACIO I DESTRUCCIO */
/***************************************************************/

Voxel::Voxel(const Ogre::Vector3& min, const Ogre::Vector3& max, int t) : Ogre::AxisAlignedBox(min, max) {
	type = t;
	layer = LAYER_INVALID;
	keyX = -1;
	keyY = -1;
	keyZ = -1;
	portal = false;
	contour = false;
	mSceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
}

Voxel::~Voxel() {
	mSceneMgr = NULL;
}

/***************************************************************/
/* GET I SET */
/***************************************************************/
int Voxel::getType() {
	return type;
}

void Voxel::setType(int t) {
	if (type != TYPE_OUT) type = t;
}

int Voxel::getLayer() {
	return layer;
}

void Voxel::setLayer(int l) {
	if (type != TYPE_OUT) layer = l;
}

void Voxel::setPortal(bool p) {
	portal = p;
}

bool Voxel::isPortal() {
	return portal;
}

void Voxel::setContour(bool c) {
	contour = c;
}

bool Voxel::isContour() {
	return contour;
}

void Voxel::addCell(int c) {
	cells.insert(c);
}

bool Voxel::hasCell(int c) {
	std::set<int>::iterator itFound = cells.find(c);
	return (itFound != cells.end());
}

size_t Voxel::numCells() {
	return cells.size();
}

std::set<int>& Voxel::getCells() {
	return cells;
}

/********************************************************************/
/* DIBUIXAT */
/********************************************************************/
/*void Voxel::draw(const Ogre::String& matName, Ogre::StaticGeometry* sg) { 
	Ogre::Vector3& voxelDim = getSize();
	Ogre::Entity* ent = mSceneMgr->createEntity("voxel.mesh");
	Ogre::Vector3 pos = getCenter(); 
	ent->setMaterialName(matName);
	if (sg == NULL) {
		Ogre::SceneNode* node = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		node->attachObject(ent);
		node->setPosition(pos);
		node->setScale(voxelDim);
	}
	else {
		sg->addEntity(ent, pos, Ogre::Quaternion::IDENTITY, voxelDim);
	}
}*/

void Voxel::draw(const Ogre::String& matName, Ogre::StaticGeometry* sg) { 
	Ogre::Vector3& voxelDim = getSize();
	Ogre::Entity* ent = mSceneMgr->createEntity("voxel.mesh");
	Ogre::Vector3 pos = getCenter();
	/*if (matName == "matVoxelPositive") setUniform("matVoxel", VERTEX_SHADER, "color", Ogre::Vector4(0.0f, 0.0f, 1.0f, 1.0f));
	else if (matName == "matVoxelNegative") setUniform("matVoxel", VERTEX_SHADER, "color", Ogre::Vector4(1.0f, 0.0f, 0.0f, 1.0f));
	else if (matName == "matVoxelAccessible") setUniform("matVoxel", VERTEX_SHADER, "color", Ogre::Vector4(0.0f, 1.0f, 0.0f, 1.0f));
	ent->setMaterialName("matVoxel");*/
	ent->setMaterialName(matName);
	if (sg == NULL) {
		Ogre::SceneNode* node = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		node->attachObject(ent);
		node->setPosition(pos);
		node->setScale(voxelDim);
	}
	else {
		sg->addEntity(ent, pos, Ogre::Quaternion::IDENTITY, voxelDim);
	}
}