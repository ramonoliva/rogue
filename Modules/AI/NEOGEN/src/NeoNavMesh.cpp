#include "NeoNavMesh.h"
#include "NeoCell.h"
#include "NeoTriangulator.h"

using namespace NEOGEN;

#pragma region CREATION AND DESTRUCTION

NeoNavMesh::NeoNavMesh(const NeoAABB& aabb) : NeoMesh(aabb) {
	_newPortalID = 1;
	_newCellID = 1;
}

void NeoNavMesh::init() {
	NeoMesh::init();
}

NeoPortal* NeoNavMesh::createPortal() {
	ElementID portalID = _newPortalID++;
	_portalMap[portalID] = NeoPortalSPtr(new NeoPortal(portalID));

	return _portalMap[portalID].get();
}

NeoCell* NeoNavMesh::createCell() {
	ElementID cellID = _newCellID++;
	_cellMap[cellID] = NeoCellSPtr(new NeoCell(cellID));
	
	return _cellMap[cellID].get();
}

#pragma endregion

#pragma region GET AND SET

NeoPortal* NeoNavMesh::getPortal(ElementID portalID) {
	NeoPortalMap::iterator it = _portalMap.find(portalID);
	if (it == _portalMap.end()) return NULL;
	return it->second.get();
}

NeoPortalMap& NeoNavMesh::getPortals() {
	return _portalMap;
}

size_t NeoNavMesh::getNumPortals() {
	return _portalMap.size() / 2;	//We divide by 2 because a portal is formed by two edges in opposite directions. 
}

NeoCell* NeoNavMesh::getCell(ElementID cellID) {
	NeoCellMap::iterator it = _cellMap.find(cellID);
	if (it == _cellMap.end()) return NULL;
	return it->second.get();
}

NeoCellMap& NeoNavMesh::getCells() {
	return _cellMap;
}

size_t NeoNavMesh::getNumCells() {
	return _cellMap.size();
}

#pragma endregion

#pragma region FILE MANAGEMENT 

void NeoNavMesh::exportOBJ(const std::string& path) {
	NeoMesh::exportOBJ(path);



	//typedef std::map<Vector3, size_t, Vector3Cmp> Point3IndexMap;

	//std::ofstream file(path, std::ofstream::out | std::ofstream::trunc);

	//Point3IndexMap vMap;
	//size_t vertexID = 1;

	////Save the obstacle vertices
	//for (NeoVertexMap::iterator it = _vertexMap.begin(); it != _vertexMap.end(); it++) {
	//	NeoVertex* v = it->second.get();
	//	if (!v->isObstacle()) continue;
	//	
	//	const NEOGEN::Vector3& pos = v->getPosition();
	//	vMap[pos] = vertexID++;

	//	file << "v " << pos.getX() << " " << pos.getY() << " " << pos.getZ() << std::endl;
	//}

	////Save the cells as a group
	//for (NeoCellMap::iterator itCell = _cellMap.begin(); itCell != _cellMap.end(); itCell++) {
	//	NeoCell* cell = itCell->second.get();
	//	const Triangles& triangles = cell->getTriangles();

	//	file << "g " << "CELL " << cell->getID() << std::endl;
	//	for (Triangles::const_iterator itTriangle = triangles.begin(); itTriangle != triangles.end(); itTriangle++) {
	//		const Triangle& t = *itTriangle;
	//		file << "f " << vMap[t[0]->getPosition()] << " " << vMap[t[1]->getPosition()] << " " << vMap[t[2]->getPosition()] << std::endl;
	//	}
	//}

	//file.close();





	//typedef std::map<Vector3, size_t, Vector3Cmp> Point3IndexMap;

	//std::ofstream file(path, std::ofstream::out | std::ofstream::trunc);

	//Point3IndexMap vMap;
	//size_t vertexID = 1;

	////Save the vertices
	//for (NeoVertexMap::iterator it = _vertexMap.begin(); it != _vertexMap.end(); it++) {
	//	const NEOGEN::Vector3& pos = it->second->getPosition();
	//	vMap[pos] = vertexID++;

	//	file << "v " << pos.getX() << " " << pos.getY() << " " << pos.getZ() << std::endl;
	//}

	////Save the cells as a group
	//for (NeoCellMap::iterator itCell = _cellMap.begin(); itCell != _cellMap.end(); itCell++) {
	//	NeoCell* cell = itCell->second.get();
	//	NeoFacePSet& faces = cell->getFaces();

	//	file << "g " << "CELL " << cell->getID() << std::endl;

	//	for (NeoFacePSet::iterator itFace = faces.begin(); itFace != faces.end(); itFace++) {
	//		Triangles& triangles = (*itFace)->getTriangles();
	//		for (Triangles::iterator itTriangle = triangles.begin(); itTriangle != triangles.end(); itTriangle++) {
	//			Triangle& t = *itTriangle;
	//			file << "f "
	//				<< vMap[t[0]->getPosition()] << " "
	//				<< vMap[t[1]->getPosition()] << " "
	//				<< vMap[t[2]->getPosition()] << std::endl;
	//		}
	//	}
	//}

	//file.close();
}

#pragma endregion