#include "NeoVector3.h"
#include "NeoMath.h"

using namespace NEOGEN;

const Vector3 ONE = Vector3(1,1,1);
const Vector3 ZERO = Vector3(0,0,0);
			
const Vector3 UP = Vector3(0,1,0);
const Vector3 DOWN = Vector3(0,-1,0);

const Vector3 LEFT = Vector3(-1,0,0);
const Vector3 RIGHT = Vector3(1,0,0);

const Vector3 FORWARD = Vector3(0,0,1);
const Vector3 BACKWARD = Vector3(0,0,-1);

Vector3 NEOGEN::operator*(NeoReal d, const Vector3& v) {
	return Vector3(v._x * d, v._y * d, v._z * d);
}

Vector3 NEOGEN::operator*(const Vector3& v, NeoReal d) {
	return d * v;
}

Vector3 NEOGEN::operator-(const Vector3& v) {
	return -1.0 * v;
}