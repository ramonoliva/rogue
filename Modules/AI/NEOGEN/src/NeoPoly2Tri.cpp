#include "NeoPoly2Tri.h"
#include "NeoVector2.h"
#include "NeoVector3.h"
#include "NeoMath.h"
#include "NeoGeometrySolver.h"

#include <OgreLogManager.h>

using namespace NEOGEN;

void NeoPoly2Tri::toPoly2Tri(const Polygon3& polygon, Point2ToPoint3Map& mResult, Polygon2& pResult) {
	for (Polygon3::const_iterator it = polygon.begin(); it != polygon.end(); it++) {
		const Vector3& p = *it;
		mResult[p.to2D()] = p;
		pResult.push_back(p.to2D());
	}
}

void NeoPoly2Tri::toPoly2Tri(const Polygon2& polygon, p2tPolyline& result) {
	//Poly2Tri has a bug ocurring when the input polygon is not exactly simple, i.e., when 
	//the contour is touching itself at a single point. So we move the conflictive points a little bit
	//in order to avoid this bug. 

	//1) Find the repeated vertices
	std::vector<size_t> repeatedVertices;
	Vector2Set vertices;
	Polygon2 tmp;
	for (size_t i = 0; i < polygon.size(); i++) {
		const Vector2& p = polygon[i];
		if (vertices.find(p) != vertices.end()) repeatedVertices.push_back(i);
		vertices.insert(p);
		tmp.push_back(p);
	}

	//2) Break the problematic vertices. 
	NeoReal offset = Math::NEO_REAL_PRECISION * NeoReal(0.1);
	for (std::vector<size_t>::iterator it = repeatedVertices.begin(); it != repeatedVertices.end(); it++) {
		Vector2& p = tmp[*it];
		const Vector2& pNext = tmp[((*it) + 1) % tmp.size()];
		Vector2 u = (pNext - p).normalized();
		p += u * offset;
	}

	//3) Convert tmp to the data used by Poly2Tri
	for (size_t i = 0; i < tmp.size(); i++) {
		const Vector2& p = tmp[i];
		result.push_back(new p2t::Point(p.getX(), p.getY()));
	}
}

void NeoPoly2Tri::toNeogen(p2tTriangle& triangle, Polygon2& result) {
	p2t::Point* p0 = triangle.GetPoint(0);
	p2t::Point* p1 = triangle.GetPoint(1);
	p2t::Point* p2 = triangle.GetPoint(2);

	result.push_back(Vector2(p0->x, p0->y));
	result.push_back(Vector2(p1->x, p1->y));
	result.push_back(Vector2(p2->x, p2->y));
}

void NeoPoly2Tri::toNeogen(p2tTriangles& triangles, Polygons2& result) {
	for (p2tTriangles::iterator it = triangles.begin(); it != triangles.end(); it++) {
		Polygon2 tmp;
		toNeogen(**it, tmp);
		result.push_back(tmp);
	}
}

void NeoPoly2Tri::triangulate(const Polygon3& floor, Polygons3& result) {
	Polygons3 emptyHoles;
	triangulate(floor, emptyHoles, result);
}

void NeoPoly2Tri::triangulate(const Polygon3& floor, const Polygons3& holes, Polygons3& result) {
	Point2ToPoint3Map pointMap;
	
	//1) Project the floor to the 2D plane
	Polygon2 floor2D;
	toPoly2Tri(floor, pointMap, floor2D);
	
	//2) Project the holes to the 2D plane
	Polygons2 holes2D;
	for (Polygons3::const_iterator itHoles = holes.begin(); itHoles != holes.end(); itHoles++) {
		const Polygon3& h = *itHoles;
		Polygon2 h2D;
		toPoly2Tri(h, pointMap, h2D);
		holes2D.push_back(h2D);
	}
	
	//3) Triangulate using the 2D pipeline. 
	Polygons2 result2D;
	triangulate(floor2D, holes2D, result2D);

	//4) Reconstruct the 3D result by using the pointMap
	Vector3 floorNormal = NeoGeometrySolver::getPolygonNormal(floor);
	for (Polygons2::iterator itResult = result2D.begin(); itResult != result2D.end(); itResult++) {
		Polygon2 r2D = *itResult;
		Polygon3 r;
		for (Polygon2::iterator it = r2D.begin(); it != r2D.end(); it++) {
			r.push_back(pointMap[*it]);
		}
		Vector3 rNormal = NeoGeometrySolver::getPolygonNormal(r);
		if (Math::less(floorNormal.dot(rNormal), 0)) NeoGeometrySolver::polygonReverse(r);
		result.push_back(r);
	}
}

void NeoPoly2Tri::triangulate(const Polygon2& floor, Polygons2& result) {
	Polygons2 emptyHoles;
	triangulate(floor, emptyHoles, result);
}

void NeoPoly2Tri::triangulate(const Polygon2& floor, const Polygons2& holes, Polygons2& result) {

	p2tPolylines polylines;

	//1) Create the polyline for the floor
	p2tPolyline polyline;
	toPoly2Tri(floor, polyline);
	polylines.push_back(polyline);

	//2) Create the CDT and add the primary polyline. 
	// NOTE: polyline must be a simple polygon. The polyline's points constitute constrained edges. No repeat points!!!
	p2t::CDT cdt(polyline);

	//3) Create and add the holes
	for (Polygons2::const_iterator it = holes.begin(); it != holes.end(); it++) {
		p2tPolyline hole;
		toPoly2Tri(*it, hole);
		cdt.AddHole(hole);
		polylines.push_back(hole);
	}

	//4) Triangulate
	cdt.Triangulate();
	toNeogen(cdt.GetTriangles(), result);
	
	// Free points
	for (size_t i = 0; i < polylines.size(); i++) {
		p2tPolyline poly = polylines[i];
		freeClear(poly);
	}
}