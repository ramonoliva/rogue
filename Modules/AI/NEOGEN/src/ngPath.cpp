#include "ngPath.h"

using namespace NEOGEN;

/***************************************************************************/
/* CREATION AND DESTRUCTION */
/***************************************************************************/
void Path::clear() {
	_nodeList.clear();
	_nodeOrderList.clear();
}

void Path::addNode(int n) {
	_nodeOrderList[n] = getNumNodes();
	_nodeList.push_back(n);
}
	
/***************************************************************************/
/* GET AND SET */
/***************************************************************************/
size_t Path::getNumNodes() {
	return _nodeList.size();
}

bool Path::isGoalNode(int n) {
	int pos = getNodePosition(n);
	if (pos == -1) return false;
	return (pos == (getNumNodes() - 1));
}

bool Path::empty() {
	return (getNumNodes() == 0);
}

int Path::getPrevNode(int n) {
	int pos = getNodePosition(n);
	if (pos <= 0) return -1;	//n does not exists in the path or n is the first node, and therefore it has not a previous one. 
	
	return _nodeList[pos - 1];
}

int Path::getNextNode(int n) {
	int pos = getNodePosition(n);
	if ((pos == -1) || (pos == (getNumNodes() - 1))) return -1;	//n does not exists in the path or is the last node

	return _nodeList[pos + 1];
}

int Path::getNodePosition(int n) {
	//Returns the node position inside the path, or -1 if it does not exists. 
	NodeOrderList::iterator itFind = _nodeOrderList.find(n);
	if (itFind == _nodeOrderList.end()) return -1;
	return itFind->second;
}

bool Path::hasNode(int n) {
	return (getNodePosition(n) != -1);
}


