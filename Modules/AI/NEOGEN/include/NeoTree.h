#pragma once

#include "NeoTypes.h"

namespace NEOGEN {

	template<class TNode>
	class NeoTreeNode {

	public:

		NeoTreeNode(TNode* parent) {
			_parent = parent;
		}

		virtual ~NeoTreeNode() {
			clear();
		}

		virtual void clear() {
			_childs.clear();
		}

		virtual TNode* getParent() {
			return _parent;
		}

		virtual const std::vector<std::shared_ptr<TNode>>& getChilds() {
			return _childs;
		}

		virtual void addChild(std::shared_ptr<TNode>& c) {
			_childs.push_back(c);
		}

	protected:

		std::vector<std::shared_ptr<TNode>> _childs;
		TNode* _parent;

	};

	template<class TNode>
	class NeoTree {

	public:

		virtual TNode* getRoot() {
			return _root.get();
		}

	protected:

		std::shared_ptr<TNode> _root;

	};

}