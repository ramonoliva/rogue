#pragma once

#include "NeoTypes.h"
#include "NeoVertex.h"
#include "NeoEdge.h"
#include "NeoFace.h"
#include "NeoAABB.h"

#include "NeoOctree.h"

#include <fstream>

namespace NEOGEN {

	typedef std::pair<NeoVertex*, NeoVertex*> NeoVertexPair;
	typedef std::map<NeoVertexPair, NeoEdge*> NeoEdgeMapEndpoints;
	
	typedef std::map<Vector3, NeoVertex*, Vector3Cmp> NeoVertexMapPosition;

	class NeoMesh {

		public:

			NeoMesh(const NeoAABB& aabb);
			NeoMesh(Vector3List& pointList, ElementIDVector& triangleList);
			virtual ~NeoMesh();

			virtual NeoVertex* createVertex(const Vector3& position);
			virtual NeoEdge* createEdge(NeoVertex* v1, NeoVertex* v2);
			virtual NeoEdge* createEdge(ElementID v1ID, ElementID v2ID);
			virtual NeoEdge* createEdge(const Vector3& pos1, const Vector3& pos2);
			virtual NeoFace* createFace(const Polygon3& polygon, ElementID faceID = 0);

			virtual NeoEdgePair subdivideEdge(NeoEdge* edge, const Vector3& point, bool subdivideOpposite = true);
			virtual NeoEdgePair subdivideEdge(NeoEdge* edge, NeoVertex* v, bool subdivideOpposite = true);

			virtual void destroyVertex(NeoVertex* vertex);
			virtual void destroyEdge(NeoEdge* edge);
			virtual void destroyFace(NeoFace* face);
			virtual void destroyFaceOnly(NeoFace* face);
			virtual void destroyFaceAndEdgesOnly(NeoFace* face);

			virtual NeoVertex* getVertex(ElementID vertexID);
			virtual NeoVertex* getVertex(const Vector3& position);
			
			virtual NeoEdge* getEdge(ElementID edgeID);
			virtual NeoEdge* getEdge(NeoVertex* v1, NeoVertex* v2);
			virtual NeoEdge* getEdge(const Vector3& posBegin, const Vector3& posEnd);
			
			virtual NeoFace* getFace(ElementID faceID);

			virtual NeoVertexMap& getVertices();
			virtual NeoEdgeMap& getEdges();
			virtual NeoFaceMap& getFaces();

			virtual void getFaceMapKeys(ElementIDSet& result);

			virtual size_t getNumVertices();
			virtual size_t getNumEdges();
			virtual size_t getNumFaces();

			virtual const NeoAABB& getNeoAABB();
			virtual NeoOctreeFace* getOctree();

			virtual void refreshOctree();

			virtual void removeDuplicatedVertices();

			virtual void sew();
			virtual void sew(NeoVertex* v);

			virtual void simplify();
			virtual void simplify(const ElementIDSet& faces);
			
			virtual void log();

			virtual void exportOBJ(const std::string& path);

		protected:

			ElementID _newVertexID;
			ElementID _newEdgeID;
			ElementID _newFaceID;

			NeoVertexMap _vertexMap;
			NeoEdgeMap _edgeMap;
			NeoFaceMap _faceMap;

			NeoEdgeMapEndpoints _edgeMapEndpoints;
			NeoVertexMapPosition _vertexMapPosition;

			NeoAABB _aabb;
			NeoOctreeFaceUPtr _octree;

			bool _is2D;

			virtual void init();

			virtual NeoFace* mergeFaces(NeoFace* f1, NeoFace* f2);

			virtual void getCoplanarPatch(NeoFace* face, const ElementIDSet& facesCandidate, ElementIDSet& result);
			virtual void getRings(const ElementIDSet& faces, Polygons3& result);
			virtual NeoFace* getRingNextFace(NeoEdge* currentEdge, const ElementIDSet& faces);
	};

};