#ifndef __ngVoxelizerCPU_h_
#define __ngVoxelizerCPU_h_

#include "ngVoxelizer.h"
#include <OgreSubEntity.h>
#include <OgreSubMesh.h>

namespace NEOGEN {
	class VoxelizerCPU : public Voxelizer {

	public:

		VoxelizerCPU(Ogre::SceneManager* mgr);
		~VoxelizerCPU();

		void voxelization(ngTriangleList& tList, int voxelType);
		
	private:

	
	};
};

#endif