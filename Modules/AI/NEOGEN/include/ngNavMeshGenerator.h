#ifndef __ngNavMeshGenerator_h_
#define __ngNavMeshGenerator_h_

#include "ngNavMeshGeneratorBase.h"

#define PI			3.14159265f
#define SQRT_HALF	0.70710676908493042f

//Per evitar problemes d'arrodoniment, defineixo un llindar per l'�rea resultant del
//test d'orientaci�. 
#define AREA_THRESHOLD		0.001f
#define ANGLE_THRESHOLD		0.001f

#define ERROR_DISTANCE		0.001f

#define HALF_EDGES_BOARD	500

#define ELEMENT_TYPE_VERTEX	0
#define ELEMENT_TYPE_EDGE	1

namespace NEOGEN {
	
	class NavMeshGenerator : public NavMeshGeneratorBase {

		public:

			NavMeshGenerator(Ogre::SceneManager* scMan);
			virtual ~NavMeshGenerator();

			void clearData();

			void setConvexDistance(float d);
			bool getNavMeshGenerated();
			int getNumPoly();
			void getStats(int* concV, int* splittedV, int* numCurrentP, int* numCurrentC);
			int getInitialVertexs();
			int getRealNotches();
			int getFalseNotches();
			int getSplittedVertexs();
			int getNewVertexs();
			int getNumCurrentPortals();
			int getNumCurrentCells();
			float getElapsedTimeNotchDetection();
			float getElapsedTimePortalCreation();
			float getElapsedTimeNonEssentialPortals();
			float getElapsedTime();

			void setVertPosition(Ogre::String vertName, Ogre::Vector3& position);

			bool generate();
			void generateStepByStep();
			void generateStepByStepCPU();
			void generateFull();

			void repairCells(NavMesh* nm);

			void clearScene();
			void reset();
		
			void showGraph(bool v);
			void showBoard(bool s);

			void printPolyTable();
			void printVertsToSplit();
			void printPortals();
			void printData();

			void saveMapData(std::string fileName);
			void loadMapData(std::string fileName);
			void cellVoxelDescription(Cell& c, std::set<Voxel*>& voxels);

		protected:

			int redComponent;
			int greenComponent;
			int blueComponent;

			/**********************************************/
			/* STATS */
			/**********************************************/
			int initialVertexs;
			int realNotches;
			int falseNotches;
			int splittedVertexs;
			int newVertexs;
			int numCurrentPortals;
			int numNonEssentialPortals;
			unsigned long elapsedTimeNotchDetection;
			unsigned long elapsedTimePortalCreation;
			unsigned long elapsedTimeNonEssentialPortals;
			unsigned long timeBefore;
			unsigned long timeAfter;
			Ogre::Timer* timer;
			/**********************************************/

			float _convexDistance;					//Dist�ncia que utilitzem per determinar si un v�rtex �s realment un notch

			bool navMeshGenerated;				//Indica si s'ha generat la malla navegacional
			bool showAdjacencyGraph;			//Indica si s'ha de mostrar el grapf d'adjac�ncia
		
			MapParser* mapParser;

			struct ClosestElement {
				int elementType;
				int id;
				Vec2D candidate;
				float dist2;
			};

			NotchSet vertsToSplit;	//Cont� els identificadors dels v�rtexs que no s�n convexos
			NotchSet::iterator notchIterator;
			
			Ogre::SceneNode* currentNotchNode;
			Ogre::ManualObject* manualInterestArea;
			Ogre::ManualObject* manualBoard;
		
			Vec2D vAOIRight;			//Vector director actual de la recta que delimita la AOI per la DRETA del v�rtex
			Vec2D vAOILeft;				//Vector director actual de la recta que delimita la AOI per la ESQUERRA del v�rtex
		
			/////////////////////////////////////////////////////////////////////////////////////////////

			void showContour(bool s);
			void createCurrentNotchNode();
			void createBoard();
			void initializeData();
			void initializeVertsToSplit();
			void initializeVertsToSplit(Cell& c);
			void initializeCandidateEdges(EdgeTable& eTable);
			void detectFalseNotches(std::vector<int>& notchString);
			void computeNotchString(std::vector<int>& notchString);
			void addVertToSplit(int idVert);
			void initializeStats();
		
			void erasePortalPointer(int idEdge);
		
			void computeAreaOfInterest(int idVert, Vec2D& vDirRight, Vec2D& vDirLeft);

			bool inZone(Vec2D& a, int b, Vec2D& vRight, Vec2D& vLeft);

			void computeClosestElement();
			ClosestElement computeClosestEdge(Vertex& vi);
			ClosestElement computeBestCandidateEdge(Vertex& vi, Edge& edge);
		
			void createPortal(Vertex* vi, ClosestElement* ce);
			void portalVertEdge(Vertex* vi, ClosestElement* ce);
			void portalVertPortal(Vertex* vi, Edge* e);
			void portalVertPortalCPU(Vertex* vi, Edge* e);
			void portalVertPortalEndPointCPU(Vertex* vi, int portalID, int endPointID);
			void portalVertGeometryEdge(Vertex* vi, ClosestElement* ce);
			void portalVertVert(Vertex* vi, int closestVertID);

			virtual void createCPG();

			ClosestElement computeClosestIntersectionCPU(Vec2D& initPoint, Vec2D& endPoint);
		
			void drawBoard();
			void drawInterestArea(int idVert);
			void drawEdges();
			void drawNavMeshPortals();
			void drawCurrentNotch();

			bool projectedIntersection(Voxel* v, Poly& cProjected);
			Ogre::ManualObject* createManualObject();
			Ogre::ManualObject* createManualObject(const Ogre::String& manualName);
			void createGraphNodes();
			void deleteNonEssentialPortals();
	};
};

#endif