#ifndef __ngDataStructures_h_
#define __ngDataStructures_h_

#include <OgreManualObject.h>
#include <deque>
#include "ngVoxel.h"
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <list>

#include "ngVertex.h"
#include "ngPoly.h"

namespace NEOGEN {

	struct Edge {

		enum Type {
			OBSTACLE,	//The edge represents an obstacle edge
			PORTAL,		//The edge represents a portal edge
			CLEARANCE,	//The edge represents a clearance edge
		};

		//Representa una aresta ORIENTADA
		int id;						//identificador de l'aresta
		int v1;						//Identificador del v�rtex inicial de l'aresta
		int v2;						//Identificador del v�rtex final de l'aresta
		Vec3D _vDirNormalized;		//Vector 3D director normalitzat de l'aresta
		Vec2D _vDirNormalized2D;	//Vector 2D director normalitzat de l'aresta
		Vec2D _vNormal2D;			//Vector 2D normal de l'aresta que apunta cap a l'interior de la cel�la de la que forma part
		int cellID;					//Cel�la a la que pertany l'aresta
		Type _type;					//The type of the cell
		bool isShared;				//indica si l'aresta �s potencialment compartida
		int opposite;				//En cas d'un portal, indica quina �s l'aresta oposada
		int next;					//Punter a la seg�ent aresta de la cel�la
		int prev;					//Punter a la aresta anterior
		float clearance2;			//clearance^2 de l'aresta (dist�ncia entre els seus extrems). Nom�s t� sentit en portals
		Ogre::ManualObject* manual;	//Objecte que ens serveix per dibuixar l'aresta
		Ogre::MaterialPtr material;	//Nom del material que utilitza l'aresta
		Ogre::ColourValue colour;	//Color de l'aresta

		bool isPortal() {
			return (_type == Edge::PORTAL);
		}
	};

	typedef std::vector<int> EdgeList;

	struct nmPoly {
		int id;						//identificador del pol�gon
		std::vector<int> edges;		//arestes que formen el v�rtex
	};

	struct Cell {
	public:

		int id;							//Identificador de la cel�la
		Poly* p;						//Pol�gon convex que representa la cel�la
		std::set<int> adjacentTo;		//Identificadors de les cel�les adjacents
		std::set<int> edges;			//Identificadors de les arestes que formen la cel�la
		std::set<int> portals;			//Identificadors de les arestes que de la cel�la que s�n portals
		VoxelPointerTable voxels;		//Voxels que descriuen la cel�la
	};

	typedef std::unordered_map<int, Edge> EdgeTable;	//A table of edges accessed by its ID
	typedef std::unordered_map<int, Vertex> VertTable;	//A table of vertices accessed by its ID
	typedef std::unordered_map<int, Cell> CellTable;	//A table of cells accessed by its ID
	typedef std::unordered_set<int> PortalTable;		//A table that contains the IDs of those edges that are portals
	typedef std::pair<int, int> EdgeKey;
	typedef std::map<EdgeKey, int> VertsToEdgeTable;	//Given a pair of vertices, returns the ID of the edge that they form (if any)

	typedef std::vector<Ogre::Vector3>	ngTriangle;		//Representa els 3 v�rtexs del triangle
	typedef std::vector<ngTriangle>		ngTriangleList;	//Representa una llista de triangles

	//typedef std::list<int> Path;
};

#endif