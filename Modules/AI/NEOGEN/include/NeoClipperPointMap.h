#pragma once

#include "NeoTypes.h"
#include "NeoVector2.h"

namespace NEOGEN {

	struct ClipperPoint;
	typedef int ClipperPointID;
	typedef std::set<ClipperPoint*> AdjacencyList;
	typedef std::shared_ptr<ClipperPoint> ClipperPointSPtr;
	
	struct ClipperPoint {

		ClipperPointID _id;
		Vector2 _position;
		AdjacencyList _adjacencies;

		ClipperPoint() {}

		ClipperPoint(ClipperPointID id, const Vector2& pos) {
			_id = id;
			_position = pos;
		}
	};

	typedef std::map<ClipperPointID, ClipperPointSPtr> PointMap;
	typedef std::map<Vector2, ClipperPoint*, Vector2Cmp> PointMapPosition;
	
	class ClipperPointMap {

		public:

			ClipperPointMap();

			virtual ClipperPoint* getPoint(const Vector2& position);

			virtual void addPoints(const Polygon3& polygon);
			virtual void removePoint(ClipperPoint* point);

			virtual void addAdjacency(const Vector2& pos1, const Vector2& pos2);
			virtual void addAdjacency(ClipperPoint* p1, ClipperPoint* p2);
			virtual void removeAdjacency(ClipperPoint* p1, ClipperPoint* p2);

			/*virtual void fusion(ClipperPointMap& cPointMap);*/

			virtual size_t getNumPoints();
			
			virtual void clearOverlappingEdges();

			virtual bool isEmpty();
			
			virtual PointMap::iterator begin();
			virtual PointMap::iterator end();

		protected:

			ClipperPointID _newPointID;

			PointMap _pointMap;
			PointMapPosition _pointMapPosition;

			virtual ClipperPoint* createPoint(const Vector2& position);

	};

};