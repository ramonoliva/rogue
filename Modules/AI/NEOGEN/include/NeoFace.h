#pragma once

#include "NeoMeshElement.h"
#include "NeoEdge.h"
#include "NeoTriangulator.h"
#include "NeoAABB.h"

namespace NEOGEN {

	class NeoFace : public NeoMeshElement {

		public:

			enum Type {
				UNDEFINED,
				WALKABLE,
				OBSTACLE,
			};

			NeoFace(ElementID id);
			~NeoFace();

			virtual void begin();
			virtual void addEdge(NeoEdge* edge);
			virtual void end();

			virtual void setType(Type type);
			virtual Type getType();
			virtual bool isTypeUndefined();
			virtual bool isTypeWalkable();
			virtual bool isTypeObstacle();

			virtual NeoCell* getCell();
			virtual void setCell(NeoCell* cell);

			virtual void getContour(Polygon3& result);

			virtual void refresh();

			virtual NeoEdgePVector& getEdges();
			virtual void getEdgesTo(NeoFace* face, NeoEdgePSet& result);
			virtual NeoEdge* getNextEdge(NeoEdge* edge);
			virtual NeoEdge* getPrevEdge(NeoEdge* edge);

			virtual Vector3& getNormal();
			virtual Vector3& getCenter();
			virtual const NeoAABB& getAABB();

			virtual bool isCoplanarTo(NeoFace* face);

			virtual NeoVertexPVector& getVertices();
			virtual size_t getNumVertices();
			virtual NeoVertex* getVertex(size_t i);

			virtual Triangles& getTriangles();
			virtual size_t getNumTriangles();

			virtual void getAdjacents(NeoFacePSet& result);
			virtual void getAdjacentsCoplanar(NeoFacePSet& result);
			virtual bool isAdjacent(NeoFace* face);	//face shares one edge with this face

			virtual void getNeighbors(NeoFacePSet& result);
			virtual bool isNeighbor(NeoFace* face);	//face shares at least one vertex with this face
									
			virtual void triangulate();

			virtual void log();

		protected:

			Type _type;

			NeoEdgePVector _edges;		//Edges conforming the polygonal face
			NeoVertexPVector _vertices;	//Vertices conforming the polygonal face
			
			NeoCell* _cell;

			Triangles _triangles;		//The triangularization of this face. Used mainly for rendering purposes. 	

			Vector3 _normal;
			Vector3 _center;
			NeoAABB _aabb;

			virtual void computeNormal();
			virtual void computeCenter();
			virtual void computeAABB();

	};

};