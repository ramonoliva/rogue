#ifndef __ngVoxel_h_
#define __ngVoxel_h_

#include <OgreAxisAlignedBox.h>
#include <OgreRoot.h>
#include <OgreSceneManager.h>
#include <OgreEntity.h>
#include <OgreSceneNode.h>
#include <OgreStaticGeometry.h>

#define LAYER_INVALID -1

namespace NEOGEN {
	class Voxel : public Ogre::AxisAlignedBox {
	
	public:

		//Identificador del v�xel
		int keyX;
		int keyY;
		int keyZ;

		enum VOXEL_TYPE {
			TYPE_OUT = -1,
			TYPE_EMPTY = 1 << 0,
			TYPE_POSITIVE = 1 << 1,		//V�xel potencialment accessible
			TYPE_NEGATIVE = 1 << 2,		//V�xel inaccessible (obstacle)
			TYPE_ACCESSIBLE = 1 << 3,	//V�xel realment accessible (top v�xel que forma part de l'�rea caminable)
			TYPE_CONTOUR = 1 << 4,		//V�xel contorn (pot contenir part d'�rea caminable)
			TYPE_ANY = 65535
		};
	
		Voxel(const Ogre::Vector3& min, const Ogre::Vector3& max, int t = TYPE_EMPTY);
		~Voxel();

		int getType();
		void setType(int t);

		int getLayer();
		void setLayer(int l);

		void setPortal(bool p);
		bool isPortal();

		void setContour(bool c);
		bool isContour();

		void addCell(int c);
		bool hasCell(int c);
		size_t numCells();
		std::set<int>& getCells();

		void draw(const Ogre::String& matName, Ogre::StaticGeometry* sg = NULL);

	private: 

		int type;
		int layer;
		bool portal;
		bool contour;
		std::set<int> cells;	//Llista de cel�les que interseccionen amb el v�xel
		Ogre::SceneManager* mSceneMgr;
	};

	struct VoxelPointer {
		int x;
		int y;
		int z;
	
		VoxelPointer(int pX = 0, int pY = 0, int pZ = 0) {
			x = pX; 
			y = pY; 
			z = pZ;
		}
	};

	struct VoxelPointerCmp {
		bool operator()(const VoxelPointer& vc0, const VoxelPointer& vc1) const {
			//return strcmp(s1, s2) < 0;
			//Ogre::LogManager::getSingletonPtr()->logMessage("OPERATOR LESS");
			//Intento discriminar amb la coordenada (y), que indica el slice on es troba el v�xel
			if (vc0.y < vc1.y) return true;		//Voxel actual est� en un nivell inferior a vc1
			if (vc0.y > vc1.y) return false;	//Voxel actual est� en un nivell superior a vc2
		
			//Voxel actual i v�xel vc1 tenen la mateixa coordenada (y). Intento discriminar amb la coordenada (x)
			if (vc0.x < vc1.x) return true;		
			if (vc0.x > vc1.x) return false;	

			//Voxel actual i v�xel vc1 tenen les mateixes coordenades (x, y). La coordenada z discrimina
			return (vc0.z < vc1.z);
		}
	};

	typedef std::set<VoxelPointer, VoxelPointerCmp> VoxelPointerTable;
};

#endif