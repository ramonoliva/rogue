#ifndef __ngNavMeshGeneratorBase_h_
#define __ngNavMeshGeneratorBase_h_

#include <OgreManualObject.h>
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
#include <OgreEntity.h>
#include <OgreMaterialManager.h>
#include <OgreStaticGeometry.h>
#include <OgreTimer.h>
#include <OgreHardwarePixelBuffer.h>
#include <OgreRoot.h>

#include <string>
#include <algorithm>
#include <vector>
#include <list>

#include "ngPoly.h"
#include "ngVector.h"
#include "ngDataStructures.h"
#include "ngMapParser.h"
#include "ngGeometrySolver.h"
#include "ngGlobalVars.h"
#include "ngNavMesh.h"
#include "ngLayer.h"

namespace NEOGEN {

	typedef std::set<int>			PortalSet;
	typedef std::set<int>			NotchSet;
	typedef std::pair<int, int>		VertexPairID;
	typedef std::set<VertexPairID>	EdgeSetID;

	class NavMeshGeneratorBase {

		public:

			NavMeshGeneratorBase(Ogre::SceneManager* scMan);
			virtual ~NavMeshGeneratorBase();

			virtual NavMesh* getNavMesh();
			virtual void setPredefinedPortals(EdgeSetID& p);
			virtual void setData(Poly* f, PolygonList& h);
			virtual void setFloor(Poly* f);
			virtual void addHole(Poly* h);
			
			virtual bool generate() = 0;

			virtual void test();

		protected:

			bool _initialized;
			NavMesh* navMesh;
			Poly* floor;					//Pol�gon que representa el terra de l'escenari
			std::vector<Poly*> holes;		//possibles forats que tingui el terra
			PortalSet portals;				//Cont� els identificadors de les arestes que s�n portals
			std::vector<nmPoly> polyTable;	//Taula de pol�gons
			std::set<int> candidateEdges;
			int numCurrentCells;
			EdgeSetID _predefinedPortals;	//Portals that are manually created by the user

			Ogre::SceneManager* mSceneMgr;

			virtual void initializeData();

			virtual void addPoly(Poly* p);
			virtual int addSubEdge(int idP, int v1, int v2, NEOGEN::Edge::Type eType = NEOGEN::Edge::OBSTACLE);
			
			virtual void createCPG();
			virtual void portalSplit();
			virtual void reconstructCells();
			virtual int chooseNextEdge(Edge* eCurrent);

	};

};

#endif