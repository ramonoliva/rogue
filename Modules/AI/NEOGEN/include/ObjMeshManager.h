#ifndef __ObjMeshManager_H__
#define __ObjMeshManager_H__
 
#include <OgreResourceManager.h>
#include <OgreLogManager.h>
#include <OgreStringConverter.h>
#include "ObjMesh.h"
#include "ObjMeshPtr.h"

#include <vector>
 
class ObjMeshManager : public Ogre::ResourceManager, public Ogre::Singleton<ObjMeshManager> {

	public:

		ObjMeshManager(void);
		virtual ~ObjMeshManager(void);
		void initGameMapList(void);
 
		virtual GameMapPtr load(const Ogre::String &name, const Ogre::String &group = Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
		virtual GameMapPtr load(size_t pos);
 
		static ObjMeshManager &getSingleton(void);
		static ObjMeshManager *getSingletonPtr(void);

		size_t getNumResources(void);

	protected:
 
		// must implement this from ResourceManager's interface
		Ogre::Resource *createImpl(const Ogre::String &name, Ogre::ResourceHandle handle, 
        const Ogre::String &group, bool isManual, Ogre::ManualResourceLoader *loader, 
        const Ogre::NameValuePairList *createParams);

	private:

		std::vector<Ogre::String> gameMapList;
};
 
#endif