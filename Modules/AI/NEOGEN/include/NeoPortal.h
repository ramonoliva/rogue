#pragma once

#include "NeoEdge.h"

namespace NEOGEN {

	class NeoPortal : public NeoEdge {

		public:

			NeoPortal(ElementID id);

			virtual void addSubPortalBack(NeoEdge* sPortal);
			virtual void addSubPortalFront(NeoEdge* sPortal);
			virtual const NeoSubPortals& getSubPortals();

			virtual Vector3 getDirection();
			
			virtual NeoPortal* getOpposite();
			virtual void setOpposite(NeoPortal* opposite);

			virtual NeoCell* getCell();

		protected:

			NeoSubPortals _subPortals;

	};

};