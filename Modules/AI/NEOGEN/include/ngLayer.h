#ifndef __ngLayer_h_
#define __ngLayer_h_

namespace NEOGEN {
	
	struct Layer {
		
		std::set<Voxel*> voxels;			//Voxels que formen la layer
		std::vector<Voxel*> portalVoxels;	//V�xels que s�n portal entre dues layers
		std::set<int> conflictLayers;		//IDs de les layers amb les que la layer t� conflicte
		std::set<int> neighbors;			//IDs de les layers amb les que la layer est� connectada
				
		Voxel* voxelSeed;				//Seed d'aquesta layer que servir� per calcular la seed al refinar
		
		NavMesh* navMesh;					//La NavMesh de la layer

		Layer(int i) {
			navMesh = NULL;
			identifier = i;
		}
		
		~Layer() {
			voxels.clear();
			conflictLayers.clear();
			neighbors.clear();
			if (navMesh != NULL) delete navMesh;
			navMesh = NULL;
			voxelSeed = NULL;
		}

		int getID() {
			return identifier;
		}

		bool inConflict(int l) {
			//Indica si la layer est� en conflicte amb la layer l
			std::set<int>::iterator it = conflictLayers.find(l);
			return (it != conflictLayers.end());
		}

		void showNavMesh(bool s) {
			if (navMesh) navMesh->showNavMesh(s);
		}

	private:
		int identifier;
	};
};

#endif