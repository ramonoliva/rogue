#pragma once

#include "NeoTypes.h"

namespace NEOGEN {

	class NeoMeshElement {
		
		public:
			
			NeoMeshElement(ElementID id);

			virtual ElementID getID() const;

			virtual void log() {};

		protected:

			ElementID _id;

	};

	struct NeoMeshElementCmp {
		bool operator()(const NeoMeshElement* e1, const NeoMeshElement* e2) const {
			if (e1->getID() == e2->getID()) return false;
			return (e1->getID() < e2->getID());
		}
	};

};