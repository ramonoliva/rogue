#ifndef __ObjMesh_H__
#define __ObjMesh_H__
 
#include <OgreResourceManager.h>

class ObjMesh : public Ogre::Resource {
     
	public:
 
		ObjMesh(Ogre::ResourceManager *creator, 
				const Ogre::String &name, 
				Ogre::ResourceHandle handle, 
				const Ogre::String &group, 
				bool isManual = false, 
				Ogre::ManualResourceLoader *loader = 0);
 
		virtual ~ObjMesh(void);

		void setGeometryMode(int mode);
		int getGeometryMode(void);
		void setGeometryFile(const Ogre::String& fName);
		const Ogre::String& getGeometryFile(void) const;
		void setImgPreview(const Ogre::String& fName);
		const Ogre::String& getImgPreview(void) const;
		void setNavMeshFile(const Ogre::String& fName);
		const Ogre::String& getNavMeshFile(void) const;
		void setRedFlagPos(float x, float y, float z);
		void setBlueFlagPos(float x, float y, float z);
		void getRedFlagPos(float& x, float& y, float& z);
		void getBlueFlagPos(float& x, float& y, float& z);

	protected:
 		// must implement these from the Ogre::Resource interface
		void loadImpl(void);
		void unloadImpl(void);
		size_t calculateSize(void) const;

	private:

		int geometryMode;			//Indica si la informaci� del mapa est� en una imatge o en un fitxer .scene
		Ogre::String geometryFile;	//Fitxer que cont� la geometria del mapa (imatge o fitxer .scene)
		Ogre::String imgPreview;	//Imatge que cont� la preview del mapa
		Ogre::String navMeshFile;	//Fitxer que cont� la informaci� de la navMesh del mapa

		float flagRedPosX, flagRedPosY, flagRedPosZ;
		float flagBluePosX, flagBluePosY, flagBluePosZ;
};
 
#endif