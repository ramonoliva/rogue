#ifndef __ngPathFinder_h_
#define __ngPathFinder_h_

#include <vector>
#include "ngNavMesh.h"

#include <OgreLogManager.h>
#include "ngRealMath.h"
#include "ngPath.h"

namespace NEOGEN {

	struct State {

		enum StateTypeEnum {
			TYPE_NULL = -1,
			TYPE_OPEN, 
			TYPE_CLOSED
		};

		int cellID;			//Identificador de la cel�la
		int parentID;		//Identificador del node predecessor
		float f;			//Cost total del node (real + estimat)
		float g;			//Cost per arribar del node inicial a aquest node
		float h;			//Cost estimat per arribar d'aquest node al node final
		StateTypeEnum type;	//Obert o tancat

		State(int cID) {
			cellID = cID;
			reset();
		}

		void reset() {
			parentID = -1;
			f = 0.0f;
			g = 0.0f;
			h = 0.0f;
			type = TYPE_NULL;
		}

		void print() const {
			Ogre::LogManager::getSingletonPtr()->logMessage("cellID = " + std::to_string(cellID));
			Ogre::LogManager::getSingletonPtr()->logMessage("parentID = " + std::to_string(parentID));
			Ogre::LogManager::getSingletonPtr()->logMessage("f = " + std::to_string(f));
			Ogre::LogManager::getSingletonPtr()->logMessage("g = " + std::to_string(g));
			Ogre::LogManager::getSingletonPtr()->logMessage("h = " + std::to_string(h));
		}
	};

	struct StateCmp {
		bool operator()(const State* s0, const State* s1) const {
			if (s0->cellID == s1->cellID) {
				return false;
			}
			bool cmp;
			if (RealMath::equal(s0->f, s1->f)) {
				//Els dos estats tenen el mateix cost f.
				//Discrimino utilitzant el cost g (cost conegut per arribar fins l'estat)
				if (RealMath::equal(s0->g, s1->g)) {
					//Els dos estats tenen el mateix cost g. 
					//Discrimino utilitzant el cost h (cost estimat per arribar a l'estat final)
					if (RealMath::equal(s0->h, s1->h)) {
						//Els dos estats tenen el mateix cost h. 
						//Discrimino per id de cel�la
						cmp = (s0->cellID < s1->cellID);
					}
					else cmp = (s0->h < s1->h); //Els dos estats tenen cost h diferent
				}
				else cmp = (s0->g < s1->g);	//Els dos estats tenen cost g diferent
			}
			else cmp = (s0->f < s1->f);	//Els dos estats tenen cost f diferent
			return cmp;
		}
	};

	//typedef std::list<State> StateTable;
	typedef std::set<State*, StateCmp>	StateTable;			
	typedef std::map<int, State*>		StateKeyTable;		

	static void printStateTableSet(StateTable& states, const Ogre::String& name = "") {
		Ogre::LogManager::getSingletonPtr()->logMessage("PRINTING STATE TABLE " + name);
		for (StateTable::iterator it = states.begin(); it != states.end(); it++) {
			Ogre::LogManager::getSingletonPtr()->logMessage("=========================================");
			(*it)->print();
		}
		Ogre::LogManager::getSingletonPtr()->logMessage("=========================================");
	}

	class PathFinder {

		public:
			static PathFinder& getSingleton() {
				static PathFinder pFinder;
				return pFinder;
			};
			PathFinder();
			PathFinder(PathFinder const&);		// Don't Implement.
			void operator=(PathFinder const&);	// Don't implement
			~PathFinder();

			void initialize(NavMesh* nm);		
			void solve(int begin, int end, Vec3D& goalPosition, float charRadius, Path& path); 

		private:

			float minClearance2;
			int startID;
			int goalID;
			NavMesh* navMesh;
			StateTable openStates;				//Els estats oberts (s'han de visitar)
			StateKeyTable _stateList;			//The complete list of all states
			std::vector<int> _visitedStates;

			bool checkGoalClearance(Vec3D& goalPosition, float charRadius);
		
			void generateSuccessors(State* current);
		
			float computeG(int stateID, int parentID);
			float computeH(int stateID);
			float computeDistance(Cell* c1, Cell* c2);

			State* openState(int stateID, int parentID);
			State* reopenState(int stateID, int parentID);
			void closeState(State* s);

			void reconstructPath(std::map<int, int>& cameFrom, Path& path);
	};

};

#endif