#pragma once

#include "NeoGeometrySolverAux.h"

namespace NEOGEN {

	namespace NeoGeometrySolver {

		Vector3 getPolygonNormal(const Polygon3& polygon);

		bool getPolygonOrientation(const Polygon3& polygon, const Vector3& up = Vector3(0,1,0));
		bool getPolygonOrientation(const Triangle& triangle, const Vector3& up = Vector3(0,1,0));

		void getSubPolygon(const Polygon3& polygon, size_t startVertex, size_t endVertex, Polygon3& result);

		bool pointInPolygon(const Vector3& point, const Polygon3& polygon);

		NeoReal polygonArea(const Polygon3& polygon);
		
		bool isPolygonIntersection(const Polygon3& p1, const Polygon3& p2);
		bool isPolygonMonotone(const Polygon3& polygon);

		void polygonReverse(Polygon3& polygon);

		bool isTriangleBoxIntersection(const Polygon3& polygon, const NeoAABB& aabb);
		bool isTriangleTriangleIntersection(const Polygon3& t1, const Polygon3& t2);

		template<class T>
		bool isPointInSegment(const T& point, const T& s0, const T& s1) {
			//Indicates if point is in the finite segment defined by s0 and s1. 
			if (point.equal(s0) || point.equal(s1)) return true;

			NeoReal d = pointLineDistance(point, s0, s1);
			if (Math::greater(d, Math::NEO_REAL_PRECISION)) return false;

			NeoReal segmentLength = s0.dist(s1);
			NeoReal d0 = s0.dist(point);
			NeoReal d1 = s1.dist(point);
			return (Math::lessOrEqual(d0, segmentLength) && Math::lessOrEqual(d1, segmentLength));
		}

		bool linePlaneIntersection(const Line3& line, const Plane& plane, IntersectionResult3& result);
		bool linePlaneIntersection(const Vector3& p0, const Vector3& p1, const Plane& plane, IntersectionResult3& result);

		bool rayPlaneIntersection(const Ray3& ray, const Plane& plane, IntersectionResult3& result);

		bool segmentPlaneIntersection(const Vector3& p0, const Vector3& p1, const Plane& plane, IntersectionResult3& result);

		bool computeBarycentricCoordinates(const Vector3& a, const Vector3& b, const Vector3& c, const Vector3& p, Vector3& result);
		bool lineTriangleIntersection(const Line3& line, const Vector3& a, const Vector3& b, const Vector3& c, IntersectionResult3& result);
		bool lineTriangleIntersection(const Vector3& p0, const Vector3& p1, const Vector3& a, const Vector3& b, const Vector3& c, IntersectionResult3& result);

		bool rayTriangleIntersection(const Ray3& ray, const Vector3& a, const Vector3& b, const Vector3& c, IntersectionResult3& result);

		template<class T>
		bool lineLineIntersection(const Line<T>& line0, const Line<T>& line1, IntersectionResult<T>& result) {
			return lineLineIntersection(line0._v0, line0._v1, line1._v0, line1._v1, result);
		}
		bool lineLineIntersection(const Vector3& p0, const Vector3& p1, const Vector3& q0, const Vector3& q1, IntersectionResult3& result);
		bool lineLineIntersection(const Vector2& p0, const Vector2& p1, const Vector2& q0, const Vector2& q1, IntersectionResult2& result);

		template<class T>
		bool lineSegmentIntersection(const Line<T>& line0, const Line<T>& line1, IntersectionResult<T>& result) {
			return lineSegmentIntersection(line0._v0, line0._v1, line1._v0, line1._v1, result);
		}
		template<class T>
		bool lineSegmentIntersection(const T& p0, const T& p1, const T& q0, const T& q1, IntersectionResult<T>& result) {
			lineLineIntersection(p0, p1, q0, q1, result);
			if (result._intersectionType == IntersectionType::COINCIDENT) {
				result._intersectionPoint = q0;
				result._intersectionPoint2 = q1;
			}
			else if (result._intersectionType == IntersectionType::SINGLE_POINT) {
				if (!isPointInSegment(result._intersectionPoint, q0, q1)) {
					result._intersectionType = IntersectionType::UNDEFINED;
				}
			}

			return result.isIntersection();
		}

		template<class T> 
		bool segmentSegmentOverlapping(const Line<T>& s0, const Line<T>& s1) {
			return segmentSegmentOverlapping(s0._v0, s0._v1, s1._v0, s1._v1);
		}

		template<class T>
		bool segmentSegmentOverlapping(const T& p0, const T& p1, const T& q0, const T& q1) {
			if (!(p1 - p0).isParallelTo(q1 - q0)) return false;
			return (
				isPointInSegment(p0, q0, q1) ||
				isPointInSegment(p1, q0, q1) ||
				isPointInSegment(q0, p0, p1) ||
				isPointInSegment(q1, p0, p1)
				);
		}

		template<class T>
		bool segmentSegmentIntersection(const Line<T>& line0, const Line<T>& line1, IntersectionResult<T>& result) {
			return segmentSegmentIntersection(line0._v0, line0._v1, line1._v0, line1._v1, result);
		}
		template<class T>
		bool segmentSegmentIntersection(const T& p0, const T& p1, const T& q0, const T& q1, IntersectionResult<T>& result) {
			boost::geometry::model::segment<T> s0(p0, p1);
			boost::geometry::model::segment<T> s1(q0, q1);
			std::vector<T> iPoints;
			boost::geometry::intersection(s0, s1, iPoints);
			if (iPoints.size() == 0) {
				result._intersectionType = IntersectionType::UNDEFINED;
			}
			else if (iPoints.size() == 1) {
				result._intersectionType = IntersectionType::SINGLE_POINT;
				result._intersectionPoint = result._intersectionPoint2 = iPoints[0];
			}
			else if (iPoints.size() == 2) {
				result._intersectionType = IntersectionType::COINCIDENT;
				result._intersectionPoint = iPoints[0];
				result._intersectionPoint2 = iPoints[1];
			}

			return result.isIntersection();
		}

		bool rayBoxIntersection(const Ray3& ray, const NeoAABB& box);
		bool rayBoxIntersection(const Ray3& ray, const NeoAABB& box, IntersectionResult3& result);
		
		template<class T>
		NeoReal pointLineDistance(const T& p, const T& l0, const T& l1) {
			return std::sqrt(pointLineDistance2(p, l0, l1));
		}
		NeoReal pointLineDistance2(const Vector3& p, const Vector3& l0, const Vector3& l1);
		NeoReal pointLineDistance2(const Vector2& p, const Vector2& l0, const Vector2& l1);
		
		template<class T>
		T pointLineProjection(const T& p, const Line<T>& line) {
			return pointLineProjection(p, line._v0, line._v1);
		}
		template<class T>
		T pointLineProjection(const T& p, const T& l0, const T& l1) {
			T u = p - l0;
			T v = (l1 - l0).normalized();
			NeoReal d = u.dot(v);
	
			return (l0 + d * v);
		}
		
		template<class T>
		T pointSegmentProjection(const T& p, const Line<T>& line) {
			return pointSegmentProjection(p, line._v0, line._v1);
		}
		template<class T>
		T pointSegmentProjection(const T& p, const T& s0, const T& s1) {
			T proj = pointLineProjection(p, s0, s1);	//The projection of p in the infinite line defined by s0 and s1

			//Now simply check if the projection is between s0 and s1. If not, return the closest endpoint of the segment. 
			if (!isPointInSegment<T>(proj, s0, s1)) {
				T sDir = s1 - s0;
				T pDir = proj - s0;
				proj = (sDir.dot(pDir) > 0)? s1 : s0;
			}

			return proj;
		}

		Vector3 pointPlaneProjection(const Vector3& p, const Plane& plane);

	};

};