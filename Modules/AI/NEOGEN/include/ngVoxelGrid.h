#ifndef __ngVoxelGrid_h_
#define __ngVoxelGrid_h_

#include "ngVoxel.h"
#include <OgreRoot.h>
#include <OgreEntity.h>
#include <OgreSceneNode.h>
#include <OgreManualObject.h>
#include <OgreSceneManager.h>

namespace NEOGEN {
	class VoxelGrid {
	
		public:
			static VoxelGrid& getSingleton() {
				static VoxelGrid vGrid;
				return vGrid;
			};
			VoxelGrid();
			VoxelGrid(VoxelGrid const&);        // Don't Implement.
			void operator=(VoxelGrid const&);	// Don't implement
			~VoxelGrid();
		
			size_t getSizeX();
			size_t getSizeY();
			size_t getSizeZ();
			Ogre::Vector3& getVoxelDim();
			Ogre::Vector3& getHalfSize();
			void createGrid(Ogre::Entity* ent, float charRadius, float maxStepDepth);
			void createGrid(size_t sX, size_t sY, size_t sZ, float vDimX, float vDimY, float vDimZ);
			Voxel* getVoxel(VoxelPointer& vPointer);
			Voxel* getVoxel(int x, int y, int z);
			Voxel* getVoxelFromPos(Ogre::Vector3& realPos);
			void setVoxelType(int x, int y, int z, int newType);
			Ogre::AxisAlignedBox& getBoundingBox();
			VoxelPointerTable& getPositiveVoxels();
			VoxelPointerTable& getNegativeVoxels();
			VoxelPointerTable& getAccessibleVoxels();

			void drawGrid();
			void drawGrid(Ogre::ManualObject* manual, Ogre::Vector3& bbMin, Ogre::Vector3& bbMax);
		
		private:

			Ogre::SceneManager* mSceneMgr;

			typedef std::vector<std::vector<std::vector<Voxel*>>> VoxelMat3x3;
			Voxel* voxelOut;						//V�xel inv�lid que est� fora de la grid
			VoxelPointerTable positiveVoxels;		//Guarda els v�xels de la grid que s�n positius
			VoxelPointerTable negativeVoxels;		//Guarda els v�xels de la grid que s�n negatius
			VoxelPointerTable accessibleVoxels;		//Guarda els v�xels de la grid que s�n  accessibles 

			size_t sizeX, sizeY, sizeZ;				//Dimensions de la grid

			VoxelMat3x3 grid;
			Ogre::Vector3 voxelDim;
			Ogre::AxisAlignedBox bbGrid;			//BB que engloba tota l'escena a voxelitzar i representa una grid de v�xels
			Ogre::Vector3 bbHalfSize;				//Dimensions / 2 de la bb

			bool isOutOfBounds(int x, int y, int z);
			void drawBoundingBox(Ogre::ManualObject* manual, Ogre::Vector3& bbMin, Ogre::Vector3& bbMax);
	};
};

#endif