#ifndef __ngVertex_h_
#define __ngVertex_h_

#include "ngVector.h"

namespace NEOGEN {

	typedef std::deque<int> IncidentEdgesList;
	
	class Vertex {

		public:
			
			Vertex(Vec3D& pos = Vec3D(0.0f, 0.0f, 0.0f));
			virtual ~Vertex();

			virtual int getID();
			virtual void setID(int id);

			virtual Vec3D getPosition();
			virtual Vec2D getPosition2D(RealMath::ProjectionPlaneEnum projectionPlane = RealMath::PROJECTION_XZ);
			virtual void setPosition(Vec3D& pos);

			virtual int getConcEdge1();
			virtual int getConcEdge2();
			virtual void setConcEdge1(int edgeID);
			virtual void setConcEdge2(int edgeID);

			virtual IncidentEdgesList& getIncidentEdges();
			virtual size_t getNumIncidentEdges();
			virtual int getIncidentEdge(size_t pos);

			virtual void addIncidentEdgeBack(int edgeID);
			virtual void addIncidentEdgeFront(int edgeID);
			virtual void addIncidentEdgeAt(int edgeID, IncidentEdgesList::iterator& it);
			
			virtual void removeIncidentEdge(int edgeID);

			virtual bool hasIncidentEdge(int edgeID);

			virtual bool isShared();
			virtual bool isNotch();
			virtual void setShared(bool b);

			virtual void print();
		
	protected:

			int _id;
			Vec3D _position;	
			bool _shared;
			IncidentEdgesList _edges;	//arestes incidents a aquests v�rtexs, ordenades en relaci� a l'aresta de refer�ncia
		
			//Identificadors de les arestes que produeixen que el v�rtex sigui concau
			int _concEdge1;
			int _concEdge2;

			bool _isNotch;			
		};

};

#endif