#pragma once

#include "ngNavMeshGeneratorBase.h"

namespace NEOGEN {

	class NavMeshGeneratorManual : public NavMeshGeneratorBase {

		public:

			NavMeshGeneratorManual(Ogre::SceneManager* scMan);

			virtual bool generate();

		protected:

			virtual void initializeData();
			
	};

};

