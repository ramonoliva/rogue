#pragma once

#include "NeoMeshElement.h"
#include "NeoTypes.h"
#include "NeoVector3.h"

#include <map>

namespace NEOGEN {

	class NeoVertex : public NeoMeshElement {

		public:

			NeoVertex(ElementID id, const Vector3& position = Vector3(0.0, 0.0, 0.0));
			~NeoVertex();

			virtual const Vector3& getPosition() const;
			virtual void setPosition(const Vector3& position);

			virtual Vector3 getNormal();

			virtual void addIncidentEdge(NeoEdge* edge);
			virtual void removeIncidentEdge(NeoEdge* edge);
			virtual bool isIncidentEdge(NeoEdge* edge);
			virtual NeoEdgePSet& getIncidentEdges();
			virtual void getIncidentEdgesIn(NeoEdgePSet& result);
			virtual void getIncidentEdgesOut(NeoEdgePSet& result);
			virtual size_t getNumIncidentEdges();

			virtual bool isIncidentFace(NeoFace* face);
			virtual void getIncidentFaces(NeoFacePSet& result);

			virtual bool isObstacle();
			virtual bool isNotch(NeoEdge* edgeIn);
			virtual bool isNotch(const Vector3& edgeInDir);

		protected:

			Vector3 _position;

			NeoEdgePSet _incidentEdges;

			virtual NeoEdge* getEdgeObstacle(NeoEdgePSet& edges);
			virtual NeoEdge* getIncidentEdgeObstacle(const Vector3& edgeDir);

	};

};