#ifndef __GameMapSerializer_H__
#define __GameMapSerializer_H__
 
#include <OgreSerializer.h>
#include <OgreLogManager.h>
#include <OgreStringConverter.h>

#include "ObjMesh.h"
 
class ObjMeshSerializer : public Ogre::Serializer {
	public:
    ObjMeshSerializer();
    virtual ~ObjMeshSerializer();
 
    void exportObjMesh(const ObjMesh *obj, const Ogre::String &fileName);
    void importObjMesh(Ogre::DataStreamPtr &stream, GameMap *pDest);
};
 
#endif