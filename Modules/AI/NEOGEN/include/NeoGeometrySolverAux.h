#pragma once

#include "NeoTypes.h"
#include "NeoVector3.h"

namespace NEOGEN {

	namespace NeoGeometrySolver {

		enum IntersectionType {
			UNDEFINED,
			PARALLEL,		//The line and the plane are parallel and the line is not contained in the plane. 
			COINCIDENT,		//The line and the plane are parallel and the line is contained in the plane. 
			SINGLE_POINT,	//The line and the plane are not parallel and therefore, they intersect in a single point. 
		};

		template<class T>
		struct IntersectionResult {

			T _intersectionPoint;
			T _intersectionPoint2;	//The second intersection point. It only has sense in the COINCIDENT intersection type. 
			IntersectionType _intersectionType;

			IntersectionResult() {
				_intersectionType = IntersectionType::UNDEFINED;
			}

			virtual bool isIntersection() {
				return ((_intersectionType == IntersectionType::SINGLE_POINT) || (_intersectionType == IntersectionType::COINCIDENT));
			}

		};

		typedef IntersectionResult<Vector2> IntersectionResult2;
		typedef IntersectionResult<Vector3> IntersectionResult3;

		template<class T>
		struct Line {
			T _v0;
			T _v1;

			Line(const T& v0, const T& v1) {
				_v0 = v0;
				_v1 = v1;
			}
		};

		typedef Line<Vector2> Line2;
		typedef Line<Vector3> Line3;

		template<class T>
		struct Ray {
			T _v0;
			T _direction;
			NeoReal _length;

			Ray(const T& v0, const T& direction, NeoReal length = Math::NEO_REAL_INFINITY) {
				_v0 = v0;
				_direction = direction.normalized();
				_length = length;
			}
		};

		typedef Ray<Vector2> Ray2;
		typedef Ray<Vector3> Ray3;

		struct Plane {
			Vector3 _v0;
			Vector3 _normal;
			Vector3 _hintDir;	//Defines the direction of the semiplane. Set it to ZERO to use an ordinary infinite plane. 

			Plane(const Vector3& v0, const Vector3& normal, const Vector3& hintDir = Vector3(0,0,0)) {
				_v0 = v0;
				_normal = normal.normalized();
				_hintDir = hintDir;
			}

		};

	};

};