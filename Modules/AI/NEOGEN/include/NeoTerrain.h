#pragma once

#include "NeoClipper.h"
#include "NeoMesh.h"
#include "NeoGeometrySolver.h"

namespace NEOGEN {

	class NeoTerrain : public NeoMesh {

		public:

			int _test;

			NeoTerrain(Vector3List& pointList, ElementIDVector& triangleList);
			virtual ~NeoTerrain();

			virtual NeoFacePSet& getFacesWalkable();
			virtual NeoFacePSet& getFacesObstacle();
			virtual NeoMesh* getMeshAccessible();

			virtual void applySlopeConstraint(NeoReal maxSlope);
			virtual void applyCeilConstraints(NeoReal height);

		protected:

			typedef std::map<NeoFace*, ElementIDVector> AccessibleFaceMap;

			NeoFacePSet _facesWalkable;
			NeoFacePSet _facesObstacle;

			AccessibleFaceMap _accessibleFaceMap;
			NeoMeshUPtr _meshAccessible;

			virtual void addFaceObstacle(NeoFace* face);
			virtual void addFaceWalkable(NeoFace* face);
						
			virtual void applyCeilConstraints(NeoFace* face, NeoReal height);
			virtual void refineCeil(NeoReal height, NeoFace* face, NeoFace* rawCeil, Polygon3& result);
			virtual void createFaces(ClipperLib::PolyNode* node, NeoFace* face, NeoReal height, NeoFacePSet& ceilFaces);
			virtual void sewMeshAccessible();
			virtual void sewAccessibleFaces(const ElementIDVector& s1, const ElementIDVector& s2);
			virtual void sewAccessibleFaces(NeoFace* f1, NeoFace* f2);
			virtual NeoFace* reconstructAccessibleFace(NeoFace* f, const Vector3& e1Begin, const Vector3& e1End, const Vector3& e2Begin, const Vector3& e2End);

	};

};