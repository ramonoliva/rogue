#ifndef __ngVector_h_
#define __ngVector_h_

#include <math.h>
#include <cmath>
#include "ngRealMath.h"
#include <iostream>

#include <OgreLogManager.h>
#include <OgreStringConverter.h>

#include <string>

namespace NEOGEN {
	class Vec2D {
	
		public:

			Vec2D(float px = 0.0f, float py = 0.0f) {
				x = px;
				y = py;
			};
		
			~Vec2D(){};

			/*********************************************************/
			/* GET I SET */
			/*********************************************************/
			float getX() {
				return x;
			};

			float getY() {
				return y;
			};

			void setX(float pX) {
				x = pX;
			};

			void setY(float pY) {
				y = pY;
			};

			std::string toString() {
				std::ostringstream ss;
				ss << "(" << x << ", " << y << ")";
				
				return ss.str();
			}
		
			/*********************************************************/
			/* OPERATORS */
			/*********************************************************/
			bool equal(const Vec2D& right, float epsilon = FLOAT_PRECISION) const{
				return (RealMath::equal(x, right.x, epsilon) && RealMath::equal(y, right.y, epsilon));
			};

			bool operator!=(const Vec2D& right) const {
				return (!RealMath::equal(x, right.x) || !RealMath::equal(y, right.y));
			};

			Vec2D operator+(const Vec2D& p) const{
				return Vec2D(x + p.x, y + p.y);
			};

			Vec2D& operator+=(const Vec2D& p){
				x += p.x;
				y += p.y;
				return *this;
			};

			Vec2D operator-(const Vec2D& p) const{
				return Vec2D(x - p.x, y - p.y);
			};

			Vec2D& operator-=(const Vec2D& p){
				x -= p.x;
				y -= p.y;
				return *this;
			};

			Vec2D operator*(float d) const{
				return Vec2D(x * d, y * d);
			};

			Vec2D& operator*=(float d){
				x *= d;
				y *= d;
				return *this;
			};

			Vec2D operator/(float d) const{
				return Vec2D(x / d, y / d);
			};

			Vec2D& operator/=(float d){
				x /= d;
				y /= d;
				return *this;
			};
				
			/*********************************************************/
			/* OPERACIONS DE CLASSE */
			/*********************************************************/
			void rotate(float alpha) {
				//Rota el v�rtex alpha radians
				float sinAlpha = (float)sin((double)alpha);
				float cosAlpha = (float)cos((double)alpha);
	
				float currentX = x;
				float currentY = y;
				x = (currentX * cosAlpha) - (currentY * sinAlpha);
				y = (currentX * sinAlpha) + (currentY * cosAlpha);
			};

			float dotProduct(const Vec2D &p) const{
				return ((x * p.x) + (y * p.y));
			};

			float norm() const{
				return std::sqrtf(x * x + y * y);
			};

			void normalize() {
				float n = norm();
				x /= n;
				y /= n;
			};

			Vec2D normalized(){
				float n = norm();
				return Vec2D(x/n, y/n);
			};

			float dist2(Vec2D& p2) {
				//Calcula la dist�ncia^2 entre 2 punts en el pl� XZ (y = 0)
				return ((x - p2.x)*(x - p2.x) + (y - p2.y)*(y - p2.y));
			};

			float dist(Vec2D& p2) {
				//Calcula la distancia entre el punt self i un punt donat p2 en el pl� XZ (y = 0)
				return (std::sqrtf(dist2(p2)));
			};

			Vec2D perpVector() {
				return Vec2D(y, -x);
			};

			float perpProduct(Vec2D& v) {
				//El producte perpendicular es defineix com el dot product entre self i 
				//un vector perpendicular a v
				return dotProduct(v.perpVector());
			};

		private:

			float x, y;
	};

	inline Vec2D operator*(float d, Vec2D& v) {
		return Vec2D(v.getX() * d, v.getY() * d);
	};

	/****************************************************************************************/

	class Vec3D {
	
		public:

			Vec3D(float px = 0.0f, float py = 0.0f, float pz = 0.0f) {
				x = px;
				y = py;
				z = pz;
			};

			~Vec3D() {};

			/*********************************************************/
			/* GET I SET */
			/*********************************************************/
			float getX() {
				return x;
			};

			float getY() {
				return y;
			};

			float getZ() {
				return z;
			};

			void setX(float pX) {
				x = pX;
			};

			void setY(float pY) {
				y = pY;
			};

			void setZ(float pZ) {
				z = pZ;
			};
		
			inline void setCoords(float cx, float cy, float cz) {
				x = cx;
				y = cy;
				z = cz;
			};
		
			inline void getCoords(float& cx, float& cy, float& cz) {
				cx = x;
				cy = y;
				cz = z;
			};

			/*********************************************************/
			/* OPERATORS */
			/*********************************************************/
			bool equal(const Vec3D& right, float epsilon) const {
				return (RealMath::equal(x, right.x, epsilon) && RealMath::equal(y, right.y, epsilon) && RealMath::equal(z, right.z, epsilon));
			};

			Vec3D operator+(const Vec3D& p) const{
				return Vec3D(x + p.x, y + p.y, z + p.z);
			};

			Vec3D& operator+=(const Vec3D& p){
				x += p.x;
				y += p.y;
				z += p.z;
				return *this;
			};

			Vec3D operator-(const Vec3D& p) const{
				return Vec3D(x - p.x, y - p.y, z - p.z);
			};

			Vec3D& operator-=(const Vec3D& p){
				x -= p.x;
				y -= p.y;
				z -= p.z;
				return *this;
			};

			Vec3D operator*(float d) const{
				return Vec3D(x * d, y * d, z * d);
			};

			Vec3D& operator*=(float d){
				x *= d;
				y *= d;
				z *= d;
				return *this;
			};

			Vec3D operator/(float d) const{
				return Vec3D(x / d, y / d, z / d);
			};

			Vec3D& operator/=(float d){
				x /= d;
				y /= d;
				z /= d;
				return *this;
			};
				
			/*********************************************************/
			/* OPERACIONS DE CLASSE */
			/*********************************************************/
			Vec3D crossProduct(const Vec3D &p) const{
				return Vec3D(y * p.z - z * p.y, z * p.x - x * p.z, x * p.y - y * p.x);
			};
		
			float dotProduct(const Vec3D &p) const{
				return ((x * p.x) + (y * p.y) + (z * p.z));
			};

			float norm2() const{
				return (x * x + y * y + z * z);
			};

			float norm() const{
				return std::sqrtf(norm2());
			};

			void normalize(){
				float n = norm();
				x /= n;
				y /= n;
				z /= n;
			};

			Vec3D normalized(){
				float n = norm();
				return Vec3D(x/n, y/n, z/n);
			};

			float dist2(Vec3D& p2) {
				//Calcula la dist�ncia^2 entre 2 punts
				return ((x - p2.x)*(x - p2.x) + (y - p2.y)*(y - p2.y) + (z - p2.z)*(z - p2.z));
			};

			float dist(Vec3D& p2) {
				//Calcula la distancia entre 2 punts
				return (std::sqrtf(dist2(p2)));
			};

			void printData() {
				Ogre::LogManager::getSingletonPtr()->logMessage("x = " + std::to_string((float)x));
				Ogre::LogManager::getSingletonPtr()->logMessage("y = " + std::to_string((float)y));
				Ogre::LogManager::getSingletonPtr()->logMessage("z = " + std::to_string((float)z));
			};

		private:

			float x, y, z;
	};

	inline Vec3D operator*(float d, Vec3D p) {
		return Vec3D(p.getX() * d, p.getY() * d, p.getZ() * d);
	};

	typedef std::vector<Vec3D> PointList;

};

#endif