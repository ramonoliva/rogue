#pragma once

#include "NeoTypes.h"
#include "NeoVector2.h"
#include "NeoVector3.h"
#include "NeoMath.h"
#include "NeoGeometrySolver.h"

#include "clipper.hpp"

namespace NEOGEN {
	
	class NeoClipper {

		public:

			typedef ClipperLib::ClipType Operation;
			typedef std::vector<std::pair<Polygon3, Polygons3>> Result;

			static NeoReal getClipperScaleFactor() {
				return NeoReal(100000000.0);	
			}

			static ClipperLib::IntPoint toClipper(const Vector3& v) {
				return toClipper(Vector2(v.getX(), v.getZ()));
			}

			static ClipperLib::IntPoint toClipper(const Vector2& v) {
				NeoReal scaleFactor = getClipperScaleFactor();	
				ClipperLib::cInt pX = ClipperLib::cInt(v.getX() * scaleFactor);
				ClipperLib::cInt pY = ClipperLib::cInt(v.getY() * scaleFactor);
				return ClipperLib::IntPoint(pX, pY);
			}

			static void toClipper(const Polygon3& polygon, ClipperLib::Path& result) {
				for (Polygon3::const_iterator it = polygon.begin(); it != polygon.end(); it++) {
					ClipperLib::IntPoint p = toClipper(*it);
					result.push_back(p);
				}
			}

			static Vector3 toNeogen(const ClipperLib::IntPoint& p) {
				NeoReal scaleFactor = 1.0 / getClipperScaleFactor();
				return Vector3(p.X * scaleFactor, NeoReal(0.0), p.Y * scaleFactor);
			}

			static void toNeogen(const ClipperLib::Path& path, Polygon3& result) {
				for (ClipperLib::Path::const_iterator it = path.begin(); it != path.end(); it++) {
					result.push_back(toNeogen(*it));
				}
			}

			/*static bool isKeyPoint(const ClipperLib::IntPoint& p) {
				return (p.Z == ClipperLib::cInt(1));
			}*/

			virtual void applyCeilConstraints(const Polygon3& subject, const Polygons3& ceilConstraints, ClipperLib::PolyTree& solution);

			virtual void execute(const Polygon3& polygonA, const Polygon3& polygonB, NeoClipper::Operation op, Polygon3& floor, Polygons3& holes);

		protected:

			ClipperLib::Clipper _clipper;

			virtual void addPolygonSubject(const Polygon3& polygon);
			virtual void addPolygonsClip(const Polygons3& clippers);
			virtual void addPolygonClip(const Polygon3& polygon);

	};

};