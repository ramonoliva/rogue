#ifndef __ngObstacleSolver_h_
#define __ngObstacleSolver_h_

#include <Ogre.h>
#include <OgreTexture.h>
#include <OgreImage.h>
#include <OgreResourceGroupManager.h>
#include <vector>
#include <deque>

#include "ngPoly.h"
#include "ngGeometrySolver.h"

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/linestring.hpp>
#include <boost/geometry/geometries/point_xy.hpp>

#define FLOAT_EPSILON	0.00001

namespace NEOGEN {
	class ObstacleSolver {

		//Classe que s'encarrega de separar els obstacles de la geometria per on el personatge
		//pot caminar

		public:

			ObstacleSolver();
			void initializeData(Ogre::TexturePtr normalDepthTex, float mSlope, float mHeight, float zN, float zF, float cOffset, float tRes);
			~ObstacleSolver();

			std::vector<Poly*>& getObstacles();

			float getElapsedTime();

			size_t getMapWidth();
			size_t getMapHeight();

			void generateObstaclePartition(size_t refX, size_t refY);
		
			void savePartition(const Ogre::String& imgName);

		private:

			enum {
				PIXEL_TYPE_NEGATIVE, 
				PIXEL_TYPE_POSITIVE, 
				PIXEL_TYPE_ACCESSIBLE 
			} PixelTypeEnum;

			struct IntPair {
				int x;
				int y;

				IntPair() {
					x = 0;
					y = 0;
				}

				IntPair(int i1, int i2) {
					x = i1;
					y = i2;
				}
			};

			struct IntPairCmp {
				bool operator()(const IntPair& ip0, const IntPair& ip1) const {
					//return strcmp(s1, s2) < 0;
					//Ogre::LogManager::getSingletonPtr()->logMessage("OPERATOR LESS");
					//Intento discriminar amb la coordenada (y), que indica el slice on es troba el v�xel
					if (ip0.x < ip1.x) return true;			//Si la coordenada x �s inferior, retorna cert
					else if (ip0.x > ip1.x) return false;	//La coordenada x �s major, retorna fals
					return (ip0.y < ip1.y);
				}
			};

			struct Pixel {
				int label;
				int type;
				bool isContour;
				bool portal;
				bool strong;
				float depth;

				Pixel(int l, int t, bool c, bool p, float d) {
					label = l;
					type = t;
					isContour = c;
					portal = p;
					depth = d;
					strong = false;
				}
			};

			unsigned long elapsedTime;
			unsigned long timeBefore;
			unsigned long timeAfter;
			Ogre::Timer* timer;

			typedef std::set<std::pair<float, float>>				VertContourSet;
			typedef std::vector<std::pair<size_t, size_t>>			ContourMap;
			typedef std::vector<std::pair<int, int>>				PixelVector;
			typedef PixelVector::iterator							PixelVectorIterator;
			typedef boost::geometry::model::point<double, 3, boost::geometry::cs::cartesian> BoostPoint;
		
			Ogre::Image normalDepthMap;											//Imatge que cont� la informaci� de les normals i la profunditat
			size_t normalDepthMapWidth;
			size_t normalDepthMapHeight;
			Ogre::Image obstaclePartition;										//Imatge bin�ria que cont� la partici� de l'escenari en terra i obstacles
			Ogre::TexturePtr obstaclePartitionTex;								//Textura que cont� la partici� de l'escenari en terra i obstacles
			std::vector<std::vector<bool>>	pixelVisited;						//Indica si un p�xel ja ha estat visitat durant el proc�s de flooding
			std::vector<std::vector<Pixel>>	pixelTable;							//Indica si un p�xel ja ha estat visitat durant el proc�s de flooding
			std::vector<Poly*> obstacles;

			ContourMap contourMap;			//Cont�, per cada p�xel de contorn, 
			VertContourSet contourVerts;	//Cont� els v�rtexs de contorn
				
			float maxSlope;			//Pendent m�xima que un personatge pot caminar
			float maxStepHeight;	//Al�ada m�xima d'un obstacle que el personatge pot superar caminant
			float zNear, zFar;
			float cameraOffset;
			float cosMaxSlope;		//Cosinus de la pendent m�xima
			float pixelsPerUnit;	//Resoluci� de la textura utilitzada (p�xels per unitat de model)

			void initPixelVisited(size_t w, size_t h);
			void initPixelTable();
			void initPixelLabel(size_t w, size_t h);
			void addPixelContour(size_t x, size_t y, int label = -1);
			void addVertContour(float x, float y);
			void setPixelContourLabel(size_t x, size_t y, int label);
			bool pixelContourExist(int x, int y);

			void createNormalDepthMap(Ogre::TexturePtr normalDepthTex);
			void setColourAt(Ogre::TexturePtr& tex, size_t x, size_t y, Ogre::ColourValue& colour);

			void detectObstacles(int refX, int refY);
			void scanLine(std::deque<std::pair<int, int>>& seeds, std::pair<int, int>& s, int incr, bool& seedUP, bool& seedDOWN);
			bool isLimitPixel(size_t pX, size_t pY);
			bool isOutOfBounds(int pX, int pY);
			void polygonReconstruction();
			void createPolygon(std::pair<size_t, size_t>& initialPixelID, int label);
			float angleBetweenVectors(Vec3D& v1, Vec3D& v2);
			void simplifyPolygon(PixelVector& lPoint, int label);
			bool isPixelContourAlligned(size_t x, size_t y);
									
			void contourPaint(Ogre::Image& img);
			void printLabelContours();
	};
};

#endif
