#pragma once

#include "poly2tri.h"
#include "NeoTypes.h"

namespace NEOGEN {

	class NeoPoly2Tri {

		typedef std::map<Vector2, Vector3, Vector2Cmp> Point2ToPoint3Map;
		typedef std::vector<p2t::Point*> p2tPolyline;
		typedef std::vector<p2tPolyline> p2tPolylines;
		typedef p2t::Triangle p2tTriangle;
		typedef std::vector<p2tTriangle*> p2tTriangles;

		public:

			static void triangulate(const Polygon3& floor, Polygons3& result);
			static void triangulate(const Polygon3& floor, const Polygons3& holes, Polygons3& result);

			static void triangulate(const Polygon2& floor, Polygons2& result);
			static void triangulate(const Polygon2& floor, const Polygons2& holes, Polygons2& result);

			static void toPoly2Tri(const Polygon3& polygon, Point2ToPoint3Map& mResult, Polygon2& pResult);
			static void toPoly2Tri(const Polygon2& polygon, p2tPolyline& result);

			static void toNeogen(p2tTriangle& triangle, Polygon2& result);
			static void toNeogen(p2tTriangles& triangles, Polygons2& result);

		protected:

			template <class C> 
			static void freeClear(C & cntr) {
				for (typename C::iterator it = cntr.begin(); it != cntr.end(); ++it) {
					delete * it;
				}
				cntr.clear();
			}

	};

};