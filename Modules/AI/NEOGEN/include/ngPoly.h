#ifndef __ngPoly_h_
#define __ngPoly_h_

#include <OgreManualObject.h>
#include <OgreLogManager.h>
#include <OgreStringConverter.h>
#include <OgreSceneNode.h>

#include "ngVector.h"
#include <vector>
#include "ngTriangulator.h"
#include <OgreSceneManager.h>
#include <OgreRoot.h>

#include "ngVertex.h"

namespace NEOGEN {

	typedef std::vector<Vertex> VertexList;
	
	class Poly {

		/*Representa un pol�gon*/

		public:

			Poly();
			Poly(VertexList& l, bool needsTriangulation = false);
			~Poly();

			void clearManualObject();

			VertexList& getVertexList();
			unsigned int getNumVert();
			Vertex& getVertexAt(size_t idVert);
			void getCenterCoords(float& cX, float& cY, float& cZ);
			void getCenter(Vec3D& result);
			void setPolyName(Ogre::String name);
			Ogre::String getPolyName();
			void setVisible(bool v);

			float area();

			void reverse();
				
			void draw(const Ogre::String& materialName, bool solid = true);
			void drawSolid(const Ogre::String& materialName);
			void drawWireFrame(const Ogre::String& materialName);
			void printData();

		private:

			Ogre::String polyName;
			Ogre::SceneManager* mSceneMgr;
			Ogre::ManualObject* manualObject;

			VertexList lVert;			//llista de v�rtexs del pol�gon (ordre counterclokcside)
			VectorVertID lTriangle;			//Llista de triangles que representa la triangulaci� del pol�gon (per poder-lo dibuixar en mode s�lid)
			int polyOrient;					//indica si el pol�gon est� orientat en sentit antihorari o horari. 
			bool triangulated;
			bool _validCenter;
			Vec3D _center;

			void setVerts(VertexList& l);
			float det2(float a11, float a12, float a21, float a22);
			void triangulate();
	};

	typedef std::vector<Poly*> PolygonList;
};

#endif