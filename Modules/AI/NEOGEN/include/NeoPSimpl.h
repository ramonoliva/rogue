#pragma once

#include "NeoTypes.h"
#include "NeoGeometrySolver.h"
#include "psimpl.h"

#include <OgreLogManager.h>
#include <OgreStringConverter.h>

namespace NEOGEN {

	namespace NeoPSimpl {

		template<class T>
		struct IntersectionPointsSorter {
			T _begin;
			T _end;
			
			IntersectionPointsSorter(T begin, T end) {
				_begin = begin;
				_end = end;
			}

			bool operator()(T v1, T v2) {
				NeoReal tmp1 = _begin.dist2(v1);
				NeoReal tmp2 = _begin.dist2(v2);

				return Math::less(tmp1, tmp2);
			}
		};

#pragma region HELPERS

		static size_t toPSimpl(const Polygon3& polygon, NeoRealVector& result) {
			for (Polygon3::const_iterator itPoint = polygon.begin(); itPoint != polygon.end(); itPoint++) {
				result.push_back(itPoint->getX());
				result.push_back(itPoint->getY());
				result.push_back(itPoint->getZ());
			}

			return 3;
		}

		static size_t toPSimpl(const Polygon2& polygon, NeoRealVector& result) {
			for (Polygon2::const_iterator itPoint = polygon.begin(); itPoint != polygon.end(); itPoint++) {
				result.push_back(itPoint->getX());
				result.push_back(itPoint->getY());
			}

			return 2;
		}

		static void toNeogen(NeoRealVector& v, Polygon3& result) {
			result.clear();
			for (size_t i = 0; i < v.size(); i+= 3) {
				result.push_back(Vector3(v[i], v[i+1], v[i+2]));
			}
			
			if (result.size() > 0) {
				//Check if the first and last vertices of the polygon are the same. In this case, we remove the last one. 
				Vector3 pBegin = result[0];
				Vector3 pEnd = result[result.size() - 1];
				if (pBegin.equal(pEnd)) result.pop_back();
			}
		}

		static void toNeogen(NeoRealVector& v, Polygon2& result) {
			result.clear();
			for (size_t i = 0; i < v.size(); i += 2) {
				result.push_back(Vector2(v[i], v[i + 1]));
			}

			if (result.size() > 0) {
				//Check if the first and last vertices of the polygon are the same. In this case, we remove the last one. 
				Vector2 pBegin = result[0];
				Vector2 pEnd = result[result.size() - 1];
				if (pBegin.equal(pEnd)) result.pop_back();
			}
		}

		template <class T>
		static void checkEndpoints(T& polygon, NeoReal tolerance = Math::NEO_REAL_PRECISION) {
			//As most of this algorithms considers the input an open polyline, the 
			//first and last vertices have to be treated as an special case. 
			if (polygon.size() < 3) return;

			//Check the first vertex
			NeoReal d = NeoGeometrySolver::pointLineDistance(polygon[0], polygon[1], polygon[polygon.size() - 1]);
			if (d < tolerance) {
				//The first vertex must be removed. The output structure is a vector and we only can remove
				//efficiently the last one. So reverse, remove the last element (the first one in the original vector)
				//and reverse it again. 
				std::reverse(polygon.begin(), polygon.end());
				polygon.pop_back();
				std::reverse(polygon.begin(), polygon.end());
			}

			if (polygon.size() < 3) return;

			//Check the last vertex
			d = NeoGeometrySolver::pointLineDistance(polygon[polygon.size() - 1], polygon[0], polygon[polygon.size() - 2]);
			if (d < tolerance) polygon.pop_back();
		}

#pragma endregion

#pragma region CLEAN POLYGON

		template<class T, class U>
		void cleanPolygon(std::vector<T>& polygon, NeoReal distTolerance = Math::NEO_REAL_PRECISION) {
			std::vector<T> result;
			cleanPolygon<T, U>(polygon, result, distTolerance);
			polygon = result;
		}

		template<class T, class U>
		void cleanPolygon(const std::vector<T>& polygon, std::vector<T>& result, NeoReal distTolerance = Math::NEO_REAL_PRECISION) {
			if (polygon.size() < 3) return;

			std::vector<T> r1, r2;
			
			simplifyRadialDistance(polygon, r1, distTolerance);		//Remove "equal" vertices
			simplifyPerpendicularDistance(r1, r2, distTolerance);	//Remove "collinear" edges
			simplifyOverlappingEdges<T, U>(r2, result);				//Remove overlapping edges
		}

		template<class T, class U>
		void simplifyOverlappingEdges(const std::vector<T>& polygon, std::vector<T>& result) {
			//1) Generate all the intersection points of between all the edges in the polygon. 
			std::vector<T> tmpResult;
			size_t numVertices = polygon.size();
			for (size_t i = 0; i < numVertices; i++) {
				//For each edge, check the intersections against the other edges. 
				T eBegin = polygon[i];
				T eEnd = polygon[(i + 1) % numVertices];
				std::set<T, U> intersections;
				intersections.insert(eBegin);
				intersections.insert(eEnd);

				for (size_t j = 0; j < numVertices; j++) {
					if (i == j) continue;
					T tmpBegin = polygon[j];
					T tmpEnd = polygon[(j + 1) % numVertices];

					IntersectionResult<T> iResult;
					if (segmentSegmentIntersection(eBegin, eEnd, tmpBegin, tmpEnd, iResult)) {
						intersections.insert(iResult._intersectionPoint);
						if (iResult._intersectionType == IntersectionType::COINCIDENT) intersections.insert(iResult._intersectionPoint2);
					}
				}

				//Sort the intersection points
				IntersectionPointsSorter<T> sorter(eBegin, eEnd);
				std::vector<T> vSorted(intersections.begin(), intersections.end());
				std::sort(vSorted.begin(), vSorted.end(), sorter);

				//Add the sorted points to the result
				tmpResult.insert(tmpResult.end(), vSorted.begin(), vSorted.end());
			}

			simplifyRadialDistance(tmpResult);

			//2) Now we check for overlapping edges in the result, i.e., edges that appear in both orders. 
			numVertices = tmpResult.size();
			for (size_t i = 0; i < numVertices; i++) {
				T eBegin = tmpResult[i];
				T eEnd = tmpResult[(i + 1) % numVertices];

				bool overlap = false;
				for (size_t j = 0; j < numVertices; j++) {
					T tmpBegin = tmpResult[j];
					T tmpEnd = tmpResult[(j + 1) % numVertices];

					overlap = eBegin.equal(tmpEnd) && eEnd.equal(tmpBegin);
					if (overlap) break;
				}

				if (!overlap) result.push_back(eBegin);
			}

			//result = polygon;
		}

		/*template<class T>
		void cleanPolygonEdgesAtAngle(std::vector<T>& polygon, NeoReal angle, NeoReal tolerance = Math::NEO_REAL_PRECISION) {
			std::vector<T> result;
			cleanPolygonEdgesAtAngle(polygon, angle, result, tolerance);
			polygon = result;
		}

		template<class T>
		void cleanPolygonEdgesAtAngle(const std::vector<T>& polygon, NeoReal angle, std::vector<T>& result, NeoReal tolerance = Math::NEO_REAL_PRECISION) {
			for (size_t i = 0; i < polygon.size(); i++) {
				const T& v = polygon[i];
				const T& vPrev = (i == 0) ? polygon.back() : polygon[i - 1];
				const T& vNext = polygon[(i + 1) % polygon.size()];

				NeoReal a = (vPrev - v).angle(vNext - v);
				if (Math::equal(a, angle, tolerance)) continue;

				result.push_back(v);
			}
		}*/

#pragma endregion

#pragma region DOUGLAS PEUCKER

		static void simplifyDouglasPeucker(const Polygon3& polygon, NeoReal tolerance, Polygon3& result) {
			NeoRealVector p;
			toPSimpl(polygon, p);

			NeoRealVector r;
			psimpl::simplify_douglas_peucker<3>(p.begin(), p.end(), tolerance, std::back_inserter(r));

			toNeogen(r, result);

			/*checkEndpoints(result, tolerance);*/
		}

		static void simplifyDouglasPeucker(Polygon3& polygon, NeoReal tolerance) {
			simplifyDouglasPeucker(polygon, tolerance, polygon);
		}

#pragma endregion

#pragma region REUMANN WITKAM

		static void simplifyReumannWitkam(const Polygon3& polygon, NeoReal tolerance, Polygon3& result) {
			if (Math::lessOrEqual(tolerance, 0.0)) result = polygon;
			else {
				NeoRealVector p;
				toPSimpl(polygon, p);

				NeoRealVector r;
				psimpl::simplify_reumann_witkam<3>(p.begin(), p.end(), tolerance, std::back_inserter(r));
				
				toNeogen(r, result);

				/*checkEndpoints(result, tolerance);*/
			}
		}

		static void simplifyReumannWitkam(Polygon3& polygon, NeoReal tolerance) {
			simplifyReumannWitkam(polygon, tolerance, polygon);
		}

#pragma endregion

#pragma region RADIAL DISTANCE

		template <class T>
		static void simplifyRadialDistance(const T& polygon, T& result, NeoReal tolerance = Math::NEO_REAL_PRECISION) {
			NeoRealVector p;
			toPSimpl(polygon, p);

			NeoRealVector r;
			if (typeid(T) == typeid(Polygon2)) {
				psimpl::simplify_radial_distance<2>(p.begin(), p.end(), tolerance, std::back_inserter(r));
			}
			else {
				psimpl::simplify_radial_distance<3>(p.begin(), p.end(), tolerance, std::back_inserter(r));
			}
			toNeogen(r, result);
		}

		template <class T>
		static void simplifyRadialDistance(T& polygon, NeoReal tolerance = Math::NEO_REAL_PRECISION) {
			simplifyRadialDistance(polygon, polygon, tolerance);
		}

#pragma endregion

#pragma region PERPENDICULAR DISTANCE

		template <class T> 
		static void simplifyPerpendicularDistance(const T& polygon, T& result, NeoReal tolerance = Math::NEO_REAL_PRECISION) {
			NeoRealVector p;
			toPSimpl(polygon, p);

			NeoRealVector r;
			size_t s;
			do {
				s = p.size();
				r.clear();
				if (typeid(T) == typeid(Polygon2)) {
					psimpl::simplify_perpendicular_distance<2>(p.begin(), p.end(), tolerance, std::back_inserter(r));
				}
				else {
					psimpl::simplify_perpendicular_distance<3>(p.begin(), p.end(), tolerance, std::back_inserter(r));
				}
				p = r;
			} while (p.size() != s);

			toNeogen(r, result);
		}

		template <class T>
		static void simplifyPerpendicularDistance(T& polygon, NeoReal tolerance = Math::NEO_REAL_PRECISION) {
			simplifyPerpendicularDistance(polygon, polygon, tolerance);
		}
		
	};

#pragma endregion

};