#pragma once

#include <vector>
#include <map>
#include <set>
#include <iterator>
#include <utility>
#include <memory>
#include <deque>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/polygon/polygon.hpp>

namespace NEOGEN {

	typedef double NeoReal;
	typedef std::vector<NeoReal> NeoRealVector;

	class Vector2;
	struct Vector2Cmp;

	struct Vector2Pair;
	struct Vector2PairCmp;

	class Vector3;
	struct Vector3Cmp;
	
	
	typedef unsigned long ElementID;
	struct NeoMeshElementCmp;
	typedef std::vector<ElementID> ElementIDVector;
	typedef std::set<ElementID> ElementIDSet;

	typedef unsigned int uint;
	
	typedef std::vector<Vector2> Vector2List;
	typedef std::set<Vector2, Vector2Cmp> Vector2Set;
	typedef std::set<Vector2Pair, Vector2PairCmp> Vector2PairSet;

	typedef std::vector<Vector3> Vector3List;
	typedef std::set<Vector3, Vector3Cmp> Vector3Set;
	typedef std::pair<Vector3, Vector3> Vector3Pair;
		
	typedef std::vector<Vector2> Polygon2;
	typedef std::vector<Polygon2> Polygons2;

	typedef std::vector<Vector3> Polygon3;
	typedef std::vector<Polygon3> Polygons3;

	class NeoVertex;
	typedef std::shared_ptr<NeoVertex> NeoVertexSPtr;
	typedef std::unique_ptr<NeoVertex> NeoVertexUPtr;
	typedef std::weak_ptr<NeoVertex> NoeVertexWPtr;
	typedef std::vector<NeoVertex*> NeoVertexPVector;
	typedef std::map<ElementID, NeoVertexUPtr> NeoVertexMap;
	typedef std::set<NeoVertex*, NeoMeshElementCmp> NeoVertexPSet;
	typedef std::vector<NeoVertex*> Triangle;
	typedef std::vector<Triangle> Triangles;
	
	class NeoEdge;
	typedef std::shared_ptr<NeoEdge> NeoEdgeSPtr;
	typedef std::unique_ptr<NeoEdge> NeoEdgeUPtr;
	typedef std::weak_ptr<NeoEdge> NoeEdgeWPtr;
	typedef std::vector<NeoEdge*> NeoEdgePVector;
	typedef std::map<ElementID, NeoEdgeUPtr> NeoEdgeMap;
	typedef std::set<NeoEdge*, NeoMeshElementCmp> NeoEdgePSet;
	typedef std::pair<NeoEdge*, NeoEdge*> NeoEdgePair;

	class NeoFace;
	typedef std::shared_ptr<NeoFace> NeoFaceSPtr;
	typedef std::unique_ptr<NeoFace> NeoFaceUPtr;
	typedef std::weak_ptr<NeoFace> NoeFaceWPtr;
	typedef std::vector<NeoFace*> NeoFacePVector;
	typedef std::map<ElementID, NeoFaceSPtr> NeoFaceMap;
	typedef std::set<NeoFace*, NeoMeshElementCmp> NeoFacePSet;

	class NeoMesh;
	typedef std::shared_ptr<NeoMesh> NeoMeshSPtr;
	typedef std::unique_ptr<NeoMesh> NeoMeshUPtr;
	typedef std::weak_ptr<NeoMesh> NeoMeshWPtr;

	class NeoTerrain;
	typedef std::shared_ptr<NeoTerrain> NeoTerrainSPtr;
	typedef std::unique_ptr<NeoTerrain> NeoTerrainUPtr;
	typedef std::weak_ptr<NeoTerrain> NeoTerrainWPtr;

	class NeoNavMesh;
	typedef std::shared_ptr<NeoNavMesh> NeoNavMeshSPtr;
	typedef std::unique_ptr<NeoNavMesh> NeoNavMeshUPtr;
	typedef std::weak_ptr<NeoNavMesh> NeoNavMeshWPtr;

	class NeoPortal;
	typedef std::shared_ptr<NeoPortal> NeoPortalSPtr;
	typedef std::unique_ptr<NeoPortal> NeoPortalUPtr;
	typedef std::weak_ptr<NeoPortal> NoePortalWPtr;
	typedef std::vector<NeoPortal*> NeoPortalPVector;
	typedef std::map<ElementID, NeoPortalSPtr> NeoPortalMap;
	typedef std::set<NeoPortal*, NeoMeshElementCmp> NeoPortalPSet;
	typedef std::deque<NeoEdge*> NeoSubPortals;
	typedef std::pair<NeoPortal*, NeoPortal*> NeoPortalPair;

	class NeoCell;
	typedef std::shared_ptr<NeoCell> NeoCellSPtr;
	typedef std::unique_ptr<NeoCell> NeoCellUPtr;
	typedef std::weak_ptr<NeoCell> NeoCellWPtr;
	typedef std::map<ElementID, NeoCellSPtr> NeoCellMap;
	typedef std::vector<NeoCell*> NeoCellPVector;
	typedef std::set<NeoCell*, NeoMeshElementCmp> NeoCellPSet;
		
	class NeoNavMeshGenerator;
	typedef std::shared_ptr<NeoNavMeshGenerator> NeoNavMeshGeneratorSPtr;
	typedef std::unique_ptr<NeoNavMeshGenerator> NeoNavMeshGeneratorUPtr;
	typedef std::weak_ptr<NeoNavMeshGenerator> NeoNavMeshGeneratorWPtr;

	template <class T, class U> class NeoOctree;
	typedef NeoOctree<NeoFace*, NeoMeshElementCmp> NeoOctreeFace;
	typedef std::shared_ptr<NeoOctreeFace> NeoOctreeFaceSPtr;
	typedef std::unique_ptr<NeoOctreeFace> NeoOctreeFaceUPtr;
	typedef std::weak_ptr<NeoOctreeFace> NeoOctreeFaceWPtr;

	template <class T, class U> class NeoOctreeNode;
	typedef NeoOctreeNode<NeoFace*, NeoMeshElementCmp> NeoOctreeNodeFace;
	typedef std::shared_ptr<NeoOctreeNodeFace> NeoOctreeNodeFaceSPtr;
	typedef std::unique_ptr<NeoOctreeNodeFace> NeoOctreeNodeFaceUPtr;
	typedef std::weak_ptr<NeoOctreeNodeFace> NeoOctreeNodeFaceWPtr;

	class NeoAABB;
	typedef std::vector<NeoAABB> NeoAABBVector;

	typedef boost::geometry::model::ring<Vector2> BoostRing2;
	typedef boost::geometry::model::polygon<Vector2> BoostPolygon2;
	typedef std::vector<BoostPolygon2> BoostPolygons2;
	typedef boost::geometry::model::segment<Vector2> BoostSegment2;

	typedef boost::geometry::model::ring<Vector3> BoostRing3;
	typedef boost::geometry::model::polygon<Vector3> BoostPolygon3;
	typedef std::vector<BoostPolygon3> BoostPolygons3;
	typedef boost::geometry::model::segment<Vector3> BoostSegment3;

};
