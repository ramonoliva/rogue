#pragma once

#include "NeoTypes.h"

#include <algorithm>
#include <cmath>
#include <limits>

namespace NEOGEN {

	namespace Math {

		extern const NeoReal NEO_REAL_PRECISION;
		extern const NeoReal NEO_REAL_INFINITY;
		extern const NeoReal NEO_PI;
		extern const NeoReal NEO_VERTEX_MERGE_DISTANCE;

		enum ProjectionPlane {
			XY,	//The Z is 0
			XZ,	//The Y is 0 
			YZ,	//The X is 0
		};

		static bool equal(NeoReal f1, NeoReal f2, NeoReal epsilon = NEO_REAL_PRECISION) {
			return (std::abs(f1 - f2) < epsilon);  
		}

		static bool less(NeoReal f1, NeoReal f2, NeoReal epsilon = NEO_REAL_PRECISION) {
			if (equal(f1, f2, epsilon)) return false;
			return (f1 < f2);
		}

		static bool lessOrEqual(NeoReal f1, NeoReal f2, NeoReal epsilon = NEO_REAL_PRECISION) {
			return less(f1, f2, epsilon) || equal(f1, f2, epsilon);
		}

		static bool greater(NeoReal f1, NeoReal f2, NeoReal epsilon = NEO_REAL_PRECISION) {
			if (equal(f1, f2, epsilon)) return false;
			return (f1 > f2);
		}

		static bool greaterOrEqual(NeoReal f1, NeoReal f2, NeoReal epsilon = NEO_REAL_PRECISION) {
			return greater(f1, f2, epsilon) || equal(f1, f2, epsilon);
		}

		static NeoReal toRadians(NeoReal angle) {
			return NeoReal(angle * NEO_PI / 180.0);
		}

		static NeoReal toDegrees(NeoReal angle) {
			return NeoReal(angle * 180.0 / NEO_PI);
		}

		static NeoReal clamp(NeoReal value, NeoReal min, NeoReal max) {
			return std::min(max, std::max(min, value));
		}

		static NeoReal clamp01(NeoReal value) {
			return clamp(value, NeoReal(0), NeoReal(1));
		}

		static int sign(NeoReal value) {
			if (greaterOrEqual(value, 0)) return 1;
			return -1;
		}

		static NeoReal cos(NeoReal rad) {
			return std::cos(rad);
		}

		static NeoReal acos(NeoReal cos) {
			return std::acos(clamp(cos, NeoReal(-1), NeoReal(1)));
		}

		static NeoReal sin(NeoReal rad) {
			return std::sin(rad);
		}

		static NeoReal log2(NeoReal n) {  
			// log(n)/log(2) is log2.  
			return std::log(n) / std::log(NeoReal(2));  
		}

		static unsigned long long nextPowerOf2(NeoReal n) {
			unsigned long long pos = unsigned long long(std::ceil(log2(n)));
			return (unsigned long long(1) << pos);
		}

	};

};