#ifndef __ngNavMesh_h_
#define __ngNavMesh_h_

#include <vector>
#include "ngPoly.h"
#include "ngDataStructures.h"
#include <OgreSceneManager.h>
#include <OgreEntity.h>
#include <OgreRoot.h>
#include <OgreMaterialManager.h>
#include "ngGeometrySolver.h"
#include "OgreUtils.h"
#include <unordered_map>


namespace NEOGEN {

	class NavMesh {

		public:

			NavMesh();
			NavMesh(VertTable& vTable, EdgeTable& eTable, CellTable& cTable);
			~NavMesh();

			void createGraphics();

			int addVertex(Vec3D& v, bool isShared = false, int id = -1);
			void eraseSharedVertex(int idVertex);
			int addCell(std::vector<Vertex>& lVert, std::vector<int>& edges); 
			void addAdjacency(int c1, int c2);
			void clearCellsToRepair();

			int addSubEdge(int idP, int idV1, int idV2, NEOGEN::Edge::Type eType = NEOGEN::Edge::OBSTACLE);
			void addSharedEdge(int idEdge);
			void redoConcEdge(int idVert, int oldEdge, int newEdge);
			std::pair<int, int> subdivideEdge(int oldEdge, int idNewVert);
			void addEdgePointer(int idVert, int idEdge);
			bool computeConcEdges(int idVert);
			void eraseEdge(int idEdge);
			void eraseVertex(int idVert);
			void eraseCell(int idCell);
			bool isNonEssentialPortal(int idPortal);
			bool isNonEssentialPortal(int idPortal, int idVert);

			bool checkCellClearance(int cellID, Vec3D& position, float clearance);
		
			void clearManualEdges();

			size_t getNumVertices();
			size_t getNumEdges();
			size_t getNumCells();
			Vertex& getVertex(int idVerted);

			VertTable& getVertTable();
			EdgeTable& getEdgeTable();
			Edge& getEdge(int idEdge);
			Edge& getEdge(int idV1, int idV2);
			int getEdgeID(Ogre::ColourValue& color);
			std::set<int>& getSharedVertices();
			void setSharedVertices(std::set<int>& shV);
			std::set<int>& getSharedEdges();
			void setSharedEdges(std::set<int>& shE);
			std::set<int>& getCellsToRepair();

			CellTable& getCellTable();
			Cell& getCell(int idCell);
			Cell& getCellByPortal(int idPortal);
			int getPortalTo(int cA, int cB);
			int getRandomCell();

			void setEdgeCellPointer(int edgeID, int cellID);
			void replaceVertPointer(int idEdge, int idOldVert, int idNewVert);

			int existEdge(int idV1, int idV2);

			float angleBetweenEdges(int origin, int idE1, int idE2);
		
			void merge(NavMesh* nmSource);
			void fusionSharedVertices(std::set<int>& sharedVerticesDest, std::set<int>& sharedEdgesDest, std::set<int>& sharedVerticesSource, std::set<int>& sharedEdgesSource, float maxD);
			void sewMesh(std::set<int>& sharedVerticesDest, std::set<int>& sharedEdgesDest, std::set<int>& sharedVerticesSource, std::set<int>& sharedEdgesSource, float maxD);
			void fusionEdges(int idEdgeDestination, int idEdgeSource);
			void mergeVertTable(VertTable& vTableSource);
			void mergeEdgeTable(EdgeTable& eTableSource);
			void mergeCellTable(CellTable& cTableSource);

			void deleteTJoints(std::set<int>& sharedEdgesDest, std::set<int>& sharedEdgesSource);
			void computeAdjacency(int idCell);

			void drawEdge(int idEdge, Ogre::ColourValue& color);
			void drawEdge(int idEdge, const Ogre::String& matName);
			void drawCell(int idCell);
			void drawCell(int idCell, const Ogre::String& matName);
			void hideCell(int idCell);

			void drawEdges();
			void drawGraph();

			void showGraph(bool v);
			void showNavMesh(bool v);

			void printData();
			void printVertTable();
			void printEdgeTable();
			void printCellTable();
			void printCell(int idCell);
			void printSharedVertices();
			void printSharedEdges();
			void printVertex(int idVert);
			void printEdge(int idEdge);

		private:

			enum EdgeColorEnum { 
				EDGE_COLOUR_RED,
				EDGE_COLOUR_GREEN,
				EDGE_COLOUR_BLUE,
				EDGE_COLOUR_MAGENTA,
				EDGE_COLOUR_YELLOW,
				EDGE_COLOUR_CYAN,
				EDGE_COLOUR_GREY,
				NUM_EDGE_COLOUR_TYPES
			};
			typedef std::map<Ogre::RGBA, int> ColourTable;
			ColourTable colourTable;	//Taula que relaciona un color amb una aresta
			int currentColourType;
			int rGreen, rBlue;
			int gRed, gBlue;
			int bRed, bGreen;
			int mGreen;
			int yBlue;
			int cRed;
			int wRed, wGreen, wBlue;

			typedef std::map<int, int> VertEquivalence;

			VertTable vertTable;					//Taula de v�rtexs
			EdgeTable edgeTable;					//Taula d'arestes
			CellTable cellTable;					//Taula de cel�les
			PortalTable _portalTable;		
			VertsToEdgeTable vertsToEdgeTable;

			EdgeList _clearanceEdges;	//Contains all the clearance edges
			EdgeList _portalEdges;		//Contains all the portal edges
			EdgeList _obstacleEdges;	//Contains all the obstacle edges
		
			std::set<int> tJoints;					//Taula de portals entre layers
			std::set<int> cellsToRepair;			//Cel�les que s'han de reparar degut a qu� poden contenir concavitats

			std::set<int> sharedVertices;			//Taula que indica els v�rtexs que potencialment estan compartits amb una layer ve�na
			std::set<int> sharedEdges;				//Taula que indica les arestes que potencialment estan compartides amb una layer ve�na
		
			Ogre::SceneManager* mSceneMgr;

			Ogre::ManualObject* manualGraph;
			Ogre::ManualObject* manualGeometryEdges;
			Ogre::ManualObject* manualSharedEdges;
			Ogre::ManualObject* manualPortals;
			Ogre::SceneNode* _nodeEdges;
			Ogre::SceneNode* _nodeGraph;

			void initPortalTable();

			bool referenceEdge(int idVert, int idEdge);
			void fusionVertices(int idVertDest, std::set<int>& sharedEdgesDest, int idVertSource, std::set<int>& sharedEdgesSource);
			int findClosestSharedEdge(int idVert, float maxD, std::set<int>& sharedEdgesSource);
			int findOppositeEdge(int idEdge, std::deque<int>& edgeList);
			bool isSharedVertex(int idVertex);
			bool isWeakVertex(int idVertex);
			void eraseEdgePointer(int idEdge);
			void eraseEdgePointerVert(int idVert, int idEdge);
			void eraseEdgePointerCell(int idCell, int idEdge);
			int mergeEquivalentCells(std::set<int>& equivalentEdges);
			void mergeCells(int cellDestID, int cellSourceID);

			Vec3D computeCenterCell(int cellID);

			void createEdgeColour(int idEdge);
			void createEdgeColourRed(Ogre::ColourValue& edgeColour);
			void createEdgeColourGreen(Ogre::ColourValue& edgeColour);
			void createEdgeColourBlue(Ogre::ColourValue& edgeColour);
			void createEdgeColourMagenta(Ogre::ColourValue& edgeColour);
			void createEdgeColourYellow(Ogre::ColourValue& edgeColour);
			void createEdgeColourCyan(Ogre::ColourValue& edgeColour);
			void createEdgeColourGrey(Ogre::ColourValue& edgeColour);
		};
};

#endif 