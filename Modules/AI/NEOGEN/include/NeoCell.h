#pragma once

#include "NeoTypes.h"
#include "NeoMeshElement.h"

namespace NEOGEN {

	class NeoCell : public NeoMeshElement {

		public:

			NeoCell(ElementID id);

			virtual void begin();
			virtual void addFace(NeoFace* face);
			virtual void removeFace(NeoFace* face);
			virtual void end();
			
			virtual bool hasFace(NeoFace* face);
			virtual NeoFacePSet& getFaces();

			virtual void getContour(Polygon3& result);
			virtual void getContour2D(Polygon2& result);
			virtual NeoEdge* getNextEdge(NeoEdge* edge);

			virtual void getNeighbors(NeoCellPSet& result);

			virtual void triangulate();
			virtual const Triangles& getTriangles();

		protected:

			NeoFacePSet _faces;
			NeoPortalPSet _portals;
			NeoVertexPVector _vertices;	//The contour vertices

			Triangles _triangles;	

			virtual void computeContour();

	};

};