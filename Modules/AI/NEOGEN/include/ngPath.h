#ifndef __ngPath_h_
#define __ngPath_h_

#include <deque>
#include <map>

namespace NEOGEN {

	class Path {

		public:

			void clear();

			void addNode(int n);
			
			int getPrevNode(int n);
			int getNextNode(int n);
			int getNodePosition(int n);
			
			size_t getNumNodes();

			bool isGoalNode(int n);
			bool empty();
			bool hasNode(int n);
			
		private:

			typedef std::deque<int> NodeList;			//Contains the nodes of the path in the right order
			typedef std::map<int, int> NodeOrderList;	//Given a node, retun its position in the NodeList
			
			NodeList _nodeList;
			NodeOrderList _nodeOrderList;
	};

};

#endif