#include <vector>
#include "NeoTypes.h"

#pragma once

namespace NEOGEN {

	enum PolygonOrientation {
		UNDEFINED,
		COUNTER_CLOCKWISE,
		CLOCKWISE,
	};

	typedef std::vector<Vector3> Points;

	class Polygon {

		public:

			Polygon();

			virtual void addPoint(const Vector3& p);
			virtual const Points& getPoints();
			virtual PolygonOrientation getOrientation(const Vector3& up = Vector3(0,1,0));
			
		protected:

			Points _points;	//The list of all the points of the polygons

	};

};