#ifndef __ObjMeshPtr_H__
#define __ObjMeshPtr_H__

#include "ObjMesh.h"

class ObjMeshPtr : public Ogre::SharedPtr<ObjMesh> {
	
	public:
		
		ObjMeshPtr(); 
		explicit ObjMeshPtr(ObjMesh *rep);
		ObjMeshPtr(const ObjMeshPtr &r); 
		
		ObjMeshPtr(const Ogre::ResourcePtr &r);
 
		/// Operator used to convert a ResourcePtr to a GameMapPtr
		ObjMeshPtr& operator=(const Ogre::ResourcePtr& r); 

	private:

		void lockAndCopyMutexPointer(const Ogre::ResourcePtr &r);

};

#endif