#ifndef __ngMapParser_h_
#define __ngMapParser_h_

#include <iostream>
#include <fstream>
#include <string>

#include <vector>

#include <OgreImage.h>
#include <OgreResourceGroupManager.h>

#include "ngPoly.h"
#include "ngVector.h"
#include "ngDataStructures.h"
#include "ngNavMesh.h"
#include "ngVoxelGrid.h"

#include "pugixml.hpp"
#include "pugiconfig.hpp"

#define PATTERN_ALL_WALL	0
#define PATTERN_ALL_FLOOR	1

#define PATTERN_VERT0		2
#define PATTERN_VERT1		3
#define PATTERN_VERT2		4
#define PATTERN_VERT3		5

#define PATTERN_EDGE0		6
#define PATTERN_EDGE1		7
#define PATTERN_EDGE2		8
#define PATTERN_EDGE3		9

#define WALL_R				1.0
#define WALL_G				1.0
#define WALL_B				1.0

#define FLOOR_R				0.0
#define FLOOR_G				0.0
#define FLOOR_B				0.0

namespace NEOGEN {
	class MapParser {

		public:

			typedef std::pair<int, Vec3D> VertexDescription;			
			typedef std::vector<VertexDescription> Obstacle;
			typedef std::vector<Obstacle> ObstacleDescription;
			
			typedef std::pair<int, int> PairInt;
			typedef std::vector<PairInt> PortalDescription;
		
			static MapParser& getSingleton() {
				static MapParser mParser;
				return mParser;
			};
			MapParser();
			MapParser(MapParser const&);        // Don't Implement.
			void operator=(MapParser const&);	// Don't implement
			~MapParser();

			void loadPolyMapData(std::vector<Poly*>* data, const std::string& fileName);
			void savePolyMapData(std::vector<Poly*>* data, const std::string& fileName);
			void savePolyMapData(Poly* p, std::ofstream& file);

			void saveObstacleDescription(ObstacleDescription& obstacles, PortalDescription& portals, const std::string& fileName);
			void loadObstacleDescription(ObstacleDescription& resultObstacles, PortalDescription& resultPortals, const std::string& fileName);
			
			void saveNavMesh(NavMesh* navMesh, const std::string& fileName);
			void loadNavMesh(NavMesh* navMesh, const std::string& fileName);

		private:

			int pattern(Ogre::Image* img, int i, int j);
			//int patternWindow(vector<Ogre::ColourValue>* colorWindow);

			bool isVertPattern(int pat);
			bool isWallColor(Ogre::ColourValue* c);
			Poly* createPolygon(Ogre::Image* img, std::vector<std::vector<bool>>& visited, int i, int j);

			void saveVoxelGrid(VoxelGrid& vGrid, pugi::xml_node& voxelGridNode);
			void saveVertTable(VertTable& vertTable, pugi::xml_node& vertTableNode);
			void saveEdgeTable(EdgeTable& edgeTable, pugi::xml_node& edgeTableNode);
			void saveCellTable(CellTable& cellTable, pugi::xml_node& cellTableNode);

			void loadVoxelGrid(VoxelGrid& vGrid, pugi::xml_node& voxelGridNode);
			void loadVertTable(VertTable& vertTable, pugi::xml_node& vertTableNode);
			void loadEdgeTable(VertTable& vertTable, EdgeTable& edgeTable, pugi::xml_node& edgeTableNode);
			void loadCellTable(VertTable& vertTable, EdgeTable& edgeTable, CellTable& cellTable, pugi::xml_node& cellTableNode);

			void loadObstacleDescription_1_0(pugi::xml_node& rootNode, ObstacleDescription& resultObstacles);
			void loadObstacleDescription_1_1(pugi::xml_node& rootNode, ObstacleDescription& resultObstacles);
			void loadUserPortals(pugi::xml_node& userPortalsNode, PortalDescription& resultPortals);
	};
};

#endif