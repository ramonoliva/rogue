#ifndef __exClearanceSolver_h_
#define __exClearanceSolver_h_

#include "ngNavMesh.h"
#include <map>
#include <unordered_map>
#include "ngPath.h"
#include <OgreTimer.h>

namespace Exact {

	typedef std::pair<int, int> CrossKey;			//A cross is defined by a pair of portal identificators (an entry portal and an exit portal)
	typedef std::map<CrossKey, float> CrossTable;	//A table of crosses

	typedef std::pair<int, float> ShrinkedPortalKey;
	typedef std::pair<NEOGEN::Vec3D, NEOGEN::Vec3D> ShrinkedPortal;
	typedef std::map<ShrinkedPortalKey, ShrinkedPortal> ShrinkedPortalMap;

	typedef std::unordered_map<int, float> CriticalRadiiMap;

	class ClearanceSolver {

		public:

			enum ShrinkPortalMode {
				SP_SIMPLE,
				SP_EXACT,
			};

			static ClearanceSolver& getSingleton() {
				static ClearanceSolver instance;
				return instance;
			}

			struct Statistics {

				public:

					//Elapsed time in milliseconds
					unsigned long _timeComputeClearanceCell;	//The time inverted in computing the clearance of the cells
					unsigned long _timeCriticalRadii;			//The time inverted in computing the critical radii
					unsigned long _timeShrinkPortal;			//The time inverted in shrinking the portal

					Statistics() {
						reset();
					}

					void reset() {
						_timeComputeClearanceCell = 0;
						_timeCriticalRadii = 0;
						_timeShrinkPortal = 0;
					}

			};

			void resetStatistics();
			Statistics& getStatistics();
			
			void setNavMesh(NEOGEN::NavMesh* nm);
			void setAcceleration(bool useTables, bool useCriticalRadius);
			void setShrinkPortalMode(ShrinkPortalMode mode);
			ShrinkPortalMode getShrinkPortalMode();
			void useRecursivity(bool rec);
			bool isUsingRecursivity();

			void addCross(CrossKey& key, float cValue2 = 0.0f);
			float getCross(CrossKey& key);

			void computeClearance();

			void computeCriticalRadii();
			void setCriticalRadius(int vertexID, float r);

			void computeClearanceCells();
			void computeClearanceCell(int idCell);
			void computeVertStrings(int entryPortalID, int exitPortalID, std::vector<int>& leftString, std::vector<int>& rightString);
			float computeClearancePortal(int idPortal);

			//bool shrinkPortal(int entryPortalID, int exitPortalID, float clearance, NEOGEN::Vec3D& v1Result, NEOGEN::Vec3D& v2Result);
			bool shrinkPortal(int entryPortalID, NEOGEN::Path& path, float clearance, NEOGEN::Vec3D& v1Result, NEOGEN::Vec3D& v2Result);
			
			void addShrinkedPortal(ShrinkedPortalMap& sMap, int portalID, float clearance, NEOGEN::Vec3D& shrinkedPortalV1, NEOGEN::Vec3D& shrinkedPortalV2);
			bool getShrinkedPortal(ShrinkedPortalMap& sMap, int portalID, float clearance, NEOGEN::Vec3D& shrinkedPortalV1, NEOGEN::Vec3D& shrinkedPortalV2);
			void clearShrinkedTables();
		
		private:

			ShrinkPortalMode _shrinkPortalMode;			//Indicates how to shrink the portal
			NEOGEN::NavMesh* _navMesh;					//Current NavMesh for which we are computing clearance
			CrossTable _crossTable;	
			ShrinkedPortalMap _shrinkedPortalMap;		//Mapa que guarda per cada portal i valor de clearance, el shrunk portal que s'ha calculat
			ShrinkedPortalMap _shrinkedPortalMapSimple;
			CriticalRadiiMap _criticalRadiiMap;			//Stores the critical radius for each portal endpoint

			Ogre::Timer _timer;
			Statistics _statistics;

			bool _useRecursivity;
			bool _useTables;
			bool _useCriticalRadius;

			/*** SINGLETON PATTERN ***/
			ClearanceSolver();
			ClearanceSolver(ClearanceSolver const&);    // Don't Implement.
			void operator=(ClearanceSolver const&);		// Don't implement
			/***/

			void computeCriticalRadius(int portalID);

			void correctCrossKey(CrossKey& key);

			float computeClearanceCrossCell(NEOGEN::Cell& c, CrossKey& portalPair);
			float computeClearanceStringString(std::vector<int>& vertString1, std::vector<int>& vertString2);
			float computeClearanceVertexString(int idVertex, std::vector<int>& vertString);

			enum ShrinkEdgeMode {
				EM_BOTH,	//Take into account both edge type with respect to the portal when shrinking by cell
				EM_IN,		//Take into account only IN edges with respect to the portal
				EM_OUT,		//Take into account only OUT edges with respect to the portal
			};

			bool shrinkPortalSimple(int portalID, float clearance, NEOGEN::Vec3D& v1Result, NEOGEN::Vec3D& v2Result);
			bool shrinkPortalExACT(int entryPortalID, NEOGEN::Path& path, float clearance, NEOGEN::Vec3D& v1Result, NEOGEN::Vec3D& v2Result);
			
			int shrinkPortalByExitPortalEndpoint(int shrinkPortalID, NEOGEN::Vec2D& vToShrink, NEOGEN::Vec2D& vFixed, bool inEndpoint, int startingCellID, NEOGEN::Path& path, float clearance);
			void shrinkPortalByCellRec(int shrinkPortalID, NEOGEN::Vec2D& v1Result, NEOGEN::Vec2D& v2Result, float clearance, int entryPortalID);
			bool shrinkPortalByPortalEndpoint(NEOGEN::Vec2D& vToShrink, NEOGEN::Vec2D& vFixed, NEOGEN::Vec2D& vTest, float clearance);

			bool edgeModeMatch(NEOGEN::Edge& shrinkPortal, NEOGEN::Edge& e, ShrinkEdgeMode shrinkEdgeMode);
	};

};

#endif