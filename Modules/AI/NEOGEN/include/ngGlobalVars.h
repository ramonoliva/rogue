#ifndef __ngGlobalVars_h_
#define __ngGlobalVars_h_

#include <OgreTimer.h>
#include <OgreRoot.h>
#include <OgreMaterial.h>
#include <OgreMaterialManager.h>
#include <OgreLogManager.h>
#include "ngRealMath.h"

extern RealMath::ProjectionPlaneEnum NEOGEN_PROJECTION_PLANE;

extern float vertScale;
extern float vertScaleInc;
extern int applicationState;
extern int applicationMode;		//Indica si estem renderitzant en mode 2D o 3D
extern int computationMode;
extern int generationMode;		//Indica el mode de generaci� de la malla (pas a pas o autom�tic)
extern float alphaThreshold;
extern Ogre::Timer* globalTimer;
extern unsigned long appTime;
extern unsigned long appTimeBefore;
extern unsigned long appTimeAfter;
extern unsigned long appTimeDiff;

extern bool DEBUG_MODE;

extern int newVertID;
extern int newEdgeID;
extern int newCellID;

#define MIN_VERT_SCALE	0.01f
#define MAX_VERT_SCALE	1.0f

enum APPLICATION_STATE {
	STATE_IDLE,
	STATE_VOXELIZED,
	STATE_WALKABLE_AREA_CONNECTED,
	STATE_LAYERS_GENERATED, 
	STATE_SELECT_LAYER,
	STATE_LAYER_SELECTED,
	STATE_LAYER_REFINED,
	STATE_GENERATE_NAVMESH_LAYER,
	STATE_NAVMESH_LAYER_GENERATED,
	STATE_NAVMESH_IDENTIFICATION, 
	STATE_MERGE_NAV_MESHES,
	STATE_DELETE_TJOINTS,
	STATE_REPAIR_CELLS,
	STATE_FINAL_NAVMESH_GENERATED
};

#define APPLICATION_MODE_3D	0
#define APPLICATION_MODE_2D	1

#define GENERATION_MODE_STEP_BY_STEP	0
#define GENERATION_MODE_FULL			1

#define COMPUTATION_MODE_CPU	0
#define COMPUTATION_MODE_GPU	1

#define CONVEX_ANGLE_STRICT		180.0f

#define CELL_OFFSET 0.0f

namespace NEOGEN {

	inline unsigned long gComputeTimeDiffMS() {
		appTimeAfter = globalTimer->getMilliseconds();
		appTimeDiff = appTimeAfter - appTimeBefore;
		appTime += appTimeDiff;
	
		return appTimeDiff;
	}

	inline void gPrintTimeDiffS(const std::string& title) {
		Ogre::LogManager::getSingletonPtr()->logMessage(title + std::to_string((float)appTimeDiff / 1000.0f));
	}

	inline void gPrintHeader(const std::string& title) {
		Ogre::LogManager::getSingletonPtr()->logMessage("*****************************************************");
		Ogre::LogManager::getSingletonPtr()->logMessage(title);
		Ogre::LogManager::getSingletonPtr()->logMessage("*****************************************************");
	}

}

#endif