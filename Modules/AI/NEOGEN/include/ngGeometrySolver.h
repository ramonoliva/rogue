#ifndef __ngGeometrySolver_h_
#define __ngGeometrySolver_h_

#include "ngVector.h"
#include "ngPoly.h"
#include "ngRealMath.h"
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/geometries.hpp>
#include <boost/geometry/geometries/point.hpp>

namespace GeometrySolver {

	//Cont� funcions �tils per resoldre problemes geom�trics

	class Ray {

		public:
		
			Ray(NEOGEN::Vec3D& o, NEOGEN::Vec3D& dir) {
				origin = o;
				direction = dir;
			}

			Ray() {
				origin = NEOGEN::Vec3D(0.0f, 0.0f, 0.0f);
				direction = NEOGEN::Vec3D(0.0f, 0.0f, 0.0f);
			}

			NEOGEN::Vec3D& getOrigin() {
				return origin;
			}

			void setOrigin(NEOGEN::Vec3D& o) {
				origin = o;
			}

			NEOGEN::Vec3D& getDirection() {
				return direction;
			}

			void setDirection(NEOGEN::Vec3D& dir) {
				direction = dir.normalized();
			}

		private:
		
			NEOGEN::Vec3D origin;
			NEOGEN::Vec3D direction;
	};

	class Ray2D {

		public:
		
			Ray2D(NEOGEN::Vec2D& o, NEOGEN::Vec2D& dir) {
				origin = o;
				setDirection(dir);
			}

			Ray2D() {
				origin = NEOGEN::Vec2D(0.0f, 0.0f);
				direction = NEOGEN::Vec2D(0.0f, 0.0f);
			}

			NEOGEN::Vec2D& getOrigin() {
				return origin;
			}

			void setOrigin(NEOGEN::Vec2D& o) {
				origin = o;
			}

			NEOGEN::Vec2D& getDirection() {
				return direction;
			}

			void setDirection(NEOGEN::Vec2D& dir) {
				direction = dir.normalized();
			}

		private:
		
			NEOGEN::Vec2D origin;
			NEOGEN::Vec2D direction;
	};

	struct IntersectionPoint2D {
		NEOGEN::Vec2D location;	//Posici� 2D on es produeix la intersecci�
		bool isValid;	//Indica si la posici� d'intersecci� �s v�lida
		float t;		//Indica en quin punt de la recta s'ha produ�t la intersecci�

		IntersectionPoint2D() {
			isValid = false;
			t = 0.0f;
		}
	};

	struct IntersectionPoint3D {
		NEOGEN::Vec3D location;	//Posici� 3D on es produeix la intersecci�
		bool isValid;	//Indica si la posici� d'intersecci� �s v�lida
		float t;		//Indica en quin punt de la recta s'ha produ�t la intersecci�

		IntersectionPoint3D() {
			isValid = false;
			t = 0.0f;
		}
	};

	struct SegmentIntersection2D {
		NEOGEN::Vec2D p1;
		bool p1Valid;
		NEOGEN::Vec2D p2;
		bool p2Valid;

		SegmentIntersection2D() {
			p1Valid = false;
			p2Valid = false;
		}
	};
	
	enum OrientationEnum {
		NO_TURN = 0,
		TURN_COUNTER_CWISE = 1, 
		TURN_CWISE = -1
	};

	enum SegmentIntersectionTypeEnum {
		INTERSECTION_NONE = -1, 
		INTERSECTION_SINGLE, 
		INTERSECTION_SEGMENT
	};

	enum PolyIntersectionEnum {
		POLY_OUTSIDE = -1, 
		POLY_INSIDE = 0, 
		POLY_PARTIALLY_INSIDE = 1
	};

	enum AngleTurnEnum {
		ANGLE_TURN_LEFT, 
		ANGLE_TURN_RIGHT
	};

	NEOGEN::Vec2D to2D(NEOGEN::Vec3D& v, RealMath::ProjectionPlaneEnum projectionPlane = RealMath::PROJECTION_XZ);
	void to2D(NEOGEN::Poly& p, std::vector<NEOGEN::Vec2D>& result, RealMath::ProjectionPlaneEnum ProjectionPlaneEnum = RealMath::PROJECTION_XZ);
	NEOGEN::Vec3D to3D(NEOGEN::Vec2D& v, RealMath::ProjectionPlaneEnum projectionPlane = RealMath::PROJECTION_XZ);

	void projectionPointLine3D(NEOGEN::Vec3D& v, NEOGEN::Vec3D& s1, NEOGEN::Vec3D& s2, IntersectionPoint3D& result);
	void projectionPointSegment3D(NEOGEN::Vec3D& v, NEOGEN::Vec3D& s1, NEOGEN::Vec3D& s2, IntersectionPoint3D& result);
	void projectionPointLine2D(NEOGEN::Vec2D& v, NEOGEN::Vec2D& s1, NEOGEN::Vec2D& s2, IntersectionPoint2D& result);
	void projectionPointSegment2D(NEOGEN::Vec2D& v, NEOGEN::Vec2D& s1, NEOGEN::Vec2D& s2, IntersectionPoint2D& result);

	void segmentSegmentIntersection2D(NEOGEN::Vec2D& s1, NEOGEN::Vec2D& s2, NEOGEN::Vec2D& s3, NEOGEN::Vec2D& s4, SegmentIntersection2D& result);
	void raySegmentIntersection2D(Ray2D& ray, NEOGEN::Vec2D& s3, NEOGEN::Vec2D& s4, IntersectionPoint2D& result);
	void lineLineIntersection2D(NEOGEN::Vec2D& s1, NEOGEN::Vec2D& s2, NEOGEN::Vec2D& s3, NEOGEN::Vec2D& s4, IntersectionPoint2D& result);
	void segmentCircleIntersection2D(NEOGEN::Vec2D& s1, NEOGEN::Vec2D& s2, NEOGEN::Vec2D& center, float radius, SegmentIntersection2D& result);
	bool existSegmentCircleIntersection2D(NEOGEN::Vec2D& s1, NEOGEN::Vec2D& s2, NEOGEN::Vec2D& center, float radius);
	bool existLineCircleIntersection2D(NEOGEN::Vec2D& s1, NEOGEN::Vec2D& s2, NEOGEN::Vec2D& center, float radius);

	float segmentSegmentSquaredDistance2D(NEOGEN::Vec2D& s1, NEOGEN::Vec2D& s2, NEOGEN::Vec2D& s3, NEOGEN::Vec2D& s4);
			
	float pointSegmentDistance(NEOGEN::Vec3D* p, NEOGEN::Vec3D* lBegin, NEOGEN::Vec3D* lEnd);
			
	bool isBetween(NEOGEN::Vec3D& v, NEOGEN::Vec3D& a, NEOGEN::Vec3D& b);
	bool isBetween2D(NEOGEN::Vec2D& v, NEOGEN::Vec2D& a, NEOGEN::Vec2D& b);

	int orientationTest(NEOGEN::Vec2D& p, NEOGEN::Vec2D& q, NEOGEN::Vec2D& r, bool normalize = true);
		
	float angleBetweenVectors(NEOGEN::Vec2D& v1, NEOGEN::Vec2D& v2, AngleTurnEnum aTurn = ANGLE_TURN_RIGHT);

	int polygonOrientation(NEOGEN::Poly& p, RealMath::ProjectionPlaneEnum projectionPlane = RealMath::PROJECTION_XZ);
	bool polygonIntersection(NEOGEN::Poly& p1, NEOGEN::Poly& p2, RealMath::ProjectionPlaneEnum projectionPlane = RealMath::PROJECTION_XZ);
	bool pointInPolygon(NEOGEN::Vec3D& v, NEOGEN::Poly& p, RealMath::ProjectionPlaneEnum projectionPlane = RealMath::PROJECTION_XZ);
	bool pointInPolygon(NEOGEN::Vec2D& v, std::vector<NEOGEN::Vec2D>& pProjected);
	bool polygonInPolygon(std::vector<NEOGEN::Vec2D>& pProjected1, std::vector<NEOGEN::Vec2D>& pProjected2);
	bool edgeIntersection(NEOGEN::Vec2D& v1, NEOGEN::Vec2D& v2, std::vector<NEOGEN::Vec2D>& pProjected);

	typedef boost::geometry::model::point<double, 3, boost::geometry::cs::cartesian> BoostPoint3D;
	typedef boost::geometry::model::segment<BoostPoint3D> BoostSegment;

	typedef boost::geometry::model::point<double, 2, boost::geometry::cs::cartesian> BoostPoint2D;
	typedef boost::geometry::model::segment<BoostPoint2D> BoostSegment2D;
};

#endif 