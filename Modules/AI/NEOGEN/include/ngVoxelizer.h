#ifndef __ngVoxelizer_h_
#define __ngVoxelizer_h_

#include <OgreTexture.h>
#include <OgreImage.h>
#include <OgreColourValue.h>
#include <OgreEntity.h>
#include <OgreSceneManager.h>
#include <OgreManualObject.h>
#include <OgreMaterialManager.h>
#include <OgreStaticGeometry.h>
#include <OgreSubMesh.h>

#include <algorithm>
#include <vector>

#include "ngSliceMap.h"
#include "ngVoxel.h"
#include "ngNavMesh.h"
#include "ngLayer.h"
#include "ngVoxelGrid.h"

#include <boost\geometry.hpp>
#include <boost\geometry\geometries\point_xy.hpp>
#include <boost\geometry\geometries\polygon.hpp>
#include <boost\geometry\geometries\point.hpp>

namespace NEOGEN {
	class Voxelizer {

	public:

		enum {
			VOXELIZATION_MODE_CPU_EXACT,
			VOXELIZATION_MODE_GPU_APPROXIMATE, 
			VOXELIZATION_MODE_GPU_EXACT
		} VoxelizationModeEnum;

		Voxelizer(Ogre::SceneManager* mgr);
		virtual ~Voxelizer();
		void clearVoxelizedModel();

		int getVoxelizationMode();
		float getCharHeight();
		Ogre::Vector3& getVoxelDim();
		size_t getNumVoxelsCharDepth();
		void createGrid(Ogre::Entity* ent, float charRadius, float maxStepDepth);
		size_t getVoxelGridSizeX();
		size_t getVoxelGridSizeY();
		size_t getVoxelGridSizeZ();
		float getVoxelDimX();
		float getVoxelDimY();
		float getVoxelDimZ();
		Layer* getActiveLayer();
		NavMesh* getSceneNavMesh();

		void drawVoxelizedModel(int voxelType = Voxel::TYPE_ANY);
		void drawVoxelizedModelTop();
		void drawVoxelizedModelFront();
		void drawVoxelizedModelSide();

		virtual void voxelization(ngTriangleList& tList, int voxelType) = 0;
		Ogre::ManualObject* getManualPerpendicularFacesIntersection();

		void setSliceMapPositive(std::vector<Ogre::TexturePtr>& topTexList, std::vector<Ogre::TexturePtr>& frontTexList, std::vector<Ogre::TexturePtr>& sideTexList);
		void setSliceMapNegative(std::vector<Ogre::TexturePtr>& topTexList, std::vector<Ogre::TexturePtr>& frontTexList, std::vector<Ogre::TexturePtr>& sideTexList);
		void fusion();

		bool selectWalkableArea(const Ogre::Ray& ray);

		void detectConnectedWalkableLayers();

		void layerSubdivision();
		void detectAccessibleVoxels();

		void showVoxelizedModel(bool b);
		void showCuttingShape(bool b);
		void showNavMeshLayers(bool b);
		void showNavMesh(int idLayer, bool b);
		void drawActiveLayer(ngTriangleList& perpendicularFaces);
		bool existActiveLayer();
		void nextLayer();

		void setupCuttingMesh();

		void generateNavMeshScene();

		void deleteTJoints();

		bool allLayersFusioned();

		bool nextPairToFusion(std::pair<int, int>& layerPair);

		void navMeshVoxelDescription();
		bool projectedIntersection(Voxel* v, Poly& cProjected);
		void initShowCellDescription();
		void showCellDescription();

	protected:

		#define DEPTH_RESOLUTION 32

		/********************************************************/
		/* VOXEL GRID */
		/********************************************************/
		Voxel* voxelWalkable;					//V�xel que ha indicat l'usuari com a caminable
		/********************************************************/
		/* END VOXEL GRID */
		/********************************************************/
		/********************************************************/
		/* SCENE LAYERS */
		/********************************************************/
		CellTable::iterator cellDescriptorIt;
		CellTable::iterator oldCellDescriptorIt;
		typedef std::map<int, Layer*>							LayerTable;
		typedef std::map<std::pair<int, int>, std::set<int>>	ColumnConflictTable;
		//ColumnConflictTable: �s un mapa on cada entrada (pair<int, int>) representa una 
		//columna de la grid. El valor �s un set<int> que indica quines layers tenen algun v�xel
		//en la columna indicada per la key. 
		LayerTable sceneLayers;							//Guarda les diferents capes del model
		ColumnConflictTable columnLayers;				//Guarda per cada columna, les 
		//int currentLayerID;								//Indica l'identificador de capa actual que s'est� transmetent
		LayerTable::iterator activeLayerIt;				//Iterador que indica la layer actual
		std::set<std::pair<int, int>> fusionedPairs;	//Parelles de navmeshes que ja han estat fusionades
		std::set<int> fusionedScene;					//navMeshes que han estat fusionades amb la sceneNavMesh
		NavMesh* sceneNavMesh;
		std::vector<int> invalidLayers;
		/********************************************************/
		/* END SCENE LAYERS */
		/********************************************************/
		Ogre::SceneManager* mSceneMgr;
		Ogre::Entity* entModel;
		Ogre::TexturePtr voxelizationTex;
		Ogre::StaticGeometry* sgVoxelModelPositive;
		Ogre::StaticGeometry* sgVoxelModelNegative;
		Ogre::StaticGeometry* sgVoxelModelContour;
		Ogre::StaticGeometry* sgVoxelModelAccessible;
		Ogre::StaticGeometry* sgCellDescription;

		Ogre::ManualObject* manualPerpendicularFacesIntersection;

		SliceMap* sliceMapTopPositive;
		SliceMap* sliceMapFrontPositive;
		SliceMap* sliceMapSidePositive;

		SliceMap* sliceMapTopNegative;
		SliceMap* sliceMapFrontNegative;
		SliceMap* sliceMapSideNegative;

		Ogre::ManualObject* bbManual;		//Manual objec de la BB
		Ogre::ManualObject* manualCuttingPlaneAccessible;		//Cont� la part de la cutting shape que representa els v�xels accessibles
		Ogre::ManualObject* manualCuttingPlanePortal;			//Cont� la part de la cutting shape que representa els portals entre layers

		size_t numVoxelsCharDepth;
		int charDepthOffset;
		float charHeight;	//L'al�ada del personatge

		int numAccessibleVoxels;
		int voxelizationMode;

		/********************************************************/
		/* VARIABLES NECESSARIES PER LA FUSI� DE NAVMESHES */
		/********************************************************/
		NavMesh* nmDest;
		NavMesh* nmSource;
		std::set<int> sharedVerticesDest; 
		std::set<int> sharedEdgesDest; 
		std::set<int> sharedVerticesSource; 
		std::set<int> sharedEdgesSource; 

		void initGridData(SliceMap* sMapPositive, SliceMap* sMapNegative);
		void createLayer(int layerID);
		void propagateLayerID(Voxel* currentVoxel, std::vector<Voxel*>& accessibleNeighbors, std::vector<Voxel*>& obstacleNeighbors);
		void propagateLayerIDAccessible(Voxel* currentVoxel, Voxel* neighborVoxel);
		void propagateLayerIDObstacle(Voxel* currentVoxel, Voxel* neighborVoxel);
	
		void printVoxelPointerTable(VoxelPointerTable& vTable, const Ogre::String& tableName);
		void printSceneLayers();
		void printLayer(int layerID);

		void setVoxelLayer(Voxel* v, int layerID);
		void addVoxelPortal(Voxel* v);

		void fusion(SliceMap* sMapTop, SliceMap* sMapFront, SliceMap* sMapSide);
		void expandPositiveVoxel(int x, int y, int z);
		
		bool setVoxelSeed(const Ogre::Ray& ray, Voxel* initialVoxel);

		bool mergeLayers(int layerID1, int layerID2);
		void addColumnLayer(std::pair<int, int>& column, int layerID);
		void eraseColumnLayer(std::pair<int, int>& column, int layerID);
		bool layerInColumn(std::pair<int, int>& column, int layerID);
		bool voxelInTable(VoxelPointer& vPointer, VoxelPointerTable& vTable);
	
		void detectRealWalkableLayers(int layerID, std::set<int>& walkableLayers);
	
		bool isTopVoxelAccessible(int x, int y, int z);
		bool isNeighborAccessible(int neighborX, int currentY, int neighborZ);
		bool isNeighborObstacle(int neighborX, int currentY, int neighborZ);
		bool isNeighborEmpty(int neighborX, int currentY, int neighborZ);

		void findSeedVoxelLayer(int layerID);
	
		void fusionNavMeshes(int layerID1, int layerID2);
		bool isFusionedPair(int layerID1, int layerID2);
	
		bool checkTriangleBoxIntersection(Ogre::Vector3& v1, Ogre::Vector3& v2, Ogre::Vector3& v3, Voxel* v);
		void voxelPerpendicularFacesIntersection(int x, int y, int z, ngTriangleList& perpendicularFaces);
		void buildVoxelizedModel();
		void drawSliceMap(int voxelType, SliceMap* sMap);
		void drawSceneLayers();
		void drawLayer(Layer* layer, int layerID);
		void drawPerpendicularFace(ngTriangle& t, Ogre::ManualObject* manual);
		void drawCuttingPlane(int x, int y, int z, Ogre::ManualObject* manual);
		void computeCuttingPlane(Voxel* v, ngTriangleList& perpendicularFaces);
	};
};

#endif