#pragma once

#include <vector>  
#include <NeoTypes.h>

namespace NEOGEN {
		
	class NeoTriangulator {
		
		public:

			typedef std::vector<size_t> IndexList;

			// Triangulator a contour/polygon, places results in STL vector
			// as series of triangles.
			static bool Process(const Polygon2& contour, IndexList &result);

			// compute area of a contour/polygon
			static NeoReal Area(const Polygon2& contour);

			// decide if point Px/Py is inside triangle defined by
			// (Ax,Ay) (Bx,By) (Cx,Cy)
			static bool InsideTriangle(
										NeoReal Ax, NeoReal Ay,
										NeoReal Bx, NeoReal By,
										NeoReal Cx, NeoReal Cy,
										NeoReal Px, NeoReal Py
									);

		private:
		
			static bool Snip(const Polygon2& contour, int u, int v, int w, int n, int *V);

	};

};