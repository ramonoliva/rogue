#pragma once

#include "NeoTypes.h"
#include "NeoVector3.h"
#include "NeoEdge.h"
#include "NeoNavMesh.h"
#include "NeoGeometrySolver.h"

namespace NEOGEN {

	struct NeoNotch {

	public:

		NeoVertex* _vertex;
		Vector3 _dirIn; 
		Vector3 _dirOut;

		NeoNotch() {
			_vertex = NULL;
		}

		NeoNotch(NeoVertex* v, NeoEdge* edgeIn, NeoEdge* edgeOut) {
			init(v, edgeIn->getDirection(), edgeOut->getDirectionInverse());
		}

		NeoNotch(NeoVertex* v, const Vector3& dirIn, const Vector3& dirOut) {
			init(v, dirIn, dirOut);
		}

		bool isInsideAreaOfInterest(const Vector3& p) {
			Vector2 v = (p - _vertex->getPosition()).to2D();

			return	Math::greaterOrEqual(v.dot(_hintDir), NeoReal(0.0)) &&
					Math::greaterOrEqual(v.dot(_nIn), NeoReal(0.0)) &&
					Math::greaterOrEqual(v.dot(_nOut), NeoReal(0.0));
		}
						
	protected:

		Vector2 _hintDir;	//The direction that determines the in of the Area of Interest
		Vector2 _nIn;		//The direction of the normal of the plane supported by the input parameter dirIn
		Vector2 _nOut;		//The direction of the normal of the plane supported by the input parameter dirOut

		void init(NeoVertex* v, const Vector3& dirIn, const Vector3& dirOut) {
			_vertex = v;
			_dirIn = dirIn.normalized();
			_dirOut = dirOut.normalized();

			//1) Compute the _hintDir
			Vector2 dIn = _dirIn.to2D().normalized();
			Vector2 dOut = _dirOut.to2D().normalized();
			_hintDir = (dIn + dOut).normalized();

			//2) Compute _nIn and _nOut
			if (_hintDir.equal(dIn)) {
				_nIn = Vector2(-_hintDir.getY(), _hintDir.getX());
				_nOut = Vector2(_hintDir.getY(), -_hintDir.getX());
			}
			else {
				_nIn = Vector2(-dIn.getY(), dIn.getX()).normalized();
				if (Math::less(_nIn.dot(_hintDir), NeoReal(0.0))) _nIn *= NeoReal(-1.0);

				_nOut = Vector2(-dOut.getY(), dOut.getX()).normalized();
				if (Math::less(_nOut.dot(_hintDir), NeoReal(0.0))) _nOut *= NeoReal(-1.0);
			}
		}

	};

	struct NeoCandidate {

		NeoEdge* _edge;					//The candidate edge
		NeoEdgePSet _traversedEdges;	//All the traversed edges to arrive to the candidate edge. 
		Vector3 _point;					//The candidate point in the edge
		NeoReal _dist;					//The distance^2 the point is is from the notch. 

		NeoCandidate() {
			_edge = NULL;
			_point = Vector3(0,0,0);
			_dist = NeoReal(0);
		}

		bool operator<(const NeoCandidate& candidate) const {
			if (_edge && candidate._edge) {
				if (Math::equal(_dist, candidate._dist)) {
					//both candidates are at the same distance from the notch. 
					//In this case, the priority is for the obstacle edge (if any)
					return (_edge->isTypeObstacle());
				}
				return (Math::less(_dist, candidate._dist));
			}

			//At this point, one (or both) candidate edges are NULL
			return (_edge != NULL);
		}

	};

	typedef std::set<Vector3, Vector3Cmp> KeyPoints;
	typedef std::map<Vector3, NeoNotch, Vector3Cmp> NeoNotchMap;

	class NeoNavMeshGenerator {

		public:

			NeoNavMeshGenerator();

			virtual NeoNavMeshSPtr initNavMesh(NeoTerrain* terrain);
			virtual void applyConvexityRelaxation(NeoReal cr);
			virtual void breakRealNotches();
			virtual void createCells();
			
			virtual NeoNavMeshSPtr execute(NeoTerrain* terrain, NeoReal convexityRelaxation = 0.0);

			virtual NeoNotchMap& getNotchesReal();
			virtual NeoNotchMap& getNotchesStrict();
		
		protected:
			
			size_t _testIndex;

			typedef std::pair<NeoVertex*, NeoEdgePair> NeoVertexSubEdgePair;

			NeoNavMeshSPtr _navMesh;
			
			Polygons3 _obstacleContours;

			NeoNotchMap _allNotches;
			NeoNotchMap _realNotches;

			virtual void initNavMeshFace(NeoFace* face);
			virtual void createObstacleContours();
			virtual void computeAllNotches();
			
			virtual void getCandidateEdges(NeoVertex* vertex, NeoEdgePSet& result);
			virtual void getCandidateEdges(NeoVertex* vertex, NeoFacePSet& facesIgnored, NeoEdgePSet& result);
			virtual void getCandidateEdges(NeoVertex* vertex, NeoFace* face, NeoEdgePSet& result);

			virtual void computeCandidatePoint(NeoNotch& notch, NeoEdge* edge, NeoCandidate& result);
			virtual void computeBestCandidate(NeoNotch& notch, NeoEdgePSet& candidateEdge, NeoCandidate& result);

			virtual void breakRealNotch(NeoNotch& notch);
			virtual void createSubPortals(const Vector3& bestCandidatePos, const NeoGeometrySolver::Ray3& ray, NeoPortalPair& portals);
			virtual NeoVertex* subdivideIntersectedEdge(const NeoGeometrySolver::Ray3& ray, NeoEdgePair& result);

			virtual NeoFace* reconstructFace(const Polygon3& contourOriginal, size_t startVertex, size_t endVertex);

			virtual void addFaceToCell(NeoFace* face, NeoCell* cell);

	};

};