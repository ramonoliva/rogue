#pragma once

#include "NeoTypes.h"
#include "NeoAABB.h"
#include "NeoTree.h"

namespace NEOGEN {

	template <class T, class U> 
	class NeoOctreeNode : public NeoTreeNode<NeoOctreeNode<T, U>> {

		public:

			NeoOctreeNode(const NeoAABB& aabb, NeoOctreeNode* parent) : NeoTreeNode(parent) {
				_aabb = aabb;
			}

			virtual ~NeoOctreeNode() {
				clear();
			}

			virtual void clear() {
				NeoTreeNode::clear();
				_objects.clear();
			}

			virtual bool isEmpty() {
				return _objects.empty();
			}

			virtual const NeoAABB& getAABB() {
				return _aabb;
			}

			virtual void insert(T object) {
				_objects.insert(object);

				//Check for the leaf conditions, i.e., there is only one object in the list or
				//the octree has reached the minimum size, that by construction, it is a cube of size 1. 
				if ((_objects.size() == 1) || (Math::equal(_aabb.getSize().getX(), NeoReal(1)))) return;	

				//The node is not a leaf, so it is possible that we need to subdivide the node, if the 
				//object is completely contained in one of the childs. 

				//1) Create the bounding boxes of the childs. 
				const Vector3& center = _aabb.getCenter();
				const Vector3& min = _aabb.getMin();
				const Vector3& max = _aabb.getMax();
				NeoAABBVector boxes;
				boxes.push_back(NeoAABB(min, center));
				boxes.push_back(NeoAABB(Vector3(center.getX(), min.getY(), min.getZ()), Vector3(max.getX(), center.getY(), center.getZ())));
				boxes.push_back(NeoAABB(Vector3(center.getX(), min.getY(), center.getZ()), Vector3(max.getX(), center.getY(), max.getZ())));
				boxes.push_back(NeoAABB(Vector3(min.getX(), min.getY(), center.getZ()), Vector3(center.getX(), center.getY(), max.getZ())));
				boxes.push_back(NeoAABB(Vector3(min.getX(), center.getY(), min.getZ()), Vector3(center.getX(), max.getY(), center.getZ())));
				boxes.push_back(NeoAABB(Vector3(center.getX(), center.getY(), min.getZ()), Vector3(max.getX(), max.getY(), center.getZ())));
				boxes.push_back(NeoAABB(center, max));
				boxes.push_back(NeoAABB(Vector3(min.getX(), center.getY(), center.getZ()), Vector3(center.getX(), max.getY(), max.getZ())));

				//2) Check if any of the boxes completely contains the object. 
				size_t iContainer = 0; 
				while ((iContainer < boxes.size()) && (!boxes[iContainer].contains(object->getAABB()))) iContainer++;
				if (iContainer < boxes.size()) {
					//The object IS completely contained in the boxes[iContainer]. So we have to create the childs and 
					//(possibly) insert the objects of this node in the corresponding childs. 

					//2.1) Create the childs
					for (size_t i = 0; i < boxes.size(); i++) {
						addChild(std::shared_ptr<NeoOctreeNode<T, U>>(new NeoOctreeNode<T, U>(boxes[i], this)));
					}

					//2.2) Check if the objects are contained in any of the newly created child and update the object lists
					//accordingly. 
					std::set<T, U> tmp = _objects;
					_objects.clear();
					for (std::set<T, U>::iterator itObject = tmp.begin(); itObject != tmp.end(); itObject++) {
						const NeoAABB& aabb = (*itObject)->getAABB();
						size_t i = 0;
						for (i = 0; i < _childs.size(); i++) {
							if (_childs[i]->getAABB().contains(aabb)) {
								_childs[i]->insert(*itObject);
								break;
							}
						}
						if (i == _childs.size()) {
							//None of the childs completely contains the object, so reinsert it in the current objects node list. 
							_objects.insert(*itObject);
						}
					}
				}
			}

			virtual void remove(T object) {
				_objects.erase(object);
			}

			virtual bool contains(T object) {
				std::set<T, U>::iterator itFound = _objects.find(object);
				return itFound != _objects.end();
			}

			virtual void computeIntersectedObjects(const NeoAABB& aabb, std::set<T, U>& result) {
				//1) Check for intersections with the objects contained in this node. 
				for (std::set<T, U>::iterator it = _objects.begin(); it != _objects.end(); it++) {
					if (aabb.intersects((*it)->getAABB())) result.insert(*it);
				}

				//2) Check if aabb intersects with the aabb of any of the childs. If so, recursive call. 
				for (size_t i = 0; i < _childs.size(); i++) {
					if (aabb.intersects(_childs[i]->getAABB())) {
						_childs[i]->computeIntersectedObjects(aabb, result);
					}
				}
			}

			virtual void computeIntersectedObjects(const NeoGeometrySolver::Ray3& ray, std::set<T, U>& result) {
				//1) Check for intersections with the objects contained in this node.
				for (std::set<T, U>::iterator it = _objects.begin(); it != _objects.end(); it++) {
					T object = *it;
					NeoGeometrySolver::IntersectionResult3 intersectionResult;
					if ((*it)->getAABB().intersects(ray, intersectionResult)) result.insert(*it);
				}

				//2) Check if ray intersects with the aabb of any of the childs. If so, recursive call. 
				for (size_t i = 0; i < _childs.size(); i++) {
					NeoGeometrySolver::IntersectionResult3 intersectionResult;
					if (_childs[i]->getAABB().intersects(ray, intersectionResult)) {
						_childs[i]->computeIntersectedObjects(ray, result);
					}
				}
			}

		protected:

			std::set<T, U> _objects;
			NeoAABB _aabb;

	};
	
	template <class T, class U>
	class NeoOctree : public NeoTree<NeoOctreeNode<T, U>> {

		public:

			NeoOctree(const NeoAABB& aabb) {
				//The AABB passed as parameter is just a hint for the size of the 
				//AABB of the whole tree. By convention, we are going to construct a 
				//cubic Octree, with a size that is power of 2. 
				NeoAABB rootAABB = aabb;
				Vector3 size = rootAABB.getSize();
				NeoReal m = std::max(size.getX(), std::max(size.getY(), size.getZ()));
				NeoReal s = NeoReal(Math::nextPowerOf2(m));
				rootAABB.setMax(rootAABB.getMin() + Vector3(s,s,s));

				_root = std::shared_ptr<NeoOctreeNode<T, U>>(new NeoOctreeNode<T, U>(rootAABB, NULL));
			}

			virtual NeoOctreeNode<T, U>* findContainer(T object) {
				//Find the node of the octree that perfectly contains the object. 
				return findContainer(object->getAABB());
			}

			virtual NeoOctreeNode<T, U>* findContainer(const NeoAABB& aabb) {
				//Find the node of the octree that perfectly contains the defined AABB
				if (!_root->getAABB().contains(aabb)) return NULL;

				return findContainer(aabb, _root.get());
			}

			virtual void insert(T object) {
				NeoOctreeNode<T, U>* node = findContainer(object);

				if (!node) return;
				node->insert(object);
			}

			virtual void remove(T object) {
				NeoOctreeNode<T, U>* node = findContainer(object);

				if (!node) return;
				node->remove(object);

				//Check the integrity of the branch after removing this object. 
				if (isEmptyBranch(node)) node->clear();
			}

			virtual bool isEmptyBranch(NeoOctreeNode<T, U>* node) {
				//Indicates if the branch starting at node is empty or not, i.e., the node nor any of its childs
				//contains any object. 
				const std::vector<std::shared_ptr<NeoOctreeNode<T, U>>>& childs = node->getChilds();
				size_t i = 0;
				bool empty = node->isEmpty();
				while (empty && (i < childs.size())) {
					empty = empty && isEmptyBranch(childs[i].get());
					i++;
				}
				
				return empty;
			}

			virtual void computeIntersectedObjects(const NeoAABB& aabb, ElementIDSet& result) {
				std::set<T, U> tmp;
				computeIntersectedObjects(aabb, tmp);
				for (std::set<T, U>::iterator it = tmp.begin(); it != tmp.end(); it++) {
					result.insert((*it)->getID());
				}
			}

			virtual void computeIntersectedObjects(const NeoAABB& aabb, std::set<T, U>& result) {
				_root->computeIntersectedObjects(aabb, result);
			}

			virtual void computeIntersectedObjects(const NeoGeometrySolver::Ray3& ray, ElementIDSet& result) {
				std::set<T, U> tmp;
				computeIntersectedObjects(ray, tmp);
				for (std::set<T, U>::iterator it = tmp.begin(); it != tmp.end(); it++) {
					result.insert((*it)->getID());
				}
			}

			virtual void computeIntersectedObjects(const NeoGeometrySolver::Ray3& ray, std::set<T, U>& result) {
				_root->computeIntersectedObjects(ray, result);
			}

		protected:

			virtual NeoOctreeNode<T, U>* findContainer(const NeoAABB& aabb, NeoOctreeNode<T, U>* node) {
				//Look for the child that completely contains the object (if any). 
				const std::vector<std::shared_ptr<NeoOctreeNode<T, U>>>& childs = node->getChilds();
				NeoOctreeNode<T, U>* container = NULL;
				size_t i = 0;
				while (!container && (i < childs.size())) {
					if (childs[i]->getAABB().contains(aabb)) container = childs[i].get();
					i++;
				}
				return container? findContainer(aabb, container) : node;
			}

	};

};