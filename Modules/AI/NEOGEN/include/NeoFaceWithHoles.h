#pragma once

#include "NeoFace.h"

namespace NEOGEN {

	class NeoFaceWithHoles : public NeoFace {

		public:

			virtual void addHole(Hole* h);

		protected:

			HolesPtr _holes;

	};

};