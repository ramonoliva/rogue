#ifndef __ngTypes_h_
#define __ngTypes_h_

#include <vector>
#include <pair>
#include <set>
#include <map>

namespace NEOGEN {

	typedef std::vector<NEOGEN::Vec3D> PointList;
	typedef std::vector<Rogue::ManualObject*> ManualObstacleList;
	typedef std::set<Rogue::GameObject*> GameObjectSet;
	typedef std::pair<Rogue::GameObject*, Rogue::GameObject*> GameObjectPair;
	typedef std::set<GameObjectPair> GameObjectPairSet;
	typedef std::vector<Rogue::GameObject*> ObstacleList;

};

#endif