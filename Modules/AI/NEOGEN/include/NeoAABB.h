#pragma once

#include "NeoTypes.h"
#include "NeoVector3.h"
#include "NeoGeometrySolverAux.h"

namespace NEOGEN {

	class NeoAABB {

		public:

			NeoAABB();
			NeoAABB(const Vector3& min, const Vector3& max);

			virtual void clear();

			virtual void addPoint(const Vector3& p);
			virtual void addPoints(const Polygon3& polygon);

			virtual const Vector3& getMin() const;
			virtual const Vector3& getMax() const;

			virtual void setMin(const Vector3& min);
			virtual void setMax(const Vector3& max);

			virtual Vector3 getCenter() const;

			virtual Vector3 getSize() const;

			virtual Vector3 getSizeHalf() const;

			virtual bool contains(const Vector3& p) const;
			virtual bool contains(const NeoAABB& aabb) const;

			virtual bool intersects(const NeoAABB& aabb) const;
			virtual bool intersects(const NeoGeometrySolver::Ray3& ray, NeoGeometrySolver::IntersectionResult3& result) const;

		protected:

			Vector3 _min;
			Vector3 _max;

			bool _isEmpty;

	};

};