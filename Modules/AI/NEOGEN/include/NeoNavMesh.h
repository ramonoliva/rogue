#pragma once

#include "NeoMesh.h"
#include "NeoPortal.h"
#include "NeoCell.h"

namespace NEOGEN {

	typedef std::map<NeoVertexPair, NeoPortal*> NeoPortalMapEndpoints;

	class NeoNavMesh : public NeoMesh {

		public:

			NeoNavMesh(const NeoAABB& aabb);

			virtual NeoPortal* createPortal();
			virtual NeoPortal* getPortal(ElementID portalID);
			virtual NeoPortalMap& getPortals();
			virtual size_t getNumPortals();

			virtual NeoCell* createCell();
			virtual NeoCell* getCell(ElementID cellID);
			virtual NeoCellMap& getCells();
			virtual size_t getNumCells();

			virtual void exportOBJ(const std::string& path);
			
		protected:

			ElementID _newPortalID;
			ElementID _newCellID;

			NeoPortalMap _portalMap;
			NeoCellMap _cellMap;

			virtual void init();

	};

};

