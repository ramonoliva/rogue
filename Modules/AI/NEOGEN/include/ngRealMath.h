#ifndef __ngRealMath_h_
#define __ngRealMath_h_

#include <math.h>
#include <limits>


#define FLOAT_PRECISION	0.0001f
#define FLOAT_INFINITY std::numeric_limits<float>::infinity()

namespace RealMath {
	enum ProjectionPlaneEnum {
		PROJECTION_XY,	//z = 0
		PROJECTION_XZ,	//y = 0
		PROJECTION_YZ	//x = 0
	};

	static bool equal(float f1, float f2, float epsilon = FLOAT_PRECISION) {
		return (std::abs(f1 - f2) < epsilon);  
	}

	static bool lessOrEqual(float f1, float f2, float epsilon = FLOAT_PRECISION) {
		if (equal(f1, f2, epsilon)) return true;
		return (f1 < f2);
	}

	static bool greaterOrEqual(float f1, float f2, float epsilon = FLOAT_PRECISION) {
		if (equal(f1, f2, epsilon)) return true;
		return (f1 > f2);
	}

};

#endif