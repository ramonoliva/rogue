#ifndef __ngTriangulator_h_
#define __ngTriangulator_h_

#include <vector>  // Include STL vector class.
#include "ngVector.h"
#include "ngVertex.h"

namespace NEOGEN {
	// Typedef an STL vector of vertices which are used to represent
	// a polygon/contour and a series of triangles.
	typedef std::vector<Vertex>	VectorVert;
	typedef std::vector<size_t> VectorVertID;
	class Triangulator
	{
	public:

	  // Triangulator a contour/polygon, places results in STL vector
	  // as series of triangles.
	  static bool Process(VectorVert &contour, VectorVertID &result);

	  // compute area of a contour/polygon
	  static float Area(VectorVert &contour);

	  // decide if point Px/Py is inside triangle defined by
	  // (Ax,Ay) (Bx,By) (Cx,Cy)
	  static bool InsideTriangle(float Ax, float Ay,
						  float Bx, float By,
						  float Cx, float Cy,
						  float Px, float Py);


	private:
	  static bool Snip(VectorVert &contour,int u,int v,int w,int n,int *V);

	};

};


#endif