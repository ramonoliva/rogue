#pragma once

#include "NeoMeshElement.h"
#include "NeoVertex.h"

namespace NEOGEN {

	class NeoEdge : public NeoMeshElement {

		public:

			NeoEdge(ElementID id, NeoVertex* v1, NeoVertex* v2);
			virtual ~NeoEdge();

			virtual NeoVertex* getVertexBegin();
			virtual NeoVertex* getVertexEnd();
			
			virtual NeoEdge* getOpposite();
			virtual void setOpposite(NeoEdge* opposite);

			virtual NeoEdge* getParent();
			virtual void setParent(NeoEdge* parent);
			
			virtual NeoFace* getFace();
			virtual void addIncidentFace(NeoFace* face);
			virtual void removeIncidentFace(NeoFace* face);
			virtual bool isIncidentFace(NeoFace* face);
			virtual void clearIncidentFaces();
			virtual const NeoFacePSet& getIncidentFaces();

			virtual Vector3 getDirection();
			virtual Vector3 getDirectionInverse();

			virtual void setTypeWall(bool isWall);

			virtual bool isTypeWall();
			virtual bool isTypeWalkable();
			virtual bool isTypeObstacle();
			virtual bool isTypeSubPortal();

			virtual void log();

		protected:

			NeoVertex* _v1;
			NeoVertex* _v2;

			Vector3 _direction;
			
			NeoEdge* _opposite;
			NeoEdge* _parent;
			
			NeoFacePSet _incidentFaces;

			bool _isWall;

	};

};