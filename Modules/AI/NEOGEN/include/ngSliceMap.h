#ifndef __ngSliceMap_h_
#define __ngSliceMap_h_

#include <OgreTexture.h>
#include <OgreImage.h>
#include <OgreColourValue.h>
#include <OgreEntity.h>
#include <OgreSceneManager.h>
#include <OgreManualObject.h>
#include <OgreMaterialManager.h>
#include <OgreStaticGeometry.h>
#include <OgreTexture.h>

#include <algorithm>
#include <vector>

namespace NEOGEN {
	class SliceMap {

	public:

		enum SLICE_MAP_ORIENTATION { 
			ORIENTATION_TOP,
			ORIENTATION_FRONT,
			ORIENTATION_SIDE
		};

		SliceMap(std::vector<Ogre::TexturePtr>& texList, size_t w, size_t d, size_t h, int orient = ORIENTATION_TOP);
		SliceMap(size_t w, size_t d, size_t h);
		~SliceMap();

		void clear();
	
		Ogre::uint32 getColourAt(size_t target, size_t x, size_t z);
		void setColourAt(size_t target, Ogre::uint32 color, size_t x, size_t z);
		void setSolidVoxel(size_t x, size_t y, size_t z);
		void setEmptyVoxel(size_t x, size_t y, size_t z);

		bool isSolidVoxel(int x, int y, int z);
		bool isSolidVoxelTop(size_t x, size_t y, size_t z);
		bool isSolidVoxelFront(size_t x, size_t y, size_t z);
		bool isSolidVoxelSide(size_t x, size_t y, size_t z);
		bool isOutOfBounds(int x, int y, int z);

	private:

		#define COLOUR_TEXTURE_DEPTH 32

		typedef std::vector<std::vector<Ogre::uint32>> ColourTexture;
		typedef std::vector<ColourTexture> ColourTextureList;
		ColourTextureList cTable;

		size_t width;		//Amplada de la textura (EIX X)
		size_t depth;		//Profunditat de la textura (EIX Y)
		size_t height;		//Llargada de la textura (EIX Z)
		int orientation;	//Indica si �s un SliceMap TOP, SIDE o FRONT

		void createColourTable(std::vector<Ogre::TexturePtr>& texList);
		void createEmptyColourTable();

		void clearColourTexture(ColourTexture& cTexture);

	};
};

#endif