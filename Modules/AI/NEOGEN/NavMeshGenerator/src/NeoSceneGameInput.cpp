#include "NeoSceneGame.h"
#include "ngGlobalVars.h"
#include <RogueCore.h>
#include "RogueInterfaceGUIManager.h"

//Aquest fitxer cont� les funcions d'NEOGEN que s'encarreguen de tractar l'input de l'usuari

using namespace Rogue;
using namespace NEOGEN;

#pragma region INPUT

void NeoSceneGame::processKeyboardEvents() {
	Scene::processKeyboardEvents();
	bool ctrlPressed = _inputManager->isPressedKey(OIS::KC_LCONTROL);
	if (_inputManager->isTriggeredKey(OIS::KC_R)) {
		if (applicationState == STATE_NAVMESH_LAYER_GENERATED) {
			nmGen->reset();
			obstacleSolverTime = 0.0f;
			applicationState = STATE_IDLE;
		}
	}
	else if (_inputManager->isTriggeredKey(OIS::KC_F1)) {
		showAdjacencyGraph = !showAdjacencyGraph;
		nmGen->showGraph(showAdjacencyGraph);
	}
	else if (_inputManager->isTriggeredKey(OIS::KC_F2)) {
		boardVisible = !boardVisible;
		nmGen->showBoard(boardVisible);
	}
	else if (_inputManager->isTriggeredKey(OIS::KC_F11)) {
		DEBUG_MODE = !DEBUG_MODE;
	}
	else if (_inputManager->isTriggeredKey(OIS::KC_F12)) {
		if (_renderManager->getRenderMode() == RENDER_MODE_FORWARD) _renderManager->setRenderMode(RENDER_MODE_DEFERRED);
		else _renderManager->setRenderMode(RENDER_MODE_FORWARD);
	}
	else if (_inputManager->isTriggeredKey(OIS::KC_Z)) {
		Camera* cam = _renderManager->getMainCamera();
		if (cam->getPolygonMode() == Ogre::PM_SOLID) {
			cam->setPolygonMode(Ogre::PM_WIREFRAME);
		}
		else if (cam->getPolygonMode() == Ogre::PM_WIREFRAME) {
			cam->setPolygonMode(Ogre::PM_SOLID);
		}
	}
	else if (_inputManager->isTriggeredKey(OIS::KC_NUMPAD1)) {
		initFrontView();
	}
	else if (_inputManager->isTriggeredKey(OIS::KC_NUMPAD3)) {
		initSideView();
	}
	else if (_inputManager->isTriggeredKey(OIS::KC_NUMPAD7)) {
		initTopView();
	}
	else if (_inputManager->isTriggeredKey(OIS::KC_NUMPAD0)) {
		initPerspectiveCamera(camPosPerspective);
	}
	else if (_inputManager->isTriggeredKey(OIS::KC_O)) {
		Ogre::LogManager::getSingletonPtr()->logMessage("HOLA!!!");
		Ogre::LogManager::getSingletonPtr()->logMessage("applicationState = " + std::to_string(applicationState));
		if (applicationState == STATE_IDLE) {
			if (ctrlPressed) {
				//1) Select the where we want to save the file
				InterfaceGUIManager::FileTypeList ftList;
				InterfaceGUIManager::FileType type(L"OGRE Mesh", L"*.mesh");
				ftList.push_back(type);
				const String fileName = _guiManager->openFile(ftList);
				Ogre::LogManager::getSingletonPtr()->logMessage("fileName = " + fileName);

				if (!fileName.empty()) {
					std::string baseName, extension, path;
					Ogre::StringUtil::splitFullFilename(fileName, baseName, extension, path);
					Ogre::LogManager::getSingletonPtr()->logMessage("baseName = " + baseName);
					Ogre::LogManager::getSingletonPtr()->logMessage("extension = " + extension);
					Ogre::LogManager::getSingletonPtr()->logMessage("path = " + path);
				}
			}
		}
	}
	else if (_inputManager->isTriggeredKey(OIS::KC_V)) {
		Layer* activeLayer;
		nmGen->test();
		if (applicationState == STATE_IDLE) {
			
			gPrintHeader("STATE IDLE");
						
			appTimeBefore = globalTimer->getMilliseconds();
			voxelization();
			gComputeTimeDiffMS();
			
			gPrintTimeDiffS("TIME STAGE voxelization = ");
			
			appTimeBefore = globalTimer->getMilliseconds();
			voxelizer->detectAccessibleVoxels();
			gComputeTimeDiffMS();

			gPrintTimeDiffS("TIME STAGE detectAccessibleVoxels = ");
				
			_renderManager->renderOneFrame();
		}
		else if (applicationState == STATE_LAYERS_GENERATED) {

			gPrintHeader("STATE LAYERS GENERATED");
			
			appTimeBefore = globalTimer->getMilliseconds();
			voxelizer->detectConnectedWalkableLayers();
			applicationState = STATE_SELECT_LAYER;
			gComputeTimeDiffMS();

			gPrintTimeDiffS("TIME STAGE detectConnectedWalkableLayers = ");
		}
		else if (applicationState == STATE_SELECT_LAYER) {
			gPrintHeader("STATE SELECT LAYER");
			
			appTimeBefore = globalTimer->getMilliseconds();
			layerSelection();
			gComputeTimeDiffMS();
			
			gPrintTimeDiffS("TIME STAGE layerSelection = ");
		}
		else if (applicationState == STATE_LAYER_SELECTED) {
			gPrintHeader("STATE LAYER SELECTED");
			
			appTimeBefore = globalTimer->getMilliseconds();
			fillLayerTexture();
			gComputeTimeDiffMS();
			
			gPrintTimeDiffS("TIME STAGE fillLayerTexture = ");

			appTimeBefore = globalTimer->getMilliseconds();
			levelRefinement();
			gComputeTimeDiffMS();
			
			gPrintTimeDiffS("TIME STAGE levelRefinement = ");
		}
		else if (applicationState == STATE_LAYER_REFINED) {
			gPrintHeader("STATE LAYER REFINED");
			
			appTimeBefore = globalTimer->getMilliseconds();
			initNavMeshData();
			gComputeTimeDiffMS();

			gPrintTimeDiffS("TIME STAGE initNavMeshData = ");
		}
		else if (applicationState == STATE_GENERATE_NAVMESH_LAYER) {
			gPrintHeader("STATE GENERATE NAVMESH LAYER");
			
			appTimeBefore = globalTimer->getMilliseconds();
			generateNavMesh();
			gComputeTimeDiffMS();

			gPrintTimeDiffS("TIME STAGE generateNavMesh = ");
		}
		else if (applicationState == STATE_NAVMESH_LAYER_GENERATED) {
			gPrintHeader("STATE NAVMESH LAYER GENERATED");
			
			appTimeBefore = globalTimer->getMilliseconds();
			activeLayer = voxelizer->getActiveLayer();
			activeLayer->showNavMesh(false);					//Oculto la navMesh de la layer que acabo de crear
			voxelizer->nextLayer();
			layerSelection();
			gComputeTimeDiffMS();

			gPrintTimeDiffS("TIME STAGE layerSelection = ");
		}
		else if (applicationState == STATE_MERGE_NAV_MESHES) {
			//Hem generat la navMesh de totes les layers. Ara cal unir-les en una de sola. 
			gPrintHeader("STATE MERGE NAVMESHES");
			
			computationMode = COMPUTATION_MODE_CPU;
			appTimeBefore = globalTimer->getMilliseconds();
			voxelizer->showNavMeshLayers(false);
			voxelizer->generateNavMeshScene();
			//voxelizer->getSceneNavMesh()->drawGraph();
			applicationState = STATE_DELETE_TJOINTS;
			gComputeTimeDiffMS();

			gPrintTimeDiffS("TIME STAGE generateNavMeshScene = ");
		}
		else if (applicationState == STATE_DELETE_TJOINTS) {
			//Al unir dues navMeshes, poden apar�ixer T-joints en els punts d'uni�. 
			//Cal eliminar-les. 
			gPrintHeader("STATE DELETE TJOINTS");
			
			appTimeBefore = globalTimer->getMilliseconds();
			voxelizer->deleteTJoints();
			applicationState = STATE_REPAIR_CELLS;
			gComputeTimeDiffMS();

			gPrintTimeDiffS("TIME STAGE deleteTJoints = ");
		}
		else if (applicationState == STATE_REPAIR_CELLS) {
			//Al eliminar les TJoints, genero una cel�la que pot contenir concavitats. 
			//Cal eliminar aquestes concavitats
			gPrintHeader("STATE REPAIR CELLS");
			
			appTimeBefore = globalTimer->getMilliseconds();
			NavMesh* nm = voxelizer->getSceneNavMesh();
			nmGen->repairCells(nm);
			nm->drawEdges();
			if (voxelizer->allLayersFusioned()) {
				Ogre::LogManager::getSingletonPtr()->logMessage("ALL LAYERS FUSIONED");
				if (_guiSceneGame->isGPUModeChecked()) computationMode = COMPUTATION_MODE_GPU;
				else computationMode = COMPUTATION_MODE_CPU;
				voxelizer->getSceneNavMesh()->drawGraph();
				applicationState = STATE_FINAL_NAVMESH_GENERATED;
				InterfaceGUIManager* guiManager = Rogue::Core::getManager<InterfaceGUIManager>();
				InterfaceGUIManager::FileTypeList tList;
				tList.push_back(InterfaceGUIManager::FileType(L"Navigation Mesh", L".nav"));

				String fileName = guiManager->saveFile(tList, L"navMesh");
				MapParser::getSingleton().saveNavMesh(voxelizer->getSceneNavMesh(), fileName);
			}
			else {
				Ogre::LogManager::getSingletonPtr()->logMessage("NOT ALL LAYERS FUSIONED");
				applicationState = STATE_MERGE_NAV_MESHES;
			}
				
			gComputeTimeDiffMS();

			gPrintTimeDiffS("TIME STAGE repairCells = ");
		}
	}
	else if (_inputManager->isTriggeredKey(OIS::KC_SYSRQ)) {
		_renderManager->takeScreenShot("screenshot", ".png");
    }
}

void NeoSceneGame::processGUIEvents() {
	setDrawMode(_guiSceneGame->getDrawMode());
}

void NeoSceneGame::processMouseEvents() {
	int pX, pY, w, h;
	float viewX, viewY;
	_inputManager->getClippingAreaSize(w, h);
	_inputManager->getMouseCoords(pX, pY);
	viewX = float(pX) / float(w);
	viewY = float(pY) / float(h);
	if (_inputManager->isTriggeredMouseButton(OIS::MB_Left)) {
		if (applicationState == STATE_VOXELIZED) {
			selectWalkableArea(viewX, viewY);
		}
	}
}

#pragma endregion