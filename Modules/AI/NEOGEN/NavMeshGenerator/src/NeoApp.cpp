#include "NeoApp.h"
#include <NeoSceneGame.h>
#include "NeoSceneObstacles.h"
#include "SceneNavMeshGenerator.h"

using namespace Rogue;

#pragma region CREATION AND DESTRUCTION

NeoApp::NeoApp() {}

NeoApp::NeoApp(const std::string& iFileName, const std::string& oFileName) {
	_inputFileName = iFileName;
	_outputFileName = oFileName;
}

NeoApp::~NeoApp() {}

void NeoApp::registerScenes() {
	_sceneList.push_back(new NeoSceneGame());
	_sceneList.push_back(new SceneObstacles());
	_sceneList.push_back(new SceneNavMeshGenerator());
	//_sceneList.push_back(new SceneNavMeshGenerator(_inputFileName, _outputFileName));
}

#pragma endregion