#include "DebuggerNeoMesh.h"
#include "NeoOGRE.h"

using namespace NEOGEN;
using namespace Rogue;

#pragma region CREATION AND DESTRUCTION

DebuggerNeoMesh::DebuggerNeoMesh() : DebuggerBase() {
	_debuggerFaces = DebuggerFacesUPtr(new DebuggerFaces());
	_renderManager->setParentSceneNode(_debuggerFaces->getSceneNode(), _sceneNode);

	_debuggerEdges = DebuggerPolyLineUPtr(new DebuggerPolyLine());
	_debuggerEdges->setColor(0xffffffff);
	_debuggerEdges->setSize(0);
	_renderManager->setParentSceneNode(_debuggerEdges->getSceneNode(), _sceneNode);
		
	_debuggerEdgesObstacle = DebuggerPolyLineUPtr(new DebuggerPolyLine());
	_debuggerEdgesObstacle->setColor(0xffff0000);
	_debuggerEdgesObstacle->setSize(0);
	_renderManager->setParentSceneNode(_debuggerEdgesObstacle->getSceneNode(), _sceneNode);
		
	_debuggerEdgesPortal = DebuggerPolyLineUPtr(new DebuggerPolyLine());
	_debuggerEdgesPortal->setColor(0xff00ff00);
	_debuggerEdgesPortal->setSize(0);
	_renderManager->setParentSceneNode(_debuggerEdgesPortal->getSceneNode(), _sceneNode);
	
	_debuggerNormals = DebuggerPolyLineUPtr(new DebuggerPolyLine());
	_debuggerNormals->setColor(0xff000000);
	_debuggerNormals->setSize(0);
	_renderManager->setParentSceneNode(_debuggerNormals->getSceneNode(), _sceneNode);

	_debuggerOctree = DebuggerPolyLineUPtr(new DebuggerPolyLine());
	_debuggerOctree->setColor(0xff00ff00);
	_debuggerOctree->setSize(0);
	_renderManager->setParentSceneNode(_debuggerOctree->getSceneNode(), _sceneNode);
}

DebuggerNeoMesh::~DebuggerNeoMesh() {
	
}

#pragma endregion

#pragma region GET AND SET

void DebuggerNeoMesh::setVisible(bool v) {
	setVisibleFaces(v);
	setVisibleNormals(v);
	setVisibleEdges(v);
	setVisibleOctree(v);
}

void DebuggerNeoMesh::setVisibleFaces(bool v) {
	_debuggerFaces->setVisible(v);
}

void DebuggerNeoMesh::setVisibleNormals(bool v) {
	_debuggerNormals->setVisible(v);
}

void DebuggerNeoMesh::setVisibleEdges(bool v) {
	_debuggerEdges->setVisible(v);
	_debuggerEdgesObstacle->setVisible(v);
}

void DebuggerNeoMesh::setVisibleOctree(bool v) {
	_debuggerOctree->setVisible(v);
}

void DebuggerNeoMesh::clear() {
	_debuggerFaces->clear();
	_debuggerEdges->clear();
	_debuggerEdgesObstacle->clear();
	_debuggerEdgesPortal->clear();
	_debuggerNormals->clear();
	_debuggerOctree->clear();
}

#pragma endregion

#pragma region UPDATE

void DebuggerNeoMesh::update(NeoMesh* mesh, const NEOGEN::Vector3& offset) {
	_mesh = mesh;
	_sceneNode->setPosition(Ogre::Vector3(offset.getX(), offset.getY(), offset.getZ()));
	update();
}

void DebuggerNeoMesh::update() {
	clear();

	if (!_mesh) return;
	
	refreshDebuggers();
	updateDebuggers();
}

void DebuggerNeoMesh::updateDebuggers() {
	_debuggerFaces->update();
	_debuggerEdges->update();
	_debuggerEdgesObstacle->update();
	_debuggerEdgesPortal->update();
	_debuggerNormals->update();
	_debuggerOctree->update();
}

void DebuggerNeoMesh::refreshDebuggers() {
	NeoFaceMap& faceMap = _mesh->getFaces();
	NeoFacePVector facesWalkable, facesObstacle, facesHole, facesUndefined;
	for (NeoFaceMap::iterator itFace = faceMap.begin(); itFace != faceMap.end(); itFace++) {
		NeoFace* face = itFace->second.get();
		refreshDebuggerFaces(face);
		refreshDebuggerEdges(face);
		refreshDebuggerNormals(face);
	}
	refreshDebuggerOctree();
}

void DebuggerNeoMesh::refreshDebuggerFaces(NeoFace* face) {
	_debuggerFaces->addTriangles(face->getTriangles());
}

void DebuggerNeoMesh::refreshDebuggerEdges(NeoFace* face) {
	NeoEdgePVector& edges = face->getEdges();
	for (NeoEdgePVector::iterator itEdge = edges.begin(); itEdge != edges.end(); itEdge++) {
		NeoEdge* edge = *itEdge;
		
		DebuggerPolyLine* debugger;
		if (edge->isTypeObstacle()) debugger = _debuggerEdgesObstacle.get();
		else if (edge->isTypeSubPortal()) debugger = _debuggerEdgesPortal.get();
		else debugger = _debuggerEdges.get();

		int v1ID = debugger->addVertex(NeoOGRE::toOGRE(edge->getVertexBegin()->getPosition()));
		int v2ID = debugger->addVertex(NeoOGRE::toOGRE(edge->getVertexEnd()->getPosition()));
		
		debugger->addSegment(v1ID, v2ID);
	}
}

void DebuggerNeoMesh::refreshDebuggerNormals(NeoFace* face) {
	Vector3 v1 = face->getCenter();
	Vector3 v2 = v1 + face->getNormal();
	int v1ID = _debuggerNormals->addVertex(NeoOGRE::toOGRE(v1));
	int v2ID = _debuggerNormals->addVertex(NeoOGRE::toOGRE(v2));
	_debuggerNormals->addSegment(v1ID, v2ID);
}

void DebuggerNeoMesh::refreshDebuggerOctree() {
	if (!_mesh->getOctree()) return;

	refreshDebuggerOctreeNode(_mesh->getOctree()->getRoot());
}

void DebuggerNeoMesh::refreshDebuggerOctreeNode(NeoOctreeNodeFace* node) {
	const NeoAABB& aabb = node->getAABB();

	const Vector3& min = aabb.getMin();
	const Vector3& max = aabb.getMax();

	//Add the points to the debugger
	int p0 = _debuggerOctree->addVertex(NeoOGRE::toOGRE(min));
	int p1 = _debuggerOctree->addVertex(Ogre::Vector3(max.getX(), min.getY(), min.getZ()));
	int p2 = _debuggerOctree->addVertex(Ogre::Vector3(max.getX(), min.getY(), max.getZ()));
	int p3 = _debuggerOctree->addVertex(Ogre::Vector3(min.getX(), min.getY(), max.getZ()));

	int p4 = _debuggerOctree->addVertex(NeoOGRE::toOGRE(max));
	int p5 = _debuggerOctree->addVertex(Ogre::Vector3(min.getX(), max.getY(), max.getZ()));
	int p6 = _debuggerOctree->addVertex(Ogre::Vector3(min.getX(), max.getY(), min.getZ()));
	int p7 = _debuggerOctree->addVertex(Ogre::Vector3(max.getX(), max.getY(), min.getZ()));

	//Define the segments
	_debuggerOctree->addSegment(p0, p1);
	_debuggerOctree->addSegment(p1, p2);
	_debuggerOctree->addSegment(p2, p3);
	_debuggerOctree->addSegment(p3, p0);

	_debuggerOctree->addSegment(p4, p5);
	_debuggerOctree->addSegment(p5, p6);
	_debuggerOctree->addSegment(p6, p7);
	_debuggerOctree->addSegment(p7, p4);

	_debuggerOctree->addSegment(p0, p6);
	_debuggerOctree->addSegment(p1, p7);
	_debuggerOctree->addSegment(p2, p4);
	_debuggerOctree->addSegment(p3, p5);

	const std::vector<NeoOctreeNodeFaceSPtr>& childs = node->getChilds();
	for (size_t i = 0; i < childs.size(); i++) {
		refreshDebuggerOctreeNode(childs[i].get());
	}
}

#pragma endregion