#include "DebuggerNeoTerrain.h"
#include "NeoOGRE.h"
#include "NeoOld.h"

using namespace NEOGEN;
using namespace Rogue;

#pragma region CREATION AND DESTRUCTION

DebuggerNeoTerrain::DebuggerNeoTerrain() : DebuggerNeoMesh() {
	_debuggerFacesWalkable = DebuggerFacesUPtr(new DebuggerFaces());
	_debuggerFacesWalkable->setMaterialName("DiffuseBlue");
	_renderManager->setParentSceneNode(_debuggerFacesWalkable->getSceneNode(), _sceneNode);

	_debuggerFacesObstacle = DebuggerFacesUPtr(new DebuggerFaces());
	_debuggerFacesObstacle->setMaterialName("DiffuseRed");
	_renderManager->setParentSceneNode(_debuggerFacesObstacle->getSceneNode(), _sceneNode);
}

DebuggerNeoTerrain::~DebuggerNeoTerrain() {
	
}

#pragma endregion

#pragma region GET AND SET

void DebuggerNeoTerrain::setVisible(bool v) {
	DebuggerNeoMesh::setVisible(v);
	
	setVisibleFacesWalkable(v);
	setVisibleFacesObstacle(v);
}

void DebuggerNeoTerrain::setVisibleFacesWalkable(bool v) {
	_debuggerFacesWalkable->setVisible(v);
}

void DebuggerNeoTerrain::setVisibleFacesObstacle(bool v) {
	_debuggerFacesObstacle->setVisible(v);
}

void DebuggerNeoTerrain::clear() {
	DebuggerNeoMesh::clear();
	
	_debuggerFacesWalkable->clear();
	_debuggerFacesObstacle->clear();
}

#pragma endregion

#pragma region UPDATE

void DebuggerNeoTerrain::update(NeoTerrain* terrain) {
	DebuggerNeoMesh::update(terrain);
}

void DebuggerNeoTerrain::updateDebuggers() {
	DebuggerNeoMesh::updateDebuggers();

	_debuggerFacesWalkable->update();
	_debuggerFacesObstacle->update();
}

void DebuggerNeoTerrain::refreshDebuggerFaces(NeoFace* face)  {
	if (face->isTypeWalkable()) _debuggerFacesWalkable->addTriangles(face->getTriangles());
	else if (face->isTypeObstacle()) _debuggerFacesObstacle->addTriangles(face->getTriangles());
	else _debuggerFaces->addTriangles(face->getTriangles());
}

#pragma endregion