#include "SceneNavMeshGenerator.h"

#include "NeoOGRE.h"
#include "NeoTypes.h"
#include "NeoMesh.h"
#include "NeoNavMesh.h"
#include "NeoPoly2Tri.h"

#include "DebuggerNeoTerrain.h"
#include "DebuggerNeoNavMesh.h"

#include <stdlib.h>
#include <tiny_obj_loader.h>

using namespace Rogue;
using namespace NEOGEN;

using namespace std;

#pragma region CREATION AND DESTRUCTION

static NEOGEN::Vector3 DEBUG_NAVMESH_OFFSET = NEOGEN::Vector3(0, 0.1, 0);

#pragma region CREATION AND DESTRUCTION

SceneNavMeshGenerator::SceneNavMeshGenerator() {
	_state = State::IDLE;
	_model = NULL;
	_terrain = NULL;

	_terrainDebugger = DebuggerNeoTerrainUPtr(new DebuggerNeoTerrain());
	_navMeshDebugger = DebuggerNeoNavMeshUPtr(new DebuggerNeoNavMesh());
	_navMeshGeneratorDebugger = DebuggerNeoNavMeshGeneratorUPtr(new DebuggerNeoNavMeshGenerator());

	_timer.reset();
	_timeGlobal = 0;
}

SceneNavMeshGenerator::~SceneNavMeshGenerator() {
	
}

void SceneNavMeshGenerator::createScene() {
	_renderManager->setAmbientLightColor(Color(0.25f, 0.25f, 0.25f));
	Light* directionalLight = _renderManager->createLight("directionalLight");
    directionalLight->setType(Ogre::Light::LT_DIRECTIONAL);
    directionalLight->setDiffuseColour(Color(1.0f, 1.0f, 1.0f));
    directionalLight->setSpecularColour(Color(0.0f, 0.0f, 0.0f));
	directionalLight->setDirection(Ogre::Vector3( 0, -1, -1));
	_renderManager->setRenderMode(RENDER_MODE_FORWARD);

	//_renderManager->getRenderWindow()->setHidden(true);

	_cameraManager->getCamera()->setFarClipDistance(1000.0f);

	_gui = _guiManager->createGUI<NeoGUISceneNavMeshGenerator>("GUINavMeshGenerator");

	if (__argc > 1) {
		std::string iFileName = std::string(__argv[1]);
		std::string baseName, extension, path;
		Ogre::StringUtil::splitFullFilename(iFileName, baseName, extension, path);
		if (loadTerrainModel(path, baseName, extension)) {
			createTerrain();
			applySlopeConstraint();
			applyHeightConstraint();
			initNavMesh();
			applyConvexityRelaxation();
			breakRealNotches();
			createCells();
		}
		G_SCENE_STATE = SCENE_NULL;
	}
}

#pragma endregion

#pragma region MODEL MANAGEMENT

void SceneNavMeshGenerator::loadTerrainModel() {
	if (_inputManager->isPressedKey(KeyCode::KC_O)) {
		InterfaceGUIManager::FileTypeList ftList;
		ftList.push_back(InterfaceGUIManager::FileType(L"OGRE Mesh", L"*.mesh"));
		ftList.push_back(InterfaceGUIManager::FileType(L"Wavefront Object", L"*.obj"));
		ftList.push_back(InterfaceGUIManager::FileType(L"Poly2Tri test files", L"*.dat"));
		ftList.push_back(InterfaceGUIManager::FileType(L"Boundary Mesh files", L"*.bdm"));

		const String fileName = _guiManager->openFile(ftList);
		Ogre::LogManager::getSingletonPtr()->logMessage("fileName = " + fileName);
		
		if (!fileName.empty()) {
			std::string baseName, extension, path;
			Ogre::StringUtil::splitFullFilename(fileName, baseName, extension, path);
			/*Ogre::LogManager::getSingletonPtr()->logMessage("fileName = " + fileName);
			Ogre::LogManager::getSingletonPtr()->logMessage("baseName = " + baseName);
			Ogre::LogManager::getSingletonPtr()->logMessage("extension = " + extension);
			Ogre::LogManager::getSingletonPtr()->logMessage("path = " + path);*/
			if (loadTerrainModel(path, baseName, extension)) setState(State::TERRAIN_MODEL_LOADED);
		}
	}
}

bool SceneNavMeshGenerator::loadTerrainModel(const std::string& path, const std::string& baseName, const std::string& extension) {
	Entity* ent = importModel(path, baseName, extension);
	if (!ent) return false;

	SceneNode* node = _renderManager->createSceneNode();
	Renderer* renderer = new Renderer(ent, node);

	if (_model != NULL) destroyGameObject(_model);

	//Create the GameObject
	_model = createGameObject();
	_model->setTag("TerrainModel");
	_model->addComponent(renderer);
	renderer->setMaterialName("DiffuseGrey");

	return true;
}

Entity* SceneNavMeshGenerator::importModel(const std::string& path, const std::string& baseName, const std::string& extension) {
	if (extension == "mesh") return _renderManager->createEntity(baseName + "." + extension);
	else if (extension == "obj") return importModelOBJ(path, baseName, extension);
	else if (extension == "dat") return importModelDAT(path, baseName, extension);
	else if (extension == "bdm") return importModelBDM(path, baseName, extension);

	return NULL;
}

#pragma region IMPORT MODEL OBJ

Entity* SceneNavMeshGenerator::importModelOBJ(const std::string& path, const std::string& baseName, const std::string& extension) {
	typedef std::vector<tinyobj::shape_t> ShapesVector;
	typedef std::vector<tinyobj::material_t> MaterialsVector;
	
	std::string inputfile = path + baseName + "." + extension;
	ShapesVector shapes;
	MaterialsVector materials;

	std::string err = tinyobj::LoadObj(shapes, materials, inputfile.c_str());

	if (!err.empty()) {
		Ogre::LogManager::getSingletonPtr()->logMessage("[importModelOBJ]: " + err);
		return NULL;
	}

	//Convert the imported data into an OGRE mesh. 
	ManualObject* mObject = _renderManager->createManualObject();
	mObject->begin("DiffuseGrey");

	//Define the vertices
	for (ShapesVector::iterator itShapes = shapes.begin(); itShapes != shapes.end(); itShapes++) {
		tinyobj::mesh_t& mesh = itShapes->mesh;
		assert((mesh.positions.size() % 3) == 0);

		//Define the vertices
		for (size_t i = 0; i < mesh.positions.size(); i += 3) {
			mObject->position(mesh.positions[i + 0], mesh.positions[i + 1], mesh.positions[i + 2]);
		}
	}

	//Define the indices
	size_t indexOffset = 0;
	for (ShapesVector::iterator itShapes = shapes.begin(); itShapes != shapes.end(); itShapes++) {
		tinyobj::mesh_t& mesh = itShapes->mesh;
		assert((mesh.indices.size() % 3) == 0);

		//Define the indices
		for (size_t i = 0; i < mesh.indices.size(); i+= 3) {
			mObject->index(indexOffset + mesh.indices[i + 0]);
			mObject->index(indexOffset + mesh.indices[i + 1]);
			mObject->index(indexOffset + mesh.indices[i + 2]);
		}

		indexOffset += (mesh.positions.size() / 3);
	}
	
	mObject->end();
	mObject->convertToMesh("__ManualTerrain__");

	return _renderManager->createEntity("__ManualTerrain__");
}

#pragma endregion

#pragma region IMPORT MODEL DAT

Entity* SceneNavMeshGenerator::importModelDAT(const std::string& path, const std::string& baseName, const std::string& extension) {
	string inputfile = path + baseName + "." + extension;
	string line;
	ifstream file(inputfile);

	//1) Parse the input file
	size_t index = 0;
	Point2IndexMap map;
	Polygon2 polygon;
	if (file.is_open()) {
		while (!file.eof()) {
			getline(file, line);
			if (line.size() == 0) break;

			istringstream iss(line);
			vector<string> tokens;
			copy(istream_iterator<string>(iss), istream_iterator<string>(), back_inserter<vector<string>>(tokens));
			double x = stod(tokens[0]);
			double y = stod(tokens[1]);
			Vector2 key(x, y);
			if (map.find(key) == map.end()) {
				map[key] = index;
				polygon.push_back(key);
				index++;
			}
		}
		file.close();
	}

	//2) Triangulate it. 
	Polygons2 triangles;
	NeoPoly2Tri::triangulate(polygon, triangles);
	
	//Convert the imported data into an OGRE mesh. 
	ManualObject* mObject = _renderManager->createManualObject();
	mObject->begin("DiffuseGrey");

	//Define the vertices
	//for (ShapesVector::iterator itShapes = shapes.begin(); itShapes != shapes.end(); itShapes++) {
	for (Polygon2::iterator it = polygon.begin(); it != polygon.end(); it++) {
		mObject->position((*it).getX(), 0, -(*it).getY());
	}

	//Define the indices
	size_t indexOffset = 0;
	for (Polygons2::iterator it = triangles.begin(); it != triangles.end(); it++) {
		Polygon2& t = *it;
		assert(t.size() == 3);

		//Define the indices
		for (size_t i = 0; i < 3; i++) mObject->index(map[t[i]]);
	}

	mObject->end();
	mObject->convertToMesh("__ManualTerrain__");

	return _renderManager->createEntity("__ManualTerrain__");
}

#pragma endregion

#pragma region IMPORT MODEL BDM

Entity* SceneNavMeshGenerator::importModelBDM(const std::string& path, const std::string& baseName, const std::string& extension) {
	string inputfile = path + baseName + "." + extension;
	string line;
	ifstream file(inputfile);

	Polygons2 polygons;
	size_t index = 0;
	Point2IndexMap map;

	for (;;) {
		string result = "\0";
		if (readlineBDM(file, result))  break;

		size_t nVertices = atoi(result.c_str());
		if (nVertices == 0) break;
		
		Polygon2 p;
		for (unsigned int j = 0; j < nVertices; j++) {
			if (readlineBDM(file, result) && j != nVertices) {
				cout << nVertices - j << " vertices are missed in contour ";
				file.close();
				exit(1);
			}

			double x = (double)atof(result.c_str());
			int nfields = countfieldBDM(result, 0x20);
			if (nfields<2) {
				cout << "Not enough data for vertex:" << j + 1 << "(" << x << ", ?)";
				file.close();
				exit(1);
			}

			string denstr = findfieldBDM(result);
			double y = (double)atof(denstr.c_str());

			Vector2 key(x, y);
			map[key] = index;
			p.push_back(key);
			index++;
		}

		polygons.push_back(p);
	}

	//2) Triangulate it. 
	Polygons2 triangles;
	Polygon2 floor = polygons[0];
	Polygons2 holes;
	for (size_t i = 1; i < polygons.size(); i++) holes.push_back(polygons[i]);

	NeoPoly2Tri::triangulate(floor, holes, triangles);

	//Convert the imported data into an OGRE mesh. 
	ManualObject* mObject = _renderManager->createManualObject();
	mObject->begin("DiffuseGrey");

	//Define the vertices
	for (Polygons2::iterator it = polygons.begin(); it != polygons.end(); it++) {
		Polygon2 p = *it;
		for (Polygon2::iterator itPoint = p.begin(); itPoint != p.end(); itPoint++) {
			mObject->position((*itPoint).getX(), 0, -(*itPoint).getY());
		}
	}

	//Define the indices
	size_t indexOffset = 0;
	for (Polygons2::iterator it = triangles.begin(); it != triangles.end(); it++) {
		Polygon2& t = *it;
		assert(t.size() == 3);

		//Define the indices
		for (size_t i = 0; i < 3; i++) mObject->index(map[t[i]]);
	}

	mObject->end();
	mObject->convertToMesh("__ManualTerrain__");

	return _renderManager->createEntity("__ManualTerrain__");
}

bool SceneNavMeshGenerator::readlineBDM(ifstream& file, string& result) {
	bool endofile = false;

	//Only read something that looks like a number. */
	for (;;) {
		getline(file, result);
		if (endofile = file.eof()) { return endofile; }
		if (countfieldBDM(result, 0x20) == 0) continue;
		int i = 0;
		while (result[i] == 0x20) i++;
		if (result[i]>'9' || (result[i]<'0' && result[i] != '-')) continue;
		else  { return endofile = false; }
	}
}

int SceneNavMeshGenerator::countfieldBDM(const string& source, char delim) {
	int count = 0;
	int size = source.size();

	for (int i = 0; i<size; i++) {
		if (source[i] == delim) continue;
		else {
			count++;
			do{
				i++;
				if (i >= size) break;
			} while (source[i] != delim);
		}
	}

	return count;
}

string SceneNavMeshGenerator::findfieldBDM(string& source) {
	string denstr;
	int i = 0;

	//skip whitespace characters before the string
	while ((source[i] == '\0') || (source[i] == ' ')
		|| (source[i] == '\t')) i++;

	//Skip the current field.  Stop upon reaching whitespace.
	while ((source[i] != '\0') && (source[i] != '#')
		&& (source[i] != ' ') && (source[i] != '\t')) i++;

	//Now skip the whitespace and anything else that doesn't look like a
	//number, a comment, or the end of a line.
	while ((source[i] != 0x20) && (source[i] != '#')
		&& (source[i] != '.') && (source[i] != '+') && (source[i] != '-')
		&& ((source[i] < '0') || (source[i] > '9'))) i++;

	//Check for a comment (prefixed with `#').
	if (source[i] == '#') source[i] = '\0';

	denstr = source.substr(i);
	return denstr;
}

#pragma endregion

void SceneNavMeshGenerator::createTerrain() {
	unsigned long before = _timer.getMilliseconds();

	NEOGEN::Vector3List vertexList;
	std::vector<unsigned long> triangleList;
	NeoOGRE::getMeshInformation(_model->getComponent<Renderer>()->getMesh()->getMesh(), vertexList, triangleList);
	_terrain = NeoTerrainSPtr(new NeoTerrain(vertexList, triangleList));
	//_terrain->simplify();
	
	unsigned long elapsedTime = _timer.getMilliseconds() - before;
	_timeGlobal += elapsedTime;
	Ogre::LogManager::getSingletonPtr()->logMessage("ELAPSED TIME CREATE TERRAIN = " + std::to_string(elapsedTime));

	_terrainDebugger->update(_terrain.get());
	_model->getComponent<Renderer>()->setVisible(false);

	//_terrain->exportOBJ("00_terrain.obj");

	setState(State::TERRAIN_CREATED);
}

#pragma endregion

#pragma region GET AND SET

void SceneNavMeshGenerator::setState(State newState) {
	if (_state == newState) return;

	_state = newState;
}

void SceneNavMeshGenerator::setCameraTopView() {
	if (!_terrain) return;

	setCameraView(Ogre::Vector3(0, 1, 0) * 10, Ogre::Quaternion(float(SQRT_ONE_HALF), float(-SQRT_ONE_HALF), 0.0f, 0.0f));
}

void SceneNavMeshGenerator::setCameraFrontView() {
	if (!_terrain) return;

	setCameraView(Ogre::Vector3(0, 0, 1) * 10, Ogre::Quaternion::IDENTITY);
}

void SceneNavMeshGenerator::setCameraRightView() {
	if (!_terrain) return;

	setCameraView(Ogre::Vector3(1, 0, 0) * 10, Ogre::Quaternion(float(SQRT_ONE_HALF), 0.0f, float(SQRT_ONE_HALF), 0.0f));
}

void SceneNavMeshGenerator::setCameraView(const Ogre::Vector3& centerOffset, const Ogre::Quaternion& rot) {
	const NeoAABB& aabb = _terrain->getNeoAABB();
	const Ogre::Vector3 center = NeoOGRE::toOGRE(aabb.getCenter());
	Camera* cam = _cameraManager->getCamera();
	cam->setPosition(center + centerOffset);
	cam->setFixedYawAxis(false);
	cam->setOrientation(rot);
}

#pragma endregion

#pragma region UPDATE

void SceneNavMeshGenerator::update() {
	if (
		_inputManager->isTriggeredKey(OIS::KC_ESCAPE) || 
		(_inputManager->isPressedKey(OIS::KC_LMENU) && _inputManager->isTriggeredKey(OIS::KC_F4))) 
	{
		G_SCENE_STATE = SCENE_NULL;
	}

	if (_inputManager->isPressedKey(KeyCode::KC_NUMPAD7)) setCameraTopView();
	else if (_inputManager->isPressedKey(KeyCode::KC_NUMPAD1)) setCameraFrontView();
	else if (_inputManager->isPressedKey(KeyCode::KC_NUMPAD3))setCameraRightView();
	else if (_inputManager->isTriggeredKey(OIS::KC_F12)) {
		int currentGUIMode = Core::getManager<InterfaceGUIManager>()->getGUIMode();
		int newGUIMode = (currentGUIMode + 1) % InterfaceGUIManager::GUI_NUM_MODES;
		if (newGUIMode == InterfaceGUIManager::GUI_MODE_SHOW_ALL) _guiManager->showAll();
		else if (newGUIMode == InterfaceGUIManager::GUI_MODE_HIDE_ALL_BUT_MOUSE) _guiManager->hideAllButMouse();
		else if (newGUIMode == InterfaceGUIManager::GUI_MODE_HIDE_ALL) _guiManager->hideAll();
	}
	
	if (_state == State::IDLE) loadTerrainModel();
	if (_inputManager->isTriggeredKey(OIS::KC_B)) {
		if (_state == State::TERRAIN_MODEL_LOADED) createTerrain();
		else if (_state == State::TERRAIN_CREATED) applySlopeConstraint();
		else if (_state == State::SLOPE_CONSTRAINT_APPLIED) applyHeightConstraint();
		else if (_state == State::CEIL_CONSTRAINTS_APPLIED) initNavMesh();
		else if (_state == State::NAV_MESH_INITIALIZED) applyConvexityRelaxation();
		else if (_state == State::CONVEXITY_RELAXATION_APPLIED) breakRealNotches();
		else if (_state == State::PORTALS_CREATED) createCells();
	}

	updateDebug();
}

void SceneNavMeshGenerator::updateDebug() {
	bool debugTerrain = _gui->isDebugTerrain();
	_terrainDebugger->setVisible(debugTerrain);
	_terrainDebugger->setVisibleNormals(debugTerrain && _gui->isDebugTerrainNormals());
	_terrainDebugger->setVisibleEdges(debugTerrain && _gui->isDebugTerrainEdges());
	_terrainDebugger->setVisibleOctree(debugTerrain && _gui->isDebugTerrainOctree());

	bool debugNavMesh = _gui->isDebugNavMesh();
	bool debugCells = ((_state == State::END) && _gui->isDebugNavMeshCells());
	_navMeshDebugger->setVisible(debugNavMesh);
	_navMeshDebugger->setVisibleEdges(debugNavMesh && _gui->isDebugNavMeshEdges());
	_navMeshGeneratorDebugger->setVisible(debugNavMesh && _gui->isDebugNavMeshNotches());
	_navMeshDebugger->setVisibleCells(debugNavMesh && debugCells);
	_navMeshDebugger->setVisiblePortals(debugNavMesh && !debugCells);
	_navMeshDebugger->setVisibleFaces(debugNavMesh && !debugCells);
	_navMeshDebugger->setVisibleOctree(debugNavMesh && _gui->isDebugNavMeshOctree());

	if (_inputManager->isTriggeredKey(OIS::KC_F1)) {
		_navMeshDebugger->setDebugMode(DebuggerNeoNavMesh::NEOGEN_3D);
		_navMeshDebugger->update(_navMesh.get(), DEBUG_NAVMESH_OFFSET);
	}
	if (_inputManager->isTriggeredKey(OIS::KC_F2)) {
		_navMeshDebugger->setDebugMode(DebuggerNeoNavMesh::NEOGEN_ML);
		_navMeshDebugger->update(_navMesh.get(), DEBUG_NAVMESH_OFFSET);
	}
}

void SceneNavMeshGenerator::applySlopeConstraint() {
	unsigned long before = _timer.getMilliseconds();
	
	////COMPARISON PAPER CODE BEGIN
	//NeoReal maxSlope = (__argc == 4) ? NeoReal(std::stod(std::string(__argv[3]))) : NeoReal(90);
	//_terrain->applySlopeConstraint(maxSlope);
	////COMPARISON PAPER CODE END
	
	_terrain->applySlopeConstraint(_gui->getMaxSlope());
	/*_terrain->applySlopeConstraint(NeoReal(90.0));*/
			
	unsigned long elapsedTime = _timer.getMilliseconds() - before;
	_timeGlobal += elapsedTime;
	Ogre::LogManager::getSingletonPtr()->logMessage("ELAPSED TIME SLOPE CONSTRAINT = " + std::to_string(elapsedTime));
		
	_terrainDebugger->update(_terrain.get());
	setState(State::SLOPE_CONSTRAINT_APPLIED);
}

void SceneNavMeshGenerator::applyHeightConstraint() {
	unsigned long before = _timer.getMilliseconds();
		
	_terrain->applyCeilConstraints(_gui->getHeight());
		
	unsigned long elapsedTime = _timer.getMilliseconds() - before;
	_timeGlobal += elapsedTime;
	Ogre::LogManager::getSingletonPtr()->logMessage("ELAPSED TIME CEIL CONSTRAINTS = " + std::to_string(elapsedTime));

	_terrainDebugger->update(_terrain.get());
	setState(State::CEIL_CONSTRAINTS_APPLIED);
}

void SceneNavMeshGenerator::initNavMesh() {
	_navMeshGenerator = NeoNavMeshGeneratorSPtr(new NeoNavMeshGenerator());

	unsigned long before = _timer.getMilliseconds();

	_navMesh = _navMeshGenerator->initNavMesh(_terrain.get());
	_oldConvexityRelaxation = NeoReal(_gui->getConvexityRelaxation());
	_navMeshGenerator->applyConvexityRelaxation(_oldConvexityRelaxation);

	unsigned long elapsedTime = _timer.getMilliseconds() - before;
	_timeGlobal += elapsedTime;
	Ogre::LogManager::getSingletonPtr()->logMessage("ELAPSED TIME INIT NAVMESH = " + std::to_string(elapsedTime));
		
	_navMeshDebugger->update(_navMesh.get(), DEBUG_NAVMESH_OFFSET);
	_navMeshGeneratorDebugger->update(_navMeshGenerator.get(), DEBUG_NAVMESH_OFFSET);

	setState(State::NAV_MESH_INITIALIZED);
}

void SceneNavMeshGenerator::applyConvexityRelaxation() {
	NeoReal cr = _gui->getConvexityRelaxation();
	if (!Math::equal(cr, _oldConvexityRelaxation, NeoReal(0.005))) {

		unsigned long before = _timer.getMilliseconds();

		_navMeshGenerator->applyConvexityRelaxation(cr);

		unsigned long elapsedTime = _timer.getMilliseconds() - before;
		_timeGlobal += elapsedTime;
		Ogre::LogManager::getSingletonPtr()->logMessage("ELAPSED TIME APPLY CONVEXITY RELAXATION = " + std::to_string(elapsedTime));

		_oldConvexityRelaxation = cr;

		_navMeshGeneratorDebugger->update(_navMeshGenerator.get(), DEBUG_NAVMESH_OFFSET);
	}
	else {
		//Convexity Relaxation level confirmed. Change to next step. 
		setState(State::CONVEXITY_RELAXATION_APPLIED);
	}
}

void SceneNavMeshGenerator::breakRealNotches() {
	unsigned long before = _timer.getMilliseconds();
		
	_navMeshGenerator->breakRealNotches();

	unsigned long elapsedTime = _timer.getMilliseconds() - before;
	_timeGlobal += elapsedTime;
	Ogre::LogManager::getSingletonPtr()->logMessage("ELAPSED TIME PORTALS CREATION = " + std::to_string(elapsedTime));

	//Manage the debuggers
	_navMeshDebugger->update(_navMesh.get(), DEBUG_NAVMESH_OFFSET);
	_navMeshGeneratorDebugger->update(_navMeshGenerator.get(), DEBUG_NAVMESH_OFFSET);

	setState(State::PORTALS_CREATED);
}

void SceneNavMeshGenerator::createCells() {
	unsigned long before = _timer.getMilliseconds();

	_navMeshGenerator->createCells();

	unsigned long elapsedTime = _timer.getMilliseconds() - before;
	_timeGlobal += elapsedTime;
	Ogre::LogManager::getSingletonPtr()->logMessage("ELAPSED TIME CELLS CREATION = " + std::to_string(elapsedTime));

	//Update the debuggers
	_navMeshDebugger->update(_navMesh.get(), DEBUG_NAVMESH_OFFSET);

	Ogre::LogManager::getSingletonPtr()->logMessage("TOTAL TIME = " + std::to_string(_timeGlobal));

	Ogre::LogManager* logManager = Ogre::LogManager::getSingletonPtr();
	size_t numVertices = _terrain->getNumVertices();
	NeoReal cRel = NeoReal(_gui->getConvexityRelaxation());
	size_t numNotchesStrict = _navMeshGenerator->getNotchesStrict().size();
	size_t numNotchesReal = _navMeshGenerator->getNotchesReal().size();
	size_t numCells = _navMesh->getNumCells();
	size_t numPortals = _navMesh->getNumPortals();
		
	logManager->logMessage("===================================================");
	logManager->logMessage("GENERATION RESULTS");
	logManager->logMessage("===================================================");
	logManager->logMessage("Num Original Vertices = " + std::to_string(numVertices));
	logManager->logMessage("Convexity Relaxation = " + std::to_string(cRel));
	logManager->logMessage("Num Notches Strict = " + std::to_string(numNotchesStrict));
	logManager->logMessage("Num Notches Real = " + std::to_string(numNotchesReal));
	logManager->logMessage("Notch Reduction Factor = " + std::to_string(NeoReal(numNotchesReal) / NeoReal(numNotchesStrict)));
	logManager->logMessage("Num Portals = " + std::to_string(numPortals));
	logManager->logMessage("Num Cells = " + std::to_string(numCells));
	logManager->logMessage("Portals per Notch = " + std::to_string(NeoReal(numPortals) / NeoReal(numNotchesReal)));
	logManager->logMessage("Cells per Notch = " + std::to_string(NeoReal(numCells) / NeoReal(numNotchesReal)));

	std::string oFileName = (__argc >= 3) ? std::string(__argv[2]) : "01_navMesh.obj";
	_navMesh->exportOBJ(oFileName);
	saveStats(oFileName + ".stat");
	
	setState(State::END);
}

void SceneNavMeshGenerator::saveStats(const std::string& path) {
	std::ofstream file(path, std::ofstream::out | std::ofstream::trunc);
	file << "Construction time (ms): " << _timeGlobal << std::endl;
	file.close();
}

#pragma endregion