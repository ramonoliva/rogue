#include "NeoDeferredPasses.h"
#include "ngGlobalVars.h"

using namespace Rogue;
using namespace NEOGEN;

/****************************************************************/
/* FILL LAYER TEXTURE */
/****************************************************************/
static const Ogre::String LEVEL_TEX_NAME = "levelTex";

DPFillLayerTexture::DPFillLayerTexture(size_t w, size_t h) {
	_renderQuadVisible = false;
	_materialName = "";

	//Create the Render Targets and other stuff
	Ogre::PixelFormat pFormat = Ogre::PF_R8G8B8A8;
	_levelRenderTex = _renderManager->createRenderTexture(LEVEL_TEX_NAME, w, h, pFormat);
	addRenderTarget(_levelRenderTex);
	
	_auxTex = _renderManager->createTexture("cmAuxTex0", w, h, pFormat, Ogre::TU_DEFAULT);
}

void DPFillLayerTexture::postRender() {
	DeferredPass::postRender();
	
	TexturePtr cmTex = _renderManager->getTexture(LEVEL_TEX_NAME);
	cmTex->copyToTexture(_auxTex);
	if (DEBUG_MODE) {
		Image img0;
		cmTex->convertToImage(img0);
		Ogre::ColourValue color;
		for (size_t x = 0; x < img0.getWidth(); x++) {
			for (size_t y = 0; y < img0.getHeight(); y++) {
				color = img0.getColourAt(x, y, 0);
				color.a = 1.0f;
				img0.setColourAt(color, x, y, 0);
			}
		}
		img0.save("cmTex0.png");
		_levelRenderTex->writeContentsToFile("cmTex0.jpg");
		_levelRenderTex->writeContentsToFile("cmTex0Transp.png");
	}
}

/****************************************************************/
/* PORTAL EXPANSION */
/****************************************************************/
DPPortalExpansion::DPPortalExpansion() {
	_renderQuadVisible = true;
	_materialName = "matPortalExpansion";

	//Create the render targets and other stuff
	_levelRenderTex = _renderManager->getRenderTexture(LEVEL_TEX_NAME);
	addRenderTarget(_levelRenderTex);
}

void DPPortalExpansion::postRender() {
	DeferredPass::postRender();

	TexturePtr cmTex = _renderManager->getTexture(LEVEL_TEX_NAME);
	if (DEBUG_MODE) {
		Ogre::Image img1;
		cmTex->convertToImage(img1);
		Ogre::ColourValue color;
		for (size_t x = 0; x < img1.getWidth(); x++) {
			for (size_t y = 0; y < img1.getHeight(); y++) {
				color = img1.getColourAt(x, y, 0);
				color.a = 1.0f;
				img1.setColourAt(color, x, y, 0);
			}
		}
		img1.save("cmTex1.png");
		_levelRenderTex->writeContentsToFile("cmTex1.jpg");
		_levelRenderTex->writeContentsToFile("cmTex1Transp.png");
	}
}

/****************************************************************/
/* LEVEL REFINEMENT */
/****************************************************************/
DPLevelRefinement::DPLevelRefinement(size_t w, size_t h) {
	_renderQuadVisible = false;
	_materialName = "";
	
	//Create the render targets and other stuff
	//_normalDepthMapRenderTex = _renderManager->createRenderTexture("normalDepthMapTex", w, h, Ogre::PF_FLOAT32_RGBA, Ogre::ColourValue::Black);
	_normalDepthMapRenderTex = _renderManager->createRenderTexture("normalDepthMapTex", w, h, Ogre::PF_R8G8B8A8, Ogre::ColourValue::Black);
	addRenderTarget(_normalDepthMapRenderTex);
	
	_intermediateTex0 = _renderManager->createTexture("intermediateTex0", w, h, Ogre::PF_R8G8B8A8, Ogre::TU_DEFAULT);
}

void DPLevelRefinement::postRender() {
	TexturePtr normalDepthMapTex = _renderManager->getTexture("normalDepthMapTex");
	normalDepthMapTex->copyToTexture(_intermediateTex0);	//Guardo el mapa de normals i profunditat inicial
	if (DEBUG_MODE) {
		_normalDepthMapRenderTex->writeContentsToFile("test00normalDepthMap.png");
		Image imgBaseDM;
		_intermediateTex0->convertToImage(imgBaseDM);
		Ogre::LogManager::getSingletonPtr()->logMessage("DEPTH AFTER REFINEMENT = " + std::to_string(imgBaseDM.getColourAt(2, 2, 0).a));
		Ogre::LogManager::getSingletonPtr()->logMessage("depthTest = " + std::to_string(imgBaseDM.getColourAt(2, 2, 0).b));
		NEOGEN::imageToDepthMap(imgBaseDM);
		imgBaseDM.save("intermediateTex0.png");
	}
}

/****************************************************************/
/* LEVEL REFINEMENT PERPENDICULAR */
/****************************************************************/
DPLevelRefinementPerpendicular::DPLevelRefinementPerpendicular(size_t w, size_t h) {
	_renderQuadVisible = false;
	_materialName = "";

	//Create the render targets and other stuff
	_normalDepthMapRenderTex = _renderManager->getRenderTexture("normalDepthMapTex");
	addRenderTarget(_normalDepthMapRenderTex);

	_intermediateTex1 = _renderManager->createTexture("intermediateTex1", w, h, Ogre::PF_R8G8B8A8, Ogre::TU_DEFAULT);
}

void DPLevelRefinementPerpendicular::postRender() {
	TexturePtr normalDepthMapTex = _renderManager->getTexture("normalDepthMapTex");
	normalDepthMapTex->copyToTexture(_intermediateTex1);	//Guardo el mapa de profunditat de les cares perpendiculars
	if (DEBUG_MODE) {
		_normalDepthMapRenderTex->writeContentsToFile("test01perpendicularFaces.png");
		Image imgPerpDM;
		_intermediateTex1->convertToImage(imgPerpDM);
		NEOGEN::imageToDepthMap(imgPerpDM);
		imgPerpDM.save("intermediateTex1.png");
	}
}

/****************************************************************/
/* NORMAL DEPTH MAP FUSION */
/****************************************************************/
DPNormalDepthMapFusion::DPNormalDepthMapFusion() {
	_renderQuadVisible = true;
	_materialName = "matNormalDepthMapFusion";
	
	//Create the render targets and other stuff
	_normalDepthMapRenderTex = _renderManager->getRenderTexture("normalDepthMapTex");
	addRenderTarget(_normalDepthMapRenderTex);

	_intermediateTex0 = _renderManager->getTexture("intermediateTex0");
}

void DPNormalDepthMapFusion::postRender() {
	TexturePtr normalDepthMapTex = _renderManager->getTexture("normalDepthMapTex");
	normalDepthMapTex->copyToTexture(_intermediateTex0);	//Guardo el mapa fusionat
	if (DEBUG_MODE) {
		_normalDepthMapRenderTex->writeContentsToFile("test02normalDepthMapFusion.png");
		Ogre::Image imgFusionDM;
		normalDepthMapTex->convertToImage(imgFusionDM);
		Ogre::LogManager::getSingletonPtr()->logMessage("DEPTH AFTER FUSION = " + std::to_string(imgFusionDM.getColourAt(2, 2, 0).a));
		NEOGEN::imageToDepthMap(imgFusionDM);
		imgFusionDM.save("intermediateTex2.png");
	}
}

/****************************************************************/
/* CONTOUR DETECTION */
/****************************************************************/
DPContourDetection::DPContourDetection(size_t w, size_t h) {
	_renderQuadVisible = true;
	_materialName = "matContourDetection";

	//Create the render targets and other stuff
	_contourRenderTex = _renderManager->createRenderTexture("contourTex", w, h, Ogre::PF_R8G8B8A8, Color::Black, true, _renderManager->getDeferredCamera());
	addRenderTarget(_contourRenderTex);
}

void DPContourDetection::postRender() {
	if (DEBUG_MODE) {
		_contourRenderTex->writeContentsToFile("test03obstaclesContour.png");
		Image imgObstacleContours, imgPortals;
		TexturePtr contourTex = _renderManager->getTexture("contourTex");
		contourTex->convertToImage(imgObstacleContours);
		contourTex->convertToImage(imgPortals);
		NEOGEN::imageToContourMap(imgObstacleContours);
		NEOGEN::imageToPortalMap(imgPortals);
		imgObstacleContours.save("intermediateTex3.png");
		imgPortals.save("intermediateTex4.png");
	}
}

