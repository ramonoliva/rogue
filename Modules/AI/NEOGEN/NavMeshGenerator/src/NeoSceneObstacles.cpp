#include "NeoSceneObstacles.h"

using namespace Rogue;
using namespace NEOGEN;

enum CollisionGroup {
	COL_GRID	=	1 << 0,
	COL_VERTEX	=	1 << 1,
	COL_RAY		=	1 << 2, 
};
const int gridCollidesWith = COL_RAY;
const int vertexCollidesWith = COL_RAY;
const int rayCollidesWith = COL_GRID | COL_VERTEX;

const String CURRENT_OBSTACLE_MAT_NAME = "Blue";
const String FLOOR_OBSTACLE_MAT_NAME = "Black";
const String INNER_OBSTACLE_MAT_NAME = "Red";
const String SELECTION_MAT_NAME = "Yellow";

const String TAG_OBSTACLE_VERTEX = "ObstacleVertex";
const String TAG_GRID = "Grid";

const float MAX_VERTEX_RADIUS = 1.0f;
const float MIN_VERTEX_RADIUS = 0.1f;
const float VERTEX_RADIUS_STEP = 0.1f;

#pragma region CREATION AND DESTRUCTION 

void SceneObstacles::createScene() {
	_renderManager->setRenderMode(RENDER_MODE_FORWARD);
	_gui = _guiManager->createGUI<NeoGUISceneObstacles>("GUISceneObstacles");
	_guiManager->showFrameStats(false);
	MapParser::getSingleton();
	clearSelection();

	createNavMeshGenerators();
	createGrid();

	initCamera();
	initObstacles();

	setState(ST_IDLE);
	setGeneratorType(NEOGEN);
	_gui->setGeneratorStep(0);

	_newVertexID = 0;
	_currentVertexRadius = MIN_VERTEX_RADIUS;
}

void SceneObstacles::destroyScene() {
	SceneGame::destroyScene();
	delete _navMeshGenerator;
}

void SceneObstacles::clearScene() {
	_currentObstacleID = 0;
	_newVertexID = 0;
	_obstacles.clear();
	_vertices.clear();
	_portals.clear();
	clearSelection();
	_gui->setGeneratorStep(0);
}

void SceneObstacles::clearManualObjects() {
	_manualPositionToMouse->clear();
	_manualPortals->clear();
	for (ManualObstacleList::iterator it = _manualObstacles.begin(); it != _manualObstacles.end(); it++) {
		(*it)->clear();
	}
}

void SceneObstacles::clearSelection() {
	_selectedVertices.clear();
	_selectedVerticesCenter = Vector3(0,0,0);
	_selectedVerticesScaleInfo.clear();
	_selectedEdges.clear();
}

void SceneObstacles::createNavMeshGenerators() {
	SceneManager* sceneManager = _renderManager->getSceneManager();
	_neogenGenerator = new NavMeshGenerator(sceneManager);
	_manualGenerator = new NavMeshGeneratorManual(sceneManager);
}

void SceneObstacles::createGrid() {
	//Create the plane where the obstacles will be drawn
	//Create the renderer
	ManualObject* manualObject = _renderManager->createManualObject();
	manualObject->begin("LightGrey", Ogre::RenderOperation::OT_LINE_LIST);
	int gridSize = 1000;
	for (int i = 0; i < gridSize; i++) {
		//Draw the vertical line
		manualObject->position(i, 0, 0);
		manualObject->position(i, 0, gridSize);

		//Draw the horizontal line
		manualObject->position(0, 0, i);
		manualObject->position(gridSize, 0, i);
	}
	manualObject->end();
	manualObject->convertToMesh("grid.mesh");
	Entity* ent = _renderManager->createEntity("grid.mesh");
	SceneNode* node = _renderManager->createSceneNode();
	Renderer* renderer = new Renderer(ent, node);

	//Create the rigid body
	CollisionShapePlane* shape = new CollisionShapePlane(Vector3(0,1,0), 0);
	RigidBody* rigidBody = _physicsManager->createRigidBody(shape, 0, COL_GRID, gridCollidesWith);
	rigidBody->setKinematic(true);
	
	//Create the GameObject
	_grid = createGameObject(Vector3(-float(gridSize) / 2.0f, 0.0f, -float(gridSize) / 2.0f));
	_grid->setTag(TAG_GRID);
	_grid->addComponent(rigidBody);
	_grid->addComponent(renderer);
}

void SceneObstacles::initCamera() {
	Camera* mainCamera = _renderManager->getMainCamera();
	mainCamera->setPosition(0, 10, 0);
	Viewport* vp = mainCamera->getViewport();
	vp->setBackgroundColour(Color::White);
	_cameraManager->viewTop();
	_cameraManager->fixOrientation(true);
}

void SceneObstacles::initObstacles() {
	_obstacles.clear();
	_vertices.clear();
	_portals.clear();
	_manualObstacles.clear();

	_manualPositionToMouse = _renderManager->createManualObject();
	SceneNode* node = _renderManager->createSceneNode();
	node->attachObject(_manualPositionToMouse);

	_manualPortals = _renderManager->createManualObject();
	node->attachObject(_manualPortals);

	createObstacle();
}

GameObject* SceneObstacles::createObstacleVertex(size_t obstacleID, size_t vertexID, Vec3D& position, GameObject* nextSibling) {
	//Create the renderer
	Entity* ent = _renderManager->createEntity("Vert.mesh");
	SceneNode* node = _renderManager->createSceneNode();
	Renderer* renderer = new Renderer(ent, node);

	//Create the rigidBody
	CollisionShapeSphere* shape = new CollisionShapeSphere(1.0f);
	RigidBody* rigidBody = _physicsManager->createRigidBody(shape, 0, COL_VERTEX, vertexCollidesWith);
	rigidBody->setKinematic(true);

	//Create the GameObject
	GameObject* parent = _obstacles[obstacleID];
	GameObject* go = createGameObject(Vector3(position.getX(), position.getY(), position.getZ()));
	go->setTag(TAG_OBSTACLE_VERTEX);
	go->setID(vertexID);
	go->addComponent<Renderer>(renderer);
	go->addComponent<RigidBody>(rigidBody);
	go->setScale(Vector3(_currentVertexRadius, _currentVertexRadius, _currentVertexRadius));
	
	//Add the GameObject to the corresponding obstacle
	parent->addChildBefore(go, nextSibling);

	_vertices[vertexID] = go;

	return go;
}

GameObject* SceneObstacles::createObstacle() {
	//Create the GameObject representing the obstacle
	GameObject* go = createGameObject();
	go->setTag("Obstacle");
	_currentObstacleID = _obstacles.size();
	go->setID(_currentObstacleID);
	_obstacles.push_back(go);

	//Create the ManualObject to render the obstacle
	ManualObject* manual = _renderManager->createManualObject();
	SceneNode* node = _renderManager->createSceneNode();
	node->attachObject(manual);
	_manualObstacles.push_back(manual);

	return go;
}

void SceneObstacles::createNavMesh() {
	//1) Convert the GameObjects to the polygon description needed by the NavMeshGenerator
	Poly* pFloor = createPolygon(_obstacles[0]->getChildren());
	if (pFloor == NULL) return;
	generationMode = GENERATION_MODE_FULL;
	std::vector<Poly*> pObstacles;
	for (ObstacleList::iterator it = _obstacles.begin() + 1; it != _obstacles.end(); it++) {
		Poly* pObstacle = createPolygon((*it)->getChildren());
		if (pObstacle != NULL) pObstacles.push_back(pObstacle);
	}

	//2) Convert the predefined portals
	if (_generatorType == NEOGEN) _portals.clear();
	EdgeSetID predefinedPortals;
	for (GameObjectPairSet::iterator it = _portals.begin(); it != _portals.end(); it++) {
		predefinedPortals.insert(VertexPairID(it->first->getID(), it->second->getID()));
	}
	_navMeshGenerator->setPredefinedPortals(predefinedPortals);

	//3) Generate the NavMesh
	_navMeshGenerator->setData(pFloor, pObstacles);
	while (!_navMeshGenerator->generate());
	_navMesh = _navMeshGenerator->getNavMesh();

	//Add the extra vertices that has been created during the NavMesh generation process
	/*selectAllVertices();
	VertTable& vTable = _navMesh->getVertTable();
	for (VertTable::iterator it = vTable.begin(); it != vTable.end(); it++) {
		if (!isSelectedVertex(it->first)) {
			createObstacleVertex(0, _newVertexID++, it->second.getPosition());
		}
	}
	clearSelection();*/
}

Poly* SceneObstacles::createPolygon(GameObjectList& obstacle) {
	if (obstacle.size() < 3) return NULL;	//invalid polygon

	VertexList vList;
	for (GameObjectList::iterator it = obstacle.begin(); it != obstacle.end(); it++) {
		GameObject* vertex = *it;
		Vector3& position = vertex->getTransform().getOrigin();
		Vertex v = Vertex(Vertex(Vec3D(position.getX(), position.getY(), position.getZ())));
		v.setID(vertex->getID());
		vList.push_back(v);
	}
	return new Poly(vList);
}

void SceneObstacles::initScaleInfo() {
	_selectedVerticesScaleInfo.clear();
	for (GameObjectSet::iterator it = _selectedVertices.begin(); it != _selectedVertices.end(); it++) {
		Vector3 centerToVertexDirection = (*it)->getTransform().getOrigin() - _selectedVerticesCenter;
		float centerToVertexDistance = centerToVertexDirection.length();
		centerToVertexDirection.normalize();
		
		_selectedVerticesScaleInfo[*it] = ScaleInfo(centerToVertexDirection, centerToVertexDistance);
	}
}

#pragma endregion

#pragma region GET AND SET

void SceneObstacles::changeGeneratorType() {
	int newType = (int(_generatorType) + 1) % int(NUM_TYPES);
	setGeneratorType(GeneratorType(newType));
}

void SceneObstacles::setGeneratorType(GeneratorType t) {
	_generatorType = t;
	if (_generatorType == NEOGEN) {
		Ogre::LogManager::getSingletonPtr()->logMessage("GENERATOR TYPE NEOGEN");
		_navMeshGenerator = _neogenGenerator;
	}
	else if (_generatorType == MANUAL) {
		Ogre::LogManager::getSingletonPtr()->logMessage("GENERATOR TYPE MANUAL");
		_navMeshGenerator = _manualGenerator;
	}
	_gui->setGeneratorMode(int(_generatorType));
}

void SceneObstacles::setState(State s) {
	if (s == ST_IDLE) {
		if (_state == ST_MOVING_VERTEX) recomputeSelectedVerticesCenter();
		_manualPositionToMouse->clear();
		_gui->setGeneratorStep(0);
	}
	if ((s == ST_MOVING_VERTEX) || (s == ST_SCALING_VERTICES)) {
		RayCastResult result;
		_physicsManager->rayCastCursor(result, _renderManager->getMainCamera(), COL_RAY, COL_GRID);
		if (result.hit) {
			_lastHitPosition = result.position;
		}
		if (s == ST_SCALING_VERTICES) initScaleInfo();
	}
	else if (s == ST_CREATING_PORTAL) {
		clearSelection();
	}
	else if (s == ST_NAVMESH_GENERATED) {
		clearManualObjects();
		//_navMesh->printData();
	}
	_state = s;
}

void SceneObstacles::getObstacleDescription(MapParser::ObstacleDescription& result) {
	for (ObstacleList::iterator it = _obstacles.begin(); it != _obstacles.end(); it++) {
		MapParser::Obstacle obs;
		GameObject* obstacle = *it;
		
		//Now iterate over each vertex of the polygon
		GameObjectList& vertices = obstacle->getChildren();
		if (vertices.size() >= 3) {
			for (GameObjectList::iterator v = vertices.begin(); v != vertices.end(); v++) {
				Vector3 position = (*v)->getTransform().getOrigin();
				MapParser::VertexDescription vDesc((*v)->getID(), Vec3D(position.getX(), position.getY(), position.getZ()));
				obs.push_back(vDesc);
			}
			result.push_back(obs);
		}
	}
}

void SceneObstacles::getObstaclesFromDescription(MapParser::ObstacleDescription& obstacleDescription) {
	for (MapParser::ObstacleDescription::iterator desc = obstacleDescription.begin(); desc != obstacleDescription.end(); desc++) {
		GameObject* go = createObstacle();

		for (MapParser::Obstacle::iterator obsIt = desc->begin(); obsIt != desc->end(); obsIt++) {
			GameObject* goVertex = createObstacleVertex(go->getID(), obsIt->first, obsIt->second);
			goVertex->setID(obsIt->first);
		}
	}
	createObstacle();	//Create an additional empty obstacle that represents the obstacle that we are currently creating
}

void SceneObstacles::getPortalDescription(MapParser::PortalDescription& result) {
	for (GameObjectPairSet::iterator it = _portals.begin(); it != _portals.end(); it++) {
		result.push_back(MapParser::PairInt(it->first->getID(), it->second->getID()));
	}
}

void SceneObstacles::getPortalsFromDescription(MapParser::PortalDescription& portalDescription) {
	for (MapParser::PortalDescription::iterator it = portalDescription.begin(); it != portalDescription.end(); it++) {
		addPortal(_vertices[it->first], _vertices[it->second]);
	}
}

bool SceneObstacles::isSelectedVertex(GameObject* v) {
	GameObjectSet::iterator itFound = _selectedVertices.find(v);
	return (itFound != _selectedVertices.end());
}

bool SceneObstacles::isSelectedVertex(int id) {
	bool found = false;
	for (GameObjectSet::iterator it = _selectedVertices.begin(); it != _selectedVertices.end(); it++) {
		found = ((*it)->getID() == id);
		if (found) break;
	}
	return found;
}

bool SceneObstacles::edgeInList(GameObject* v1, GameObject* v2, GameObjectPairSet& list) {
	GameObjectPairSet::iterator itFound = list.find(GameObjectPair(v1, v2));
	if (itFound != list.end()) return true;

	itFound = list.find(GameObjectPair(v2, v1));
	return (itFound != list.end());
}

bool SceneObstacles::isSelectedEdge(GameObject* v1, GameObject* v2) {
	return edgeInList(v1, v2, _selectedEdges);
}

bool SceneObstacles::isPortalEdge(GameObject* v1, GameObject* v2) {
	return edgeInList(v1, v2, _portals);
}

void SceneObstacles::addSelectedVertex(GameObject* v) {
	//Check if the next or the previous vertex of v in the polygon describing the obstacle is already selected,
	//so the edge is selected.
	if (isSelectedVertex(v)) return;

	GameObject* parent = v->getParent();
	GameObject* nextVertex = parent->getNextSibling(v);
	GameObject* prevVertex = parent->getPrevSibling(v);
	
	if (isSelectedVertex(nextVertex)) addSelectedEdge(v, nextVertex);
	if (isSelectedVertex(prevVertex)) addSelectedEdge(prevVertex, v);

	//Update the selected vertices center
	_selectedVerticesCenter *= _selectedVertices.size();
	_selectedVerticesCenter += v->getTransform().getOrigin();
	_selectedVertices.insert(v);
	_selectedVerticesCenter /= _selectedVertices.size();
}

void SceneObstacles::addSelectedEdge(GameObject* v1, GameObject* v2) {
	if (isSelectedEdge(v1, v2)) return;	//Edge already in the list
	GameObject* parent = v1->getParent();
	if (parent->getNextSibling(v1) == v2) _selectedEdges.insert(GameObjectPair(v1, v2));
	else _selectedEdges.insert(GameObjectPair(v1, v2));
}

void SceneObstacles::addPortal(GameObject* v1, GameObject* v2) {
	if (isPortalEdge(v1, v2)) {
		//Portal already created, delete it. 
		_portals.erase(GameObjectPair(v1, v2));
		_portals.erase(GameObjectPair(v2, v1));
	}
	else {
		_portals.insert(GameObjectPair(v1, v2));
	}
}

void SceneObstacles::selectObstacleVertices(GameObject* obstacle) {
	GameObjectList& children = obstacle->getChildren();
	for (GameObjectList::iterator it = children.begin(); it != children.end(); it++) {
		addSelectedVertex(*it);
	}
}

void SceneObstacles::selectAllVertices() {
	for (ObstacleList::iterator it = _obstacles.begin(); it != _obstacles.end(); it++) {
		selectObstacleVertices(*it);
	}
}

void SceneObstacles::recomputeSelectedVerticesCenter() {
	_selectedVerticesCenter = Vector3(0,0,0);
	for (GameObjectSet::iterator it = _selectedVertices.begin(); it != _selectedVertices.end(); it++) {
		_selectedVerticesCenter += (*it)->getTransform().getOrigin();
	}
	_selectedVerticesCenter /= _selectedVertices.size();
}

#pragma endregion

#pragma region UPDATE

void SceneObstacles::update() {
	SceneGame::update();

	if (_state == ST_MOVING_VERTEX) updateStateMovingVertices();
	else if (_state == ST_SCALING_VERTICES) updateStateScalingVertices();
	else if (_state == ST_CREATING_PORTAL) updateStateCreatingPortal();

	drawPortals();
	if (_navMesh) {
		selectAllVertices();
		for (GameObjectSet::iterator it = _selectedVertices.begin(); it != _selectedVertices.end(); it++) {
			GameObject* vertexGO = *it;
			Vector3& newPos = vertexGO->getTransform().getOrigin();

			Vertex& v = _navMesh->getVertex(vertexGO->getID());
			v.setPosition(Vec3D(newPos.getX(), newPos.getY(), newPos.getZ()));
		}
		_navMesh->drawEdges();
		_navMesh->drawGraph();
	}
	else {
		drawObstacles();
	}
}

void SceneObstacles::updateStateMovingVertices() {
	RayCastResult result;
	_physicsManager->rayCastCursor(result, _renderManager->getMainCamera(), COL_RAY, COL_GRID);
	if (result.hit) {
		Vector3 disp = result.position - _lastHitPosition;
		disp.setY(0.0f);	//Ignore the Y component, as we are moving on the XZ plane
		for (GameObjectSet::iterator it = _selectedVertices.begin(); it != _selectedVertices.end(); it++) {
			(*it)->getTransform().translate(disp);
		}
		_lastHitPosition = result.position;
	}
}

void SceneObstacles::updateStateScalingVertices() {
	RayCastResult result;
	_physicsManager->rayCastCursor(result, _renderManager->getMainCamera(), COL_RAY, COL_GRID);
	float initialDistance = _lastHitPosition.distance(_selectedVerticesCenter);

	if (result.hit) {
		float currentDistance = result.position.distance(_selectedVerticesCenter);
		float scaleFactor = currentDistance / initialDistance;
		scaleSelectedVertices(scaleFactor);

		//Draw a line from the center of the selected vertices to the position of the mouse in the grid
		_manualPositionToMouse->clear();
		_manualPositionToMouse->begin("Magenta", Ogre::RenderOperation::OT_LINE_LIST);
		_manualPositionToMouse->position(Ogre::Vector3(_selectedVerticesCenter.getX(), _selectedVerticesCenter.getY(), _selectedVerticesCenter.getZ()));
		_manualPositionToMouse->position(Ogre::Vector3(result.position.getX(), result.position.getY(), result.position.getZ()));
		_manualPositionToMouse->end();
	}
}

void SceneObstacles::updateStateCreatingPortal() {
	if (_selectedVertices.empty()) return;

	if (_selectedVertices.size() == 2) {
		//Create the portal
		GameObjectSet::iterator it = _selectedVertices.begin();
		GameObject* v1 = *it;
		it++;
		GameObject* v2 = *it;
		
		addPortal(v1, v2);
		_manualPositionToMouse->clear();
		_selectedVertices.clear();
	}
	else {
		RayCastResult result;
		_physicsManager->rayCastCursor(result, _renderManager->getMainCamera(), COL_RAY, COL_GRID);
		float initialDistance = _lastHitPosition.distance(_selectedVerticesCenter);

		if (result.hit) {
			//Draw a line from the selected vertex to the position of the mouse
			Vector3 begin = (*_selectedVertices.begin())->getTransform().getOrigin();
			_manualPositionToMouse->clear();
			_manualPositionToMouse->begin("Green", Ogre::RenderOperation::OT_LINE_LIST);
			_manualPositionToMouse->position(Ogre::Vector3(begin.getX(), begin.getY(), begin.getZ()));
			_manualPositionToMouse->position(Ogre::Vector3(result.position.getX(), result.position.getY(), result.position.getZ()));
			_manualPositionToMouse->end();
		}
	}
}

void SceneObstacles::scaleSelectedVertices(float scaleFactor) {
	for (GameObjectSet::iterator it = _selectedVertices.begin(); it != _selectedVertices.end(); it++) {
		GameObject* vertexGO = *it;
		Vector3 dir = _selectedVerticesScaleInfo[vertexGO].first;
		float d = _selectedVerticesScaleInfo[vertexGO].second;
		vertexGO->getTransform().setOrigin(_selectedVerticesCenter + dir * d * scaleFactor);
	}
}

void SceneObstacles::drawObstacle(size_t obstacleID, bool close) {
	String matName;
	if (obstacleID == _currentObstacleID) matName = CURRENT_OBSTACLE_MAT_NAME;
	else if (obstacleID == 0) matName = FLOOR_OBSTACLE_MAT_NAME;
	else matName = INNER_OBSTACLE_MAT_NAME;

	GameObjectList& obstacle = _obstacles[obstacleID]->getChildren();
	ManualObject* manual = _manualObstacles[obstacleID];
	
	if (!obstacle.empty()) {
		manual->clear();
		manual->begin(matName, Ogre::RenderOperation::OT_LINE_LIST);
		Vector3 begin, end;
		for (GameObjectList::iterator it = obstacle.begin(); *it != obstacle.back(); it++) {
			GameObject* current = *it;
			GameObject* next = current->getParent()->getNextSibling(current);
			if (isSelectedVertex(current)) current->getComponent<Renderer>()->setMaterialName(SELECTION_MAT_NAME);
			else current->getComponent<Renderer>()->setMaterialName(matName);
			begin = current->getTransform().getOrigin();
			end = next->getTransform().getOrigin();
			manual->position(begin.getX(), begin.getY(), begin.getZ());
			manual->position(end.getX(), end.getY(), end.getZ());
		}

		if (close) {
			//It is a complete obstacle. Draw a line joining the first and the last verted
			if (isSelectedVertex(obstacle.back())) obstacle.back()->getComponent<Renderer>()->setMaterialName(SELECTION_MAT_NAME);
			else obstacle.back()->getComponent<Renderer>()->setMaterialName(matName);
			begin = obstacle.back()->getTransform().getOrigin();
			end = obstacle.front()->getTransform().getOrigin();
			manual->position(begin.getX(), begin.getY(), begin.getZ());
			manual->position(end.getX(), end.getY(), end.getZ());
		}
		else {
			//It is the obstacle that we are creating. Draw a line between the last vertex added and the position of the cursor
			RayCastResult result;
			_physicsManager->rayCastCursor(result, _renderManager->getMainCamera());
			if (result.hit) {
				obstacle.back()->getComponent<Renderer>()->getMesh()->setMaterialName(matName);
				begin = obstacle.back()->getTransform().getOrigin();
				end = Vector3(result.position.x(), result.position.y(), result.position.z());

				manual->position(begin.getX(), begin.getY(), begin.getZ());
				manual->position(end.getX(), end.getY(), end.getZ());
			}
		}
		manual->end();
	}
}

void SceneObstacles::drawObstacles() {
	if (_navMesh) return;
	if (_state == ST_CREATING_OBSTACLE) {
		//Draw the obstacle that we are currently creating
		drawObstacle(_currentObstacleID, false);
	}
	//Draw the complete obstacles
	for (size_t i = 0; i < _currentObstacleID; i++) {
		drawObstacle(i, true);
	}
}

void SceneObstacles::drawPortals() {
	_manualPortals->clear();
	_manualPortals->begin("Green", Ogre::RenderOperation::OT_LINE_LIST);
	for (GameObjectPairSet::iterator it = _portals.begin(); it != _portals.end(); it++) {
		Vector3& begin = it->first->getTransform().getOrigin();
		Vector3& end = it->second->getTransform().getOrigin();
		_manualPortals->position(Ogre::Vector3(begin.getX(), begin.getY(), begin.getZ()));
		_manualPortals->position(Ogre::Vector3(end.getX(), end.getY(), end.getZ()));
	}
	_manualPortals->end();
}

void SceneObstacles::subdivideSelectedEdges() {
	GameObjectList newVertices;
	for (GameObjectPairSet::iterator it = _selectedEdges.begin(); it != _selectedEdges.end(); it++) {
		GameObject* v = subdivideEdge(it->first, it->second);
		newVertices.push_back(v);
	}
	_selectedEdges.clear();	//The currently selected edges are not valid anymore

	//Mark the recently added vertices as selected
	for (GameObjectList::iterator it = newVertices.begin(); it != newVertices.end(); it++) {
		addSelectedVertex(*it);
	}
	newVertices.clear();
}

GameObject* SceneObstacles::subdivideEdge(GameObject* v1, GameObject* v2) {
	GameObject* parent = v1->getParent();
	
	Vector3 origin = v1->getTransform().getOrigin();
	Vec3D p1(origin.getX(), origin.getY(), origin.getZ());

	origin = v2->getTransform().getOrigin();
	Vec3D p2(origin.getX(), origin.getY(), origin.getZ());

	return createObstacleVertex(parent->getID(), _newVertexID++, (p1 + p2) / 2.0f, v2);
}

void SceneObstacles::scaleVerticesShape() {
	for (ObstacleList::iterator it = _obstacles.begin(); it != _obstacles.end(); it++) {
		GameObjectList& vertices = (*it)->getChildren();
		
		for (GameObjectList::iterator itVertices = vertices.begin(); itVertices != vertices.end(); itVertices++) {
			GameObject* v = *itVertices;
			v->setScale(Vector3(_currentVertexRadius, _currentVertexRadius, _currentVertexRadius));
		}
	}
}

#pragma endregion

#pragma region INPUT

void SceneObstacles::processMouseEvents() {

	if (_gui->isVisibleHelpWindow()) return;

	RayCastResult result;
	_physicsManager->rayCastCursor(result, _renderManager->getMainCamera(), COL_RAY, rayCollidesWith);

	if (!result.hit) return;

	const String tag = result.gameObject->getTag();

	if (_inputManager->isTriggeredMouseButton(OIS::MB_Left)) {
		//El raig ha impactat contra alguna cosa. Cal veure de qu� es tracta
		if ((_state == ST_MOVING_VERTEX) || (_state == ST_SCALING_VERTICES)) {
			setState(ST_IDLE);
		}
		else {
			if (tag == TAG_GRID) {
				//We have hit the grid. Create a new vertex for the current polygon
				if ((_state == ST_IDLE) || (_state == ST_CREATING_OBSTACLE)) {
					float offsetY = 0.01f;
					createObstacleVertex(_currentObstacleID, _newVertexID++, Vec3D(result.position.x(), result.position.y() + offsetY, result.position.z()));
					setState(ST_CREATING_OBSTACLE);
				}
			}
		}
	}
	else if (_inputManager->isTriggeredMouseButton(OIS::MB_Right)) {
		if ((tag == TAG_OBSTACLE_VERTEX) && ((_state == ST_IDLE) || (_state == ST_CREATING_PORTAL))) {
			if (_state == ST_IDLE) {
				if (_inputManager->isPressedKey(OIS::KC_LSHIFT)) {
					//Select the whole obstacle
					size_t obstacleID = result.gameObject->getParent()->getID();
					selectObstacleVertices(_obstacles[obstacleID]);
				}
				else if (!_inputManager->isPressedKey(OIS::KC_LCONTROL)) {
					clearSelection();
				}
			}
			addSelectedVertex(result.gameObject);
		}
	}
}

void SceneObstacles::processKeyboardEvents() {
	Scene::processKeyboardEvents();

	bool ctrlPressed = _inputManager->isPressedKey(OIS::KC_LCONTROL);

	/*** UP ***/
	if (_inputManager->isTriggeredKey(OIS::KC_UP)) {
		if (ctrlPressed) {
			_currentVertexRadius = MAX_VERTEX_RADIUS;
		}
		else {
			_currentVertexRadius = std::min(MAX_VERTEX_RADIUS, _currentVertexRadius + VERTEX_RADIUS_STEP);
		}
		scaleVerticesShape();
	}

	/*** DOWN ***/
	if (_inputManager->isTriggeredKey(OIS::KC_DOWN)) {
		if (ctrlPressed) {
			_currentVertexRadius = MIN_VERTEX_RADIUS;
		}
		else {
			_currentVertexRadius = std::max(MIN_VERTEX_RADIUS, _currentVertexRadius - VERTEX_RADIUS_STEP);
		}
		scaleVerticesShape();
	}

	/*** H ***/
	if (_inputManager->isTriggeredKey(OIS::KC_H)) {
		_gui->showHelpWindow(!(_gui->isVisibleHelpWindow()));
	}

	/*** TAB ***/
	if (_inputManager->isTriggeredKey(OIS::KC_TAB) && (_state != ST_CREATING_PORTAL)) changeGeneratorType();

	/*** SPACE ***/
	if (_inputManager->isTriggeredKey(OIS::KC_SPACE)) {
		if ((_state == ST_CREATING_OBSTACLE) && (_obstacles[_currentObstacleID]->getNumChildren() >= 3)) {
			createObstacle();
			setState(ST_IDLE);
		}
	}

	/*** G ***/
	if (_inputManager->isTriggeredKey(OIS::KC_G)) {
		if (((_state == ST_IDLE) && (_generatorType == NEOGEN)) || 
			((_state == ST_CREATING_PORTAL) && (_generatorType == MANUAL))) {
			createNavMesh();
			setState(ST_NAVMESH_GENERATED);
			if (_generatorType == NEOGEN) _gui->setGeneratorStep(1);
			else _gui->setGeneratorStep(2);
		}
		else if ((_state == ST_IDLE) && (_generatorType == MANUAL)) {
			setState(ST_CREATING_PORTAL);
			_gui->setGeneratorStep(1);
		}
	}

	/*** BackSpace ***/
	if (_inputManager->isTriggeredKey(OIS::KC_BACK) && (_state == ST_CREATING_PORTAL)) {
		setState(ST_IDLE);
	}

	/*** T ***/
	if (_inputManager->isTriggeredKey(OIS::KC_T)) {
		if ((_state == ST_IDLE) && (!_selectedVertices.empty())) {
			setState(ST_MOVING_VERTEX);
		}
	}

	/*** Vertex Manipulation ***/
	if (!_inputManager->isPressedMouseButton(OIS::MB_Right)) {
		/*** S ***/
		if (_inputManager->isTriggeredKey(OIS::KC_S)) {
			if (ctrlPressed) {
				MapParser& mapParser = MapParser::getSingleton();
				InterfaceGUIManager::FileTypeList ftList;
				if (!_navMesh) {
					//Save the obstacle description

					//1) Convert the Obstacles to the data needed by the MapParser
					MapParser::ObstacleDescription obstacleDescription;
					getObstacleDescription(obstacleDescription);
					
					MapParser::PortalDescription portalDescription;
					getPortalDescription(portalDescription);

					//2) Select the where we want to save the file
					InterfaceGUIManager::FileType type(L"NEOGEN Scene Description", L"*.neo");
					ftList.push_back(type);
					const String fileName = _guiManager->saveFile(ftList);

					if (!fileName.empty()) {
						//3) Save it!
						mapParser.saveObstacleDescription(obstacleDescription, portalDescription, fileName);
					}
				}
				else {
					//Save the NavMesh
					
					//1) Select the where we want to save the file
					InterfaceGUIManager::FileType type(L"Navigation Mesh", L"*.nav");
					ftList.push_back(type);
					const String fileName = _guiManager->saveFile(ftList);

					if (!fileName.empty()) {
						//3) Save it!
						mapParser.saveNavMesh(_navMesh, fileName);
					}
				}
			}
			else if ((_state == ST_IDLE) && (_selectedVertices.size() >= 2)) {
				//Scale the selected vertices
				setState(ST_SCALING_VERTICES);
			}
			else if ((_state == ST_NAVMESH_GENERATED)) {
				selectAllVertices();
				setState(ST_SCALING_VERTICES);
			}
		}

		/*** A ***/
		if (_inputManager->isTriggeredKey(OIS::KC_A)) {
			if (_selectedVertices.empty()) selectAllVertices();
			else clearSelection();
		}

		/*** W ***/
		if (_inputManager->isTriggeredKey(OIS::KC_W)) {
			subdivideSelectedEdges();
		}
	}

	/*** O ***/
	if (_inputManager->isTriggeredKey(OIS::KC_O)) {
		if (_state == ST_IDLE) {
			//if (ctrlPressed) {
				//Load an obstacle description
				
				//1) Select the where we want to save the file
				InterfaceGUIManager::FileTypeList ftList;
				InterfaceGUIManager::FileType type(L"NEOGEN Scene Description", L"*.neo");
				ftList.push_back(type);
				const String fileName = _guiManager->openFile(ftList);

				if (!fileName.empty()) {
					//2) Read the ObstacleDescription from the selected file
					MapParser& mapParser = MapParser::getSingleton();
					MapParser::ObstacleDescription obstacleDescription;
					MapParser::PortalDescription portalDescription;
					mapParser.loadObstacleDescription(obstacleDescription, portalDescription, fileName);

					//3) Clear the current scene and load the obstacles
					clearScene();
					getObstaclesFromDescription(obstacleDescription);
					getPortalsFromDescription(portalDescription);
				}
			//}
		}
	}

	/*** V ***/
	if (_inputManager->isTriggeredKey(OIS::KC_V)) {
		Renderer* renderer = _grid->getComponent<Renderer>();
		renderer->setVisible(!renderer->isVisible());
	}

	/*** N ***/
	/*if (_inputManager->isTriggeredKey(OIS::KC_N)) {
		clearScene();
		setState(ST_IDLE);
	}*/
}

#pragma endregion

