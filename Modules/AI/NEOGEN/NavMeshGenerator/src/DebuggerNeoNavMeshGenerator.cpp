#include "DebuggerNeoNavMeshGenerator.h"
#include <NeoNavMeshGenerator.h>
#include <NeoVertex.h>
#include <NeoOGRE.h>
#include <NeoEdge.h>

using namespace NEOGEN;
using namespace Rogue;

#pragma region CREATION AND DESTRUCTION

DebuggerNeoNavMeshGenerator::DebuggerNeoNavMeshGenerator() : DebuggerBase() {
	_debuggerAOI = DebuggerPolyLineUPtr(new DebuggerPolyLine());
	_debuggerAOI->setColor(0xffffff00);
	_debuggerAOI->setSize(0);

	_renderManager->setParentSceneNode(_debuggerAOI->getSceneNode(), _sceneNode);
}

void DebuggerNeoNavMeshGenerator::clear() {
	_debuggerNotches.clear();
	_debuggerAOI->clear();
}

#pragma endregion

#pragma region GET AND SET

void DebuggerNeoNavMeshGenerator::setVisible(bool visible) {
	_debuggerAOI->setVisible(visible);
	for (DebuggerPoints::iterator it = _debuggerNotches.begin(); it != _debuggerNotches.end(); it++) {
		(*it)->setVisible(visible);
	}
}

#pragma endregion

#pragma region UPDATE

void DebuggerNeoNavMeshGenerator::update(NeoNavMeshGenerator* generator, const NEOGEN::Vector3& offset) {
	_generator = generator;
	_sceneNode->setPosition(Ogre::Vector3(offset.getX(), offset.getY(), offset.getZ()));

	clear();
	if (!_generator) return;

	NeoNotchMap& notches = _generator->getNotchesReal();
	for (NeoNotchMap::iterator it = notches.begin(); it != notches.end(); it++) {
		
		//Draw the point at the notch position
		const Vector3& position = it->second._vertex->getPosition();
		DebuggerPointUPtr debugger = DebuggerPointUPtr(new DebuggerPoint());
		debugger->setPosition(position.getX(), position.getY(), position.getZ());
		debugger->setMaterialName("DiffuseYellow");
		debugger->setScale(0.25f);
		_renderManager->setParentSceneNode(debugger->getSceneNode(), _sceneNode);
		_debuggerNotches.push_back(std::move(debugger));

		//Draw the area of interest of the notch
		Vector3 vIn = position + it->second._dirIn * 10.0;
		Vector3 vOut = position + it->second._dirOut * 10.0;
		
		//The limitEdgeIN
		int vPosID = _debuggerAOI->addVertex(NeoOGRE::toOGRE(position));
		int vInID = _debuggerAOI->addVertex(NeoOGRE::toOGRE(vIn));
		int vOutID = _debuggerAOI->addVertex(NeoOGRE::toOGRE(vOut));
		_debuggerAOI->addSegment(vPosID, vInID);
		_debuggerAOI->addSegment(vPosID, vOutID);
	}

	_debuggerAOI->update();
}

#pragma endregion

