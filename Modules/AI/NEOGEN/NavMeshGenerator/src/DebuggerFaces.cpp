#include "DebuggerFaces.h"
#include "NeoOGRE.h"

using namespace Rogue;
using namespace NEOGEN;

#pragma region CREATION AND DESTRUCTION

DebuggerFaces::DebuggerFaces() : DebuggerBase() {
	_manualPolygons = _renderManager->createManualObject();
	_sceneNode->attachObject(_manualPolygons);
	_newIndexID = 0;
	setMaterialName("DiffuseGrey");
	setOffset(Vector3(0,0,0));
}

DebuggerFaces::~DebuggerFaces() {

}

uint DebuggerFaces::createIndex(NeoVertex* v) {
	if (hasIndex(v)) return _indexMap[v];

	_indexMap[v] = _newIndexID;
	return _newIndexID++;
}

#pragma endregion

#pragma region GET AND SET

void DebuggerFaces::addTriangles(const Triangles& triangles) {
	for (Triangles::const_iterator it = triangles.begin(); it != triangles.end(); it++) _polygons.push_back(*it);
}

uint DebuggerFaces::getIndex(NeoVertex* v) {
	return _indexMap[v];
}

bool DebuggerFaces::hasIndex(NeoVertex* v) {
	return (_indexMap.find(v) != _indexMap.end());
}

void DebuggerFaces::setVisible(bool visible) {
	DebuggerBase::setVisible(visible);
	_manualPolygons->setVisible(visible);
}

void DebuggerFaces::clear() {
	DebuggerBase::clear();
	_newIndexID = 0;
	_indexMap.clear();
	_polygons.clear();
	_manualPolygons->clear();
}

void DebuggerFaces::setMaterialName(const std::string& materialName) {
	_materialName = materialName;
}

void DebuggerFaces::setOffset(const NEOGEN::Vector3& offset) {
	_offset = offset;
}

#pragma endregion

#pragma region UPDATE

void DebuggerFaces::update() {
	_manualPolygons->clear();

	_manualPolygons->begin(_materialName, Ogre::RenderOperation::OT_TRIANGLE_LIST);	

	for (Triangles::iterator itTriangle = _polygons.begin(); itTriangle != _polygons.end(); itTriangle++) {
		Triangle& t = *itTriangle;
		Vector3 v = t[0]->getPosition() - t[1]->getPosition();
		Vector3 u = t[2]->getPosition() - t[1]->getPosition();
		Vector3 normal = u.cross(v).normalized();
		for (Triangle::iterator itVertex = t.begin(); itVertex != t.end(); itVertex++) {
			NeoVertex* v = *itVertex;
			_manualPolygons->position(NeoOGRE::toOGRE(v->getPosition() + _offset));
			_manualPolygons->normal(NeoOGRE::toOGRE(normal));
		}
	}

	_manualPolygons->end();
}

#pragma endregion

			

			