#include "DebuggerNeoNavMesh.h"
#include <NeoNavMesh.h>
#include <NeoOGRE.h>

using namespace NEOGEN;
using namespace Rogue;

#pragma region CREATION AND DESTRUCTION

DebuggerNeoNavMesh::DebuggerNeoNavMesh() : DebuggerNeoMesh() {
	//Create the debuggers for the cells. 
	_debugMode = DebugMode::NEOGEN_3D;
	size_t numMaterials = 12;
	std::string baseMaterialName = "DebugCellColor";
	for (size_t i = 0; i < numMaterials; i++) {
		std::string matName = baseMaterialName + ((i < 10)? std::string("0") : std::string("")) + std::to_string(i);
		_debuggerCells[matName] = DebuggerFacesUPtr(new DebuggerFaces());
		_debuggerCells[matName]->setMaterialName(matName);
		_renderManager->setParentSceneNode(_debuggerCells[matName]->getSceneNode(), _sceneNode);
		_materialNames.push_back(matName);
	}
}

#pragma endregion

#pragma region GET AND SET

void DebuggerNeoNavMesh::setVisible(bool v) {
	DebuggerNeoMesh::setVisible(v);
	setVisiblePortals(v);
	setVisibleCells(v);
}

void DebuggerNeoNavMesh::setVisiblePortals(bool v) {
	_debuggerEdgesPortal->setVisible(v);
}

void DebuggerNeoNavMesh::setVisibleCells(bool v) {
	for (DebuggerCellMap::iterator it = _debuggerCells.begin(); it != _debuggerCells.end(); it++) {
		it->second->setVisible(v);
	}
}

DebuggerNeoNavMesh::DebugMode DebuggerNeoNavMesh::getDebugMode() {
	return _debugMode;
}

void DebuggerNeoNavMesh::setDebugMode(DebugMode mode) {
	_debugMode = mode;
}

void DebuggerNeoNavMesh::clear() {
	DebuggerNeoMesh::clear();
	for (DebuggerCellMap::iterator it = _debuggerCells.begin(); it != _debuggerCells.end(); it++) {
		it->second->clear();
	}
}

bool DebuggerNeoNavMesh::isValidMaterial(NeoCell* cell, const std::string& matName) {
	//Determine if the material matName is a valid material for the cell, i.e., none of the 
	//neighbors of cell has this material already assigned. 
	NeoCellPSet cNeighbors;
	cell->getNeighbors(cNeighbors);
	
	bool isValid = true;
	NeoCellPSet::iterator it = cNeighbors.begin();
	while (isValid && (it != cNeighbors.end())) {
		isValid = getCellMaterial(*it) != matName;
		it++;
	}
	
	return isValid;
}

std::string DebuggerNeoNavMesh::getCellMaterial(NeoCell* cell) {
	CellMaterialMap::iterator it = _cellMaterialMap.find(cell);
	if (it != _cellMaterialMap.end()) return it->second;
	return "";
}

#pragma endregion

#pragma region UPDATE

void DebuggerNeoNavMesh::updateDebuggers() {
	DebuggerNeoMesh::updateDebuggers();
	for (DebuggerCellMap::iterator it = _debuggerCells.begin(); it != _debuggerCells.end(); it++) {
		it->second->update();
	}
}

void DebuggerNeoNavMesh::refreshDebuggers() {
	DebuggerNeoMesh::refreshDebuggers();
	NeoCellMap& cells = ((NeoNavMesh*)_mesh)->getCells();
	size_t matNameID = 0;
	for (NeoCellMap::iterator itCell = cells.begin(); itCell != cells.end(); itCell++) {
		//Determine the debugger where the cell will be assigned to. This debugger must be
		//different from the debugger of the neighbors. 
		NeoCell* cell = itCell->second.get();
		NeoCellPSet cNeighbors;
		cell->getNeighbors(cNeighbors);
		
		do {
			matNameID = (matNameID + 1) % _materialNames.size();
		} while (!isValidMaterial(cell, _materialNames[matNameID]));
			
		refreshDebuggerCell(cell, _materialNames[matNameID]);
	}
}

void DebuggerNeoNavMesh::refreshDebuggerCell(NeoCell* cell, const std::string& matName) {
	if (_debugMode == DebugMode::NEOGEN_3D) {
		NeoFacePSet& faces = cell->getFaces();
		for (NeoFacePSet::iterator it = faces.begin(); it != faces.end(); it++) {
			_debuggerCells[matName]->addTriangles((*it)->getTriangles());
		}
	}
	else _debuggerCells[matName]->addTriangles(cell->getTriangles());

	_cellMaterialMap[cell] = matName;
}

#pragma endregion