#include "DebuggerPolygons.h"
#include "NeoOGRE.h"

using namespace Rogue;
using namespace NEOGEN;

#pragma region CREATION AND DESTRUCTION

DebuggerPolygons::DebuggerPolygons(const std::string& name, const std::string& materialName) : DebuggerBase(name) {
	_manualPolygons = _renderManager->createManualObject();
	_renderManager->createSceneNode()->attachObject(_manualPolygons);
	setMaterialName(materialName);
}

DebuggerPolygons::~DebuggerPolygons() {

}

#pragma endregion

#pragma region GET AND SET

void DebuggerPolygons::clear() {
	_manualPolygons->clear();
}

void DebuggerPolygons::setMaterialName(const std::string& materialName) {
	_materialName = materialName;
}

#pragma endregion

#pragma region UPDATE

void DebuggerPolygons::update(Polygons3& polygons) {
	_polygons = polygons;
	update();
}

void DebuggerPolygons::update() {
	clear();

	_manualPolygons->begin(_materialName, Ogre::RenderOperation::OT_LINE_LIST);	
	for (Polygons3::iterator itPolygon = _polygons.begin(); itPolygon != _polygons.end(); itPolygon++) {
		Polygon3& p = *itPolygon;
		for (size_t i = 0; i < p.size(); i++) {
			_manualPolygons->position(NeoOGRE::toOGRE(p[i]));
			_manualPolygons->position(NeoOGRE::toOGRE(p[(i+1) % p.size()]));
		}
	}
	_manualPolygons->end();
}

#pragma endregion