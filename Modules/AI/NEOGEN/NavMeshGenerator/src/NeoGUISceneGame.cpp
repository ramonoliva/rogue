#include "NeoGUISceneGame.h"

using namespace Rogue;

static const int GUI_MENU_WIDTH	= 200;
static const int GUI_SLIDER_WIDTH = 64;

/***********************************************************************/
/* CREATION AND DESTRUCTION */
/***********************************************************************/
NeoGUISceneGame::NeoGUISceneGame(SdkTrayManager* tMgr) : GUIBase(tMgr) {
	createInfoModeWindow();
	createModelOptionsWindow();
	createNavmeshOptionsWindow();
	createResultsWindow();
}

NeoGUISceneGame::~NeoGUISceneGame() {

}

void NeoGUISceneGame::createInfoModeWindow() {
	_modePanel = _trayMgr->createParamsPanel(OgreBites::TL_TOPLEFT, "ApplicationMode", 400, 1);
}

void NeoGUISceneGame::createModelOptionsWindow() {
	//Menu per seleccionar el tipus de model que volem i carregar un model
	Ogre::LogManager::getSingletonPtr()->logMessage("CREATE MODEL OPTIONS WINDOW");
	_trayMgr->createLabel(OgreBites::TL_TOPRIGHT, "modelTypeLabel", "Model Options", GUI_MENU_WIDTH);
	_menuModelType = _trayMgr->createThickSelectMenu(
																OgreBites::TL_TOPRIGHT, 
																"modelTypeMenu", "Model Type",
																GUI_MENU_WIDTH, 
																2);
	_menuModelType->addItem("2D Polygons");
	_menuModelType->addItem("3D Mesh");
	_menuModelType->selectItem(1, false);
	
	OgreBites::SelectMenu* fBrowser = _trayMgr->createThickSelectMenu(
																OgreBites::TL_TOPRIGHT, 
																"modelBrowser", "File",
																GUI_MENU_WIDTH, 
																16);
	
	//Menu per seleccionar el qu� volem mostrar en pantalla (model, mapa de normals, mapa de profunditat, etc)
	_menuDrawMode = _trayMgr->createThickSelectMenu(
												OgreBites::TL_TOPRIGHT, 
												"drawModeMenu", "Draw Mode: ", 
												GUI_MENU_WIDTH, 
												16);
	_menuDrawMode->addItem("Shaded");
	_menuDrawMode->addItem("Normal Map");
	_menuDrawMode->addItem("Depth Map");
	_menuDrawMode->addItem("Normal Filter HR");
	_menuDrawMode->selectItem(0, false);
	_trayMgr->createSeparator(OgreBites::TL_TOPRIGHT, "modelOptionsSep", GUI_MENU_WIDTH);
}

void NeoGUISceneGame::createNavmeshOptionsWindow() {

	_trayMgr->createLabel(OgreBites::TL_TOPRIGHT, "nmOptions", "Navmesh Options", GUI_MENU_WIDTH);
	
	//Slider per seleccionar la resoluci� que volem
	_resolutionSlider = _trayMgr->createThickSlider(
													OgreBites::TL_TOPRIGHT,
													"_resolutionSlider",
													"Pixels per Unit",
													GUI_MENU_WIDTH,
													GUI_SLIDER_WIDTH,
													1,
													10,
													10);
	_resolutionSlider->setValue(10);

	//Slider per seleccionar la convex distance (pel tema del convex relaxation)
	_convexDistSlider = _trayMgr->createThickSlider(
													OgreBites::TL_TOPRIGHT,
													"_convexDistSlider",
													"Convex Distance",
													GUI_MENU_WIDTH,
													GUI_SLIDER_WIDTH,
													0,
													1,
													101);
	_convexDistSlider->setValue(0.0f);

	//Slider per seleccionar l'al�ada m�xima que un personatge pot superar caminant
	_maxStepSlider = _trayMgr->createThickSlider(
													OgreBites::TL_TOPRIGHT,
													"_maxStepSlider",
													"Max Step Height",
													GUI_MENU_WIDTH,
													GUI_SLIDER_WIDTH,
													0,
													1,
													101);
	_maxStepSlider->setValue(0.3f);

	//Slider per seleccionar la pendent m�xima que un personatge pot caminar
	_maxSlopeSlider = _trayMgr->createThickSlider(
													OgreBites::TL_TOPRIGHT,
													"_maxSlopeSlider",
													"Max Slope",
													GUI_MENU_WIDTH,
													GUI_SLIDER_WIDTH,
													0,
													90,
													91);
	_maxSlopeSlider->setValue(60.0f);
	
	//Check box per activar/desactivar la generaci� de la NavMesh utilitzant la GPU
	_GPUModeCheckBox = _trayMgr->createCheckBox(OgreBites::TL_TOPRIGHT, "gpuCB", "Use GPU mode", GUI_MENU_WIDTH);
	_GPUModeCheckBox->setChecked(false);
		
	_trayMgr->createSeparator(OgreBites::TL_TOPRIGHT, "nmOptionsSep", GUI_MENU_WIDTH);
}

void NeoGUISceneGame::createResultsWindow() {
	_trayMgr->createLabel(OgreBites::TL_BOTTOMLEFT, "ResultsLabel", "Results", GUI_MENU_WIDTH);
	Ogre::StringVector paramNames;
	paramNames.push_back("Initial vertexs");
	paramNames.push_back("Total notches");
	paramNames.push_back("Real notches");
	paramNames.push_back("False notches");
	paramNames.push_back("Split notches");
	paramNames.push_back("New vertexs");
	paramNames.push_back("Portals");
	paramNames.push_back("Cells");
	paramNames.push_back("Elapsed time");
	_resultsPanel = _trayMgr->createParamsPanel(OgreBites::TL_BOTTOMLEFT, "_resultsPanel", GUI_MENU_WIDTH, paramNames);
	unsigned int numParams = (unsigned int)paramNames.size();
	for (unsigned int i = 0; i < numParams; i++) {
		_resultsPanel->setParamValue(i, "0");
	}
}
