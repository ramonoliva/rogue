#include "NeoGUISceneNavMeshGenerator.h"

using namespace Rogue;

#pragma region CONSTANTS

static const int GUI_MENU_WIDTH	= 200;
static const int GUI_SLIDER_WIDTH = 64;

static const float MIN_SLOPE = 0.0f;
static const float MAX_SLOPE = 90.0f;
static const float DEFAULT_MAX_SLOPE = 45.0f;

static const float MIN_CHAR_HEIGHT = 1.0f;
static const float MAX_CHAR_HEIGHT = 3.0f;
static const float DEFAULT_CHAR_HEIGHT = 1.75f;

static const float MIN_CONVEXITY_RELAXATION = 0.0f;
static const float MAX_CONVEXITY_RELAXATION = 1.0f;

#pragma endregion

#pragma region CREATION AND DESTRUCTION

NeoGUISceneNavMeshGenerator::NeoGUISceneNavMeshGenerator(Rogue::SdkTrayManager* tMgr) : GUIBase(tMgr) {
	createNavMeshOptionsTab();
	createTerrainDebugTab();
	createNavMeshDebugTab();
}

NeoGUISceneNavMeshGenerator::~NeoGUISceneNavMeshGenerator() {

}

void NeoGUISceneNavMeshGenerator::createNavMeshOptionsTab() {
	OgreBites::TrayLocation tLocation = OgreBites::TL_TOPRIGHT;
	_trayMgr->createLabel(tLocation, "_navMeshOptionsLabel", "NavMesh Options", GUI_MENU_WIDTH);

	//Slider to select the maximum slope that a character can overcome. 
	_maxSlopeSlider = createSlider(tLocation, "_maxSlopeSlider", "Max Slope", MIN_SLOPE, MAX_SLOPE);
	_maxSlopeSlider->setValue(DEFAULT_MAX_SLOPE);

	//Slider to select the maximum height of a character
	_heightSlider = createSlider(tLocation, "_heightSlider", "Char Height", MIN_CHAR_HEIGHT, MAX_CHAR_HEIGHT, 100);
	_heightSlider->setValue(DEFAULT_CHAR_HEIGHT);

	//Slider to control the convexity relaxation
	_crSlider = createSlider(tLocation, "_crSlider", "Convexity Relaxation", MIN_CONVEXITY_RELAXATION, MAX_CONVEXITY_RELAXATION, 100);
	_crSlider->setValue(0.0f);
}

void NeoGUISceneNavMeshGenerator::createTerrainDebugTab() {
	OgreBites::TrayLocation tLocation = OgreBites::TL_BOTTOMRIGHT;

	_trayMgr->createLabel(tLocation, "_debugTerrainLabel", "Debug Terrain", GUI_MENU_WIDTH);

	_debugTerrain = _trayMgr->createCheckBox(tLocation, "_debugTerrain", "Show Terrain", GUI_MENU_WIDTH);
	_debugTerrain->setChecked(true);

	_debugTerrainNormals = _trayMgr->createCheckBox(tLocation, "_debugTerrainNormals", "Show Terrain Normals", GUI_MENU_WIDTH);
	_debugTerrainNormals->setChecked(true);

	_debugTerrainEdges = _trayMgr->createCheckBox(tLocation, "_debugTerrainEdges", "Show Terrain Edges", GUI_MENU_WIDTH);
	_debugTerrainEdges->setChecked(true);

	_debugTerrainOctree = _trayMgr->createCheckBox(tLocation, "_debugTerrainOctree", "Show Terrain Octree", GUI_MENU_WIDTH);
	_debugTerrainOctree->setChecked(false);
}

void NeoGUISceneNavMeshGenerator::createNavMeshDebugTab() {
	OgreBites::TrayLocation tLocation = OgreBites::TL_BOTTOMRIGHT;

	_trayMgr->createLabel(tLocation, "_debugNavMeshLabel", "Debug NavMesh", GUI_MENU_WIDTH);

	_debugNavMesh = _trayMgr->createCheckBox(tLocation, "_debugNavMesh", "Show NavMesh", GUI_MENU_WIDTH);
	_debugNavMesh->setChecked(true);

	_debugNavMeshEdges = _trayMgr->createCheckBox(tLocation, "_debugNavMeshEdges", "Show NavMesh Edges", GUI_MENU_WIDTH);
	_debugNavMeshEdges->setChecked(true);

	_debugNavMeshNotches = _trayMgr->createCheckBox(tLocation, "_debugNotches", "Show NavMesh Notches", GUI_MENU_WIDTH);
	_debugNavMeshNotches->setChecked(true);

	_debugNavMeshCells = _trayMgr->createCheckBox(tLocation, "_debugCells", "Show NavMesh Cells", GUI_MENU_WIDTH);
	_debugNavMeshCells->setChecked(true);

	_debugNavMeshOctree = _trayMgr->createCheckBox(tLocation, "_debugNavMeshOctree", "Show NavMesh Octree", GUI_MENU_WIDTH);
	_debugNavMeshOctree->setChecked(false);
}

OgreBites::Slider* NeoGUISceneNavMeshGenerator::createSlider(OgreBites::TrayLocation tLocation, const std::string& name, const std::string& title, float min, float max, int resolution) {
	OgreBites::Slider* slider = _trayMgr->createThickSlider(
													tLocation,
													name,
													title,
													GUI_MENU_WIDTH,
													GUI_SLIDER_WIDTH,
													min,
													max,
													((int)max - (int)min) * resolution + 1);
	return slider;
}

#pragma endregion