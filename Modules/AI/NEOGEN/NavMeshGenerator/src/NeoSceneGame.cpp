#include "NeoSceneGame.h"
#include <string>
#include <list>
#include <RogueCore.h>
#include <RogueInterfaceGUIManager.h>
#include <RogueInterfaceCameraManager.h>
#include <RogueDeferredPass.h>
#include <RogueDeferredPassLibrary.h>

#include "NeoDeferredPasses.h"

using namespace Rogue;
using namespace NEOGEN;

/******************************************************************************************/
/* STATIC VARIABLES INITIALIZATION */
/******************************************************************************************/

static const String LEVEL_TEX_NAME = "levelTex";
static const String NG_MAIN_DEFERRED_CHAIN_NAME = "MainDeferredChain";
static const String LEVEL_DEFERRED_CHAIN_NAME = "LevelDeferredChain";
static const String NORMAL_DEPTH_MAP_DEFERRED_CHAIN_NAME = "NormalDepthMapDeferredChain";

/******************************************************************************************/
/* CREATION AND DESTRUCTION */
/******************************************************************************************/
NeoSceneGame::NeoSceneGame() {
	//Game Manager Initialization
	_renderManager = Core::getManager<InterfaceRenderManager>();
	_inputManager = Core::getManager<InterfaceInputManager>();
	_cameraManager = Core::getManager<InterfaceCameraManager>();
	_cameraManager->setCamera(_renderManager->getMainCamera());
	_guiManager = Core::getManager<InterfaceGUIManager>();

	showAdjacencyGraph = true;
	raySceneQuery = NULL;
	vertSelected = NULL;
	obstacleSolverTime = 0.0f;
	camPosPerspective = Ogre::Vector3(0.0f, 20.0f, 120.0f);
	camPos2D = Ogre::Vector3(0.0f, 50.0f, 0.0f);
	raySceneQuery = NULL;
	
	applicationState = STATE_IDLE;
	
	manualPerpendicularFaces = _renderManager->createManualObject();
	nodePerpendicularFaces = _renderManager->createSceneNode();
	nodePerpendicularFaces->attachObject(manualPerpendicularFaces);
	
	manualWalkableFaces = _renderManager->createManualObject();
	nodeWalkableFaces = _renderManager->createSceneNode();
	nodeWalkableFaces->attachObject(manualWalkableFaces);
	
	manualObstacleFaces = _renderManager->createManualObject();
	nodeObstacleFaces = _renderManager->createSceneNode();
	nodeObstacleFaces->attachObject(manualObstacleFaces);

	setupDeferredMode();
}

void NeoSceneGame::setupDeferredMode() {

	//Create the Standard Deferred Chain
	_mainDeferredChain = _renderManager->createDeferredChain(NG_MAIN_DEFERRED_CHAIN_NAME);
	_mainDeferredChain->addDeferredPass(new DPFillGBuffer());
	_finalDeferredPass = new DPFinalComposition();
	_mainDeferredChain->addDeferredPass(_finalDeferredPass);

	_renderManager->setDeferredChainActive(NG_MAIN_DEFERRED_CHAIN_NAME);

	_levelDeferredChain = NULL;
	_normalDepthMapDeferredChain = NULL;
}

void NeoSceneGame::createScene() {

	Ogre::StringVector paramNames;
	Ogre::StringVector paramValues;
	_guiSceneGame = _guiManager->createGUI<NeoGUISceneGame>("GUISceneGame");
	paramNames.push_back("Edit Mode");
	paramValues.push_back("Draw polygons and press 'G' to generate");
	_guiSceneGame->setInfo(paramNames, paramValues);
	globalTimer->reset();

	raySceneQuery = _renderManager->createRaySceneQuery();
	nmGen = new NavMeshGenerator(_renderManager->getSceneManager());
	oSolver = new ObstacleSolver();
	voxelizer = new VoxelizerCPU(_renderManager->getSceneManager());
	nodeModel = NULL;
	init3DMode();

	_renderManager->setAmbientLightColor(Color(0.5f, 0.5f, 0.5f));
	Light* directionalLight = _renderManager->createLight("directionalLight");
    directionalLight->setType(Ogre::Light::LT_DIRECTIONAL);
    directionalLight->setDiffuseColour(Color(1.0f, 1.0f, 1.0f));
    directionalLight->setSpecularColour(Color(0.0f, 0.0f, 0.0f));
	directionalLight->setDirection(Ogre::Vector3( 0, -1, -1));
	_renderManager->setRenderMode(RENDER_MODE_FORWARD);

	std::ifstream iFile;
	std::string filename = "mapToLoad.cfg";
    iFile.open(filename.c_str(), std::ios::in);
	std::string mapName;
	iFile >> mapName;
	iFile.close();

	loadModel(mapName);
}

void NeoSceneGame::destroyScene() {
	
}

NeoSceneGame::~NeoSceneGame() {
	Ogre::LogManager::getSingletonPtr()->logMessage("DESTROYING NEOGEN");
	_renderManager->destroyRaySceneQuery(raySceneQuery);
	delete nmGen;
	delete oSolver;
	delete voxelizer;
	_guiManager->destroyGUI("GUISceneGame");		
}

/******************************************************************************************/
/* CLASS FUNCTIONS */
/******************************************************************************************/
void NeoSceneGame::init2DMode() {
	Ogre::LogManager::getSingletonPtr()->logMessage("INITIALIZING 2D Model");
	setModelVisible(false);
	initPerspectiveCamera(camPos2D);
	boardVisible = true;
	nmGen->showBoard(boardVisible);
	_guiSceneGame->setVisibleDrawModeMenu(false);
	applicationMode = APPLICATION_MODE_2D;
}

void NeoSceneGame::init3DMode() {
	//loadModel("egTest.mesh");				//Escenari simple amb escales
	//loadModel("egTest2.mesh");			//Escenari simple amb escales
	//loadModel("multiLayerTest01.mesh");	//Escenari de les anelles
	//loadModel("multiLayerTest03.mesh");	//L'arbre
	//loadModel("scaSample01.mesh");
	
	//loadModel("donutTest.mesh");
	//loadModel("scaSample03.mesh");
	//loadModel("dungeon.obj");
	//loadModel("dungeon.mesh");
	setModelVisible(true);
	initPerspectiveCamera(camPosPerspective);
	boardVisible = false;
	nmGen->showBoard(boardVisible);
	_guiSceneGame->setVisibleDrawModeMenu(true);
	applicationMode = APPLICATION_MODE_3D;
}

void NeoSceneGame::initPerspectiveCamera(Ogre::Vector3& camPos) {
	//Inicialitza la c�mera en mode perspectiva
	Camera* cam = _renderManager->getMainCamera();
	cam->setProjectionType(Ogre::PT_PERSPECTIVE);
	cam->setOrientation(Ogre::Quaternion::IDENTITY);
	cam->setPosition(camPos);
    cam->lookAt(Ogre::Vector3(0,0,0));
    cam->setNearClipDistance(0.1f);
	cam->setFarClipDistance(1000.0f);
	cam->setFOVy(Ogre::Degree(45.0f));
	size_t w, h;
	_renderManager->getRenderWindowSize(w, h);
	cam->setAspectRatio(float(w) / float(h));
}

void NeoSceneGame::initBottomView() {
	//Inicialitza una c�mera ortogr�fica en BOTTOM view (Projecci� en el pl� XZ)
	Ogre::LogManager::getSingletonPtr()->logMessage("INITIALIZING BOTTOM VIEW");
	
	Ogre::Vector3 bbMinCorner = bbModel.getMinimum();	//Extrem m�nim de la bb
	float minY = bbMinCorner.y;

	//Calculo la posici� de la c�mera
	float camDist = 1.0f;	//Dist�ncia en l'eix y entre el punt m�xim de la bb i la c�mera
	Ogre::Vector3 posCam(0.0f, minY - camDist, 0.0f);

	Camera* cam = _renderManager->getMainCamera();
	cam->setProjectionType(Ogre::PT_ORTHOGRAPHIC);
	cam->setFixedYawAxis(false);
	cam->setPosition(posCam);
	Ogre::Quaternion q1 = Ogre::Quaternion(float(SQRT_ONE_HALF), float(SQRT_ONE_HALF), 0.0f, 0.0f);
	cam->setOrientation(q1);
	
	
	//Recalculo el zNear i zFar
	cam->setNearClipDistance(camDist - 0.00001f);
	cam->setFarClipDistance(camDist + 0.00001f + bbModelDim.y);

	//Calculo les dimensions de la finestra orthogonal
	unsigned int orthoW = (unsigned int)bbModelDim.x;			//Amplada necess�ria de la textura
	unsigned int orthoH = (unsigned int)bbModelDim.z;			//Al�ada necess�ria de la textura

	cam->setOrthoWindow(orthoW, orthoH);
	cam->setAspectRatio((float)orthoW / (float)orthoH);

	Ogre::LogManager::getSingletonPtr()->logMessage("orthoW = " + std::to_string(orthoW));
	Ogre::LogManager::getSingletonPtr()->logMessage("orthoH = " + std::to_string(orthoH));
	Ogre::LogManager::getSingletonPtr()->logMessage("orthoAR = " + std::to_string(cam->getAspectRatio()));
	Ogre::LogManager::getSingletonPtr()->logMessage("zNear = " + std::to_string(cam->getNearClipDistance()));
	Ogre::LogManager::getSingletonPtr()->logMessage("zFar = " + std::to_string(cam->getFarClipDistance()));
	Ogre::LogManager::getSingletonPtr()->logMessage("minY = " + std::to_string(minY));
}

void NeoSceneGame::initTopView() {
	//Inicialitza una c�mera ortogr�fica en TOP view (Projecci� en el pl� XZ)
	Ogre::LogManager::getSingletonPtr()->logMessage("INITIALIZING TOP VIEW");
	
	float maxY = (voxelizer->getVoxelGridSizeY() * voxelizer->getVoxelDimY()) / 2.0f;
	Ogre::LogManager::getSingletonPtr()->logMessage("voxelGridDepth = " + std::to_string(voxelizer->getVoxelGridSizeY()));
	Ogre::LogManager::getSingletonPtr()->logMessage("voxelDimY = " + std::to_string(voxelizer->getVoxelDimY()));
	Ogre::LogManager::getSingletonPtr()->logMessage("maxYBefore = " + std::to_string(maxY));
	size_t bbOffset = (voxelizer->getVoxelGridSizeY() % DEPTH_RESOLUTION);
	if (bbOffset > 0) {
		Ogre::LogManager::getSingletonPtr()->logMessage("bbOffset > 0 => " + std::to_string(bbOffset));
		bbOffset = DEPTH_RESOLUTION - bbOffset;
	}
	maxY += bbOffset * voxelizer->getVoxelDimY();
	Ogre::LogManager::getSingletonPtr()->logMessage("maxYAfter = " + std::to_string(maxY));


	//Calculo la posici� de la c�mera
	float camDist = 1.0f;	//Dist�ncia en l'eix y entre el punt m�xim de la bb i la c�mera
	Ogre::Vector3 posCam(0.0f, maxY + camDist, 0.0f);

	Camera* cam = _renderManager->getMainCamera();
	cam->setProjectionType(Ogre::PT_ORTHOGRAPHIC);
	cam->setFixedYawAxis(false);
	cam->setPosition(posCam);
	cam->setOrientation(Ogre::Quaternion(float(SQRT_ONE_HALF), float(-SQRT_ONE_HALF), 0.0f, 0.0f));
	
	//Recalculo el zNear i zFar
	//float zFarOffset = 0.00001f;
	float zFarOffset = 0.0f;
	cam->setNearClipDistance(camDist - zFarOffset);
	cam->setFarClipDistance(camDist + zFarOffset + (voxelizer->getVoxelGridSizeY() + bbOffset) * voxelizer->getVoxelDimY());

	//Calculo les dimensions de la finestra orthogonal
	size_t orthoW = Ogre::Math::Ceil(voxelizer->getVoxelGridSizeX() * voxelizer->getVoxelDimX());		//Amplada necess�ria de la textura
	size_t orthoH = Ogre::Math::Ceil(voxelizer->getVoxelGridSizeZ() * voxelizer->getVoxelDimZ());		//Al�ada necess�ria de la textura

	cam->setOrthoWindow(orthoW, orthoH);
	cam->setAspectRatio((float)orthoW / (float)orthoH);

	Ogre::LogManager::getSingletonPtr()->logMessage("orthoW = " + std::to_string(orthoW));
	Ogre::LogManager::getSingletonPtr()->logMessage("orthoH = " + std::to_string(orthoH));
	Ogre::LogManager::getSingletonPtr()->logMessage("orthoAR = " + std::to_string(cam->getAspectRatio()));
	Ogre::LogManager::getSingletonPtr()->logMessage("zNear = " + std::to_string(cam->getNearClipDistance()));
	Ogre::LogManager::getSingletonPtr()->logMessage("zFar = " + std::to_string(cam->getFarClipDistance()));
	Ogre::LogManager::getSingletonPtr()->logMessage("maxY = " + std::to_string(maxY));
	Ogre::LogManager::getSingletonPtr()->logMessage("bbOffset = " + std::to_string(bbOffset));
}

void NeoSceneGame::initFrontView() {
	//Inicialitza una c�mera ortogr�fica en FRONT view (Projecci� en el pl� XY)
	Ogre::LogManager::getSingletonPtr()->logMessage("INITIALIZING FRONT VIEW");
	
	float maxZ = (voxelizer->getVoxelGridSizeZ() * voxelizer->getVoxelDimZ()) / 2.0f;
	size_t bbOffset = (voxelizer->getVoxelGridSizeZ() % DEPTH_RESOLUTION);
	if (bbOffset > 0) bbOffset = DEPTH_RESOLUTION - bbOffset;
	maxZ += bbOffset * voxelizer->getVoxelDimZ();

	//Calculo la posici� de la c�mera
	float camDist = 1.0f;	//Dist�ncia en l'eix y entre el punt m�xim de la bb i la c�mera
	Ogre::Vector3 posCam(0.0f, 0.0f, maxZ + camDist);

	Camera* cam = _renderManager->getMainCamera();
	cam->setProjectionType(Ogre::PT_ORTHOGRAPHIC);
	cam->setFixedYawAxis(false);
	cam->setPosition(posCam);
	//cam->setOrientation(Ogre::Quaternion(0.0, 0.0, 1.0, 0.0));
	cam->lookAt(Ogre::Vector3(0.0f, 0.0f, 0.0f));
	
	//Recalculo el zNear i zFar
	//float zFarOffset = 0.00001f;
	float zFarOffset = 0.0f;
	cam->setNearClipDistance(camDist - zFarOffset);
	cam->setFarClipDistance(camDist + zFarOffset + (voxelizer->getVoxelGridSizeZ() + bbOffset) * voxelizer->getVoxelDimZ());

	//Calculo les dimensions de la finestra orthogonal
	size_t orthoW = Ogre::Math::Ceil(voxelizer->getVoxelGridSizeX() * voxelizer->getVoxelDimX());		//Amplada necess�ria de la textura
	size_t orthoH = Ogre::Math::Ceil(voxelizer->getVoxelGridSizeY() * voxelizer->getVoxelDimY());		//Al�ada necess�ria de la textura

	cam->setOrthoWindow(orthoW, orthoH);
	cam->setAspectRatio((float)orthoW / (float)orthoH);

	Ogre::LogManager::getSingletonPtr()->logMessage("orthoW = " + std::to_string(orthoW));
	Ogre::LogManager::getSingletonPtr()->logMessage("orthoH = " + std::to_string(orthoH));
	Ogre::LogManager::getSingletonPtr()->logMessage("bbModelDim.z = " + std::to_string(bbModelDim.z));
	Ogre::LogManager::getSingletonPtr()->logMessage("orthoAR = " + std::to_string(cam->getAspectRatio()));
	Ogre::LogManager::getSingletonPtr()->logMessage("zNear = " + std::to_string(cam->getNearClipDistance()));
	Ogre::LogManager::getSingletonPtr()->logMessage("zFar = " + std::to_string(cam->getFarClipDistance()));
	Ogre::LogManager::getSingletonPtr()->logMessage("clipDistance = " + std::to_string(cam->getFarClipDistance() - cam->getNearClipDistance()));
	Ogre::LogManager::getSingletonPtr()->logMessage("maxZ = " + std::to_string(maxZ));
	Ogre::LogManager::getSingletonPtr()->logMessage("bbOffset = " + std::to_string(bbOffset));
}

void NeoSceneGame::initSideView() {
	//Inicialitza una c�mera ortogr�fica en SIDE view (Projecci� en el pl� ZY)
	Ogre::LogManager::getSingletonPtr()->logMessage("INITIALIZING SIDE VIEW");
	
	float maxX = (voxelizer->getVoxelGridSizeX() * voxelizer->getVoxelDimX()) / 2.0f;
	size_t bbOffset = (voxelizer->getVoxelGridSizeX() % DEPTH_RESOLUTION);
	if (bbOffset > 0) bbOffset = DEPTH_RESOLUTION - bbOffset;
	maxX += bbOffset * voxelizer->getVoxelDimX();

	//Calculo la posici� de la c�mera
	float camDist = 1.0f;	//Dist�ncia en l'eix y entre el punt m�xim de la bb i la c�mera
	Ogre::Vector3 posCam(maxX + camDist, 0.0f, 0.0f);

	Camera* cam = _renderManager->getMainCamera();
	cam->setProjectionType(Ogre::PT_ORTHOGRAPHIC);
	cam->setFixedYawAxis(false);
	cam->setPosition(posCam);
	//cam->setOrientation(Ogre::Quaternion(SQRT_ONE_HALF, 0.0, -SQRT_ONE_HALF, 0.0));
	cam->lookAt(Ogre::Vector3(0.0f, 0.0f, 0.0f));
	
	//Recalculo el zNear i zFar
	float zFarOffset = 0.0f;
	cam->setNearClipDistance(camDist - zFarOffset);
	cam->setFarClipDistance(camDist + zFarOffset + (voxelizer->getVoxelGridSizeX() + bbOffset) * voxelizer->getVoxelDimX());

	//Calculo les dimensions de la finestra orthogonal
	size_t orthoW = Ogre::Math::Ceil(voxelizer->getVoxelGridSizeZ() * voxelizer->getVoxelDimZ());		//Amplada necess�ria de la textura
	size_t orthoH = Ogre::Math::Ceil(voxelizer->getVoxelGridSizeY() * voxelizer->getVoxelDimY());		//Al�ada necess�ria de la textura

	cam->setOrthoWindow(orthoW, orthoH);
	cam->setAspectRatio((float)orthoW / (float)orthoH);

	Ogre::LogManager::getSingletonPtr()->logMessage("orthoW = " + std::to_string(orthoW));
	Ogre::LogManager::getSingletonPtr()->logMessage("orthoH = " + std::to_string(orthoH));
	Ogre::LogManager::getSingletonPtr()->logMessage("orthoAR = " + std::to_string(cam->getAspectRatio()));
	Ogre::LogManager::getSingletonPtr()->logMessage("zNear = " + std::to_string(cam->getNearClipDistance()));
	Ogre::LogManager::getSingletonPtr()->logMessage("zFar = " + std::to_string(cam->getFarClipDistance()));
	Ogre::LogManager::getSingletonPtr()->logMessage("maxX = " + std::to_string(maxX));
	Ogre::LogManager::getSingletonPtr()->logMessage("bbOffset = " + std::to_string(bbOffset));
}

void NeoSceneGame::setDrawMode(int sel) {
	if (sel == DRAW_MODE_SHADED) _finalDeferredPass->setMaterialName("matShowDiffuse");
	else if (sel == DRAW_MODE_NORMAL_MAP) _finalDeferredPass->setMaterialName("matShowNormals");
	/*if (renderQuad == NULL) return;
	switch (sel) {
		case DRAW_MODE_SHADED:
			renderQuad->setMaterial("matShadedModel");
			break;

		case DRAW_MODE_NORMAL_MAP:
			renderQuad->setMaterial("matShowNormals");
			break;

		case DRAW_MODE_NORMAL_FILTER:
			Ogre::LogManager::getSingletonPtr()->logMessage("MODE NORMAL FILTER");
			float maxSlope = Ogre::Degree(_guiSceneGame->getMaxSlope()).valueRadians();
			float s = Ogre::Math::Cos(maxSlope);	
			setUniform("matShowNormalFilter", 0, FRAGMENT_SHADER, "cosMaxSlope", s);
			renderQuad->setMaterial("matShowNormalFilter");
			break;
	}*/
}

void NeoSceneGame::initNavMeshData() {
	//Obtinc els pol�gons que ha generat el obstacleSolver
	std::vector<Poly*>& obstacles = oSolver->getObstacles();
	size_t numObstacles = obstacles.size();
	Ogre::LogManager::getSingletonPtr()->logMessage("numObstacles = " + std::to_string(numObstacles));
	if (numObstacles > 0) {
		Poly* floor = obstacles.at(0);
		std::vector<Poly*> holes;
		for (size_t i = 1; i < obstacles.size(); i++) {
			holes.push_back(obstacles.at(i));
		}

		//Canvio a mode pol�gon 2D
		init2DMode();
		nmGen->setData(floor, holes);
		Ogre::LogManager::getSingletonPtr()->logMessage("DATA SET");

		//Inicialitzaci� de la generaci�
		float convexDist = _guiSceneGame->getConvexDist();
		Ogre::LogManager::getSingletonPtr()->logMessage("INITIALIZING NAV MESH DATA");
		nmGen->setConvexDistance(convexDist);
		Ogre::LogManager::getSingletonPtr()->logMessage("NAV MESH DATA INITIALIZED");
		//canvio d'estat
		applicationState = STATE_GENERATE_NAVMESH_LAYER;
	}
	else {
		Ogre::LogManager::getSingletonPtr()->logMessage("EMPTY LAYER!");
		init2DMode();
		init3DMode();
		applicationState = STATE_NAVMESH_LAYER_GENERATED;
	}
}

void NeoSceneGame::generateNavMesh() {
	Ogre::LogManager::getSingletonPtr()->logMessage("GENERATING NAV MESH LAYER");
	generationMode = GENERATION_MODE_FULL;
	Ogre::LogManager::getSingletonPtr()->logMessage("GENERATING FULL");
	generateNavMeshFull();
	Ogre::LogManager::getSingletonPtr()->logMessage("FULL GENERATION COMPLETE");
	
	//Recupero la NavMesh generada
	NavMesh* nm = nmGen->getNavMesh();

	//Restauro el mode 3D
	init3DMode();

	//Guardo la NavMesh a la layer corresponent
	Layer* layer = voxelizer->getActiveLayer();
	layer->navMesh = nm;
	voxelizer->navMeshVoxelDescription();
	
	nm->drawEdges();
}

void NeoSceneGame::generateNavMeshStepByStep() {

}

void NeoSceneGame::generateNavMeshFull() {
	_renderManager->getRenderWindow()->setAutoUpdated(false);
	_guiSceneGame->setVisibleAll(false);
	bool navMeshGenerated = false;
	while (!navMeshGenerated) navMeshGenerated = nmGen->generate();
	_guiSceneGame->setVisibleAll(true);
	//initPerspectiveCamera(camPosPerspective);
	initPerspectiveCamera(camPos2D);
	size_t w, h;
	_renderManager->getRenderWindowSize(w, h);
	_renderManager->getMainCamera()->setAspectRatio(float(w) / float(h));	
	_renderManager->getRenderWindow()->setAutoUpdated(true);
	applicationState = STATE_NAVMESH_LAYER_GENERATED;
}

void NeoSceneGame::modifyVertScale(float modifier) {
	vertScale += modifier;
	if (vertScale > MAX_VERT_SCALE) vertScale = MAX_VERT_SCALE;
	if (vertScale < MIN_VERT_SCALE) vertScale = MIN_VERT_SCALE;
	//nmGen->drawVerts();
	scaleVertNodes();
}

void NeoSceneGame::scaleVertNodes() {
	std::list<SceneNode*>::iterator it;
	for (it = vertNodes.begin(); it != vertNodes.end(); it++) {
		(*it)->setScale(vertScale, vertScale, vertScale);
	}
}

/******************************************************************************/
/* INPUT */
/******************************************************************************/
void NeoSceneGame::loadObj(const std::string& fileName) {
	std::ifstream file;
	std::string line;
	file.open(fileName.c_str());
	std::string meshName = fileName.substr(0, fileName.size() - 4); //Descarto el format del nom del fitxer
	meshName += ".mesh";
	Ogre::MeshPtr mesh = Ogre::MeshManager::getSingletonPtr()->getByName(meshName);
	if (mesh.isNull()) {
		//El model encara no ha estat carregat abans. El carrego des del fitxer
		if (file.is_open()) {
			std::vector<Ogre::Vector3> vertices;
			Ogre::Vector3 v;
			std::vector<std::vector<int>> faces;
			std::vector<int> f(3);
			std::string type;
			while (std::getline(file, line)) {	//Llegim el fitxer l�nia a l�nia
				std::istringstream in(line);
				in >> type;
				if (type.compare("v") == 0) {
					//Es tracta d'un v�rtex. Llegeixo les 3 coordenades
					in >> v.x >> v.y >> v.z;
					vertices.push_back(v);
				}
				else if (type.compare("f") == 0) {
					//Es tracta d'una cara. Llegeixo els 3 v�rtexs del triangle
					in >> f[0] >> f[1] >> f[2];
					f[0]--; f[1]--; f[2]--; //Corregeixo els identificadors dels v�rtexs, ja que en un obj es numeren a partir del 1
					faces.push_back(f);
				}
			}
			createManualMesh(meshName, vertices, faces);
		}
		else Ogre::LogManager::getSingletonPtr()->logMessage("ERROR LOADING OBJ FILE!");
	}
}

void NeoSceneGame::createManualMesh(const std::string& meshName, std::vector<Ogre::Vector3>& vertices, std::vector<std::vector<int>>& faces) {
	Ogre::LogManager::getSingletonPtr()->logMessage("CREATING MANUAL MESH");
	Ogre::LogManager::getSingletonPtr()->logMessage("meshName = " + meshName);
	Ogre::LogManager::getSingletonPtr()->logMessage("numVerts = " + std::to_string(vertices.size()));
	Ogre::LogManager::getSingletonPtr()->logMessage("numFaces = " + std::to_string(faces.size()));
	Ogre::ManualObject* manual = _renderManager->createManualObject();
	std::vector<std::vector<int>>::iterator it;
	std::vector<int> f(3);
	manual->begin("matModel", Ogre::RenderOperation::OT_TRIANGLE_LIST);
	int i = 0;	//Indica l'index del v�rtex inicial del triangle
	for (it = faces.begin(); it != faces.end(); it++) {
		//Defineixo les posicions dels v�rtexs 
		manual->position(vertices.at(it->at(0)));
		manual->position(vertices.at(it->at(1)));
		manual->position(vertices.at(it->at(2)));

		//Defineixo l'�s dels v�rtexs referenciant els seus �ndex (necessari per crear una mesh)
		manual->index(i);
		manual->index(i + 1);
		manual->index(i + 2);

		//Incremento l'identificador de triangle
		i += 3;
	}
	manual->end();
	Ogre::MeshPtr mesh = manual->convertToMesh(meshName);
	//if (mesh.isNull()) Ogre::LogManager::getSingletonPtr()->logMessage("NULL MESH!");
	//else Ogre::LogManager::getSingletonPtr()->logMessage("MESH NOT NULL! " + mesh->getName()); 
	Ogre::LogManager::getSingletonPtr()->logMessage("MANUAL MESH CREATED");
}

void NeoSceneGame::loadModel(const std::string& fileName) {
	//Carrega el model indicat
	Ogre::LogManager::getSingletonPtr()->logMessage("LOADING MODEL");
	//Comprovo quin format t� el fitxer d'entrada
	size_t i = 0;
	while (fileName[i] != '.') {
		i++;
	}
	std::string baseName = fileName.substr(0, i);
	std::string format = fileName.substr(i, fileName.size());
	Ogre::LogManager::getSingletonPtr()->logMessage("baseName = " + baseName);
	Ogre::LogManager::getSingletonPtr()->logMessage("format = " + format);
	if (format.compare(".obj") == 0) loadObj(fileName);

	_renderManager->destroySceneNode(nodeModel);
	nodeModel = _renderManager->createSceneNode();
	//entModel = mSceneMgr->createEntity("ramps00.mesh");
	//entModel = mSceneMgr->createEntity("nav_test.mesh");
	
	//<<< BEGIN MAPES DE TEST >>>
	entModel = _renderManager->createEntity(baseName + ".mesh");
	//entModel = mSceneMgr->createEntity("multiLayerTest03.mesh");
	//entModel = mSceneMgr->createEntity("multiLayerTest01.mesh");
	//<<< END MAPES DE TEST >>>
	
	//entModel = mSceneMgr->createEntity("nav_test.mesh");
	//entModel = mSceneMgr->createEntity("nmgMapTest01.mesh");
	//entModel = mSceneMgr->createEntity("dungeon.mesh");
	//entModel = mSceneMgr->createEntity("dungeonTest08.mesh");
	//entModel = mSceneMgr->createEntity("dungeonTest09.mesh");
	//entModel = mSceneMgr->createEntity("dungeon2.mesh");
	//entModel = mSceneMgr->createEntity("nmgMapTest00.mesh");
	//entModel = mSceneMgr->createEntity("problematicObjects3.mesh");
	entModel->setMaterialName("Default");
	nodeModel->attachObject(entModel);
	
	bbModel = entModel->getBoundingBox();		//Obtinc la bb del model
	bbModelCenter = bbModel.getCenter();		//El centre de la bb del model
	nodeModel->translate(-bbModelCenter);	//Posiciono el model de tal forma que el centre de la BB correspon amb el punt (0, 0, 0)
	bbModelDim = bbModel.getSize();				//Dimensions de la bb. 
	bbModelDim.x = (float)Ogre::Math::Ceil(bbModelDim.x);
	bbModelDim.y = (float)Ogre::Math::Ceil(bbModelDim.y);
	bbModelDim.z = (float)Ogre::Math::Ceil(bbModelDim.z);
	
	Ogre::Vector3 bbHalfSize = bbModelDim / 2.0f;
	Ogre::Vector3 bbMin = bbModel.getCenter() - bbHalfSize;
	Ogre::Vector3 bbMax = bbModel.getCenter() + bbHalfSize;
	bbModel.setExtents(bbMin, bbMax);
	
	setDrawMode(0);
	setModelVisible(true);
	timeAnim = 0.0f;
	rotateModel = false;
	currentModelAngle = 0.0f;
	showFaceType = 0;
	Ogre::LogManager::getSingletonPtr()->logMessage("MODEL LOADED");
}

void NeoSceneGame::classifyModelFaces() {
	//Classifica les cares del model en:
	//-> Caminable: La normal de la cara �s superable pel personatge
	//-> Obstacle: La normal de la cara no �s superable pel personatge, i no �s perpendicular al vector (0, 1, 0)
	//-> Perpendicular: La normal de la cara �s near-perpendicular en relaci� al vector (0, 1, 0)  
	manualWalkableFaces->clear();
	manualObstacleFaces->clear();
	manualPerpendicularFaces->clear();

	walkableFaces.clear();
	obstacleFaces.clear();
	perpendicularFaces.clear();
	size_t vertexCount;
    size_t indexCount;
    Ogre::Vector3 *vertexList;
    unsigned long *indexList;

	getMeshInformation(entModel->getMesh(), vertexCount, vertexList, indexCount, indexList);

	// test for hitting individual triangles on the mesh
	Ogre::Vector3 v1, v2, v3;
	Ogre::Vector3 v1v2Dir, v1v3Dir;
	Ogre::Vector3 normalFace;
	Ogre::Real dotProd;
	ngTriangle t;
	Ogre::LogManager::getSingletonPtr()->logMessage("vertexCount = " + std::to_string(vertexCount));
	Ogre::LogManager::getSingletonPtr()->logMessage("indexCount = " + std::to_string(indexCount));
	float maxSlope = _guiSceneGame->getMaxSlope();
	float cosMaxSlope = Ogre::Math::Cos(Ogre::Degree(maxSlope).valueRadians());
	manualPerpendicularFaces->begin("matModel", Ogre::RenderOperation::OT_TRIANGLE_LIST);
	manualWalkableFaces->begin("matModel", Ogre::RenderOperation::OT_TRIANGLE_LIST);
	manualObstacleFaces->begin("matModel", Ogre::RenderOperation::OT_TRIANGLE_LIST);
	Ogre::LogManager::getSingletonPtr()->logMessage("maxSlope = " + std::to_string(maxSlope));
	Ogre::LogManager::getSingletonPtr()->logMessage("cosMaxSlope = " + std::to_string(cosMaxSlope));
    for (int i = 0; i < static_cast<int>(indexCount); i += 3) {
		//Obtinc els 3 v�rtexs del triangle
		v1 = vertexList[indexList[i]] - bbModelCenter;
		v2 = vertexList[indexList[i + 1]] - bbModelCenter;
		v3 = vertexList[indexList[i + 2]] - bbModelCenter;

		//Creo el triangle
		t.clear();
		t.push_back(v1);
		t.push_back(v2);
		t.push_back(v3);
		        
        //Calculo la normal de la cara. 
		v1v2Dir = v2 - v1;	//Vector que va de v1 a v2
		v1v3Dir = v3 - v1;	//Vector que va de v1 a v3
		
		//Calculo un vector perpendicular als vectors (v1v2Dir, v1v3Dir) (CROSS PRODUCT)
		normalFace = v1v2Dir.crossProduct(v1v3Dir);
		normalFace.normalise();
		
		//Comprobo si el vector normalFace i el vector up s�n perpendiculars (DOT PRODUCT)
		dotProd = normalFace.dotProduct(Ogre::Vector3::UNIT_Y);

		/*Ogre::LogManager::getSingletonPtr()->logMessage("v1v2Dir = " + std::to_string(v1v2Dir));
		Ogre::LogManager::getSingletonPtr()->logMessage("v1v3Dir = " + std::to_string(v1v3Dir));
		Ogre::LogManager::getSingletonPtr()->logMessage("normalFace = " + std::to_string(normalFace));
		Ogre::LogManager::getSingletonPtr()->logMessage("dotProd = " + std::to_string(dotProd));*/
		if (Ogre::Math::RealEqual(dotProd, 0.0f, 0.01f)) {
			//He trobat una cara "problem�tica". Afegeixo el triangle al manual object
			/*Ogre::LogManager::getSingletonPtr()->logMessage("PROBLEMATIC FACE!");
			Ogre::LogManager::getSingletonPtr()->logMessage("v1 = " + std::to_string(v1));
			Ogre::LogManager::getSingletonPtr()->logMessage("v2 = " + std::to_string(v2));
			Ogre::LogManager::getSingletonPtr()->logMessage("v3 = " + std::to_string(v3));
			*/
			perpendicularFaces.push_back(t);
			manualPerpendicularFaces->position(v1);
			manualPerpendicularFaces->position(v2);
			manualPerpendicularFaces->position(v3);
		}
		else if (dotProd > cosMaxSlope) {
			//He trobat una cara caminable
			walkableFaces.push_back(t);
			manualWalkableFaces->position(v1);
			manualWalkableFaces->position(v2);
			manualWalkableFaces->position(v3);
		}
		else {
			//He trobat una cara obstacle
			obstacleFaces.push_back(t);
			manualObstacleFaces->position(v1);
			manualObstacleFaces->position(v2);
			manualObstacleFaces->position(v3);
		}
	}
	manualWalkableFaces->end();
	manualPerpendicularFaces->end();
	manualObstacleFaces->end();
	// free the verticies and indicies memory
    delete[] vertexList;
    delete[] indexList;
}

void NeoSceneGame::getMeshInformation(const Ogre::MeshPtr mesh,
                                size_t &vertex_count,
                                Ogre::Vector3* &vertices,
                                size_t &index_count,
                                unsigned long* &indices)
{
    bool added_shared = false;
    size_t current_offset = 0;
    size_t shared_offset = 0;
    size_t next_offset = 0;
    size_t index_offset = 0;
 
    vertex_count = index_count = 0;
 
    // Calculate how many vertices and indices we're going to need
    for (unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i) {
        Ogre::SubMesh* submesh = mesh->getSubMesh(i);
 
        // We only need to add the shared vertices once
        if (submesh->useSharedVertices) {
            if (!added_shared) {
                vertex_count += mesh->sharedVertexData->vertexCount;
                added_shared = true;
            }
        }
        else {
            vertex_count += submesh->vertexData->vertexCount;
        }
 
        // Add the indices
        index_count += submesh->indexData->indexCount;
    }
 
 
    // Allocate space for the vertices and indices
    vertices = new Ogre::Vector3[vertex_count];
    indices = new unsigned long[index_count];
 
    added_shared = false;
 
    // Run through the submeshes again, adding the data into the arrays
    for (unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i) {
        Ogre::SubMesh* submesh = mesh->getSubMesh(i);
 
        Ogre::VertexData* vertex_data = submesh->useSharedVertices ? mesh->sharedVertexData : submesh->vertexData;
 
        if ((!submesh->useSharedVertices) || (submesh->useSharedVertices && !added_shared)) {
            if(submesh->useSharedVertices) {
                added_shared = true;
                shared_offset = current_offset;
            }
 
            const Ogre::VertexElement* posElem =
                vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);
 
            Ogre::HardwareVertexBufferSharedPtr vbuf =
                vertex_data->vertexBufferBinding->getBuffer(posElem->getSource());
 
            unsigned char* vertex =
                static_cast<unsigned char*>(vbuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
 
            // There is _no_ baseVertexPointerToElement() which takes an Ogre::Real or a double
            //  as second argument. So make it float, to avoid trouble when Ogre::Real will
            //  be comiled/typedefed as double:
            //      Ogre::Real* pReal;
            float* pReal;
 
            for (size_t j = 0; j < vertex_data->vertexCount; ++j, vertex += vbuf->getVertexSize()) {
                posElem->baseVertexPointerToElement(vertex, &pReal);
                Ogre::Vector3 pt(pReal[0], pReal[1], pReal[2]);
                //vertices[current_offset + j] = (orient * (pt * scale)) + position;
				vertices[current_offset + j] = pt;
            }
            vbuf->unlock();
            next_offset += vertex_data->vertexCount;
        }
 
        Ogre::IndexData* index_data = submesh->indexData;
        size_t numTris = index_data->indexCount / 3;
        Ogre::HardwareIndexBufferSharedPtr ibuf = index_data->indexBuffer;
        if (ibuf.isNull()) continue; // need to check if index buffer is valid (which will be not if the mesh doesn't have triangles like a pointcloud)
 
        bool use32bitindexes = (ibuf->getType() == Ogre::HardwareIndexBuffer::IT_32BIT);
 
        unsigned long*  pLong = static_cast<unsigned long*>(ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
        unsigned short* pShort = reinterpret_cast<unsigned short*>(pLong);
 
 
        size_t offset = (submesh->useSharedVertices)? shared_offset : current_offset;
        size_t index_start = index_data->indexStart;
        size_t last_index = numTris*3 + index_start;
 
        if (use32bitindexes) {
            for (size_t k = index_start; k < last_index; ++k) {
                indices[index_offset++] = pLong[k] + static_cast<unsigned long>(offset);
            }
		}
        else {
            for (size_t k = index_start; k < last_index; ++k) {
                indices[ index_offset++ ] = static_cast<unsigned long>(pShort[k]) +
                    static_cast<unsigned long>( offset );
            }
		}
        ibuf->unlock();
        current_offset = next_offset;
    }
}

void NeoSceneGame::setModelVisible(bool visible) {
	//Neteja el model carregat
	if (nodeModel != NULL) {
		nodeModel->setVisible(visible);
	}
}

void NeoSceneGame::setModelPosition(Ogre::Vector3& pos) {
	nodeModel->setPosition(pos);
	nodeWalkableFaces->setPosition(pos);
	nodeObstacleFaces->setPosition(pos);
	nodePerpendicularFaces->setPosition(pos);
}

void NeoSceneGame::setModelFacesVisible(unsigned int mask, bool b) {
	if ((mask & WALKABLE_FACES) > 0) manualWalkableFaces->setVisible(b);
	else manualWalkableFaces->setVisible(!b);
	if ((mask & OBSTACLE_FACES) > 0) manualObstacleFaces->setVisible(b);
	else manualObstacleFaces->setVisible(!b);
	if ((mask & PERPENDICULAR_FACES) > 0) manualPerpendicularFaces->setVisible(b);
	else manualPerpendicularFaces->setVisible(!b);
}

void NeoSceneGame::setManualMaterial(Ogre::ManualObject* manual, const Ogre::String& matName) {
	if (manual->getNumSections() > 0) manual->setMaterialName(0, matName);
}

void NeoSceneGame::voxelization() {
	classifyModelFaces();
	voxelizer->createGrid(entModel, 1.0f, 0.5f);
	voxelizationCPU();
	applicationState = STATE_VOXELIZED;
}

void NeoSceneGame::voxelizationCPU() {
	Ogre::LogManager::getSingletonPtr()->logMessage("CPU VOXELIZATION");
	setModelFacesVisible(ALL_FACES, false);
	//voxelizer->voxelization(obstacleFaces, Voxel::TYPE_NEGATIVE);
	//voxelizer->voxelization(perpendicularFaces, Voxel::TYPE_NEGATIVE);
	voxelizer->voxelization(walkableFaces, Voxel::TYPE_POSITIVE);
}

void NeoSceneGame::fillLayerTexture() {
	//Renders the Cutting SHape of the current layer to create a texture that will be used
	//to filter the model in the High Resolution step
	
	//Create and activate the Level Deferred Chain
	if (_levelDeferredChain == NULL) {
		float resolution = 1.0f;
		size_t w = size_t(bbModelDim.x) * size_t(resolution);
		size_t h = size_t(bbModelDim.z) * size_t(resolution);
		_levelDeferredChain = _renderManager->createDeferredChain(LEVEL_DEFERRED_CHAIN_NAME);
		_levelDeferredChain->addDeferredPass(new DPFillLayerTexture(w, h));
		_levelDeferredChain->addDeferredPass(new DPPortalExpansion());
	}
	_renderManager->setRenderMode(RENDER_MODE_DEFERRED);
	_renderManager->setDeferredChainActive(LEVEL_DEFERRED_CHAIN_NAME);
	
	//Prepare the scene to be rendered
	setModelVisible(false);
	voxelizer->showCuttingShape(true);
	voxelizer->showVoxelizedModel(false);
	voxelizer->setupCuttingMesh();
	initTopView();

	//Force a render Update to update the render textures right now
	_renderManager->update();
	
	//Restore the Scene State
	voxelizer->showCuttingShape(false);
	initPerspectiveCamera(camPosPerspective);
	
	_renderManager->setDeferredChainActive(NG_MAIN_DEFERRED_CHAIN_NAME);
	_renderManager->setRenderMode(RENDER_MODE_FORWARD);

	Ogre::LogManager::getSingletonPtr()->logMessage("LAYER TEXTURE FILLED");
}

void NeoSceneGame::layerSelection() {
	Ogre::LogManager::getSingletonPtr()->logMessage("LAYER SELECTION ANAVMG");
	if (voxelizer->existActiveLayer()) {
		//Encara hi ha layers que no tenen la seva NavMesh generada
		Ogre::LogManager::getSingletonPtr()->logMessage("ACTIVE LAYER EXIST");
		voxelizer->drawActiveLayer(perpendicularFaces);
		applicationState = STATE_LAYER_SELECTED;
	}
	else {
		//Totes les layers ja tenen la seva NavMesh generada
		Ogre::LogManager::getSingletonPtr()->logMessage("NO ACTIVE LAYER EXIST");
		voxelizer->showNavMeshLayers(true);
		applicationState = STATE_MERGE_NAV_MESHES;
	}
}

void NeoSceneGame::levelRefinement() {
	Ogre::LogManager::getSingletonPtr()->logMessage("REFINING LEVEL");

	//Create and activate the Level Deferred Chain
	if (_normalDepthMapDeferredChain == NULL) {
		size_t resolution = size_t(_guiSceneGame->getResolution());
		size_t w = size_t(bbModelDim.x * resolution);			
		size_t h = size_t(bbModelDim.z * resolution);
		_normalDepthMapDeferredChain = _renderManager->createDeferredChain(NORMAL_DEPTH_MAP_DEFERRED_CHAIN_NAME);
		_normalDepthMapDeferredChain->addDeferredPass(new DPLevelRefinement(w, h));
		_normalDepthMapDeferredChain->addDeferredPass(new DPContourDetection(w, h));
	}

	_renderManager->setRenderMode(RENDER_MODE_DEFERRED);
	_renderManager->setDeferredChainActive(NORMAL_DEPTH_MAP_DEFERRED_CHAIN_NAME);


	//Netejo el contingut del generador de malles navegacionals
	nmGen->clearData();

	//Mostro el model i oculto el model voxelitzat
	setModelVisible(true);
	entModel->setMaterialName("matLevelRefinement");
	Ogre::ManualObject* manualPerpendicularFaces = voxelizer->getManualPerpendicularFacesIntersection();
	manualPerpendicularFaces->setVisible(true);
	//setModelFacesVisible(WALKABLE_FACES | OBSTACLE_FACES, true);
	//setManualMaterial(manualWalkableFaces, "matLevelRefinement");
	//setManualMaterial(manualObstacleFaces, "matLevelRefinement");
	voxelizer->showVoxelizedModel(false);
	voxelizer->showCuttingShape(false);

	//Renderitzo l'escena per obtenir el normalDepthMap
	initTopView();
	Ogre::LogManager::getSingletonPtr()->logMessage("RENDERING NORMAL DEPTH MAP");
	//UNIFORM: Calculo el cosinus del m�xim angle que pot superar un personatge
	float maxSlope = Ogre::Degree(_guiSceneGame->getMaxSlope()).valueRadians();
	float s = Ogre::Math::Cos(maxSlope);	
	setUniform("matLevelRefinement", 0, FRAGMENT_SHADER, "cosMaxSlope", s);
	//UNIFORM: Calculo el offset per dalt de la cutting mesh
	Camera* cam = _renderManager->getMainCamera();
	float zNear = cam->getNearClipDistance();
	float zFar = cam->getFarClipDistance();
	float zDist = zFar - zNear;
	//float minOffset = voxelizer->getCharHeight() / zDist;	//Al�ada del personatge normalitzada
	float minOffset = 0.0f;
	setUniform("matLevelRefinement", 0, FRAGMENT_SHADER, "minOffset", minOffset);
	//UNIFORM: Calculo el offset per baix de la cutting mesh
	float vSizeY = voxelizer->getVoxelDimY();
	int numSecureVoxels = 2;
	float maxOffset = (vSizeY * numSecureVoxels) / zDist;
	Ogre::LogManager::getSingletonPtr()->logMessage("zNear = " + std::to_string(zNear));
	Ogre::LogManager::getSingletonPtr()->logMessage("zFar = " + std::to_string(zFar));
	Ogre::LogManager::getSingletonPtr()->logMessage("zDist = " + std::to_string(zDist));
	Ogre::LogManager::getSingletonPtr()->logMessage("charHeight = " + std::to_string(voxelizer->getCharHeight()));
	Ogre::LogManager::getSingletonPtr()->logMessage("minOffset = " + std::to_string(minOffset));
	Ogre::LogManager::getSingletonPtr()->logMessage("charHeight = " + std::to_string(vSizeY));
	Ogre::LogManager::getSingletonPtr()->logMessage("maxOffset = " + std::to_string(maxOffset));
	//setUniform("matLevelRefinement", "maxOffset", maxOffset);
	

	_renderManager->update();

	setModelVisible(false);
	entModel->setMaterialName("Default");
	manualPerpendicularFaces->setVisible(false);

	//Reactivo la finestra i desactivo el renderTarget pr�viament activat
	float clipDist = cam->getFarClipDistance() - cam->getNearClipDistance();
	float resolution = _guiSceneGame->getResolution();
	float maxStep = _guiSceneGame->getMaxStep();
	Ogre::LogManager::getSingletonPtr()->logMessage("TESTING BBDIM");
	Ogre::LogManager::getSingletonPtr()->logMessage("bbModelDim.y = " + std::to_string(bbModelDim.y));
	Ogre::AxisAlignedBox bbAux = entModel->getBoundingBox();
	Ogre::LogManager::getSingletonPtr()->logMessage("auxDim.y = " + std::to_string(bbAux.getSize().y));
	TexturePtr normalDepthMapTex = _renderManager->getTexture("contourTex");
	oSolver->initializeData(normalDepthMapTex, maxSlope, maxStep, cam->getNearClipDistance(), cam->getFarClipDistance(), cam->getPosition().y, resolution);
	
	//Defineixo la llavor a alta resoluci� a partir del v�xel llavor de la layer
	Voxel* vSeed = voxelizer->getActiveLayer()->voxelSeed;
	size_t gridWidth = voxelizer->getVoxelGridSizeX();		//Amplada de la bbModel
	size_t gridHeight = voxelizer->getVoxelGridSizeZ();	//Llargada de la bbModel
	size_t hrMapWidth = oSolver->getMapWidth();				//Amplada del HRMap
	size_t hrMapHeight = oSolver->getMapHeight();			//Llargada del HRMap
	float widthFactor = (float)hrMapWidth / (float)gridWidth;
	float heightFactor = (float)hrMapHeight / (float)gridHeight;
	size_t hrSeedX = (size_t)(vSeed->keyX * widthFactor);
	size_t hrSeedY = (size_t)(vSeed->keyZ * heightFactor);
	hrSeedX += 5;
	hrSeedY += 5;
	Ogre::LogManager::getSingletonPtr()->logMessage("voxelSeedX = " + std::to_string(vSeed->keyX));
	Ogre::LogManager::getSingletonPtr()->logMessage("voxelSeedY = " + std::to_string(vSeed->keyY));
	Ogre::LogManager::getSingletonPtr()->logMessage("voxelSeedZ = " + std::to_string(vSeed->keyZ));
	Ogre::LogManager::getSingletonPtr()->logMessage("hrSeedX = " + std::to_string(hrSeedX));
	Ogre::LogManager::getSingletonPtr()->logMessage("hrSeedY = " + std::to_string(hrSeedY));
	oSolver->generateObstaclePartition(hrSeedX, hrSeedY);
	obstacleSolverTime = oSolver->getElapsedTime();
		
	Ogre::LogManager::getSingletonPtr()->logMessage("OBSTACLE PARTITION GENERATED!");
	
	//Canvio d'estat
	applicationState = STATE_LAYER_REFINED;
	initNavMeshData();
	setModelFacesVisible(ALL_FACES, false);
	
	_renderManager->setDeferredChainActive(NG_MAIN_DEFERRED_CHAIN_NAME);
	_renderManager->setRenderMode(RENDER_MODE_FORWARD);
}

void NeoSceneGame::selectWalkableArea(float viewX, float viewY) {
	Camera* cam = _renderManager->getMainCamera();
	Ogre::Ray mouseRay = cam->getCameraToViewportRay(viewX, viewY);
	bool walkableVoxelHit = voxelizer->selectWalkableArea(mouseRay);
	if (walkableVoxelHit) {
		//El raig ha impactat amb un v�xel caminable. L'�rea camniable ha estat
		//determinada a partir d'aquest v�xel. Canvio d'estat. 
		applicationState = STATE_LAYERS_GENERATED;
	}
}

/*********************************************************************************************************/
/* UPDATE */
/*********************************************************************************************************/
void NeoSceneGame::update() {
	_cameraManager->update();
	_inputManager->update();
	processMouseEvents();
	processKeyboardEvents();
	processGUIEvents();	
	_renderManager->update();
}

/*********************************************************************************************************/
/* DIBUIXAT */
/*********************************************************************************************************/
void NeoSceneGame::updateModelRotation(Ogre::Real timeLF) {
	//Actualitza l'animaci� del marcador de trampa

	float timeToRotate = 0.03f;
	float angleToRotate = 1.5f;
	timeAnim += timeLF;

	if (timeAnim >= timeToRotate) {
		nodeModel->yaw(Ogre::Degree(angleToRotate));
		timeAnim = 0.0f;
		currentModelAngle += angleToRotate;
		if (currentModelAngle >= 360.0f) {
			currentModelAngle = 0.0f;
			rotateModel = false;
			nodeModel->setOrientation(Ogre::Quaternion::IDENTITY);
		}
	}
}