#include "NeoApp.h"

using namespace Rogue;

#pragma region GLOBAL VARS INITIALIZATION

/***

Use the global variable G_SCENE_STATE to activate the corresponding NEOGEN mode:

0 => Neogen3D: Generates the NavMesh from a 3D model representing a virtual scenario. 
1 => Neogen2D: Generates the NavMesh from a polygon with holes defined by the user. 
2 => New NEOGEN implementation. 

***/

int Rogue::G_SCENE_STATE = 2;			
String Rogue::G_APP_NAME = "NEOGEN";
bool Rogue::G_MOUSE_EXCLUSIVE = true;

typedef std::unique_ptr<NeoApp> NeoAppUPtr;

#pragma endregion

#pragma region MAIN

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <stdlib.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT )
#else
    int main(int argc, char *argv[])
#endif
    {
		NeoAppUPtr app = NeoAppUPtr(new NeoApp());

		////COMPARISON PAPER CODE BEGIN
		//NeoAppUPtr app = NULL;
		//if ((__argc == 3) || (__argc == 4)) app = NeoAppUPtr(new NeoApp());
		//else {
		//	MessageBox(NULL, "Usage: NavMeshGenerator.exe inputFile outputFile [maxSlope]", "Bad number of arguments!!!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
		//}
		////COMPARISON PAPER CODE END

		if (app) {
			try {
				app->go();
			}
			catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
				MessageBox( NULL, e.getFullDescription().c_str(), "An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
				std::cerr << "An exception has occured: " <<
					e.getFullDescription().c_str() << std::endl;
#endif
			}
		}

        return 0;
    }

#ifdef __cplusplus
}
#endif

#pragma endregion