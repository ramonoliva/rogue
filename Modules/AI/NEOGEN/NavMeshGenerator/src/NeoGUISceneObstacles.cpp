#include "NeoGUISceneObstacles.h"
#include <RogueCore.h>
#include <RogueInterfaces.h>

using namespace Rogue;

const int PANEL_WIDTH = 512;

const String GENERATOR_MODE[] = {
									"Mode NEOGEN", 
									"Mode Manual",
								};

const String INFO_NEOGEN[] =	{
									"Draw the obstacles and press 'G' to generate the NavMesh; press 'H' for help", 
									"NavMesh Generated! Remember to save it ('Ctrl' + 'S'); Press 'Ctrl' + 'N' to create a new scene",
								};

const String INFO_MANUAL[] =	{
									"Draw the obstacles and press 'G' to set the portals; press 'H' for help",
									"Select 2 vertices to create / destroy a portal; press 'G' to create the NavMesh; press 'BackSpace' to return to Edit Mode",
									"NavMesh Generated! Remember to save it ('Ctrl' + 'S'); Press 'Ctrl' + 'N' to create a new scene",
								};

const Ogre::UTFString HELP_TEXT[] =	{
										"OBSTACLE MANIPULATION\n",
										"Create Vertex:	\tLMB\n",
										"Close Obstacle:\tSpace\n",
										"Select Vertex:	\tLMB (on a vertex)\n",
										"Subdivide Edge:\tW\n"
									};


#pragma region CREATION AND DESTRUCTION

NeoGUISceneObstacles::NeoGUISceneObstacles(SdkTrayManager* tMgr) : GUIBase(tMgr) {
	createResultsPanel();
	createPanelInfo();
	showHelpWindow(false);
	_generatorMode = 0;
	_generatorStep = 0;
}

void NeoGUISceneObstacles::createResultsPanel() {
	/*_trayMgr->createLabel(OgreBites::TL_BOTTOMLEFT, "ResultsLabelObstacle", "Results", PANEL_WIDTH);
	Ogre::StringVector paramNames;
	paramNames.push_back("Obstacles");
	paramNames.push_back("Vertices");
	paramNames.push_back("Notches");
	paramNames.push_back("Portals");
	paramNames.push_back("Cells");
	paramNames.push_back("Elapsed time");
	_resultsPanel = _trayMgr->createParamsPanel(OgreBites::TL_BOTTOMLEFT, "_resultsPanelObstacle", PANEL_WIDTH, paramNames);
	for (size_t i = 0; i < paramNames.size(); i++) {
		_resultsPanel->setParamValue(i, "pepito");
	}*/
	_resultsPanel = NULL;
}

void NeoGUISceneObstacles::createPanelInfo() {
	InterfaceRenderManager* renderManager = Core::getManager<InterfaceRenderManager>();
	size_t w, h;
	renderManager->getRenderWindowSize(w, h);
	_panelInfo = _trayMgr->createParamsPanel(OgreBites::TL_BOTTOM, "panelInfo", w, 1);
}

void NeoGUISceneObstacles::createHelpWindow() {

	const int width = 400;
	/*** Obstacle Creation Controls ***/
	OgreBites::Label* label = _trayMgr->createLabel(OgreBites::TL_CENTER, "helpLabel00", "OBSTACLE CREATION");

	Ogre::StringVector paramNames;
	paramNames.push_back("Create obstacle vertex");
	paramNames.push_back("Close obstacle");
	
	Ogre::StringVector paramValues;
	paramValues.push_back("LMB");
	paramValues.push_back("Space");
	
	OgreBites::ParamsPanel* panel = _trayMgr->createParamsPanel(OgreBites::TL_CENTER, "helpWindowPanel00", width, paramNames);
	panel->setAllParamValues(paramValues);
	/*** ***/

	/*** Vertex Manipulation Controls ***/
	label = _trayMgr->createLabel(OgreBites::TL_CENTER, "helpLabel01", "OBSTACLE MANIPULATION");

	paramNames.clear();
	paramNames.push_back("Select vertex");
	paramNames.push_back("Select multiple vertices");
	paramNames.push_back("Select all vertices of an obstacle");
	paramNames.push_back("Select / Deselect all vertices");
	paramNames.push_back("Subdivide selected edges");
	paramNames.push_back("Scale selected vertices");
	paramNames.push_back("Translate selected vertices");
	paramNames.push_back("Save obstacles to file");
	paramNames.push_back("Load obstacles from file");
		
	paramValues.clear();
	paramValues.push_back("RMB");
	paramValues.push_back("Ctrl + RMB");
	paramValues.push_back("LShift + RMB");
	paramValues.push_back("A");
	paramValues.push_back("W");
	paramValues.push_back("S");
	paramValues.push_back("T");
	paramValues.push_back("Ctrl + S");
	paramValues.push_back("Ctrl + O");
	
	panel = _trayMgr->createParamsPanel(OgreBites::TL_CENTER, "helpWindowPanel01", width, paramNames);
	panel->setAllParamValues(paramValues);
	/*** ***/

	/*** NavMesh Generation Controls ***/
	label = _trayMgr->createLabel(OgreBites::TL_CENTER, "helpLabel02", "NAVMESH GENERATION");

	paramNames.clear();
	paramNames.push_back("Change Generation Mode");
	paramNames.push_back("Generate NavMesh");
	paramNames.push_back("Return to Edit Mode");

	paramValues.clear();
	paramValues.push_back("Tab");
	paramValues.push_back("G");
	paramValues.push_back("BackSpace");
	
	panel = _trayMgr->createParamsPanel(OgreBites::TL_CENTER, "helpWindowPanel02", width, paramNames);
	panel->setAllParamValues(paramValues);
	/*** ***/

	/*** Camera Controls ***/
	label = _trayMgr->createLabel(OgreBites::TL_CENTER, "helpLabel03", "CAMERA CONTROLS");

	paramNames.clear();
	paramNames.push_back("Movement");
	paramNames.push_back("Zoom");

	paramValues.clear();
	paramValues.push_back("RMB + W, S, D, A, E, Q");
	paramValues.push_back("MMB");

	panel = _trayMgr->createParamsPanel(OgreBites::TL_CENTER, "helpWindowPanel03", width, paramNames);
	panel->setAllParamValues(paramValues);

	/*** Other Controls ***/
	label = _trayMgr->createLabel(OgreBites::TL_CENTER, "helpLabel04", "OTHER CONTROLS");

	paramNames.clear();
	paramNames.push_back("Show / Hide grid");
	paramNames.push_back("Show / Hide this window");

	paramValues.clear();
	paramValues.push_back("V");
	paramValues.push_back("H");
	
	panel = _trayMgr->createParamsPanel(OgreBites::TL_CENTER, "helpWindowPanel04", width, paramNames);
	panel->setAllParamValues(paramValues);
	/*** ***/
}

#pragma endregion

#pragma region GET AND SET

void NeoGUISceneObstacles::setGeneratorMode(int m) {
	_generatorMode = m;
	updateInfo();
}

void NeoGUISceneObstacles::setGeneratorStep(int s) {
	_generatorStep = s;
	updateInfo();
}

void NeoGUISceneObstacles::showHelpWindow(bool s) {
	if (s) createHelpWindow();
	else _trayMgr->destroyAllWidgetsInTray(OgreBites::TL_CENTER);
	_helpWindowVisible = s;
}

bool NeoGUISceneObstacles::isVisibleHelpWindow() {
	return _helpWindowVisible;
}

void NeoGUISceneObstacles::updateInfo() {
	Ogre::StringVector paramNames;
	paramNames.push_back(GENERATOR_MODE[_generatorMode]);

	Ogre::StringVector paramValues;
	if (_generatorMode == 0) paramValues.push_back(INFO_NEOGEN[_generatorStep]);
	else paramValues.push_back(INFO_MANUAL[_generatorStep]);

	_panelInfo->setAllParamNames(paramNames);
	_panelInfo->setAllParamValues(paramValues);
}

#pragma endregion