#pragma once

#include <ngVector.h>
#include <NeoTypes.h>

namespace NeoOld {

	static NEOGEN::Vector3 toNewNeogen(NEOGEN::Vec3D& vec) {
		return NEOGEN::Vector3(vec.getX(), vec.getY(), vec.getZ());
	}

	static NEOGEN::Vec3D toOldNeogen(NEOGEN::Vector3& vec) {
		return NEOGEN::Vec3D(vec.getX(), vec.getY(), vec.getZ());
	}

};