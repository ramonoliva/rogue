#ifndef __NeoGUISceneObstacles_h_
#define __NeoGUISceneObstacles_h_

#include <RogueGUIBase.h>

class NeoGUISceneObstacles : public Rogue::GUIBase {

	public:

		NeoGUISceneObstacles(Rogue::SdkTrayManager* tMgr);
		virtual ~NeoGUISceneObstacles() {}

		virtual void setGeneratorMode(int m);
		virtual void setGeneratorStep(int s);
		virtual void showHelpWindow(bool s);
		virtual bool isVisibleHelpWindow();

	protected:

		int _generatorMode;			//The mode choosen to generate the NavMesh
		int _generatorStep;			//Indicates the step of the generation process
		bool _helpWindowVisible;	//Indicates if the help window is visible

		OgreBites::ParamsPanel* _panelInfo;
		OgreBites::ParamsPanel* _resultsPanel;

		virtual void createPanelInfo();
		virtual void createResultsPanel();
		virtual void createHelpWindow();

		virtual void updateInfo();

};


#endif