#pragma once

#include <RogueGUIBase.h>
#include <NeoTypes.h>

class NeoGUISceneNavMeshGenerator : public Rogue::GUIBase {

	public:

		NeoGUISceneNavMeshGenerator(Rogue::SdkTrayManager* tMgr);
		virtual ~NeoGUISceneNavMeshGenerator();

		virtual NEOGEN::NeoReal getMaxSlope() {return NEOGEN::NeoReal(_maxSlopeSlider->getValue());}
		virtual NEOGEN::NeoReal getHeight() {return NEOGEN::NeoReal(_heightSlider->getValue());}
		virtual NEOGEN::NeoReal getConvexityRelaxation() {return NEOGEN::NeoReal(_crSlider->getValue());}

		virtual bool isDebugTerrain() {return _debugTerrain->isChecked();};
		virtual bool isDebugTerrainNormals() {return _debugTerrainNormals->isChecked();};
		virtual bool isDebugTerrainEdges() {return _debugTerrainEdges->isChecked();};
		virtual bool isDebugTerrainOctree() { return _debugTerrainOctree->isChecked();};
		
		virtual bool isDebugNavMesh() {return _debugNavMesh->isChecked();};
		virtual bool isDebugNavMeshEdges() {return _debugNavMeshEdges->isChecked();};
		virtual bool isDebugNavMeshNotches() {return _debugNavMeshNotches->isChecked();};
		virtual bool isDebugNavMeshCells() {return _debugNavMeshCells->isChecked();};
		virtual bool isDebugNavMeshOctree() { return _debugNavMeshOctree->isChecked(); };

	protected:

		//NavMesh Options components
		OgreBites::Slider* _maxSlopeSlider;
		OgreBites::Slider* _heightSlider;
		OgreBites::Slider* _crSlider;

		//Terrain Debug components
		OgreBites::CheckBox* _debugTerrain;
		OgreBites::CheckBox* _debugTerrainNormals;
		OgreBites::CheckBox* _debugTerrainEdges;
		OgreBites::CheckBox* _debugTerrainOctree;
		
		//NavMesh Debug components
		OgreBites::CheckBox* _debugNavMesh;
		OgreBites::CheckBox* _debugNavMeshEdges;
		OgreBites::CheckBox* _debugNavMeshNotches;
		OgreBites::CheckBox* _debugNavMeshCells;
		OgreBites::CheckBox* _debugNavMeshOctree;
								
		virtual OgreBites::Slider* createSlider(OgreBites::TrayLocation tLocation, const std::string& name, const std::string& title, float min, float max, int resolution = 1);

		virtual void createNavMeshOptionsTab();
		virtual void createTerrainDebugTab();
		virtual void createNavMeshDebugTab();

};
