#pragma once

#include "NeoDebuggers.h"
#include "NeoNavMeshGenerator.h"
#include "NeoTypes.h"

#include <RogueDebuggerBase.h>
#include <RogueDebuggerPoint.h>
#include <RogueDebuggerPolyLine.h>

namespace NEOGEN {

	typedef std::vector<Rogue::DebuggerPointUPtr> DebuggerPoints;
	
	class DebuggerNeoNavMeshGenerator : public Rogue::DebuggerBase {

		public:

			DebuggerNeoNavMeshGenerator();

			virtual void setVisible(bool visible);

			virtual void clear();

			virtual void update(NeoNavMeshGenerator* generator, const NEOGEN::Vector3& offset = NEOGEN::Vector3(0,0,0));


		protected:

			NeoNavMeshGenerator* _generator;
			
			DebuggerPoints _debuggerNotches;
			Rogue::DebuggerPolyLineUPtr _debuggerAOI;

	};

};