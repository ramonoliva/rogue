#ifndef __NeoSceneObstacles_h_
#define __NeoSceneObstacles_h_

#include <RogueSceneGame.h>

#include <ngNavMeshGeneratorBase.h>
#include <ngNavMeshGenerator.h>
#include <ngNavMeshGeneratorManual.h>

#include "NeoGUISceneObstacles.h"

class SceneObstacles : public Rogue::SceneGame {

	public:

		virtual void createScene();
		virtual void destroyScene();

	protected:

		enum GeneratorType {
			NEOGEN,
			MANUAL,
			NUM_TYPES,
		};
		GeneratorType _generatorType;

		typedef std::vector<NEOGEN::Vec3D> PointList;
		typedef std::vector<Rogue::ManualObject*> ManualObstacleList;
		typedef std::set<Rogue::GameObject*> GameObjectSet;
		typedef std::pair<Rogue::GameObject*, Rogue::GameObject*> GameObjectPair;
		typedef std::set<GameObjectPair> GameObjectPairSet;
		typedef std::vector<Rogue::GameObject*> ObstacleList;
		typedef std::pair<Rogue::Vector3, float> ScaleInfo;
		typedef std::map<Rogue::GameObject*, ScaleInfo> ScaleInfoMap;
		typedef std::map<int, Rogue::GameObject*> IntGameObjectMap;

		enum State {
			ST_IDLE,
			ST_CREATING_OBSTACLE,
			ST_MOVING_VERTEX,
			ST_SCALING_VERTICES,
			ST_CREATING_PORTAL,
			ST_NAVMESH_GENERATED,
		};
		State _state;

		ObstacleList _obstacles;			
		size_t _currentObstacleID;

		int _newVertexID;

		NEOGEN::NavMeshGeneratorBase* _navMeshGenerator;	//The NavMeshGenerator used to create the NavMesh
		NEOGEN::NavMeshGenerator* _neogenGenerator;					
		NEOGEN::NavMeshGeneratorManual* _manualGenerator;

		Rogue::GameObject* _grid;
		GameObjectPairSet _selectedEdges;
		Rogue::Vector3 _lastHitPosition;

		GameObjectSet _selectedVertices;
		Rogue::Vector3 _selectedVerticesCenter;	//The center of ALL the selected vertices
		ScaleInfoMap _selectedVerticesScaleInfo;
		float _currentVertexRadius;

		Rogue::ManualObject* _manualPositionToMouse;
		Rogue::ManualObject* _manualPortals;
		ManualObstacleList _manualObstacles;
		
		GameObjectPairSet _portals;
		IntGameObjectMap _vertices;

		NeoGUISceneObstacles* _gui;

		virtual void clearScene();

		virtual void clearSelection();
		virtual void clearManualObjects();
		virtual void recomputeSelectedVerticesCenter();
		
		virtual void initCamera();
		virtual void initObstacles();
		virtual void initScaleInfo();

		virtual void setState(State s);
		virtual void setGeneratorType(GeneratorType t);
		virtual void changeGeneratorType();
		
		virtual void createGrid();
		virtual void createNavMeshGenerators();
		virtual void createNavMesh();

		virtual bool edgeInList(Rogue::GameObject* v1, Rogue::GameObject* v2, GameObjectPairSet& list);

		virtual bool isSelectedVertex(Rogue::GameObject* v);
		virtual bool isSelectedVertex(int id);
		virtual bool isSelectedEdge(Rogue::GameObject* v1, Rogue::GameObject* v2);
		virtual bool isPortalEdge(Rogue::GameObject* v1, Rogue::GameObject* v2);
		virtual void addSelectedVertex(Rogue::GameObject* v);
		virtual void addSelectedEdge(Rogue::GameObject* v1, Rogue::GameObject* v2);
		virtual void addPortal(Rogue::GameObject* v1, Rogue::GameObject* v2);
		virtual void selectObstacleVertices(Rogue::GameObject* obstacle);
		virtual void selectAllVertices();

		virtual void getObstacleDescription(NEOGEN::MapParser::ObstacleDescription& result);
		virtual void getPortalDescription(NEOGEN::MapParser::PortalDescription& result);
		virtual void getObstaclesFromDescription(NEOGEN::MapParser::ObstacleDescription& obstacleDescription);
		virtual void getPortalsFromDescription(NEOGEN::MapParser::PortalDescription& portalDescription);

		virtual void subdivideSelectedEdges();
		virtual Rogue::GameObject* subdivideEdge(Rogue::GameObject* v1, Rogue::GameObject* v2);

		virtual void update();
		virtual void updateStateMovingVertices();
		virtual void updateStateScalingVertices();
		virtual void updateStateCreatingPortal();
		virtual void scaleSelectedVertices(float scaleFactor);
		virtual void scaleVerticesShape();

		virtual void drawObstacles();
		virtual void drawObstacle(size_t obstacleID, bool close);
		virtual void drawPortals();
		
		virtual Rogue::GameObject* createObstacleVertex(size_t obstacleID, size_t vertexID, NEOGEN::Vec3D& position, Rogue::GameObject* nextSibling = NULL);
		virtual Rogue::GameObject* createObstacle();
		virtual NEOGEN::Poly* createPolygon(Rogue::GameObjectList& obstacle);

		virtual void processMouseEvents();
		virtual void processKeyboardEvents();

};

#endif