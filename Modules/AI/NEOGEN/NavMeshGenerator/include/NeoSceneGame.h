#pragma once

#include "ngNavMeshGenerator.h"
#include "ngMapParser.h"
#include <OgreManualObject.h>
#include "ngGlobalVars.h"
#include "ngObstacleSolver.h"
#include "ngVoxelizer.h"
#include "ngVoxelizerCPU.h"
#include "NeoGUISceneGame.h"
#include "ngVertex.h"

#include <RogueInterfaceRenderManager.h>
#include <RogueInterfaceInputManager.h>
#include <RogueInterfaceCameraManager.h>
#include <RogueInterfaceGUIManager.h>

#include <RogueScene.h>

enum DrawModeEnum {
	DRAW_MODE_SHADED, 
	DRAW_MODE_NORMAL_MAP, 
	DRAW_MODE_DEPTH_MAP, 
	DRAW_MODE_NORMAL_FILTER,
	DRAW_MODE_POS
};

enum ViewEnum {
	VIEW_TOP, 
	VIEW_FRONT,
	VIEW_SIDE
};

class NeoSceneGame : public Rogue::Scene {
public:
    NeoSceneGame();
    virtual ~NeoSceneGame();

protected:

	NEOGEN::NavMeshGenerator* nmGen;
	NEOGEN::ObstacleSolver* oSolver;

	virtual void createScene();
	virtual void destroyScene();
	virtual void update();
	
    void updateModelRotation(Ogre::Real timeLF);
	
	void selectWalkableArea(float viewX, float viewY);

private:

	Rogue::InterfaceRenderManager* _renderManager;
	Rogue::InterfaceInputManager* _inputManager;
	Rogue::InterfaceCameraManager* _cameraManager;
	Rogue::InterfaceGUIManager* _guiManager;
	NeoGUISceneGame* _guiSceneGame;

	Rogue::DeferredPass* _finalDeferredPass;
	Rogue::DeferredChain* _mainDeferredChain;
	Rogue::DeferredChain* _levelDeferredChain;
	Rogue::DeferredChain* _normalDepthMapDeferredChain;

	enum {
		ALL_FACES = 7,			
		WALKABLE_FACES = 1,
		OBSTACLE_FACES = 2,
		PERPENDICULAR_FACES = 4
	} ApplicationActions;
	
	float obstacleSolverTime;
	/**************************************************/
	/* OPTIONS */
	/**************************************************/
	bool boardVisible;	//El board �s visible?
	
	/***************************************************/
	/* SCENE MODEL */
	/***************************************************/
	Ogre::SceneNode* nodeModel;			//SceneNode que cont� el model de l'escenari
	Ogre::Entity* entModel;				//Entitat que cont� el model de l'escenari
	//Ogre::SceneNode* nodePerpendicularFaces;		//SceneNode que cont� les cares del model que s�n perpendiculars
	Ogre::Vector3 camPosPerspective;	//Posici� de la c�mera en mode 3D
	Ogre::Vector3 camPos2D;				//Posici� de la c�mera en mode 2D
	float timeAnim;
	bool rotateModel;
	float currentModelAngle;
	int showFaceType;
	/***************************************************/
	/* END SCENE MODEL */
	/***************************************************/
	/********************************************************/
	/* MODEL FACES */
	/********************************************************/
	NEOGEN::ngTriangleList walkableFaces;
	NEOGEN::ngTriangleList obstacleFaces;
	NEOGEN::ngTriangleList perpendicularFaces;
	Ogre::ManualObject* manualWalkableFaces;
	Ogre::ManualObject* manualObstacleFaces;
	Ogre::ManualObject* manualPerpendicularFaces;		//Manual object on pinto les cares perpendiculars
	Ogre::SceneNode* nodeWalkableFaces;
	Ogre::SceneNode* nodeObstacleFaces;
	Ogre::SceneNode* nodePerpendicularFaces;
	/********************************************************/
	/* END PERPENDICULAR FACES */
	/********************************************************/

	Ogre::AxisAlignedBox bbModel;		//Bounding Box del model
	Ogre::Vector3 bbModelDim;			//Dimensions de la Bounding Box del model
	Ogre::Vector3 bbModelCenter;		//Centre de la Bounding Box del model

	Ogre::RaySceneQuery *raySceneQuery;	// The ray scene query pointer
	
	NEOGEN::VoxelizerCPU* voxelizer;
	NEOGEN::VoxelizerCPU* voxelizerCPU;
	
	typedef std::vector<Ogre::String> TexList;
	
	Ogre::ManualObject* mObject;
	std::vector<NEOGEN::Vertex> listVert;
	std::vector<NEOGEN::Poly*> polyData;
	std::list<Ogre::SceneNode*> vertNodes;
	std::list<Ogre::SceneNode*> edgeNodes;
	Ogre::SceneNode* vertSelected;

	bool showAdjacencyGraph;

	void loadObj(const std::string& fileName);
	void createManualMesh(const std::string& meshName, std::vector<Ogre::Vector3>& vertices, std::vector<std::vector<int>>& faces);
	void setupDeferredMode();
	void loadModel(const std::string& fileName);
	void classifyModelFaces();
	void getMeshInformation(const Ogre::MeshPtr mesh, size_t &vertex_count, Ogre::Vector3* &vertices, size_t &index_count, unsigned long* &indices);
	void setModelVisible(bool visible);
	void setModelPosition(Ogre::Vector3& pos);
	void setModelFacesVisible(unsigned int mask, bool b);
	void setManualMaterial(Ogre::ManualObject* manual, const Ogre::String& matName);

	void setDrawMode(int sel);

	void init2DMode();
	void init3DMode();

	void initPerspectiveCamera(Ogre::Vector3& camPos);
	
	void initBottomView();
	void initTopView();
	void initFrontView();
	void initSideView();

	void voxelization();
	void voxelizationCPU();

	void layerSelection();
	void fillLayerTexture();
	void levelRefinement();
	
	void setupBitmask();
		
	void setInfo();
	
	void initNavMeshData();
	void generateNavMesh();
	void generateNavMeshFull();
	void generateNavMeshStepByStep();
	void modifyVertScale(float modifier);
	void scaleVertNodes();

	virtual void processMouseEvents();
	virtual void processKeyboardEvents();
	virtual void processGUIEvents();

	void printStageInfo(const std::string& stateName, unsigned long timeBeforeMS, unsigned long timeAfterMS);
};
