#ifndef __NeoHelpers_h_
#define __NeoHelpers_h_

#include <RogueTypes.h>

namespace NEOGEN {

	static void imageToDepthMap(Rogue::Image& img) {
		//Funci� que simplement �s per visualitzar millor els mapes resultants del proc�s de refinament de la layer
		Rogue::Color color;
		float depth;
		for (size_t x = 0; x < img.getWidth(); x++) {
			for (size_t y = 0; y < img.getHeight(); y++) {
				color = img.getColourAt(x, y, 0);
				depth = color.a;
				color.r = depth;
				color.g = depth;
				color.b = depth;
				color.a = 1.0f;
				img.setColourAt(color, x, y, 0);
			}
		}
	}

	static void imageToContourMap(Rogue::Image& img) {
		//Funci� que simplement �s per visualitzar millor els mapes resultants del proc�s de refinament de la layer
		Rogue::Color color;
		for (size_t x = 0; x < img.getWidth(); x++) {
			for (size_t y = 0; y < img.getHeight(); y++) {
				color = img.getColourAt(x, y, 0);
				if (Ogre::Math::RealEqual(color.r, 0.0f)) {
					img.setColourAt(Ogre::ColourValue::Black, x, y, 0);
				}
				else {
					if (Ogre::Math::RealEqual(color.g, 1.0f)) {
						img.setColourAt(Ogre::ColourValue::White, x, y, 0);
					}
					else {
						img.setColourAt(Ogre::ColourValue::Green, x, y, 0);
					}
				}
			}
		}
	}

	static void imageToPortalMap(Rogue::Image& img) {
		//Funci� que simplement �s per visualitzar millor els mapes resultants del proc�s de refinament de la layer
		Rogue::Color color;
		for (size_t x = 0; x < img.getWidth(); x++) {
			for (size_t y = 0; y < img.getHeight(); y++) {
				color = img.getColourAt(x, y, 0);
				if (Ogre::Math::RealEqual(color.r, 0.0f)) {
					img.setColourAt(Ogre::ColourValue::Black, x, y, 0);
				}
				else {
					if (Ogre::Math::RealEqual(color.r, 1.0f)) {
						img.setColourAt(Ogre::ColourValue::White, x, y, 0);
					}
					else {
						img.setColourAt(Ogre::ColourValue::Red, x, y, 0);
					}
				}
			}
		}
	}

};

#endif