#pragma once

#include <NeoTypes.h>
#include <RogueDebuggers.h>

namespace NEOGEN {

	class DebuggerFaces;
	typedef std::shared_ptr<DebuggerFaces> DebuggerFacesSPtr;
	typedef std::unique_ptr<DebuggerFaces> DebuggerFacesUPtr;
	typedef std::weak_ptr<DebuggerFaces> DebuggerFacesWPtr;

	class DebuggerNeoMesh;
	typedef std::shared_ptr<DebuggerNeoMesh> DebuggerNeoMeshSPtr;
	typedef std::unique_ptr<DebuggerNeoMesh> DebuggerNeoMeshUPtr;
	typedef std::weak_ptr<DebuggerNeoMesh> DebuggerNeoMeshWPtr;

	class DebuggerNeoTerrain;
	typedef std::shared_ptr<DebuggerNeoTerrain> DebuggerNeoTerrainSPtr;
	typedef std::unique_ptr<DebuggerNeoTerrain> DebuggerNeoTerrainUPtr;
	typedef std::weak_ptr<DebuggerNeoTerrain> DebuggerNeoTerrainWPtr;

	class DebuggerNeoNavMesh;
	typedef std::shared_ptr<DebuggerNeoNavMesh> DebuggerNeoNavMeshSPtr;
	typedef std::unique_ptr<DebuggerNeoNavMesh> DebuggerNeoNavMeshUPtr;
	typedef std::weak_ptr<DebuggerNeoNavMesh> DebuggerNeoNavMeshWPtr;

	class DebuggerNeoNavMeshGenerator;
	typedef std::shared_ptr<DebuggerNeoNavMeshGenerator> DebuggerNeoNavMeshGeneratorSPtr;
	typedef std::unique_ptr<DebuggerNeoNavMeshGenerator> DebuggerNeoNavMeshGeneratorUPtr;
	typedef std::weak_ptr<DebuggerNeoNavMeshGenerator> DebuggerNeoNavMeshGeneratorWPtr;
};

