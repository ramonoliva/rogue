#pragma once

#include <RogueDebuggerBase.h>
#include <NeoMesh.h>
#include "NeoDebuggers.h"

namespace NEOGEN {

	class DebuggerFaces : public Rogue::DebuggerBase {

		public:
			
			DebuggerFaces();
			virtual ~DebuggerFaces();

			virtual void addTriangles(const Triangles& triangles);

			virtual void setVisible(bool visible);

			virtual void setMaterialName(const std::string& materialName);

			virtual void setOffset(const NEOGEN::Vector3& offset);

			virtual void clear();

			virtual void update();

		protected:

			typedef std::map<NeoVertex*, uint> IndexMap;

			Rogue::ManualObject* _manualPolygons;
			Triangles _polygons;
			IndexMap _indexMap;
			uint _newIndexID;

			std::string _materialName;
			Vector3 _offset;

			virtual uint createIndex(NeoVertex* v);
			
			virtual uint getIndex(NeoVertex* v);
			virtual bool hasIndex(NeoVertex* v);
			
	};

};