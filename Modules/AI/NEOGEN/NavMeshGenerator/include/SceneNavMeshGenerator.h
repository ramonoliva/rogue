#pragma once

#include <RogueScene.h>
#include <NeoTypes.h>
#include <NeoTerrain.h>
#include <NeoNavMeshGenerator.h>

#include "NeoDebuggers.h"
#include "DebuggerNeoNavMeshGenerator.h"

#include "NeoGUISceneNavMeshGenerator.h"

class SceneNavMeshGenerator : public Rogue::Scene {

	public:

		SceneNavMeshGenerator();
		
		virtual ~SceneNavMeshGenerator();

		virtual void createScene();

		virtual void update();

	protected:

		typedef std::map<NEOGEN::Vector2, size_t, NEOGEN::Vector2Cmp> Point2IndexMap;
		typedef std::map<NEOGEN::Vector3, size_t, NEOGEN::Vector3Cmp> Point3IndexMap;

		enum State {
			IDLE,	
			TERRAIN_MODEL_LOADED,
			TERRAIN_CREATED,
			SLOPE_CONSTRAINT_APPLIED,
			CEIL_CONSTRAINTS_APPLIED,
			NAV_MESH_INITIALIZED,
			CONVEXITY_RELAXATION_APPLIED,
			PORTALS_CREATED,
			END,
		};
		State _state;

		Rogue::Timer _timer;
		unsigned long _timeGlobal;

		NeoGUISceneNavMeshGenerator* _gui;

		Rogue::GameObject* _model;
		NEOGEN::NeoTerrainSPtr _terrain;
		NEOGEN::NeoNavMeshSPtr _navMesh;
		NEOGEN::NeoNavMeshGeneratorSPtr _navMeshGenerator;
		
		NEOGEN::DebuggerNeoTerrainUPtr _terrainDebugger;
		NEOGEN::DebuggerNeoNavMeshUPtr _navMeshDebugger;
		NEOGEN::DebuggerNeoNavMeshGeneratorUPtr _navMeshGeneratorDebugger;

		NEOGEN::NeoReal _oldConvexityRelaxation;
		
		virtual void setState(State newState);

		virtual void loadTerrainModel();
		virtual bool loadTerrainModel(const std::string& path, const std::string& baseName, const std::string& extension);
		
		virtual Rogue::Entity* importModel(const std::string& path, const std::string& baseName, const std::string& extension);
		virtual Rogue::Entity* importModelOBJ(const std::string& path, const std::string& baseName, const std::string& extension);
		virtual Rogue::Entity* importModelDAT(const std::string& path, const std::string& baseName, const std::string& extension);
		virtual Rogue::Entity* importModelBDM(const std::string& path, const std::string& baseName, const std::string& extension);
		virtual bool readlineBDM(std::ifstream& bdmfile, std::string& result);
		virtual int countfieldBDM(const std::string& source, char delim);
		virtual std::string findfieldBDM(std::string& source);

		virtual void setCameraTopView();
		virtual void setCameraFrontView();
		virtual void setCameraRightView();
		virtual void setCameraView(const Ogre::Vector3& centerOffset, const Ogre::Quaternion& rot);

		virtual void createTerrain();

		virtual void applySlopeConstraint();
		virtual void applyHeightConstraint();
		virtual void initNavMesh();
		virtual void applyConvexityRelaxation();
		virtual void breakRealNotches();
		virtual void createCells();

		virtual void updateDebug();
		virtual void saveStats(const std::string& path);
		
};



