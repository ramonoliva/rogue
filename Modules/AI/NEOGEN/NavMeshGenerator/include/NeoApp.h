#ifndef __NeoApp_h_
#define __NeoApp_h_

#include <RogueGameApp.h>

class NeoApp : public Rogue::GameApp {
	public:
		NeoApp();
		NeoApp(const std::string& iFileName, const std::string& oFileName);

		virtual ~NeoApp();

	protected:

		std::string _inputFileName;
		std::string _outputFileName;

		virtual void registerScenes();
};

#endif
