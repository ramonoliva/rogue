#pragma once

#include "DebuggerNeoMesh.h"
#include "NeoTerrain.h"

namespace NEOGEN {

	class DebuggerNeoTerrain : public DebuggerNeoMesh {

		public:

			DebuggerNeoTerrain();
			virtual ~DebuggerNeoTerrain();

			virtual void setVisible(bool v);
			virtual void setVisibleFacesWalkable(bool v);
			virtual void setVisibleFacesObstacle(bool v);

			virtual void clear();

			virtual void update(NeoTerrain* terrain);

		protected:

			DebuggerFacesUPtr _debuggerFacesWalkable;
			DebuggerFacesUPtr _debuggerFacesObstacle;

			virtual void updateDebuggers();

			virtual void refreshDebuggerFaces(NeoFace* face);
	};

};