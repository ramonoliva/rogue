#ifndef __NeoDeferredPasses_h_
#define __NeoDeferredPasses_h_

#include "RogueDeferredPass.h"
#include "NeoHelpers.h"

/****************************************************************/
/* FILL LAYER TEXTURE */
/****************************************************************/
class DPFillLayerTexture : public Rogue::DeferredPass {
	
	public: 
		
		DPFillLayerTexture(size_t w, size_t h);

		void postRender();

	private:

		Rogue::RenderTexture* _levelRenderTex;
		Rogue::TexturePtr _auxTex;
};

/****************************************************************/
/* PORTAL EXPANSION */
/****************************************************************/
class DPPortalExpansion : public Rogue::DeferredPass {
	public: 
		
		DPPortalExpansion();

		void postRender();

	private:

		Rogue::RenderTexture* _levelRenderTex;
};

/****************************************************************/
/* LEVEL REFINEMENT */
/****************************************************************/
class DPLevelRefinement : public Rogue::DeferredPass {

	public:

		DPLevelRefinement(size_t w, size_t h);

		void postRender();

	private:

		Rogue::RenderTexture* _normalDepthMapRenderTex;
		Rogue::TexturePtr _intermediateTex0;
};

/****************************************************************/
/* LEVEL REFINEMENT PERPENDICULAR */
/****************************************************************/
class DPLevelRefinementPerpendicular : public Rogue::DeferredPass {

	public:

		DPLevelRefinementPerpendicular(size_t w, size_t h);

		void postRender();

	private:

		Rogue::RenderTexture* _normalDepthMapRenderTex;
		Rogue::TexturePtr _intermediateTex1;
};

/****************************************************************/
/* NORMAL DEPTH MAP FUSION */
/****************************************************************/
class DPNormalDepthMapFusion : public Rogue::DeferredPass {

	public:

		DPNormalDepthMapFusion();

		void postRender();

	private:

		Rogue::RenderTexture* _normalDepthMapRenderTex;
		Rogue::TexturePtr _intermediateTex0;

};

/****************************************************************/
/* CONTOUR DETECTION */
/****************************************************************/
class DPContourDetection : public Rogue::DeferredPass {

	public:

		DPContourDetection(size_t w, size_t h);

		void postRender();

	private:

		Rogue::RenderTexture* _contourRenderTex;
};

#endif