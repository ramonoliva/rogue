#pragma once

#include <RogueDebuggerBase.h>
#include <NeoTypes.h>

namespace NEOGEN {

	class DebuggerPolygons : public Rogue::DebuggerBase {

		public:
			
			DebuggerPolygons(const std::string& name, const std::string& materialName = "DiffuseGrey");
			virtual ~DebuggerPolygons();

			virtual void setMaterialName(const std::string& materialName);
			
			virtual void clear();

			virtual void update();
			virtual void update(Polygons3& polygons);

		protected:

			Rogue::ManualObject* _manualPolygons;
			Polygons3 _polygons;
			
			std::string _materialName;
			
	};

};