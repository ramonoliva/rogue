#pragma once

#include "NeoDebuggers.h"

#include <RogueDebuggerBase.h>
#include <RogueDebuggerPoint.h>
#include <RogueDebuggerPolyLine.h>
#include "DebuggerFaces.h"
#include <NeoMesh.h>

#include <RogueCore.h>
#include <RogueInterfaceRenderManager.h>

namespace NEOGEN {

	class DebuggerNeoMesh : public Rogue::DebuggerBase {

		public:

			DebuggerNeoMesh();
			virtual ~DebuggerNeoMesh();

			virtual void setVisible(bool v);
			virtual void setVisibleFaces(bool v);
			virtual void setVisibleNormals(bool v);
			virtual void setVisibleEdges(bool v);
			virtual void setVisibleOctree(bool v);
			
			virtual void update(NeoMesh* mesh, const NEOGEN::Vector3& offset = Vector3(0,0,0));

		protected:

			NeoMesh* _mesh;
			
			DebuggerFacesUPtr _debuggerFaces;
			Rogue::DebuggerPolyLineUPtr _debuggerEdges;
			Rogue::DebuggerPolyLineUPtr _debuggerEdgesObstacle;
			Rogue::DebuggerPolyLineUPtr _debuggerEdgesPortal;
			Rogue::DebuggerPolyLineUPtr _debuggerNormals;
			Rogue::DebuggerPolyLineUPtr _debuggerOctree;

			virtual void clear();

			virtual void update();
			virtual void updateDebuggers();

			virtual void refreshDebuggers();
			virtual void refreshDebuggerFaces(NeoFace* face);
			virtual void refreshDebuggerEdges(NeoFace* face);
			virtual void refreshDebuggerNormals(NeoFace* face);
			virtual void refreshDebuggerOctree();
			virtual void refreshDebuggerOctreeNode(NeoOctreeNodeFace* node);
	};

};