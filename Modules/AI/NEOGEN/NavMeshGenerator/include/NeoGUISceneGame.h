#ifndef __NeoGUISceneGame_h_
#define __NeoGUISceneGame_h_

#include <RogueGUIBase.h>

class NeoGUISceneGame : public Rogue::GUIBase {

	public:

		NeoGUISceneGame(Rogue::SdkTrayManager* tMgr);
		virtual ~NeoGUISceneGame();

		void setInfo(Ogre::StringVector& paramNames, Ogre::StringVector& paramValues) {
			_modePanel->setAllParamNames(paramNames);
			_modePanel->setAllParamValues(paramValues);
		}
		int getDrawMode() {return _menuDrawMode->getSelectionIndex();}
		int getModelType() {return _menuModelType->getSelectionIndex();}
		float getConvexDist() {return _convexDistSlider->getValue();}
		float getResolution() {return _resolutionSlider->getValue();}
		float getMaxSlope() {return _maxSlopeSlider->getValue();}
		float getMaxStep() {return _maxStepSlider->getValue();}

		bool isGPUModeChecked() {return _GPUModeCheckBox->isChecked();}

		void setVisibleResultsPanel(bool visible) {setVisibleWidget(_resultsPanel, visible);}
		void setVisibleModePanel(bool visible) {setVisibleWidget(_modePanel, visible);}
		void setVisibleResolutionSlider(bool visible) {setVisibleWidget(_resolutionSlider, visible);}
		void setVisibleConvexDistSlider(bool visible) {setVisibleWidget(_convexDistSlider, visible);}
		void setVisibleMaxStepSlider(bool visible) {setVisibleWidget(_maxStepSlider, visible);}
		void setVisibleMaxSlopeSlider(bool visible) {setVisibleWidget(_maxSlopeSlider, visible);}
		void setVisibleModelTypeMenu(bool visible) {setVisibleWidget(_menuModelType, visible);}
		void setVisibleDrawModeMenu(bool visible) {setVisibleWidget(_menuDrawMode, visible);}
		void setVisibleGPUModeCheckBox(bool visible) {setVisibleWidget(_GPUModeCheckBox, visible);}

	private:

		OgreBites::ParamsPanel* _resultsPanel;
		OgreBites::ParamsPanel* _modePanel;
		OgreBites::Slider* _resolutionSlider;
		OgreBites::Slider* _convexDistSlider;
		OgreBites::Slider* _maxStepSlider;
		OgreBites::Slider* _maxSlopeSlider;
		OgreBites::SelectMenu* _menuModelType;
		OgreBites::SelectMenu* _menuDrawMode;
		OgreBites::CheckBox* _GPUModeCheckBox;

		void createInfoModeWindow();
		void createModelOptionsWindow();
		void createNavmeshOptionsWindow();
		void createBuildOptionsWindow();
		void createResultsWindow();
		void setVisibleWidget(Rogue::Widget* widget, bool visible) {
			if (visible) widget->show();
			else widget->hide();
		}
	

};

#endif