#pragma once

#include "DebuggerNeoMesh.h"

namespace NEOGEN {

	typedef std::map<std::string, DebuggerFacesUPtr> DebuggerCellMap;
	typedef std::map<NeoCell*, std::string> CellMaterialMap;
	typedef std::vector<std::string> MaterialNames;

	class DebuggerNeoNavMesh : public DebuggerNeoMesh {

	public:

		enum DebugMode {
			NEOGEN_3D,
			NEOGEN_ML,
		};

		DebuggerNeoNavMesh();

		virtual void setVisible(bool v);
		virtual void setVisibleCells(bool v);
		virtual void setVisiblePortals(bool v);

		virtual DebugMode getDebugMode();
		virtual void setDebugMode(DebugMode mode);

	protected:

		DebugMode _debugMode;

		DebuggerCellMap _debuggerCells;
		MaterialNames _materialNames;
		CellMaterialMap _cellMaterialMap;

		virtual bool isValidMaterial(NeoCell* cell, const std::string& matName);
		virtual std::string getCellMaterial(NeoCell* cell);

		virtual void clear();

		virtual void updateDebuggers();

		virtual void refreshDebuggers();
		virtual void refreshDebuggerNormals(NeoFace* face) {};
		virtual void refreshDebuggerCell(NeoCell* cell, const std::string& matName);

	};

};