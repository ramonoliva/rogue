#version 120

//uniform variables
uniform sampler2D shadowMask;

//varying variables
varying vec2 uv;

//Shows the shadow Mask
void main() {
	float shadowCoeff = texture2D(shadowMask, uv).x;
	if (shadowCoeff < 0.0) {
		if (shadowCoeff > -1.0) gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
		else if (shadowCoeff > -2.0) gl_FragColor = vec4(0.0, 1.0, 0.0, 1.0);
	}
	else gl_FragColor = vec4(shadowCoeff, shadowCoeff, shadowCoeff, 1.0);
}