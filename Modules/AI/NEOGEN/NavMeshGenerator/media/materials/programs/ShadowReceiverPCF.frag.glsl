#version 120

/////////////////////////////////////////////////////
// IMPORTED AUXILIARY FUNCTIONS: utils.frag.glsl
///////////////////////////////////////////////////// 
bool realLEqual(float a, float b, float epsilon);

/////////////////////////////////////////////////////
// ENTRY POINT
///////////////////////////////////////////////////// 

//uniform variables
uniform mat4 uViewSpaceToShadowMapMatrix;	//Matrix to transform positions given in View Space to Shadow Map Space (so, it is the concatenation of the inverse of the view matrix and the shadow map matrix)
uniform vec3 uLightPosVS;
uniform float uShadowDepthBias;
uniform float uShadowDepthBiasClamp;
uniform float texelSize; 
uniform int kernelSize;
uniform sampler2D shadowMap;
uniform sampler2D viewSpacePosMap;
uniform sampler2D normalMap;

//varying variables
varying vec2 uv;

float computeShadowCoeff(vec4 shadowUV, vec3 lightDir) {
	if ((abs(shadowUV.x) > 1.0) || (abs(shadowUV.y) > 1.0) || (shadowUV.w < 0.0)) return 1.0;	//Invalid texCoords. Ignore Shadow Coeff Computation
	float shadowMapValue = texture2D(shadowMap, shadowUV.xy).x;									//The depth value stored in the ShadowMap
	if (shadowMapValue == 0.0) return 1.0;														//The depth value stored in the ShadowMap corresponds to a background fragment. Ignore it. 
	
	//If we arrive here is because shadowMapValue and shadowUV are valid. 
	//Determine if the fragment is in shadow or not, by applying a certain bias
	//that depends on the curvature of the surface
	vec3 normal = texture2D(normalMap, uv).xyz;					//Fragment's normal
	float cosTheta = clamp(dot(normal, lightDir), 0.0, 1.0);	//cosTheta is dot( n,l ), clamped between 0 and 1
	float bias = uShadowDepthBias * tan(acos(cosTheta)); 	
	bias = clamp(bias, 0, uShadowDepthBiasClamp);
	if (realLEqual(shadowUV.z, shadowMapValue, bias)) return 1.0;	//The fragment is lit
	return 0.0;
}

void main() {
    //Get the non-homobeneous texture coordinates (divide by w, equivalent to the perspective division in positions)
	vec3 posVS = texture2D(viewSpacePosMap, uv).xyz;				//Fragment's View Space Position
	vec4 shadowUV = uViewSpaceToShadowMapMatrix * vec4(posVS, 1.0); //Fragment's Shadow Map Coords
    shadowUV.xy = shadowUV.xy / shadowUV.w;							//Texture Coordinates for the Shadow Map (perspective division)
	vec3 lightDir = uLightPosVS - posVS;
	shadowUV.z = length(lightDir);
	lightDir /= shadowUV.z;	//Normalized Light Direction
	
	float shadowCoeff = computeShadowCoeff(shadowUV, lightDir);
	float halfSamples = (float(kernelSize) * 0.5) - 0.5;
	vec2 offset;
	for (int y = 0; y < kernelSize; y++) {		
		for (int x = 0; x < kernelSize; x++) {	
			offset.x = float(x) - halfSamples;
			offset.y = float(y) - halfSamples;
			shadowCoeff += computeShadowCoeff(vec4(shadowUV.xy + (offset * texelSize), shadowUV.zw), lightDir);
		}
	}
	gl_FragColor = vec4(shadowCoeff / float(1.0 + (kernelSize * kernelSize)));
}