#version 120

//Vertex attributes
attribute vec4 vertex;

//uniform variables
uniform mat4 uWorldMatrix;			//Transform from Object Space to World Space
uniform mat4 uLightViewMatrix;		//Transform from Object Space to Light Space
uniform mat4 uLightViewProjMatrix;	//Transform from World Space to Light's Clip Space

//varying variables
varying vec3 oLightSpacePos;

//Vertex shader to cast shadows. A depth render is done from the point of view of the light. 
void main() {
	vec4 worldPos = uWorldMatrix * vertex;				//World Space position	
	oLightSpacePos = (uLightViewMatrix * worldPos).xyz;	//Light Space position
	gl_Position = uLightViewProjMatrix * worldPos;		//Light Clip Space position
}