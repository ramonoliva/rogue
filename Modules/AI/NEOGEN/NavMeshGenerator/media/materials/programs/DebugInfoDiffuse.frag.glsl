#version 120

//uniform parameters
uniform sampler2D diffuseMap;	

//varying parameters
varying vec2 uv;

//Shows the View Space Normals of the scene
void main() {
	vec3 color = texture2D(diffuseMap, uv).xyz;
	gl_FragColor = vec4(color, 1.0);
}