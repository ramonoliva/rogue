#version 120

//uniform parameters
uniform sampler2D sceneSSAO;

//varying parameters
varying vec2 uv;

//Shows the SSAO texture
void main() {
	float oFactor = texture2D(sceneSSAO, uv).x;
	gl_FragColor = vec4(oFactor, oFactor, oFactor, 1.0);
}