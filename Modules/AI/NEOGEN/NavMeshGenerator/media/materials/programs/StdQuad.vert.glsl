//uniform parameters
uniform mat4 uWorldViewProjMatrix;

//varying parameters
varying vec2 uv;

void main()                    
{
	gl_Position = uWorldViewProjMatrix * gl_Vertex;
	
	//sign(a): 1.0 => a positive; 0.0 => a is zero; -1.0 a negative. 
	//If a is a vector, the operation is done component-wise
	vec2 inPos = sign(gl_Vertex.xy);
	
	uv = (vec2(inPos.x, -inPos.y) + 1.0)/2.0;
}

