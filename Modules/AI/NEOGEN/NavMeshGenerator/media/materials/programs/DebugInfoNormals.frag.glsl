#version 120

//uniform parameters
uniform sampler2D normalMap;	

//varying parameters
varying vec2 uv;

//Shows the View Space Normals of the scene
void main() {
	vec3 normal = texture2D(normalMap, uv).xyz;
	gl_FragColor = vec4(normal, 1.0);
}