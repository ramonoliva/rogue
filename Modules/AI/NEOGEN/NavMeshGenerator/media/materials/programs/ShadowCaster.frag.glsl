#version 120

//varying variables
varying vec3 oLightSpacePos;

// Shadow caster fragment shader for high-precision single-channel textures    
void main() {
	float depth = length(oLightSpacePos);
    gl_FragColor = vec4(depth);	
}