#version 120

//uniform variables
uniform sampler2D shadowMap;

//varying variables
varying vec2 uv;

//Shows the shadowmap of the current light
void main() {
    float depth = texture2D(shadowMap, uv).x;
	if (depth == 0.0) gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);	//The Background
	else gl_FragColor = vec4(depth, depth, depth, 1.0);
}