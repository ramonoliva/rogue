#version 120

/////////////////////////////////////////////////////
// AUXILIAR CONSTANTS
/////////////////////////////////////////////////////
const float PI = 3.14159265;

/////////////////////////////////////////////////////
// AUXILIAR FUNCTIONS
///////////////////////////////////////////////////// 

bool realEqual(float a, float b, float epsilon) {
	return (abs(a - b) < epsilon);
}

bool realGEqual(float a, float b, float epsilon) {
	if (a > b) return true;
	return realEqual(a, b, epsilon);
}

bool realLEqual(float a, float b, float epsilon) {
	if (a < b) return true;
	return realEqual(a, b, epsilon);
}


