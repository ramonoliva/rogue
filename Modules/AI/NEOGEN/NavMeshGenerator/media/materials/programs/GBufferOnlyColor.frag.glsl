#version 120
#extension GL_ARB_draw_buffers : enable

//uniform variables
uniform vec4 color;

void main() {
	gl_FragData[0] = color;
	gl_FragData[1] = vec4(0.0);
	gl_FragData[2] = vec4(0.0);
}