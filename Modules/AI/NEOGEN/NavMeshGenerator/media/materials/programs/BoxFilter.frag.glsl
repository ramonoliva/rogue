//uniform parameters
uniform int orientation;	//0 => Filter_X; 1 => Filter_Y;
uniform sampler2D theTex;	//The texture to filter	
uniform float texelSizeX;							
uniform float texelSizeY;
uniform int kernelSize;

//varying parameters
varying vec2 uv;

//A 2 pass box filter for EVEN kernel size
void main() {
	vec4 sum;
	vec2 offset;
	
	int halfSize = kernelSize / 2;
					
	if (orientation == 0) {
		//Filter horizontally
		offset.y = 0.0;
		for (int i = -halfSize; i < halfSize; i++) {
			offset.x = texelSizeX * float(i);
			sum += texture2D(theTex, uv + offset);
		}
	}
	else if (orientation == 1) {
		//Filter vertically
		offset.x = 0.0;
		for (int i = -halfSize; i < halfSize; i++) {
			offset.y = texelSizeY * float(i);
			sum += texture2D(theTex, uv + offset);
		}
	}
	float mean = sum / float(kernelSize);
	gl_FragColor = vec4(mean);
}