Copyright (c) 2014 Ramon Oliva, Nuria Pelechano

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software. The authors have no obligations to provide maintenance, support, updates, enhancements or modifications. 

Permission to use, copy, modify and distribute this software and its documentation for educational, research, and non-profit purposes, without fee, and without a written agreement is hereby granted. 

Permission to incorporate this software into commercial products may be obtained by contacting directly to the main author of the software, Ramon Oliva <ramon.oliva.martinez@gmail.com>. 

All users of this software may accept the following conditions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. 

2. If you use this software in a product, an acknowledgment in the product documentation would be appreciated.

3. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

4. This software uses 3rd party libraries with their own licenses that the user also must respect. 

5. This notice may not be removed or altered from any source distribution.