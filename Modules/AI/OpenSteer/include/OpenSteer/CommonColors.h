#ifndef __CommonColors_h_
#define __CommonColors_h_

#include "Vec3.h"

namespace OpenSteer {
	
	const Vec3 gBlack   (0, 0, 0);
    const Vec3 gWhite   (1, 1, 1);

    const Vec3 gRed     (1, 0, 0);
    const Vec3 gYellow  (1, 1, 0);
    const Vec3 gGreen   (0, 1, 0);
    const Vec3 gCyan    (0, 1, 1);
    const Vec3 gBlue    (0, 0, 1);
    const Vec3 gMagenta (1, 0, 1);

    const Vec3 gOrange (1, 0.5f, 0);

    inline Vec3 grayColor (const float g) {return Vec3 (g, g, g);}

    const Vec3 gGray10 = grayColor (0.1f);
    const Vec3 gGray20 = grayColor (0.2f);
    const Vec3 gGray30 = grayColor (0.3f);
    const Vec3 gGray40 = grayColor (0.4f);
    const Vec3 gGray50 = grayColor (0.5f);
    const Vec3 gGray60 = grayColor (0.6f);
    const Vec3 gGray70 = grayColor (0.7f);
    const Vec3 gGray80 = grayColor (0.8f);
    const Vec3 gGray90 = grayColor (0.9f);

};

#endif