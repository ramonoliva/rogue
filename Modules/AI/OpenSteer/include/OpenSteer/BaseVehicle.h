#ifndef __BaseVehicle_h_
#define __BaseVehicle_h_

#include "AbstractVehicle.h"
#include "SteerLibrary.h"
#include "Proximity.h"

namespace OpenSteer {

	typedef AbstractProximityDatabase<AbstractVehicle*> ProximityDataBase;
	typedef AbstractTokenForProximityDatabase<AbstractVehicle*> ProximityToken;
	typedef LQProximityDatabase<AbstractVehicle*> LQPDAV;

	class BaseVehicle;
	typedef std::vector<BaseVehicle*> VehicleList;
	typedef std::vector<OpenSteer::SphericalObstacle*> SOG;  // spherical obstacle group

	// SimpleVehicle_1 adds concrete LocalSpace methods to AbstractVehicle
    typedef LocalSpaceMixin<AbstractVehicle> BaseVehicle_1;

	// SimpleVehicle_3 adds concrete steering methods to SimpleVehicle_2
    typedef SteerLibraryMixin<BaseVehicle_1> BaseVehicle_2;

	class BaseVehicle : public BaseVehicle_2 {
	public:

		BaseVehicle(ProximityDataBase* pdb);
		~BaseVehicle();

		void setWander(bool b);
		bool getWander();

		void setTarget(BaseVehicle* target);
		void setTarget(const OpenSteer::Vec3& target);

		void getTarget(BaseVehicle* target);
		void getTarget(OpenSteer::Vec3& target);
			
		virtual void setFleeFrom(VehicleList& vehicles);
		virtual void addFleeFrom(BaseVehicle* vehicle);
		virtual void clearFleeFrom();

		virtual void setObstacles(SOG& oList);
		virtual void addObstacle(OpenSteer::SphericalObstacle* o);
		virtual void clearObstacles();

		virtual void setMinTimeToCollision(float t);
	
		virtual void setMaxPredictionTime(float t);

		virtual void setFleeRadius(float r);

		virtual OpenSteer::Vec3 computeTotalSteering(float elapsedTime);
		virtual void updateSteering(float elapsedTime);

		virtual OpenSteer::Vec3 steerForEvasion();
		virtual OpenSteer::Vec3 steerForPursuit();
		
		// reset vehicle state
        virtual void reset();

		virtual Vec3 setPosition(Vec3& pos);

		/**********************************************************************/
		/* BEGIN ABSTRACT VEHICLE INTERFACE */
		/**********************************************************************/
        // get/set mass
        float mass () const {return _mass;}
        float setMass (float m) {return _mass = m;}

        // get velocity of vehicle
        Vec3 velocity () const {return forward() * _speed;}

        // get/set speed of vehicle  (may be faster than taking mag of velocity)
        float speed () const {return _speed;}
        float setSpeed (float s) {return _speed = s;}

        // size of bounding sphere, for obstacle avoidance, etc.
        float radius () const {return _radius;}
        float setRadius (float m) {return _radius = m;}

        // get/set maxForce
        float maxForce () const {return _maxForce;}
        float setMaxForce (float mf) {return _maxForce = mf;}

        // get/set maxSpeed
        float maxSpeed () const {return _maxSpeed;}
        float setMaxSpeed (float ms) {return _maxSpeed = ms;}

		// predict position of this vehicle at some time in the future
        // (assumes velocity remains constant)
        Vec3 predictFuturePosition (const float predictionTime) const;

		/**********************************************************************/
		/* END ABSTRACT VEHICLE INTERFACE */
		/**********************************************************************/

        // apply a given steering force to our momentum,
        // adjusting our orientation to maintain velocity-alignment.
        virtual void applySteeringForce (const Vec3& force, const float deltaTime);

        // the default version: keep FORWARD parallel to velocity, change
        // UP as little as possible.
        virtual void regenerateLocalSpace (const Vec3& newVelocity, const float elapsedTime);

        // alternate version: keep FORWARD parallel to velocity, adjust UP
        // according to a no-basis-in-reality "banking" behavior, something
        // like what birds and airplanes do.  (XXX experimental cwr 6-5-03)
        void regenerateLocalSpaceForBanking (const Vec3& newVelocity, const float elapsedTime);

        // adjust the steering force passed to applySteeringForce.
        // allows a specific vehicle class to redefine this adjustment.
        // default is to disallow backward-facing steering at low speed.
        // xxx experimental 8-20-02
        virtual Vec3 adjustRawSteeringForce (const Vec3& force, const float deltaTime);

        // apply a given braking force (for a given dt) to our momentum.
        // xxx experimental 9-6-02
        void applyBrakingForce (const float rate, const float deltaTime);

        // get instantaneous curvature (since last update)
        float curvature () {return _curvature;}

        // get/reset smoothedCurvature, smoothedAcceleration and smoothedPosition
        float smoothedCurvature () {return _smoothedCurvature;}
        float resetSmoothedCurvature (float value = 0) {
            _lastForward = Vec3::zero;
            _lastPosition = Vec3::zero;
            return _smoothedCurvature = _curvature = value;
        }
        Vec3 smoothedAcceleration () {return _smoothedAcceleration;}
        Vec3 resetSmoothedAcceleration (const Vec3& value = Vec3::zero) {
            return _smoothedAcceleration = value;
        }
        Vec3 smoothedPosition () {return _smoothedPosition;}
        Vec3 resetSmoothedPosition (const Vec3& value = Vec3::zero) {
            return _smoothedPosition = value;
        }

        // give each vehicle a unique number
        int serialNumber;
        static int serialNumberCounter;

	protected:

		float _mass;       // mass (defaults to unity so acceleration=force)
		float _radius;     // size of bounding sphere, for obstacle avoidance, etc.
		float _speed;      // speed along Forward direction.  Because local space is velocity-aligned, velocity = Forward * Speed
		float _maxForce;   // the maximum steering force this vehicle can apply (steering force is clipped to this magnitude)
		float _maxSpeed;   // the maximum speed this vehicle is allowed to move (velocity is clipped to this magnitude)

        float _curvature;
        Vec3 _lastForward;
        Vec3 _lastPosition;
        Vec3 _smoothedPosition;
        float _smoothedCurvature;
        Vec3 _smoothedAcceleration;

        OpenSteer::Vec3 _targetFixedPoint;		//The fixed point of the space where the vehicle have to move to
		BaseVehicle* _targetVehicle;			//The vehicle that we are pursuing
		VehicleList _fleeFrom;					//The list of vehicles that we must flee from
		float _fleeRadius;						//Vehicles outside the flee radius are ignored
		SOG _obstacles;							//The obstacles of the scene (currently only Spherical Obstacles) that must be avoided
		bool _wander;							//Indicates if the vehicle has to move in a wandering like fashion

		float _minTimeToCollision;	//We only consider obstacles that we will collide in minTimeCollision seconds in the future
		float _maxPredictionTime;	//Upper bound for the time prediction of the pursued vehicle. 

		OpenSteer::ProximityToken* _proximityToken;	// a pointer to this boid's interface object for the proximity database

		virtual void createProximityToken(ProximityDataBase* pdb);

		virtual void measurePathCurvature (const float elapsedTime);	// measure path curvature (1/turning-radius), maintain smoothed version

		virtual bool isValidPursuitTo();
		virtual float computePredictionTime(BaseVehicle* vehicle);
	};

};

#endif