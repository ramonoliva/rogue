#include "OpenSteer\BaseVehicle.h"

#include "BaseVehicle.h"
#include <algorithm>

using namespace OpenSteer;

int OpenSteer::BaseVehicle::serialNumberCounter = 0;

/*******************************************************************************************/
/* CONSTRUCTION AND DESTRUCTION */
/*******************************************************************************************/
BaseVehicle::BaseVehicle(ProximityDataBase* pdb) {
	reset ();								// set inital state
	serialNumber = serialNumberCounter++;	// maintain unique serial numbers
	_targetVehicle = NULL;
	setWander(false);			//No wander by default
	createProximityToken(pdb);
	setSpeed(3);             // speed along Forward direction.
    setMaxForce(3.0);        // steering force is clipped to this magnitude
    setMaxSpeed(3.0);        // velocity is clipped to this magnitude
	setMinTimeToCollision(1.0f);
	setMaxPredictionTime(2.0f);
	setFleeRadius(10.0f);
}

BaseVehicle::~BaseVehicle() {
	clearFleeFrom();
	clearObstacles();
	delete _proximityToken;
}

void BaseVehicle::createProximityToken(ProximityDataBase* pdb) {
	_proximityToken = pdb->allocateToken(this);	// allocate a token for this boid in the proximity database
}

void BaseVehicle::reset () {
	// reset LocalSpace state
    resetLocalSpace ();

    // reset SteerLibraryMixin state
    // (XXX this seems really fragile, needs to be redesigned XXX)
    BaseVehicle_2::reset ();

    setMass (1);          // mass (defaults to 1 so acceleration=force)
    setSpeed (0);         // speed along Forward direction.

    setRadius (0.5f);     // size of bounding sphere

    setMaxForce (0.1f);   // steering force is clipped to this magnitude
    setMaxSpeed (1.0f);   // velocity is clipped to this magnitude

    // reset bookkeeping to do running averages of these quanities
    resetSmoothedPosition ();
    resetSmoothedCurvature ();
    resetSmoothedAcceleration ();
}

/*******************************************************************************************/
/* GETTERS AND SETTERS */
/*******************************************************************************************/
void BaseVehicle::setWander(bool b) {
	_wander = b;
}

bool BaseVehicle::getWander() {
	return _wander;
}

Vec3 BaseVehicle::setPosition(Vec3& pos) {
	_proximityToken->updateForNewPosition(pos); // notify proximity database that our position has changed
	return BaseVehicle_1::setPosition(pos);
}

void BaseVehicle::setFleeRadius(float r) {
	_fleeRadius = r;
}

void BaseVehicle::setMinTimeToCollision(float t) {
	_minTimeToCollision = t;
}

void BaseVehicle::setMaxPredictionTime(float t) {
	_maxPredictionTime = t;
}

bool BaseVehicle::isValidPursuitTo() {
	return (_targetVehicle != NULL);
}

void BaseVehicle::setTarget(const OpenSteer::Vec3& target) {
	_targetFixedPoint = target;
}

void BaseVehicle::setTarget(BaseVehicle* vehicle) {
	_targetVehicle = vehicle;
}

void BaseVehicle::getTarget(BaseVehicle* target) {
	target = _targetVehicle;
}

void BaseVehicle::getTarget(OpenSteer::Vec3& target) {
	target = _targetFixedPoint;
}

void BaseVehicle::setFleeFrom(VehicleList& vehicles) {
	_fleeFrom = vehicles;
}

void BaseVehicle::addFleeFrom(BaseVehicle* vehicle) {
	_fleeFrom.push_back(vehicle);
}

void BaseVehicle::clearFleeFrom() {
	_fleeFrom.clear();
}

void BaseVehicle::setObstacles(SOG& oList) {
	_obstacles = oList;
}

void BaseVehicle::addObstacle(OpenSteer::SphericalObstacle* o) {
	_obstacles.push_back(o);
}

void BaseVehicle::clearObstacles() {
	_obstacles.clear();
}

/*******************************************************************************************/
/* CLASS FUNCTIONS */
/*******************************************************************************************/
float BaseVehicle::computePredictionTime(BaseVehicle* vehicle) {
	const Vec3 offset = vehicle->position() - position();
    float distance = offset.length();	//Distance between me and my target

    // xxx maybe this should take into account vehicle's heading? xxx
    float timeEstimate = 0.5f * distance / vehicle->speed(); //xxx
	timeEstimate = std::min<float>(timeEstimate, _maxPredictionTime);

	return timeEstimate;
}

/*OpenSteer::Vec3 BaseVehicle::computeTotalSteering(float elapsedTime) {
	Vec3 avoidance = steerToAvoidObstacles(_minTimeToCollision, (ObstacleGroup&)_obstacles);
	Vec3 seek = steerForPursuit();
	Vec3 evasion = steerForEvasion();

	Vec3 steer;
	float p = 0.75f;
	if (avoidance != Vec3::zero) steer = avoidance;
	else if (evasion != Vec3::zero) {
		 steer = p * seek + (1 - p) * evasion;
	}
	else steer = seek;
	return steer;
}*/

OpenSteer::Vec3 BaseVehicle::computeTotalSteering(float elapsedTime) {
    // move forward
    Vec3 steeringForce;

    // determine if obstacle avoidance is required
    Vec3 obstacleAvoidance = steerToAvoidObstacles(_minTimeToCollision, (ObstacleGroup&)_obstacles);

	const float caLeadTime = 3;
	// find all neighbors within maxRadius using proximity database
    // (radius is largest distance between vehicles traveling head-on
    // where a collision is possible within caLeadTime seconds.)
    const float maxRadius = caLeadTime * maxSpeed() * 2;
	AVGroup neighbors;
    _proximityToken->findNeighbors (position(), maxRadius, neighbors);

    Vec3 collisionAvoidance = steerToAvoidNeighbors(caLeadTime, neighbors) * 10;
        
    // if obstacle avoidance is needed, do it
	steeringForce = obstacleAvoidance + collisionAvoidance;
	if (steeringForce == Vec3::zero) {
		// otherwise, apply low-priority steers
        
		steeringForce = steerForPursuit();	//Pursuit Steer

		//add wander force if required
        if (_wander) steeringForce += steerForWander(elapsedTime);	//Wander Steer
    }
	
	// return steering constrained to global XZ "ground" plane
    return steeringForce.setYtoZero();
}

void BaseVehicle::updateSteering(float elapsedTime) {
	Vec3 steer = computeTotalSteering(elapsedTime);
	applySteeringForce(steer, elapsedTime);
}

Vec3 BaseVehicle::steerForPursuit() {
	Vec3 steer(Vec3::zero);
	if (isValidPursuitTo()) {
		float timeEstimate = computePredictionTime(_targetVehicle);
		steer = BaseVehicle_2::steerForPursuit(*_targetVehicle, timeEstimate);
	}
	else {
		//The vehicle target is invalid, so do a standard seek. 
		steer = steerForSeek(_targetFixedPoint);
	}
	return steer;
}

Vec3 BaseVehicle::steerForEvasion() {
    // sum up weighted evasion
	Vec3 evade(Vec3::zero);
    BaseVehicle* enemy;
	for(VehicleList::iterator it = _fleeFrom.begin(); it != _fleeFrom.end(); it++) {
        enemy = *it;
        
		const Vec3 eOffset = enemy->position() - position();
		float eDistance2 = eOffset.lengthSquared();

		if (eDistance2 < (_fleeRadius * _fleeRadius)) {
			//Enemy is inside the _fleeRadius, so it must be taken into account
			float timeEstimate = computePredictionTime(enemy);
			const Vec3 eFuture = enemy->predictFuturePosition(timeEstimate);

			// steering to flee from eFuture(enemy's future position)
			const Vec3 flee = BaseVehicle_2::steerForEvasion(*enemy, timeEstimate); 

			evade += flee;
		}
    }
    return evade;
}

// ----------------------------------------------------------------------------
// adjust the steering force passed to applySteeringForce.
//
// allows a specific vehicle class to redefine this adjustment.
// default is to disallow backward-facing steering at low speed.
//
// xxx should the default be this ad-hocery, or no adjustment?
// xxx experimental 8-20-02
//
// parameter names commented out to prevent compiler warning from "-W"

OpenSteer::Vec3 OpenSteer::BaseVehicle::adjustRawSteeringForce (const Vec3& force, const float deltaTime) {
    const float maxAdjustedSpeed = 0.2f * maxSpeed ();

    if ((speed () > maxAdjustedSpeed) || (force == Vec3::zero))
    {
        return force;
    }
    else
    {
        const float range = speed() / maxAdjustedSpeed;
        // const float cosine = interpolate (pow (range, 6), 1.0f, -1.0f);
        // const float cosine = interpolate (pow (range, 10), 1.0f, -1.0f);
        // const float cosine = interpolate (pow (range, 20), 1.0f, -1.0f);
        // const float cosine = interpolate (pow (range, 100), 1.0f, -1.0f);
        // const float cosine = interpolate (pow (range, 50), 1.0f, -1.0f);
        const float cosine = interpolate (pow (range, 20), 1.0f, -1.0f);
        return limitMaxDeviationAngle (force, cosine, forward());
    }
}


// ----------------------------------------------------------------------------
// xxx experimental 9-6-02
//
// apply a given braking force (for a given dt) to our momentum.
//
// (this is intended as a companion to applySteeringForce, but I'm not sure how
// well integrated it is.  It was motivated by the fact that "braking" (as in
// "capture the flag" endgame) by using "forward * speed * -rate" as a steering
// force was causing problems in adjustRawSteeringForce.  In fact it made it
// get NAN, but even if it had worked it would have defeated the braking.
//
// maybe the guts of applySteeringForce should be split off into a subroutine
// used by both applySteeringForce and applyBrakingForce?


void OpenSteer::BaseVehicle::applyBrakingForce (const float rate, const float deltaTime) {
    const float rawBraking = speed () * rate;
    const float clipBraking = ((rawBraking < maxForce ()) ? rawBraking : maxForce ());

    setSpeed (speed () - (clipBraking * deltaTime));
}


// ----------------------------------------------------------------------------
// apply a given steering force to our momentum,
// adjusting our orientation to maintain velocity-alignment.


void OpenSteer::BaseVehicle::applySteeringForce (const Vec3& force, const float elapsedTime) {

    const Vec3 adjustedForce = adjustRawSteeringForce (force, elapsedTime);

    // enforce limit on magnitude of steering force
    const Vec3 clippedForce = adjustedForce.truncateLength (maxForce ());

    // compute acceleration and velocity
    Vec3 newAcceleration = (clippedForce / mass());
    Vec3 newVelocity = velocity();

    // damp out abrupt changes and oscillations in steering acceleration
    // (rate is proportional to time step, then clipped into useful range)
    if (elapsedTime > 0) {
        const float smoothRate = clip (9 * elapsedTime, 0.15f, 0.4f);
        blendIntoAccumulator (smoothRate, newAcceleration, _smoothedAcceleration);
    }

    // Euler integrate (per frame) acceleration into velocity
    newVelocity += _smoothedAcceleration * elapsedTime;

    // enforce speed limit
    newVelocity = newVelocity.truncateLength (maxSpeed ());

    // update Speed
    setSpeed (newVelocity.length());

    // Euler integrate (per frame) velocity into position
    setPosition (position() + (newVelocity * elapsedTime));

    // regenerate local space (by default: align vehicle's forward axis with
    // new velocity, but this behavior may be overridden by derived classes.)
    regenerateLocalSpace (newVelocity, elapsedTime);

    // maintain path curvature information
    measurePathCurvature (elapsedTime);

    // running average of recent positions
    blendIntoAccumulator (elapsedTime * 0.06f, position (), _smoothedPosition);
}


// ----------------------------------------------------------------------------
// the default version: keep FORWARD parallel to velocity, change UP as
// little as possible.
//
// parameter names commented out to prevent compiler warning from "-W"


void OpenSteer::BaseVehicle::regenerateLocalSpace (const Vec3& newVelocity, const float elapsedTime) {
    // adjust orthonormal basis vectors to be aligned with new velocity
    if (speed() > 0) regenerateOrthonormalBasisUF (newVelocity / speed());
}


// ----------------------------------------------------------------------------
// alternate version: keep FORWARD parallel to velocity, adjust UP according
// to a no-basis-in-reality "banking" behavior, something like what birds and
// airplanes do

// XXX experimental cwr 6-5-03


void OpenSteer::BaseVehicle::regenerateLocalSpaceForBanking (const Vec3& newVelocity, const float elapsedTime) {
    // the length of this global-upward-pointing vector controls the vehicle's
    // tendency to right itself as it is rolled over from turning acceleration
    const Vec3 globalUp (0, 0.2f, 0);

    // acceleration points toward the center of local path curvature, the
    // length determines how much the vehicle will roll while turning
    const Vec3 accelUp = _smoothedAcceleration * 0.05f;

    // combined banking, sum of UP due to turning and global UP
    const Vec3 bankUp = accelUp + globalUp;

    // blend bankUp into vehicle's UP basis vector
    const float smoothRate = elapsedTime * 3;
    Vec3 tempUp = up();
    blendIntoAccumulator (smoothRate, bankUp, tempUp);
    setUp (tempUp.normalize());

//  annotationLine (position(), position() + (globalUp * 4), gWhite);  // XXX
//  annotationLine (position(), position() + (bankUp   * 4), gOrange); // XXX
//  annotationLine (position(), position() + (accelUp  * 4), gRed);    // XXX
//  annotationLine (position(), position() + (up ()    * 1), gYellow); // XXX

    // adjust orthonormal basis vectors to be aligned with new velocity
    if (speed() > 0) regenerateOrthonormalBasisUF (newVelocity / speed());
}


// ----------------------------------------------------------------------------
// measure path curvature (1/turning-radius), maintain smoothed version


void OpenSteer::BaseVehicle::measurePathCurvature (const float elapsedTime) {
    if (elapsedTime > 0) {
        const Vec3 dP = _lastPosition - position ();
        const Vec3 dF = (_lastForward - forward ()) / dP.length ();
        const Vec3 lateral = dF.perpendicularComponent (forward ());
        const float sign = (lateral.dot (side ()) < 0) ? 1.0f : -1.0f;
        _curvature = lateral.length() * sign;
        blendIntoAccumulator (elapsedTime * 4.0f, _curvature, _smoothedCurvature);
        _lastForward = forward ();
        _lastPosition = position ();
    }
}


// ----------------------------------------------------------------------------
// predict position of this vehicle at some time in the future
// (assumes velocity remains constant, hence path is a straight line)
//
// XXX Want to encapsulate this since eventually I want to investigate
// XXX non-linear predictors.  Maybe predictFutureLocalSpace ?
//
// XXX move to a vehicle utility mixin?


OpenSteer::Vec3 OpenSteer::BaseVehicle::predictFuturePosition (const float predictionTime) const {
    return position() + (velocity() * predictionTime);
}
// ----------------------------------------------------------------------------