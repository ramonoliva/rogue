#ifndef __CtfEnemy_h_
#define __CtfEnemy_h_

#include "CtfBase.h"

class CtfEnemy : public CtfBase
{
public:

    // constructor
    CtfEnemy () {}

    // reset state
    void reset(SOG& allObstacles);

    // per frame simulation update
    void update(const float currentTime, const float elapsedTime);

	void annotate(float currentTime);
};

#endif