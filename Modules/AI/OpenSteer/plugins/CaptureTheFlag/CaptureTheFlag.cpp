// ----------------------------------------------------------------------------
//
//
// OpenSteer -- Steering Behaviors for Autonomous Characters
//
// Copyright(c) 2002-2003, Sony Computer Entertainment America
// Original author: Craig Reynolds <craig_reynolds@playstation.sony.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files(the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//
//
// ----------------------------------------------------------------------------
//
//
// Capture the Flag  (a portion of the traditional game)
//
// The "Capture the Flag" sample steering problem, proposed by Marcin
// Chady of the Working Group on Steering of the IGDA's AI Interface
// Standards Committee(http://www.igda.org/Committees/ai.htm) in this
// message(http://sourceforge.net/forum/message.php?msg_id=1642243):
//
//     "An agent is trying to reach a physical location while trying
//     to stay clear of a group of enemies who are actively seeking
//     him. The environment is littered with obstacles, so collision
//     avoidance is also necessary."
//
// Note that the enemies do not make use of their knowledge of the 
// seeker's goal by "guarding" it.  
//
// XXX hmm, rename them "attacker" and "defender"?
//
// 08-12-02 cwr: created 
//
//
// ----------------------------------------------------------------------------


#include <iomanip>
#include <sstream>
#include "OpenSteer/SimpleVehicle.h"
#include "OpenSteer/OpenSteerDemo.h"

#include "CtfBase.h"
#include "CtfSeeker.h"
#include "CtfEnemy.h"
#include "CtfUtils.h"

#include "CtfObstacleManager.h"
   
using namespace OpenSteer;

/*****************************************************************************************/
/* PLUGIN FUNCTIONS */
/*****************************************************************************************/
typedef std::vector<CtfBase*> CtfVehicleList;

class CtfPlugIn : public PlugIn
{
public:
	
	const char* name() {
		return "Capture the Flag";
	}

    float selectionOrderSortKey() {
		return 0.01f;
	}

    virtual ~CtfPlugIn() {} // be more "nice" to avoid a compiler warning

    void open() {
		//Initialize the set of (spherical) obstacles		
		initializeObstacles();

		// create the seeker("hero"/"attacker")
		resetCount = 0;
        ctfSeeker = new CtfSeeker();
		ctfSeeker->reset(allObstacles);
		ctfSeeker->setObstacles(allObstacles);
		ctfSeeker->setSeekTo(gHomeBaseCenter);
		all.push_back(ctfSeeker);

        // create the specified number of enemies, 
        // storing pointers to them in an array.
		CtfEnemy* enemy;
        for(int i = 0; i < ctfEnemyCount; i++) {
            enemy = new CtfEnemy();
			enemy->reset(allObstacles);
			enemy->setObstacles(allObstacles);
			enemy->setPursuitTo(ctfSeeker);
			ctfEnemies.push_back(enemy);
			ctfSeeker->addFleeFrom(enemy);
            all.push_back(enemy);
        }

		// initialize camera
        OpenSteerDemo::init2dCamera(*ctfSeeker);
        OpenSteerDemo::camera.mode = Camera::cmFixedDistanceOffset;
        OpenSteerDemo::camera.fixedTarget.set(15, 0, 0);
        OpenSteerDemo::camera.fixedPosition.set(80, 60, 0);
    }

    void update(const float currentTime, const float elapsedTime) {
		// update demo state
		if(ctfSeeker->state == CtfBase::running) {
			lastRunningTime = currentTime;
		}
		else {
			const float resetDelay = 4;
			const float resetTime = lastRunningTime + resetDelay;
			if(currentTime > resetTime) {
				OpenSteerDemo::queueDelayedResetPlugInXXX();
			}
		}

        // update the vehicles
		CtfBase* vehicle;
		for (CtfVehicleList::iterator it = all.begin(); it != all.end(); it++) {
			vehicle = *it;
			if (ctfSeeker->state == CtfBase::running) vehicle->update(currentTime, elapsedTime);
			else {
				vehicle->applyBrakingForce(gBrakingRate, elapsedTime);
				vehicle->applySteeringForce(OpenSteer::Vec3::zero, elapsedTime);
			}
		}
    }

    void redraw(const float currentTime, const float elapsedTime) {
        // selected vehicle(user can mouse click to select another)
        AbstractVehicle& selected = *OpenSteerDemo::selectedVehicle;

        // vehicle nearest mouse(to be highlighted)
        AbstractVehicle& nearMouse = *OpenSteerDemo::vehicleNearestToMouse();

        // update camera
        OpenSteerDemo::updateCamera(currentTime, elapsedTime, selected);

        // draw "ground plane" centered between base and selected vehicle
        const Vec3 goalOffset = gHomeBaseCenter-OpenSteerDemo::camera.position();
        const Vec3 goalDirection = goalOffset.normalize();
        const Vec3 cameraForward = OpenSteerDemo::camera.xxxls().forward();
        const float goalDot = cameraForward.dot(goalDirection);
        const float blend = remapIntervalClip(goalDot, 1, 0, 0.5, 0);
        const Vec3 gridCenter = interpolate(blend, selected.position(), gHomeBaseCenter);
        OpenSteerDemo::gridUtility(gridCenter);

        // draw vehicles, obstacles and home base
		for (CtfVehicleList::iterator it = all.begin(); it != all.end(); it++) (*it)->draw();
        drawObstacles();
        drawHomeBase();

        // highlight vehicle nearest mouse
        OpenSteerDemo::highlightVehicleUtility(nearMouse);

		// display status in the upper left corner of the window
		std::ostringstream status;
		status << obstacleCount << " obstacles [F1/F2]" << std::endl;
		status << resetCount << " restarts" << std::ends;
		const float h = drawGetWindowHeight();
		const Vec3 screenLocation(10, h-50, 0);
		draw2dTextAt2dLocation(status, screenLocation, gGray80);
    }

    void close() {
		// delete vehicles
		for (CtfVehicleList::iterator it = all.begin(); it != all.end(); it++) {
			delete *it;
			*it = NULL;
		}
        
        // clear the group of all vehicles
        all.clear();
		ctfEnemies.clear();
    }

    void reset() {
        // count resets
        resetCount++;

		// reset the vehicles
		for (CtfVehicleList::iterator it = all.begin(); it != all.end(); it++) {
			(*it)->reset(allObstacles);
		}
        
        // reset camera position
        OpenSteerDemo::position2dCamera(*ctfSeeker);

        // make camera jump immediately to new position
        OpenSteerDemo::camera.doNotSmoothNextMove();
    }

    void handleFunctionKeys(int keyNumber) {
        switch(keyNumber) {
			case 1: addOneObstacle();    break;
			case 2: removeOneObstacle(); break;
        }
    }

    void printMiniHelpForFunctionKeys() {
        std::ostringstream message;
        message << "Function keys handled by ";
        message << '"' << name() << '"' << ':' << std::ends;
        OpenSteerDemo::printMessage(message);
        OpenSteerDemo::printMessage("  F1     add one obstacle.");
        OpenSteerDemo::printMessage("  F2     remove one obstacle.");
        OpenSteerDemo::printMessage("");
    }

    const AVGroup& allVehicles() {
		return(const AVGroup&) all;
	}

    void drawHomeBase() {
        const Vec3 up(0, 0.01f, 0);
        const Vec3 atColor(0.3f, 0.3f, 0.5f);
        const Vec3 noColor = gGray50;
        const bool reached = ctfSeeker->state == CtfSeeker::atGoal;
        const Vec3 baseColor =(reached ? atColor : noColor);
        drawXZDisk(gHomeBaseRadius,    gHomeBaseCenter, baseColor, 40);
        drawXZDisk(gHomeBaseRadius/15, gHomeBaseCenter+up, gBlack, 20);
    }

private:
	int resetCount;
	CtfSeeker* ctfSeeker;
	static const int ctfEnemyCount = 4;
	CtfVehicleList ctfEnemies;
    CtfVehicleList all;
	float lastRunningTime; // for auto-reset

};

CtfPlugIn gCtfPlugIn;