#ifndef __CtfBase_h_
#define __CtfBase_h_

#include "CtfSimpleVehicle.h"
#include "CtfUtils.h"

// ----------------------------------------------------------------------------
// short names for STL vectors (iterators) of SphericalObstacle pointers

class CtfBase : public CtfSimpleVehicle {
public:

	enum seekerState {
		running, 
		tagged, 
		atGoal
	};
	seekerState state;

    CtfBase();
	~CtfBase();

	// reset state
    virtual void reset(SOG& allObstacles);

    // draw this character/vehicle into the scene
    virtual void draw();

	virtual void annotate(float currentTime) {};

    // annotate when actively avoiding obstacles
    void annotateAvoidObstacle (const float minDistanceToCollision);

    void randomizeStartingPositionAndHeading(SOG& allObstacles);
	
	virtual void update(const float currentTime, const float elapsedTime);

	virtual OpenSteer::Vec3 steerForEvasion();
	
	// for draw method
    OpenSteer::Vec3 bodyColor;

	// xxx store steer sub-state for anotation
    bool avoiding;
};



#endif