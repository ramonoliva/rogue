#ifndef __CtfObstacleManager_h_
#define __CtfObstacleManager_h_

#include "CtfUtils.h"

using namespace OpenSteer;

const int maxObstacleCount = 100;

/*****************************************************************************************/
/* OBSTACLE MANAGEMENT */
/*****************************************************************************************/
// xxx need to combine guts of addOneObstacle and minDistanceToObstacle,
// xxx perhaps by having the former call the latter, or change the latter to
// xxx be "nearestObstacle": give it a position, it finds the nearest obstacle
// xxx(but remember: obstacles a not necessarilty spheres!)
static void initializeObstacles();
static void addOneObstacle();
static void removeOneObstacle();

static int obstacleCount = -1;
static SOG allObstacles;

static void initializeObstacles() {
    // start with 40% of possible obstacles
    if(obstacleCount == -1) {
        obstacleCount = 0;
        for(int i = 0; i <(maxObstacleCount * 0.4); i++) addOneObstacle();
    }
}

static void addOneObstacle() {
    if(obstacleCount < maxObstacleCount) {
        // pick a random center and radius,
        // loop until no overlap with other obstacles and the home base
        float r;
        OpenSteer::Vec3 c;
        float minClearance;
        const float requiredClearance = gVehicleRadius * 4; // 2 x diameter
        do {
            r = frandom2(1.5, 4);
            c = randomVectorOnUnitRadiusXZDisk() * gMaxStartRadius * 1.1f;
            minClearance = FLT_MAX;

			for(std::vector<OpenSteer::SphericalObstacle*>::iterator so = allObstacles.begin(); so != allObstacles.end(); so++) {
                testOneObstacleOverlap((**so).radius,(**so).center);
            }
            testOneObstacleOverlap(gHomeBaseRadius - requiredClearance, gHomeBaseCenter);
        }
        while(minClearance < requiredClearance);

        // add new non-overlapping obstacle to registry
        allObstacles.push_back(new SphericalObstacle(r, c));
        obstacleCount++;
    }
}

static void removeOneObstacle() {
    if(obstacleCount > 0) {
        obstacleCount--;
        allObstacles.pop_back();
    }
}

static void drawObstacles() {
	const Vec3 color(0.8f, 0.6f, 0.4f);
    const SOG& allSO = allObstacles;
	for (std::vector<OpenSteer::SphericalObstacle*>::const_iterator so = allSO.begin(); so != allSO.end(); so++) {
        drawXZCircle((**so).radius,(**so).center, color, 40);
    }
}

#endif