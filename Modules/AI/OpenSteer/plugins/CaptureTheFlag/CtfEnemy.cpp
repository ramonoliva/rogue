#include "CtfEnemy.h"

using namespace OpenSteer;

void CtfEnemy::reset(SOG& allObstacles) {
    CtfBase::reset(allObstacles);
    bodyColor.set(0.6f, 0.4f, 0.4f); // redish
}

void CtfEnemy::update(const float currentTime, const float elapsedTime) {
    updateSteering(elapsedTime);

	annotate(currentTime);
}

void CtfEnemy::annotate(float currentTime) {
	// annotation
    annotationVelocityAcceleration();
    recordTrailVertex(currentTime, position());

	// detect and record interceptions("tags") of seeker
    const float seekerToMeDist = OpenSteer::Vec3::distance(position(), pursuitTo->position());
    const float sumOfRadii = radius() + pursuitTo->radius();
    if(seekerToMeDist < sumOfRadii) {
		CtfBase* target = ((CtfBase*)pursuitTo);
        if(target->state == running) target->state = tagged;

        // annotation:
        if(target->state == tagged) {
            const OpenSteer::Vec3 color(0.8f, 0.5f, 0.5f);
            annotationXZDisk(sumOfRadii,(position() + pursuitTo->position()) / 2, color, 20);
        }
    }
}