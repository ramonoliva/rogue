#ifndef __CtfSimpleVehicle_h_
#define __CtfSimpleVehicle_h_

#include "SimpleVehicle.h"

	class CtfSimpleVehicle;
	typedef std::vector<CtfSimpleVehicle*> VehicleList;
	typedef std::vector<OpenSteer::SphericalObstacle*> SOG;  // spherical obstacle group

	class CtfSimpleVehicle : public OpenSteer::SimpleVehicle {
	public:

		CtfSimpleVehicle();
		~CtfSimpleVehicle();

		virtual void setSeekTo(const OpenSteer::Vec3& target);
		virtual const OpenSteer::Vec3& getSeekTo();
	
		virtual void setPursuitTo(CtfSimpleVehicle* vehicle);
	
		virtual void setFleeFrom(VehicleList& vehicles);
		virtual void addFleeFrom(CtfSimpleVehicle* vehicle);
		virtual void clearFleeFrom();

		virtual void setObstacles(SOG& oList);
		virtual void addObstacle(OpenSteer::SphericalObstacle* o);
		virtual void clearObstacles();

		virtual void setMinTimeToCollision(float t);
	
		virtual void setMaxPredictionTime(float t);

		virtual void setFleeRadius(float r);

		virtual void updateSteering(float elapsedTime);

		virtual OpenSteer::Vec3 steerForEvasion();
		virtual OpenSteer::Vec3 steerForPursuit();

	protected:

		OpenSteer::Vec3 seekTo;		//The fixed point of the space where the vehicle have to move to
		CtfSimpleVehicle* pursuitTo;			//The vehicle that we are pursuing
		VehicleList fleeFrom;		//The list of vehicles that we must flee from
		float fleeRadius;			//Vehicles outside the flee radius are ignored
		SOG obstacles;				//The obstacles of the scene (currently only Spherical Obstacles) that must be avoided

		float minTimeToCollision;	//We only consider obstacles that we will collide in minTimeCollision seconds in the future
		float maxPredictionTime;	//Upper bound for the time prediction of the pursued vehicle. 

		virtual bool isValidPursuitTo();
		virtual float computePredictionTime(CtfSimpleVehicle* vehicle);
	};


#endif