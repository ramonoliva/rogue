#ifndef __CtfSeeker_h_
#define __CtfSeeker_h_

#include "CtfBase.h"

class CtfSeeker : public CtfBase
{
public:

    // constructor
    CtfSeeker () {}

    // reset state
    void reset(SOG& allObstacles);

    // per frame simulation update
    void update(const float currentTime, const float elapsedTime);

    // is there a clear path to the goal?
    bool clearPathToGoal(VehicleList& ctfEnemies);

	void annotate(float currentTime);

    void updateState();
    void draw();
    void clearPathAnnotation (const float threshold, const float behindcThreshold, const OpenSteer::Vec3& goalDirection);

    bool evading; // xxx store steer sub-state for anotation

};


#endif