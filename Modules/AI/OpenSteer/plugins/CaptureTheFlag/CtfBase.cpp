#include "CtfBase.h"
#include <algorithm>

using namespace OpenSteer;

/*******************************************************************************************/
/* CONSTRUCTION AND DESTRUCTION */
/*******************************************************************************************/
CtfBase::CtfBase() {
	pursuitTo = NULL;
	setMinTimeToCollision(1.0f);
	setMaxPredictionTime(2.0f);
	setFleeRadius(10.0f);
}

CtfBase::~CtfBase() {
	clearFleeFrom();
	clearObstacles();
}

/*******************************************************************************************/
/* CLASS FUNCTIONS */
/*******************************************************************************************/
void CtfBase::reset(SOG& allObstacles) {
    CtfSimpleVehicle::reset();  // reset the vehicle 

    setSpeed(3);             // speed along Forward direction.
    setMaxForce(3.0);        // steering force is clipped to this magnitude
    setMaxSpeed(3.0);        // velocity is clipped to this magnitude

    avoiding = false;         // not actively avoiding

    randomizeStartingPositionAndHeading(allObstacles);  // new starting position

    clearTrailHistory();     // prevent long streaks due to teleportation
}

void CtfBase::randomizeStartingPositionAndHeading(SOG& allObstacles) {
    // randomize position on a ring between inner and outer radii
    // centered around the home base
    const float rRadius = frandom2(gMinStartRadius, gMaxStartRadius);
    const Vec3 randomOnRing = RandomUnitVectorOnXZPlane() * rRadius;
    setPosition(gHomeBaseCenter + randomOnRing);

    // are we are too close to an obstacle?
    if(minDistanceToObstacle(position(), allObstacles) < radius()*5) {
        // if so, retry the randomization(this recursive call may not return
        // if there is too little free space)
        randomizeStartingPositionAndHeading(allObstacles);
    }
    else {
        // otherwise, if the position is OK, randomize 2D heading
        randomizeHeadingOnXZPlane();
    }
}

Vec3 CtfBase::steerForEvasion() {
    // sum up weighted evasion
	Vec3 evade(Vec3::zero);
    CtfSimpleVehicle* enemy;
	for(VehicleList::iterator it = fleeFrom.begin(); it != fleeFrom.end(); it++) {
        enemy = *it;
        
		const Vec3 eOffset = enemy->position() - position();
		float eDistance2 = eOffset.lengthSquared();

		if (eDistance2 < (fleeRadius * fleeRadius)) {
			//Enemy is inside the fleeRadius, so it must be taken into account
			float timeEstimate = computePredictionTime(enemy);
			const Vec3 eFuture = enemy->predictFuturePosition(timeEstimate);

			// steering to flee from eFuture(enemy's future position)
			const Vec3 flee = SimpleVehicle::steerForEvasion(*enemy, timeEstimate); 

			evade += flee;

			// annotation
			annotationXZCircle(enemy->radius(), eFuture, gRed, 20);
		}
    }
    return evade;
}

void CtfBase::update(const float currentTime, const float elapsedTime) {}

// draw this character/vehicle into the scene
void CtfBase::draw() {
    drawBasic2dCircularVehicle(*this, bodyColor);
    drawTrail();
}

// ----------------------------------------------------------------------------
// xxx perhaps this should be a call to a general purpose annotation
// xxx for "local xxx axis aligned box in XZ plane" -- same code in in
// xxx Pedestrian.cpp
void CtfBase::annotateAvoidObstacle(const float minDistanceToCollision) {
    const Vec3 boxSide = side() * radius();
    const Vec3 boxFront = forward() * minDistanceToCollision;
    const Vec3 FR = position() + boxFront - boxSide;
    const Vec3 FL = position() + boxFront + boxSide;
    const Vec3 BR = position()            - boxSide;
    const Vec3 BL = position()            + boxSide;
    const Vec3 white(1,1,1);
    annotationLine(FR, FL, white);
    annotationLine(FL, BL, white);
    annotationLine(BL, BR, white);
    annotationLine(BR, FR, white);
}