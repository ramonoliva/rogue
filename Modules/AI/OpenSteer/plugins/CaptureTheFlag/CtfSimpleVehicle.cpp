#include "CtfSimpleVehicle.h"

#include "CtfSimpleVehicle.h"
#include <algorithm>

using namespace OpenSteer;

/*******************************************************************************************/
/* CONSTRUCTION AND DESTRUCTION */
/*******************************************************************************************/
CtfSimpleVehicle::CtfSimpleVehicle() {
	pursuitTo = NULL;
	setSpeed(3);             // speed along Forward direction.
    setMaxForce(3.0);        // steering force is clipped to this magnitude
    setMaxSpeed(3.0);        // velocity is clipped to this magnitude
	setMinTimeToCollision(1.0f);
	setMaxPredictionTime(2.0f);
	setFleeRadius(10.0f);
}

CtfSimpleVehicle::~CtfSimpleVehicle() {
	clearFleeFrom();
	clearObstacles();
}

/*******************************************************************************************/
/* GETTERS AND SETTERS */
/*******************************************************************************************/
void CtfSimpleVehicle::setFleeRadius(float r) {
	fleeRadius = r;
}

void CtfSimpleVehicle::setMinTimeToCollision(float t) {
	minTimeToCollision = t;
}

void CtfSimpleVehicle::setMaxPredictionTime(float t) {
	maxPredictionTime = t;
}

bool CtfSimpleVehicle::isValidPursuitTo() {
	return (pursuitTo != NULL);
}

void CtfSimpleVehicle::setSeekTo(const OpenSteer::Vec3& target) {
	seekTo = target;
}

const OpenSteer::Vec3& CtfSimpleVehicle::getSeekTo() {
	return seekTo;
}

void CtfSimpleVehicle::setPursuitTo(CtfSimpleVehicle* vehicle) {
	pursuitTo = vehicle;
}

void CtfSimpleVehicle::setFleeFrom(VehicleList& vehicles) {
	fleeFrom = vehicles;
}

void CtfSimpleVehicle::addFleeFrom(CtfSimpleVehicle* vehicle) {
	fleeFrom.push_back(vehicle);
}

void CtfSimpleVehicle::clearFleeFrom() {
	fleeFrom.clear();
}

void CtfSimpleVehicle::setObstacles(SOG& oList) {
	obstacles = oList;
}

void CtfSimpleVehicle::addObstacle(OpenSteer::SphericalObstacle* o) {
	obstacles.push_back(o);
}

void CtfSimpleVehicle::clearObstacles() {
	obstacles.clear();
}

/*******************************************************************************************/
/* CLASS FUNCTIONS */
/*******************************************************************************************/
float CtfSimpleVehicle::computePredictionTime(CtfSimpleVehicle* vehicle) {
	const Vec3 offset = vehicle->position() - position();
    float distance = offset.length();	//Distance between me and my target

    // xxx maybe this should take into account vehicle's heading? xxx
    float timeEstimate = 0.5f * distance / vehicle->speed(); //xxx
	timeEstimate = std::min<float>(timeEstimate, maxPredictionTime);

	return timeEstimate;
}

void CtfSimpleVehicle::updateSteering(float elapsedTime) {
	Vec3 avoidance = steerToAvoidObstacles(minTimeToCollision, (ObstacleGroup&)obstacles);
	Vec3 seek = steerForPursuit();
	Vec3 evasion = steerForEvasion();

	Vec3 steer;
	float p = 0.75f;
	if (avoidance != Vec3::zero) steer = avoidance;
	else if (evasion != Vec3::zero) {
		 steer = p * seek + (1 - p) * evasion;
	}
	else steer = seek;
	applySteeringForce(steer, elapsedTime);
}

Vec3 CtfSimpleVehicle::steerForPursuit() {
	Vec3 steer(Vec3::zero);
	if (isValidPursuitTo()) {
		float timeEstimate = computePredictionTime(pursuitTo);
		steer = SimpleVehicle::steerForPursuit(*pursuitTo, timeEstimate);
	}
	else {
		//The vehicle target is invalid, so do a standard seek. 
		steer = steerForSeek(seekTo);
	}
	return steer;
}

Vec3 CtfSimpleVehicle::steerForEvasion() {
    // sum up weighted evasion
	Vec3 evade(Vec3::zero);
    CtfSimpleVehicle* enemy;
	for(VehicleList::iterator it = fleeFrom.begin(); it != fleeFrom.end(); it++) {
        enemy = *it;
        
		const Vec3 eOffset = enemy->position() - position();
		float eDistance2 = eOffset.lengthSquared();

		if (eDistance2 < (fleeRadius * fleeRadius)) {
			//Enemy is inside the fleeRadius, so it must be taken into account
			float timeEstimate = computePredictionTime(enemy);
			const Vec3 eFuture = enemy->predictFuturePosition(timeEstimate);

			// steering to flee from eFuture(enemy's future position)
			const Vec3 flee = SimpleVehicle::steerForEvasion(*enemy, timeEstimate); 

			evade += flee;
		}
    }
    return evade;
}