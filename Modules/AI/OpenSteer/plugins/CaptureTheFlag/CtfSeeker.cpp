#include "CtfSeeker.h"
#include <iomanip>      // std::setprecision
#include <sstream>

using namespace OpenSteer;

void CtfSeeker::reset(SOG& allObstacles) {
    CtfBase::reset(allObstacles);
    bodyColor.set(0.4f, 0.4f, 0.6f); // blueish
    state = running;
    evading = false;
}

// ----------------------------------------------------------------------------
// are there any enemies along the corridor between us and the goal?
bool CtfSeeker::clearPathToGoal(VehicleList& ctfEnemies) { 
    const float sideThreshold = radius() * 8.0f;
    const float behindThreshold = radius() * 2.0f;

    const Vec3 goalOffset = gHomeBaseCenter - position();
    const float goalDistance = goalOffset.length();
    const Vec3 goalDirection = goalOffset / goalDistance;

    const bool goalIsAside = isAside(gHomeBaseCenter, 0.5);

    // for annotation: loop over all and save result, instead of early return 
    bool xxxReturn = true;

    // loop over enemies
	CtfSimpleVehicle* enemy;
	for(VehicleList::iterator it = ctfEnemies.begin(); it != ctfEnemies.end(); it++) {
        // short name for this enemy
        enemy = *it;
        const float eDistance = Vec3::distance(position(), enemy->position());
        const float timeEstimate = 0.3f * eDistance / enemy->speed(); //xxx
        const Vec3 eFuture = enemy->predictFuturePosition(timeEstimate);
        const Vec3 eOffset = eFuture - position();
        const float alongCorridor = goalDirection.dot(eOffset);
        const bool inCorridor =((alongCorridor > -behindThreshold) && (alongCorridor < goalDistance));
        const float eForwardDistance = forward().dot(eOffset);

        // xxx temp move this up before the conditionals
        annotationXZCircle(enemy->radius(), eFuture, clearPathColor, 20); //xxx

        // consider as potential blocker if within the corridor
        if (inCorridor) {
            const Vec3 perp = eOffset - (goalDirection * alongCorridor);
            const float acrossCorridor = perp.length();
            if(acrossCorridor < sideThreshold) {
                // not a blocker if behind us and we are perp to corridor
                const float eFront = eForwardDistance + enemy->radius();
				const bool eIsBehind = eFront < -behindThreshold;
                const bool eIsWayBehind = eFront < (-2 * behindThreshold);
                const bool safeToTurnTowardsGoal = ((eIsBehind && goalIsAside) || eIsWayBehind);

                if (!safeToTurnTowardsGoal) {
                    // this enemy blocks the path to the goal, so return false
                    //annotationLine(position(), enemy->position(), clearPathColor);
                    // return false;
                    xxxReturn = false;
                }
            }
        }
    }

    // no enemies found along path, return true to indicate path is clear
    clearPathAnnotation(sideThreshold, behindThreshold, goalDirection);
    return xxxReturn;
}

void CtfSeeker::clearPathAnnotation(const float sideThreshold, const float behindThreshold, const Vec3& goalDirection) {
    const Vec3 behindSide = side() * sideThreshold;
    const Vec3 behindBack = forward() * -behindThreshold;
    const Vec3 pbb = position() + behindBack;
    const Vec3 gun = localRotateForwardToSide(goalDirection);
    const Vec3 gn = gun * sideThreshold;
    const Vec3 hbc = gHomeBaseCenter;
    annotationLine(pbb + gn,         hbc + gn,         clearPathColor);
    annotationLine(pbb - gn,         hbc - gn,         clearPathColor);
    annotationLine(hbc - gn,         hbc + gn,         clearPathColor);
    annotationLine(pbb - behindSide, pbb + behindSide, clearPathColor);
}

void CtfSeeker::updateState() {
    // if we reach the goal before being tagged, switch to atGoal state
    if(state == running) {
        const float baseDistance = Vec3::distance(position(),gHomeBaseCenter);
        if (baseDistance < (radius() + gHomeBaseRadius)) state = atGoal;
    }
}

// update method for goal seeker
void CtfSeeker::update(const float currentTime, const float elapsedTime) {
    // do behavioral state transitions, as needed
    updateState();

    updateSteering(elapsedTime);

	annotate(currentTime);
}

void CtfSeeker::annotate(float currentTime) {
	Vec3 offset = seekTo - position();
	annotationLine(position(), position() + offset, gBlue);
    annotationVelocityAcceleration();
    recordTrailVertex(currentTime, position());
}

/*
Vec3 CtfSeeker::steeringForSeeker() {
    // determine if obstacle avodiance is needed
	const bool clearPath = clearPathToGoal(fleeFrom);
    Vec3 steer = steerToAvoidObstacles(minTimeToCollision, (ObstacleGroup&)obstacles);

	// saved for annotation
    avoiding = (steer != Vec3::zero);

    if (!avoiding) {
        // otherwise seek home base and perhaps evade defenders
        const Vec3 seek = steerForSeek(seekTo);
        if(clearPath) {
            // we have a clear path(defender-free corridor), use pure seek

            // xxx experiment 9-16-02
            steer = limitMaxDeviationAngle(seek, 0.707f, forward());
			//steer = seek;
			
            //annotationLine(position(), position() + (steer * 0.2f), seekColor);
        }
        else {
            const Vec3 evade = steerFleeFrom();
            steer = limitMaxDeviationAngle(seek + evade, 0.707f, forward());
			
            //annotationLine(position(),position() + seek, gGreen);
            //annotationLine(position(),position() + evade, gRed);

			//annotationLine(position(),position() + evade.normalize() * 2.0, gRed);

            // annotation: show evasion steering force
            //annotationLine(position(),position()+(steer*0.2f),evadeColor);
        }
    }
	return steer;
}
*/

// ----------------------------------------------------------------------------
// adjust obstacle avoidance look ahead time: make it large when we are far
// from the goal and heading directly towards it, make it small otherwise.
/*void CtfSeeker::adjustObstacleAvoidanceLookAhead(const bool clearPath) {
    if(clearPath) {
        evading = false;
        const float goalDistance = Vec3::distance(gHomeBaseCenter,position());
        const bool headingTowardGoal = isAhead(gHomeBaseCenter, 0.98f);
        const bool isNear =(goalDistance/speed()) < gAvoidancePredictTimeMax;
        const bool useMax = headingTowardGoal && !isNear;
        avoidancePredictTime = (useMax ? gAvoidancePredictTimeMax : gAvoidancePredictTimeMin);
    }
    else {
        evading = true;
        avoidancePredictTime = gAvoidancePredictTimeMin;
    }
}*/

void CtfSeeker::draw() {
    // first call the draw method in the base class
    CtfBase::draw();

    // select string describing current seeker state
    char* seekerStateString = "";
    switch(state) {
		case running:
			if(avoiding) seekerStateString = "avoid obstacle";
			else if(evading) seekerStateString = "seek and evade";
			else seekerStateString = "seek goal";
			break;
		case tagged: seekerStateString = "tagged"; break;
		case atGoal: seekerStateString = "reached goal"; break;
    }

    // annote seeker with its state as text
    const Vec3 textOrigin = position() + Vec3(0, 0.25, 0);
    std::ostringstream annote;
    annote << seekerStateString << std::endl;
    annote << std::setprecision(2) << std::setiosflags(std::ios::fixed) << speed() << std::ends;
    draw2dTextAt3dLocation(annote, textOrigin, gWhite);
}