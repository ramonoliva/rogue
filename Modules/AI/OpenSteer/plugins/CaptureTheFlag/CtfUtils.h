#ifndef __CtfUtils_h_
#define __CtfUtils_h_

#include "OpenSteer\Vec3.h"
#include "OpenSteer\Obstacle.h"

const OpenSteer::Vec3 gHomeBaseCenter(0, 0, 0);
const float gHomeBaseRadius = 1.5;

const float gVehicleRadius = 0.5;

const float gMinStartRadius = 30;
const float gMaxStartRadius = 40;

const float gBrakingRate = 0.75;

const OpenSteer::Vec3 evadeColor    (0.6f, 0.6f, 0.3f); // annotation
const OpenSteer::Vec3 seekColor     (1.0f, 1.0f, 0.0f); // annotation
const OpenSteer::Vec3 clearPathColor(0.3f, 0.6f, 0.3f); // annotation

const float gAvoidancePredictTimeMin  = 0.9f;
const float gAvoidancePredictTimeMax  = 2;

#define testOneObstacleOverlap(radius, center)             \
{                                                          \
    float d = OpenSteer::Vec3::distance(c, center);        \
    float clearance = d -(r +(radius));                    \
    if(minClearance > clearance) minClearance = clearance;  \
}

static float minDistanceToObstacle(const OpenSteer::Vec3 point, std::vector<OpenSteer::SphericalObstacle*>& allObstacles) {
    float r = 0;
    OpenSteer::Vec3 c = point;
    float minClearance = FLT_MAX;
	for(std::vector<OpenSteer::SphericalObstacle*>::iterator so = allObstacles.begin(); so != allObstacles.end(); so++) {
        testOneObstacleOverlap((**so).radius,(**so).center);
    }
    return minClearance;
}

#endif