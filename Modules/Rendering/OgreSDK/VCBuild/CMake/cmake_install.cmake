# Install script for directory: G:/Rogue/Modules/Rendering/OgreSDK/CMake

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/sdk")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/CMake" TYPE FILE FILES
    "G:/Rogue/Modules/Rendering/OgreSDK/CMake/Packages/FindOIS.cmake"
    "G:/Rogue/Modules/Rendering/OgreSDK/CMake/Packages/FindOGRE.cmake"
    "G:/Rogue/Modules/Rendering/OgreSDK/CMake/Utils/FindPkgMacros.cmake"
    "G:/Rogue/Modules/Rendering/OgreSDK/CMake/Utils/MacroLogFeature.cmake"
    "G:/Rogue/Modules/Rendering/OgreSDK/CMake/Utils/PreprocessorUtils.cmake"
    "G:/Rogue/Modules/Rendering/OgreSDK/CMake/Utils/PrecompiledHeader.cmake"
    "G:/Rogue/Modules/Rendering/OgreSDK/CMake/Utils/OgreAddTargets.cmake"
    "G:/Rogue/Modules/Rendering/OgreSDK/CMake/Utils/OgreConfigTargets.cmake"
    "G:/Rogue/Modules/Rendering/OgreSDK/CMake/Utils/OgreGetVersion.cmake"
    "G:/Rogue/Modules/Rendering/OgreSDK/CMake/Utils/OgreFindFrameworks.cmake"
    "G:/Rogue/Modules/Rendering/OgreSDK/CMake/Templates/VisualStudioUserFile.vcproj.user.in"
    "G:/Rogue/Modules/Rendering/OgreSDK/CMake/Templates/VisualStudioUserFile.vcxproj.user.in"
    )
endif()

