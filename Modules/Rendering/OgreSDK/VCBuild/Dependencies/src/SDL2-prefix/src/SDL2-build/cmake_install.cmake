# Install script for directory: G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/sdk")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/Dependencies/src/SDL2-prefix/src/SDL2-build/Debug/SDL2.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/Dependencies/src/SDL2-prefix/src/SDL2-build/Release/SDL2.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/Dependencies/src/SDL2-prefix/src/SDL2-build/MinSizeRel/SDL2.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/Dependencies/src/SDL2-prefix/src/SDL2-build/RelWithDebInfo/SDL2.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/Dependencies/src/SDL2-prefix/src/SDL2-build/Debug/SDL2.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/Dependencies/src/SDL2-prefix/src/SDL2-build/Release/SDL2.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/Dependencies/src/SDL2-prefix/src/SDL2-build/MinSizeRel/SDL2.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/Dependencies/src/SDL2-prefix/src/SDL2-build/RelWithDebInfo/SDL2.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/Dependencies/src/SDL2-prefix/src/SDL2-build/Debug/SDL2.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/Dependencies/src/SDL2-prefix/src/SDL2-build/Release/SDL2.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/Dependencies/src/SDL2-prefix/src/SDL2-build/MinSizeRel/SDL2.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/Dependencies/src/SDL2-prefix/src/SDL2-build/RelWithDebInfo/SDL2.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/Dependencies/src/SDL2-prefix/src/SDL2-build/Debug/SDL2main.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/Dependencies/src/SDL2-prefix/src/SDL2-build/Release/SDL2main.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/Dependencies/src/SDL2-prefix/src/SDL2-build/MinSizeRel/SDL2main.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/Dependencies/src/SDL2-prefix/src/SDL2-build/RelWithDebInfo/SDL2main.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/SDL2" TYPE FILE FILES
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/begin_code.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/close_code.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_assert.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_atomic.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_audio.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_bits.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_blendmode.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_clipboard.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_config_android.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_config_iphoneos.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_config_macosx.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_config_minimal.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_config_pandora.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_config_psp.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_config_windows.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_config_winrt.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_config_wiz.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_copying.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_cpuinfo.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_egl.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_endian.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_error.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_events.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_filesystem.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_gamecontroller.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_gesture.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_haptic.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_hints.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_joystick.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_keyboard.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_keycode.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_loadso.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_log.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_main.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_messagebox.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_mouse.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_mutex.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_name.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_opengl.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_opengles.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_opengles2.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_opengles2_gl2.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_opengles2_gl2ext.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_opengles2_gl2platform.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_opengles2_khrplatform.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_opengl_glext.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_pixels.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_platform.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_power.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_quit.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_rect.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_render.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_revision.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_rwops.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_scancode.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_shape.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_stdinc.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_surface.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_system.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_syswm.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_test.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_test_assert.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_test_common.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_test_compare.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_test_crc32.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_test_font.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_test_fuzzer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_test_harness.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_test_images.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_test_log.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_test_md5.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_test_random.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_thread.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_timer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_touch.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_types.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_version.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/SDL2/include/SDL_video.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/Dependencies/src/SDL2-prefix/src/SDL2-build/include/SDL_config.h"
    )
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/Dependencies/src/SDL2-prefix/src/SDL2-build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
