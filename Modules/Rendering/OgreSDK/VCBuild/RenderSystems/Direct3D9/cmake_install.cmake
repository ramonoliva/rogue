# Install script for directory: G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/sdk")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/Release/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/Release/RenderSystem_Direct3D9.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Release" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Release/RenderSystem_Direct3D9.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/RelWithDebInfo/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/RelWithDebInfo/RenderSystem_Direct3D9.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/RelWithDebInfo" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/RelWithDebInfo/RenderSystem_Direct3D9.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/MinSizeRel/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/MinSizeRel/RenderSystem_Direct3D9.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/MinSizeRel" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/MinSizeRel/RenderSystem_Direct3D9.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/Debug/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/Debug/RenderSystem_Direct3D9_d.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Debug" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Debug/RenderSystem_Direct3D9_d.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Debug" TYPE FILE FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Debug/RenderSystem_Direct3D9_d.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/RelWithDebInfo" TYPE FILE FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/RelWithDebInfo/RenderSystem_Direct3D9.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OGRE/RenderSystems/Direct3D9" TYPE FILE FILES
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9DepthBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9Device.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9DeviceManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9Driver.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9DriverList.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9GpuProgram.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9GpuProgramManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9HardwareBufferManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9HardwareIndexBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9HardwareOcclusionQuery.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9HardwarePixelBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9HardwareVertexBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9HLSLProgram.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9HLSLProgramFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9Mappings.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9MultiRenderTarget.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9Plugin.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9Prerequisites.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9RenderSystem.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9RenderWindow.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9Resource.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9ResourceManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9Texture.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9TextureManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9VertexDeclaration.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9VideoMode.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D9/include/OgreD3D9VideoModeList.h"
    )
endif()

