# Install script for directory: G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/sdk")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/Release/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/Release/RenderSystem_GL.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Release" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Release/RenderSystem_GL.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/RelWithDebInfo/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/RelWithDebInfo/RenderSystem_GL.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/RelWithDebInfo" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/RelWithDebInfo/RenderSystem_GL.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/MinSizeRel/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/MinSizeRel/RenderSystem_GL.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/MinSizeRel" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/MinSizeRel/RenderSystem_GL.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/Debug/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/Debug/RenderSystem_GL_d.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Debug" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Debug/RenderSystem_GL_d.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Debug" TYPE FILE FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Debug/RenderSystem_GL_d.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/RelWithDebInfo" TYPE FILE FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/RelWithDebInfo/RenderSystem_GL.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OGRE/RenderSystems/GL" TYPE FILE FILES
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLATIFSInit.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLContext.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLDefaultHardwareBufferManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLDepthBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLFBOMultiRenderTarget.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLFBORenderTexture.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLFrameBufferObject.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLGpuNvparseProgram.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLGpuProgram.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLGpuProgramManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLHardwareBufferManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLHardwareIndexBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLHardwareOcclusionQuery.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLHardwarePixelBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLHardwareVertexBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLPBRenderTexture.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLPBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLPixelFormat.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLPlugin.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLPrerequisites.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLRenderSystem.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLRenderTexture.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLRenderToVertexBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLStateCacheManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLSupport.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLTexture.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLTextureManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreGLUniformCache.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreSDLGLSupport.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreSDLPrerequisites.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/OgreSDLWindow.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/StateCacheManager/OgreGLNullStateCacheManagerImp.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/StateCacheManager/OgreGLNullUniformCacheImp.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/GLSL/include/OgreGLSLExtSupport.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/GLSL/include/OgreGLSLGpuProgram.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/GLSL/include/OgreGLSLLinkProgram.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/GLSL/include/OgreGLSLLinkProgramManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/GLSL/include/OgreGLSLPreprocessor.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/GLSL/include/OgreGLSLProgram.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/GLSL/include/OgreGLSLProgramFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/nvparse/macro.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/nvparse/nvparse.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/nvparse/nvparse_errors.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/nvparse/nvparse_externs.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/nvparse/ps1.0_program.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/nvparse/rc1.0_combiners.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/nvparse/rc1.0_final.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/nvparse/rc1.0_general.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/nvparse/rc1.0_register.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/nvparse/ts1.0_inst.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/nvparse/ts1.0_inst_list.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/nvparse/vs1.0_inst.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/nvparse/vs1.0_inst_list.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/nvparse/_ps1.0_parser.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/nvparse/_rc1.0_parser.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/nvparse/_ts1.0_parser.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/nvparse/_vs1.0_parser.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OGRE/RenderSystems/GL" TYPE FILE FILES
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/Win32/OgreWin32Context.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/Win32/OgreWin32GLSupport.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/Win32/OgreWin32Prerequisites.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/Win32/OgreWin32RenderTexture.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/Win32/OgreWin32Window.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/Win32/OgreGLUtil.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OGRE/RenderSystems/GL" TYPE DIRECTORY FILES "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/include/GL")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OGRE/RenderSystems/GL" TYPE DIRECTORY FILES "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/GL/src/GLSL/include/")
endif()

