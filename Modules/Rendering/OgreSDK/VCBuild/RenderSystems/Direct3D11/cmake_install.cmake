# Install script for directory: G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/sdk")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/Release/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/Release/RenderSystem_Direct3D11.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Release" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Release/RenderSystem_Direct3D11.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/RelWithDebInfo/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/RelWithDebInfo/RenderSystem_Direct3D11.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/RelWithDebInfo" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/RelWithDebInfo/RenderSystem_Direct3D11.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/MinSizeRel/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/MinSizeRel/RenderSystem_Direct3D11.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/MinSizeRel" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/MinSizeRel/RenderSystem_Direct3D11.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/Debug/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/Debug/RenderSystem_Direct3D11_d.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Debug" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Debug/RenderSystem_Direct3D11_d.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Debug" TYPE FILE FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Debug/RenderSystem_Direct3D11_d.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/RelWithDebInfo" TYPE FILE FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/RelWithDebInfo/RenderSystem_Direct3D11.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OGRE/RenderSystems/Direct3D11" TYPE FILE FILES
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11DepthBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11Device.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11Driver.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11DriverList.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11GpuProgram.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11GpuProgramManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11HardwareBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11HardwareBufferManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11HardwareIndexBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11HardwareOcclusionQuery.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11HardwarePixelBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11HardwareUniformBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11HardwareVertexBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11HLSLProgram.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11HLSLProgramFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11Mappings.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11MultiRenderTarget.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11Plugin.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11Prerequisites.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11RenderSystem.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11RenderToVertexBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11RenderWindow.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11Texture.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11TextureManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11VertexDeclaration.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11VideoMode.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/RenderSystems/Direct3D11/include/OgreD3D11VideoModeList.h"
    )
endif()

