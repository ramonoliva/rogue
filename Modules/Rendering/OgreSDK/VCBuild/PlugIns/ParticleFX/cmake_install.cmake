# Install script for directory: G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/sdk")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/Release/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/Release/Plugin_ParticleFX.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Release" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Release/Plugin_ParticleFX.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/RelWithDebInfo/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/RelWithDebInfo/Plugin_ParticleFX.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/RelWithDebInfo" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/RelWithDebInfo/Plugin_ParticleFX.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/MinSizeRel/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/MinSizeRel/Plugin_ParticleFX.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/MinSizeRel" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/MinSizeRel/Plugin_ParticleFX.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/Debug/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/Debug/Plugin_ParticleFX_d.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Debug" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Debug/Plugin_ParticleFX_d.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Debug" TYPE FILE FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Debug/Plugin_ParticleFX_d.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/RelWithDebInfo" TYPE FILE FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/RelWithDebInfo/Plugin_ParticleFX.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OGRE/Plugins/ParticleFX" TYPE FILE FILES
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreAreaEmitter.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreBoxEmitter.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreBoxEmitterFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreColourFaderAffector.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreColourFaderAffector2.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreColourFaderAffectorFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreColourFaderAffectorFactory2.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreColourImageAffector.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreColourImageAffectorFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreColourInterpolatorAffector.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreColourInterpolatorAffectorFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreCylinderEmitter.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreCylinderEmitterFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreDeflectorPlaneAffector.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreDeflectorPlaneAffectorFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreDirectionRandomiserAffector.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreDirectionRandomiserAffectorFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreEllipsoidEmitter.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreEllipsoidEmitterFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreHollowEllipsoidEmitter.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreHollowEllipsoidEmitterFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreLinearForceAffector.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreLinearForceAffectorFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreParticleFXPlugin.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreParticleFXPrerequisites.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgrePointEmitter.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgrePointEmitterFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreRingEmitter.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreRingEmitterFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreRotationAffector.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreRotationAffectorFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreScaleAffector.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/ParticleFX/include/OgreScaleAffectorFactory.h"
    )
endif()

