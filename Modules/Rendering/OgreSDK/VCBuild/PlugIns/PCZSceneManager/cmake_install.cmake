# Install script for directory: G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/PCZSceneManager

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/sdk")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/Release/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/Release/Plugin_PCZSceneManager.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Release" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Release/Plugin_PCZSceneManager.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/RelWithDebInfo/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/RelWithDebInfo/Plugin_PCZSceneManager.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/RelWithDebInfo" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/RelWithDebInfo/Plugin_PCZSceneManager.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/MinSizeRel/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/MinSizeRel/Plugin_PCZSceneManager.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/MinSizeRel" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/MinSizeRel/Plugin_PCZSceneManager.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/Debug/opt" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/Debug/Plugin_PCZSceneManager_d.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Debug" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Debug/Plugin_PCZSceneManager_d.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Debug" TYPE FILE FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Debug/Plugin_PCZSceneManager_d.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/RelWithDebInfo" TYPE FILE FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/RelWithDebInfo/Plugin_PCZSceneManager.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OGRE/Plugins/PCZSceneManager" TYPE FILE FILES
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/PCZSceneManager/include/OgreAntiPortal.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/PCZSceneManager/include/OgreCapsule.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/PCZSceneManager/include/OgreDefaultZone.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/PCZSceneManager/include/OgrePCPlane.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/PCZSceneManager/include/OgrePCZCamera.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/PCZSceneManager/include/OgrePCZFrustum.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/PCZSceneManager/include/OgrePCZLight.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/PCZSceneManager/include/OgrePCZone.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/PCZSceneManager/include/OgrePCZoneFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/PCZSceneManager/include/OgrePCZPlugin.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/PCZSceneManager/include/OgrePCZPrerequisites.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/PCZSceneManager/include/OgrePCZSceneManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/PCZSceneManager/include/OgrePCZSceneNode.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/PCZSceneManager/include/OgrePCZSceneQuery.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/PCZSceneManager/include/OgrePortal.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/PCZSceneManager/include/OgrePortalBase.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/PlugIns/PCZSceneManager/include/OgreSegment.h"
    )
endif()

