# Install script for directory: G:/Rogue/Modules/Rendering/OgreSDK/OgreMain

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/sdk")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/Release" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/Release/OgreMain.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Release" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Release/OgreMain.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/RelWithDebInfo" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/RelWithDebInfo/OgreMain.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/RelWithDebInfo" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/RelWithDebInfo/OgreMain.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/MinSizeRel" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/MinSizeRel/OgreMain.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/MinSizeRel" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/MinSizeRel/OgreMain.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/Debug" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/Debug/OgreMain_d.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Debug" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Debug/OgreMain_d.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Debug" TYPE FILE FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Debug/OgreMain_d.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/RelWithDebInfo" TYPE FILE FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/RelWithDebInfo/OgreMain.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OGRE" TYPE FILE FILES
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/asm_math.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Ogre.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreAlignedAllocator.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreAnimable.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreAnimation.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreAnimationState.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreAnimationTrack.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreAny.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreArchive.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreArchiveFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreArchiveManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreAtomicObject.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreAtomicScalar.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreAutoParamDataSource.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreAxisAlignedBox.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreBillboard.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreBillboardChain.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreBillboardParticleRenderer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreBillboardSet.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreBitwise.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreBlendMode.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreBone.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreCamera.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreCodec.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreColourValue.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreCommon.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreCompositionPass.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreCompositionTargetPass.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreCompositionTechnique.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreCompositor.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreCompositorChain.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreCompositorInstance.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreCompositorLogic.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreCompositorManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreConfig.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreConfigDialog.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreConfigFile.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreConfigOptionMap.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreController.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreControllerManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreConvexBody.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreCustomCompositionPass.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreDataStream.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreDefaultHardwareBufferManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreDeflate.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreDepthBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreDistanceLodStrategy.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreDualQuaternion.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreDynLib.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreDynLibManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreEdgeListBuilder.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreEntity.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreErrorDialog.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreException.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreExternalTextureSource.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreExternalTextureSourceManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreFactoryObj.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreFileSystem.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreFileSystemLayer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreFrameListener.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreFrustum.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreGpuProgram.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreGpuProgramManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreGpuProgramParams.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreGpuProgramUsage.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreHardwareBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreHardwareBufferManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreHardwareCounterBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreHardwareIndexBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreHardwareOcclusionQuery.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreHardwarePixelBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreHardwareUniformBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreHardwareVertexBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreHeaderPrefix.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreHeaderSuffix.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreHighLevelGpuProgram.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreHighLevelGpuProgramManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreImage.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreImageCodec.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreInstanceBatch.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreInstanceBatchHW.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreInstanceBatchHW_VTF.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreInstanceBatchShader.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreInstanceBatchVTF.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreInstancedEntity.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreInstancedGeometry.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreInstanceManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreIteratorRange.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreIteratorWrapper.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreIteratorWrappers.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreKeyFrame.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreLight.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreLodConfig.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreLodListener.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreLodStrategy.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreLodStrategyManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreLog.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreLogManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreManualObject.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMaterial.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMaterialManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMaterialSerializer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMath.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMatrix3.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMatrix4.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMemoryAllocatedObject.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMemoryAllocatorConfig.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMemoryNedAlloc.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMemoryNedPooling.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMemoryStdAlloc.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMemorySTLAllocator.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMemoryTracker.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMesh.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMeshFileFormat.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMeshManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMeshSerializer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMeshSerializerImpl.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMovableObject.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreMovablePlane.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreNameGenerator.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreNode.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreNumerics.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreOptimisedUtil.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreParticle.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreParticleAffector.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreParticleAffectorFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreParticleEmitter.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreParticleEmitterCommands.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreParticleEmitterFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreParticleIterator.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreParticleSystem.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreParticleSystemManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreParticleSystemRenderer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgrePass.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgrePatchMesh.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgrePatchSurface.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgrePixelCountLodStrategy.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgrePixelFormat.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgrePixelFormatDescriptions.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgrePlane.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgrePlaneBoundedVolume.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgrePlatform.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgrePlatformInformation.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgrePlugin.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgrePolygon.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgrePose.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgrePredefinedControllers.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgrePrefabFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgrePrerequisites.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreProfiler.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreProgressiveMeshGenerator.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreQuaternion.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreQueuedProgressiveMeshGenerator.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRadixSort.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRay.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRectangle2D.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRenderable.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRenderObjectListener.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRenderOperation.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRenderQueue.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRenderQueueInvocation.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRenderQueueListener.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRenderQueueSortingGrouping.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRenderSystem.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRenderSystemCapabilities.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRenderSystemCapabilitiesManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRenderSystemCapabilitiesSerializer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRenderTarget.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRenderTargetListener.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRenderTexture.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRenderToVertexBuffer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRenderWindow.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreResource.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreResourceBackgroundQueue.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreResourceGroupManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreResourceManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRibbonTrail.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRoot.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreRotationalSpline.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSceneManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSceneManagerEnumerator.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSceneNode.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSceneQuery.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreScriptCompiler.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreScriptLexer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreScriptLoader.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreScriptParser.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreScriptTranslator.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSearchOps.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSerializer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreShadowCameraSetup.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreShadowCameraSetupFocused.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreShadowCameraSetupLiSPSM.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreShadowCameraSetupPlaneOptimal.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreShadowCameraSetupPSSM.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreShadowCaster.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreShadowTextureManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreShadowVolumeExtrudeProgram.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSharedPtr.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSimpleRenderable.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSimpleSpline.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSingleton.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSkeleton.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSkeletonFileFormat.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSkeletonInstance.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSkeletonManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSkeletonSerializer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSmallVector.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSphere.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSpotShadowFadePng.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreStableHeaders.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreStaticFaceGroup.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreStaticGeometry.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreStdHeaders.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreStreamSerialiser.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreString.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreStringConverter.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreStringInterface.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreStringVector.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSubEntity.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreSubMesh.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreTagPoint.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreTangentSpaceCalc.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreTechnique.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreTexture.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreTextureManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreTextureUnitState.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreTimer.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreUnifiedHighLevelGpuProgram.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreUserObjectBindings.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreUTFString.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreVector2.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreVector3.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreVector4.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreVertexBoneAssignment.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreVertexIndexData.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreViewport.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreWindowEventUtilities.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreWireBoundingBox.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreWorkQueue.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/include/OgreBuildSettings.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/src/OgreImageResampler.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/src/OgrePixelConversions.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/src/OgreSIMDHelper.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreDefaultWorkQueue.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreDefaultWorkQueueStandard.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreDefaultWorkQueueTBB.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadDefines.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadDefinesBoost.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadDefinesNone.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadDefinesPoco.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadDefinesTBB.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadHeaders.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadHeadersBoost.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadHeadersPoco.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadHeadersTBB.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadDefinesNone.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreDefaultWorkQueueStandard.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreFreeImageCodec.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreDDSCodec.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/OgreZip.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OGRE/WIN32" TYPE FILE FILES
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/WIN32/OgreConfigDialogImp.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/WIN32/OgreErrorDialogImp.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/WIN32/OgreTimerImp.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/WIN32/OgreMinGWSupport.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OGRE/Threading" TYPE FILE FILES
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreDefaultWorkQueue.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreDefaultWorkQueueStandard.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreDefaultWorkQueueTBB.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadDefines.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadDefinesBoost.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadDefinesNone.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadDefinesPoco.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadDefinesTBB.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadHeaders.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadHeadersBoost.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadHeadersPoco.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadHeadersTBB.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreThreadDefinesNone.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/OgreMain/include/Threading/OgreDefaultWorkQueueStandard.h"
    )
endif()

