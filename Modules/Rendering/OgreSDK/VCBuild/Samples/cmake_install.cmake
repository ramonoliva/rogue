# Install script for directory: G:/Rogue/Modules/Rendering/OgreSDK/Samples

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/sdk")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OGRE" TYPE FILE FILES
    "G:/Rogue/Modules/Rendering/OgreSDK/Samples/Common/include/ExampleApplication.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Samples/Common/include/ExampleFrameListener.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Samples/Common/include/ExampleLoadingBar.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Samples/Common/include/InputContext.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Samples/Common/include/OgreStaticPluginLoader.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Samples/Common/include/Sample.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Samples/Common/include/SampleContext.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Samples/Common/include/SamplePlugin.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Samples/Common/include/SdkCameraMan.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Samples/Common/include/SdkSample.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Samples/Common/include/SdkTrays.h"
    )
endif()

