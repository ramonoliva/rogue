# Install script for directory: G:/Rogue/Modules/Rendering/OgreSDK/Components/Paging

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/sdk")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/Release" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/Release/OgrePaging.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Release" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Release/OgrePaging.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/RelWithDebInfo" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/RelWithDebInfo/OgrePaging.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/RelWithDebInfo" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/RelWithDebInfo/OgrePaging.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/MinSizeRel" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/MinSizeRel/OgrePaging.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/MinSizeRel" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/MinSizeRel/OgrePaging.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/Debug" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/Debug/OgrePaging_d.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Debug" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Debug/OgrePaging_d.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Debug" TYPE FILE FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Debug/OgrePaging_d.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/RelWithDebInfo" TYPE FILE FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/RelWithDebInfo/OgrePaging.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OGRE/Paging" TYPE FILE FILES
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/Paging/include/OgreGrid2DPageStrategy.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/Paging/include/OgreGrid3DPageStrategy.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/Paging/include/OgrePage.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/Paging/include/OgrePageConnection.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/Paging/include/OgrePageContent.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/Paging/include/OgrePageContentCollection.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/Paging/include/OgrePageContentCollectionFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/Paging/include/OgrePageContentFactory.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/Paging/include/OgrePagedWorld.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/Paging/include/OgrePagedWorldSection.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/Paging/include/OgrePageFileFormats.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/Paging/include/OgrePageManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/Paging/include/OgrePageStrategy.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/Paging/include/OgrePaging.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/Paging/include/OgrePagingPrerequisites.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/Paging/include/OgreSimplePageContentCollection.h"
    )
endif()

