# Install script for directory: G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/sdk")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/Release" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/Release/OgreRTShaderSystem.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Release" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Release/OgreRTShaderSystem.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/RelWithDebInfo" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/RelWithDebInfo/OgreRTShaderSystem.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/RelWithDebInfo" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/RelWithDebInfo/OgreRTShaderSystem.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/MinSizeRel" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/MinSizeRel/OgreRTShaderSystem.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/MinSizeRel" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/MinSizeRel/OgreRTShaderSystem.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/Debug" TYPE STATIC_LIBRARY OPTIONAL FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/lib/Debug/OgreRTShaderSystem_d.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Debug" TYPE SHARED_LIBRARY FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Debug/OgreRTShaderSystem_d.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/Debug" TYPE FILE FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/Debug/OgreRTShaderSystem_d.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/RelWithDebInfo" TYPE FILE FILES "G:/Rogue/Modules/Rendering/OgreSDK/VCBuild/bin/RelWithDebInfo/OgreRTShaderSystem.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OGRE/RTShaderSystem" TYPE FILE FILES
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreRTShaderSystem.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderCGProgramProcessor.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderCGProgramWriter.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderExDualQuaternionSkinning.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderExHardwareSkinning.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderExHardwareSkinningTechnique.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderExIntegratedPSSM3.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderExLayeredBlending.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderExLinearSkinning.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderExNormalMapLighting.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderExPerPixelLighting.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderExTextureAtlasSampler.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderExTriplanarTexturing.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderFFPColour.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderFFPFog.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderFFPLighting.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderFFPRenderState.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderFFPRenderStateBuilder.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderFFPTexturing.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderFFPTransform.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderFunction.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderFunctionAtom.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderGenerator.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderGLSLESProgramProcessor.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderGLSLESProgramWriter.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderGLSLProgramProcessor.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderGLSLProgramWriter.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderHLSLProgramProcessor.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderHLSLProgramWriter.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderMaterialSerializerListener.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderParameter.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderPrerequisites.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderProgram.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderProgramManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderProgramProcessor.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderProgramSet.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderProgramWriter.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderProgramWriterManager.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderRenderState.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderScriptTranslator.h"
    "G:/Rogue/Modules/Rendering/OgreSDK/Components/RTShaderSystem/include/OgreShaderSubRenderState.h"
    )
endif()

