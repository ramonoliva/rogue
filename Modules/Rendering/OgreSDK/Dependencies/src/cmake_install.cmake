# Install script for directory: G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/ogredeps")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/Cg/cmake_install.cmake")
  include("G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/AMD_Quad_Buffer_SDK_v11/cmake_install.cmake")
  include("G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/NVAPI-R313-developer/cmake_install.cmake")
  include("G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/FreeImage/cmake_install.cmake")
  include("G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/freetype/cmake_install.cmake")
  include("G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/ois/cmake_install.cmake")
  include("G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/rapidjson/cmake_install.cmake")
  include("G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/zlib/cmake_install.cmake")
  include("G:/Rogue/Modules/Rendering/OgreSDK/Dependencies/src/zziplib/cmake_install.cmake")

endif()

