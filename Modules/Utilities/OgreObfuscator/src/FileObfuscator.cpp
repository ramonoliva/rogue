#include <string> 
#include <zzip/plugin.h> 

// Your magic number. Change this to match the magic number in your un-obfuscator. 
static int xor_value = 0xC9A3; 

/*************************************************************************************************************/
/* MAIN */
/*************************************************************************************************************/
int main (int argc, char* argv[]) { 
	std::string usage = 
						" Obfuscate <fileName>\n" 
						" - This program will (un)obfuscate a (.flag).zip file.\n" 
						" Feed it a .zip file and it will obfuscate it.\n" 
						" Feed it a .flag file and it will unobfuscate it.\n"; 

	if (argc != 2) { 
		//Wrong number of parameters
		printf(usage.c_str()); 
		return 0; 
	} 

	if (strlen(argv[1]) > 128) { 
		//Filename is too long
		fprintf(stderr, "Please provide a filename shorter than 128 chars.\n"); 
		exit(1); 
	} 

	//If we arrive here, everything is ok
	std::string fileName_ext = argv[1]; 
	std::string outFileName; 

	// Get the extension and filename 
	size_t delim = fileName_ext.find_last_of("."); 
	std::string fileName = fileName_ext.substr(0 , delim); 
	std::string extension = fileName_ext.substr(delim + 1); 

	// Convert the extension to lower case. 
	for(size_t position = 0; position < extension.size(); ++position) { 
		extension[position] = tolower(extension[position]); 
	} 

	// Determine the filename of the out file
	if(extension == "zip") outFileName = fileName + ".flag";		//If the input is a .zip unobfuscated file, the output is a .flag obfuscated file
	else if(extension == "flag") outFileName = fileName + ".zip";	//If the input is a .flag obfuscated file, the output is a .zip unobfuscated file
	else {
		//The file input file has an unkwown extension
		printf(usage.c_str()); 
		return 0; 
	} 

	//FILE CONVERSION
	FILE* fin = fopen(fileName_ext.c_str(), "rb");	//Read (r) a binary (b) file
	if(!fin) { 
		//Error opening the input file
		fprintf(stderr, "Can't open input file \"%s\"\n", fileName_ext.c_str()); 
		exit(1); 
	} 

	FILE* fout = fopen(outFileName.c_str(), "wb");	//Write (w) a binary (b) file
	if(!fout) { 
		fprintf(stderr, "Can't open output file \"%s\"\n", outFileName.c_str()); 
		exit(1); 
	} 

	//Obfuscate/Unobfuscate each character using the xor operation.
	//If we use this logic operation on a number, this number gets obfuscated. To unobfuscate this number, 
	//we simply must apply the same operation with the same magic number again, so we obtain the original number. 
	int ch; 
	while((ch = fgetc(fin)) != EOF) { 
		ch ^= xor_value; 
		fputc(ch, fout); 
	} 

	fclose(fout); 
	fclose(fin); 
	return 0; 
}

