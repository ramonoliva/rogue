#ifndef __OgreUtils_h_
#define __OgreUtils_h_

#include <OgreRoot.h>
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
#include <OgreMaterial.h>
#include <OgreMaterialManager.h>
#include <OgreEntity.h>
#include <OgreSubEntity.h>
#include <OgreManualObject.h>
#include "ngVector.h"

namespace OgreUtils {
	
	static void destroyAllAttachedMovableObjects(Ogre::SceneNode* node) {
		if(node == NULL) return;
		
		//Destroy all the attached objects
		Ogre::SceneNode::ObjectIterator itObject = node->getAttachedObjectIterator();
		while (itObject.hasMoreElements()) node->getCreator()->destroyMovableObject(itObject.getNext());

		//Recurse to child SceneNodes
		Ogre::SceneNode::ChildNodeIterator itChild = node->getChildIterator();
		while (itChild.hasMoreElements()) {
			Ogre::SceneNode* pChildNode = static_cast<Ogre::SceneNode*>(itChild.getNext());
			destroyAllAttachedMovableObjects( pChildNode );
		}
	}

	static void destroySceneNode(Ogre::SceneNode* node) {
	   if (node == NULL) return;
	   destroyAllAttachedMovableObjects(node);
	   node->removeAndDestroyAllChildren();
	   node->getCreator()->destroySceneNode(node);
	}

	/*********************************************************************************************************************/
	/* SET UNIFORM PER MATERIAL (ALL ENTITIES SHARING THAT MATERIAL ARE AFFECTED)	*/
	/*********************************************************************************************************************/
	enum SHADER_PROGRAM {
		VERTEX_SHADER, 
		GEOMETRY_SHADER, 
		FRAGMENT_SHADER
	};

	static Ogre::GpuProgramParametersSharedPtr getShaderParams(const Ogre::String& matName, int passNum, SHADER_PROGRAM progID) {
		Ogre::MaterialPtr material = Ogre::MaterialManager::getSingletonPtr()->getByName(matName);
		Ogre::GpuProgramParametersSharedPtr shaderParams;
		if (progID == VERTEX_SHADER) shaderParams = material->getTechnique(0)->getPass(passNum)->getVertexProgramParameters();
		else if (progID == GEOMETRY_SHADER) shaderParams = material->getTechnique(0)->getPass(passNum)->getGeometryProgramParameters();
		else shaderParams = material->getTechnique(0)->getPass(passNum)->getFragmentProgramParameters();
		return shaderParams;
	}

	static void setTextureUnit(const Ogre::String& matName, int passNum, int texNum, const Ogre::String& texName) {
		Ogre::MaterialPtr material = Ogre::MaterialManager::getSingletonPtr()->getByName(matName);
		Ogre::TextureUnitState* tUnitState = material->getTechnique(0)->getPass(passNum)->getTextureUnitState(texNum);
		tUnitState->setTextureName(texName);
	}

	static void setUniform(const Ogre::String& matName, int passNum, SHADER_PROGRAM progID, const Ogre::String& uniformName, float uniformValue) {
		Ogre::GpuProgramParametersSharedPtr shaderParams = getShaderParams(matName, passNum, progID);
		shaderParams->setNamedConstant(uniformName, uniformValue);
	}

/*	static void setUniform(const Ogre::String& matName, int passNum, SHADER_PROGRAM progID, const Ogre::String& uniformName, Vert& uniformValue) {
		Ogre::GpuProgramParametersSharedPtr shaderParams = getShaderParams(matName, passNum, progID);
		shaderParams->setNamedConstant(uniformName, Ogre::Vector3(uniformValue.getX(), uniformValue.getY(), uniformValue.getZ()));
	}
	*/

	static void setUniform(const Ogre::String& matName, int passNum, SHADER_PROGRAM progID, const Ogre::String& uniformName, const Ogre::Vector3& uniformValue) {
		Ogre::GpuProgramParametersSharedPtr shaderParams = getShaderParams(matName, passNum, progID);
		shaderParams->setNamedConstant(uniformName, uniformValue);
	}

	static void setUniform(const Ogre::String& matName, int passNum, SHADER_PROGRAM progID, const Ogre::String& uniformName, Ogre::Vector4& uniformValue) {
		Ogre::GpuProgramParametersSharedPtr shaderParams = getShaderParams(matName, passNum, progID);
		shaderParams->setNamedConstant(uniformName, uniformValue);
	}

	static void setUniform(const Ogre::String& matName, int passNum, SHADER_PROGRAM progID, const Ogre::String& uniformName, const Ogre::ColourValue& uniformValue) {
		Ogre::GpuProgramParametersSharedPtr shaderParams = getShaderParams(matName, passNum, progID);
		shaderParams->setNamedConstant(uniformName, uniformValue);
	}

	static void setUniform(const Ogre::String& matName, int passNum, SHADER_PROGRAM progID, const Ogre::String& uniformName, Ogre::Matrix4& uniformValue) {
		Ogre::GpuProgramParametersSharedPtr shaderParams = getShaderParams(matName, passNum, progID);
		shaderParams->setNamedConstant(uniformName, uniformValue);
	}

	static void setUniform(const Ogre::String& matName, int passNum, SHADER_PROGRAM progID, const Ogre::String& uniformName, int uniformValue) {
		Ogre::GpuProgramParametersSharedPtr shaderParams = getShaderParams(matName, passNum, progID);
		shaderParams->setNamedConstant(uniformName, uniformValue);
	}

	static void setUniform(const Ogre::String& matName, int passNum, SHADER_PROGRAM progID, const Ogre::String& uniformName, const float* uniformValue, size_t numElements) {
		Ogre::GpuProgramParametersSharedPtr shaderParams = getShaderParams(matName, passNum, progID);
		shaderParams->setNamedConstant(uniformName, uniformValue, numElements, 3u);
	}

	/*********************************************************************************************************************/
	/* SET UNIFORM PER ENTITY (SPECIFIES UNIFORM VALUES IN A PER ENTITY BASIS, SO ONLY THE ENTITY IS AFFECTED) */
	/*********************************************************************************************************************/
	static Ogre::GpuProgramParametersSharedPtr getShaderParams(Ogre::SubEntity* subEnt, int passNum, SHADER_PROGRAM progID) {
		Ogre::GpuProgramParametersSharedPtr shaderParams;
		if (progID == VERTEX_SHADER) shaderParams = subEnt->getTechnique()->getPass(passNum)->getVertexProgramParameters();
		else if (progID == GEOMETRY_SHADER) shaderParams = subEnt->getTechnique()->getPass(passNum)->getGeometryProgramParameters();
		else shaderParams = subEnt->getTechnique()->getPass(passNum)->getFragmentProgramParameters();
		return shaderParams;
	}

	static void setUniform(Ogre::SubEntity* subEnt, int passNum, SHADER_PROGRAM progID, const Ogre::String& uniformName, const Ogre::ColourValue& uniformValue) {
		Ogre::GpuProgramParametersSharedPtr shaderParams = getShaderParams(subEnt, passNum, progID);
		shaderParams->setNamedConstant(uniformName, uniformValue);
	}

	static Ogre::GpuProgramParametersSharedPtr getShaderParams(Ogre::ManualObject* manual, int section, int passNum, SHADER_PROGRAM progID) {
		Ogre::GpuProgramParametersSharedPtr shaderParams;
		Ogre::Pass* pass = manual->getSection(section)->getTechnique()->getPass(passNum);
		if (progID == VERTEX_SHADER) shaderParams = pass->getVertexProgramParameters();
		else if (progID == GEOMETRY_SHADER) shaderParams = pass->getGeometryProgramParameters();
		else shaderParams = pass->getFragmentProgramParameters();
		return shaderParams;
	}

	static void setUniform(Ogre::ManualObject* manual, int section, int passNum, SHADER_PROGRAM progID, const Ogre::String& uniformName, const Ogre::ColourValue& uniformValue) {
		Ogre::GpuProgramParametersSharedPtr shaderParams = getShaderParams(manual, section, passNum, progID);
		shaderParams->setNamedConstant(uniformName, uniformValue);
	}

};

#endif