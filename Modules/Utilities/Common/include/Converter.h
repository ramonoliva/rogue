#ifndef __Converter_h_
#define __Converter_h_

#include "ngVector.h"
#include "BtOgreExtras.h"
#include <OpenSteer\Vec3.h>

namespace Converter {

	static NEOGEN::Vec3D fromOgreToVert(const Ogre::Vector3& v) {
		return NEOGEN::Vec3D(v.x, v.y, v.z);
	}

	static OpenSteer::Vec3 fromBulletToOpenSteer(const btVector3& v) {
		return OpenSteer::Vec3(v.getX(), v.getY(), v.getZ());
	}

	static btVector3 fromOpenSteerToBullet(const OpenSteer::Vec3& v) {
		return btVector3(v.x, v.y, v.z);
	}

};

#endif