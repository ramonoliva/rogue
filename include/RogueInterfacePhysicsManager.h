#ifndef __RogueInterfacePhysicsManager_h_
#define __RogueInterfacePhysicsManager_h_

/*
Describes the interface of the Physics Service
*/

#include "ngGeometrySolver.h"
#include "RogueInterfaceBase.h"
#include "RogueTypes.h"
#include "RogueRigidBody.h"

#include <set>

namespace Rogue {

	struct RayCastResult {
		bool hit;
		GameObject* gameObject;
		BaseCharacter* character;	//TODO: This is just a patch. In a future, everything must be a GameObject
		Vector3 position;
		Vector3 normal;

		RayCastResult() {
			hit = false;
			gameObject = NULL;
			character = NULL;
			position = Vector3(0,0,0);
			normal = Vector3(0,0,0);
		}
	};

	typedef std::pair<CollisionObject*, CollisionObject*> CollisionPair;
	typedef std::set<CollisionPair> CollisionPairSet;

	class InterfacePhysicsManager : public InterfaceBase {

		public:

			virtual PhysicsWorld* getPhysicsWorld() = 0;
		
			virtual void update() = 0;

			virtual RigidBody* createRigidBody(CollisionShape* shape, float mass,  short collisionFilterGroup = (short)-1, short collisionFilterMask = (short)-1) = 0;
			virtual RigidBody* createRigidBody(CollisionShape* shape, float mass, const Vector3& position, short collisionFilterGroup = (short)-1, short collisionFilterMask = (short)-1) = 0;
			virtual void removeRigidBody(RigidBody* body) = 0;
			virtual void clearWorld() = 0;

			virtual GhostObject* createGhostObject(CollisionShape* shape, short collisionFilterGroup = (short)-1, short collisionFilterMask = (short)-1) = 0;
		
			virtual void rayCastCursor(RayCastResult& result, Camera* camera, short collisionFilterGroup = (short)-1, short collisionFilterMask = (short)-1) = 0;
			virtual void rayCast(Vector3& from, Vector3& to, RayCastResult& result, short collisionFilterGroup = (short)-1, short collisionFilterMask = (short)-1) = 0;

		protected:

			virtual bool addCollisionPair(CollisionPair& pair) = 0;
			virtual bool removeCollisionPair(CollisionPair& pair) = 0;

			virtual void onCollisionPairAdded(CollisionPair& pair) = 0;		//Actions to do when the collision pair is created for the first time
			virtual void onCollisionPairStay(CollisionPair& pair) = 0;		//Actions to do when the bodies are in contact
			virtual void onCollisionPairRemoved(CollisionPair& pair) = 0;	//Actions to do when the bodies are not in contact anymore
			
			CollisionPairSet _collidingBodies;
	};

};

#endif