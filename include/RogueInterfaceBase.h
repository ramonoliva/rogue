#ifndef __RogueInterfaceBase_h_
#define __RogueInterfaceBase_h_

#include "RogueTypes.h"

//Just a dummy class used to group all the manager interfaces. 

namespace Rogue {
	
	class InterfaceBase {
	
		public:

			virtual ~InterfaceBase() {};

	};

};

#endif