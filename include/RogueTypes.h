#ifndef __RogueTypes_h_
#define __RogueTypes_h_

#define SQRT_ONE_HALF 0.7071067811865475244	//Square root of 1/2

/**********************************************************************/
/* RENDER TYPES */
/**********************************************************************/
#include <OgreRoot.h>
#include <OgreCamera.h>
#include <OgreSceneNode.h>
#include <OgreEntity.h>
#include <OgreString.h>
#include <OgreFrameListener.h>
#include <OgreRenderTargetListener.h>
#include <OgreRenderWindow.h>
#include <OgreSceneManager.h>
#include <OgreWindowEventUtilities.h>
#include <OgreTimer.h>

/**********************************************************************/
/* RENDER TYPES */
/**********************************************************************/
namespace Rogue {
	typedef Ogre::Root RenderRoot;
	typedef Ogre::RenderWindow RenderWindow;
	typedef Ogre::Camera Camera;
	typedef Ogre::SceneManager SceneManager;
	typedef Ogre::SceneType SceneType;
	typedef Ogre::SceneNode SceneNode;
	typedef Ogre::MovableObject MovableObject;
	typedef Ogre::ManualObject ManualObject;
	typedef Ogre::Entity Entity;
	typedef Ogre::Entity Mesh;
	typedef Ogre::Light Light;
	typedef Ogre::FrameListener FrameListener;
	typedef Ogre::WindowEventListener WindowEventListener;
	typedef Ogre::FrameEvent FrameEvent;
	typedef Ogre::String String;
	typedef Ogre::Viewport Viewport;
	typedef Ogre::Rectangle2D Rectangle2D;
	typedef Ogre::RenderTarget RenderTarget;
	typedef Ogre::MultiRenderTarget MultiRenderTarget;
	typedef Ogre::TexturePtr TexturePtr;
	typedef Ogre::RenderTexture RenderTexture;
	typedef Ogre::ColourValue Color;
	typedef Ogre::PixelFormat PixelFormat;
	typedef Ogre::RaySceneQuery RaySceneQuery;
	typedef Ogre::Image Image;
	typedef Ogre::RenderTargetListener RenderTargetListener;
	typedef Ogre::Timer Timer;
}

namespace Rogue {
	typedef Ogre::ProjectionType CameraType;
}

namespace Rogue {
	class DeferredPass;
	class DeferredChain;
}

/**********************************************************************/
/* PHYSICS TYPES */
/**********************************************************************/
#include <BtOgrePG.h>
#include <BtOgreGP.h>
#include <BulletCollision\CollisionShapes\btHeightfieldTerrainShape.h>
#include <BulletCollision\CollisionDispatch\btGhostObject.h>

namespace Rogue {
	//The Bullet World
	typedef btDiscreteDynamicsWorld PhysicsWorld;
	typedef btBroadphaseInterface BroadphaseInterface;
	typedef btDefaultCollisionConfiguration DefaultCollisionConfiguration;
	typedef btCollisionDispatcher CollisionDispatcher;
	typedef btSequentialImpulseConstraintSolver ConstraintSolver;
	typedef BtOgre::DebugDrawer DebugDrawer;

	//Physic bodies
	typedef btCollisionObject CollisionObject;
	typedef btMotionState MotionState;
	typedef btDefaultMotionState DefaultMotionState;
	typedef btPairCachingGhostObject GhostObject;

	//CollisionShapes
	typedef btCollisionShape CollisionShape;
	typedef btSphereShape CollisionShapeSphere;
	typedef btBoxShape CollisionShapeBox;
	typedef btCylinderShape CollisionShapeCylinder;
	typedef btCapsuleShape CollisionShapeCapsule;
	typedef btConeShape CollisionShapeCone;
	typedef btMultiSphereShape CollisionShapeMultiSphere;
	typedef btConvexHullShape CollisionShapeConvexHull;
	typedef btBvhTriangleMeshShape CollisionShapeMesh;
	typedef btHeightfieldTerrainShape CollisionShapeHeightMap;
	typedef btStaticPlaneShape CollisionShapePlane;

	//Other stuff
	typedef btVector3 Vector3;
	//typedef btDiscreteDynamicsWorld::ClosestRayResultCallback RayCastResult;

	typedef std::vector<Vector3> Vector3List;
	typedef std::pair<Vector3, Vector3> Vector3Pair;
};

/**********************************************************************/
/* INPUT TYPES */
/**********************************************************************/
#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

namespace Rogue {
	typedef OIS::KeyCode KeyCode;
	typedef OIS::MouseButtonID MouseButtonID;
	typedef OIS::Mouse Mouse;
	typedef OIS::Keyboard Keyboard;
	typedef OIS::KeyEvent KeyEvent;
	typedef OIS::MouseEvent MouseEvent;
	typedef OIS::MouseState MouseState;
	typedef OIS::KeyListener KeyListener;
	typedef OIS::MouseListener MouseListener;
};

/**********************************************************************/
/* GUI TYPES */
/**********************************************************************/
#include <OgreOverlayManager.h>
#include <OgreOverlay.h>
#include <OgreOverlayContainer.h>
#include <OgreOverlayElement.h>
#include <SdkTrays.h>

namespace Rogue {
	typedef OgreBites::SdkTrayManager SdkTrayManager;
	typedef OgreBites::SdkTrayListener SdkTrayListener;
	typedef OgreBites::Widget Widget;
	typedef OgreBites::Button Button;
	typedef OgreBites::SelectMenu SelectMenu;
	typedef OgreBites::Label Label;
	typedef OgreBites::Slider Slider;
	typedef OgreBites::CheckBox CheckBox;
};

#endif