#ifndef __RogueCollisionFilter_h_
#define __RogueCollisionFilter_h_

#define BIT(x) (1<<(x))

enum collisiontypes {
    COL_NOTHING			= 0,			
    COL_CHAR			= BIT(1),		
    COL_WALL			= BIT(2),		
	COL_SCENE_BOUND		= BIT(3),
	

	COL_CHAR_LOCATOR	= BIT(12),
	COL_NAVMESH_CELL	= BIT(13),
	COL_NAVMESH_PORTAL	= BIT(14),
};

const int wallCollidesWith = COL_CHAR;
const int sceneBoundCollidesWith = COL_CHAR;
const int charCollidesWith = COL_WALL | COL_SCENE_BOUND | COL_CHAR | COL_NAVMESH_CELL | COL_NAVMESH_PORTAL;
const int rayGroundCollidesWith = COL_WALL;
const int navMeshPortalCollidesWith = COL_CHAR;
const int navMeshCellCollidesWith = COL_CHAR_LOCATOR | COL_CHAR;
const int charLocatorCollidesWith = COL_NAVMESH_CELL;

#endif
