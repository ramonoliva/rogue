#ifndef __RogueAutonomousCharacter_h_
#define __RogueAutonomousCharacter_h_

#include "RogueBaseCharacter.h"

#include "BulletCollision/CollisionDispatch/btGhostObject.h"

#include "ngPathFinder.h"

#include <ngGeometrySolver.h>

#include "RogueGlobalVars.h"

#include <vector>
#include <list>
#include <unordered_map>
#include <unordered_set>

typedef enum {
	NINJA_STATE_IDLE,
	NINJA_STATE_EXPLORE_MAP, 
	NINJA_STATE_SCRIPTED_PATH, 
	NINJA_STATE_RANDOM_TARGET
} NinjaStateEnum;

namespace Rogue {

	class AutonomousCharacter : public BaseCharacter {

		public:

			typedef enum {
				CELL_STATE_NON_VISITED, 
				CELL_STATE_VISITED, 
				CELL_STATE_EXPLORED
			} CellStateEnum;
			typedef std::unordered_map<int, CellStateEnum> CellMap;

			/*************************************/
			/* CREADORA I DESTRUCTORA			 */
			/*************************************/

			AutonomousCharacter(BaseCharConfigurator& baseCFG, RigidBody* rigidBody, OpenSteerCharConfigurator& openSteerCFG);
			virtual ~AutonomousCharacter();
						
			/*************************************/
			/* GET I SET						 */
			/*************************************/
			virtual int getCellState(int cellID);
			virtual void setCellState(int cellID, CellStateEnum cellState);
			virtual Ogre::AnimationState* getAnimState();
			virtual void setOrigin(btVector3& pos);
			virtual void setCurrentPortal(int p);
			virtual int getCurrentPortal();
			virtual void setCurrentCellID(int cellID);
			virtual void setTargetCellID(int c);
			virtual int getTargetCellID();
		
			virtual btVector3 getForwardDir();
			virtual btVector3 getUpDir();
			virtual btVector3 getStrafeDir();
			
			virtual void computePortalSeekPoint();
			virtual void portalSeekProjection();
			virtual void portalSeekMidPoint();
			virtual void computeShrinkedPortal(int portalID, NEOGEN::Vec3D& v1, NEOGEN::Vec3D& v2);
			virtual void computeProjection(NEOGEN::Vec3D& portalV1, NEOGEN::Vec3D& portalV2, NEOGEN::Vec3D& steeringPoint);
			virtual void findPath();

			virtual void updatePath();
			virtual void updateLogic(Ogre::Real timeLF);
			virtual void updateRender(Ogre::Real timeLF);
			virtual void updateSteering(float elapsedTime);		//OpenSteer::BaseVehicle overriding

		protected:

			NEOGEN::Vec3D goalPosition;							//Posici� objectiu del personatge
			
			////////////////////////////////////////////////////////
			// BRAIN 
			////////////////////////////////////////////////////////
			CellMap cellMap;
			size_t numExploredCells;
			std::unordered_set<int> visitedCells;
			////////////////////////////////////////////////////////
			// END BRAIN 
			////////////////////////////////////////////////////////

			NEOGEN::Path path;					//Cami a seguir
			int currentPortal;					//indica quin �s el portal cap al que es dirigeix el personatge
			
			Ogre::AnimationState *animState;	// L'animacio actual de l'objecte

			virtual void initializeBodyParams();
			
			virtual void updateAnimState(Ogre::Real timeLF);
			virtual void updateGraphics();
		
			/**********************************************/
			/* ACTUALITZACI� DE L'ESTAT */
			/**********************************************/
			virtual void choosePatternState();
			virtual void updateState(Ogre::Real timeLF);
			virtual void updateStateIdle();
			virtual void updateStateRandomTarget();
			virtual void updateStateScriptedPath();
	};
};

#endif // #ifndef __RogueAutonomousCharacter_h_