#ifndef __RogueGUIBase_h_
#define __RogueGUIBase_h_

#include <OgreLogManager.h>
#include "RogueTypes.h"

namespace Rogue {

	class GUIBase : public Rogue::SdkTrayListener {

		public:

			typedef std::vector<Rogue::Widget*> WidgetList;

			GUIBase(Rogue::SdkTrayManager* tMgr);
			virtual ~GUIBase();

			virtual void setVisibleAll(bool visible) {
				if (visible) _trayMgr->showAll();
				else _trayMgr->hideAll();
			}

			virtual void update() {}
		
		protected:

			Rogue::SdkTrayManager* _trayMgr;

			virtual void createGUI() {}
		
			//SdkTrayListener functions
			virtual void buttonHit(Rogue::Button* button) {}
			virtual void itemSelected(Rogue::SelectMenu* menu) {}
			virtual void labelHit(Rogue::Label* label) {}
			virtual void sliderMoved(Rogue::Slider* slider) {}
			virtual void checkBoxToggled(Rogue::CheckBox* box) {}
			virtual void okDialogClosed(const Ogre::DisplayString& message) {}
			virtual void yesNoDialogClosed(const Ogre::DisplayString& question, bool yesHit) {}
	};

};

#endif