#ifndef __RogueInterfaceCameraManager_h_
#define __RogueInterfaceCameraManager_h_


#include "RogueInterfaceBase.h"

namespace Rogue {

	class InterfaceCameraManager : public InterfaceBase {

		public:

			virtual void setCamera(Camera* cam) = 0;
			virtual Camera* getCamera() = 0;
			virtual void fixOrientation(bool f) = 0;

			virtual void update() = 0;

			virtual void viewTop() = 0;
			/*virtual void viewFront() = 0;
			virtual void viewSide() = 0;*/

	};

};

#endif 