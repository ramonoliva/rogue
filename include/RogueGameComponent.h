#ifndef __RogueGameComponent_h_
#define __RogueGameComponent_h_

#include <vector>
#include <unordered_map>
#include <typeindex>
#include "RogueTypes.h"

namespace Rogue {

	class GameObject;

	class GameComponent {

		public:

			GameComponent() {
				_gameObject = NULL;
			}

			virtual ~GameComponent() {
				_gameObject = NULL;
			}

			virtual GameObject* getGameObject() {
				return _gameObject;
			}

			virtual void setGameObject(GameObject* go) {
				_gameObject = go;
			}

			virtual void update() {}

		protected:

			GameObject* _gameObject;

	};

	typedef std::unordered_map<std::type_index, GameComponent*> GameComponentList;

};

#endif