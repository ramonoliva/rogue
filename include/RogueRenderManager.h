#ifndef __RogueRenderManager_h_
#define __RogueRenderManager_h_

#include <OgreSingleton.h>
#include <OgreWindowEventUtilities.h>
#include <OgreOverlaySystem.h>
#include <OgreFrameListener.h>
#include <OgreTimer.h>

#include <unordered_set>

#include "RogueInterfaceRenderManager.h"
#include "RogueDeferredPass.h"

namespace Rogue {

	class RenderManager : public InterfaceRenderManager, public RenderTargetListener {

		public:
		
			RenderManager(const String& windowName, SceneType sceneType);
			virtual ~RenderManager();

			virtual bool showConfigDialog();
			virtual bool restoreConfig();

			virtual void createRenderWindow(bool autoCreateWindow, const String& windowName);
			virtual SceneManager* createSceneManager(Ogre::SceneType type, const String& name);
			virtual Camera* createCamera(const String& cameraName, CameraType type, float zNear = 0.1f, float zFar = 1000.0f, SceneManager* sceneManager = NULL);
			virtual SceneNode* createSceneNode(SceneNode* parent = NULL);
			virtual void destroySceneNode(SceneNode* node);
			virtual void destroyAllAttachedMovableObjects(SceneNode* node);
			virtual Entity* createEntity(const String& meshFile);
			virtual ManualObject* createManualObject();
			virtual RaySceneQuery* createRaySceneQuery();
			virtual void destroyRaySceneQuery(RaySceneQuery* q);
			virtual Light* createLight(const String& name);

			virtual DeferredChain* createDeferredChain(const String chainName);
			virtual void setDeferredChainActive(const String chainName);

			virtual void setupResourcesDebug();
			virtual void setupResourcesRelease();

			virtual RenderWindow* getRenderWindow();
			virtual void getRenderWindowSize(size_t& w, size_t& h);
			virtual Camera* getCamera(const String& cameraName);
			virtual Camera* getMainCamera();
			virtual Camera* getDeferredCamera();
			virtual SceneManager* getSceneManager();
			virtual SceneNode* getRootSceneNode();
			virtual void setParentSceneNode(SceneNode* node, SceneNode* parent);
			virtual void setRenderMode(RenderMode rMode);
			virtual RenderMode getRenderMode();
			virtual void setAmbientLightColor(const Color& color);

			virtual void update();
			virtual void renderOneFrame();

			virtual void addFrameListener(Ogre::FrameListener* listener);
			virtual void removeFrameListener(Ogre::FrameListener* listener);

			virtual void addWindowEventListener(WindowEventListener* listener);
			virtual void removeWindowEventListener(WindowEventListener* listener);
			
			virtual void clearViewport(Viewport* vp, const Color& bgColor);
			virtual void clearViewport(Viewport* vp);
			virtual Rectangle2D* createRenderQuad(float left, float top, float right, float bottom);
			virtual MultiRenderTarget* createMultiRenderTarget(const String& mrtName, TexList& tList, size_t w, size_t h, PixelFormat pFormat, const Color& bgColor, bool clearEveryFrame = true, Camera* cam = NULL);
			virtual TexturePtr createTexture(const String& texName, size_t w, size_t h, PixelFormat format, int usage = Ogre::TU_RENDERTARGET);
			virtual TexturePtr createTexture1D(const String& texName, size_t w, PixelFormat format, int usage = Ogre::TU_RENDERTARGET);
			virtual TexturePtr getTexture(const String& texName);
			virtual RenderTexture* createRenderTexture(const String& texName, size_t w, size_t h, PixelFormat format, const Color& bgColor = Color::ZERO, bool clearEveryFrame = true, Camera* cam = NULL);
			virtual RenderTexture* getRenderTexture(const String& texName);
			virtual void saveTextureToDisk(const String& texName, const String& fileName);
			virtual void takeScreenShot(const String& picName, const String& picExtension);
			
		protected:

			typedef std::unordered_set<SceneNode*> SceneNodeList;

			Ogre::Root* _ogreRoot;
			RenderWindow* _renderWindow;
			Ogre::OverlaySystem* _overlaySystem;
			Ogre::RenderSystem* _renderSystem;
			Ogre::TextureManager* _texManager;
			SceneNodeList _sceneNodes;

			SceneManager* _sceneMgr;			//The main Scene Manager
			SceneManager* _deferredSceneMgr;	//The render manager used to draw the quad on the screen when in deferred

			Camera* _mainCamera;				//The main camera
			Camera* _deferredCamera;			//The camera used to draw the quad on the screen when in deferred

			String _resourcesCFG;
			String _pluginsCFG;

			Ogre::Timer _timer;
			unsigned long _timeLastUpdate;
			RenderMode _renderMode;

			virtual void createOverlaySystem();

			virtual void renderOneFrameForward();
						
			/********************************************/
			/* Deferred Rendering Mode */
			/********************************************/
			Rectangle2D* _renderQuad;				//Quad that composites the final image when we are in Deferred Rendering Mode
			RenderTargetList _rtList;				//The list of all the render targets that has been created
			RenderTargetList _rtActiveList;			//The list of all the render targets that are currently active
			
			DeferredChainList _deferredChainList;	//The list of all created deferred chains
			DeferredChain* _activeDeferredChain;	//The current active deferred chain

			virtual void setRenderTargetActive(RenderTarget* renderTarget, bool active);
			
			virtual void renderOneFrameDeferred();
			virtual void clearGBuffer();
			virtual void setMaterialModels(const String& matName, SceneNodeList& mList);
			virtual void showModels(bool show);
			virtual void computeProjectionMatrix(Camera* cam, Ogre::Matrix4& projectionMatrix, bool requiresTextureFlipping);

			/********************************************/
			/* RTTListener Functions */
			/********************************************/
			virtual void preRenderTargetUpdate(const Ogre::RenderTargetEvent& evt);
			virtual void postRenderTargetUpdate(const Ogre::RenderTargetEvent& evt);
	};

};

#endif