#ifndef __RogueInterfaceGUIManager_h_
#define __RogueInterfaceGUIManager_h_

#include <utility>
#include <type_traits>
#include <unordered_map>

#include "RogueGUIBase.h"
#include "RogueInterfaceBase.h"

namespace Rogue {

	class InterfaceGUIManager : public InterfaceBase {

		public:

			enum GUIModeEnum {
				GUI_MODE_SHOW_ALL, 
				GUI_MODE_HIDE_ALL_BUT_MOUSE, 
				GUI_MODE_HIDE_ALL, 
				GUI_NUM_MODES
			};

			typedef std::unordered_map<std::string, GUIBase*> GUIMap;
			typedef std::pair<std::wstring, std::wstring> FileType;
			typedef std::vector<FileType> FileTypeList;
						
			template <class T>
			typename std::enable_if<std::is_base_of<GUIBase, T>::value, T*>::type
			createGUI(const std::string& guiName) {
				//Creates any GUI that derives from GUIBase
				if (_guiMap.find(guiName) != _guiMap.end()) return NULL;	//A GUI with such name already exists
			
				//Create the new GUI
				T* gui = new T(_trayMgr);
				_guiMap[guiName] = gui;
				return gui;
			};

			virtual GUIBase* getGUI(const std::string& guiName) = 0;
			virtual int getGUIMode() = 0;
		
			virtual void destroyGUI(const std::string& guiName) = 0;
			virtual void destroyAllGUI() = 0;

			virtual void setCursorVisible(bool v) = 0;
			virtual void showFrameStats(bool show) = 0;
			virtual void hideAll() = 0;
			virtual void hideAllButMouse() = 0;
			virtual void showAll() = 0;

			virtual String openFile(FileTypeList& typeList, const std::wstring& defaultFileName = L"", const std::wstring& openButton = L"") = 0;
			virtual String saveFile(FileTypeList& typeList, const std::wstring& defaultFileName = L"", const std::wstring& openButton = L"") = 0;
			virtual int addMenuItem(const String& itemName, bool isSubMenu, int parentID = 0) = 0;

			virtual void update() = 0;

		protected:

			OgreBites::SdkTrayManager* _trayMgr;
			GUIMap _guiMap;
	};

};

#endif