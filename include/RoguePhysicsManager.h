#ifndef __RoguePhysicsManager_h_
#define __RoguePhysicsManager_h_

#include <OgreRoot.h>
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
#include <OgreTimer.h>
#include <OgreSingleton.h>

#include "RogueGlobalVars.h"
#include "RogueCollisionFilter.h"

#include "RogueInterfacePhysicsManager.h"

#include <unordered_set>

namespace Rogue {

	class PhysicsManager : public InterfacePhysicsManager {

		public:

			PhysicsManager();
			virtual ~PhysicsManager();

			virtual PhysicsWorld* getPhysicsWorld();
		
			virtual void update();
			virtual void updatePhysics();
			virtual void dispatchCollisions();

			virtual RigidBody* createRigidBody(CollisionShape* shape, float mass, const Vector3& position, short collisionFilterGroup = (short)1, short collisionFilterMask = (short)-1);
			virtual RigidBody* createRigidBody(CollisionShape* shape, float mass, short collisionFilterGroup = (short)1, short collisionFilterMask = (short)-1);
			virtual void removeRigidBody(RigidBody* body);
			virtual void clearWorld();

			virtual GhostObject* createGhostObject(CollisionShape* shape, short collisionFilterGroup = (short)-1, short collisionFilterMask = (short)-1);
					
			virtual void rayCastCursor(RayCastResult& result, Camera* camera, short collisionFilterGroup = (short)-1, short collisionFilterMask = (short)-1);
			virtual void rayCast(Vector3& from, Vector3& to, RayCastResult& result, short collisionFilterGroup = (short)-1, short collisionFilterMask = (short)-1);

		protected:

			typedef std::unordered_set<CollisionObject*> CollisionObjectList;
			CollisionObjectList _collisionObjectList;		//The list of all bodies added to the world

			Ogre::Timer _timer;
			unsigned long _timePrevStep;
			unsigned long _timeCurrStep;
			unsigned long _timeLastUpdate;
		
			/****************************************/
			/* THE BULLET WORLD */
			/****************************************/
			PhysicsWorld *_world;
			BroadphaseInterface* _broadphase;
			DefaultCollisionConfiguration* _collisionConfig;
			CollisionDispatcher* _dispatcher;
			ConstraintSolver* _constraintSolver;

			virtual bool addCollisionPair(CollisionPair& pair);
			virtual bool removeCollisionPair(CollisionPair& pair);

			virtual void onCollisionPairAdded(CollisionPair& pair);		//Actions to do when the collision pair is created for the first time
			virtual void onCollisionPairStay(CollisionPair& pair);		//Actions to do when the bodies are in contact
			virtual void onCollisionPairRemoved(CollisionPair& pair);	//Actions to do when the bodies are not in contact anymore

			virtual CollisionPair getCollisionPairKey(CollisionPair& pair);
	};

};

#endif