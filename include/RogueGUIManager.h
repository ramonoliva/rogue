#ifndef __RogueGUIManager_h_
#define __RogueGUIManager_h_

#include <OgreOverlayManager.h>
#include <OgreOverlay.h>
#include <OgreOverlayContainer.h>
#include <OgreOverlayElement.h>
#include <SdkTrays.h>
#include <OgreLogManager.h>

#include <string>
#include <unordered_map>

#include "RogueInterfaceGUIManager.h"
#include "RogueInputListener.h"

#include <windows.h>
#include <stdlib.h>     //wcstombs, wchar_t(C)
#include <shobjidl.h> 
#include <locale>		// std::wstring_convert
#include <codecvt>      // std::codecvt_utf8
#include <atlbase.h>

namespace Rogue {

	class GUIManager : 
		public InterfaceGUIManager, 
		public FrameListener, 
		public InputListener 
	{

		public: 

			GUIManager();
			virtual ~GUIManager();

			GUIBase* getGUI(const std::string& guiName);
			virtual int getGUIMode();
		
			virtual void destroyGUI(const std::string& guiName);
			virtual void destroyAllGUI();

			virtual void setCursorVisible(bool v);
			virtual void showFrameStats(bool show);
			virtual void hideAll();
			virtual void hideAllButMouse();
			virtual void showAll();

			virtual String openFile(FileTypeList& typeList, const std::wstring& defaultFileName = L"", const std::wstring& openButton = L"");
			virtual String saveFile(FileTypeList& typeList, const std::wstring& defaultFileName = L"", const std::wstring& openButton = L"");
			virtual int addMenuItem(const String& itemName, bool isSubMenu, int parentID = 0);

			//InputListener
			virtual void injectMouseMove(const MouseEvent &arg);
			virtual void injectMouseDown(const MouseEvent &arg, MouseButtonID id);
			virtual void injectMouseUp(const MouseEvent &arg, MouseButtonID id);

			virtual void update();

		protected:

			virtual String fileDialog(CComPtr<IFileDialog> dialogWindow, FileTypeList& typeList, const std::wstring& title = L"", const std::wstring& defaultFileName = L"", const std::wstring& openButton = L"");
			
			virtual void createMenu();
			virtual int registerMenu(HMENU menu);
			virtual HMENU getMenu(int id);

			// Ogre::FrameListener
			virtual bool frameStarted (const FrameEvent &evt);
			virtual bool frameRenderingQueued(const FrameEvent& evt);
			virtual bool frameEnded(const FrameEvent &evt);

			typedef std::unordered_map<int, HMENU> MenuItemTable;

			MenuItemTable _menuItemTable;
			int _menuItemID;
			int _guiMode;
			HWND _windowHandler;	//The handler of the Render Window
			HMENU _menuHandler;		//The main toolbar menu
	};

};

#endif