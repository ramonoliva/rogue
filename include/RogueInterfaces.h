#ifndef __RogueInterfaces_h_
#define __RogueInterfaces_h_

#include "RogueInterfaceBase.h"
#include "RogueInterfaceCameraManager.h"
#include "RogueInterfaceGUIManager.h"
#include "RogueInterfaceInputManager.h"
#include "RogueInterfacePhysicsManager.h"
#include "RogueInterfaceRenderManager.h"

#endif