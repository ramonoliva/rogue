#ifndef __RogueScene_h_
#define __RogueScene_h_

#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>

#include "RogueCore.h"
#include "RogueGlobalVars.h"
#include "RogueGameObject.h"

namespace Rogue {

	class Scene {

		public:
    
			Scene();
			virtual ~Scene();
	
			static Scene* getCurrent();
			static GameObjectSet& getGameObjects() {
				return _gameObjects;
			};

			static GameObject* createGameObject(const Vector3& position = Vector3(0,0,0));
			static void destroyGameObject(GameObject* go);

			virtual void createScene(){};
			virtual void destroyScene();
			virtual void update(){};

		protected:
    
			static Scene* _current;	//A pointer to the current scene
			Ogre::SceneManager* _sceneMgr;
			static GameObjectSet _gameObjects;
			
			//Direct access to GameManagers
			InterfaceRenderManager* _renderManager;
			InterfacePhysicsManager* _physicsManager;
			InterfaceGUIManager* _guiManager;
			InterfaceCameraManager* _cameraManager;
			InterfaceInputManager* _inputManager;
					
			virtual void processMouseEvents(){};
			virtual void processKeyboardEvents() {
				if (Core::getManager<InterfaceInputManager>()->isTriggeredKey(OIS::KC_ESCAPE)) G_SCENE_STATE = SCENE_NULL;	
			};
	};

	typedef std::vector<Scene*> SceneList;

};

#endif // #ifndef __RogueScene_h_
