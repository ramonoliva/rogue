#ifndef __RogueBaseCharacter_h_
#define __RogueBaseCharacter_h_

#include <OgreVector3.h>
#include <OgreSceneNode.h>
#include <OgreEntity.h>
#include <OgreAnimationState.h>

#include "BtOgrePG.h"
#include "BtOgreGP.h"

#include <vector>

#include <ngVector.h>
#include <ngVoxelGrid.h>
#include <ngNavMesh.h>

#include "OpenSteer/BaseVehicle.h"

#include "OgreUtils.h"
#include "Converter.h"

#include "RogueTypes.h"
#include "RogueRigidBody.h"
#include "RogueDebuggerBase.h"

namespace Rogue {

	struct BaseCharConfigurator {
		bool isInstanced;
		Ogre::InstancedEntity* instancedEnt;
		Ogre::Entity* ent;
		Ogre::SceneNode* node;
		Ogre::Vector3 pos;
		NEOGEN::NavMesh* navMesh;
		float height;
		float radius;
	};

	struct OpenSteerCharConfigurator {
		OpenSteer::ProximityDataBase* pdb;
	};

	class BaseCharacter : public OpenSteer::BaseVehicle {

		protected:

			bool _isInstanced;
			RigidBody* _rigidBody;
			Ogre::InstancedEntity* _instancedEnt;
			Ogre::Entity *ent;						// Punter a l'entity que estem animant
			Ogre::SceneNode *node;					// Punter al SceneNode al que est� assignat l'entitat
		
			float diffCenterY;			// Difer�ncia de distancia en l'eix y entre l'objecte i la seva shape
			float _height;				//Character height

			int state;					//Indica l'estat en el que es troba el personatge
			
			NEOGEN::NavMesh* navMesh;			
			int currentCellID;			//Identificador de la cel�la on est� actualment el personatge
			std::set<int> _overlappingCells;
			int _waypointMode;		//Indicates the mode used to compute the seek point over the portal. 

			NEOGEN::Vec3D _wayPoint;				//The Waypoint of the current portal
			NEOGEN::Vec3D _portalV1;				//The start endpoint of the current shrinked portal
			NEOGEN::Vec3D _portalV2;				//The final endpoint of the current shrinked portal

			DebuggerBase* _debugger;

		public: 

			//WARNING!!! DEBUG PURPOSES ONLY
			std::map<int, int> _scriptedPath;
			
			int targetCellID;					//Indica la cel�la on es troba l'objectiu
			
			typedef enum {
				PORTAL_SEEK_NULL = -1, 
				PORTAL_SEEK_PROJECTION,
				PORTAL_SEEK_MID_POINT
			} PortalSeekEnum;

			/*************************************/
			/* CREADORA I DESTRUCTORA			 */
			/*************************************/

			BaseCharacter(BaseCharConfigurator& baseCFG, RigidBody* rigidBody, OpenSteerCharConfigurator& openSteerCFG);
			virtual ~BaseCharacter();

			/*************************************/
			/* GET I SET						 */
			/*************************************/
			virtual btVector3 getForwardDir() {
				return btVector3(0,0,0);
			}
			virtual btVector3 getUpDir() {
				return btVector3(0,0,0);
			}
			virtual btVector3 getStrafeDir() {
				return btVector3(0,0,0);
			}
			
			void clearOverlappingCells();
			void addOverlappingCell(int cellID);
			bool isInCell(int cellID);

			void setMaterial(const Ogre::String& matName);
			void getWayPoint(NEOGEN::Vec3D& result);
			void getShrinkedPortal(NEOGEN::Vec3D& v1, NEOGEN::Vec3D& v2);
			int getState();
			void setState(int s);
			void setWaypointMode(int mode);
			int getWaypointMode();
			float getHeight();
			void setHeight(float h);

			void setVisible(bool v);

			void setPosition(Ogre::Vector3& pos);
			
			Ogre::Vector3 getPosition();
			Ogre::Quaternion getOrientation();
			Ogre::SceneNode* getNode();
			Ogre::Real getDiffCenterY();

			Ogre::Entity* getEntity();

			DebuggerBase* getDebugger();
			void setDebugger(DebuggerBase* debugger);

			virtual void setCurrentCellID(int cellID);
			int getCurrentCellID();
		
			/*************************************/
			/* ACTUALITZACIO					 */
			/*************************************/

			virtual void update();
			virtual void updateLogic(float elapsedTime);
			virtual void updateRender(float elapsedTime);
	};

	typedef std::vector<BaseCharacter*> CharList;
};

#endif