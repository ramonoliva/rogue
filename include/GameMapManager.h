#ifndef __RogueGameMapManager_H__
#define __RogueGameMapManager_H__
 
#include <OgreResourceManager.h>
#include <OgreLogManager.h>
#include <OgreStringConverter.h>
#include "GameMap.h"

#include <vector>
 
namespace Rogue {

	class GameMapManager : public Ogre::ResourceManager, public Ogre::Singleton<GameMapManager> {

		public:

			GameMapManager();
			virtual ~GameMapManager();
			void initialize();
 
			static GameMapManager &getSingleton();
			static GameMapManager *getSingletonPtr();

			const Ogre::StringVector& getGameMapList();

			size_t getNumResources();
			void setupResources();

		protected:
 
			// must implement this from ResourceManager's interface
			Ogre::Resource *createImpl(const Ogre::String &name, Ogre::ResourceHandle handle, 
			const Ogre::String &group, bool isManual, Ogre::ManualResourceLoader *loader, 
			const Ogre::NameValuePairList *createParams);

		private:

			Ogre::StringVector _gameMapList;
	};
 
};

#endif