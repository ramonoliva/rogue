#ifndef __RogueInterfaceRenderManager_h_
#define __RogueInterfaceRenderManager_h_

/*
Describes the interface of the Render Service
*/

#include "RogueInterfaceBase.h"

namespace Rogue {

	enum RenderMode {
		RENDER_MODE_FORWARD, 
		RENDER_MODE_DEFERRED
	};

	typedef std::vector<String> TexList;

	class InterfaceRenderManager : public InterfaceBase {

		public:

			virtual bool showConfigDialog() = 0;
			virtual bool restoreConfig() = 0;

			virtual void createRenderWindow(bool autoCreateWindow, const String& windowName) = 0;
			virtual SceneManager* createSceneManager(Ogre::SceneType type, const String& name) = 0;
			virtual Camera* createCamera(const String& cameraName, CameraType type, float zNear = 0.1f, float zFar = 1000.0f, SceneManager* sceneManager = NULL) = 0;
			virtual SceneNode* createSceneNode(SceneNode* parent = NULL) = 0;
			virtual void destroySceneNode(SceneNode* node) = 0;
			virtual void destroyAllAttachedMovableObjects(SceneNode* node) = 0;
			virtual Entity* createEntity(const String& meshFile) = 0;
			virtual ManualObject* createManualObject() = 0;
			virtual RaySceneQuery* createRaySceneQuery() = 0;
			virtual void destroyRaySceneQuery(RaySceneQuery* q) = 0;
			virtual Light* createLight(const String& name) = 0;

			virtual DeferredChain* createDeferredChain(const String chainName) = 0;
			virtual void setDeferredChainActive(const String chainName) = 0;

			virtual void clearViewport(Viewport* vp, const Color& bgColor) = 0;
			virtual void clearViewport(Viewport* vp) = 0;
			virtual Rectangle2D* createRenderQuad(float left, float top, float right, float bottom) = 0;
			virtual MultiRenderTarget* createMultiRenderTarget(const String& mrtName, TexList& tList, size_t w, size_t h, PixelFormat pFormat, const Color& bgColor, bool clearEveryFrame = true, Camera* cam = NULL) = 0;
			virtual TexturePtr createTexture(const String& texName, size_t w, size_t h, PixelFormat format, int usage = Ogre::TU_RENDERTARGET) = 0;
			virtual TexturePtr createTexture1D(const String& texName, size_t w, PixelFormat format, int usage = Ogre::TU_RENDERTARGET) = 0;
			virtual TexturePtr getTexture(const String& texName) = 0;
			virtual RenderTexture* createRenderTexture(const String& texName, size_t w, size_t h, PixelFormat format, const Color& bgColor = Color::Black, bool clearEveryFrame = true, Camera* cam = NULL) = 0;
			virtual RenderTexture* getRenderTexture(const String& texName) = 0;
			virtual void saveTextureToDisk(const String& texName, const String& fileName) = 0;
			virtual void takeScreenShot(const String& picName, const String& picExtension) = 0;

			virtual void setupResourcesDebug() = 0;
			virtual void setupResourcesRelease() = 0;

			virtual RenderWindow* getRenderWindow() = 0;
			virtual void getRenderWindowSize(size_t& w, size_t& h) = 0;
			virtual Camera* getCamera(const String& cameraName) = 0;
			virtual Camera* getMainCamera() = 0;
			virtual Camera* getDeferredCamera() = 0;
			virtual SceneManager* getSceneManager() = 0;
			virtual SceneNode* getRootSceneNode() = 0;
			virtual void setParentSceneNode(SceneNode* node, SceneNode* parent) = 0;
			virtual void setRenderMode(RenderMode rMode) = 0;
			virtual RenderMode getRenderMode() = 0;
			virtual void setAmbientLightColor(const Color& color) = 0;

			virtual void update() = 0;
			virtual void renderOneFrame() = 0;

			virtual void addFrameListener(FrameListener* listener) = 0;
			virtual void removeFrameListener(FrameListener* listener) = 0;

			virtual void addWindowEventListener(WindowEventListener* listener) = 0;
			virtual void removeWindowEventListener(WindowEventListener* listener) = 0;
	};

};

#endif