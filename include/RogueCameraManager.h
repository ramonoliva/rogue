#ifndef __RogueCameraManager_h_
#define __RogueCameraManager_h_

#include "RogueInterfaceCameraManager.h"
#include "RogueInterfaceInputManager.h"

namespace Rogue {
	
	class CameraManager : public InterfaceCameraManager {

		public:
		
			CameraManager();
			virtual ~CameraManager();
		
			virtual void setCamera(Camera* cam);
			virtual Camera* getCamera();
			virtual void fixOrientation(bool f);

			virtual void update();

			virtual void viewTop();
			/*virtual void viewFront();
			virtual void viewSide();*/

		private:

			Camera* _camera;
			InterfaceInputManager* _inputManager;
			bool _fixOrientation;

	};

};

#endif