#ifndef __DeferredPassLibrary_h_
#define __DeferredPassLibrar_h_

/*
A library of standar deferred passes
*/

#include "RogueTypes.h"
#include "RogueDeferredPass.h"

namespace Rogue {
	
	/****************************************************************/
	/* GBUFFER CONSTRUCTION */
	/****************************************************************/
	class DPFillGBuffer : public DeferredPass {
		public: 
			DPFillGBuffer();
	};

	/****************************************************************/
	/* FINAL COMPOSITION */
	/****************************************************************/
	class DPFinalComposition : public DeferredPass {
		public:
			DPFinalComposition();
	};
};

#endif