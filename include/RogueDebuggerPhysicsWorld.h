#ifndef __RogueDebuggerPhysicsWorld_h_
#define __RogueDebuggerPhysicsWorld_h_

#include "RogueDebuggerBase.h"
#include "BtOgrePG.h"
#include "BtOgreGP.h"

namespace Rogue {

	class DebuggerPhysicsWorld : public DebuggerBase {

		public:

			DebuggerPhysicsWorld();
			~DebuggerPhysicsWorld();

			virtual void update();

		private:

			BtOgre::DebugDrawer* _debugDrawer;

	};

};

#endif