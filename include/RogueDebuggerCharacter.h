#ifndef __RogueDebuggerChar_h_
#define __RogueDebuggerChar_h_

#include "RogueDebuggerBase.h"
#include "RogueBaseCharacter.h"
#include "RogueDebuggerPoint.h"
#include "RogueDebuggerPolyLine.h"

#include <OgreUtils.h>
#include <OgreSceneManager.h>
#include <OgreManualObject.h>
#include <OgreEntity.h>
#include <OgreSceneNode.h>

#include <vector>

namespace Rogue {

	class DebuggerCharacter : public DebuggerBase {

	public:
	
		DebuggerCharacter(BaseCharacter* c);
		virtual ~DebuggerCharacter();

		virtual void clear();

		virtual void setSampleFrequency(float f);
		virtual void setMaterialTrajectory(const std::string& matName);
		virtual BaseCharacter* getCharacter();
	
		virtual void update();
	
		virtual void setVisible(bool v);
		virtual void setVisibleCharacter(bool v);
		virtual void setVisibleCharToWayPoint(bool v);
		virtual void setVisibleTargetPoint(bool v);
		virtual void setVisibleShrinkedPortal(bool v);
		virtual void setVisibleTrajectory(bool v);
		virtual void setVisibleDebugTransform(bool v);

		virtual void clearSamplePoints();
		virtual void saveCurrentPosition();

	protected:

		Ogre::Timer _timer;
		unsigned long _timeLastSample;
		float _sampleFrequency;

		BaseCharacter* _char;

		//Transform Debug
		DebuggerPolyLine* _debuggerTransformForward;
		DebuggerPolyLine* _debuggerTransformUp;
		DebuggerPolyLine* _debuggerTransformRight;

		//Waypoint debug
		DebuggerPolyLine* _debuggerCharToWayPoint;
		DebuggerPoint* _debuggerWayPoint;	//The Waypoint of the current portal
				
		//Shrinked portal debug
		NEOGEN::Vec3D _portalV1;	//The start endpoint of the current shrinked portal
		NEOGEN::Vec3D _portalV2;	//The final endpoint of the current shrinked portal
		DebuggerPolyLine* _debuggerShrinkedPortal;

		//Trajectory debug
		typedef std::vector<Ogre::Vector3> SamplePointList;
		SamplePointList _samplePoints;
		Ogre::ManualObject* _manualSamplePoints;
		Ogre::SceneNode* _nodeSamplePoints;
		size_t _samplePointFirstIndex;
	
		//Used materials
		std::string _materialTrajectory;
		std::string _materialWayPoint;
		std::string _materialTargetPoint;
		std::string _materialShrinkedPortal;

		virtual void updateDebugTransform();
		virtual void updateTrajectory();
		virtual void updateWayPoint();
		virtual void updateTargetPoint();
		virtual void updateShrinkedPortal();
	};

};

#endif