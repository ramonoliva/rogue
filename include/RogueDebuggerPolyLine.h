#ifndef __RogueDebuggerPolyLine_h_
#define __RogueDebuggerPolyLine_h_

#include "RogueDebuggerBase.h"

namespace Rogue {

	class DebuggerPolyLine : public DebuggerBase {

	public:

		DebuggerPolyLine();
		virtual ~DebuggerPolyLine();
		virtual void clear();

		virtual int addVertex(Ogre::Vector3& v);
		virtual void setVertex(size_t id, Ogre::Vector3& value);
		virtual bool getVertex(size_t id, Ogre::Vector3& result);

		virtual void addSegment(size_t v1, size_t v2);

		virtual void setSize(float size);
		virtual float getSize();

		virtual void setColor(size_t color);
		virtual size_t getColor();

		virtual void setPattern(unsigned short pattern);
		virtual unsigned short getPattern();

		virtual void setVisible(bool v);

		virtual void setVertexOffset(float x, float y, float z);
		virtual void getVertexOffset(float& x, float& y, float& z);

		virtual void update();
		
	protected:

		typedef std::vector<Ogre::Vector3> OgreVector3List;
		typedef std::pair<int, int> Segment;
		typedef std::vector<Segment> SegmentList;

		static int POLY_LINE_COUNTER;
		
		int _id;
		std::string _materialName;
		std::string _patternTexName;
		ManualObject* _manual;

		OgreVector3List _vertexList;
		SegmentList _segments;
		Ogre::Vector3 _vertexOffset;	//Offset applied to each vertex of the polyline

		float _size;
		size_t _color;					//AARRGGBB
		unsigned short _pattern;

		std::string getPrefix();

		virtual void createPatternTexture();
		virtual void createMaterial();
		virtual void createManualObject();

		virtual void renderLine();
		virtual void renderMesh();
	};

};

#endif