#ifndef __RogueGameMapSerializer_H__
#define __RogueGameMapSerializer_H__
 
#include <OgreSerializer.h>
#include <OgreLogManager.h>
#include <OgreStringConverter.h>

#include "GameMap.h"
 
namespace Rogue {

	class GameMapSerializer : public Ogre::Serializer {
		public:
		GameMapSerializer();
		virtual ~GameMapSerializer();
 
		void exportGameMap(const GameMap *gMap, const Ogre::String &fileName);
		void importGameMap(Ogre::DataStreamPtr &stream, GameMap *pDest);
	};

};
 
#endif