#ifndef __RogueCore_h_
#define __RogueCore_h_

#include "RogueInterfaces.h"
#include <typeinfo>       
#include <typeindex>   
#include <unordered_map>

/*
Implements a service locator. It allows to register any service that derives from InterfaceBase
*/

namespace Rogue {

	class Core {

		public:

			//The map type that stores the manager list
			typedef std::unordered_map<std::type_index, InterfaceBase*> ManagerList;

			//Register a manager that derives from InterfaceBase
			template <class T>
			static void registerManager(T* manager) {
				_managerList[typeid(T)] = manager;
			};

			//Gets a manager previously registered. If such manager does not exists, returns NULL
			template <class T>
			static T* getManager() {
				if (_managerList.find(typeid(T)) != _managerList.end()) {
					T* manager = dynamic_cast<T*>(_managerList[typeid(T)]);
					return manager;
				}
				return NULL;
			};

			//Removes a manager, if such manager exists. 
			template <class T>
			static void removeManager() {
				if (_managerList.find(typeid(T)) != _managerList.end()) {
					_managerList.erase(typeid(T));
				}
			};

			//Removes all the previously registered managers
			static void removeAllManagers() {
				_managerList.clear();
			};

		protected:

			static ManagerList _managerList;
	};

};

#endif