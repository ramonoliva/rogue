#ifndef __RogueInputManager_h_
#define __RogueInputManager_h_

#include "RogueInterfaceInputManager.h"

#include <unordered_map>

namespace Rogue {

	class InputManager :
		public InterfaceInputManager, 
		public KeyListener, 
		public MouseListener, 
		public WindowEventListener {

		public:

			InputManager();
			virtual ~InputManager();

			virtual void addInputListener(InputListener* listener);
			virtual void removeInputListener(InputListener* listener);

			virtual bool isTriggeredKey(KeyCode key);
			virtual bool isPressedKey(KeyCode key);
			virtual void releaseAllKeys();

			virtual bool isTriggeredMouseButton(MouseButtonID button);
			virtual bool isPressedMouseButton(MouseButtonID button);

			virtual void getMouseCoords(int& mX, int& mY);
			virtual void getMouseDisplacement(int& dX, int& dY, int& dZ);
			virtual void getClippingAreaSize(int& w, int& h);

			virtual Mouse* getMouse();
			virtual Keyboard* getKeyboard();

			virtual void update();

		protected:

			struct KeyState {

				enum State {
					STATE_TRIGGERED, 
					STATE_PRESSED
				};

				State _state;
				unsigned long _timeDown;	//Indicates how many milliseconds the key is being maintained down. 

				KeyState() {
					_timeDown = 0;
				}
			};

			typedef std::unordered_map<KeyCode, KeyState> KeyStateList;
			typedef std::unordered_map<MouseButtonID, KeyState> MouseButtonStateList;

			KeyStateList			_keyStateList;
			MouseButtonStateList	_mouseButtonStateList;
		
			InputListenerList _inputListenerList;

			OIS::InputManager*		_oisManager;
			Mouse*		_mouse;
			Keyboard*	_keyboard;

			void updateKeyState();

			// OIS::KeyListener
			virtual bool keyPressed(const KeyEvent &arg);
			virtual bool keyReleased(const KeyEvent &arg);
		
			// OIS::MouseListener
			virtual bool mouseMoved(const MouseEvent &arg );
			virtual bool mousePressed(const MouseEvent &arg, MouseButtonID id);
			virtual bool mouseReleased(const MouseEvent &arg, MouseButtonID id);
		
			// Ogre::WindowEventListener
			virtual void windowResized(RenderWindow* rw);
			virtual void windowClosed(RenderWindow* rw);
			virtual bool windowClosing(RenderWindow* rw);
	};

};

#endif