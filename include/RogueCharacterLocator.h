#ifndef __RogueCharacterLocator_h_
#define __RogueCharacterLocator_h_

#include <ngNavMesh.h>
#include "RogueCore.h"
#include "RogueTypes.h"
#include "RogueGameObject.h"
#include "RogueBaseCharacter.h"

namespace Rogue {

	class CharacterLocator {

	public:

		CharacterLocator(NEOGEN::NavMesh* navMesh);
		virtual ~CharacterLocator();

		virtual int locateCharacter(BaseCharacter* character);

	protected:

		NEOGEN::NavMesh* _navMesh;

		InterfacePhysicsManager* _physicsManager;

		virtual void create2DMesh();
	};

};

#endif