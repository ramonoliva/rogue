#ifndef __RogueDeferredPass_h_
#define __RogueDeferredPass_h_

#include "RogueTypes.h"
#include <unordered_set>
#include <unordered_map>
#include "RogueCore.h"

namespace Rogue {

	typedef std::unordered_set<RenderTarget*> RenderTargetList;

	class DeferredPass {

		public:

			DeferredPass() {
				_renderManager = Core::getManager<InterfaceRenderManager>();
			}

			virtual ~DeferredPass(){};

			//Actions that has to be done BEFORE rendering to the render targets, eg, render target management, material configuration
			virtual void preRender() {
				for (RenderTargetList::iterator rt = _renderTargets.begin(); rt != _renderTargets.end(); rt++) {
					(*rt)->setActive(true);
					(*rt)->update(true);
				}
			};		

			//Actions that has to be done AFTER rendering to the render targets, eg, deactivate render targets, save the result to an image for debugging purposes
			virtual void postRender() {
				for (RenderTargetList::iterator rt = _renderTargets.begin(); rt != _renderTargets.end(); rt++) {
					(*rt)->setActive(false);
				}
			};	

			void addRenderTarget(RenderTarget* target) {
				_renderTargets.insert(target);
			}

			const String& getMaterialName() {
				return _materialName;
			}

			void setMaterialName(const String& matName) {
				_materialName = matName;
			}

			bool getRenderQuadVisible() {
				return _renderQuadVisible;
			}

		protected:

			bool _renderQuadVisible;					//The render quad has to be rendered on this pass?
			RenderTargetList _renderTargets;			//The render targets that will store the render result
			String _materialName;						//The material used by this pass
			InterfaceRenderManager* _renderManager;		//A pointer to the current render manager

	};
	typedef std::vector<DeferredPass*> DeferredPassList;			//A list of deferred passes

	class DeferredChain {

		public: 

			DeferredChain() {}

			~DeferredChain() {
				for (DeferredPassList::iterator it = _deferredPasses.begin(); it != _deferredPasses.end(); it++) {
					delete *it;
				}
			}

			void addDeferredPass(DeferredPass* pass) {
				_deferredPasses.push_back(pass);
			}

			DeferredPassList& getDeferredPasses() {
				return _deferredPasses;
			}

		private:

			DeferredPassList _deferredPasses;
	};

	typedef std::unordered_map<String, DeferredChain*> DeferredChainList;	//A list of deferred chains

};

#endif