#ifndef __RogueRenderer_h_
#define __RogueRenderer_h_

#include "RogueTypes.h"
#include "RogueGameComponent.h"

namespace Rogue {

	class Renderer : public GameComponent {

	public:

		Renderer(Mesh* m, SceneNode* n);
		virtual ~Renderer();

		Mesh* getMesh() {
			return _mesh;
		}

		SceneNode* getSceneNode() {
			return _node;
		}

		virtual void setVisible(bool visible) {
			_node->setVisible(visible);
			_visible = visible;
		}

		virtual void setMaterialName(String matName) {
			_mesh->setMaterialName(matName);
		}

		virtual bool isVisible() {
			return _visible;
		}

		virtual void setScale(Vector3& scale);

		virtual void update();

	protected:

		Mesh* _mesh;	
		SceneNode* _node;
		bool _visible;

	};

};

#endif