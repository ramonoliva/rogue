#pragma once

#include <OgreUtils.h>
#include <OgreSceneManager.h>
#include <OgreManualObject.h>
#include <OgreEntity.h>
#include <OgreSceneNode.h>

#include "RogueDebuggerBase.h"

namespace Rogue {

	class DebuggerPoint : public DebuggerBase {

		public:
	
			DebuggerPoint();
			~DebuggerPoint();

			virtual void setPosition(float x, float y, float z);
			virtual void setPosition(NEOGEN::Vec3D& v);	//TO BE REMOVED

			virtual void setMaterialName(const std::string& matName);

			virtual void setScale(float s);
			
			virtual void setVisible(bool v);
			
			virtual void draw(NEOGEN::Vec3D& v, const Ogre::String& matName);	//TO BE REMOVED

		protected:

			Ogre::SceneNode* _node;

	};

};