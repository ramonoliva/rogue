#ifndef __RogueSceneGame_h_
#define __RogueSceneGame_h_

#include "RogueScene.h"
#include <OgreTimer.h>
#include "GameMap.h"

#include <stdlib.h>
#include <vector>

#include <ngMapParser.h>

#include "RogueAutonomousCharacter.h"
#include "RogueGlobalVars.h"
#include "RogueCharacterLocator.h"
#include "RogueDeferredPassLibrary.h"

#include "RogueDebuggerNavMesh.h"

namespace Rogue {

	class SceneGame : public Scene {

		public:

			typedef std::vector<Ogre::SceneNode*> SceneNodeList;
		
			SceneGame();
			virtual ~SceneGame();
			virtual void createScene();
			virtual void destroyScene();
			virtual void destroyCharacters();
		
			virtual void update();

		protected:

			//WARNING!!!
			bool _pause;
			
			CharacterLocator* _characterLocator;
			Ogre::Timer _timer;
			unsigned long _timePrevAI;
			unsigned long _timeCurrAI;
				
			SceneNodeList _sceneNodeList;
			CharList _charList;					//List of all the characters that are present in the scene
			GameMapPtr _gameMap;				//Estructura que cont� la informaci� del mapa de joc
			NEOGEN::NavMesh* _navMesh;			//The NavMesh of the Scene
			DebuggerNavMesh* _debuggerNavMesh;
			OpenSteer::ProximityDataBase* _proximityDB;	//An acceleration data structure for OpenSteer stuff
			SceneNode* _manualMapNode;	//The manual object that is used to create the game map in auto mode

			virtual void loadMap(const Ogre::String& mapName);
			virtual void loadNavMesh(const String& fileName);
			virtual void resetTimer();
		
			virtual void createProximityDataBase();
			virtual void createGameMapScene(const Ogre::String& sceneFile);
			virtual void createGameMapMesh(const Ogre::String& sceneFile);
			virtual void createGameMapFromNavMesh();
			virtual void createSceneNodes(const pugi::xml_node& sceneNodes, Ogre::Vector3& parentPos, Ogre::Vector3& parentScale);
		
			virtual void addBoundingEdge(NEOGEN::Vec3D& v1, NEOGEN::Vec3D& v2);
			virtual BaseCharacter* loadCharacter(const Ogre::String& meshFile, float radius, float height, float mass, const Ogre::Vector3& pos = Ogre::Vector3::ZERO, const Ogre::Vector3& scale = Ogre::Vector3(1.0f));
			virtual void selectCharacter();
			virtual void locateCharacter(BaseCharacter* character);

			virtual void processMouseEvents();
			virtual void processKeyboardEvents();

			virtual void updateLogic();
				
			virtual void pauseGame();
	};

};

#endif
