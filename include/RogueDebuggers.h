#pragma once

#include <memory>

namespace Rogue {

	class DebuggerBase;
	typedef std::shared_ptr<DebuggerBase> DebuggerBaseSPtr;
	typedef std::unique_ptr<DebuggerBase> DebuggerBaseUPtr;
	typedef std::weak_ptr<DebuggerBase> DebuggerBaseWPtr;
	
	class DebuggerCharacter;
	typedef std::shared_ptr<DebuggerCharacter> DebuggerCharacterSPtr;
	typedef std::unique_ptr<DebuggerCharacter> DebuggerCharacterUPtr;
	typedef std::weak_ptr<DebuggerCharacter> DebuggerCharacterWPtr;

	class DebuggerPhysicsWorld;
	typedef std::shared_ptr<DebuggerPhysicsWorld> DebuggerPhysicsWorldSPtr;
	typedef std::unique_ptr<DebuggerPhysicsWorld> DebuggerPhysicsWorldUPtr;
	typedef std::weak_ptr<DebuggerPhysicsWorld> DebuggerPhysicsWorldWPtr;

	class DebuggerPoint;
	typedef std::shared_ptr<DebuggerPoint> DebuggerPointSPtr;
	typedef std::unique_ptr<DebuggerPoint> DebuggerPointUPtr;
	typedef std::weak_ptr<DebuggerPoint> DebuggerPointWPtr;

	class DebuggerPolyLine;
	typedef std::shared_ptr<DebuggerPolyLine> DebuggerPolyLineSPtr;
	typedef std::unique_ptr<DebuggerPolyLine> DebuggerPolyLineUPtr;
	typedef std::weak_ptr<DebuggerPolyLine> DebuggerPolyLineWPtr;

};