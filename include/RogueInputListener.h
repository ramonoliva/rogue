#ifndef __RogueInputListener_h_
#define __RogueInputListener_h_

#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

#include <unordered_set>

namespace Rogue {

	class InputListener {

		public:

			virtual void injectMouseMove(const OIS::MouseEvent &arg) = 0;
			virtual void injectMouseDown(const OIS::MouseEvent &arg, OIS::MouseButtonID id) = 0;
			virtual void injectMouseUp(const OIS::MouseEvent &arg, OIS::MouseButtonID id) = 0;

	};

	typedef std::unordered_set<InputListener*> InputListenerList;
};

#endif