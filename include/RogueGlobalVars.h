#ifndef __RogueGlobalVars_h_
#define __RogueGlobalVars_h_

#include <OgreString.h>

#include "BtOgrePG.h"
#include "BtOgreGP.h"
#include "ngVector.h"

#include "RogueTypes.h"

namespace Rogue {

	extern int G_SCENE_STATE;
	extern String G_APP_NAME;
	extern bool G_MOUSE_EXCLUSIVE;
	extern String G_DEFERRED_CHAIN_MAIN;

	#define SCENE_NULL	-1

	enum PlayerActionEnum {
		PLAYER_ACTION_NONE,
		PLAYER_ACTION_ATTACK, 
		PLAYER_ACTION_USE_GENERIC_ITEM, 
		PLAYER_ACTION_MOVE
	};

	enum SHADER_PROGRAM {
		VERTEX_SHADER, 
		GEOMETRY_SHADER, 
		FRAGMENT_SHADER
	};
	
	static char* wideCharToChar(wchar_t* src) {
		int size = 0;
		while (src[size] != '\0') size++;
		size++;
		
		char* dst = new char[size];

		size_t nBytes = sizeof(char*) * size;
		wcstombs(dst, src, nBytes);
		dst[size - 1]= '\0';

		return dst;
	}

	// Convert a wide Unicode string to an UTF8 string
	static String wideStringToString(const std::wstring &wstr) {
		int size_needed = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
		std::string strTo(size_needed, 0);
		WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), &strTo[0], size_needed, NULL, NULL);
		return strTo;
	}

	static Ogre::GpuProgramParametersSharedPtr getShaderParams(const Ogre::String& matName, int passNum, SHADER_PROGRAM progID) {
		Ogre::MaterialPtr material = Ogre::MaterialManager::getSingletonPtr()->getByName(matName);
		Ogre::GpuProgramParametersSharedPtr shaderParams;
		if (progID == VERTEX_SHADER) shaderParams = material->getTechnique(0)->getPass(passNum)->getVertexProgramParameters();
		else if (progID == GEOMETRY_SHADER) shaderParams = material->getTechnique(0)->getPass(passNum)->getGeometryProgramParameters();
		else shaderParams = material->getTechnique(0)->getPass(passNum)->getFragmentProgramParameters();
		return shaderParams;
	}

	static void setUniformTexture(const Ogre::String& matName, int passNum, int texNum, const Ogre::String& texName) {
		Ogre::MaterialPtr material = Ogre::MaterialManager::getSingletonPtr()->getByName(matName);
		Ogre::TextureUnitState* tUnitState = material->getTechnique(0)->getPass(passNum)->getTextureUnitState(texNum);
		tUnitState->setName(texName);
	}

	static void setUniform(const Ogre::String& matName, int passNum, SHADER_PROGRAM progID, const Ogre::String& uniformName, float uniformValue) {
		Ogre::GpuProgramParametersSharedPtr shaderParams = getShaderParams(matName, passNum, progID);
		shaderParams->setNamedConstant(uniformName, uniformValue);
	}

	static void setUniform(const Ogre::String& matName, int passNum, SHADER_PROGRAM progID, const Ogre::String& uniformName, NEOGEN::Vec3D& uniformValue) {
		Ogre::GpuProgramParametersSharedPtr shaderParams = getShaderParams(matName, passNum, progID);
		shaderParams->setNamedConstant(uniformName, Ogre::Vector3(uniformValue.getX(), uniformValue.getY(), uniformValue.getZ()));
	}

	static void setUniform(const Ogre::String& matName, int passNum, SHADER_PROGRAM progID, const Ogre::String& uniformName, const Ogre::Vector3& uniformValue) {
		Ogre::GpuProgramParametersSharedPtr shaderParams = getShaderParams(matName, passNum, progID);
		shaderParams->setNamedConstant(uniformName, uniformValue);
	}

	static void setUniform(const Ogre::String& matName, int passNum, SHADER_PROGRAM progID, const Ogre::String& uniformName, Ogre::Vector4& uniformValue) {
		Ogre::GpuProgramParametersSharedPtr shaderParams = getShaderParams(matName, passNum, progID);
		shaderParams->setNamedConstant(uniformName, uniformValue);
	}

	static void setUniform(const Ogre::String& matName, int passNum, SHADER_PROGRAM progID, const Ogre::String& uniformName, const Color& uniformValue) {
		Ogre::GpuProgramParametersSharedPtr shaderParams = getShaderParams(matName, passNum, progID);
		shaderParams->setNamedConstant(uniformName, uniformValue);
	}

	static void setUniform(const Ogre::String& matName, int passNum, SHADER_PROGRAM progID, const Ogre::String& uniformName, int uniformValue) {
		Ogre::GpuProgramParametersSharedPtr shaderParams = getShaderParams(matName, passNum, progID);
		shaderParams->setNamedConstant(uniformName, uniformValue);
	}

	static void setUniform(const Ogre::String& matName, int passNum, SHADER_PROGRAM progID, const Ogre::String& uniformName, Ogre::Matrix4& uniformValue) {
		Ogre::GpuProgramParametersSharedPtr shaderParams = getShaderParams(matName, passNum, progID);
		shaderParams->setNamedConstant(uniformName, uniformValue);
	}

};

#endif