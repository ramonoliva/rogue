#ifndef __RogueGameMap_H__
#define __RogueGameMap_H__
 
#include <OgreResourceManager.h>

enum GeometryModeEnum {
	GEOMETRY_MODE_SCENE = 1, 
	GEOMETRY_MODE_MESH = 2,
	GEOMETRY_MODE_AUTO = 3,		//Generates the Map directly from the NavMesh
};
 
namespace Rogue {

	class GameMap : public Ogre::Resource {
     
		public:

			GameMap(Ogre::ResourceManager *creator, 
					const Ogre::String &name, 
					Ogre::ResourceHandle handle, 
					const Ogre::String &group, 
					bool isManual = false, 
					Ogre::ManualResourceLoader *loader = 0);
 
			virtual ~GameMap();

			void setGeometryMode(int mode);
			int getGeometryMode();
			void setGeometryFile(const Ogre::String& fName);
			const Ogre::String& getGeometryFile() const;
			void setImgPreview(const Ogre::String& fName);
			const Ogre::String& getImgPreview() const;
			void setNavMeshFile(const Ogre::String& fName);
			const Ogre::String& getNavMeshFile() const;
			void setRedFlagPos(float x, float y, float z);
			void setBlueFlagPos(float x, float y, float z);
			void getRedFlagPos(float& x, float& y, float& z);
			void getBlueFlagPos(float& x, float& y, float& z);

		protected:
 			// must implement these from the Ogre::Resource interface
			void loadImpl();
			void unloadImpl();
			size_t calculateSize() const;

		private:

			int geometryMode;			//Indica si la informaci� del mapa est� en una imatge o en un fitxer .scene
			Ogre::String geometryFile;	//Fitxer que cont� la geometria del mapa (imatge o fitxer .scene)
			Ogre::String imgPreview;	//Imatge que cont� la preview del mapa
			Ogre::String navMeshFile;	//Fitxer que cont� la informaci� de la navMesh del mapa

			float flagRedPosX, flagRedPosY, flagRedPosZ;
			float flagBluePosX, flagBluePosY, flagBluePosZ;
	};
	
	typedef Ogre::SharedPtr<GameMap> GameMapPtr;
 
};

#endif