#ifndef __RogueDebuggerNavMesh_h_
#define __RogueDebuggerNavMesh_h_

#include "RogueDebuggerPolyLine.h"
#include "ngNavMesh.h"
#include <set>

namespace Rogue {

	class DebuggerNavMesh : public DebuggerBase {

	public:

		DebuggerNavMesh(NEOGEN::NavMesh* navMesh = NULL);
		~DebuggerNavMesh();
		virtual void clear();

		virtual void setNavMesh(NEOGEN::NavMesh* navMesh);
		virtual NEOGEN::NavMesh* getNavMesh();

		virtual void setVisible(bool v);

		virtual void setEdgeSize(float size);
		virtual float getEdgeSize();
		
		virtual void setEdgeOffset(float x, float y, float z);
		virtual void getEdgeOffset(float& x, float& y, float& z);

		virtual void update();

	protected:

		DebuggerPolyLine* _debuggerObstacleEdges;
		DebuggerPolyLine* _debuggerPortalEdges;

		NEOGEN::NavMesh* _navMesh;
		

	};

};

#endif