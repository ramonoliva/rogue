#ifndef __RogueDebuggerBase_h_
#define __RogueDebuggerBase_h_

#include <string>
#include "RogueTypes.h"
#include "RogueInterfaceRenderManager.h"

#include "OgreUtils.h"

namespace Rogue {
	
	class DebuggerBase {

		public:

			DebuggerBase();
			virtual ~DebuggerBase();

			virtual Rogue::SceneNode* getSceneNode();

			virtual void setVisible(bool v);
			virtual bool isVisible();
			virtual void clear() {};
			virtual void update() {};

		protected:

			InterfaceRenderManager* _renderManager;
			bool _isVisible;

			Rogue::SceneNode* _sceneNode;
	};

};

#endif