#ifndef __GameObject_h_
#define __GameObject_h_

#include "RogueTransform.h"
#include "RogueGameComponent.h"
#include "RogueRenderer.h"
#include "RogueRigidBody.h"

namespace Rogue {

	typedef std::list<GameObject*> GameObjectList;
	typedef std::set<GameObject*> GameObjectSet;

	class GameObject {

		public:

			GameObject(const Vector3& position = Vector3(0,0,0));
			virtual ~GameObject();

			//Adds a component to this GameObject
			template <class T>
			void addComponent(T* component) {
				addComponentCommon<T>(component);
			}

			template <>
			void addComponent<RigidBody>(RigidBody* rigidBody) {
				addComponentCommon<RigidBody>(rigidBody);
				onTransformDirty();	
				_rigidBody = rigidBody;
			}

			template <>
			void addComponent<Renderer>(Renderer* renderer) {
				addComponentCommon<Renderer>(renderer);
				renderer->update();
				_renderer = renderer;
			}

			//Gets a component of this GameObject previously added. If such component does not exists, returns NULL
			template <class T>
			T* getComponent() {
				if (_gameComponents.find(typeid(T)) != _gameComponents.end()) {
					T* component = dynamic_cast<T*>(_gameComponents[typeid(T)]);
					return component;
				}
				return NULL;
			}

			Transform& getTransform() {
				return _transform;
			}

			virtual const String& getTag() {
				return _tag;
			}

			virtual void setTag(const String& t) {
				_tag = t;
			}

			virtual int getID() {
				return _id;
			}

			virtual void setID(int id) {
				_id = id;
			}

			virtual GameObject* getParent() {
				return _parent;
			}

			virtual void setParent(GameObject* p) {
				_parent = p;
			}

			virtual GameObjectList& getChildren() {
				return _children;
			}

			virtual size_t getNumChildren() {
				return _children.size();
			}

			virtual Vector3& getScale() {
				return _scale;
			}

			virtual void setScale(Vector3& s);

			virtual GameObject* getNextSibling(GameObject* c);
			virtual GameObject* getPrevSibling(GameObject* c);
			
			virtual void addChild(GameObject* c);
			virtual void addChildBefore(GameObject* c, GameObject* next);
			virtual void destroyChildren();
			virtual void destroyGameComponents();

			virtual void onUpdateRender();		//Actions to perform at each render manager step
			virtual void onUpdatePhysics();		//Actions to perform at each physics manager step
			virtual void onTransformDirty();	//Actions done when the transform has been manually modified, e.g., update the transform of the rigid body accordingly

		protected:

			Transform _transform;
			GameObjectList _children;
			GameObject* _parent;
			String _tag;	//String identifier for application specific purposes
			int _id;		//Integer identifier for application specific purposes
			Vector3 _scale;
			
			/*** GAME COMPONENTS ***/
			GameComponentList _gameComponents;
			Renderer* _renderer;
			RigidBody* _rigidBody;

			template <class T>
			void addComponentCommon(T* component) {
				GameComponentList::iterator itFound = _gameComponents.find(typeid(T));
				if (itFound != _gameComponents.end()) delete itFound->second;
				component->setGameObject(this);
				_gameComponents[typeid(T)] = component;
			}
						
	};

};

#endif