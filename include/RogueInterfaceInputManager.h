#ifndef __RogueInterfaceInputManager_h_
#define __RogueInterfaceInputManager_h_


#include "RogueInterfaceBase.h"
#include "RogueInputListener.h"

namespace Rogue {

	class InterfaceInputManager : public InterfaceBase {

		public:

			virtual void addInputListener(InputListener* listener) = 0;
			virtual void removeInputListener(InputListener* listener) = 0;

			virtual bool isTriggeredKey(KeyCode key) = 0;
			virtual bool isPressedKey(KeyCode key) = 0;
			virtual void releaseAllKeys() = 0;

			virtual bool isTriggeredMouseButton(MouseButtonID button) = 0;
			virtual bool isPressedMouseButton(MouseButtonID button) = 0;

			virtual void getMouseCoords(int& mX, int& mY) = 0;
			virtual void getMouseDisplacement(int& dX, int& dY, int& dZ) = 0;
			virtual void getClippingAreaSize(int& w, int& h) = 0;

			virtual Mouse* getMouse() = 0;
			virtual Keyboard* getKeyboard() = 0;

			virtual void update() = 0;

	};

};

#endif