#ifndef __RogueTransform_h_
#define __RogueTransform_h_

#include "RogueTypes.h"
#include "RogueGameComponent.h"

namespace Rogue {

	class Transform : public btTransform, public GameComponent {

	public:

		virtual void setOrigin(const btVector3& origin); 

		Vector3 getForward() {
			return getBasis()[2];
		}

		Vector3 getUp() {
			return getBasis()[1];
		}

		Vector3 getRight() {
			return getBasis()[0];
		}

		Vector3 getBack() {
			return -getForward();
		}
			
		Vector3 getDown() {
			return -getUp();
		}
		
		Vector3 getLeft() {
			return -getRight();
		}

		void translate(const Vector3& disp);

	};

};

#endif