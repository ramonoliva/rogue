#ifndef __RogueGameMain_h_
#define __RogueGameMain_h_

#include "GameMapManager.h"

#include "RogueScene.h"
#include "RogueSceneGame.h"

#include "RogueCore.h"
#include "RogueGUIManager.h"
#include "RogueRenderManager.h"
#include "RogueInputManager.h"
#include "RogueCameraManager.h"
#include "RoguePhysicsManager.h"

#include "ObfuscatedZip.h"

#include <string>
#include <windows.h>
#include <cstdlib>
#include <ctime>

namespace Rogue {

	class GameApp {

		public:

			GameApp();
			virtual ~GameApp();

			virtual void go();

		protected:

			//GAME MANAGERS POINTERS
			GUIManager* _guiManager;
			RenderManager* _renderManager;
			PhysicsManager* _physicsManager;
			GameMapManager* _gameMapManager;
			ObfuscatorZip::ObfuscatedZipFactory* _obfuscator;
			InputManager* _inputManager;
			CameraManager* _cameraManager;
				
			//Pointer to the current scene
			SceneList _sceneList;

			virtual void registerGameManagers();
			virtual void registerRenderManager();
			virtual void registerInputManager();
			virtual void registerGUIManager();
			virtual void registerPhysicsManager();
			virtual void registerCameraManager();
			virtual void registerObfuscatorManager();
			virtual void registerGameMapManager();

			virtual Scene* loadScene(int sceneID);
			virtual void registerScenes() = 0;
	}; 

};

#endif
