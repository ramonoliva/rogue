#ifndef __RogueRigidBody_h_
#define __RogueRigidBody_h_

#include <BtOgrePG.h>
#include <BtOgreGP.h>
#include "RogueGameComponent.h"

namespace Rogue {

	class BaseCharacter;

	class RigidBody : public btRigidBody, public GameComponent {

		public:

			RigidBody(float mass, MotionState* state, CollisionShape* shape, const Vector3& localInertia = Vector3(0,0,0));
			virtual ~RigidBody();

			virtual void setKinematic(bool k);
			virtual void setScale(Vector3& scale);

			virtual void setGhostObject(GhostObject* g);
			virtual GhostObject* getGhostObject();

			//TODO: This is just a patch for the paper
			virtual void setCharacter(BaseCharacter* character);
			virtual BaseCharacter* getCharacter();
			virtual void onThisIsJustAPatch();

			virtual void update();

		protected:
			
			//TODO: This is just a patch for the paper
			BaseCharacter* _character;

			GhostObject* _ghostObject;
		
	};

};

#endif