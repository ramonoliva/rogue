#include "GameMapManager.h"

using namespace Rogue;

/*******************************************************************************************/
/* SINGLETON TREATMENT */
/*******************************************************************************************/
template<> GameMapManager *Ogre::Singleton<GameMapManager>::msSingleton = new GameMapManager();

/***************************************************************************************/
/* CREACIO I DESTRUCCIO */
/***************************************************************************************/
GameMapManager *GameMapManager::getSingletonPtr() {
     return msSingleton;
}
 
GameMapManager &GameMapManager::getSingleton() {  
	assert(msSingleton);  
    return(*msSingleton);
}
 
GameMapManager::GameMapManager() {}

void GameMapManager::initialize() {
	mResourceType = "GameMap";
 
    // low, because it will likely reference other resources
    mLoadOrder = 30.0f;
 
	// this is how we register the ResourceManager with OGRE
    Ogre::ResourceGroupManager::getSingleton()._registerResourceManager(mResourceType, this);
}
 
GameMapManager::~GameMapManager() {
    // and this is how we unregister it
	/*ResourceMapIterator it = getResourceIterator();
	GameMapPtr gMap;
	while (it.hasMoreElements()) {
		gMap = it.getNext();
		gMap->unload();
	}
	_gameMapList.clear();*/
    Ogre::ResourceGroupManager::getSingleton()._unregisterResourceManager(mResourceType);
}
 
Ogre::Resource *GameMapManager::createImpl(const Ogre::String &name, Ogre::ResourceHandle handle, 
                                           const Ogre::String &group, bool isManual, Ogre::ManualResourceLoader *loader, 
                                           const Ogre::NameValuePairList *createParams) {
    return new GameMap(this, name, handle, group, isManual, loader);
}

void GameMapManager::setupResources() {
	//Add the locations where the .map files are expected to be
	Ogre::ResourceGroupManager& resourceGroupManager = Ogre::ResourceGroupManager::getSingleton();
	resourceGroupManager.addResourceLocation("../../media/maps", "FileSystem", "Scenes");

	//Find all the map resources on that locations and declare them
	Ogre::ResourceGroupManager::LocationList locationList = resourceGroupManager.getResourceLocationList("Scenes");
	for (Ogre::ResourceGroupManager::LocationList::iterator it = locationList.begin(); it != locationList.end(); it++) {
		Ogre::Archive* archive = (*it)->archive;
		
		Ogre::StringVectorPtr files = archive->find("*.map");
		for (Ogre::StringVector::iterator itFile = files->begin(); itFile != files->end(); itFile++) {
			_gameMapList.push_back(*itFile);
			resourceGroupManager.declareResource(*itFile, mResourceType);
		}
		
	}
	
	//Initialize all the the resources
	resourceGroupManager.initialiseAllResourceGroups();
}

/***************************************************************************************/
/* GET I SET */
/***************************************************************************************/
const Ogre::StringVector& GameMapManager::getGameMapList() {
	return _gameMapList;
}

size_t GameMapManager::getNumResources() {
	return mResources.size();
}