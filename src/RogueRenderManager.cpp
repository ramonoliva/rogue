#include "RogueRenderManager.h"
#include "ngRealMath.h"
#include <OgreWindowEventUtilities.h>
#include "RogueGlobalVars.h"

using namespace Rogue;

static const float UPDATE_FREQUENCY = 0.0f;	//Update frequency in seconds i.e., an update occurs every UPDATE_FREQUENCY seconds

static const String MAIN_SCENE_MANAGER_NAME = "SceneManager";
static const String DEFERRED_SCENE_MANAGER_NAME = "DeferredSceneManager";

static const String MAIN_CAMERA_NAME = "MainCamera";
static const String DEFERRED_CAMERA_NAME = "DeferredCamera";

static const Color BACKGROUND_COLOR = Color(0.464f,0.796f,1.0f);

String Rogue::G_DEFERRED_CHAIN_MAIN = "__MainDeferredChain__";

#pragma region CREATION AND DESTRUCTION

RenderManager::RenderManager(const String& windowName, SceneType sceneType) {
#ifdef _DEBUG
    //_resourcesCFG = "resources_d.cfg";
    _pluginsCFG = "plugins_d.cfg";
#else
    //_resourcesCFG = "resources.cfg";
    _pluginsCFG = "plugins.cfg";
#endif
	
	_ogreRoot = new Ogre::Root(_pluginsCFG);

	restoreConfig();
	createRenderWindow(true, windowName);

	//Create the Scene Managers
	_sceneMgr = createSceneManager(sceneType, MAIN_SCENE_MANAGER_NAME);
	_deferredSceneMgr = createSceneManager(Ogre::ST_GENERIC, DEFERRED_SCENE_MANAGER_NAME);

	//Create the cameras
	_mainCamera = createCamera(MAIN_CAMERA_NAME, Ogre::PT_PERSPECTIVE, 0.1f, 1000.0f, _sceneMgr);
	_deferredCamera = createCamera(DEFERRED_CAMERA_NAME, Ogre::PT_ORTHOGRAPHIC, 1.0f, 10.0f, _deferredSceneMgr);

	_activeDeferredChain = NULL;

	//Creates the overlay system
	createOverlaySystem();

	//Setup and initialize its resources
	setupResourcesDebug();

	_timer.reset();
	_timeLastUpdate = _timer.getMilliseconds();
	_renderSystem = _ogreRoot->getRenderSystem();
	_texManager = Ogre::TextureManager::getSingletonPtr();

	_renderQuad = NULL;
	setRenderMode(RENDER_MODE_FORWARD);
}

RenderManager::~RenderManager() {
	for (DeferredChainList::iterator it = _deferredChainList.begin(); it != _deferredChainList.end(); it++) {
		delete it->second;
	}
	delete _ogreRoot;
}

SceneManager* RenderManager::createSceneManager(SceneType type, const String& name) {
	//Create the main Scene Manager
	SceneManager* sceneManager = _ogreRoot->createSceneManager(type, name);
	return sceneManager;
}

void RenderManager::createOverlaySystem() {
	_overlaySystem = new Ogre::OverlaySystem();
}

void RenderManager::createRenderWindow(bool autoCreateWindow, const String& windowName) {
	_renderWindow = _ogreRoot->initialise(true, windowName);
	if (_renderWindow->getWidth() < 1024) {
		if (_renderWindow->isFullScreen()) {
			_renderWindow->setFullscreen(true, 1024, 768);
		}
		else {
			_renderWindow->resize(1024, 768);
			_renderWindow->reposition(0,0);
		}
	}
}

Camera* RenderManager::createCamera(const String& cameraName, CameraType type, float zNear, float zFar, SceneManager* sceneManager) {
	
	if (sceneManager == NULL) sceneManager = _sceneMgr;
    
	Camera* cam = sceneManager->createCamera(cameraName);
	cam->setProjectionType(type);
	cam->setNearClipDistance(zNear);
	cam->setFarClipDistance(zFar);

    return cam;
}

SceneNode* RenderManager::createSceneNode(SceneNode* parent) {
	if (parent == NULL) parent = _sceneMgr->getRootSceneNode();
	SceneNode* node = parent->createChildSceneNode();
	_sceneNodes.insert(node);
	return node;
}

void RenderManager::destroySceneNode(SceneNode* node) {
   if (node == NULL) return;
   _sceneNodes.erase(node);
   destroyAllAttachedMovableObjects(node);
   node->removeAndDestroyAllChildren();
   node->getCreator()->destroySceneNode(node);
}

void RenderManager::destroyAllAttachedMovableObjects(SceneNode* node) {
	if (node == NULL) return;
		
	//Destroy all the attached objects
	Ogre::SceneNode::ObjectIterator itObject = node->getAttachedObjectIterator();
	while (itObject.hasMoreElements()) node->getCreator()->destroyMovableObject(itObject.getNext());

	//Recurse to child SceneNodes
	Ogre::SceneNode::ChildNodeIterator itChild = node->getChildIterator();
	while (itChild.hasMoreElements()) {
		Ogre::SceneNode* pChildNode = static_cast<Ogre::SceneNode*>(itChild.getNext());
		destroyAllAttachedMovableObjects( pChildNode );
	}
}
			
Entity* RenderManager::createEntity(const String& meshFile) {
	return _sceneMgr->createEntity(meshFile);
}

ManualObject* RenderManager::createManualObject() {
	return _sceneMgr->createManualObject();
}

RaySceneQuery* RenderManager::createRaySceneQuery() {
	return _sceneMgr->createRayQuery(Ogre::Ray());
}

void RenderManager::destroyRaySceneQuery(RaySceneQuery* q) {
	_sceneMgr->destroyQuery(q);
}

Light* RenderManager::createLight(const String& name) {
	return _sceneMgr->createLight(name);
}

void RenderManager::setupResourcesDebug() {
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media", "FileSystem", "General");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/thumbnails", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/packs/SdkTrays.zip", "Zip", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/fonts", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/materials/programs", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/materials/programs/CG", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/materials/scripts", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/materials/textures", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/materials/textures/skybox", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/materials/textures/GUIElements", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/music", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/maps", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/overlays", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/particle", "FileSystem", "Essential");

	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/ninja", "FileSystem", "Essential");

	//TILESET COMMON
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/tilesetCommon/arbol00", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/tilesetCommon/arbol00ramas", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/tilesetCommon/arbol01", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/tilesetCommon/barril", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/tilesetCommon/carro", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/tilesetCommon/cubo", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/tilesetCommon/flagBlueMarker", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/tilesetCommon/flagRedMarker", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/tilesetCommon/jarron", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/tilesetCommon/lampara", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/tilesetCommon/portalMadera", "FileSystem", "Essential");

	//TILESET CASTILLO
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/tilesetCastillo/casaCastillo00", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/tilesetCastillo/casaCastillo01", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/tilesetCastillo/diana", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/tilesetCastillo/portalCastillo", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/tilesetCastillo/portalCastilloOP", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/tilesetCastillo/tiendaCampana", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/tilesetCastillo/torreVigia", "FileSystem", "Essential");

	//MAP00
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/maps/map00/bounds", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/maps/map00/floor00", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/maps/map00/floor01", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/maps/map00/wall00", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/models/maps/map00/wall01", "FileSystem", "Essential");

	//LOAD THE RESOURCES INTRODUCED
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

	// Set default mipmap level (NB some APIs ignore this)
    Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
}

void RenderManager::setupResourcesRelease() {
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media", "FileSystem", "General");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/thumbnails", "FileSystem", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/music", "FileSystem", "Essential");

	//Obfuscated files
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/packs/SdkTrays.flag", "flag", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/packs/fonts.flag", "flag", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/packs/GameGUI.flag", "flag", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/packs/GUIElements.flag", "flag", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/packs/maps.flag", "flag", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/packs/materialPrograms.flag", "flag", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/packs/materialScripts.flag", "flag", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/packs/materialTextures.flag", "flag", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/packs/models.flag", "flag", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/packs/particle.flag", "flag", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/packs/skybox.flag", "flag", "Essential");

	//MODELS
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/packs/modelMap00.flag", "flag", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/packs/modelNinja.flag", "flag", "Essential");

	//TILESETS
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/packs/tilesetCommon.flag", "flag", "Essential");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../../media/packs/tilesetCastillo.flag", "flag", "Essential");
}

DeferredChain* RenderManager::createDeferredChain(const String chainName) {
	DeferredChain* dc = new DeferredChain();
	_deferredChainList[chainName] = dc;
	if (_activeDeferredChain == NULL) _activeDeferredChain = dc;	//Is the first created deferred chain
	return dc;
}

void RenderManager::setDeferredChainActive(const String chainName) {
	DeferredChainList::iterator it = _deferredChainList.find(chainName);
	if (it != _deferredChainList.end()) {
		_activeDeferredChain = it->second;
	}
}

/*void GameApp::setupOgreResources() {
	// Load resource paths from config file
    Ogre::ConfigFile cf;
    cf.load(_resourcesCFG);

    // Go through all sections & settings in the file
    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

    String secName, typeName, archName;
    while (seci.hasMoreElements()) {
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i) {
            typeName = i->first;
            archName = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
                archName, typeName, secName);
        }
    }
}*/

#pragma endregion

/*******************************************************************************************/
/* CONFIGURATION */
/*******************************************************************************************/
bool RenderManager::showConfigDialog() {
	return _ogreRoot->showConfigDialog();
}

bool RenderManager::restoreConfig() {
	return _ogreRoot->restoreConfig();
}

#pragma region GET AND SET

RenderWindow* RenderManager::getRenderWindow() {
	return _renderWindow;
}

void RenderManager::getRenderWindowSize(size_t& w, size_t& h) {
	w = _renderWindow->getWidth();
	h = _renderWindow->getHeight();
}

Camera* RenderManager::getCamera(const String& cameraName) {
	return _sceneMgr->getCamera(cameraName);
}

Camera* RenderManager::getMainCamera() {
	return _mainCamera;
}

Camera* RenderManager::getDeferredCamera() {
	return _deferredCamera;
}

SceneManager* RenderManager::getSceneManager() {
	return _sceneMgr;
}

SceneNode* RenderManager::getRootSceneNode() {
	return _sceneMgr->getRootSceneNode();
}

void RenderManager::setParentSceneNode(SceneNode* node, SceneNode* parent) {
	if (!parent) return;

	Ogre::Node* oldParent = node->getParent();
	if (oldParent) oldParent->removeChild(node);

	parent->addChild(node);
}

void RenderManager::setRenderTargetActive(RenderTarget* renderTarget, bool active) {
	renderTarget->setActive(active);
	if (active) _rtActiveList.insert(renderTarget);
	else _rtActiveList.erase(renderTarget);
}

void RenderManager::setAmbientLightColor(const Color& color) {
	_sceneMgr->setAmbientLight(color);	
}

#pragma endregion

/*******************************************************************************************/
/* UPDATE */
/*******************************************************************************************/
void RenderManager::update() {
	unsigned long now = _timer.getMilliseconds();
	float diffTime = float(now - _timeLastUpdate) / 1000.0f;
	if (RealMath::greaterOrEqual(diffTime, UPDATE_FREQUENCY)) {
		Ogre::WindowEventUtilities::messagePump();
		renderOneFrame();
		_timeLastUpdate = now;
	}
}

void RenderManager::renderOneFrame() {
	if (_renderMode == RENDER_MODE_FORWARD) renderOneFrameForward();
	else renderOneFrameDeferred();
}

void RenderManager::renderOneFrameForward() {
	_ogreRoot->renderOneFrame();
}

void RenderManager::renderOneFrameDeferred() {
	//clearGBuffer();

	if (_activeDeferredChain == NULL) return;

	//Execute all the deferred passes of the active deferred chain
	DeferredPassList& deferredPasses = _activeDeferredChain->getDeferredPasses();	
	DeferredPass* pass;
	for (DeferredPassList::iterator it = deferredPasses.begin(); it != deferredPasses.end(); it++) {
		pass = *it;

		//1) The material has to be applied to the full screen render quad or at scene meshes?
		const String matName = pass->getMaterialName();
		if (matName != "") {
			if (pass->getRenderQuadVisible()) {
				_renderQuad->setMaterial(pass->getMaterialName());
			}
			else {
				setMaterialModels(pass->getMaterialName(), _sceneNodes);
			}
		}
		
		//2) Render the scene
		pass->preRender();
		_ogreRoot->renderOneFrame();
		pass->postRender();
	}
}

void RenderManager::clearGBuffer() {
	//Clears the information of the scene GBuffer
	/*Ogre::TexturePtr tex;
	tex = _texManager->getByName(MRT_0);
	clearViewport(tex->getBuffer()->getRenderTarget()->getViewport(0));
	tex = _texManager->getByName(MRT_1);
	clearViewport(tex->getBuffer()->getRenderTarget()->getViewport(0));
	tex = _texManager->getByName(MRT_2);
	clearViewport(tex->getBuffer()->getRenderTarget()->getViewport(0));*/
}

void RenderManager::setMaterialModels(const Ogre::String& matName, SceneNodeList& mList) {
	SceneNode* node;
	MovableObject* mObject;
	Entity* ent;
	ManualObject* man;
	for (SceneNodeList::iterator itModel = mList.begin(); itModel != mList.end(); itModel++) {
		node = *itModel;
		Ogre::SceneNode::ObjectIterator itObject = node->getAttachedObjectIterator();
		while (itObject.hasMoreElements()) {
			mObject = itObject.getNext();
			if (mObject->getMovableType() == "Entity") {
				ent = static_cast<Entity*>(mObject);
				ent->setMaterialName(matName);
			}
			else if (mObject->getMovableType() == "ManualObject") {
				man = static_cast<ManualObject*>(mObject);
				for (size_t i = 0; i < man->getNumSections(); i++) {
					man->setMaterialName(i, matName);
				}
			}
		}
	}
}

void RenderManager::showModels(bool show) {
	for (SceneNodeList::iterator itModel = _sceneNodes.begin(); itModel != _sceneNodes.end(); itModel++) {
		(*itModel)->setVisible(show);
	}
}

void RenderManager::computeProjectionMatrix(Camera* cam, Ogre::Matrix4& projectionMatrix, bool requiresTextureFlipping) {
	projectionMatrix = cam->getProjectionMatrixWithRSDepth();
	if (requiresTextureFlipping) {
		projectionMatrix[1][0] = -projectionMatrix[1][0];
		projectionMatrix[1][1] = -projectionMatrix[1][1];
		projectionMatrix[1][2] = -projectionMatrix[1][2];
		projectionMatrix[1][3] = -projectionMatrix[1][3];
	}
}

/*******************************************************************************************/
/* CLASS FUNCTIONS */
/*******************************************************************************************/
void RenderManager::addFrameListener(FrameListener* listener) {
	_ogreRoot->addFrameListener(listener);
}

void RenderManager::removeFrameListener(FrameListener* listener) {
	_ogreRoot->removeFrameListener(listener);
}

void RenderManager::addWindowEventListener(WindowEventListener* listener) {
	Ogre::WindowEventUtilities::addWindowEventListener(_renderWindow, listener);
}

void RenderManager::removeWindowEventListener(WindowEventListener* listener) {
	Ogre::WindowEventUtilities::removeWindowEventListener(_renderWindow, listener);
}

void RenderManager::setRenderMode(RenderMode rMode) {
	_renderMode = rMode;

	Camera* finalCamera;	//The camera used to show the final composition of the scene

	if (_renderMode == RENDER_MODE_FORWARD) {
		//Deactivate the Deferred Rendering Parameters if needed
		finalCamera = _mainCamera;
		_deferredSceneMgr->removeRenderQueueListener(_overlaySystem);
		_sceneMgr->addRenderQueueListener(_overlaySystem);
	}
	else if (_renderMode == RENDER_MODE_DEFERRED) {
		//Create the Deferred Rendering Parameters if needed
		if (_renderQuad == NULL) {
			_renderQuad = createRenderQuad(-1.0f, 1.0f, 1.0f, -1.0f);
		}
		finalCamera = _deferredCamera;
		_sceneMgr->removeRenderQueueListener(_overlaySystem);
		_deferredSceneMgr->addRenderQueueListener(_overlaySystem);
	}

	//Remove the previously created viewports in the render window
	_renderWindow->removeAllViewports();

	//Setup the viewport of the render window
    Viewport* vp = _renderWindow->addViewport(finalCamera); 
    vp->setBackgroundColour(BACKGROUND_COLOR); 

    //The aspect ratio of the camera and the viewport should be the same
    finalCamera->setAspectRatio(float(vp->getActualWidth()) / float(vp->getActualHeight()));
}

RenderMode RenderManager::getRenderMode() {
	return _renderMode;
}

void RenderManager::clearViewport(Viewport* vp) {
	_renderSystem->_setViewport(vp);
	_renderSystem->clearFrameBuffer(Ogre::FBT_COLOUR | Ogre::FBT_DEPTH | Ogre::FBT_STENCIL, vp->getBackgroundColour());
}

void RenderManager::clearViewport(Viewport* vp, const Color& bgColor) {
	_renderSystem->_setViewport(vp);
	_renderSystem->clearFrameBuffer(Ogre::FBT_COLOUR | Ogre::FBT_DEPTH | Ogre::FBT_STENCIL, bgColor);
}

Rectangle2D* RenderManager::createRenderQuad(float left, float top, float right, float bottom) {
	Rectangle2D* quad = new Rectangle2D(true);	//Indiquem que volem generar coordenades de textura
	quad->setCorners(left, top, right, bottom);	//Definim les cantonades del quad
	//A continuaci�, assignem una bounding box enorme al quad. Aix� es fa per evitar que el quad sigui 
	//descartat quan no estem mirant el node que cont� el quad. 
	quad->setBoundingBox(Ogre::AxisAlignedBox(-100000.0f * Ogre::Vector3::UNIT_SCALE, 100000.0f * Ogre::Vector3::UNIT_SCALE));
	SceneNode* quadNode = _deferredSceneMgr->getRootSceneNode()->createChildSceneNode();
	quadNode->attachObject(quad);
	return quad;
}

TexturePtr RenderManager::createTexture(const String& texName, size_t w, size_t h, PixelFormat format, int usage) {
	//Creates a texture on the fly
	TexturePtr tex = _texManager->getByName(texName);
	if (!tex.isNull()) {
		//Texture with name texName already exists. Destroy it and create it again
		tex->unload();
		_texManager->unload(texName);
		_texManager->remove(texName);
	}
	tex = _texManager->createManual(
									texName,
									Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, 
									Ogre::TEX_TYPE_2D, 
									w, 
									h, 
									0, 
									format,
									usage);
	tex->load();
	return tex;
}

TexturePtr RenderManager::createTexture1D(const String& texName, size_t w, PixelFormat format, int usage) {
	//Creates a texture on the fly
	TexturePtr tex = _texManager->getByName(texName);
	if (!tex.isNull()) {
		//Texture with name texName already exists. Destroy it and create it again
		tex->unload();
		_texManager->unload(texName);
		_texManager->remove(texName);
	}
	tex = _texManager->createManual(
									texName,
									Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, 
									Ogre::TEX_TYPE_1D, 
									w, 
									1, 
									1, 
									0, 
									format,
									usage);
	tex->load();
	return tex;
}

TexturePtr RenderManager::getTexture(const String& texName) {
	return _texManager->getByName(texName);
}

RenderTexture* RenderManager::createRenderTexture(const String& texName, size_t w, size_t h, PixelFormat format, const Color& bgColor, bool clearEveryFrame, Camera* cam) {
	TexturePtr tex = createTexture(texName, w, h, format, Ogre::TU_RENDERTARGET);
	RenderTexture* renderTex = tex->getBuffer()->getRenderTarget();
	if (cam == NULL) cam = _mainCamera;
	if (renderTex->getNumViewports() == 0) {
		renderTex->addViewport(cam);				//Viewport el contingut del qual s'utilitzar� per emplenar la textura
		renderTex->addListener(this);
	}
	Viewport* vp = renderTex->getViewport(0);
	vp->setClearEveryFrame(clearEveryFrame);		//El viewport es neteja a cada frame
	vp->setBackgroundColour(bgColor);
	vp->setOverlaysEnabled(false);					//Desactivem els overlays en la textura
	clearViewport(vp, bgColor);
	renderTex->setActive(false);
	return renderTex;
}

RenderTexture* RenderManager::getRenderTexture(const String& texName) {
	TexturePtr tex = getTexture(texName);
	if (tex.isNull()) return NULL;

	return tex->getBuffer()->getRenderTarget();
}

MultiRenderTarget* RenderManager::createMultiRenderTarget(const String& mrtName, TexList& tList, size_t w, size_t h, PixelFormat pFormat, const Color& bgColor, bool clearEveryFrame, Camera* cam) {
	//Crea un MultiRenderTarget
	//0) Primer el destrueixo i el torno a crear, per si ja existia abans un MRT amb el mateix nom
	_renderSystem->destroyRenderTarget(mrtName);
	MultiRenderTarget* multiRT = _renderSystem->createMultiRenderTarget(mrtName);

	//1) Creo les textures que formen el MRT i les vinculo a aquest
	RenderTexture* renderTex;
	std::vector<RenderTexture*> surfaces;
	size_t i = 0;
	if (cam == NULL) cam = _mainCamera;
	for (TexList::iterator it = tList.begin(); it != tList.end(); it++) {
		renderTex = createRenderTexture(*it, w, h, pFormat, bgColor, clearEveryFrame, cam);
		renderTex->setAutoUpdated(false);
		multiRT->bindSurface(i, renderTex);
		i++;
	}
	
	//2) Acabo de definir les propietats del MRT
	multiRT->setAutoUpdated(true);
	Viewport* vp = multiRT->addViewport(cam);
	vp->setClearEveryFrame(clearEveryFrame);
	vp->setOverlaysEnabled(false);
	vp->setSkiesEnabled(false);
	vp->setBackgroundColour(bgColor);
	multiRT->addListener(this);
	multiRT->setActive(false);

	return multiRT;
}

void RenderManager::saveTextureToDisk(const String& texName, const String& fileName) {
	//Create an image with the texture information
	TexturePtr texPtr = _texManager->getByName(texName);
	Image imgSrc;
	texPtr->convertToImage(imgSrc);
	
	//Create the destination image
	Image imgDest(imgSrc);
	imgDest.load("baseWhite.png", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	imgDest.resize(imgSrc.getWidth(), imgSrc.getHeight());
	Color colourSrc;
	for (size_t j = 0; j < imgSrc.getHeight(); j++) {
		for (size_t i = 0; i < imgSrc.getWidth(); i++) {
			colourSrc = imgSrc.getColourAt(i, j, 0);
			colourSrc.saturate();
			imgDest.setColourAt(colourSrc, i, j, 0);
		}
	}

	imgDest.save(fileName);
}

void RenderManager::takeScreenShot(const String& picName, const String& picExtension) {
	_renderWindow->writeContentsToTimestampedFile(picName, picExtension);
}

/******************************************************************************/
/* RTTLISTENER FUNCTIONS */
/******************************************************************************/
void RenderManager::preRenderTargetUpdate(const Ogre::RenderTargetEvent& evt) {
	/*if (renderQuad == NULL) return;
	Ogre::LogManager::getSingletonPtr()->logMessage("PRE RENDER TARGET UPDATE!");
	renderQuad->setVisible(showRenderQuadPre);*/
}

void RenderManager::postRenderTargetUpdate(const Ogre::RenderTargetEvent& evt) {
	/*if (renderQuad == NULL) return;
	renderQuad->setVisible(showRenderQuadPost);*/
}