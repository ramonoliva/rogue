#include "GameMapPtr.h"

using namespace Rogue;

/*********************************************************************************************/
/* CREACIO I DESTRUCCIO */
/*********************************************************************************************/

GameMapPtr::GameMapPtr() : Ogre::SharedPtr<GameMap>() {}
	
GameMapPtr::GameMapPtr(GameMap *rep) : Ogre::SharedPtr<GameMap>(rep) {}

GameMapPtr::GameMapPtr(const GameMapPtr &r) : Ogre::SharedPtr<GameMap>(r) {} 

GameMapPtr::GameMapPtr(const Ogre::ResourcePtr &r) : Ogre::SharedPtr<GameMap>() {
	if (r.isNull()) return;
	lockAndCopyMutexPointer(r);
	Ogre::MaterialPtr;
}

void GameMapPtr::lockAndCopyMutexPointer(const Ogre::ResourcePtr &r) {
	// lock & copy other mutex pointer
	OGRE_LOCK_MUTEX(*r.OGRE_AUTO_MUTEX_NAME)
	OGRE_COPY_AUTO_SHARED_MUTEX(r.OGRE_AUTO_MUTEX_NAME)
	pRep = static_cast<GameMap*>(r.getPointer());
	pUseCount = r.useCountPointer();
	useFreeMethod = r.freeMethod();
	if (pUseCount) {
		++(*pUseCount);
	}
}

/*********************************************************************************************/
/* OPERATORS */
/*********************************************************************************************/

/// Operator used to convert a ResourcePtr to a GameMapPtr
GameMapPtr& GameMapPtr::operator=(const Ogre::ResourcePtr& r) {
	if(pRep == static_cast<GameMap*>(r.getPointer())) return *this;
	release();
	if(r.isNull()) return *this; // resource ptr is null, so the call to release above has done all we need to do.
	lockAndCopyMutexPointer(r);
	return *this;
}