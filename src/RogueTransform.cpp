#include "RogueTransform.h"
#include "RogueGameObject.h"

using namespace Rogue;

void Transform::setOrigin(const btVector3& origin) {
	btTransform::setOrigin(origin);
	if (_gameObject) _gameObject->onTransformDirty();
}

void Transform::translate(const Vector3& disp) {
	const Vector3& origin = getOrigin();
	setOrigin(origin + disp);
}