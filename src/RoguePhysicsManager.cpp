#include "RoguePhysicsManager.h"
#include "RogueScene.h"
#include "RogueBaseCharacter.h"

using namespace Rogue;
using namespace NEOGEN;
using namespace GeometrySolver;

static const float SIMULATION_FREQUENCY = 1.0f / 60.0f;	//Frequency at which the Physics Simulation occurs (by default, it does 60 updates per second)
static const int MAX_SUBSTEPS = 7;						//When the current timeStep is > than SIMULATION_FREQUENCY, current timeStep is divided into a maximum MAX_SUBSTEPS steps

static void physicsTickCallback(btDynamicsWorld *world, float timeStep) {
	PhysicsManager* pManager = static_cast<PhysicsManager*>(world->getWorldUserInfo());
	pManager->updatePhysics();
	pManager->dispatchCollisions();
}

/**********************************************************************/
/* CREATION AND DESTRUCTION	*/
/**********************************************************************/
PhysicsManager::PhysicsManager() {
	_broadphase = new btDbvtBroadphase();
    _collisionConfig = new btDefaultCollisionConfiguration();
    _dispatcher = new btCollisionDispatcher(_collisionConfig);
    _constraintSolver = new btSequentialImpulseConstraintSolver();

	_world = new btDiscreteDynamicsWorld(_dispatcher, _broadphase, _constraintSolver, _collisionConfig);
	Vector3 gravity(0.0f, -9.8f, 0.0f);
    _world->setGravity(gravity);
	//_world->setInternalTickCallback(myTickCallback);
	_world->setInternalTickCallback(physicsTickCallback, static_cast<void*>(this));
	_broadphase->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());	//This is necessary in order to make ghost objects work
	
	//Initialize the timer
	_timer.reset();
	_timeLastUpdate = _timer.getMilliseconds();
	_timePrevStep = _timer.getMilliseconds();
	_timeCurrStep = _timer.getMilliseconds();
}

PhysicsManager::~PhysicsManager() {
	clearWorld();
	delete _world;
	delete _constraintSolver;
	delete _dispatcher;
	delete _collisionConfig;
	delete _broadphase;
}

/**********************************************************************/
/* PHYSIC BODIES MANAGEMENT */
/**********************************************************************/
RigidBody* PhysicsManager::createRigidBody(CollisionShape* shape, float mass, short collisionFilterGroup, short collisionFilterMask) {
	return createRigidBody(shape, mass, Vector3(0,0,0), collisionFilterGroup, collisionFilterMask);
}

RigidBody* PhysicsManager::createRigidBody(CollisionShape* shape, float mass, const Vector3& position, short collisionFilterGroup, short collisionFilterMask) {
	//Create the motion state
	MotionState* motionState = new DefaultMotionState();
	Transform xform;
	xform.setIdentity();
	xform.setOrigin(position);
	motionState->setWorldTransform(xform);

	//Compute the inertia
	Vector3 inertia;
	if (mass == 0.0f) inertia = Vector3(0,0,0);
	else shape->calculateLocalInertia(mass, inertia);

	//Create the rigid body and add it to the physics world
	RigidBody* body = new RigidBody(mass, motionState, shape, inertia);
	if (mass == 0.0f) _world->addCollisionObject(body, collisionFilterGroup, collisionFilterMask);
	else _world->addRigidBody(body, collisionFilterGroup, collisionFilterMask);
	body->setCollisionFlags(body->getCollisionFlags() | CollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);	//Allow the rigid body to receive custom callbacks
	_collisionObjectList.insert(body);

	return body;
}

void PhysicsManager::removeRigidBody(RigidBody* body) {
	if (_collisionObjectList.find(body) != _collisionObjectList.end()) {
		_world->removeRigidBody(body);
		_collisionObjectList.erase(body);
	}
}

void PhysicsManager::clearWorld() {
	for (CollisionObjectList::iterator it = _collisionObjectList.begin(); it != _collisionObjectList.end(); it++) {
		_world->removeCollisionObject(*it);
	}
	_collisionObjectList.clear();
}

GhostObject* PhysicsManager::createGhostObject(CollisionShape* shape, short collisionFilterGroup, short collisionFilterMask) {
	GhostObject* ghost = new GhostObject();
	ghost->setCollisionShape(shape);
	ghost->setCollisionFlags(btCollisionObject::CF_NO_CONTACT_RESPONSE);
	_world->addCollisionObject(ghost, collisionFilterGroup, collisionFilterMask);
	return ghost;
}

/**********************************************************************/
/* GET AND SET */
/**********************************************************************/
PhysicsWorld* PhysicsManager::getPhysicsWorld() {
	return _world;
}

bool PhysicsManager::addCollisionPair(CollisionPair& pair) {
	CollisionPair key = getCollisionPairKey(pair);
	if (_collidingBodies.find(key) == _collidingBodies.end()) {
		//The two colliding bodies where not colliding in the previous frame
		_collidingBodies.insert(key);
		onCollisionPairAdded(key);
		return true;
	}
	onCollisionPairStay(key);
	return false;
}

bool PhysicsManager::removeCollisionPair(CollisionPair& pair) {
	CollisionPair key = getCollisionPairKey(pair);
	if (_collidingBodies.find(key) != _collidingBodies.end()) {
		//Exists a colliding pair with such bodies, so remove it
		_collidingBodies.erase(key);
		onCollisionPairRemoved(key);
		return true;
	}
	return false;
}

CollisionPair PhysicsManager::getCollisionPairKey(CollisionPair& pair) {
	//Sort the pair by pointer order
	CollisionPair key;
	if (pair.first < pair.second) key = pair;
	else key = CollisionPair(pair.second, pair.first);

	return key;
}

/**********************************************************************/
/* ACTUALITZACI�	*/
/**********************************************************************/
void PhysicsManager::update() {
	// Tick the bullet world. Keep in mind that bullet takes seconds, and we measure time in milliseconds.
	_timeCurrStep = _timer.getMilliseconds();
	unsigned long diffTime = _timeCurrStep - _timePrevStep;
	float timeStep = float(diffTime) / 1000.0f;		//Remember: Bullet Physics Simulation works in seconds!!!
	_world->stepSimulation(timeStep, MAX_SUBSTEPS, SIMULATION_FREQUENCY);
	_timePrevStep = _timeCurrStep;						

	/*//Update the physics part of the game objects if necessary
	if (RealMath::greaterOrEqual(float(_timeCurrStep - _timeLastUpdate) / 1000.0f, SIMULATION_FREQUENCY)) {
		//TODO: this is a patch for the paper
		for (CollisionObjectList::iterator it = _collisionObjectList.begin(); it != _collisionObjectList.end(); it++) {
			RigidBody* body = dynamic_cast<RigidBody*>(*it);
			body->onThisIsJustAPatch();
		}
		
		GameObjectList& gameObjects = Scene::getGameObjects();

		for (GameObjectList::iterator it = gameObjects.begin(); it != gameObjects.end(); it++) {
			(*it)->onUpdatePhysics();
		}

		_timeLastUpdate = _timeCurrStep;
	}*/
}

void PhysicsManager::updatePhysics() {
	for (CollisionObjectList::iterator it = _collisionObjectList.begin(); it != _collisionObjectList.end(); it++) {
		RigidBody* body = dynamic_cast<RigidBody*>(*it);
		body->onThisIsJustAPatch();
	}
		
	GameObjectSet& gameObjects = Scene::getGameObjects();

	for (GameObjectSet::iterator it = gameObjects.begin(); it != gameObjects.end(); it++) {
		(*it)->onUpdatePhysics();
	}
}

void PhysicsManager::dispatchCollisions() {
	CollisionPairSet oldCollidingBodies = _collidingBodies;	//Save the current colliding bodies
	int numManifolds = _world->getDispatcher()->getNumManifolds();
	for (int i = 0; i < numManifolds; i++) {
		btPersistentManifold* contactManifold =  _world->getDispatcher()->getManifoldByIndexInternal(i);
		CollisionObject* obA = const_cast<btCollisionObject*>(contactManifold->getBody0());
		CollisionObject* obB = const_cast<btCollisionObject*>(contactManifold->getBody1());

		int numContacts = contactManifold->getNumContacts();
		int j = 0;
		bool contact = false;
		while (!contact && (j < numContacts)) {
			btManifoldPoint& pt = contactManifold->getContactPoint(j);
			contact = (pt.getDistance() < 0.01f);
			j++;
		}
		if (contact) {
			CollisionPair key = getCollisionPairKey(CollisionPair(obA, obB));
			addCollisionPair(key);
			oldCollidingBodies.erase(key);
		}

		/*
		GameObject* goCell = rBodyA->getGameObject()? rBodyA->getGameObject() : rBodyB->getGameObject();
		BaseCharacter* character = rBodyA->getCharacter()? rBodyA->getCharacter() : rBodyB->getCharacter();

		if (goCell && (goCell->getTag() == "NavMeshCell") && character) {
			character->addOverlappingCell(goCell->getID());
		}
		*/
	}

	//Remove all the colliding pairs that does not have a collision on this frame, i.e., the bodies are not colliding anymore
	for (CollisionPairSet::iterator it = oldCollidingBodies.begin(); it != oldCollidingBodies.end(); it++) {
		removeCollisionPair(CollisionPair(it->first, it->second));
	}
}

/*************************************************************************************/
/* COL�LISIONS */
/*************************************************************************************/
void PhysicsManager::onCollisionPairAdded(CollisionPair& pair) {
	//Actions to do when the collision pair is created for the first time	
}

void PhysicsManager::onCollisionPairStay(CollisionPair& pair) {
	//Actions to do when the bodies are in contact
	RigidBody* rBodyA = dynamic_cast<RigidBody*>(pair.first);
	RigidBody* rBodyB = dynamic_cast<RigidBody*>(pair.second);
	GameObject* goCell = rBodyA->getGameObject()? rBodyA->getGameObject() : rBodyB->getGameObject();
	BaseCharacter* character = rBodyA->getCharacter()? rBodyA->getCharacter() : rBodyB->getCharacter();

	if (goCell && (goCell->getTag() == "NavMeshCell") && character) {
		character->addOverlappingCell(goCell->getID());
	}
}

void PhysicsManager::onCollisionPairRemoved(CollisionPair& pair) {
	//Actions to do when the bodies are not in contact anymore
}

/*void PhysicsManager::rayHitPoint(Vector3 &from, Vector3 &to, IntersectionPoint3D& intersection, short collisionFilterGroup, short collisionFilterMask) {
	btCollisionWorld::ClosestRayResultCallback result(from, to);	
	result.m_collisionFilterGroup = collisionFilterGroup;
	result.m_collisionFilterMask = collisionFilterMask;
	_world->rayTest(from, to, result);
	if (result.hasHit()) {
		Vector3 hitPoint = result.m_hitPointWorld;
		intersection.location = Vec3D(hitPoint.getX(), hitPoint.getY(), hitPoint.getZ());
	}
	intersection.isValid = result.hasHit();
}

const CollisionObject* PhysicsManager::rayHitClosestBody(Vector3& from, Vector3& to, short collisionFilterGroup, short collisionFilterMask) {
	btDiscreteDynamicsWorld::ClosestRayResultCallback result(from, to);
	result.m_collisionFilterGroup = collisionFilterGroup;
	result.m_collisionFilterMask = collisionFilterMask; 
	_world->rayTest(from, to, result);

	if (result.hasHit()) return result.m_collisionObject;
	return NULL;
}*/

void PhysicsManager::rayCastCursor(RayCastResult& result, Camera* camera, short collisionFilterGroup, short collisionFilterMask) {
	//Get the size of the viewport in pixels
	Viewport* vp = camera->getViewport();
	int l, t, w, h;
	vp->getActualDimensions(l, t, w, h);

	//Get the pixel coordinates of the mouse
	int mX, mY;
	Core::getManager<InterfaceInputManager>()->getMouseCoords(mX, mY);
		
	//Compute the normalized screen coordinates of the cursor
	float scX = float(mX - l) / float(w);
	float scY = float(mY - t) / float(h);
	if ((scX < 0) || (scY < 0) || (scX > 1) || (scY > 1)) return;
		
	Ogre::Ray mouseRay = camera->getCameraToViewportRay(scX, scY);

	//If we arrive here, the ray is valid, i.e., it is on the bounds of the viewport. 
	Vector3 from = BtOgre::Convert::toBullet(mouseRay.getOrigin());			
	Vector3 to = BtOgre::Convert::toBullet(mouseRay.getPoint(10000.0f));	

	rayCast(from, to, result, collisionFilterGroup, collisionFilterMask);
}

void PhysicsManager::rayCast(Vector3& from, Vector3& to, RayCastResult& result, short collisionFilterGroup, short collisionFilterMask) {
	//Configure the bullet's ray cast data structure
	btDiscreteDynamicsWorld::ClosestRayResultCallback resultCallback(from, to);
	resultCallback.m_collisionFilterGroup = collisionFilterGroup;
	resultCallback.m_collisionFilterMask = collisionFilterMask; 
	_world->rayTest(from, to, resultCallback);

	//Transform bullet's ray cast data structure to Rogue's data structure
	result.hit = resultCallback.hasHit();
	if (result.hit) {
		CollisionObject* collisionObject = const_cast<CollisionObject*>(resultCallback.m_collisionObject);
		RigidBody* rigidBody = static_cast<RigidBody*>(collisionObject);		
		result.gameObject = rigidBody->getGameObject();
		result.character = rigidBody->getCharacter();
		result.normal = resultCallback.m_hitNormalWorld;
		result.position = resultCallback.m_hitPointWorld;
	}
}
