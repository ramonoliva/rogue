#include "RogueInputManager.h"
#include "RogueCore.h"
#include "RogueGlobalVars.h"

using namespace Rogue;

/***********************************************************************************/
/* CREATION AND DESTRUCTION */
/***********************************************************************************/
InputManager::InputManager() {
	Rogue::RenderWindow* renderWindow = Core::getManager<InterfaceRenderManager>()->getRenderWindow();
	OIS::ParamList pl;
    size_t windowHnd = 0;
    std::ostringstream windowHndStr;

    renderWindow->getCustomAttribute("WINDOW", &windowHnd);
    windowHndStr << windowHnd;
    pl.insert(std::make_pair(Ogre::String("WINDOW"), windowHndStr.str()));

	#if defined OIS_WIN32_PLATFORM
    pl.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_FOREGROUND" )));
    if (G_MOUSE_EXCLUSIVE) pl.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_EXCLUSIVE")));
	else pl.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_NONEXCLUSIVE")));
	#endif

    _oisManager = OIS::InputManager::createInputSystem(pl);
	
    _keyboard = static_cast<Keyboard*>(_oisManager->createInputObject(OIS::OISKeyboard, true));
    _mouse = static_cast<Mouse*>(_oisManager->createInputObject(OIS::OISMouse, true));

	//Register this as mouse and keyboard listener
	_mouse->setEventCallback(this);
	_keyboard->setEventCallback(this);

    //Set initial mouse clipping size
    windowResized(renderWindow);

    //Register as a Window listener
	Core::getManager<InterfaceRenderManager>()->addWindowEventListener(this);
}

InputManager::~InputManager() {
	Core::getManager<InterfaceRenderManager>()->removeWindowEventListener(this);
	_oisManager->destroyInputObject(_mouse);
    _oisManager->destroyInputObject(_keyboard);
    OIS::InputManager::destroyInputSystem(_oisManager);
}

void InputManager::addInputListener(InputListener* listener) {
	_inputListenerList.insert(listener);
}

void InputManager::removeInputListener(InputListener* listener) {
	_inputListenerList.erase(listener);
}

/***********************************************************************************/
/* KEYBOARD MANAGEMENT */
/***********************************************************************************/
bool InputManager::isTriggeredKey(KeyCode key) {
	if (_keyStateList.find(key) == _keyStateList.end()) return false;	//The key has not been pressed

	//If we arrive here is because key has been previously pressed. 
	KeyState& kState = _keyStateList[key];
	if (kState._state == KeyState::STATE_TRIGGERED) {
		//The key has been triggered. Change its state to pressed to avoid multiple calls
		//before the key is released. 
		kState._state = KeyState::STATE_PRESSED;
		return true;
	}
	return false;
}

bool InputManager::isPressedKey(KeyCode key) {
	return (_keyStateList.find(key) != _keyStateList.end());
}

void InputManager::releaseAllKeys() {
	_keyStateList.clear();
}

/***********************************************************************************/
/* MOUSE MANAGEMENT */
/***********************************************************************************/
bool InputManager::isTriggeredMouseButton(MouseButtonID button) {
	if (_mouseButtonStateList.find(button) == _mouseButtonStateList.end()) return false;	//The button has not been pressed

	//If we arrive here is because button has been previously pressed. 
	KeyState& kState = _mouseButtonStateList[button];
	if (kState._state == KeyState::STATE_TRIGGERED) {
		//The key has been triggered. Change its state to undefined to avoid multiple calls
		//before the key is released. 
		kState._state = KeyState::STATE_PRESSED;
		return true;
	}
	return false;
}

bool InputManager::isPressedMouseButton(MouseButtonID button) {
	return (_mouseButtonStateList.find(button) != _mouseButtonStateList.end()); 
}

void InputManager::getMouseCoords(int& mX, int& mY) {
	mX = _mouse->getMouseState().X.abs;
	mY = _mouse->getMouseState().Y.abs;
}

void InputManager::getMouseDisplacement(int& dX, int& dY, int& dZ) {
	dX = _mouse->getMouseState().X.rel;
	dY = _mouse->getMouseState().Y.rel;
	dZ = _mouse->getMouseState().Z.rel;
}

void InputManager::getClippingAreaSize(int& w, int& h) {
	w = _mouse->getMouseState().width;
	h = _mouse->getMouseState().height;
}

Mouse* InputManager::getMouse() {
	return _mouse;
}

Keyboard* InputManager::getKeyboard() {
	return _keyboard;
}

/***********************************************************************************/
/* UPDATE */
/***********************************************************************************/
void InputManager::update() {
	_keyboard->capture();
    _mouse->capture();
}

/***********************************************************************************/
/* OIS::KeyListener */
/***********************************************************************************/
bool InputManager::keyPressed(const KeyEvent &arg) {
	//A new key has been pressed. Initialize its state. 
	KeyState kState;
	kState._state = KeyState::STATE_TRIGGERED;
	kState._timeDown = 0;
	_keyStateList[arg.key] = kState;
	return true;
}

bool InputManager::keyReleased(const KeyEvent &arg) {
	//Drop the key state
	_keyStateList.erase(arg.key);
	return true;
}

/***********************************************************************************/
/* OIS::MouseListener */
/***********************************************************************************/
bool InputManager::mouseMoved(const MouseEvent &arg) {
	for (InputListenerList::iterator it = _inputListenerList.begin(); it != _inputListenerList.end(); it++) {
		(*it)->injectMouseMove(arg);
	}
	return true;
}

bool InputManager::mousePressed(const MouseEvent &arg, MouseButtonID id) {
	//A new key has been pressed. Initialize its state. 
	KeyState kState;
	kState._state = KeyState::STATE_TRIGGERED;
	kState._timeDown = 0;
	_mouseButtonStateList[id] = kState;

	for (InputListenerList::iterator it = _inputListenerList.begin(); it != _inputListenerList.end(); it++) {
		(*it)->injectMouseDown(arg, id);
	}
	return true;
}

bool InputManager::mouseReleased(const MouseEvent &arg, MouseButtonID id) {
	//Drop the key state
	_mouseButtonStateList.erase(id);

	for (InputListenerList::iterator it = _inputListenerList.begin(); it != _inputListenerList.end(); it++) {
		(*it)->injectMouseUp(arg, id);
	}
	return true;
}

/***********************************************************************************/
/* Ogre::WindowListener */
/***********************************************************************************/
//Adjust mouse clipping area
void InputManager::windowResized(Ogre::RenderWindow* rw) {
    unsigned int width, height, depth;
    int left, top;
    rw->getMetrics(width, height, depth, left, top);

    const MouseState &ms = _mouse->getMouseState();
    ms.width = width;
    ms.height = height;
}

void InputManager::windowClosed(Ogre::RenderWindow* rw) {
}

bool InputManager::windowClosing(Ogre::RenderWindow* rw) {

	/*
	Deshabilitem la possibilitat de poder tancar el joc
	amb accessos r�pids, com ALT + F4, per assegurar
	que el joc es tanca correctament
	*/
	return false;
}

