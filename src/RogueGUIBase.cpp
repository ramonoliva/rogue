#include "RogueGUIBase.h"

using namespace Rogue;

/**************************************************************************************/
/* CREATION AND DESTRUCTION */
/**************************************************************************************/
GUIBase::GUIBase(SdkTrayManager* tMgr) {
	_trayMgr = tMgr;
	_trayMgr->setListener(this);
	createGUI();
}

GUIBase::~GUIBase() {
	_trayMgr->clearAllTrays();
	_trayMgr->setListener(NULL);
}