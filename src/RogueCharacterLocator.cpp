#include "RogueCharacterLocator.h"
#include "RogueCollisionFilter.h"
#include "RogueScene.h"

using namespace Rogue;
using namespace NEOGEN;

const String TAG_NAVMESH_CELL = "NavMeshCell";

/***********************************************************************/
/* CREATION AND DESTRUCTION */
/***********************************************************************/
CharacterLocator::CharacterLocator(NavMesh* navMesh) {
	_navMesh = navMesh;
	_physicsManager = Core::getManager<InterfacePhysicsManager>();
	create2DMesh();
}

CharacterLocator::~CharacterLocator() {
	_navMesh = NULL;
}

void CharacterLocator::create2DMesh() {
	CellTable& cTable = _navMesh->getCellTable();
	for (CellTable::iterator it = cTable.begin(); it != cTable.end(); it++) {
		VertexList& vList = it->second.p->getVertexList();

		//Create a rigid body with the same shape than the polygon representing the cell
		CollisionShapeConvexHull* shape = new CollisionShapeConvexHull();
		for (VertexList::iterator v = vList.begin(); v != vList.end(); v++) {
			Vec3D& position = v->getPosition();
			shape->addPoint(Vector3(position.getX(), position.getY(), position.getZ()));
		}

		//Create the rigidBody
		RigidBody* rigidBody = _physicsManager->createRigidBody(shape, 0, COL_NAVMESH_CELL, navMeshCellCollidesWith);
		
		//Create the GameObject
		GameObject* go = Scene::createGameObject();
		go->addComponent<RigidBody>(rigidBody);
		go->setTag(TAG_NAVMESH_CELL);
		go->setID(it->first);
	}
}

int CharacterLocator::locateCharacter(BaseCharacter* character) {
	//Localitza a quina cel�la est� el personatge

	//Disparo un raig cap abaix des de la posici� del personatge, per localitzar el v�xel accessible sobre el qual estic
	Ogre::Vector3 charCenter = character->getPosition();
	btVector3 from(charCenter.x, charCenter.y, charCenter.z);
	btVector3 to(charCenter.x, charCenter.y - 10.0f, charCenter.z);
	RayCastResult result;
	_physicsManager->rayCast(from, to, result, COL_CHAR_LOCATOR, charLocatorCollidesWith);
	if (!result.hit) {
		return -1;
	}
	return result.gameObject->getID();
}