#include "RogueBaseCharacter.h"

using namespace Rogue;
using namespace NEOGEN;

/********************************************************************/
/* CREADORA I DESTRUCTORA											*/
/********************************************************************/
BaseCharacter::BaseCharacter(BaseCharConfigurator& baseCFG, RigidBody* rigidBody, OpenSteerCharConfigurator& openSteerCFG)
	: 
	OpenSteer::BaseVehicle(openSteerCFG.pdb)
{
	_rigidBody = rigidBody;
	_rigidBody->setCharacter(this);
	_isInstanced = baseCFG.isInstanced;
	_instancedEnt = baseCFG.instancedEnt;
	ent = baseCFG.ent;
	node = baseCFG.node;
	if (_isInstanced) node->attachObject(_instancedEnt);
	else node->attachObject(ent);
	_height = baseCFG.height;
	navMesh = baseCFG.navMesh;
	OpenSteer::BaseVehicle::setPosition(OpenSteer::Vec3(baseCFG.pos.x, baseCFG.pos.y, baseCFG.pos.z));
	setRadius(baseCFG.radius);
	
	currentCellID = -1;
	_waypointMode = PORTAL_SEEK_PROJECTION;

	_debugger = NULL;
}

BaseCharacter::~BaseCharacter() {
	OgreUtils::destroySceneNode(node);
	clearOverlappingCells();
	delete _rigidBody;
}

/********************************************************************/
/* GET I SET														*/
/********************************************************************/
DebuggerBase* BaseCharacter::getDebugger() {
	return _debugger;
}

void BaseCharacter::setDebugger(DebuggerBase* debugger) {
	_debugger = debugger;
}

void BaseCharacter::clearOverlappingCells() {
	_overlappingCells.clear();
}

void BaseCharacter::addOverlappingCell(int cellID) {
	_overlappingCells.insert(cellID);
}

bool BaseCharacter::isInCell(int cellID) {
	std::set<int>::iterator itFound = _overlappingCells.find(cellID);
	return (itFound != _overlappingCells.end());
}

void BaseCharacter::setMaterial(const Ogre::String& matName) {
	ent->setMaterialName(matName);
}

void BaseCharacter::getWayPoint(Vec3D& result) {
	result.setX(_wayPoint.getX());
	result.setY(_wayPoint.getY());
	result.setZ(_wayPoint.getZ());
}

void BaseCharacter::getShrinkedPortal(Vec3D& v1, Vec3D& v2) {
	v1.setX(_portalV1.getX());
	v1.setY(_portalV1.getY());
	v1.setZ(_portalV1.getZ());

	v2.setX(_portalV2.getX());
	v2.setY(_portalV2.getY());
	v2.setZ(_portalV2.getZ());
}

float BaseCharacter::getHeight() {
	return _height;
}

void BaseCharacter::setHeight(float h) {
	_height = h;
}

void BaseCharacter::setVisible(bool b) {
	node->setVisible(b);
}

void BaseCharacter::setPosition(Ogre::Vector3& pos) {
	node->setPosition(pos);
}

Ogre::Vector3 BaseCharacter::getPosition() {
	return node->getPosition();
}

Ogre::Quaternion BaseCharacter::getOrientation() {
	return node->getOrientation();
}

Ogre::SceneNode* BaseCharacter::getNode() {
	return node;
}

Ogre::Entity* BaseCharacter::getEntity() {
	return ent;
}

Ogre::Real BaseCharacter::getDiffCenterY() {
	return diffCenterY;
}

int BaseCharacter::getCurrentCellID() {
	return currentCellID;
}

void BaseCharacter::setCurrentCellID(int cellID) {
	currentCellID = cellID;
}

void BaseCharacter::setState(int s) {
	state = s;
}

int BaseCharacter::getState() {
	return state;
}

void BaseCharacter::setWaypointMode(int mode) {
	_waypointMode = mode;
}

int BaseCharacter::getWaypointMode() {
	return _waypointMode;
}

/********************************************************************/
/* ACTUALITZACIO													*/
/********************************************************************/

void BaseCharacter::update() {}

void BaseCharacter::updateLogic(float elapsedTime) {}

void BaseCharacter::updateRender(float elapsedTime) {}