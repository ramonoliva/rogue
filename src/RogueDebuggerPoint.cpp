#include "RogueDebuggerPoint.h"

using namespace Rogue;
using namespace NEOGEN;

#pragma region CREATION AND DESTRUCTION

DebuggerPoint::DebuggerPoint() : DebuggerBase() {
	Rogue::Entity* ent =  _renderManager->createEntity("Vert.mesh");
	_node = _renderManager->createSceneNode();
	_node->attachObject(ent);
	_node->setPosition(0,0,0);
}

DebuggerPoint::~DebuggerPoint() {
	OgreUtils::destroySceneNode(_node);
}

#pragma endregion

#pragma region GET AND SET

void DebuggerPoint::setPosition(float x, float y, float z) {
	_node->setPosition(x, y, z);
}

void DebuggerPoint::setPosition(Vec3D& v) {
	setPosition(v.getX(), v.getY(), v.getZ());
}

void DebuggerPoint::setScale(float s) {
	_node->setScale(s, s, s);
}

void DebuggerPoint::setVisible(bool v) {
	_node->setVisible(v);
}

void DebuggerPoint::setMaterialName(const std::string& matName) {
	((Rogue::Entity*)_node->getAttachedObject(0))->setMaterialName(matName);
}

void DebuggerPoint::draw(Vec3D& v, const Ogre::String& matName) {
	setMaterialName(matName);
	setPosition(v.getX(), v.getY(), v.getZ());
}

#pragma endregion