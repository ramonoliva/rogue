#include "RogueScene.h"

using namespace Rogue;

GameObjectSet Scene::_gameObjects;
Scene* Scene::_current;

#pragma region CREATION AND DESTRUCTION

Scene::Scene() {
	_renderManager = Core::getManager<InterfaceRenderManager>();
	_physicsManager = Core::getManager<InterfacePhysicsManager>();
	_guiManager = Core::getManager<InterfaceGUIManager>();
	_cameraManager = Core::getManager<InterfaceCameraManager>();
	_inputManager = Core::getManager<InterfaceInputManager>();
	_current = this;
}

Scene::~Scene() {
	_renderManager = NULL;
	_physicsManager = NULL;
	_guiManager = NULL;
	_cameraManager = NULL;
	_inputManager = NULL;
	destroyScene();
}

void Scene::destroyScene() {
	/*for (GameObjectList::iterator it = _gameObjects.begin(); it != _gameObjects.end(); it++) {
			delete *it;
	}*/
	_gameObjects.clear();
}

GameObject* Scene::createGameObject(const Vector3& position) {
	GameObject* go = new GameObject(position);
	_gameObjects.insert(go);
	return go;
}

void Scene::destroyGameObject(GameObject* go) {
	GameObjectSet::iterator it = _gameObjects.find(go);
	if (it != _gameObjects.end()) {
		_gameObjects.erase(go);
		delete go;
	}
}

#pragma endregion

#pragma region GET AND SET

Scene* Scene::getCurrent() {
	return _current;
}

#pragma endregion