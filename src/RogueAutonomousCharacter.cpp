#include "RogueAutonomousCharacter.h"
#include <exClearanceSolver.h>

using namespace Rogue;
using namespace NEOGEN;
using namespace Exact;
using namespace GeometrySolver;

/********************************************************************/
/* CREADORA I DESTRUCTORA											*/
/********************************************************************/

AutonomousCharacter::AutonomousCharacter(BaseCharConfigurator& baseCFG, RigidBody* rigidBody, OpenSteerCharConfigurator& openSteerCFG) 
				: 
				BaseCharacter(baseCFG, rigidBody, openSteerCFG)
{
	initializeBodyParams();
	diffCenterY = 0.12f; //0.24675f; //dist�ncia en Y entre el centre de la mesh de col�lisi� i de la mesh del personatge
	animState = NULL;
	setCurrentPortal(-1);					
	_waypointMode = PORTAL_SEEK_PROJECTION;
	currentCellID = -1;
	targetCellID = -1;
	numExploredCells = 0;
	setState(NINJA_STATE_IDLE);
}

AutonomousCharacter::~AutonomousCharacter() {
	
}

void AutonomousCharacter::initializeBodyParams() {
	//Inicialitza varis atributs de Bullet
	_rigidBody->setSleepingThresholds(0.0f, 0.0f);
	_rigidBody->setAngularFactor(0.0f);
	//setRestitution(0.0f);
	_rigidBody->setActivationState(DISABLE_DEACTIVATION);
}

/********************************************************************/
/* GET I SET														*/
/********************************************************************/
Ogre::AnimationState* AutonomousCharacter::getAnimState() {
	return animState;
}

void AutonomousCharacter::setOrigin(btVector3 &pos) {
	btTransform xform = _rigidBody->getWorldTransform();
	xform.setOrigin(btVector3(pos.getX(), pos.getY(), pos.getZ()));
	_rigidBody->setWorldTransform(xform);
}

btVector3 AutonomousCharacter::getForwardDir() {
	btTransform xform = _rigidBody->getWorldTransform();
	return xform.getBasis()[2];
}

btVector3 AutonomousCharacter::getUpDir() {
	btTransform xform = _rigidBody->getWorldTransform();
	return xform.getBasis()[1];
}

btVector3 AutonomousCharacter::getStrafeDir() {
	//return localAxis[0];
	btTransform xform = _rigidBody->getWorldTransform();
	return xform.getBasis()[0];
}

void AutonomousCharacter::setCurrentPortal(int p) {
	currentPortal = p;
}

int AutonomousCharacter::getCellState(int cellID) {
	CellMap::iterator itFound = cellMap.find(cellID);
	if (itFound == cellMap.end()) return CELL_STATE_NON_VISITED;
	return itFound->second;
}

void AutonomousCharacter::setCellState(int cellID, CellStateEnum cellState) {
	if (cellMap[cellID] != cellState) cellMap[cellID] = cellState;
	if (cellState == CELL_STATE_VISITED) visitedCells.insert(cellID);
	else if (cellState == CELL_STATE_EXPLORED) {
		visitedCells.erase(cellID);	//La cel�la ja no est� visitada, si no explorada. 
		numExploredCells++;
	}
}

void AutonomousCharacter::setCurrentCellID(int cellID) {
	BaseCharacter::setCurrentCellID(cellID);
	if (cellID == -1 || (getCellState(cellID) == CELL_STATE_EXPLORED)) return;
	setCellState(cellID, CELL_STATE_VISITED);	//La cel�la passa a estar visitada (potser tamb� explorada)
	Cell& c = navMesh->getCell(cellID);
	Cell* neighborC;
	int neighborState;
	bool neighborExplored;
	bool currentExplored = true;	//Suposo que la cel�la actual est� explorada
	std::set<int>::iterator it;
	for (std::set<int>::iterator itNeighbor = c.adjacentTo.begin(); itNeighbor != c.adjacentTo.end(); itNeighbor++) {
		neighborState = getCellState(*itNeighbor);
		if (neighborState == CELL_STATE_NON_VISITED) currentExplored = false;
		else if (neighborState == CELL_STATE_VISITED) {
			//El ve� est� marcat com a VISITAT. Cal comprobar si despr�s de visitar cellID, passa a estar EXPLORAT
			neighborC = &(navMesh->getCell(*itNeighbor));
			neighborExplored = true;	//Suposo que la cel�la ve�na est� explorada
			it = neighborC->adjacentTo.begin();
			while (neighborExplored && (it != neighborC->adjacentTo.end())) {
				neighborExplored = (getCellState(*it) != CELL_STATE_NON_VISITED);
				it++;
			}
			if (neighborExplored) {
				//Totes les cel�les adjacents de la cel�la ve�na han estat visitades o explorades. 
				//Per tant, el ve� est� explorat. 
				setCellState(*itNeighbor, CELL_STATE_EXPLORED);
			}
		}
	}
	if (currentExplored) {
		//Totes les cel�les adjacents de la cel�la actual han estat visitades o explorades. 
		//Per tant, la cel�la actual est� explorada. 
		setCellState(currentCellID, CELL_STATE_EXPLORED);
	}
}

void AutonomousCharacter::setTargetCellID(int c) {
	targetCellID = c;
}

int AutonomousCharacter::getTargetCellID() {
	return targetCellID;
}

int AutonomousCharacter::getCurrentPortal() {
	return currentPortal;
}

void AutonomousCharacter::findPath() {
	if ((currentCellID != -1) && (targetCellID != -1)) {
		//Les cel�les introdu�des s�n v�lides, per tant, puc trobar un cam�
		PathFinder::getSingleton().solve(currentCellID, targetCellID, goalPosition, radius(), path);
		if (path.empty()) {
			//No existeix cam� entre la posici� actual del personatge i la posici� final. 
			setCellState(targetCellID, CELL_STATE_EXPLORED);
		}
		//printPath();
	}
	else {
		//alguna de les cel�les introdu�da no �s v�lida. 
		path.clear();
	}
}

void AutonomousCharacter::updatePath() {
	if (currentCellID == targetCellID) {
		//He arribat a la cel�la on es troba l'objectiu
		//No haig de travessar cap portal perqu� el meu objectiu est� a la mateixa cel�la
		setCurrentPortal(-1); 
	}
	else {
		//Encara no he arribat a la cel�la on es troba l'objectiu. 
		//Comprobo si em trobo en alguna cel�la del cam� o si 
		//m'he perdut
		if (path.hasNode(currentCellID)) {
			//Estic en una cel�la del cam�
			int nextCell = path.getNextNode(currentCellID);
			//Calculo quin portal haig d'agafar per anar de currentCell a nextCell
			int portal = navMesh->getPortalTo(currentCellID, nextCell);
			setCurrentPortal(portal);
			computePortalSeekPoint();
		}
		else {
			//Estic en una cel�la que no pertany 
			//al cam� => m'he perdut. Cal calular el cam� entre la cel�la actual i la cel�la objectiu
			findPath();
		}
	}
}

void AutonomousCharacter::computePortalSeekPoint() {
	if (_waypointMode == PORTAL_SEEK_PROJECTION) portalSeekProjection();
	else if (_waypointMode == PORTAL_SEEK_MID_POINT) portalSeekMidPoint();
}

void AutonomousCharacter::portalSeekProjection() {
	//Computes the steering point over the portal by first computing the shrinked portal 
	//and then computing the projection point of the character's center over that transformed 
	//portal (see Oliva and Pelechano, SCA13). 

	//1) Compute the ShrinkedPortal
	Vec3D v1Shrinked, v2Shrinked;
	computeShrinkedPortal(currentPortal, v1Shrinked, v2Shrinked);

	//2) Compute the projection point over the shrinked portal
	Vec3D steeringPoint;
	computeProjection(v1Shrinked, v2Shrinked, steeringPoint);

	//WARNING!!! DEBUG
	_wayPoint.setCoords(steeringPoint.getX(), steeringPoint.getY(), steeringPoint.getZ());

	//3) To avoid deacceleration when character approaches to the steeringPoint, I compute
	//a point at distance d in the same direction of the steeringPoint. 
	Vec3D charCenter = Converter::fromOgreToVert(getPosition());
	steeringPoint.setY(charCenter.getY());
	Vec3D vDir = steeringPoint - charCenter;
	vDir.normalize();
	Vec3D t = charCenter + 5.0f * vDir;
	setTarget(OpenSteer::Vec3(t.getX(), t.getY(), t.getZ()));

	//WARNING!!! DEBUG
	_portalV1.setCoords(v1Shrinked.getX(), v1Shrinked.getY(), v1Shrinked.getZ());
	_portalV2.setCoords(v2Shrinked.getX(), v2Shrinked.getY(), v2Shrinked.getZ());
}

void AutonomousCharacter::portalSeekMidPoint() {
	//Computes a trivial steering point over the portal, that is simply its midpoint. 
	Edge& portal = navMesh->getEdge(currentPortal);
	Vec3D v1 = navMesh->getVertex(portal.v1).getPosition();
	Vec3D v2 = navMesh->getVertex(portal.v2).getPosition();
	Vec3D steeringPoint = (v1 + v2) / 2.0f;

	//WARNING!!! DEBUG
	_wayPoint.setCoords(steeringPoint.getX(), steeringPoint.getY(), steeringPoint.getZ());

	//To avoid deacceleration when character approaches to the steeringPoint, I compute
	//a point at distance d in the same direction of the steeringPoint. 
	Vec3D charCenter = Converter::fromOgreToVert(getPosition());
	steeringPoint.setY(charCenter.getY());
	Vec3D vDir = steeringPoint - charCenter;
	vDir.normalize();
	Vec3D t = charCenter + 5.0f * vDir;
	setTarget(OpenSteer::Vec3(t.getX(), t.getY(), t.getZ()));

	//WARNING!!! DEBUG
	_portalV1.setCoords(v1.getX(), v1.getY(), v1.getZ());
	_portalV2.setCoords(v2.getX(), v2.getY(), v2.getZ());
}

void AutonomousCharacter::computeShrinkedPortal(int portalID, Vec3D& v1, Vec3D& v2) {
	int nextCellID = path.getNextNode(currentCellID);
	int entryPortalID = navMesh->getPortalTo(nextCellID, currentCellID);	//The portal of nextCell used to pass from nextCell to currentCell
	ClearanceSolver::getSingleton().shrinkPortal(entryPortalID, path, radius(), v1, v2);
}

void AutonomousCharacter::computeProjection(Vec3D& portalV1, Vec3D& portalV2, Vec3D& steeringPoint) {
	//Computes the projection of the center of the character over the portal defined by the endpoints portalV1 and portalV2.
	//If the projection is valid (i.e., it falls inside the endpoints of the portal), this point is choosen;
	//otherwise, it returns the FURTHEST endpoint from the character. 
	//The result is stored in steeringPoint. 
	IntersectionPoint3D projection;
	Vec3D charCenter = Converter::fromOgreToVert(getPosition());
	GeometrySolver::projectionPointLine3D(charCenter, portalV1, portalV2, projection);
	if (GeometrySolver::isBetween(projection.location, portalV1, portalV2)) {
		//The projection is valid
		steeringPoint = projection.location;
	}
	else {
		float d1, d2;
		//Projection is invalid. Choose the furthest endpoint with respect to the character. 
		d1 = GeometrySolver::to2D(charCenter).dist2(GeometrySolver::to2D(portalV1));
		d2 = GeometrySolver::to2D(charCenter).dist2(GeometrySolver::to2D(portalV2));
		if (d1 > d2) steeringPoint = portalV1;
		else steeringPoint = portalV2;
	}
}

/********************************************************************/
/* ACTUALITZACIO													*/
/********************************************************************/

void AutonomousCharacter::updateLogic(Ogre::Real timeLF) {
	//Actualitza la l�gica del personatge
	choosePatternState();
	updateState(timeLF);
	updateSteering(timeLF);
	//updateGraphics();
}

void AutonomousCharacter::updateSteering(float elapsedTime) {
	OpenSteer::Vec3 totalSteering = computeTotalSteering(elapsedTime);
	totalSteering.y = 0.0f;	//Currently, consider only steerings on the XZ plane
	applySteeringForce(totalSteering, elapsedTime);

	btVector3 oldVel = _rigidBody->getLinearVelocity();	//The current velocity of the RigidBody 
	OpenSteer::Vec3 newVel = velocity();
	_rigidBody->setLinearVelocity(btVector3(newVel.x, newVel.y + oldVel.getY(), newVel.z));
	btTransform xform = _rigidBody->getWorldTransform();
	btVector3 newPos = xform.getOrigin();
	//newPos.setY(newPos.getY() - diffCenterY);
	
	//Update the position of the 3D model representing the character
	BaseVehicle::setPosition(Converter::fromBulletToOpenSteer(newPos));
	setPosition(Ogre::Vector3(newPos.x(), newPos.y(), newPos.z()));
}

void AutonomousCharacter::updateRender(Ogre::Real timeLF) {
	updateAnimState(timeLF);
}

void AutonomousCharacter::updateAnimState(Ogre::Real timeLF) {
	if (animState == NULL) return;
	animState->addTime(timeLF);
}

void AutonomousCharacter::updateGraphics() {
	/*
	Orienta i posiciona la mesh d'OGRE en relaci� a la 
	shape de bullet
	*/
	
	//Actualitzo la posici�
	btTransform xform = _rigidBody->getWorldTransform();
	btVector3 newPos = xform.getOrigin();
	newPos.setY(newPos.getY() - diffCenterY);
	setPosition(Ogre::Vector3(newPos.x(), newPos.y(), newPos.z()));
	
	//Actualitzo la rotaci�
	Ogre::Vector3 v = Ogre::Vector3::UNIT_Z;
	Ogre::Vector3 shapeForward = BtOgre::Convert::toOgre(getForwardDir());
	Ogre::Quaternion newRot = v.getRotationTo(shapeForward);
	node->setOrientation(newRot);
}