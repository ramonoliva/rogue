#include "RogueGameApp.h"

#include "RogueGlobalVars.h"

using namespace Rogue;

GameApp::GameApp() {
	//Null initialization to avoid problems on delete of uninitialized pointers
	_renderManager = NULL;
	_inputManager = NULL;
	_guiManager = NULL;
	_physicsManager = NULL;
	_cameraManager = NULL;
	_obfuscator = NULL;
	_gameMapManager = NULL;
}

GameApp::~GameApp() {
	/*for (SceneList::iterator it = _sceneList.begin(); it != _sceneList.end(); it++) {
		delete *it;
	}*/
	_sceneList.clear();

	delete _obfuscator;
	delete _gameMapManager;
	
	//Delete the GameManagers
	delete _cameraManager;
	delete _physicsManager;
	delete _guiManager;
	delete _inputManager;
	delete _renderManager;

	Core::removeAllManagers();
}

Scene* GameApp::loadScene(int sceneID) {
	if ((sceneID < 0) || (sceneID >= int(_sceneList.size()))) return NULL;
	return _sceneList[sceneID];
}

void GameApp::go() {
	//Game Managers Initialization
	registerGameManagers();

	registerScenes();
	    
	//GameLoop
	int currentSceneType;	//Helper variable to determine if we have changed the scene
	Scene* scene = NULL;
	while (G_SCENE_STATE != SCENE_NULL) {
		currentSceneType = G_SCENE_STATE;	//Save the current value of G_SCENE_STATE
		scene = loadScene(G_SCENE_STATE);
		scene->createScene();
		while (currentSceneType == G_SCENE_STATE) {
			_cameraManager->update();
			_inputManager->update();
			scene->update();	//Update the current scene until a scene change is required
			_renderManager->update();
		}
		scene->destroyScene();
	}
}

void GameApp::registerGameManagers() {
	registerRenderManager();
	registerInputManager();
	registerGUIManager();
	registerPhysicsManager();
	registerCameraManager();
	registerObfuscatorManager();
	registerGameMapManager();
}

void GameApp::registerRenderManager() {
	//RenderManager
	_renderManager = new RenderManager(G_APP_NAME, Ogre::ST_EXTERIOR_CLOSE);
	Core::registerManager<InterfaceRenderManager>(_renderManager);
}

void GameApp::registerInputManager() {
	//InputManager
	_inputManager = new InputManager(); 
	Core::registerManager<InterfaceInputManager>(_inputManager);
}

void GameApp::registerGUIManager() {
	//GUIManager
	_guiManager = new GUIManager();
	Core::registerManager<InterfaceGUIManager>(_guiManager);
}

void GameApp::registerPhysicsManager() {
	//PhysicsManager
	_physicsManager = new PhysicsManager();
	Core::registerManager<InterfacePhysicsManager>(_physicsManager);
}
			 
void GameApp::registerCameraManager() {
	//CameraManager
	_cameraManager = new CameraManager();
	Core::registerManager<InterfaceCameraManager>(_cameraManager);
}

void GameApp::registerObfuscatorManager() {
	//ObfuscatorManager
	_obfuscator = new ObfuscatorZip::ObfuscatedZipFactory();
	Ogre::ArchiveManager::getSingleton().addArchiveFactory(_obfuscator);
}

void GameApp::registerGameMapManager() {
	//GameMapManager
	_gameMapManager = GameMapManager::getSingletonPtr();
	_gameMapManager->initialize();
	_gameMapManager->setupResources();
}