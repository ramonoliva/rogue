#include "GameMapSerializer.h"

using namespace Rogue;

/***************************************************************************************/
/* CREACIO I DESTRUCCIO */
/***************************************************************************************/

GameMapSerializer::GameMapSerializer() {

}
 
GameMapSerializer::~GameMapSerializer() {

}
 
void GameMapSerializer::exportGameMap(const GameMap *pText, const Ogre::String &fileName) {
    //std::ofstream outFile;
    //outFile.open(fileName.c_str(), std::ios::out);
    //outFile << pText->getString();
	//outFile.close();
}

void GameMapSerializer::importGameMap(Ogre::DataStreamPtr &stream, GameMap *pDest) {
	int gMode = Ogre::StringConverter::parseInt(stream->getLine());	//Llegeixo el mode en que m'arriba la geometria
	pDest->setGeometryMode(gMode);													
	pDest->setGeometryFile(stream->getLine());						//Fitxer que cont� la geometria
	pDest->setImgPreview(stream->getLine());						//Preview del mapa de joc
	pDest->setNavMeshFile(stream->getLine());						//Fitxer que cont� la navMesh del mapa
	if (gMode == GEOMETRY_MODE_MESH) {
		//La geometria arriba per un fitxer tipus mesh. Obtinc la posici� inicial de les banderes
		//Posici� de la bandera vermella
		float x, y, z;
		x = Ogre::StringConverter::parseReal(stream->getLine());
		y = Ogre::StringConverter::parseReal(stream->getLine());
		z = Ogre::StringConverter::parseReal(stream->getLine());
		pDest->setRedFlagPos(x, y, z);
		x = Ogre::StringConverter::parseReal(stream->getLine());
		y = Ogre::StringConverter::parseReal(stream->getLine());
		z = Ogre::StringConverter::parseReal(stream->getLine());
		pDest->setBlueFlagPos(x, y, z);
	}

}