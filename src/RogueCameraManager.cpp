#include "RogueCameraManager.h"
#include "RogueCore.h"

using namespace Rogue;

static const float MOUSE_WHEEL_SENSITIVITY = 0.025f;
static const float MOUSE_MOVE_SENSITIVITY = 0.1f;
static const float MOUSE_ROTATE_SPEED = 0.1f;
static const float CAMERA_MOVE_SPEED = 0.25f;

/*************************************************************/
/* CREATION AND DESTRUCTION */
/*************************************************************/
CameraManager::CameraManager() {
	_camera = NULL;
	_fixOrientation = false;
	_inputManager = Core::getManager<InterfaceInputManager>();
}

CameraManager::~CameraManager() {}

/*************************************************************/
/* GET AND SET */
/*************************************************************/
void CameraManager::setCamera(Camera* cam) {
	_camera = cam;
}

Camera* CameraManager::getCamera() {
	return _camera;
}

void CameraManager::fixOrientation(bool f) {
	_fixOrientation = f;
}

/*************************************************************/
/* INPUT REACTION */
/*************************************************************/
void CameraManager::update() {
	if (_camera == NULL) return;
	Ogre::Vector3 moveVector = Ogre::Vector3::ZERO;
	float yaw = 0.0f;
	float pitch = 0.0f;
	int mouseDX, mouseDY, mouseDZ;
	_inputManager->getMouseDisplacement(mouseDX, mouseDY, mouseDZ);
	if (mouseDZ) {
		//Mouse Wheel => Zoom. Move the camera towards its local forward direction. 
		moveVector = Ogre::Vector3::NEGATIVE_UNIT_Z * float(mouseDZ) * MOUSE_WHEEL_SENSITIVITY;
		_camera->moveRelative(moveVector);
	}
	else if (_inputManager->isPressedMouseButton(OIS::MB_Right)) {
		//We are moving the mouse with the RMB pressed
		if (_inputManager->isPressedKey(OIS::KC_W)) moveVector.z = -1.0f;
		else if (_inputManager->isPressedKey(OIS::KC_S)) moveVector.z = 1.0f;

		if (_inputManager->isPressedKey(OIS::KC_D)) moveVector.x = 1.0f;
		else if (_inputManager->isPressedKey(OIS::KC_A)) moveVector.x = -1.0f;

		if (_inputManager->isPressedKey(OIS::KC_E)) moveVector.y = 1.0f;
		else if (_inputManager->isPressedKey(OIS::KC_Q)) moveVector.y = -1.0f;

		if (!_fixOrientation) {
			yaw = -mouseDX * MOUSE_ROTATE_SPEED;
			pitch = -mouseDY * MOUSE_ROTATE_SPEED;
		}
	}
	_camera->moveRelative(moveVector * CAMERA_MOVE_SPEED);
	_camera->yaw(Ogre::Degree(yaw));
	_camera->pitch(Ogre::Degree(pitch));
}

void CameraManager::viewTop() {
	_camera->setFixedYawAxis(false);
	_camera->setOrientation(Ogre::Quaternion(float(SQRT_ONE_HALF), float(-SQRT_ONE_HALF), 0.0f, 0.0f));
}