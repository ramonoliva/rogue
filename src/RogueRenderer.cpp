#include "RogueRenderer.h"
#include "RogueGameObject.h"
#include "OgreUtils.h"

using namespace Rogue;

/**************************************************************************/
/* CREATION AND DESTRUCTION */
/**************************************************************************/
Renderer::Renderer(Mesh* m, SceneNode* n) : GameComponent() {
	_mesh = m;
	_node = n;
	_node->attachObject(_mesh);
	_visible = true;
}

Renderer::~Renderer() {
	OgreUtils::destroySceneNode(_node);
}

/**************************************************************************/
/* GET AND SET */
/**************************************************************************/
void Renderer::setScale(Vector3& scale) {
	_node->setScale(scale.getX(), scale.getY(), scale.getZ());
}

/**************************************************************************/
/* UPDATE */
/**************************************************************************/
void Renderer::update() {
	//Actualitzo la posici�
	const Transform& xform = _gameObject->getTransform();
	Vector3 newPos = xform.getOrigin();
	_node->setPosition(Ogre::Vector3(newPos.x(), newPos.y(), newPos.z()));
}