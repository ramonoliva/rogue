#include "RogueGameObject.h"
#include "RogueTypes.h"

using namespace Rogue;

/***************************************************************************/
/* CREATION AND DESTRUCTION */
/***************************************************************************/
GameObject::GameObject(const Vector3& position) {
	_tag = "";
	_id = 0;
	_parent = NULL;
	_transform.setGameObject(this);
	_transform.setOrigin(position);

	_renderer = NULL;
	_rigidBody = NULL;
}

GameObject::~GameObject() {
	destroyGameComponents();
	destroyChildren();
}

void GameObject::destroyChildren() {
	for (GameObjectList::iterator it = _children.begin(); it != _children.end(); it++) {
		delete *it;
	}
	_children.clear();
}

void GameObject::destroyGameComponents() {
	for (GameComponentList::iterator it = _gameComponents.begin(); it != _gameComponents.end(); it++) {
		delete it->second;
	}
	_gameComponents.clear();
}

/***************************************************************************/
/* GET AND SET */
/***************************************************************************/
void GameObject::addChild(GameObject* c) {
	addChildBefore(c, NULL);
}

void GameObject::addChildBefore(GameObject* c, GameObject* next) {
	if (!next || (next->getParent() != this)) {
		_children.push_back(c);
	}
	else {
		GameObjectList::iterator itNext = std::find(_children.begin(), _children.end(), next);
		_children.insert(itNext, c);
	}
	c->setParent(this);
}

GameObject* GameObject::getNextSibling(GameObject* c) {
	if (c->getParent() != this) return NULL;

	GameObjectList::iterator it = std::find(_children.begin(), _children.end(), c);
	it++;

	if (it == _children.end()) return _children.front();
	return *it;
}

GameObject* GameObject::getPrevSibling(GameObject* c) {
	if (c->getParent() != this) return NULL;

	GameObjectList::iterator it = std::find(_children.begin(), _children.end(), c);
	if (it == _children.begin()) return _children.back();
	it--;
	return *it;
}

void GameObject::setScale(Vector3& s) {
	_scale = s;

	if (_renderer) _renderer->setScale(s);
	if (_rigidBody) _rigidBody->setScale(s);
}

/***************************************************************************/
/* UPDATE */
/***************************************************************************/
void GameObject::onUpdateRender() {
	
}

void GameObject::onUpdatePhysics() {
	RigidBody* rBody = getComponent<RigidBody>();
	if (rBody) rBody->update();

	Renderer* renderer = getComponent<Renderer>();
	if (renderer) renderer->update();
}

void GameObject::onTransformDirty() {
	RigidBody* rBody = getComponent<RigidBody>();
	if (rBody) {
		Transform xform;
		rBody->getMotionState()->getWorldTransform(xform);
		xform.setOrigin(_transform.getOrigin());
		rBody->getMotionState()->setWorldTransform(xform);
	}
}