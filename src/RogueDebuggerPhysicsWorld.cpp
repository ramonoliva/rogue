#include "RogueDebuggerPhysicsWorld.h"
#include "RogueCore.h"

using namespace Rogue;

#pragma region CREATION AND DESTRUCTION

DebuggerPhysicsWorld::DebuggerPhysicsWorld() : DebuggerBase() {
	PhysicsWorld* world = Rogue::Core::getManager<InterfacePhysicsManager>()->getPhysicsWorld();
	SceneNode* rootNode = _renderManager->getRootSceneNode();
	_debugDrawer = new DebugDrawer(rootNode, world);
	world->setDebugDrawer(_debugDrawer);
}

DebuggerPhysicsWorld::~DebuggerPhysicsWorld() {
	delete _debugDrawer;
}

#pragma endregion

#pragma region UPDATE

void DebuggerPhysicsWorld::update() {
	if (_isVisible) _debugDrawer->setDebugMode(1);
	else _debugDrawer->setDebugMode(0);
	_debugDrawer->step();
}

#pragma endregion