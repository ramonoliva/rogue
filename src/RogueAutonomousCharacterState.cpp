#include "RogueAutonomousCharacter.h"

using namespace Rogue;

/*************************************************************************************/
/* Aquest fitxer cont� les funcions relacionades amb l'estat del ninja */
/*************************************************************************************/

/************************************/
/* ACTUALITZACI� GENERAL DE L'ESTAT */
/************************************/

void AutonomousCharacter::updateState(Ogre::Real timeLF) {
	//Actualitza l'estat actual del personatge
	if (state == NINJA_STATE_IDLE) {
		updateStateIdle();
	}
	else if (state == NINJA_STATE_RANDOM_TARGET) {
		updateStateRandomTarget();
	}
	else if (state == NINJA_STATE_SCRIPTED_PATH) {
		updateStateScriptedPath();
	}
}

void AutonomousCharacter::choosePatternState() {
	//Indica de forma general, de quina manera es comporta el personatge
	if (currentCellID == -1) {
		setState(NINJA_STATE_IDLE);
		return;
	}
	if (_scriptedPath.size() == 0) {
		setState(NINJA_STATE_RANDOM_TARGET);
	}
	else {
		setState(NINJA_STATE_SCRIPTED_PATH);
	}
}

/***************************************/
/* ACTUALITZACI� ESPEC�FICA DE L'ESTAT */
/***************************************/

void AutonomousCharacter::updateStateRandomTarget() {
	if ((targetCellID == -1) || (isInCell(targetCellID)) || path.empty()) {
		int nextCell = -1;
		nextCell = navMesh->getRandomCell();
		if (nextCell != -1) navMesh->getCell(nextCell).p->getCenter(goalPosition);
		setTargetCellID(nextCell);
		findPath();
	}
	updatePath();
}

void AutonomousCharacter::updateStateIdle() {
	if (currentCellID == -1) {
		Ogre::LogManager::getSingletonPtr()->logMessage("UPDATING IDLE BECAUSE NO CELL DETECTED!");
	}
}

void AutonomousCharacter::updateStateScriptedPath() {
	if ((targetCellID == -1) || (currentCellID == targetCellID) || path.empty()) {
		setTargetCellID(_scriptedPath[currentCellID]);
		findPath();
	}
	updatePath();
}