#include "RogueSceneGame.h"
#include "ngPathFinder.h"
#include "RogueCollisionFilter.h"

#include "RogueCore.h"
#include "RogueGameObject.h"
#include "GameMapManager.h"

static const float FOVY = 45.0f;
static const float PHYSICS_OFFSET_Y = 0.1f;	
static const unsigned long TIME_TO_UPDATE_AI = 33;		//AI update frequency in ms. 

using namespace Rogue;
using namespace NEOGEN;
using namespace GeometrySolver;

/**************************************************************************************************************/
/* CREATION, INITIALIZATION AND DESTRUCTION */
/**************************************************************************************************************/
SceneGame::SceneGame() {
	_pause = false;
	_proximityDB = NULL;
	_navMesh = NULL;
	_debuggerNavMesh = NULL;
	_characterLocator = NULL;
	_sceneMgr = Core::getManager<InterfaceRenderManager>()->getSceneManager();
	_manualMapNode = NULL;
}

void SceneGame::createProximityDataBase() {
	//WARNING!!! Provide a proper DataBase configuration based on the propoerties of the scene
	OpenSteer::Vec3 center = OpenSteer::Vec3::zero;
    float div = 125.0f;
    OpenSteer::Vec3 divisions (div, 1.0f, div);
    float diameter = 500.0f; 
    OpenSteer::Vec3 dimensions (diameter, diameter, diameter);
    //_proximityDB = new OpenSteer::LQPDAV(center, dimensions, divisions);
	_proximityDB = new OpenSteer::BruteForceProximityDatabase<OpenSteer::AbstractVehicle*>();
}

SceneGame::~SceneGame(){
	destroyScene();
}

void SceneGame::createScene() {
	createProximityDataBase();
	_pause = false;
	
	//Configure the main camera params
	Camera* camera = Core::getManager<InterfaceRenderManager>()->getMainCamera();
	camera->setPosition(0.0f, 25.0f, 45.0f);
	camera->lookAt(0.0f, 0.0f, 0.0f);
	camera->setFOVy(Ogre::Degree(FOVY));
	Core::getManager<InterfaceCameraManager>()->setCamera(camera);

	//Configure the lighting
	_renderManager->setAmbientLightColor(Color(0.5f, 0.5f, 0.5f));
	Light* directionalLight = _renderManager->createLight("directionalLight");
    directionalLight->setType(Ogre::Light::LT_DIRECTIONAL);
    directionalLight->setDiffuseColour(Color(1.0f, 1.0f, 1.0f));
    directionalLight->setSpecularColour(Color(0.0f, 0.0f, 0.0f));
	directionalLight->setDirection(Ogre::Vector3( 0, -1, -1));

	resetTimer();
}

void SceneGame::destroyScene() {
	Scene::destroyScene();
	//Clear the physics world
	Core::getManager<InterfacePhysicsManager>()->clearWorld();

	//Destroy the characters and its (possibly) debuggers
	destroyCharacters();
	
	//Destroy other graphic objects
	for (SceneNodeList::iterator it = _sceneNodeList.begin(); it != _sceneNodeList.end(); it++) {
		OgreUtils::destroySceneNode(*it);
	}
	_sceneNodeList.clear();

	//Destroy the lights
	_sceneMgr->destroyAllLights();

	_renderManager->destroySceneNode(_manualMapNode);
	_manualMapNode = NULL;

	//Destroy other stuff
	delete _navMesh;
	delete _debuggerNavMesh;
	delete _characterLocator;
}

void SceneGame::destroyCharacters() {
	//Remove all characters
	for (CharList::iterator it = _charList.begin(); it != _charList.end(); it++) {
		delete *it;
	}
	_charList.clear();
	delete _proximityDB;
}

void SceneGame::createGameMapScene(const Ogre::String& sceneFile) {
	//Construeix una escena a partir d'un fitxer .scene
	Ogre::String fileLocation = "../../media/maps/" + sceneFile;
	Ogre::LogManager::getSingletonPtr()->logMessage("CREATING SCENE FROM FILE = " + fileLocation);
	
	pugi::xml_document doc;
	pugi::xml_parse_result result;
	result = doc.load_file(fileLocation.c_str());
	
	pugi::xml_node root = doc.child("scene");
	pugi::xml_node sceneNodes = root.child("nodes");
	createSceneNodes(sceneNodes, Ogre::Vector3(0.0f), Ogre::Vector3(1.0f));
}

void SceneGame::createGameMapMesh(const Ogre::String& sceneFile) {
	//Construeix una escena a partir d'un fitxer .mesh
	Ogre::String fileLocation = "../../media/maps/" + sceneFile;
	Ogre::LogManager::getSingletonPtr()->logMessage("CREATING SCENE FROM FILE = " + fileLocation);
	Ogre::Entity* ent = _sceneMgr->createEntity(sceneFile);
	Ogre::SceneNode* node = _sceneMgr->getRootSceneNode()->createChildSceneNode();
	node->setPosition(0.0f, -0.5f, 0.0f);	//Offset per map2
	node->attachObject(ent);
	
	BtOgre::StaticMeshToShapeConverter converter(ent);
	CollisionShape* shape = converter.createTrimesh();
	Core::getManager<InterfacePhysicsManager>()->createRigidBody(shape, 0.0f, COL_WALL, wallCollidesWith);
}

void SceneGame::createSceneNodes(const pugi::xml_node& sceneNodes, Ogre::Vector3& parentPos, Ogre::Vector3& parentScale) {
	//Crea els nodes de l'escena
	
	//Variables de pugixml
	pugi::xml_node position;	//Node que defineix la posici� de l'objecte
	pugi::xml_node quaternion;	//Node que defineix la rotaci� de l'objecte
	pugi::xml_node scale;		//Node que defineix l'escalat de l'objecte
	pugi::xml_node entity;		//Node que defineix la mesh de l'objecte
	pugi::xml_node userData;	//Node que defineix altres atributs de l'objecte (atributs l�gics de Blender)
	
	//Variables d'OGRE
	float pX, pY, pZ;			//Posici� de l'objecte
	float qX, qY, qZ, qW;		//Rotaci� de l'objecte (quaternion)
	float sX, sY, sZ;			//Escalat de l'objecte
	Ogre::String meshFile;
	Ogre::SceneNode* ogreNode;
	Ogre::Entity* ogreEnt;
	Ogre::Vector3 ogrePos;
	Ogre::Vector3 ogreScale;
	Ogre::String propName;
	bool isFloor = false;
	Ogre::String matName;
	Ogre::String nodeName;
	Ogre::MaterialManager& matManager = Ogre::MaterialManager::getSingleton();

	//Recorro tots els nodes
	for (pugi::xml_node node = sceneNodes.child("node"); node; node = node.next_sibling("node")) {
		//Obtinc els atributs del xml i els converteixo a Ogre

		//Nom del node
		nodeName = node.attribute("name").as_string();
		
		//Posici�
		position = node.child("position");		
		pX = position.attribute("x").as_float();
		pY = position.attribute("y").as_float();
		pZ = position.attribute("z").as_float();
		ogrePos = parentPos + Ogre::Vector3(pX, pY, pZ);

		//Rotaci�
		quaternion = node.child("quaternion");
		qX = quaternion.attribute("x").as_float();
		qY = quaternion.attribute("y").as_float();
		qZ = quaternion.attribute("z").as_float();
		qW = quaternion.attribute("w").as_float();

		//Escalat
		scale = node.child("scale");
		sX = scale.attribute("x").as_float();
		sY = scale.attribute("y").as_float();
		sZ = scale.attribute("z").as_float();
		ogreScale = Ogre::Vector3(parentScale.x * sX, parentScale.y * sY, parentScale.z * sZ);

		//Mesh
		entity = node.child("entity");
		meshFile = entity.attribute("meshFile").value();

		//Construeixo l'objecte d'OGRE amb la informaci� recuperada
		ogreEnt = _sceneMgr->createEntity(meshFile);
		ogreNode = _sceneMgr->getRootSceneNode()->createChildSceneNode();
		ogreNode->setPosition(ogrePos);
		ogreNode->setOrientation(qW, qX, qY, qZ);
		ogreNode->setScale(sX, sY, sZ);
		ogreNode->attachObject(ogreEnt);
		_sceneNodeList.push_back(ogreNode);

		//Creo els objectes del m�n de bullet
		BtOgre::StaticMeshToShapeConverter converter(ogreEnt);
		btCollisionShape* shape = converter.createTrimesh();
		Core::getManager<InterfacePhysicsManager>()->createRigidBody(shape, 0, Vector3(ogrePos.x, ogrePos.y, ogrePos.z), COL_WALL, wallCollidesWith); 

		//Comprovo si el node t� altres nodes fills
		if (node.child("node")) createSceneNodes(node, ogrePos, ogreScale);
	}
}

void SceneGame::createGameMapFromNavMesh() {
	ManualObject* manualMap = _renderManager->createManualObject();
	CellTable& cTable = _navMesh->getCellTable();
	
	//Create the ManualObject from the previously loaded NavMesh
	manualMap->begin("scaFloor", Ogre::RenderOperation::OT_TRIANGLE_LIST);
	float offsetY = 0.1f;
	for (CellTable::iterator itCell = cTable.begin(); itCell != cTable.end(); itCell++) {
		Poly* p = itCell->second.p;
		Vec3D& pivotPos = p->getVertexAt(0).getPosition();

		for (size_t i = 2; i < p->getNumVert(); i++) {
			Vec3D& pos1 = p->getVertexAt(i).getPosition();
			Vec3D& pos2 = p->getVertexAt(i - 1).getPosition();

			manualMap->position(Ogre::Vector3(pivotPos.getX(), pivotPos.getY() - offsetY, pivotPos.getZ()));
			manualMap->position(Ogre::Vector3(pos1.getX(), pos1.getY() - offsetY, pos1.getZ()));
			manualMap->position(Ogre::Vector3(pos2.getX(), pos2.getY() - offsetY, pos2.getZ()));
		}
	}
	manualMap->end();

	//Transform the ManualObject to an Entity and attach it to a SceneNode in order to see it
	Ogre::MeshPtr meshPtr = manualMap->convertToMesh("manualMap.mesh");
	Entity* ent = _renderManager->createEntity("manualMap.mesh");
	_manualMapNode = _renderManager->createSceneNode();
	_manualMapNode->attachObject(ent);
	//manualMap->clear();
	
	//Creo els objectes del m�n de bullet
	/*BtOgre::StaticMeshToShapeConverter converter(ent);
	btCollisionShape* shape = converter.createTrimesh();
	Core::getManager<InterfacePhysicsManager>()->createRigidBody(shape, 0, Vector3(0,0,0), COL_WALL, wallCollidesWith); */
	btCollisionShape* shape = new btStaticPlaneShape(btVector3(0,1,0),0);
	_physicsManager->createRigidBody(shape, 0, Vector3(0,0,0), COL_WALL, wallCollidesWith);
}

void SceneGame::loadMap(const Ogre::String& mapName) {
	_gameMap = GameMapManager::getSingletonPtr()->load(mapName, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME).staticCast<GameMap>();
	loadNavMesh(_gameMap->getNavMeshFile());
	
	//Load the GameMap according to the indicated scene mode
	int geometryMode = _gameMap->getGeometryMode();
		
	switch (geometryMode) {
		case GEOMETRY_MODE_SCENE:	//The information is in a .scene file
			createGameMapScene(_gameMap->getGeometryFile());
			break;

		case GEOMETRY_MODE_MESH:	//The information is in a .mesh file
			createGameMapMesh(_gameMap->getGeometryFile());
			break;

		case GEOMETRY_MODE_AUTO:	//Generates the map directly from the NavMesh
			createGameMapFromNavMesh();
			break;
	}
}

void SceneGame::loadNavMesh(const Ogre::String& fileName) {
	Ogre::String fileLocation = "../../media/maps/" + fileName;
	_navMesh = new NavMesh();
	MapParser::getSingleton().loadNavMesh(_navMesh, fileLocation);
	PathFinder::getSingleton().initialize(_navMesh);
	//Core::getManager<InterfaceRenderManager>()->setLinePattern(4, 1, 0xFFFF);
	//_navMesh->drawEdges();
	_debuggerNavMesh = new DebuggerNavMesh();
	_debuggerNavMesh->setNavMesh(_navMesh);
	_debuggerNavMesh->setEdgeSize(0.2f);
	_debuggerNavMesh->update();

	//Create a bounding shape that avoids characters going through geometry edges. 
	EdgeTable& eTable = _navMesh->getEdgeTable();
	for (EdgeTable::iterator it = eTable.begin(); it != eTable.end(); it++) {
		if (!(it->second.isPortal())) {
			Vec3D v1 = _navMesh->getVertex(it->second.v1).getPosition();
			Vec3D v2 = _navMesh->getVertex(it->second.v2).getPosition();
			addBoundingEdge(v1, v2);
		}
	}
	_characterLocator = new CharacterLocator(_navMesh);
}

void SceneGame::resetTimer() {
	_timer.reset();
	_timePrevAI = _timer.getMilliseconds();
	_timeCurrAI = _timer.getMilliseconds();
}

/**************************************************************************************************************/
/* CLASS FUNCTIONS */
/**************************************************************************************************************/
void SceneGame::addBoundingEdge(Vec3D& v1, Vec3D& v2) {
	//Afegeix una nova restricci� a la boundingShape
	btConvexHullShape* shape = new btConvexHullShape();
	float extrusionDist = 3.0f;
	shape->addPoint(btVector3(v1.getX(), v1.getY(), v1.getZ()));					//v1
	shape->addPoint(btVector3(v2.getX(), v2.getY(), v2.getZ()));					//v2
	shape->addPoint(btVector3(v2.getX(), v2.getY() + extrusionDist, v2.getZ()));	//v2 extruded
	shape->addPoint(btVector3(v1.getX(), v1.getY() + extrusionDist, v1.getZ()));	//v1 extruded
	Core::getManager<InterfacePhysicsManager>()->createRigidBody(shape, 0, COL_SCENE_BOUND, sceneBoundCollidesWith);
}

BaseCharacter* SceneGame::loadCharacter(const Ogre::String& meshFile, float radius, float height, float mass, const Ogre::Vector3& pos, const Ogre::Vector3& scale) {
	//Configure Ogre Character Attributes
	BaseCharConfigurator baseCFG;
	baseCFG.isInstanced = false;
	baseCFG.ent = _sceneMgr->createEntity("charCylinder.mesh");
	baseCFG.node = _sceneMgr->getRootSceneNode()->createChildSceneNode();
	baseCFG.node->setPosition(pos + Ogre::Vector3(0.0f, PHYSICS_OFFSET_Y, 0.0f));
	baseCFG.node->setScale(scale);
	baseCFG.pos = pos;
	baseCFG.navMesh = _navMesh;
	baseCFG.height = height;
	baseCFG.radius = radius;

	//Configure Bullet Character Attributes
	CollisionShapeCapsule* shape = new CollisionShapeCapsule(radius, std::max(0.0f, height - 2.0f * radius));
	RigidBody* rigidBody = _physicsManager->createRigidBody(shape, mass, Vector3(pos.x, pos.y, pos.z), COL_CHAR, charCollidesWith);

	//GhostObject* ghost = _physicsManager->createGhostObject(shape, COL_CHAR, charCollidesWith);
	//rigidBody->setGhostObject(ghost);

	//Configure Open Steer Attributes
	OpenSteerCharConfigurator openSteerCFG;
	openSteerCFG.pdb = _proximityDB;

	BaseCharacter* character = new AutonomousCharacter(baseCFG, rigidBody, openSteerCFG);
	_charList.push_back(character);
		
	return character;
}

/**************************************************************************************************************/
/* SCENE UPDATE */
/**************************************************************************************************************/
void SceneGame::update() {
	Core::getManager<InterfaceCameraManager>()->update();
	Core::getManager<InterfaceInputManager>()->update();
	Core::getManager<InterfaceGUIManager>()->update();
	processMouseEvents();
	processKeyboardEvents();
	if (!_pause) {
		Core::getManager<InterfacePhysicsManager>()->update();	
	}
	updateLogic();
	Core::getManager<InterfaceRenderManager>()->update();
}

void SceneGame::locateCharacter(BaseCharacter* character) {
	//Localitza a quina cel�la est� el personatge
	int cellID = _characterLocator->locateCharacter(character);
	character->setCurrentCellID(cellID);
}

void SceneGame::updateLogic() {
	_timeCurrAI = _timer.getMilliseconds();
	unsigned long elapsedMS = _timeCurrAI - _timePrevAI;
	if (elapsedMS >= TIME_TO_UPDATE_AI) {
		if (!_pause) {
			BaseCharacter* character;
			for (CharList::iterator it = _charList.begin(); it != _charList.end(); it++) {
				character = *it;
				locateCharacter(character);
				character->updateLogic(float(elapsedMS) / 1000.0f);
			}		
		}
		_timePrevAI = _timeCurrAI;
	}
}

/**************************************************************************************************************/
/* INPUT PROCESSING */
/**************************************************************************************************************/
void SceneGame::processKeyboardEvents() {
	InterfaceInputManager* inputManager = Core::getManager<InterfaceInputManager>();
	InterfaceGUIManager* guiManager = Core::getManager<InterfaceGUIManager>();
	if (inputManager->isTriggeredKey(OIS::KC_P)) pauseGame();
	if (inputManager->isTriggeredKey(OIS::KC_F12)) {
		int currentGUIMode = Core::getManager<InterfaceGUIManager>()->getGUIMode();
		int newGUIMode = (currentGUIMode + 1) % InterfaceGUIManager::GUI_NUM_MODES;
		if (newGUIMode == InterfaceGUIManager::GUI_MODE_SHOW_ALL) guiManager->showAll();
		else if (newGUIMode == InterfaceGUIManager::GUI_MODE_HIDE_ALL_BUT_MOUSE) guiManager->hideAllButMouse();
		else if (newGUIMode == InterfaceGUIManager::GUI_MODE_HIDE_ALL) guiManager->hideAll();
	}
	Scene::processKeyboardEvents();
}

void SceneGame::pauseGame() {
	_pause = !_pause;
	if (!_pause) resetTimer();
}

void SceneGame::processMouseEvents() {
	if (Core::getManager<InterfaceInputManager>()->isTriggeredMouseButton(OIS::MB_Left)) {
		selectCharacter();
	}
}

void SceneGame::selectCharacter() {
	// Setup the ray scene query
	int w, h, mX, mY;
	Core::getManager<InterfaceInputManager>()->getClippingAreaSize(w, h);
	Core::getManager<InterfaceInputManager>()->getMouseCoords(mX, mY);
	Camera* camera = Core::getManager<InterfaceRenderManager>()->getMainCamera();
	Ogre::Ray mouseRay = camera->getCameraToViewportRay(float(mX) / float(w), float(mY) / float(h));

	//Creo un raig que t� com a punt inicial la posici� de la c�mera i que passa pel punt
	//que s'ha clicat amb el ratol�
	
	btVector3 from = BtOgre::Convert::toBullet(mouseRay.getOrigin());		//Punt inicial del raig
	btVector3 to = BtOgre::Convert::toBullet(mouseRay.getPoint(10000.0f));	//Punt final del raig

	RayCastResult result;
	_physicsManager->rayCast(from, to, result, COL_CHAR);
	
	if (result.hit) {
		//El raig ha impactat contra alguna cosa. Cal veure de qu� es tracta
		/*CollisionObject* cObject = result.collisionObject;
		int objectGroup = cObject->getBroadphaseHandle()->m_collisionFilterGroup;
		if (objectGroup == COL_CHAR) {
			//El raig ha impactat amb un ninja
			_selectedChar = ((AutonomousCharacter*)(cObject));
		}*/
	}
}