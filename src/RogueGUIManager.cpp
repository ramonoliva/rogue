#include "RogueGUIManager.h"
#include "RogueCore.h"
#include "RogueGlobalVars.h"

#include <climits>

using namespace Rogue;
using namespace ATL;

/****************************************************************************************/
/* CREATION AND DESTRUCTION */
/****************************************************************************************/
GUIManager::GUIManager() {
	Keyboard* keyboard = Core::getManager<InterfaceInputManager>()->getKeyboard();
	Mouse* mouse = Core::getManager<InterfaceInputManager>()->getMouse();
	OgreBites::InputContext input;
	input.mAccelerometer = NULL;
	input.mKeyboard = keyboard;
	input.mMouse = mouse;
	input.mMultiTouch = NULL;

	RenderWindow* renderWindow = Core::getManager<InterfaceRenderManager>()->getRenderWindow();
	_trayMgr = new OgreBites::SdkTrayManager("InterfaceName", renderWindow, input, NULL);
	Ogre::FontManager::getSingleton().getByName("SdkTrays/Caption")->load();
	Ogre::FontManager::getSingleton().getByName("SdkTrays/Value")->load();
	//Ogre::FontManager::getSingleton().getByName("CoreFont")->load();
		
	setCursorVisible(true);
	_trayMgr->showFrameStats(OgreBites::TL_BOTTOMLEFT);

	Core::getManager<InterfaceRenderManager>()->addFrameListener(this);
	Core::getManager<InterfaceInputManager>()->addInputListener(this);
	
	//Create the menu bar
	_menuItemID = 0;
	createMenu();
	
	_guiMode = GUI_MODE_SHOW_ALL;
}

GUIManager::~GUIManager() {
	destroyAllGUI();
	Core::getManager<InterfaceRenderManager>()->removeFrameListener(this);
	Core::getManager<InterfaceInputManager>()->removeInputListener(this);
	delete _trayMgr;
	DestroyMenu(_menuHandler);
}

void GUIManager::createMenu() {
	RenderWindow* renderWindow = Core::getManager<InterfaceRenderManager>()->getRenderWindow();
	renderWindow->getCustomAttribute("WINDOW", &_windowHandler);
	_menuHandler = CreateMenu();
	SetMenu(_windowHandler, _menuHandler);
	registerMenu(_menuHandler);
}

int GUIManager::registerMenu(HMENU menu) {
	_menuItemTable[_menuItemID] = menu;
	return _menuItemID++;
}

void GUIManager::destroyGUI(const std::string& guiName) {
	if (_guiMap.find(guiName) == _guiMap.end()) return;
	delete _guiMap[guiName];
	_guiMap.erase(guiName);
}

void GUIManager::destroyAllGUI() {
	for (GUIMap::iterator it = _guiMap.begin(); it != _guiMap.end(); it++) {
		delete it->second;
	}
	_guiMap.clear();
}

/*****************************************************************************************************************/
/* GET AND SET */
/*****************************************************************************************************************/
GUIBase* GUIManager::getGUI(const std::string& guiName) {
	if (_guiMap.find(guiName) == _guiMap.end()) return NULL;	//There is no GUI with guiName
	return _guiMap[guiName];
}

int GUIManager::getGUIMode() {
	return _guiMode;
}

HMENU GUIManager::getMenu(int id) {
	MenuItemTable::iterator itFound = _menuItemTable.find(id);
	if (itFound == _menuItemTable.end()) {
		return NULL;
	}
	return _menuItemTable[id];
}

/*****************************************************************************************************************/
/* CLASS FUNCTIONS */
/*****************************************************************************************************************/
void GUIManager::hideAll() {
	_trayMgr->hideAll();
	_guiMode = GUI_MODE_HIDE_ALL;
}

void GUIManager::hideAllButMouse() {
	hideAll();
	_trayMgr->showCursor();
	_guiMode = GUI_MODE_HIDE_ALL_BUT_MOUSE;
}

void GUIManager::showAll() {
	_trayMgr->showAll();
	_guiMode = GUI_MODE_SHOW_ALL;
}

void GUIManager::setCursorVisible(bool v) {
	if (!G_MOUSE_EXCLUSIVE) v = false;
	if (v == false) _trayMgr->hideCursor();
	else _trayMgr->showCursor();
}

void GUIManager::showFrameStats(bool show) {
	if (show) _trayMgr->showFrameStats(OgreBites::TL_BOTTOMLEFT);
	else _trayMgr->hideFrameStats();
}

/*****************************************************************************************************************/
/* FILE DIALOGS */
/*****************************************************************************************************************/
String GUIManager::openFile(FileTypeList& typeList, const std::wstring& defaultFileName, const std::wstring& openButton) {
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
	if (FAILED(hr)) return "";

	CComPtr<IFileDialog> dialogWindow;
	hr = dialogWindow.CoCreateInstance(__uuidof(FileOpenDialog));
	if (FAILED(hr)) return "";

	String st = fileDialog(dialogWindow, typeList, L"Open File", defaultFileName, openButton);
	
	CoUninitialize();

	return st;

	//char* buffer;

	//HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
	//if (SUCCEEDED(hr)) {
	//	IFileOpenDialog *pFileOpen;

	//	hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL, IID_IFileOpenDialog, reinterpret_cast<void**>(&pFileOpen));

	//	if (SUCCEEDED(hr)) {
	//		hr = pFileOpen->Show(NULL);

	//		if (SUCCEEDED(hr)) {
	//			IShellItem *pItem;
	//			hr = pFileOpen->GetResult(&pItem);
	//			if (SUCCEEDED(hr)) {
	//				PWSTR pszFilePath;
	//				hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);

	//				if (SUCCEEDED(hr)) {
	//					//MessageBox(NULL, pszFilePath, L"File Path", MB_OK);
	//					buffer = wideCharToChar(pszFilePath);
	//					CoTaskMemFree(pszFilePath);
	//				}
	//				pItem->Release();
	//			}
	//		}
	//		pFileOpen->Release();
	//	}
	//	CoUninitialize();
	//}

	//if (SUCCEEDED(hr)) return (const_cast<char*>(buffer));
	//return "";
}

String GUIManager::saveFile(FileTypeList& typeList, const std::wstring& defaultFileName, const std::wstring& openButton) {
	CComPtr<IFileDialog> dialogWindow;
	HRESULT hr = dialogWindow.CoCreateInstance(__uuidof(FileSaveDialog));
	if (FAILED(hr)) return "";

	return fileDialog(dialogWindow, typeList, L"Save File", defaultFileName, openButton);
}

String GUIManager::fileDialog(CComPtr<IFileDialog> dialogWindow, FileTypeList& typeList, const std::wstring& title, const std::wstring& defaultFileName, const std::wstring& openButton) {
	// Set the dialog's caption text, file types, Save button label,
	// default file name, and default extension.

	//The main window has lost the focus, so notify to the input manager to release all the keys that were marked as pressed
	InterfaceInputManager* inputManager = Core::getManager<InterfaceInputManager>();
	inputManager->releaseAllKeys();

	//Convert the data structures that represents the types
	const size_t NUM_TYPES = typeList.size() + 1;
	COMDLG_FILTERSPEC* fileTypes = new COMDLG_FILTERSPEC[NUM_TYPES];
	for (size_t i = 0; i < NUM_TYPES; i++) {
		if (i == (NUM_TYPES - 1)) {
			fileTypes[i].pszName = L"All Files";
			fileTypes[i].pszSpec = L"*.*";
		}
		else {
			fileTypes[i].pszName = typeList[i].first.c_str();
			fileTypes[i].pszSpec = typeList[i].second.c_str();
		}
	}

	dialogWindow->SetFileTypes(NUM_TYPES, fileTypes);
	if (title != L"") dialogWindow->SetTitle(title.c_str());
	if (openButton != L"") dialogWindow->SetOkButtonLabel(openButton.c_str());
	if (defaultFileName != L"") dialogWindow->SetFileName(defaultFileName.c_str());
	if (fileTypes != NULL) dialogWindow->SetDefaultExtension(fileTypes[0].pszSpec);

	//Show the dialog
	HRESULT hr = dialogWindow->Show(NULL);
	if (FAILED(hr)) return "";

	CComPtr<IShellItem> pItem;
	hr = dialogWindow->GetResult(&pItem);
	if (FAILED(hr)) return "";
	
	wchar_t* pszFilePath;
	hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);
	char* buffer = wideCharToChar(pszFilePath);
					  
	CoTaskMemFree(pszFilePath);
	delete[] fileTypes;

	String st(const_cast<char*>(buffer));
	return st;
}

/*****************************************************************************************************************/
/* MENU MANAGEMENT */
/*****************************************************************************************************************/
int GUIManager::addMenuItem(const String& itemName, bool isSubMenu, int parentID) {
	HMENU parent = getMenu(parentID);
	if (parent == NULL) return - 1;
	
	MENUITEMINFO itemInfo;
	itemInfo.cbSize = sizeof(MENUITEMINFO);
	itemInfo.fMask = MIIM_STRING | MIIM_DATA;
	itemInfo.fType = MFT_STRING;
	itemInfo.dwTypeData = const_cast<LPSTR>(itemName.c_str());
	itemInfo.cch = itemName.length();

	int result;
	if (isSubMenu) {
		HMENU subMenu = CreateMenu();
		itemInfo.fMask |= MIIM_SUBMENU;
		itemInfo.hSubMenu = subMenu;
		result = registerMenu(subMenu);
	}
	else result = parentID;
	
	InsertMenuItem(parent, UINT_MAX, TRUE, &itemInfo);
	DrawMenuBar(_windowHandler);

	return result;
}

/*****************************************************************************************************************/
/* INPUT MANAGEMENT */
/*****************************************************************************************************************/
void GUIManager::injectMouseMove(const MouseEvent &arg) {
	_trayMgr->injectMouseMove(arg);
}

void GUIManager::injectMouseDown(const MouseEvent &arg, MouseButtonID id) {
	_trayMgr->injectMouseDown(arg, id);
}

void GUIManager::injectMouseUp(const MouseEvent &arg, MouseButtonID id) {
	_trayMgr->injectMouseUp(arg, id);
}

/****************************************************************************************/
/* FRAME LISTENER																		*/
/****************************************************************************************/
bool GUIManager::frameStarted (const FrameEvent &evt) {
	_trayMgr->frameRenderingQueued(evt);
	return true;
}

bool GUIManager::frameRenderingQueued(const FrameEvent& evt) {
	return true;
}

bool GUIManager::frameEnded(const FrameEvent &evt) {
	return true;
}

/****************************************************************************************/
/* UPDATE	*/
/****************************************************************************************/
void GUIManager::update() {
	for (GUIMap::iterator it = _guiMap.begin(); it != _guiMap.end(); it++) {
		it->second->update();
	}
}