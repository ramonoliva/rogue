#include "RogueDebuggerBase.h"
#include "RogueCore.h"

using namespace Rogue;

#pragma region CREATION AND DESTRUCTION

DebuggerBase::DebuggerBase() {
	_renderManager = Core::getManager<InterfaceRenderManager>();
	_sceneNode = _renderManager->createSceneNode();
	_isVisible = true;
}

DebuggerBase::~DebuggerBase() {

}

#pragma endregion

#pragma region GET AND SET

void DebuggerBase::setVisible(bool v) {
	_isVisible = v;
}

bool DebuggerBase::isVisible() {
	return _isVisible;
}

SceneNode* DebuggerBase::getSceneNode() {
	return _sceneNode;
}

#pragma endregion