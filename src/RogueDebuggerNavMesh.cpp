#include "RogueDebuggerNavMesh.h"

using namespace Rogue;
using namespace NEOGEN;

#pragma region CREATION AND DESTRUCTION

DebuggerNavMesh::DebuggerNavMesh(NavMesh* navMesh) : DebuggerBase() {
	_navMesh = navMesh;
	
	_debuggerObstacleEdges = new DebuggerPolyLine();
	_debuggerObstacleEdges->setColor(0xffff0000);

	_debuggerPortalEdges = new DebuggerPolyLine();
	_debuggerPortalEdges->setColor(0xffffffff);
}

DebuggerNavMesh::~DebuggerNavMesh() {
	clear();
	delete _debuggerObstacleEdges;
	delete _debuggerPortalEdges;
	_navMesh = NULL;
}

void DebuggerNavMesh::clear() {
	_debuggerObstacleEdges->clear();
	_debuggerPortalEdges->clear();
}

#pragma endregion

#pragma region GET AND SET

void DebuggerNavMesh::setNavMesh(NavMesh* navMesh) {
	_navMesh = navMesh;
}

NavMesh* DebuggerNavMesh::getNavMesh() {
	return _navMesh;
}

void DebuggerNavMesh::setVisible(bool v) {
	DebuggerBase::setVisible(v);
	_debuggerObstacleEdges->setVisible(v);
	_debuggerPortalEdges->setVisible(v);
}

void DebuggerNavMesh::setEdgeSize(float size) {
	_debuggerObstacleEdges->setSize(size);
	_debuggerPortalEdges->setSize(size);
}

float DebuggerNavMesh::getEdgeSize() {
	return _debuggerObstacleEdges->getSize();
}

void DebuggerNavMesh::setEdgeOffset(float x, float y, float z) {
	_debuggerObstacleEdges->setVertexOffset(x, y, z);
	_debuggerPortalEdges->setVertexOffset(x, y, z);
}

void DebuggerNavMesh::getEdgeOffset(float& x, float& y, float& z) {
	_debuggerObstacleEdges->getVertexOffset(x, y, z);
}

#pragma endregion

#pragma region UPDATE

void DebuggerNavMesh::update() {

	if (!_navMesh) return;

	std::set<int> drawnPortals;

	_debuggerObstacleEdges->clear();
	_debuggerPortalEdges->clear();
	DebuggerPolyLine* debugger;

	EdgeTable& eTable = _navMesh->getEdgeTable();
	Vec3D v1, v2;
	int v1ID, v2ID;
	for (EdgeTable::iterator it = eTable.begin(); it != eTable.end(); it++) {
		Edge& e = it->second;
		if (drawnPortals.find(e.id) != drawnPortals.end()) continue;

		v1 = _navMesh->getVertex(e.v1).getPosition();
		v2 = _navMesh->getVertex(e.v2).getPosition();
		if (it->second.isPortal()) {
			debugger = _debuggerPortalEdges;
			drawnPortals.insert(e.id);
			drawnPortals.insert(e.opposite);
		}
		else {
			debugger = _debuggerObstacleEdges;
		}
		v1ID = debugger->addVertex(Ogre::Vector3(v1.getX(), v1.getY(), v1.getZ()));
		v2ID = debugger->addVertex(Ogre::Vector3(v2.getX(), v2.getY(), v2.getZ()));
		debugger->addSegment(v1ID, v2ID);
	}

	_debuggerObstacleEdges->update();
	_debuggerPortalEdges->update();
}

#pragma endregion