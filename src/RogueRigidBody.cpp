#include "RogueRigidBody.h"
#include "RogueGameObject.h"
#include "RogueCore.h"

#include "RogueBaseCharacter.h"

using namespace Rogue;

/*************************************************************************/
/* CREATION AND DESTRUCTION */
/*************************************************************************/
RigidBody::RigidBody(float mass, MotionState* state, CollisionShape* shape, const Vector3& localInertia) :
btRigidBody(mass, state, shape, localInertia), 
GameComponent()
{
	_character = NULL;
	_ghostObject = NULL;
	setActivationState(DISABLE_DEACTIVATION);
}

RigidBody::~RigidBody() {
	Core::getManager<InterfacePhysicsManager>()->removeRigidBody(this);
}

void RigidBody::setKinematic(bool k) {
	if (k) setCollisionFlags(getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
	else setCollisionFlags(getCollisionFlags() & ~btCollisionObject::CF_KINEMATIC_OBJECT);
}

/*************************************************************************/
/* GET AND SET */
/*************************************************************************/
void RigidBody::setCharacter(BaseCharacter* character) {
	_character = character;
}

BaseCharacter* RigidBody::getCharacter() {
	return _character;
}

void RigidBody::onThisIsJustAPatch() {
	if (_character) {
		_character->clearOverlappingCells();
	}
}

void RigidBody::setScale(Vector3& scale) {
	btCollisionShape* cShape = getCollisionShape();
	cShape->setLocalScaling(scale);
}

void RigidBody::setGhostObject(GhostObject* g) {
	_ghostObject = g;
}

GhostObject* RigidBody::getGhostObject() {
	return _ghostObject;
}

/*************************************************************************/
/* UPDATE */
/*************************************************************************/
void RigidBody::update() {
	//Get the current position of the rigid body
	MotionState* state = getMotionState();
	btTransform xform;
	state->getWorldTransform(xform);
	btVector3 pos = xform.getOrigin();	

	if (_gameObject) {
		//Modify accordingly the position of the transform of the GameObject that this
		//rigid body is attached to
		Transform& goXform = _gameObject->getTransform();
		goXform.setOrigin(pos);
	}

	if (_ghostObject) {
		_ghostObject->setWorldTransform(xform);
	}
}