#include "RogueDebuggerCharacter.h"

using namespace Rogue;
using namespace NEOGEN;

static const float DEFAULT_SAMPLE_FREQUENCY = 0.1f;
static const int MAX_SAMPLE_POINTS = 1000;

//Default Materials
static const std::string DEFAULT_MATERIAL_TRAJECTORY = "blank";
static const std::string DEFAULT_MATERIAL_WAY_POINT = "matDotted";
static const std::string DEFAULT_MATERIAL_TARGET_POINT = "blank";
static const std::string DEFAULT_MATERIAL_SHRINKED_PORTAL = "Blue";

#pragma region CREATION AND DESTRUCTION

DebuggerCharacter::DebuggerCharacter(BaseCharacter* c) : DebuggerBase() {
	_char = c;

	//Initialize the transform debug information
	_debuggerTransformForward = new DebuggerPolyLine();
	_debuggerTransformForward->setColor(0x0000ff);
	_debuggerTransformForward->addVertex(Ogre::Vector3(0,0,0));
	_debuggerTransformForward->addVertex(Ogre::Vector3(0,0,0));
	_debuggerTransformForward->addSegment(0, 1);

	_debuggerTransformUp = new DebuggerPolyLine();
	_debuggerTransformUp->setColor(0x00ff00);
	_debuggerTransformUp->addVertex(Ogre::Vector3(0,0,0));
	_debuggerTransformUp->addVertex(Ogre::Vector3(0,0,0));
	_debuggerTransformUp->addSegment(0, 1);

	_debuggerTransformRight = new DebuggerPolyLine();
	_debuggerTransformRight->setColor(0xff0000);
	_debuggerTransformRight->addVertex(Ogre::Vector3(0,0,0));
	_debuggerTransformRight->addVertex(Ogre::Vector3(0,0,0));
	_debuggerTransformRight->addSegment(0, 1);

	//Initialize waypoint information
	_debuggerWayPoint = new DebuggerPoint();
	_debuggerCharToWayPoint = new DebuggerPolyLine();
	_debuggerCharToWayPoint->setPattern(0xF0F0);
	_debuggerCharToWayPoint->addVertex(Ogre::Vector3(0,0,0));
	_debuggerCharToWayPoint->addVertex(Ogre::Vector3(0,0,0));
	_debuggerCharToWayPoint->addSegment(0, 1);
	
	//Initialize shrinked portal information
	_debuggerShrinkedPortal = new DebuggerPolyLine();
	_debuggerShrinkedPortal->setColor(0xff0000ff);
	_debuggerShrinkedPortal->setSize(0.2f);
	_debuggerShrinkedPortal->setVertexOffset(0.0f, 0.15f, 0.0f);
	_debuggerShrinkedPortal->addVertex(Ogre::Vector3(0,0,0));
	_debuggerShrinkedPortal->addVertex(Ogre::Vector3(0,0,0));
	_debuggerShrinkedPortal->addSegment(0, 1);
	
	//Initialize samplePoints information
	_manualSamplePoints = NULL;
	_nodeSamplePoints = NULL;
	_samplePointFirstIndex = 0;
	_timer.reset();
	_timeLastSample = _timer.getMilliseconds();
	_sampleFrequency = DEFAULT_SAMPLE_FREQUENCY;

	//Initialize materials
	_materialTrajectory = DEFAULT_MATERIAL_TRAJECTORY;
	_materialWayPoint = DEFAULT_MATERIAL_WAY_POINT;
	_materialTargetPoint = DEFAULT_MATERIAL_TARGET_POINT;
	_materialShrinkedPortal = DEFAULT_MATERIAL_SHRINKED_PORTAL;
}

DebuggerCharacter::~DebuggerCharacter() {
	clear();
	delete _debuggerTransformForward;
	delete _debuggerWayPoint;
	delete _debuggerCharToWayPoint;
}

void DebuggerCharacter::clearSamplePoints() {
	_samplePoints.clear();
	_samplePointFirstIndex = 0;
}

void DebuggerCharacter::clear() {
	//Destroy waypoint information
	_debuggerTransformForward->clear();
	_debuggerWayPoint->clear();
	_debuggerCharToWayPoint->clear();
	
	//Destroy shrinked portal information
	_debuggerShrinkedPortal->clear();

	//Destroy trajectory information
	OgreUtils::destroySceneNode(_nodeSamplePoints);
	_nodeSamplePoints = NULL;
	_manualSamplePoints = NULL;
	clearSamplePoints();
}

#pragma endregion

/********************************************************************************/
/* GET AND SET */
/********************************************************************************/
void DebuggerCharacter::setSampleFrequency(float f) {
	_sampleFrequency = f;
}

void DebuggerCharacter::setMaterialTrajectory(const std::string& matName) {
	_materialTrajectory = matName;
}

BaseCharacter* DebuggerCharacter::getCharacter() {
	return _char;
}

void DebuggerCharacter::setVisible(bool v) {
	DebuggerBase::setVisible(v);
	setVisibleCharacter(v);
	//setVisibleShrinkedPortal(v);
	setVisibleCharToWayPoint(v);
	setVisibleTrajectory(v);
	setVisibleDebugTransform(v);
}

void DebuggerCharacter::setVisibleDebugTransform(bool v) {
	_debuggerTransformForward->setVisible(v);
	_debuggerTransformUp->setVisible(v);
	_debuggerTransformRight->setVisible(v);
}

void DebuggerCharacter::setVisibleCharacter(bool v) {
	_char->setVisible(v);
}

void DebuggerCharacter::setVisibleTargetPoint(bool v) {

}

void DebuggerCharacter::setVisibleShrinkedPortal(bool v) {
	_debuggerShrinkedPortal->setVisible(v);
}

void DebuggerCharacter::setVisibleCharToWayPoint(bool v) {
	_debuggerTransformForward->setVisible(v);
	_debuggerWayPoint->setVisible(v);
	_debuggerCharToWayPoint->setVisible(v);
}

void DebuggerCharacter::setVisibleTrajectory(bool v) {
	if (_nodeSamplePoints != NULL) _nodeSamplePoints->setVisible(v);
}

/********************************************************************************/
/* UPDATE */
/********************************************************************************/
void DebuggerCharacter::update() {
	updateDebugTransform();
	updateTrajectory();
	updateWayPoint();
	updateTargetPoint();
	updateShrinkedPortal();
}

void DebuggerCharacter::updateDebugTransform() {
	Ogre::Vector3 charPos = _char->getPosition();
	float scale = 3.0f;

	//Update the forward vector
	btVector3 forward = Converter::fromOpenSteerToBullet(_char->forward());
	forward = forward.normalized() * scale;
	_debuggerTransformForward->setVertex(0, charPos);
	_debuggerTransformForward->setVertex(1, charPos + Ogre::Vector3(forward.getX(), forward.getY(), forward.getZ()));
	_debuggerTransformForward->update();

	//Update the up vector
	//btVector3 up = _char->getUpDir();
	btVector3 up = Converter::fromOpenSteerToBullet(_char->up());
	up = up.normalized() * scale;
	_debuggerTransformUp->setVertex(0, charPos);
	_debuggerTransformUp->setVertex(1, charPos + Ogre::Vector3(up.getX(), up.getY(), up.getZ()));
	_debuggerTransformUp->update();

	//Update the right vector
	//btVector3 right = _char->getStrafeDir();
	btVector3 right = Converter::fromOpenSteerToBullet(_char->side());
	right = right.normalized() * scale;
	_debuggerTransformRight->setVertex(0, charPos);
	_debuggerTransformRight->setVertex(1, charPos + Ogre::Vector3(right.getX(), right.getY(), right.getZ()));
	_debuggerTransformRight->update();
}

void DebuggerCharacter::updateTrajectory() {
	//Determine if a new position sample is required
	unsigned long now = _timer.getMilliseconds();
	float elapsedSeconds = float(now - _timeLastSample) / 1000.0f;
	if (RealMath::greaterOrEqual(elapsedSeconds, _sampleFrequency)) {
		saveCurrentPosition();
		_timeLastSample = now;
	}

	//Draw the trajectory
	if (_samplePoints.size() < 2) return;
	if (_manualSamplePoints == NULL) {
		_manualSamplePoints = _renderManager->createManualObject();
		_nodeSamplePoints = _renderManager->createSceneNode();
		_nodeSamplePoints->attachObject(_manualSamplePoints);
		_manualSamplePoints->begin(_materialTrajectory, Ogre::RenderOperation::OT_LINE_STRIP);
	}
	else {
		_manualSamplePoints->beginUpdate(0);
	}
	//Draw a polyline that shows char's trajectory
	size_t i = _samplePointFirstIndex;
	Ogre::Vector3 pos;
	float h = _char->getHeight();
	float trajectoryOffsetY = 0.1f;
	do {
		pos = _samplePoints[i];
		pos.y = (pos.y - (h / 2.0f)) + trajectoryOffsetY;
		_manualSamplePoints->position(pos);
		i = (i + 1) % _samplePoints.size();
	} while (i != _samplePointFirstIndex);
	//End with the current _char position
	pos = _char->getPosition();
	pos.y = (pos.y - (h / 2.0f)) + trajectoryOffsetY;
	_manualSamplePoints->position(pos);
	_manualSamplePoints->end();
}

void DebuggerCharacter::updateWayPoint() {
	Vec3D wp;
	_char->getWayPoint(wp);
	Ogre::Vector3 charPos = _char->getPosition();
	Ogre::Vector3 begin = Ogre::Vector3(charPos.x, wp.getY() + 0.2f, charPos.z);
	Ogre::Vector3 end = Ogre::Vector3(wp.getX(), wp.getY() + 0.2f, wp.getZ());

	_debuggerCharToWayPoint->setVertex(0, begin);
	_debuggerCharToWayPoint->setVertex(1, end);
	_debuggerCharToWayPoint->update();

	_debuggerWayPoint->setPosition(wp);
	_debuggerWayPoint->draw(wp, "DarkGrey");
}

void DebuggerCharacter::updateTargetPoint() {
	
}

void DebuggerCharacter::updateShrinkedPortal() {
	//Draw a line that represents the portal that the _char has to cross
	_char->getShrinkedPortal(_portalV1, _portalV2);
	_debuggerShrinkedPortal->setVertex(0, Ogre::Vector3(_portalV1.getX(), _portalV1.getY(), _portalV1.getZ()));
	_debuggerShrinkedPortal->setVertex(1, Ogre::Vector3(_portalV2.getX(), _portalV2.getY(), _portalV2.getZ()));
	_debuggerShrinkedPortal->update();
}

void DebuggerCharacter::saveCurrentPosition() {
	Ogre::Vector3 pos = _char->getPosition();
	if (_samplePoints.size() < MAX_SAMPLE_POINTS) _samplePoints.push_back(pos);
	else {
		//The vector has reached its maximum capacity. Replace the oldest sample point
		//by the new one. 
		_samplePoints[_samplePointFirstIndex] = pos;
		_samplePointFirstIndex = (_samplePointFirstIndex + 1) % MAX_SAMPLE_POINTS;
	}
}