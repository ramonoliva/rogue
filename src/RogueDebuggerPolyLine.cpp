#include "RogueDebuggerPolyLine.h"
#include "RogueCore.h"

using namespace Rogue;

#pragma region STATIC AND CONSTANTS INITIALIZATION

int DebuggerPolyLine::POLY_LINE_COUNTER = 0;
static const size_t PATTERN_TEXTURE_WIDTH = 16;
static const float MAX_SIZE = 1.0f;

#pragma endregion

#pragma region CREATION AND DESTRUCTION

DebuggerPolyLine::DebuggerPolyLine() : DebuggerBase() {
	_id = POLY_LINE_COUNTER++;
	_color = 0xff000000;
	_pattern = 0xffff;
	_materialName = getPrefix() + "_Material";
	_patternTexName = getPrefix() + "_Pattern";

	setSize(0.0f);
	setVertexOffset(0,0,0);

	createManualObject();
	createPatternTexture();
	createMaterial();
}

DebuggerPolyLine::~DebuggerPolyLine() {
	//if (_manual) OgreUtils::destroySceneNode(_manual->getParentSceneNode());
}

void DebuggerPolyLine::clear() {
	_vertexList.clear();
	_segments.clear();
	_manual->clear();
}

void DebuggerPolyLine::createPatternTexture() {
	TexturePtr tex = _renderManager->createTexture1D(_patternTexName, PATTERN_TEXTURE_WIDTH, Ogre::PF_A8R8G8B8, Ogre::TU_DEFAULT);

	//Get the pixel buffer of the texture and lock it, so we can write it. 
	Ogre::HardwarePixelBufferSharedPtr pixelBuffer = tex->getBuffer();
	size_t *data = (size_t*)pixelBuffer->lock(Ogre::HardwareBuffer::HBL_DISCARD);
	unsigned short pMask = 0x8000;
	for (int i = 0; i < PATTERN_TEXTURE_WIDTH; i++) {
		if (pMask & _pattern) {
			//Visible. Set the alpha to 1 to ensure that will be shown
			data[i] = 0xff000000 | _color;
		}
		else {
			//Not visible. 
			data[i] = 0;
		}
		pMask = pMask >> 1;
	}
	pixelBuffer->unlock();
}

void DebuggerPolyLine::createMaterial() {
	// Make the material. It has no lighting and an alpha reject (so the clear dots are really clear).
	Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().create(_materialName, "General");
	Ogre::Pass* pass = mat->getTechnique(0)->getPass(0);
	pass->createTextureUnitState(_patternTexName);
	pass->setLightingEnabled(false);
	pass->setCullingMode(Ogre::CULL_NONE);
	pass->setAlphaRejectSettings(Ogre::CMPF_EQUAL, 255);
}

void DebuggerPolyLine::createManualObject() {
	_manual = _renderManager->createManualObject();
	_sceneNode->attachObject(_manual);
}

#pragma endregion

#pragma region GET AND SET

void DebuggerPolyLine::setSize(float size) {
	_size = std::max(0.0f, std::min(MAX_SIZE, size));
	if (_size <= 0.025f) _size = 0.0f;
}

float DebuggerPolyLine::getSize() {
	return _size;
}

void DebuggerPolyLine::setColor(size_t color) {
	if (_color != color) {
		_color = color;
		createPatternTexture();
	}
}

size_t DebuggerPolyLine::getColor() {
	return _color;
}

void DebuggerPolyLine::setPattern(unsigned short pattern) {
	if (_pattern != pattern) {
		_pattern = pattern;
		createPatternTexture();
	}
}

unsigned short DebuggerPolyLine::getPattern() {
	return _pattern;
}

int DebuggerPolyLine::addVertex(Ogre::Vector3& v) {
	_vertexList.push_back(v);
	return _vertexList.size() - 1;
}

bool DebuggerPolyLine::getVertex(size_t id, Ogre::Vector3& result) {
	bool valid = (id < _vertexList.size());
	result = (valid)? _vertexList[id] : Ogre::Vector3(0,0,0);
	
	return valid;
}

void DebuggerPolyLine::setVertex(size_t id, Ogre::Vector3& value) {
	if (id >= _vertexList.size()) return;

	_vertexList[id] = value;
}

void DebuggerPolyLine::addSegment(size_t v1, size_t v2) {
	if ((v1 >= _vertexList.size()) || (v2 >= _vertexList.size())) return;

	_segments.push_back(Segment(v1, v2));
}

void DebuggerPolyLine::setVisible(bool v) {
	if (!_manual) return;

	DebuggerBase::setVisible(v);
	_manual->setVisible(v);
}

void DebuggerPolyLine::setVertexOffset(float x, float y, float z) {
	_vertexOffset = Ogre::Vector3(x, y, z);
}

void DebuggerPolyLine::getVertexOffset(float& x, float& y, float& z) {
	x = _vertexOffset.x;
	y = _vertexOffset.y;
	z = _vertexOffset.z;
}

std::string DebuggerPolyLine::getPrefix() {
	return "line" + std::to_string(_id) + "_";
}

#pragma endregion

#pragma region UPDATE

void DebuggerPolyLine::update() {
	if (_vertexList.size() < 2) return;

	if (!_manual) createManualObject();

	if (_size == 0) renderLine();
	else renderMesh();
}

void DebuggerPolyLine::renderLine() {
	_manual->clear();
	_manual->begin(_materialName, Ogre::RenderOperation::OT_LINE_LIST);

	for (SegmentList::iterator it = _segments.begin(); it != _segments.end(); it++) {
		Ogre::Vector3& v1 = _vertexList[it->first] + _vertexOffset;
		Ogre::Vector3& v2 = _vertexList[it->second] + _vertexOffset;

		float distance = v1.distance(v2);

		_manual->position(v1);
		_manual->textureCoord(0.0f);
		_manual->position(v2);
		_manual->textureCoord(distance);
	}

	_manual->end();
}

void DebuggerPolyLine::renderMesh() {
	_manual->clear();
	_manual->begin(_materialName, Ogre::RenderOperation::OT_TRIANGLE_LIST);

	for (SegmentList::iterator it = _segments.begin(); it != _segments.end(); it++) {
		Ogre::Vector3& v1 = _vertexList[it->first] + _vertexOffset;
		Ogre::Vector3& v2 = _vertexList[it->second] + _vertexOffset;

		Ogre::Vector3 forward = (v2 - v1).normalisedCopy();
		Ogre::Vector3 right = Ogre::Vector3::UNIT_Y.crossProduct(forward).normalisedCopy();
		Ogre::Vector3 up = right.crossProduct(forward).normalisedCopy();

		float halfSize = _size / 2.0f;

		Ogre::Vector3 t1 = v1 + (right * halfSize) + (up * halfSize);
		Ogre::Vector3 t3 = v1 - (right * halfSize) + (up * halfSize);
		Ogre::Vector3 t5 = v1 - (right * halfSize) - (up * halfSize);
		Ogre::Vector3 t7 = v1 + (right * halfSize) - (up * halfSize);

		Ogre::Vector3 t2 = v2 + (right * halfSize) + (up * halfSize);
		Ogre::Vector3 t4 = v2 - (right * halfSize) + (up * halfSize);
		Ogre::Vector3 t6 = v2 - (right * halfSize) - (up * halfSize);
		Ogre::Vector3 t8 = v2 + (right * halfSize) - (up * halfSize);

		float distance = v1.distance(v2);

		/////////////////////////////////////////////
		_manual->position(t1);
		_manual->textureCoord(0.0f);

		_manual->position(t2);
		_manual->textureCoord(distance);

		_manual->position(t3);
		_manual->textureCoord(0.0f);
		
		/////////////////////////////////////////////
		_manual->position(t2);
		_manual->textureCoord(distance);

		_manual->position(t3);
		_manual->textureCoord(0.0f);

		_manual->position(t4);
		_manual->textureCoord(distance);

		/////////////////////////////////////////////
		_manual->position(t3);
		_manual->textureCoord(0.0f);

		_manual->position(t4);
		_manual->textureCoord(distance);

		_manual->position(t5);
		_manual->textureCoord(0.0f);
		
		/////////////////////////////////////////////
		_manual->position(t4);
		_manual->textureCoord(distance);

		_manual->position(t5);
		_manual->textureCoord(0.0f);

		_manual->position(t6);
		_manual->textureCoord(distance);

		/////////////////////////////////////////////
		_manual->position(t5);
		_manual->textureCoord(0.0f);

		_manual->position(t6);
		_manual->textureCoord(distance);

		_manual->position(t7);
		_manual->textureCoord(0.0f);

		/////////////////////////////////////////////
		_manual->position(t6);
		_manual->textureCoord(distance);

		_manual->position(t7);
		_manual->textureCoord(0.0f);

		_manual->position(t8);
		_manual->textureCoord(distance);
		
		/////////////////////////////////////////////
		_manual->position(t7);
		_manual->textureCoord(0.0f);

		_manual->position(t8);
		_manual->textureCoord(distance);

		_manual->position(t1);
		_manual->textureCoord(0.0f);

		/////////////////////////////////////////////
		_manual->position(t8);
		_manual->textureCoord(distance);

		_manual->position(t1);
		_manual->textureCoord(0.0f);

		_manual->position(t2);
		_manual->textureCoord(distance);
	}

	_manual->end();
}

#pragma endregion