#include "GameMap.h"
#include "GameMapSerializer.h"
#include <OgreLogManager.h>
 
/*********************************************************************************************/
/* CREACIO I DESTRUCCIO */
/*********************************************************************************************/

using namespace Rogue;

GameMap::GameMap(Ogre::ResourceManager* creator, 
				 const Ogre::String &name, 
                 Ogre::ResourceHandle handle, 
				 const Ogre::String &group, bool isManual, 
                 Ogre::ManualResourceLoader *loader) : Ogre::Resource(creator, name, handle, group, isManual, loader) {
    /* If you were storing a pointer to an object, then you would set that pointer to NULL here. */

    /* For consistency with StringInterface, but we don't add any parameters here
    That's because the Resource implementation of StringInterface is to
    list all the options that need to be set before loading, of which 
    we have none as such. Full details can be set through scripts.
    */ 
    createParamDictionary("GameMap");
}
 
GameMap::~GameMap() {
}

/*********************************************************************************************/
/* GET I SET */
/*********************************************************************************************/
 
void GameMap::setGeometryMode(int mode) {
	geometryMode = mode;
}

int GameMap::getGeometryMode() {
	return geometryMode;
}

void GameMap::setGeometryFile(const Ogre::String& fName) {
	geometryFile = fName;
}

const Ogre::String& GameMap::getGeometryFile() const {
	return geometryFile;
}

void GameMap::setImgPreview(const Ogre::String& fName) {
	imgPreview = fName;
}

const Ogre::String& GameMap::getImgPreview() const {
	return imgPreview;
}

void GameMap::setNavMeshFile(const Ogre::String& fName) {
	navMeshFile = fName;
}

const Ogre::String& GameMap::getNavMeshFile() const {
	return navMeshFile;
}

void GameMap::setRedFlagPos(float x, float y, float z) {
	flagRedPosX = x;
	flagRedPosY = y;
	flagRedPosZ = z;
}

void GameMap::setBlueFlagPos(float x, float y, float z) {
	flagBluePosX = x;
	flagBluePosY = y;
	flagBluePosZ = z;
}

void GameMap::getRedFlagPos(float& x, float& y, float& z) {
	x = flagRedPosX;
	y = flagRedPosY;
	z = flagRedPosZ;
}

void GameMap::getBlueFlagPos(float& x, float& y, float& z) {
	x = flagBluePosX;
	y = flagBluePosY;
	z = flagBluePosZ;
}

/*********************************************************************************************/
/* OPERACIONS DE CLASSE */
/*********************************************************************************************/
 
// farm out to GameMapSerializer
void GameMap::loadImpl() {
    GameMapSerializer serializer;
    Ogre::DataStreamPtr stream = Ogre::ResourceGroupManager::getSingleton().openResource(mName, mGroup, true, this);
    serializer.importGameMap(stream, this);
}
 
void GameMap::unloadImpl() {
    /* If you were storing a pointer to an object, then you would check the pointer here,
    and if it is not NULL, you would destruct the object and set its pointer to NULL again.
    */
	geometryFile.clear();
	imgPreview.clear();
	navMeshFile.clear();
}
 
size_t GameMap::calculateSize() const {
	/*size_t size = 0;
	size += geometryFile.length();
	size += imgPreview.length();
	size += navMeshFile.length();*/
    return 0;
}
