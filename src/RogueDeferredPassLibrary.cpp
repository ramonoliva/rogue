#include "RogueDeferredPassLibrary.h"

using namespace Rogue;

/****************************************************************/
/* GBUFFER CONSTRUCTION */
/****************************************************************/
static const String MRT_MODEL_NAME = "mrtModel";
static const String MRT_0 = "sceneDiffuseTex";			//Diffuse color (RGB)
static const String MRT_1 = "sceneNormalDepthTex";		//Normal (RGB) and Depth (A)
static const String MRT_2 = "modelObjectSpacePosTex";	//Object Space Pos (RGB)

DPFillGBuffer::DPFillGBuffer() {
	_renderQuadVisible = false;
	_materialName = "matGenericShaded";

	//Create the Render Targets
	TexList tList;
	tList.push_back(MRT_0);
	tList.push_back(MRT_1);
	tList.push_back(MRT_2);

	size_t w, h;
	_renderManager->getRenderWindowSize(w, h);

	MultiRenderTarget* mrtScene = _renderManager->createMultiRenderTarget(MRT_MODEL_NAME, tList, w, h, Ogre::PF_FLOAT32_RGBA, Color::Black, true, _renderManager->getMainCamera());
	addRenderTarget(mrtScene);
}

/****************************************************************/
/* FINAL COMPOSITION */
/****************************************************************/
DPFinalComposition::DPFinalComposition() {
	_renderQuadVisible = true;
	_materialName = "matShowNormals";
	addRenderTarget(_renderManager->getRenderWindow());
}